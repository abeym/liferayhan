package org.cdph.han.synch.services;

import com.mir3.ws.AddNewRecipientsResponseType;
import com.mir3.ws.AddNewRecipientsType;
import com.mir3.ws.AddressType;
import com.mir3.ws.AlternatesType;
import com.mir3.ws.AuthorizationType;
import com.mir3.ws.CustomFieldType;
import com.mir3.ws.CustomFieldsType;
import com.mir3.ws.DeleteRecipientsResponseType;
import com.mir3.ws.DeleteRecipientsType;
import com.mir3.ws.Device;
import com.mir3.ws.DevicesType;
import com.mir3.ws.ErrorType;
import com.mir3.ws.ErrorsType;
import com.mir3.ws.LanguageType;
import com.mir3.ws.Mir3;
import com.mir3.ws.Mir3Service;
import com.mir3.ws.RecipientDetailType;
import com.mir3.ws.RecipientOneSearchType;
import com.mir3.ws.RecipientType;
import com.mir3.ws.ResponseType;
import com.mir3.ws.SearchRecipientsResponseType;
import com.mir3.ws.SearchRecipientsType;
import com.mir3.ws.SendReportType;
import com.mir3.ws.TimeZoneType;
import com.mir3.ws.UpdateOneRecipientType;
import com.mir3.ws.UpdateRecipientsResponseType;
import com.mir3.ws.UpdateRecipientsType;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.synch.dto.Mir3DeviceTO;
import org.cdph.han.synch.dto.Mir3RecipientResponse;
import org.cdph.han.synch.dto.Mir3UserTO;

public class UserSynchServiceImpl
  implements UserSynchService
{
  private static final Log log = LogFactory.getLog(UserSynchServiceImpl.class);
  private static final String DEFAULT_LOCATION = "DEFAULT_LOCATION";
  private Mir3 service = new Mir3Service().getMir3();
  
  @SuppressWarnings("rawtypes")
public List<Mir3RecipientResponse> addRecipients(List<Mir3UserTO> paramList, String paramString1, String paramString2, String paramString3)
  {
    ArrayList localArrayList = new ArrayList();
    AddNewRecipientsType localAddNewRecipientsType = new AddNewRecipientsType();
    AuthorizationType localAuthorizationType = new AuthorizationType();
    localAuthorizationType.setPassword(paramString2);
    localAuthorizationType.setUsername(paramString1);
    localAddNewRecipientsType.setApiVersion(paramString3);
    localAddNewRecipientsType.setAuthorization(localAuthorizationType);
    Object localObject1 = paramList.iterator();
    Object localObject2;
    while (((Iterator)localObject1).hasNext())
    {
      localObject2 = (Mir3UserTO)((Iterator)localObject1).next();
      RecipientDetailType localRecipientDetailType = new RecipientDetailType();
      localRecipientDetailType.setActiveLocationStatus("DEFAULT_LOCATION");
      localRecipientDetailType.setAddress(createAddress((Mir3UserTO)localObject2));
      localRecipientDetailType.setAlternates(createAlternates(((Mir3UserTO)localObject2).getAlternates()));
      localRecipientDetailType.setCompany(((Mir3UserTO)localObject2).getCompany());
      localRecipientDetailType.setCustomFields(createCustomFields((Mir3UserTO)localObject2));
      localRecipientDetailType.setDevices(createDevices(((Mir3UserTO)localObject2).getDevices()));
      localRecipientDetailType.setDivision(((Mir3UserTO)localObject2).getDivision());
      localRecipientDetailType.setEmployeeId(((Mir3UserTO)localObject2).getEmployeeId());
      localRecipientDetailType.setFirstName(((Mir3UserTO)localObject2).getFirstName());
      localRecipientDetailType.setJobTitle(((Mir3UserTO)localObject2).getJobTitle());
      localRecipientDetailType.setLanguage(createLanguage(((Mir3UserTO)localObject2).getLanguage()));
      localRecipientDetailType.setLastName(((Mir3UserTO)localObject2).getLastName());
      localRecipientDetailType.setPassword(((Mir3UserTO)localObject2).getPassword());
      localRecipientDetailType.setPin(((Mir3UserTO)localObject2).getPin());
      localRecipientDetailType.setRole(((Mir3UserTO)localObject2).getMir3Role());
      localRecipientDetailType.setTelephonyId(((Mir3UserTO)localObject2).getTelephonyId());
      localRecipientDetailType.setTimeZone(TimeZoneType.fromValue(((Mir3UserTO)localObject2).getTimezone()));
      localRecipientDetailType.setUsername(((Mir3UserTO)localObject2).getUsername());
      localAddNewRecipientsType.getRecipientDetail().add(localRecipientDetailType);
    }
    localObject1 = this.service.addNewRecipientsOp(localAddNewRecipientsType);
    Object localObject3;
    if ((((ResponseType)localObject1).getError() != null) && (!((ResponseType)localObject1).getError().isEmpty()))
    {
      localObject2 = ((ResponseType)localObject1).getError().iterator();
      while (((Iterator)localObject2).hasNext())
      {
        localObject3 = (ErrorType)((Iterator)localObject2).next();
        log.error("Error adding new recipients. Code: " + ((ErrorType)localObject3).getErrorCode() + " Error Message: " + ((ErrorType)localObject3).getErrorMessage());
      }
      throw new RuntimeException("Error adding new recipients.");
    }
    localObject2 = ((ResponseType)localObject1).getAddNewRecipientsResponse();
    Iterator localIterator = ((AddNewRecipientsResponseType)localObject2).getRecipient().iterator();
    while (localIterator.hasNext())
    {
      RecipientType localRecipientType = (RecipientType)localIterator.next();
      if ((localRecipientType.getErrors().getError() != null) && (!localRecipientType.getErrors().getError().isEmpty()))
      {
        ErrorType localErrorType = (ErrorType)localRecipientType.getErrors().getError().get(0);
        localObject3 = new Mir3RecipientResponse(localRecipientType.getUserId(), localRecipientType.getUsername(), localErrorType.getErrorMessage(), Integer.valueOf(localErrorType.getErrorCode()));
      }
      else
      {
        localObject3 = new Mir3RecipientResponse(localRecipientType.getUserId(), localRecipientType.getUsername(), null, null);
      }
      localArrayList.add(localObject3);
    }
    return localArrayList;
  }
  
  public List<Mir3RecipientResponse> updateRecipients(List<Mir3UserTO> paramList, String paramString1, String paramString2, String paramString3, boolean paramBoolean)
  {
    ArrayList localArrayList = new ArrayList();
    UpdateRecipientsType localUpdateRecipientsType = new UpdateRecipientsType();
    localUpdateRecipientsType.setApiVersion(paramString3);
    AuthorizationType localAuthorizationType = new AuthorizationType();
    localAuthorizationType.setPassword(paramString2);
    localAuthorizationType.setUsername(paramString1);
    localUpdateRecipientsType.setAuthorization(localAuthorizationType);
    localUpdateRecipientsType.setCreateIfNotFound(Boolean.valueOf(paramBoolean));
    Object localObject1 = paramList.iterator();
    Object localObject2;
    while (((Iterator)localObject1).hasNext())
    {
      localObject2 = (Mir3UserTO)((Iterator)localObject1).next();
      UpdateOneRecipientType localUpdateOneRecipientType = new UpdateOneRecipientType();
      RecipientType localRecipientType1 = new RecipientType();
      if (((Mir3UserTO)localObject2).getMir3Id() > 0) {
        localRecipientType1.setUserId(Integer.valueOf(((Mir3UserTO)localObject2).getMir3Id()));
      } else {
        localRecipientType1.setUsername(((Mir3UserTO)localObject2).getUsername());
      }
      RecipientDetailType localRecipientDetailType = new RecipientDetailType();
      localRecipientDetailType.setActiveLocationStatus("DEFAULT_LOCATION");
      localRecipientDetailType.setAddress(createAddress((Mir3UserTO)localObject2));
      localRecipientDetailType.setAlternates(createAlternates(((Mir3UserTO)localObject2).getAlternates()));
      localRecipientDetailType.setCompany(((Mir3UserTO)localObject2).getCompany());
      localRecipientDetailType.setCustomFields(createCustomFields((Mir3UserTO)localObject2));
      localRecipientDetailType.setDevices(createDevices(((Mir3UserTO)localObject2).getDevices()));
      localRecipientDetailType.setDivision(((Mir3UserTO)localObject2).getDivision());
      localRecipientDetailType.setEmployeeId(((Mir3UserTO)localObject2).getEmployeeId());
      localRecipientDetailType.setFirstName(((Mir3UserTO)localObject2).getFirstName());
      localRecipientDetailType.setJobTitle(((Mir3UserTO)localObject2).getJobTitle());
      localRecipientDetailType.setLanguage(createLanguage(((Mir3UserTO)localObject2).getLanguage()));
      localRecipientDetailType.setLastName(((Mir3UserTO)localObject2).getLastName());
      localRecipientDetailType.setPassword(((Mir3UserTO)localObject2).getPassword());
      localRecipientDetailType.setPin(((Mir3UserTO)localObject2).getPin());
      localRecipientDetailType.setRole(((Mir3UserTO)localObject2).getMir3Role());
      localRecipientDetailType.setTelephonyId(((Mir3UserTO)localObject2).getTelephonyId());
      localRecipientDetailType.setTimeZone(TimeZoneType.fromValue(((Mir3UserTO)localObject2).getTimezone()));
      localRecipientDetailType.setUsername(((Mir3UserTO)localObject2).getUsername());
      localUpdateOneRecipientType.setRecipientDetail(localRecipientDetailType);
      localUpdateOneRecipientType.setRecipient(localRecipientType1);
      localUpdateRecipientsType.getUpdateOneRecipient().add(localUpdateOneRecipientType);
    }
    localObject1 = this.service.updateRecipientsOp(localUpdateRecipientsType);
    Object localObject3;
    if ((((ResponseType)localObject1).getError() != null) && (!((ResponseType)localObject1).getError().isEmpty()))
    {
      localObject2 = ((ResponseType)localObject1).getError().iterator();
      while (((Iterator)localObject2).hasNext())
      {
        localObject3 = (ErrorType)((Iterator)localObject2).next();
        log.error("Error updating recipients. Code: " + ((ErrorType)localObject3).getErrorCode() + " Error Message: " + ((ErrorType)localObject3).getErrorMessage());
      }
      throw new RuntimeException("Error updating recipients.");
    }
    localObject2 = ((ResponseType)localObject1).getUpdateRecipientsResponse();
    Iterator localIterator = ((UpdateRecipientsResponseType)localObject2).getRecipient().iterator();
    while (localIterator.hasNext())
    {
      RecipientType localRecipientType2 = (RecipientType)localIterator.next();
      if ((localRecipientType2.getErrors().getError() != null) && (!localRecipientType2.getErrors().getError().isEmpty()))
      {
        ErrorType localErrorType = (ErrorType)localRecipientType2.getErrors().getError().get(0);
        localObject3 = new Mir3RecipientResponse(localRecipientType2.getUserId(), localRecipientType2.getUsername(), localErrorType.getErrorMessage(), Integer.valueOf(localErrorType.getErrorCode()));
      }
      else
      {
        localObject3 = new Mir3RecipientResponse(localRecipientType2.getUserId(), localRecipientType2.getUsername(), null, null);
      }
      localArrayList.add(localObject3);
    }
    return localArrayList;
  }
  
  public List<Mir3RecipientResponse> deleteRecipients(List<String> paramList, String paramString1, String paramString2, String paramString3)
  {
    ArrayList localArrayList = new ArrayList();
    DeleteRecipientsType localDeleteRecipientsType = new DeleteRecipientsType();
    localDeleteRecipientsType.setApiVersion(paramString3);
    AuthorizationType localAuthorizationType = new AuthorizationType();
    localAuthorizationType.setPassword(paramString2);
    localAuthorizationType.setUsername(paramString1);
    localDeleteRecipientsType.setAuthorization(localAuthorizationType);
    Object localObject1 = paramList.iterator();
    Object localObject2;
    while (((Iterator)localObject1).hasNext())
    {
      localObject2 = (String)((Iterator)localObject1).next();
      RecipientType localRecipientType1 = new RecipientType();
      localRecipientType1.setUsername((String)localObject2);
      localDeleteRecipientsType.getRecipient().add(localRecipientType1);
    }
    localObject1 = this.service.deleteRecipientsOp(localDeleteRecipientsType);
    Object localObject3;
    if ((((ResponseType)localObject1).getError() != null) && (!((ResponseType)localObject1).getError().isEmpty()))
    {
      localObject2 = ((ResponseType)localObject1).getError().iterator();
      while (((Iterator)localObject2).hasNext())
      {
        localObject3 = (ErrorType)((Iterator)localObject2).next();
        log.error("Error deleting recipients. Code: " + ((ErrorType)localObject3).getErrorCode() + " Error Message: " + ((ErrorType)localObject3).getErrorMessage());
      }
      throw new RuntimeException("Error deleting recipients.");
    }
    localObject2 = ((ResponseType)localObject1).getDeleteRecipientsResponse();
    Iterator localIterator = ((DeleteRecipientsResponseType)localObject2).getRecipient().iterator();
    while (localIterator.hasNext())
    {
      RecipientType localRecipientType2 = (RecipientType)localIterator.next();
      if ((localRecipientType2.getErrors().getError() != null) && (!localRecipientType2.getErrors().getError().isEmpty()))
      {
        ErrorType localErrorType = (ErrorType)localRecipientType2.getErrors().getError().get(0);
        localObject3 = new Mir3RecipientResponse(localRecipientType2.getUserId(), localRecipientType2.getUsername(), localErrorType.getErrorMessage(), Integer.valueOf(localErrorType.getErrorCode()));
      }
      else
      {
        localObject3 = new Mir3RecipientResponse(localRecipientType2.getUserId(), localRecipientType2.getUsername(), null, null);
      }
      localArrayList.add(localObject3);
    }
    return localArrayList;
  }
  
  public Mir3UserTO searchRecipient(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    SearchRecipientsType localSearchRecipientsType = new SearchRecipientsType();
    localSearchRecipientsType.setApiVersion(paramString4);
    AuthorizationType localAuthorizationType = new AuthorizationType();
    localAuthorizationType.setPassword(paramString3);
    localAuthorizationType.setUsername(paramString2);
    localSearchRecipientsType.setAuthorization(localAuthorizationType);
    localSearchRecipientsType.setIncludeDetail(Boolean.valueOf(true));
    RecipientOneSearchType localRecipientOneSearchType = new RecipientOneSearchType();
    localRecipientOneSearchType.setUsername(paramString1);
    localSearchRecipientsType.setQuery(localRecipientOneSearchType);
    ResponseType localResponseType = this.service.searchRecipientsOp(localSearchRecipientsType);
    Object localObject1;
    Mir3UserTO localMir3UserTO;
    Object localObject2;
    if (localResponseType.isSuccess())
    {
      localObject1 = localResponseType.getSearchRecipientsResponse();
      if (((SearchRecipientsResponseType)localObject1).getMatchCount() == 0)
      {
        localMir3UserTO = null;
      }
      else
      {
        localMir3UserTO = new Mir3UserTO();
        localObject2 = (RecipientDetailType)((SearchRecipientsResponseType)localObject1).getRecipientDetail().get(0);
        localMir3UserTO.setUsername(((RecipientDetailType)localObject2).getUsername());
        localMir3UserTO.setPin(((RecipientDetailType)localObject2).getPin());
        localMir3UserTO.setTelephonyId(((RecipientDetailType)localObject2).getTelephonyId());
        localMir3UserTO.setPassword(((RecipientDetailType)localObject2).getPassword());
      }
    }
    else
    {
      localObject1 = localResponseType.getError().iterator();
      while (((Iterator)localObject1).hasNext())
      {
        localObject2 = (ErrorType)((Iterator)localObject1).next();
        log.error("Error code: " + ((ErrorType)localObject2).getErrorCode() + " Error Message: " + ((ErrorType)localObject2).getErrorMessage());
      }
      throw new RuntimeException("Error querying recipient.");
    }
    return localMir3UserTO;
  }
  
  private AddressType createAddress(Mir3UserTO paramMir3UserTO)
  {
    AddressType localAddressType = new AddressType();
    localAddressType.setAddress1(paramMir3UserTO.getAddress1());
    localAddressType.setAddress2(paramMir3UserTO.getAddress2());
    localAddressType.setCity(paramMir3UserTO.getCity());
    localAddressType.setCountry(paramMir3UserTO.getCountry());
    localAddressType.setFloor(paramMir3UserTO.getFloor());
    localAddressType.setState(paramMir3UserTO.getState());
    localAddressType.setZip(paramMir3UserTO.getZip());
    return localAddressType;
  }
  
  private AlternatesType createAlternates(List<String> paramList)
  {
    AlternatesType localAlternatesType = (paramList == null) || (paramList.size() == 0) ? null : new AlternatesType();
    if (paramList != null)
    {
      Iterator localIterator = paramList.iterator();
      while (localIterator.hasNext())
      {
        String str = (String)localIterator.next();
        RecipientType localRecipientType = new RecipientType();
        localRecipientType.setUsername(str);
        localAlternatesType.getRecipient().add(localRecipientType);
      }
    }
    return localAlternatesType;
  }
  
  private CustomFieldsType createCustomFields(Mir3UserTO paramMir3UserTO)
  {
    CustomFieldsType localCustomFieldsType = new CustomFieldsType();
    CustomFieldType localCustomFieldType = new CustomFieldType();
    localCustomFieldType.setName("Department");
    localCustomFieldType.setValue(paramMir3UserTO.getDepartment() == null ? "" : paramMir3UserTO.getDepartment());
    localCustomFieldsType.getCustomField().add(localCustomFieldType);
    localCustomFieldType = new CustomFieldType();
    localCustomFieldType.setName("Role (HAN)");
    localCustomFieldType.setValue(paramMir3UserTO.getRole() == null ? "" : paramMir3UserTO.getRole());
    localCustomFieldsType.getCustomField().add(localCustomFieldType);
    localCustomFieldType = new CustomFieldType();
    localCustomFieldType.setName("Area/Field");
    localCustomFieldType.setValue(paramMir3UserTO.getAreaField() == null ? "" : paramMir3UserTO.getAreaField());
    localCustomFieldsType.getCustomField().add(localCustomFieldType);
    localCustomFieldType = new CustomFieldType();
    localCustomFieldType.setName("Type of Organization");
    localCustomFieldType.setValue(paramMir3UserTO.getOrganizationType() == null ? "" : paramMir3UserTO.getOrganizationType());
    localCustomFieldsType.getCustomField().add(localCustomFieldType);
    localCustomFieldType = new CustomFieldType();
    localCustomFieldType.setName("Organization");
    localCustomFieldType.setValue(paramMir3UserTO.getOrganization() == null ? "" : paramMir3UserTO.getOrganization());
    localCustomFieldsType.getCustomField().add(localCustomFieldType);
    return localCustomFieldsType;
  }
  
  private DevicesType createDevices(List<Mir3DeviceTO> paramList)
  {
    DevicesType localDevicesType = new DevicesType();
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      Mir3DeviceTO localMir3DeviceTO = (Mir3DeviceTO)localIterator.next();
      Device localDevice = new Device();
      localDevice.setAddress(localMir3DeviceTO.getAddress());
      localDevice.setDescription(localMir3DeviceTO.getDescription());
      localDevice.setDeviceType(localMir3DeviceTO.getType());
      localDevice.setPagerPin(localMir3DeviceTO.getPagerPin());
      localDevice.setPagingCarrierId(localMir3DeviceTO.getPagerCarrier());
      if (localMir3DeviceTO.getSendReports() != null) {
        localDevice.setSendReports2(SendReportType.fromValue(localMir3DeviceTO.getSendReports()));
      }
      localDevice.setDisabled(Boolean.valueOf(localMir3DeviceTO.isDisabled()));
      localDevice.setDefaultPriority(localMir3DeviceTO.getPriority());
      localDevicesType.getDevice().add(localDevice);
    }
    return localDevicesType;
  }
  
  private LanguageType createLanguage(String paramString)
  {
    LanguageType localLanguageType = LanguageType.fromValue(paramString);
    return localLanguageType;
  }
}


/* Location:              C:\dev\analysis\hanwsclient.jar!\org\cdph\han\synch\services\UserSynchServiceImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */