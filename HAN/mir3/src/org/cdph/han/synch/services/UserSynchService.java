package org.cdph.han.synch.services;

import java.util.List;
import org.cdph.han.synch.dto.Mir3RecipientResponse;
import org.cdph.han.synch.dto.Mir3UserTO;

public abstract interface UserSynchService
{
  public abstract List<Mir3RecipientResponse> addRecipients(List<Mir3UserTO> paramList, String paramString1, String paramString2, String paramString3);
  
  public abstract List<Mir3RecipientResponse> updateRecipients(List<Mir3UserTO> paramList, String paramString1, String paramString2, String paramString3, boolean paramBoolean);
  
  public abstract List<Mir3RecipientResponse> deleteRecipients(List<String> paramList, String paramString1, String paramString2, String paramString3);
  
  public abstract Mir3UserTO searchRecipient(String paramString1, String paramString2, String paramString3, String paramString4);
}


/* Location:              C:\dev\analysis\hanwsclient.jar!\org\cdph\han\synch\services\UserSynchService.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */