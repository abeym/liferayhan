package org.cdph.han.synch.dto;

import java.io.Serializable;

public class Mir3DevicePriorityTO
  implements Serializable
{
  private static final long serialVersionUID = -5796065382311414263L;
  private int priority;
  private int index;
  
  public Mir3DevicePriorityTO(int paramInt1, int paramInt2)
  {
    this.priority = paramInt1;
    this.index = paramInt2;
  }
  
  public Mir3DevicePriorityTO() {}
  
  public int getPriority()
  {
    return this.priority;
  }
  
  public void setPriority(int paramInt)
  {
    this.priority = paramInt;
  }
  
  public int getIndex()
  {
    return this.index;
  }
  
  public void setIndex(int paramInt)
  {
    this.index = paramInt;
  }
}


/* Location:              C:\dev\analysis\hanwsclient.jar!\org\cdph\han\synch\dto\Mir3DevicePriorityTO.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */