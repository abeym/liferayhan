package org.cdph.han.synch.dto;

import java.io.Serializable;

public class Mir3RecipientResponse
  implements Serializable
{
  private static final long serialVersionUID = -1278056319661382629L;
  private Integer userId;
  private String username;
  private String errorMessage;
  private Integer errorCode;
  
  public Mir3RecipientResponse(Integer paramInteger1, String paramString1, String paramString2, Integer paramInteger2)
  {
    this.userId = paramInteger1;
    this.username = paramString1;
    this.errorMessage = paramString2;
    this.errorCode = paramInteger2;
  }
  
  public Integer getUserId()
  {
    return this.userId;
  }
  
  public String getUsername()
  {
    return this.username;
  }
  
  public String getErrorMessage()
  {
    return this.errorMessage;
  }
  
  public Integer getErrorCode()
  {
    return this.errorCode;
  }
}


/* Location:              C:\dev\analysis\hanwsclient.jar!\org\cdph\han\synch\dto\Mir3RecipientResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */