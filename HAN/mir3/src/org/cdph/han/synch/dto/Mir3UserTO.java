package org.cdph.han.synch.dto;

import java.io.Serializable;
import java.util.List;

public class Mir3UserTO
  implements Serializable
{
  private static final long serialVersionUID = 8020122268952771892L;
  private String mir3Role;
  private String language;
  private String firstName;
  private String lastName;
  private String company;
  private String jobTitle;
  private String division;
  private String timezone;
  private String telephonyId;
  private String employeeId;
  private String pin;
  private String username;
  private String password;
  private String address1;
  private String address2;
  private String floor;
  private String city;
  private String state;
  private String zip;
  private String country;
  private String organization;
  private String areaField;
  private String title;
  private String organizationType;
  private String department;
  private String role;
  private List<String> alternates;
  private List<Mir3DeviceTO> devices;
  private int mir3Id;
  
  public Mir3UserTO(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, String paramString9, String paramString10, String paramString11, String paramString12, String paramString13, String paramString14, String paramString15, String paramString16, String paramString17, String paramString18, String paramString19, String paramString20, String paramString21, String paramString22, String paramString23, String paramString24, String paramString25, String paramString26, List<String> paramList, List<Mir3DeviceTO> paramList1)
  {
    this.mir3Role = paramString1;
    this.language = paramString2;
    this.firstName = paramString3;
    this.lastName = paramString4;
    this.company = paramString5;
    this.jobTitle = paramString6;
    this.division = paramString7;
    this.timezone = paramString8;
    this.telephonyId = paramString9;
    this.employeeId = paramString10;
    this.pin = paramString11;
    this.username = paramString12;
    this.password = paramString13;
    this.address1 = paramString14;
    this.address2 = paramString15;
    this.floor = paramString16;
    this.city = paramString17;
    this.state = paramString18;
    this.zip = paramString19;
    this.country = paramString20;
    this.organization = paramString21;
    this.areaField = paramString22;
    this.title = paramString23;
    this.organizationType = paramString24;
    this.department = paramString25;
    this.role = paramString26;
    this.alternates = paramList;
    this.devices = paramList1;
  }
  
  public Mir3UserTO() {}
  
  public String getMir3Role()
  {
    return this.mir3Role;
  }
  
  public void setMir3Role(String paramString)
  {
    this.mir3Role = paramString;
  }
  
  public String getLanguage()
  {
    return this.language;
  }
  
  public void setLanguage(String paramString)
  {
    this.language = paramString;
  }
  
  public String getFirstName()
  {
    return this.firstName;
  }
  
  public void setFirstName(String paramString)
  {
    this.firstName = paramString;
  }
  
  public String getLastName()
  {
    return this.lastName;
  }
  
  public void setLastName(String paramString)
  {
    this.lastName = paramString;
  }
  
  public String getCompany()
  {
    return this.company;
  }
  
  public void setCompany(String paramString)
  {
    this.company = paramString;
  }
  
  public String getJobTitle()
  {
    return this.jobTitle;
  }
  
  public void setJobTitle(String paramString)
  {
    this.jobTitle = paramString;
  }
  
  public String getDivision()
  {
    return this.division;
  }
  
  public void setDivision(String paramString)
  {
    this.division = paramString;
  }
  
  public String getTimezone()
  {
    return this.timezone;
  }
  
  public void setTimezone(String paramString)
  {
    this.timezone = paramString;
  }
  
  public String getTelephonyId()
  {
    return this.telephonyId;
  }
  
  public void setTelephonyId(String paramString)
  {
    this.telephonyId = paramString;
  }
  
  public String getEmployeeId()
  {
    return this.employeeId;
  }
  
  public void setEmployeeId(String paramString)
  {
    this.employeeId = paramString;
  }
  
  public String getPin()
  {
    return this.pin;
  }
  
  public void setPin(String paramString)
  {
    this.pin = paramString;
  }
  
  public String getUsername()
  {
    return this.username;
  }
  
  public void setUsername(String paramString)
  {
    this.username = paramString;
  }
  
  public String getPassword()
  {
    return this.password;
  }
  
  public void setPassword(String paramString)
  {
    this.password = paramString;
  }
  
  public String getAddress1()
  {
    return this.address1;
  }
  
  public void setAddress1(String paramString)
  {
    this.address1 = paramString;
  }
  
  public String getAddress2()
  {
    return this.address2;
  }
  
  public void setAddress2(String paramString)
  {
    this.address2 = paramString;
  }
  
  public String getFloor()
  {
    return this.floor;
  }
  
  public void setFloor(String paramString)
  {
    this.floor = paramString;
  }
  
  public String getCity()
  {
    return this.city;
  }
  
  public void setCity(String paramString)
  {
    this.city = paramString;
  }
  
  public String getState()
  {
    return this.state;
  }
  
  public void setState(String paramString)
  {
    this.state = paramString;
  }
  
  public String getZip()
  {
    return this.zip;
  }
  
  public void setZip(String paramString)
  {
    this.zip = paramString;
  }
  
  public String getCountry()
  {
    return this.country;
  }
  
  public void setCountry(String paramString)
  {
    this.country = paramString;
  }
  
  public String getOrganization()
  {
    return this.organization;
  }
  
  public void setOrganization(String paramString)
  {
    this.organization = paramString;
  }
  
  public String getAreaField()
  {
    return this.areaField;
  }
  
  public void setAreaField(String paramString)
  {
    this.areaField = paramString;
  }
  
  public String getTitle()
  {
    return this.title;
  }
  
  public void setTitle(String paramString)
  {
    this.title = paramString;
  }
  
  public String getOrganizationType()
  {
    return this.organizationType;
  }
  
  public void setOrganizationType(String paramString)
  {
    this.organizationType = paramString;
  }
  
  public String getDepartment()
  {
    return this.department;
  }
  
  public void setDepartment(String paramString)
  {
    this.department = paramString;
  }
  
  public String getRole()
  {
    return this.role;
  }
  
  public void setRole(String paramString)
  {
    this.role = paramString;
  }
  
  public List<String> getAlternates()
  {
    return this.alternates;
  }
  
  public void setAlternates(List<String> paramList)
  {
    this.alternates = paramList;
  }
  
  public List<Mir3DeviceTO> getDevices()
  {
    return this.devices;
  }
  
  public void setDevices(List<Mir3DeviceTO> paramList)
  {
    this.devices = paramList;
  }
  
  public int getMir3Id()
  {
    return this.mir3Id;
  }
  
  public void setMir3Id(int paramInt)
  {
    this.mir3Id = paramInt;
  }
}


/* Location:              C:\dev\analysis\hanwsclient.jar!\org\cdph\han\synch\dto\Mir3UserTO.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */