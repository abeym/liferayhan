package org.cdph.han.synch.dto;

import java.io.Serializable;

public class Mir3DeviceTO
  implements Serializable
{
  private static final long serialVersionUID = -8829940961016774032L;
  private String address;
  private String description;
  private String pagerPin;
  private Integer pagerCarrier;
  private String sendReports;
  private String type;
  private String priority;
  private boolean disabled;
  
  public String getAddress()
  {
    return this.address;
  }
  
  public void setAddress(String paramString)
  {
    this.address = paramString;
  }
  
  public String getDescription()
  {
    return this.description;
  }
  
  public void setDescription(String paramString)
  {
    this.description = paramString;
  }
  
  public String getPagerPin()
  {
    return this.pagerPin;
  }
  
  public void setPagerPin(String paramString)
  {
    this.pagerPin = paramString;
  }
  
  public Integer getPagerCarrier()
  {
    return this.pagerCarrier;
  }
  
  public void setPagerCarrier(Integer paramInteger)
  {
    this.pagerCarrier = paramInteger;
  }
  
  public String getSendReports()
  {
    return this.sendReports;
  }
  
  public void setSendReports(String paramString)
  {
    this.sendReports = paramString;
  }
  
  public String getType()
  {
    return this.type;
  }
  
  public void setType(String paramString)
  {
    this.type = paramString;
  }
  
  public String getPriority()
  {
    return this.priority;
  }
  
  public void setPriority(String paramString)
  {
    this.priority = paramString;
  }
  
  public boolean isDisabled()
  {
    return this.disabled;
  }
  
  public void setDisabled(boolean paramBoolean)
  {
    this.disabled = paramBoolean;
  }
}


/* Location:              C:\dev\analysis\hanwsclient.jar!\org\cdph\han\synch\dto\Mir3DeviceTO.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */