/*    */ package com.mir3.ws;
/*    */ 
/*    */ import java.util.ArrayList;
/*    */ import java.util.List;
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="changeIncidentsStatusResponseType", propOrder={"incident"})
/*    */ public class ChangeIncidentsStatusResponseType
/*    */ {
/*    */   @XmlElement(required=true)
/*    */   protected List<StatusChangeResponseType> incident;
/*    */   
/*    */   public List<StatusChangeResponseType> getIncident()
/*    */   {
/* 63 */     if (this.incident == null) {
/* 64 */       this.incident = new ArrayList();
/*    */     }
/* 66 */     return this.incident;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\ChangeIncidentsStatusResponseType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */