/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="downloadVoiceFileResponseType", propOrder={"division", "voiceFile"})
/*    */ public class DownloadVoiceFileResponseType
/*    */ {
/*    */   @XmlElement(required=true)
/*    */   protected String division;
/*    */   @XmlElement(required=true)
/*    */   protected VoiceFileType voiceFile;
/*    */   
/*    */   public String getDivision()
/*    */   {
/* 51 */     return this.division;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setDivision(String value)
/*    */   {
/* 63 */     this.division = value;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public VoiceFileType getVoiceFile()
/*    */   {
/* 75 */     return this.voiceFile;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setVoiceFile(VoiceFileType value)
/*    */   {
/* 87 */     this.voiceFile = value;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\DownloadVoiceFileResponseType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */