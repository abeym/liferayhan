/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlEnum;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlType(name="keywordMatchType")
/*    */ @XmlEnum
/*    */ public enum KeywordMatchType
/*    */ {
/* 28 */   ALL, 
/* 29 */   ANY, 
/* 30 */   ADVANCED;
/*    */   
/*    */   public String value() {
/* 33 */     return name();
/*    */   }
/*    */   
/*    */   public static KeywordMatchType fromValue(String v) {
/* 37 */     return valueOf(v);
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\KeywordMatchType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */