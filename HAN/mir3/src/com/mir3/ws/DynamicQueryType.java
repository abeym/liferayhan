/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="dynamicQueryType", propOrder={"divisionPermission", "dynamicCondition"})
/*    */ public class DynamicQueryType
/*    */ {
/*    */   protected DivisionPermissionType divisionPermission;
/*    */   @XmlElement(required=true)
/*    */   protected DynamicConditionSearchType dynamicCondition;
/*    */   
/*    */   public DivisionPermissionType getDivisionPermission()
/*    */   {
/* 50 */     return this.divisionPermission;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setDivisionPermission(DivisionPermissionType value)
/*    */   {
/* 62 */     this.divisionPermission = value;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public DynamicConditionSearchType getDynamicCondition()
/*    */   {
/* 74 */     return this.dynamicCondition;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setDynamicCondition(DynamicConditionSearchType value)
/*    */   {
/* 86 */     this.dynamicCondition = value;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\DynamicQueryType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */