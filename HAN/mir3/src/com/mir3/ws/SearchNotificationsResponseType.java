/*     */ package com.mir3.ws;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="searchNotificationsResponseType", propOrder={"matchCount", "returnCount", "notificationDetail", "notification"})
/*     */ public class SearchNotificationsResponseType
/*     */ {
/*     */   protected int matchCount;
/*     */   protected int returnCount;
/*     */   protected List<NotificationDetailType> notificationDetail;
/*     */   protected List<String> notification;
/*     */   
/*     */   public int getMatchCount()
/*     */   {
/*  52 */     return this.matchCount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setMatchCount(int value)
/*     */   {
/*  60 */     this.matchCount = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getReturnCount()
/*     */   {
/*  68 */     return this.returnCount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setReturnCount(int value)
/*     */   {
/*  76 */     this.returnCount = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<NotificationDetailType> getNotificationDetail()
/*     */   {
/* 102 */     if (this.notificationDetail == null) {
/* 103 */       this.notificationDetail = new ArrayList();
/*     */     }
/* 105 */     return this.notificationDetail;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getNotification()
/*     */   {
/* 131 */     if (this.notification == null) {
/* 132 */       this.notification = new ArrayList();
/*     */     }
/* 134 */     return this.notification;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\SearchNotificationsResponseType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */