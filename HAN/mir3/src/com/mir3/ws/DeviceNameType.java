/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlEnum;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlType(name="deviceNameType")
/*    */ @XmlEnum
/*    */ public enum DeviceNameType
/*    */ {
/* 36 */   HOME_PHONE, 
/* 37 */   WORK_PHONE, 
/* 38 */   MOBILE_PHONE, 
/* 39 */   HOME_EMAIL, 
/* 40 */   WORK_EMAIL, 
/* 41 */   SMS, 
/* 42 */   PAGER_ONE_WAY, 
/* 43 */   PAGER_TWO_WAY, 
/* 44 */   PAGER_NUMERIC, 
/* 45 */   FAX, 
/* 46 */   TTY_PHONE;
/*    */   
/*    */   public String value() {
/* 49 */     return name();
/*    */   }
/*    */   
/*    */   public static DeviceNameType fromValue(String v) {
/* 53 */     return valueOf(v);
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\DeviceNameType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */