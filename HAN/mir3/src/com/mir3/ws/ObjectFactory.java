/*      */ package com.mir3.ws;
/*      */ 
/*      */ import javax.xml.bind.JAXBElement;
/*      */ import javax.xml.bind.annotation.XmlElementDecl;
/*      */ import javax.xml.bind.annotation.XmlRegistry;
/*      */ import javax.xml.namespace.QName;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ @XmlRegistry
/*      */ public class ObjectFactory
/*      */ {
/*   27 */   private static final QName _SearchRecipientGroups_QNAME = new QName("http://www.mir3.com/ws", "searchRecipientGroups");
/*   28 */   private static final QName _UpdateNotifications_QNAME = new QName("http://www.mir3.com/ws", "updateNotifications");
/*   29 */   private static final QName _GetProviderInfo_QNAME = new QName("http://www.mir3.com/ws", "getProviderInfo");
/*   30 */   private static final QName _UploadVoiceFileResponse_QNAME = new QName("http://www.mir3.com/ws", "uploadVoiceFileResponse");
/*   31 */   private static final QName _GetRecipientRoles_QNAME = new QName("http://www.mir3.com/ws", "getRecipientRoles");
/*   32 */   private static final QName _SetCustomVerbiageTypeByDivision_QNAME = new QName("http://www.mir3.com/ws", "setCustomVerbiageTypeByDivision");
/*   33 */   private static final QName _AddNewDivisions_QNAME = new QName("http://www.mir3.com/ws", "addNewDivisions");
/*   34 */   private static final QName _TerminateNotificationGroups_QNAME = new QName("http://www.mir3.com/ws", "terminateNotificationGroups");
/*   35 */   private static final QName _UpdateNotificationGroups_QNAME = new QName("http://www.mir3.com/ws", "updateNotificationGroups");
/*   36 */   private static final QName _ChangeIncidentsStatus_QNAME = new QName("http://www.mir3.com/ws", "changeIncidentsStatus");
/*   37 */   private static final QName _SearchNotificationGroups_QNAME = new QName("http://www.mir3.com/ws", "searchNotificationGroups");
/*   38 */   private static final QName _GetNotificationSchedule_QNAME = new QName("http://www.mir3.com/ws", "getNotificationSchedule");
/*   39 */   private static final QName _SearchRecipientSchedules_QNAME = new QName("http://www.mir3.com/ws", "searchRecipientSchedules");
/*   40 */   private static final QName _DeleteDivisions_QNAME = new QName("http://www.mir3.com/ws", "deleteDivisions");
/*   41 */   private static final QName _UpdateRecipientGroups_QNAME = new QName("http://www.mir3.com/ws", "updateRecipientGroups");
/*   42 */   private static final QName _RemoveRecipientsFromGroup_QNAME = new QName("http://www.mir3.com/ws", "removeRecipientsFromGroup");
/*   43 */   private static final QName _DeleteSeverity_QNAME = new QName("http://www.mir3.com/ws", "deleteSeverity");
/*   44 */   private static final QName _GetPortUtilization_QNAME = new QName("http://www.mir3.com/ws", "getPortUtilization");
/*   45 */   private static final QName _SearchIncidents_QNAME = new QName("http://www.mir3.com/ws", "searchIncidents");
/*   46 */   private static final QName _DeleteNotifications_QNAME = new QName("http://www.mir3.com/ws", "deleteNotifications");
/*   47 */   private static final QName _DownloadVoiceFile_QNAME = new QName("http://www.mir3.com/ws", "downloadVoiceFile");
/*   48 */   private static final QName _GetLoggedInUser_QNAME = new QName("http://www.mir3.com/ws", "getLoggedInUser");
/*   49 */   private static final QName _HideReportsResponse_QNAME = new QName("http://www.mir3.com/ws", "hideReportsResponse");
/*   50 */   private static final QName _GetAvailableTopicsResponse_QNAME = new QName("http://www.mir3.com/ws", "getAvailableTopicsResponse");
/*   51 */   private static final QName _UpdatePriority_QNAME = new QName("http://www.mir3.com/ws", "updatePriority");
/*   52 */   private static final QName _DeleteCategory_QNAME = new QName("http://www.mir3.com/ws", "deleteCategory");
/*   53 */   private static final QName _GetAvailableTopics_QNAME = new QName("http://www.mir3.com/ws", "getAvailableTopics");
/*   54 */   private static final QName _SearchRecipients_QNAME = new QName("http://www.mir3.com/ws", "searchRecipients");
/*   55 */   private static final QName _SearchVoiceFiles_QNAME = new QName("http://www.mir3.com/ws", "searchVoiceFiles");
/*   56 */   private static final QName _AddRecipientsToGroup_QNAME = new QName("http://www.mir3.com/ws", "addRecipientsToGroup");
/*   57 */   private static final QName _AddNewRecipients_QNAME = new QName("http://www.mir3.com/ws", "addNewRecipients");
/*   58 */   private static final QName _UpdateRecipients_QNAME = new QName("http://www.mir3.com/ws", "updateRecipients");
/*   59 */   private static final QName _GetRecipientScheduleDetail_QNAME = new QName("http://www.mir3.com/ws", "getRecipientScheduleDetail");
/*   60 */   private static final QName _PermissionsCheck_QNAME = new QName("http://www.mir3.com/ws", "permissionsCheck");
/*   61 */   private static final QName _DisableDevices_QNAME = new QName("http://www.mir3.com/ws", "disableDevices");
/*   62 */   private static final QName _DeleteRecipients_QNAME = new QName("http://www.mir3.com/ws", "deleteRecipients");
/*   63 */   private static final QName _DeleteNotificationGroups_QNAME = new QName("http://www.mir3.com/ws", "deleteNotificationGroups");
/*   64 */   private static final QName _UploadVoiceFile_QNAME = new QName("http://www.mir3.com/ws", "uploadVoiceFile");
/*   65 */   private static final QName _SetNotificationSchedule_QNAME = new QName("http://www.mir3.com/ws", "setNotificationSchedule");
/*   66 */   private static final QName _GetNotificationScheduleResponse_QNAME = new QName("http://www.mir3.com/ws", "getNotificationScheduleResponse");
/*   67 */   private static final QName _UpdateDivision_QNAME = new QName("http://www.mir3.com/ws", "updateDivision");
/*   68 */   private static final QName _ChooseNotificationResponseOption_QNAME = new QName("http://www.mir3.com/ws", "chooseNotificationResponseOption");
/*   69 */   private static final QName _Response_QNAME = new QName("http://www.mir3.com/ws", "response");
/*   70 */   private static final QName _SearchVoiceFilesResponse_QNAME = new QName("http://www.mir3.com/ws", "searchVoiceFilesResponse");
/*   71 */   private static final QName _GetNotificationGroupReport_QNAME = new QName("http://www.mir3.com/ws", "getNotificationGroupReport");
/*   72 */   private static final QName _SetAllowedStatus_QNAME = new QName("http://www.mir3.com/ws", "setAllowedStatus");
/*   73 */   private static final QName _SearchNotifications_QNAME = new QName("http://www.mir3.com/ws", "searchNotifications");
/*   74 */   private static final QName _DeletePriority_QNAME = new QName("http://www.mir3.com/ws", "deletePriority");
/*   75 */   private static final QName _GetEmailAttachment_QNAME = new QName("http://www.mir3.com/ws", "getEmailAttachment");
/*   76 */   private static final QName _SetPhonePrefixes_QNAME = new QName("http://www.mir3.com/ws", "setPhonePrefixes");
/*   77 */   private static final QName _DeleteIncidents_QNAME = new QName("http://www.mir3.com/ws", "deleteIncidents");
/*   78 */   private static final QName _GetRecipientGroups_QNAME = new QName("http://www.mir3.com/ws", "getRecipientGroups");
/*   79 */   private static final QName _SearchReports_QNAME = new QName("http://www.mir3.com/ws", "searchReports");
/*   80 */   private static final QName _GetDisabledDevices_QNAME = new QName("http://www.mir3.com/ws", "getDisabledDevices");
/*   81 */   private static final QName _GetRecordedResponse_QNAME = new QName("http://www.mir3.com/ws", "getRecordedResponse");
/*   82 */   private static final QName _ExecuteCustomReport_QNAME = new QName("http://www.mir3.com/ws", "executeCustomReport");
/*   83 */   private static final QName _TdxWrapper_QNAME = new QName("http://www.mir3.com/ws", "tdxWrapper");
/*   84 */   private static final QName _PrepareToRecordMessages_QNAME = new QName("http://www.mir3.com/ws", "prepareToRecordMessages");
/*   85 */   private static final QName _AddNewRecipientGroups_QNAME = new QName("http://www.mir3.com/ws", "addNewRecipientGroups");
/*   86 */   private static final QName _GetNotificationReportRecipients_QNAME = new QName("http://www.mir3.com/ws", "getNotificationReportRecipients");
/*   87 */   private static final QName _GetNotificationReports_QNAME = new QName("http://www.mir3.com/ws", "getNotificationReports");
/*   88 */   private static final QName _DeleteVoiceFilesResponse_QNAME = new QName("http://www.mir3.com/ws", "deleteVoiceFilesResponse");
/*   89 */   private static final QName _UpdateCategory_QNAME = new QName("http://www.mir3.com/ws", "updateCategory");
/*   90 */   private static final QName _GetVerbiage_QNAME = new QName("http://www.mir3.com/ws", "getVerbiage");
/*   91 */   private static final QName _GetPhonePrefixes_QNAME = new QName("http://www.mir3.com/ws", "getPhonePrefixes");
/*   92 */   private static final QName _GetPagingCarriers_QNAME = new QName("http://www.mir3.com/ws", "getPagingCarriers");
/*   93 */   private static final QName _DeleteRecipientSchedules_QNAME = new QName("http://www.mir3.com/ws", "deleteRecipientSchedules");
/*   94 */   private static final QName _DeleteRecipientGroups_QNAME = new QName("http://www.mir3.com/ws", "deleteRecipientGroups");
/*   95 */   private static final QName _SetVerbiage_QNAME = new QName("http://www.mir3.com/ws", "setVerbiage");
/*   96 */   private static final QName _AddNewRecipientSchedules_QNAME = new QName("http://www.mir3.com/ws", "addNewRecipientSchedules");
/*   97 */   private static final QName _HideReports_QNAME = new QName("http://www.mir3.com/ws", "hideReports");
/*   98 */   private static final QName _GetAllowedSMSList_QNAME = new QName("http://www.mir3.com/ws", "getAllowedSMSList");
/*   99 */   private static final QName _PermissionsCheckResponse_QNAME = new QName("http://www.mir3.com/ws", "permissionsCheckResponse");
/*  100 */   private static final QName _IdConvert_QNAME = new QName("http://www.mir3.com/ws", "idConvert");
/*  101 */   private static final QName _GetBlockedSMSList_QNAME = new QName("http://www.mir3.com/ws", "getBlockedSMSList");
/*  102 */   private static final QName _UpdateRecipientSchedules_QNAME = new QName("http://www.mir3.com/ws", "updateRecipientSchedules");
/*  103 */   private static final QName _GetBlockedStatus_QNAME = new QName("http://www.mir3.com/ws", "getBlockedStatus");
/*  104 */   private static final QName _AddNewNotifications_QNAME = new QName("http://www.mir3.com/ws", "addNewNotifications");
/*  105 */   private static final QName _DownloadVoiceFileResponse_QNAME = new QName("http://www.mir3.com/ws", "downloadVoiceFileResponse");
/*  106 */   private static final QName _AddCategory_QNAME = new QName("http://www.mir3.com/ws", "addCategory");
/*  107 */   private static final QName _OneStepNotification_QNAME = new QName("http://www.mir3.com/ws", "oneStepNotification");
/*  108 */   private static final QName _SetEmailAttachment_QNAME = new QName("http://www.mir3.com/ws", "setEmailAttachment");
/*  109 */   private static final QName _TerminateNotifications_QNAME = new QName("http://www.mir3.com/ws", "terminateNotifications");
/*  110 */   private static final QName _DeleteVoiceFiles_QNAME = new QName("http://www.mir3.com/ws", "deleteVoiceFiles");
/*  111 */   private static final QName _AddSeverity_QNAME = new QName("http://www.mir3.com/ws", "addSeverity");
/*  112 */   private static final QName _GetDivisions_QNAME = new QName("http://www.mir3.com/ws", "getDivisions");
/*  113 */   private static final QName _InitiateNotifications_QNAME = new QName("http://www.mir3.com/ws", "initiateNotifications");
/*  114 */   private static final QName _CreateSingleSignOnToken_QNAME = new QName("http://www.mir3.com/ws", "createSingleSignOnToken");
/*  115 */   private static final QName _AddPriority_QNAME = new QName("http://www.mir3.com/ws", "addPriority");
/*  116 */   private static final QName _GetDynamicGroupMembers_QNAME = new QName("http://www.mir3.com/ws", "getDynamicGroupMembers");
/*  117 */   private static final QName _InitiateNotificationGroups_QNAME = new QName("http://www.mir3.com/ws", "initiateNotificationGroups");
/*  118 */   private static final QName _ValidateSingleSignOnToken_QNAME = new QName("http://www.mir3.com/ws", "validateSingleSignOnToken");
/*  119 */   private static final QName _GetCustomVerbiageTypeByDivision_QNAME = new QName("http://www.mir3.com/ws", "getCustomVerbiageTypeByDivision");
/*  120 */   private static final QName _UpdateSeverity_QNAME = new QName("http://www.mir3.com/ws", "updateSeverity");
/*  121 */   private static final QName _AddNewNotificationGroups_QNAME = new QName("http://www.mir3.com/ws", "addNewNotificationGroups");
/*  122 */   private static final QName _UpdateIncidents_QNAME = new QName("http://www.mir3.com/ws", "updateIncidents");
/*  123 */   private static final QName _AddNewIncidents_QNAME = new QName("http://www.mir3.com/ws", "addNewIncidents");
/*  124 */   private static final QName _DynamicRecipients2TypeRecipientGroupTitle_QNAME = new QName("http://www.mir3.com/ws", "recipientGroupTitle");
/*  125 */   private static final QName _DynamicRecipients2TypeRecipientDetail_QNAME = new QName("http://www.mir3.com/ws", "recipientDetail");
/*  126 */   private static final QName _DynamicRecipients2TypeRecipient_QNAME = new QName("http://www.mir3.com/ws", "recipient");
/*  127 */   private static final QName _DynamicRecipients2TypeRecipientScheduleTitle_QNAME = new QName("http://www.mir3.com/ws", "recipientScheduleTitle");
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public InitiateNotificationsResponseType createInitiateNotificationsResponseType()
/*      */   {
/*  141 */     return new InitiateNotificationsResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public DynamicConditionManySearchType createDynamicConditionManySearchType()
/*      */   {
/*  149 */     return new DynamicConditionManySearchType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public IncidentStatusType createIncidentStatusType()
/*      */   {
/*  157 */     return new IncidentStatusType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public SearchNotificationGroupsResponseType createSearchNotificationGroupsResponseType()
/*      */   {
/*  165 */     return new SearchNotificationGroupsResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public UpdateCategoryType createUpdateCategoryType()
/*      */   {
/*  173 */     return new UpdateCategoryType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public SubscriptionsType createSubscriptionsType()
/*      */   {
/*  181 */     return new SubscriptionsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetBlockedStatusType createGetBlockedStatusType()
/*      */   {
/*  189 */     return new GetBlockedStatusType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public UpdateNotificationGroupsResponseType createUpdateNotificationGroupsResponseType()
/*      */   {
/*  197 */     return new UpdateNotificationGroupsResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public IncidentManySearchType createIncidentManySearchType()
/*      */   {
/*  205 */     return new IncidentManySearchType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public EmailAttachmentType createEmailAttachmentType()
/*      */   {
/*  213 */     return new EmailAttachmentType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public TerminateNotificationsResponseType createTerminateNotificationsResponseType()
/*      */   {
/*  221 */     return new TerminateNotificationsResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public DeleteCategoryType createDeleteCategoryType()
/*      */   {
/*  229 */     return new DeleteCategoryType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public AlternatesType createAlternatesType()
/*      */   {
/*  237 */     return new AlternatesType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public SetVerbiageType createSetVerbiageType()
/*      */   {
/*  245 */     return new SetVerbiageType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ReportCallbackEventType createReportCallbackEventType()
/*      */   {
/*  253 */     return new ReportCallbackEventType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public RemoveRecipientsFromGroupResponseType createRemoveRecipientsFromGroupResponseType()
/*      */   {
/*  261 */     return new RemoveRecipientsFromGroupResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ParamType createParamType()
/*      */   {
/*  269 */     return new ParamType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public TdxWrapperType createTdxWrapperType()
/*      */   {
/*  277 */     return new TdxWrapperType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ReportRecipientsQueryType createReportRecipientsQueryType()
/*      */   {
/*  285 */     return new ReportRecipientsQueryType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public BroadcastInfoType createBroadcastInfoType()
/*      */   {
/*  293 */     return new BroadcastInfoType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GroupReportSummaryType createGroupReportSummaryType()
/*      */   {
/*  301 */     return new GroupReportSummaryType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public DeleteIncidentsResponseType createDeleteIncidentsResponseType()
/*      */   {
/*  309 */     return new DeleteIncidentsResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public StringWithMessageTypeAndLocale createStringWithMessageTypeAndLocale()
/*      */   {
/*  317 */     return new StringWithMessageTypeAndLocale();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public IntervalRepeatForScheduleType createIntervalRepeatForScheduleType()
/*      */   {
/*  325 */     return new IntervalRepeatForScheduleType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public RecipientOneSearchType createRecipientOneSearchType()
/*      */   {
/*  333 */     return new RecipientOneSearchType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public NotificationEscalationsType createNotificationEscalationsType()
/*      */   {
/*  341 */     return new NotificationEscalationsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetDisabledDevicesResponseType createGetDisabledDevicesResponseType()
/*      */   {
/*  349 */     return new GetDisabledDevicesResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public SearchVoiceFilesType createSearchVoiceFilesType()
/*      */   {
/*  357 */     return new SearchVoiceFilesType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public SearchRecipientsResponseType createSearchRecipientsResponseType()
/*      */   {
/*  365 */     return new SearchRecipientsResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetProviderInfoResponseType createGetProviderInfoResponseType()
/*      */   {
/*  373 */     return new GetProviderInfoResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetDisabledDevicesType createGetDisabledDevicesType()
/*      */   {
/*  381 */     return new GetDisabledDevicesType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public RemoveRecipientsFromGroupType createRemoveRecipientsFromGroupType()
/*      */   {
/*  389 */     return new RemoveRecipientsFromGroupType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetCustomVerbiageTypeByDivisionResponseType createGetCustomVerbiageTypeByDivisionResponseType()
/*      */   {
/*  397 */     return new GetCustomVerbiageTypeByDivisionResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public SearchRecipientGroupsResponseType createSearchRecipientGroupsResponseType()
/*      */   {
/*  405 */     return new SearchRecipientGroupsResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public DeleteDivisionsType createDeleteDivisionsType()
/*      */   {
/*  413 */     return new DeleteDivisionsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetDivisionsResponseType createGetDivisionsResponseType()
/*      */   {
/*  421 */     return new GetDivisionsResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetCustomVerbiageTypeDivisionType createGetCustomVerbiageTypeDivisionType()
/*      */   {
/*  429 */     return new GetCustomVerbiageTypeDivisionType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetPortUtilizationResponseType createGetPortUtilizationResponseType()
/*      */   {
/*  437 */     return new GetPortUtilizationResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public VerbiageInfoType createVerbiageInfoType()
/*      */   {
/*  445 */     return new VerbiageInfoType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public DevicePriorityType createDevicePriorityType()
/*      */   {
/*  453 */     return new DevicePriorityType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ProviderType createProviderType()
/*      */   {
/*  461 */     return new ProviderType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetDynamicGroupMembersType createGetDynamicGroupMembersType()
/*      */   {
/*  469 */     return new GetDynamicGroupMembersType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public PortType createPortType()
/*      */   {
/*  477 */     return new PortType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public IntervalRepeatType createIntervalRepeatType()
/*      */   {
/*  485 */     return new IntervalRepeatType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public VerbiagePerMessageType createVerbiagePerMessageType()
/*      */   {
/*  493 */     return new VerbiagePerMessageType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public HideReportsResponseType createHideReportsResponseType()
/*      */   {
/*  501 */     return new HideReportsResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public UpdateRecipientSchedulesType createUpdateRecipientSchedulesType()
/*      */   {
/*  509 */     return new UpdateRecipientSchedulesType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public DeleteRecipientGroupsResponseType createDeleteRecipientGroupsResponseType()
/*      */   {
/*  517 */     return new DeleteRecipientGroupsResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public DayRepeatType createDayRepeatType()
/*      */   {
/*  525 */     return new DayRepeatType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ExecuteCustomReportType createExecuteCustomReportType()
/*      */   {
/*  533 */     return new ExecuteCustomReportType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetProviderInfoType createGetProviderInfoType()
/*      */   {
/*  541 */     return new GetProviderInfoType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public CategoryType createCategoryType()
/*      */   {
/*  549 */     return new CategoryType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public WrappedRequestType createWrappedRequestType()
/*      */   {
/*  557 */     return new WrappedRequestType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public RecipientType createRecipientType()
/*      */   {
/*  565 */     return new RecipientType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public DeleteSeverityType createDeleteSeverityType()
/*      */   {
/*  573 */     return new DeleteSeverityType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public DateRangeType createDateRangeType()
/*      */   {
/*  581 */     return new DateRangeType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public RecipientGroupMembersType createRecipientGroupMembersType()
/*      */   {
/*  589 */     return new RecipientGroupMembersType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public AddNewRecipientSchedulesResponseType createAddNewRecipientSchedulesResponseType()
/*      */   {
/*  597 */     return new AddNewRecipientSchedulesResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public RuleType createRuleType()
/*      */   {
/*  605 */     return new RuleType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public AddPriorityType createAddPriorityType()
/*      */   {
/*  613 */     return new AddPriorityType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ReportDeviceContactsType createReportDeviceContactsType()
/*      */   {
/*  621 */     return new ReportDeviceContactsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ReportResponseOptionType createReportResponseOptionType()
/*      */   {
/*  629 */     return new ReportResponseOptionType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public OneStepNotificationResponseType createOneStepNotificationResponseType()
/*      */   {
/*  637 */     return new OneStepNotificationResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public AddNewRecipientsType createAddNewRecipientsType()
/*      */   {
/*  645 */     return new AddNewRecipientsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ChooseNotificationResponseOptionResponseType createChooseNotificationResponseOptionResponseType()
/*      */   {
/*  653 */     return new ChooseNotificationResponseOptionResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public PermissionsCheckType createPermissionsCheckType()
/*      */   {
/*  661 */     return new PermissionsCheckType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public VoiceFileType createVoiceFileType()
/*      */   {
/*  669 */     return new VoiceFileType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ChooseNotificationResponseOptionRequestType createChooseNotificationResponseOptionRequestType()
/*      */   {
/*  677 */     return new ChooseNotificationResponseOptionRequestType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public SubscriptionType createSubscriptionType()
/*      */   {
/*  685 */     return new SubscriptionType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public PrioritiesType createPrioritiesType()
/*      */   {
/*  693 */     return new PrioritiesType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public UpdateNotificationGroupsType createUpdateNotificationGroupsType()
/*      */   {
/*  701 */     return new UpdateNotificationGroupsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public AddressesType createAddressesType()
/*      */   {
/*  709 */     return new AddressesType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public VerbiageParamType createVerbiageParamType()
/*      */   {
/*  717 */     return new VerbiageParamType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public DeviceType createDeviceType()
/*      */   {
/*  725 */     return new DeviceType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public UpdateRecipientGroupsResponseType createUpdateRecipientGroupsResponseType()
/*      */   {
/*  733 */     return new UpdateRecipientGroupsResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public DisableDevicesResponseType createDisableDevicesResponseType()
/*      */   {
/*  741 */     return new DisableDevicesResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ExecuteCustomReportResponseType createExecuteCustomReportResponseType()
/*      */   {
/*  749 */     return new ExecuteCustomReportResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public AddNewNotificationsType createAddNewNotificationsType()
/*      */   {
/*  757 */     return new AddNewNotificationsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public OneStepAttachmentDetailType createOneStepAttachmentDetailType()
/*      */   {
/*  765 */     return new OneStepAttachmentDetailType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public CategoriesType createCategoriesType()
/*      */   {
/*  773 */     return new CategoriesType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetLoggedInUserType createGetLoggedInUserType()
/*      */   {
/*  781 */     return new GetLoggedInUserType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public PermCheckType createPermCheckType()
/*      */   {
/*  789 */     return new PermCheckType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public DeletePriorityType createDeletePriorityType()
/*      */   {
/*  797 */     return new DeletePriorityType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public DownloadVoiceFileType createDownloadVoiceFileType()
/*      */   {
/*  805 */     return new DownloadVoiceFileType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ReportCascadeType createReportCascadeType()
/*      */   {
/*  813 */     return new ReportCascadeType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public TextInputPlaceholderType createTextInputPlaceholderType()
/*      */   {
/*  821 */     return new TextInputPlaceholderType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public CalendarRepeatForScheduleType createCalendarRepeatForScheduleType()
/*      */   {
/*  829 */     return new CalendarRepeatForScheduleType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public NotificationGroupOneSearchType createNotificationGroupOneSearchType()
/*      */   {
/*  837 */     return new NotificationGroupOneSearchType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ErrorType createErrorType()
/*      */   {
/*  845 */     return new ErrorType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetRecipientGroupsType createGetRecipientGroupsType()
/*      */   {
/*  853 */     return new GetRecipientGroupsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetNotificationReportRecipientsType createGetNotificationReportRecipientsType()
/*      */   {
/*  861 */     return new GetNotificationReportRecipientsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public SetNotificationScheduleType createSetNotificationScheduleType()
/*      */   {
/*  869 */     return new SetNotificationScheduleType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public CalloutInfoType createCalloutInfoType()
/*      */   {
/*  877 */     return new CalloutInfoType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public UpdateDivisionType createUpdateDivisionType()
/*      */   {
/*  885 */     return new UpdateDivisionType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public UploadVoiceFileType createUploadVoiceFileType()
/*      */   {
/*  893 */     return new UploadVoiceFileType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public VerbiageType createVerbiageType()
/*      */   {
/*  901 */     return new VerbiageType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ResponseOptionType createResponseOptionType()
/*      */   {
/*  909 */     return new ResponseOptionType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public VerbiageOverrideType createVerbiageOverrideType()
/*      */   {
/*  917 */     return new VerbiageOverrideType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public SetVerbiageResponseType createSetVerbiageResponseType()
/*      */   {
/*  925 */     return new SetVerbiageResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public UpdateRecipientSchedulesResponseType createUpdateRecipientSchedulesResponseType()
/*      */   {
/*  933 */     return new UpdateRecipientSchedulesResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public StatusChangeResponseType createStatusChangeResponseType()
/*      */   {
/*  941 */     return new StatusChangeResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public DeleteVoiceFilesResponseType createDeleteVoiceFilesResponseType()
/*      */   {
/*  949 */     return new DeleteVoiceFilesResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public SearchVoiceFilesResponseType createSearchVoiceFilesResponseType()
/*      */   {
/*  957 */     return new SearchVoiceFilesResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public IdConvertResponseType createIdConvertResponseType()
/*      */   {
/*  965 */     return new IdConvertResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public NotificationDetailType createNotificationDetailType()
/*      */   {
/*  973 */     return new NotificationDetailType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetBlockedStatusResponseType createGetBlockedStatusResponseType()
/*      */   {
/*  981 */     return new GetBlockedStatusResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public UpdateDynamicGroupsResponseType createUpdateDynamicGroupsResponseType()
/*      */   {
/*  989 */     return new UpdateDynamicGroupsResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public InitiateNotificationGroupsResponseType createInitiateNotificationGroupsResponseType()
/*      */   {
/*  997 */     return new InitiateNotificationGroupsResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public SearchReportsResponseType createSearchReportsResponseType()
/*      */   {
/* 1005 */     return new SearchReportsResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public RecipientScheduleDetailType createRecipientScheduleDetailType()
/*      */   {
/* 1013 */     return new RecipientScheduleDetailType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ReportCallbacksType createReportCallbacksType()
/*      */   {
/* 1021 */     return new ReportCallbacksType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ReportFollowUpType createReportFollowUpType()
/*      */   {
/* 1029 */     return new ReportFollowUpType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public AllowedDeviceStatusType createAllowedDeviceStatusType()
/*      */   {
/* 1037 */     return new AllowedDeviceStatusType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public AddSeverityType createAddSeverityType()
/*      */   {
/* 1045 */     return new AddSeverityType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public DeleteDivisionsResponseType createDeleteDivisionsResponseType()
/*      */   {
/* 1053 */     return new DeleteDivisionsResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public PlaceholdersReportType createPlaceholdersReportType()
/*      */   {
/* 1061 */     return new PlaceholdersReportType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public AddNewIncidentsType createAddNewIncidentsType()
/*      */   {
/* 1069 */     return new AddNewIncidentsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public RoleResponseType createRoleResponseType()
/*      */   {
/* 1077 */     return new RoleResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public LocationScheduleDetailsType createLocationScheduleDetailsType()
/*      */   {
/* 1085 */     return new LocationScheduleDetailsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public NotificationResponseOptionsType createNotificationResponseOptionsType()
/*      */   {
/* 1093 */     return new NotificationResponseOptionsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public IncidentHistoryType createIncidentHistoryType()
/*      */   {
/* 1101 */     return new IncidentHistoryType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public DeleteRecipientGroupsType createDeleteRecipientGroupsType()
/*      */   {
/* 1109 */     return new DeleteRecipientGroupsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public RecipientSchedulesOneSearchType createRecipientSchedulesOneSearchType()
/*      */   {
/* 1117 */     return new RecipientSchedulesOneSearchType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public AuthorizationType createAuthorizationType()
/*      */   {
/* 1125 */     return new AuthorizationType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetLoggedInUserResponseType createGetLoggedInUserResponseType()
/*      */   {
/* 1133 */     return new GetLoggedInUserResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public TimeConstraintType createTimeConstraintType()
/*      */   {
/* 1141 */     return new TimeConstraintType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public FirstResponseInfoType createFirstResponseInfoType()
/*      */   {
/* 1149 */     return new FirstResponseInfoType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public AddNewNotificationsResponseType createAddNewNotificationsResponseType()
/*      */   {
/* 1157 */     return new AddNewNotificationsResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public IncidentDetailType createIncidentDetailType()
/*      */   {
/* 1165 */     return new IncidentDetailType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public PermissionsCheckResponseType createPermissionsCheckResponseType()
/*      */   {
/* 1173 */     return new PermissionsCheckResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ChosenResponseType createChosenResponseType()
/*      */   {
/* 1181 */     return new ChosenResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public BlockedDeviceStatusType createBlockedDeviceStatusType()
/*      */   {
/* 1189 */     return new BlockedDeviceStatusType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public UUIDToIntType createUUIDToIntType()
/*      */   {
/* 1197 */     return new UUIDToIntType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public BaseReportType createBaseReportType()
/*      */   {
/* 1205 */     return new BaseReportType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public NotificationGroupsInitiationType createNotificationGroupsInitiationType()
/*      */   {
/* 1213 */     return new NotificationGroupsInitiationType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public UserDeviceDetailType createUserDeviceDetailType()
/*      */   {
/* 1221 */     return new UserDeviceDetailType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public AddNewIncidentsResponseType createAddNewIncidentsResponseType()
/*      */   {
/* 1229 */     return new AddNewIncidentsResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public BlockedDeviceStatusResponseType createBlockedDeviceStatusResponseType()
/*      */   {
/* 1237 */     return new BlockedDeviceStatusResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetPhonePrefixesType createGetPhonePrefixesType()
/*      */   {
/* 1245 */     return new GetPhonePrefixesType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetDynamicGroupMembersResponseType createGetDynamicGroupMembersResponseType()
/*      */   {
/* 1253 */     return new GetDynamicGroupMembersResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public NotificationReportType createNotificationReportType()
/*      */   {
/* 1261 */     return new NotificationReportType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public SearchNotificationsType createSearchNotificationsType()
/*      */   {
/* 1269 */     return new SearchNotificationsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public SearchRecipientSchedulesType createSearchRecipientSchedulesType()
/*      */   {
/* 1277 */     return new SearchRecipientSchedulesType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetNotificationScheduleResponseType createGetNotificationScheduleResponseType()
/*      */   {
/* 1285 */     return new GetNotificationScheduleResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetNotificationScheduleType createGetNotificationScheduleType()
/*      */   {
/* 1293 */     return new GetNotificationScheduleType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public DeleteRecipientSchedulesResponseType createDeleteRecipientSchedulesResponseType()
/*      */   {
/* 1301 */     return new DeleteRecipientSchedulesResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public LocationScheduleType createLocationScheduleType()
/*      */   {
/* 1309 */     return new LocationScheduleType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public BlackoutTimeType createBlackoutTimeType()
/*      */   {
/* 1317 */     return new BlackoutTimeType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetAllowedSMSListType createGetAllowedSMSListType()
/*      */   {
/* 1325 */     return new GetAllowedSMSListType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public UpdateRecipientsResponseType createUpdateRecipientsResponseType()
/*      */   {
/* 1333 */     return new UpdateRecipientsResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public UpdateIncidentsResponseType createUpdateIncidentsResponseType()
/*      */   {
/* 1341 */     return new UpdateIncidentsResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public AddNewDivisionsType createAddNewDivisionsType()
/*      */   {
/* 1349 */     return new AddNewDivisionsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public SearchNotificationGroupsType createSearchNotificationGroupsType()
/*      */   {
/* 1357 */     return new SearchNotificationGroupsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public PlaceholderValuesType createPlaceholderValuesType()
/*      */   {
/* 1365 */     return new PlaceholderValuesType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public PlaceholderOptionType createPlaceholderOptionType()
/*      */   {
/* 1373 */     return new PlaceholderOptionType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public UpdateOneNotificationType createUpdateOneNotificationType()
/*      */   {
/* 1381 */     return new UpdateOneNotificationType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public NotificationGroupManySearchType createNotificationGroupManySearchType()
/*      */   {
/* 1389 */     return new NotificationGroupManySearchType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public FollowUpType createFollowUpType()
/*      */   {
/* 1397 */     return new FollowUpType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public OneStepInfoType createOneStepInfoType()
/*      */   {
/* 1405 */     return new OneStepInfoType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public SetAllowedStatusType createSetAllowedStatusType()
/*      */   {
/* 1413 */     return new SetAllowedStatusType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public DayRepeatForScheduleType createDayRepeatForScheduleType()
/*      */   {
/* 1421 */     return new DayRepeatForScheduleType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public SuppressedSubscriptionType createSuppressedSubscriptionType()
/*      */   {
/* 1429 */     return new SuppressedSubscriptionType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public CustomVerbiageType createCustomVerbiageType()
/*      */   {
/* 1437 */     return new CustomVerbiageType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public PhoneType createPhoneType()
/*      */   {
/* 1445 */     return new PhoneType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public UpdateOneIncidentType createUpdateOneIncidentType()
/*      */   {
/* 1453 */     return new UpdateOneIncidentType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public DesktopAlertInfosType createDesktopAlertInfosType()
/*      */   {
/* 1461 */     return new DesktopAlertInfosType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ResponseChoiceType createResponseChoiceType()
/*      */   {
/* 1469 */     return new ResponseChoiceType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public DivisionDetailType createDivisionDetailType()
/*      */   {
/* 1477 */     return new DivisionDetailType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public OneRecordType createOneRecordType()
/*      */   {
/* 1485 */     return new OneRecordType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public Device createDevice()
/*      */   {
/* 1493 */     return new Device();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ReportSummaryType createReportSummaryType()
/*      */   {
/* 1501 */     return new ReportSummaryType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public StringWithLocale createStringWithLocale()
/*      */   {
/* 1509 */     return new StringWithLocale();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetNotificationGroupReportType createGetNotificationGroupReportType()
/*      */   {
/* 1517 */     return new GetNotificationGroupReportType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public UpdateOneRecipientGroupType createUpdateOneRecipientGroupType()
/*      */   {
/* 1525 */     return new UpdateOneRecipientGroupType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public LocalePreferenceType createLocalePreferenceType()
/*      */   {
/* 1533 */     return new LocalePreferenceType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public RecipientShiftType createRecipientShiftType()
/*      */   {
/* 1541 */     return new RecipientShiftType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public AddressGeocodeType createAddressGeocodeType()
/*      */   {
/* 1549 */     return new AddressGeocodeType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ChangeIncidentsStatusType createChangeIncidentsStatusType()
/*      */   {
/* 1557 */     return new ChangeIncidentsStatusType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public SearchReportsType createSearchReportsType()
/*      */   {
/* 1565 */     return new SearchReportsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public EscalationType createEscalationType()
/*      */   {
/* 1573 */     return new EscalationType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetEmailAttachmentType createGetEmailAttachmentType()
/*      */   {
/* 1581 */     return new GetEmailAttachmentType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public SearchRecipientsType createSearchRecipientsType()
/*      */   {
/* 1589 */     return new SearchRecipientsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public NotificationManySearchType createNotificationManySearchType()
/*      */   {
/* 1597 */     return new NotificationManySearchType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public UpdateNotificationsResponseType createUpdateNotificationsResponseType()
/*      */   {
/* 1605 */     return new UpdateNotificationsResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public TerminateNotificationGroupsType createTerminateNotificationGroupsType()
/*      */   {
/* 1613 */     return new TerminateNotificationGroupsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public LocationStatusesType createLocationStatusesType()
/*      */   {
/* 1621 */     return new LocationStatusesType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public RecipientManySearchType createRecipientManySearchType()
/*      */   {
/* 1629 */     return new RecipientManySearchType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public DeleteIncidentsType createDeleteIncidentsType()
/*      */   {
/* 1637 */     return new DeleteIncidentsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public InitiationResponseType createInitiationResponseType()
/*      */   {
/* 1645 */     return new InitiationResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public IncidentOneSearchType createIncidentOneSearchType()
/*      */   {
/* 1653 */     return new IncidentOneSearchType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public LocationStatus createLocationStatus()
/*      */   {
/* 1661 */     return new LocationStatus();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public DeleteDynamicGroupsResponseType createDeleteDynamicGroupsResponseType()
/*      */   {
/* 1669 */     return new DeleteDynamicGroupsResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public AddressType createAddressType()
/*      */   {
/* 1677 */     return new AddressType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ErrorsType createErrorsType()
/*      */   {
/* 1685 */     return new ErrorsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public PrepareToRecordMessagesType createPrepareToRecordMessagesType()
/*      */   {
/* 1693 */     return new PrepareToRecordMessagesType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public SetPhonePrefixesType createSetPhonePrefixesType()
/*      */   {
/* 1701 */     return new SetPhonePrefixesType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public PlaceholderReportType createPlaceholderReportType()
/*      */   {
/* 1709 */     return new PlaceholderReportType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public AddNewNotificationGroupsResponseType createAddNewNotificationGroupsResponseType()
/*      */   {
/* 1717 */     return new AddNewNotificationGroupsResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public BlockedStatusType createBlockedStatusType()
/*      */   {
/* 1725 */     return new BlockedStatusType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public DownloadVoiceFileResponseType createDownloadVoiceFileResponseType()
/*      */   {
/* 1733 */     return new DownloadVoiceFileResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public UploadVoiceFileResponseType createUploadVoiceFileResponseType()
/*      */   {
/* 1741 */     return new UploadVoiceFileResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public AllowedStatusType createAllowedStatusType()
/*      */   {
/* 1749 */     return new AllowedStatusType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public UpdateIncidentsType createUpdateIncidentsType()
/*      */   {
/* 1757 */     return new UpdateIncidentsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetRecordedResponseType createGetRecordedResponseType()
/*      */   {
/* 1765 */     return new GetRecordedResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public InitiateNotificationsType createInitiateNotificationsType()
/*      */   {
/* 1773 */     return new InitiateNotificationsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public SimpleDeviceType createSimpleDeviceType()
/*      */   {
/* 1781 */     return new SimpleDeviceType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ReportHeaderType createReportHeaderType()
/*      */   {
/* 1789 */     return new ReportHeaderType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public OverrideType createOverrideType()
/*      */   {
/* 1797 */     return new OverrideType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public InitiateOneNotificationType createInitiateOneNotificationType()
/*      */   {
/* 1805 */     return new InitiateOneNotificationType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public OneTimeSendType createOneTimeSendType()
/*      */   {
/* 1813 */     return new OneTimeSendType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GroupReportMemberType createGroupReportMemberType()
/*      */   {
/* 1821 */     return new GroupReportMemberType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public PagingCarriersType createPagingCarriersType()
/*      */   {
/* 1829 */     return new PagingCarriersType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ChangeIncidentsStatusResponseType createChangeIncidentsStatusResponseType()
/*      */   {
/* 1837 */     return new ChangeIncidentsStatusResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetVerbiageResponseType createGetVerbiageResponseType()
/*      */   {
/* 1845 */     return new GetVerbiageResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public NotificationResponseInitiatorType createNotificationResponseInitiatorType()
/*      */   {
/* 1853 */     return new NotificationResponseInitiatorType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ValidateSingleSignOnTokenResponseType createValidateSingleSignOnTokenResponseType()
/*      */   {
/* 1861 */     return new ValidateSingleSignOnTokenResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public UpdateRecipientGroupsType createUpdateRecipientGroupsType()
/*      */   {
/* 1869 */     return new UpdateRecipientGroupsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public UpdateDivisionResponseType createUpdateDivisionResponseType()
/*      */   {
/* 1877 */     return new UpdateDivisionResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public AddRecipientsToGroupType createAddRecipientsToGroupType()
/*      */   {
/* 1885 */     return new AddRecipientsToGroupType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public RecipientSchedulesManySearchType createRecipientSchedulesManySearchType()
/*      */   {
/* 1893 */     return new RecipientSchedulesManySearchType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public NotificationScheduleType createNotificationScheduleType()
/*      */   {
/* 1901 */     return new NotificationScheduleType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public DeleteRecipientsType createDeleteRecipientsType()
/*      */   {
/* 1909 */     return new DeleteRecipientsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public SetPhonePrefixesResponseType createSetPhonePrefixesResponseType()
/*      */   {
/* 1917 */     return new SetPhonePrefixesResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetPagingCarriersType createGetPagingCarriersType()
/*      */   {
/* 1925 */     return new GetPagingCarriersType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public OneStepNotificationType createOneStepNotificationType()
/*      */   {
/* 1933 */     return new OneStepNotificationType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public SeveritiesType createSeveritiesType()
/*      */   {
/* 1941 */     return new SeveritiesType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public SetCustomVerbiageTypeDivisionType createSetCustomVerbiageTypeDivisionType()
/*      */   {
/* 1949 */     return new SetCustomVerbiageTypeDivisionType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ReportGeneralStatistics createReportGeneralStatistics()
/*      */   {
/* 1957 */     return new ReportGeneralStatistics();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public InitiateNotificationGroupsType createInitiateNotificationGroupsType()
/*      */   {
/* 1965 */     return new InitiateNotificationGroupsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public UpdateOneRecipientScheduleType createUpdateOneRecipientScheduleType()
/*      */   {
/* 1973 */     return new UpdateOneRecipientScheduleType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public CreateSingleSignOnTokenType createCreateSingleSignOnTokenType()
/*      */   {
/* 1981 */     return new CreateSingleSignOnTokenType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public PagingCarrierType createPagingCarrierType()
/*      */   {
/* 1989 */     return new PagingCarrierType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public OneStepAttachmentType createOneStepAttachmentType()
/*      */   {
/* 1997 */     return new OneStepAttachmentType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public UpdateOneRecipientType createUpdateOneRecipientType()
/*      */   {
/* 2005 */     return new UpdateOneRecipientType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public UpdatePriorityType createUpdatePriorityType()
/*      */   {
/* 2013 */     return new UpdatePriorityType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public DevicesType createDevicesType()
/*      */   {
/* 2021 */     return new DevicesType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public EmailAndFaxType createEmailAndFaxType()
/*      */   {
/* 2029 */     return new EmailAndFaxType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public DeleteVoiceFilesType createDeleteVoiceFilesType()
/*      */   {
/* 2037 */     return new DeleteVoiceFilesType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public SetEmailAttachmentType createSetEmailAttachmentType()
/*      */   {
/* 2045 */     return new SetEmailAttachmentType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetAvailableTopicsType createGetAvailableTopicsType()
/*      */   {
/* 2053 */     return new GetAvailableTopicsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetEmailAttachmentResponseType createGetEmailAttachmentResponseType()
/*      */   {
/* 2061 */     return new GetEmailAttachmentResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public IntToUUIDType createIntToUUIDType()
/*      */   {
/* 2069 */     return new IntToUUIDType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public RecipientDetailType createRecipientDetailType()
/*      */   {
/* 2077 */     return new RecipientDetailType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ReportContactType createReportContactType()
/*      */   {
/* 2085 */     return new ReportContactType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public RecipientGroupOneSearchType createRecipientGroupOneSearchType()
/*      */   {
/* 2093 */     return new RecipientGroupOneSearchType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public InitiateOneNotificationGroupType createInitiateOneNotificationGroupType()
/*      */   {
/* 2101 */     return new InitiateOneNotificationGroupType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ReportManySearchType createReportManySearchType()
/*      */   {
/* 2109 */     return new ReportManySearchType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public SearchNotificationsResponseType createSearchNotificationsResponseType()
/*      */   {
/* 2117 */     return new SearchNotificationsResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ReportFollowUpChoiceType createReportFollowUpChoiceType()
/*      */   {
/* 2125 */     return new ReportFollowUpChoiceType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public DesktopInfo createDesktopInfo()
/*      */   {
/* 2133 */     return new DesktopInfo();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public CustomFieldType createCustomFieldType()
/*      */   {
/* 2141 */     return new CustomFieldType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public NotificationReportResponseDetailType createNotificationReportResponseDetailType()
/*      */   {
/* 2149 */     return new NotificationReportResponseDetailType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetPhonePrefixesResponseType createGetPhonePrefixesResponseType()
/*      */   {
/* 2157 */     return new GetPhonePrefixesResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public CalendarRepeatType createCalendarRepeatType()
/*      */   {
/* 2165 */     return new CalendarRepeatType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public AddNewRecipientGroupsType createAddNewRecipientGroupsType()
/*      */   {
/* 2173 */     return new AddNewRecipientGroupsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetNotificationReportsResponseType createGetNotificationReportsResponseType()
/*      */   {
/* 2181 */     return new GetNotificationReportsResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public AllowedDeviceStatusResponseType createAllowedDeviceStatusResponseType()
/*      */   {
/* 2189 */     return new AllowedDeviceStatusResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public DeleteRecipientsResponseType createDeleteRecipientsResponseType()
/*      */   {
/* 2197 */     return new DeleteRecipientsResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public SearchReportsResultType createSearchReportsResultType()
/*      */   {
/* 2205 */     return new SearchReportsResultType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public PreferencesType createPreferencesType()
/*      */   {
/* 2213 */     return new PreferencesType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ReportFollowUpsType createReportFollowUpsType()
/*      */   {
/* 2221 */     return new ReportFollowUpsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ReportCallbackEndpointType createReportCallbackEndpointType()
/*      */   {
/* 2229 */     return new ReportCallbackEndpointType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public DivisionPermissionType createDivisionPermissionType()
/*      */   {
/* 2237 */     return new DivisionPermissionType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public AddCategoryType createAddCategoryType()
/*      */   {
/* 2245 */     return new AddCategoryType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public UpdateRecipientsType createUpdateRecipientsType()
/*      */   {
/* 2253 */     return new UpdateRecipientsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public PlaceholderValueType createPlaceholderValueType()
/*      */   {
/* 2261 */     return new PlaceholderValueType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public UpdateNotificationsType createUpdateNotificationsType()
/*      */   {
/* 2269 */     return new UpdateNotificationsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public SubcategoriesType createSubcategoriesType()
/*      */   {
/* 2277 */     return new SubcategoriesType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetDivisionsType createGetDivisionsType()
/*      */   {
/* 2285 */     return new GetDivisionsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public AddNewRecipientsResponseType createAddNewRecipientsResponseType()
/*      */   {
/* 2293 */     return new AddNewRecipientsResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetPagingCarriersResponseType createGetPagingCarriersResponseType()
/*      */   {
/* 2301 */     return new GetPagingCarriersResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public SetEmailAttachmentResponseType createSetEmailAttachmentResponseType()
/*      */   {
/* 2309 */     return new SetEmailAttachmentResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ReportDetailType createReportDetailType()
/*      */   {
/* 2317 */     return new ReportDetailType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public DynamicConditionSearchType createDynamicConditionSearchType()
/*      */   {
/* 2325 */     return new DynamicConditionSearchType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ReportResponseOptionsType createReportResponseOptionsType()
/*      */   {
/* 2333 */     return new ReportResponseOptionsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public RecordedResponseType createRecordedResponseType()
/*      */   {
/* 2341 */     return new RecordedResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetRecordedResponseResponseType createGetRecordedResponseResponseType()
/*      */   {
/* 2349 */     return new GetRecordedResponseResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public NoContent createNoContent()
/*      */   {
/* 2357 */     return new NoContent();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ReportRowType createReportRowType()
/*      */   {
/* 2365 */     return new ReportRowType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetVerbiageType createGetVerbiageType()
/*      */   {
/* 2373 */     return new GetVerbiageType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public SearchRecipientSchedulesResponseType createSearchRecipientSchedulesResponseType()
/*      */   {
/* 2381 */     return new SearchRecipientSchedulesResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ResolvedDynamicGroupDetailType createResolvedDynamicGroupDetailType()
/*      */   {
/* 2389 */     return new ResolvedDynamicGroupDetailType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public RoleTemplatesType createRoleTemplatesType()
/*      */   {
/* 2397 */     return new RoleTemplatesType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public LocationOverrideType createLocationOverrideType()
/*      */   {
/* 2405 */     return new LocationOverrideType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ReportOneSearchType createReportOneSearchType()
/*      */   {
/* 2413 */     return new ReportOneSearchType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public NotificationGroupReportResponseType createNotificationGroupReportResponseType()
/*      */   {
/* 2421 */     return new NotificationGroupReportResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ResponseType createResponseType()
/*      */   {
/* 2429 */     return new ResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public RecipientGroupManySearchType createRecipientGroupManySearchType()
/*      */   {
/* 2437 */     return new RecipientGroupManySearchType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ScheduledNotificationReportType createScheduledNotificationReportType()
/*      */   {
/* 2445 */     return new ScheduledNotificationReportType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public UserDeviceType createUserDeviceType()
/*      */   {
/* 2453 */     return new UserDeviceType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public DeleteNotificationsResponseType createDeleteNotificationsResponseType()
/*      */   {
/* 2461 */     return new DeleteNotificationsResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public DeleteNotificationGroupsResponseType createDeleteNotificationGroupsResponseType()
/*      */   {
/* 2469 */     return new DeleteNotificationGroupsResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public TerminateNotificationGroupsResponseType createTerminateNotificationGroupsResponseType()
/*      */   {
/* 2477 */     return new TerminateNotificationGroupsResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public RecipientScheduleSearchType createRecipientScheduleSearchType()
/*      */   {
/* 2485 */     return new RecipientScheduleSearchType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public AddNewNotificationGroupsType createAddNewNotificationGroupsType()
/*      */   {
/* 2493 */     return new AddNewNotificationGroupsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public LeaveMessageType createLeaveMessageType()
/*      */   {
/* 2501 */     return new LeaveMessageType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public SearchIncidentsType createSearchIncidentsType()
/*      */   {
/* 2509 */     return new SearchIncidentsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public AttachmentType createAttachmentType()
/*      */   {
/* 2517 */     return new AttachmentType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ReportStatisticsType createReportStatisticsType()
/*      */   {
/* 2525 */     return new ReportStatisticsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public NotificationOneSearchType createNotificationOneSearchType()
/*      */   {
/* 2533 */     return new NotificationOneSearchType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public VoiceFileOneSearchType createVoiceFileOneSearchType()
/*      */   {
/* 2541 */     return new VoiceFileOneSearchType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public NotificationGroupDetailType createNotificationGroupDetailType()
/*      */   {
/* 2549 */     return new NotificationGroupDetailType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public RespondType createRespondType()
/*      */   {
/* 2557 */     return new RespondType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public CheckboxPlaceholderType createCheckboxPlaceholderType()
/*      */   {
/* 2565 */     return new CheckboxPlaceholderType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public SearchIncidentsResponseType createSearchIncidentsResponseType()
/*      */   {
/* 2573 */     return new SearchIncidentsResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetRecipientRolesType createGetRecipientRolesType()
/*      */   {
/* 2581 */     return new GetRecipientRolesType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ReportRecipientType createReportRecipientType()
/*      */   {
/* 2589 */     return new ReportRecipientType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public DisabledDevicesType createDisabledDevicesType()
/*      */   {
/* 2597 */     return new DisabledDevicesType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public CustomFieldsType createCustomFieldsType()
/*      */   {
/* 2605 */     return new CustomFieldsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetAvailableTopicsResponseType createGetAvailableTopicsResponseType()
/*      */   {
/* 2613 */     return new GetAvailableTopicsResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public MultipleChoicePlaceholderType createMultipleChoicePlaceholderType()
/*      */   {
/* 2621 */     return new MultipleChoicePlaceholderType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetPortUtilizationType createGetPortUtilizationType()
/*      */   {
/* 2629 */     return new GetPortUtilizationType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public AddNewRecipientGroupsResponseType createAddNewRecipientGroupsResponseType()
/*      */   {
/* 2637 */     return new AddNewRecipientGroupsResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ValidateSingleSignOnTokenType createValidateSingleSignOnTokenType()
/*      */   {
/* 2645 */     return new ValidateSingleSignOnTokenType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public DeleteRecipientSchedulesType createDeleteRecipientSchedulesType()
/*      */   {
/* 2653 */     return new DeleteRecipientSchedulesType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetRecipientGroupsResponseType createGetRecipientGroupsResponseType()
/*      */   {
/* 2661 */     return new GetRecipientGroupsResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public AddNewDivisionsResponseType createAddNewDivisionsResponseType()
/*      */   {
/* 2669 */     return new AddNewDivisionsResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public VerbiageOverrideDetailType createVerbiageOverrideDetailType()
/*      */   {
/* 2677 */     return new VerbiageOverrideDetailType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public DynamicRecipientsType createDynamicRecipientsType()
/*      */   {
/* 2685 */     return new DynamicRecipientsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ReportRecipientsType createReportRecipientsType()
/*      */   {
/* 2693 */     return new ReportRecipientsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public IncidentWithHistoryDetailType createIncidentWithHistoryDetailType()
/*      */   {
/* 2701 */     return new IncidentWithHistoryDetailType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public CatSubcatType createCatSubcatType()
/*      */   {
/* 2709 */     return new CatSubcatType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public PlaceholderType createPlaceholderType()
/*      */   {
/* 2717 */     return new PlaceholderType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public LocationOverrideDevicesType createLocationOverrideDevicesType()
/*      */   {
/* 2725 */     return new LocationOverrideDevicesType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public PlaceholdersType createPlaceholdersType()
/*      */   {
/* 2733 */     return new PlaceholdersType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public SetCustomVerbiageTypeByDivisionResponseType createSetCustomVerbiageTypeByDivisionResponseType()
/*      */   {
/* 2741 */     return new SetCustomVerbiageTypeByDivisionResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public UpdateOneNotificationGroupType createUpdateOneNotificationGroupType()
/*      */   {
/* 2749 */     return new UpdateOneNotificationGroupType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetBlockedSMSListType createGetBlockedSMSListType()
/*      */   {
/* 2757 */     return new GetBlockedSMSListType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public AttachmentSummaryType createAttachmentSummaryType()
/*      */   {
/* 2765 */     return new AttachmentSummaryType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public DeleteNotificationGroupsType createDeleteNotificationGroupsType()
/*      */   {
/* 2773 */     return new DeleteNotificationGroupsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public AddRecipientsToGroupResponseType createAddRecipientsToGroupResponseType()
/*      */   {
/* 2781 */     return new AddRecipientsToGroupResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public DeleteNotificationsType createDeleteNotificationsType()
/*      */   {
/* 2789 */     return new DeleteNotificationsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public SetAllowedStatusResponseType createSetAllowedStatusResponseType()
/*      */   {
/* 2797 */     return new SetAllowedStatusResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetRecipientScheduleDetailType createGetRecipientScheduleDetailType()
/*      */   {
/* 2805 */     return new GetRecipientScheduleDetailType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public HideReportsType createHideReportsType()
/*      */   {
/* 2813 */     return new HideReportsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public DynamicRecipients2Type createDynamicRecipients2Type()
/*      */   {
/* 2821 */     return new DynamicRecipients2Type();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public AttachmentSummariesType createAttachmentSummariesType()
/*      */   {
/* 2829 */     return new AttachmentSummariesType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public VoiceFileManySearchType createVoiceFileManySearchType()
/*      */   {
/* 2837 */     return new VoiceFileManySearchType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public AddNewRecipientSchedulesType createAddNewRecipientSchedulesType()
/*      */   {
/* 2845 */     return new AddNewRecipientSchedulesType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public AddNewDynamicGroupsResponseType createAddNewDynamicGroupsResponseType()
/*      */   {
/* 2853 */     return new AddNewDynamicGroupsResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public PhonePrefixType createPhonePrefixType()
/*      */   {
/* 2861 */     return new PhonePrefixType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public SuppressedSubscriptionsType createSuppressedSubscriptionsType()
/*      */   {
/* 2869 */     return new SuppressedSubscriptionsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public TerminateNotificationsType createTerminateNotificationsType()
/*      */   {
/* 2877 */     return new TerminateNotificationsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetRecipientRolesResponseType createGetRecipientRolesResponseType()
/*      */   {
/* 2885 */     return new GetRecipientRolesResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public DynamicQueryType createDynamicQueryType()
/*      */   {
/* 2893 */     return new DynamicQueryType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public PrepareToRecordMessagesResponseType createPrepareToRecordMessagesResponseType()
/*      */   {
/* 2901 */     return new PrepareToRecordMessagesResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public RecipientGroupDetailType createRecipientGroupDetailType()
/*      */   {
/* 2909 */     return new RecipientGroupDetailType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public TopicType createTopicType()
/*      */   {
/* 2917 */     return new TopicType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public UpdateSeverityType createUpdateSeverityType()
/*      */   {
/* 2925 */     return new UpdateSeverityType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetBlockedSMSListResponseType createGetBlockedSMSListResponseType()
/*      */   {
/* 2933 */     return new GetBlockedSMSListResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public CreateSingleSignOnTokenResponseType createCreateSingleSignOnTokenResponseType()
/*      */   {
/* 2941 */     return new CreateSingleSignOnTokenResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public PagerType createPagerType()
/*      */   {
/* 2949 */     return new PagerType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public LocationStatusType createLocationStatusType()
/*      */   {
/* 2957 */     return new LocationStatusType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ReportRecipientsResponseType createReportRecipientsResponseType()
/*      */   {
/* 2965 */     return new ReportRecipientsResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public IdConvertType createIdConvertType()
/*      */   {
/* 2973 */     return new IdConvertType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetNotificationReportsType createGetNotificationReportsType()
/*      */   {
/* 2981 */     return new GetNotificationReportsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetAllowedSMSListResponseType createGetAllowedSMSListResponseType()
/*      */   {
/* 2989 */     return new GetAllowedSMSListResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public RespondedType createRespondedType()
/*      */   {
/* 2997 */     return new RespondedType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public SearchRecipientGroupsType createSearchRecipientGroupsType()
/*      */   {
/* 3005 */     return new SearchRecipientGroupsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public NotificationRecipientsDetailsType createNotificationRecipientsDetailsType()
/*      */   {
/* 3013 */     return new NotificationRecipientsDetailsType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public RoleTemplateType createRoleTemplateType()
/*      */   {
/* 3021 */     return new RoleTemplateType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public ReportChosenResponseType createReportChosenResponseType()
/*      */   {
/* 3029 */     return new ReportChosenResponseType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public DisableDevicesType createDisableDevicesType()
/*      */   {
/* 3037 */     return new DisableDevicesType();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="searchRecipientGroups")
/*      */   public JAXBElement<SearchRecipientGroupsType> createSearchRecipientGroups(SearchRecipientGroupsType value)
/*      */   {
/* 3046 */     return new JAXBElement(_SearchRecipientGroups_QNAME, SearchRecipientGroupsType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="updateNotifications")
/*      */   public JAXBElement<UpdateNotificationsType> createUpdateNotifications(UpdateNotificationsType value)
/*      */   {
/* 3055 */     return new JAXBElement(_UpdateNotifications_QNAME, UpdateNotificationsType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="getProviderInfo")
/*      */   public JAXBElement<GetProviderInfoType> createGetProviderInfo(GetProviderInfoType value)
/*      */   {
/* 3064 */     return new JAXBElement(_GetProviderInfo_QNAME, GetProviderInfoType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="uploadVoiceFileResponse")
/*      */   public JAXBElement<UploadVoiceFileResponseType> createUploadVoiceFileResponse(UploadVoiceFileResponseType value)
/*      */   {
/* 3073 */     return new JAXBElement(_UploadVoiceFileResponse_QNAME, UploadVoiceFileResponseType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="getRecipientRoles")
/*      */   public JAXBElement<GetRecipientRolesType> createGetRecipientRoles(GetRecipientRolesType value)
/*      */   {
/* 3082 */     return new JAXBElement(_GetRecipientRoles_QNAME, GetRecipientRolesType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="setCustomVerbiageTypeByDivision")
/*      */   public JAXBElement<SetCustomVerbiageTypeDivisionType> createSetCustomVerbiageTypeByDivision(SetCustomVerbiageTypeDivisionType value)
/*      */   {
/* 3091 */     return new JAXBElement(_SetCustomVerbiageTypeByDivision_QNAME, SetCustomVerbiageTypeDivisionType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="addNewDivisions")
/*      */   public JAXBElement<AddNewDivisionsType> createAddNewDivisions(AddNewDivisionsType value)
/*      */   {
/* 3100 */     return new JAXBElement(_AddNewDivisions_QNAME, AddNewDivisionsType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="terminateNotificationGroups")
/*      */   public JAXBElement<TerminateNotificationGroupsType> createTerminateNotificationGroups(TerminateNotificationGroupsType value)
/*      */   {
/* 3109 */     return new JAXBElement(_TerminateNotificationGroups_QNAME, TerminateNotificationGroupsType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="updateNotificationGroups")
/*      */   public JAXBElement<UpdateNotificationGroupsType> createUpdateNotificationGroups(UpdateNotificationGroupsType value)
/*      */   {
/* 3118 */     return new JAXBElement(_UpdateNotificationGroups_QNAME, UpdateNotificationGroupsType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="changeIncidentsStatus")
/*      */   public JAXBElement<ChangeIncidentsStatusType> createChangeIncidentsStatus(ChangeIncidentsStatusType value)
/*      */   {
/* 3127 */     return new JAXBElement(_ChangeIncidentsStatus_QNAME, ChangeIncidentsStatusType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="searchNotificationGroups")
/*      */   public JAXBElement<SearchNotificationGroupsType> createSearchNotificationGroups(SearchNotificationGroupsType value)
/*      */   {
/* 3136 */     return new JAXBElement(_SearchNotificationGroups_QNAME, SearchNotificationGroupsType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="getNotificationSchedule")
/*      */   public JAXBElement<GetNotificationScheduleType> createGetNotificationSchedule(GetNotificationScheduleType value)
/*      */   {
/* 3145 */     return new JAXBElement(_GetNotificationSchedule_QNAME, GetNotificationScheduleType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="searchRecipientSchedules")
/*      */   public JAXBElement<SearchRecipientSchedulesType> createSearchRecipientSchedules(SearchRecipientSchedulesType value)
/*      */   {
/* 3154 */     return new JAXBElement(_SearchRecipientSchedules_QNAME, SearchRecipientSchedulesType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="deleteDivisions")
/*      */   public JAXBElement<DeleteDivisionsType> createDeleteDivisions(DeleteDivisionsType value)
/*      */   {
/* 3163 */     return new JAXBElement(_DeleteDivisions_QNAME, DeleteDivisionsType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="updateRecipientGroups")
/*      */   public JAXBElement<UpdateRecipientGroupsType> createUpdateRecipientGroups(UpdateRecipientGroupsType value)
/*      */   {
/* 3172 */     return new JAXBElement(_UpdateRecipientGroups_QNAME, UpdateRecipientGroupsType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="removeRecipientsFromGroup")
/*      */   public JAXBElement<RemoveRecipientsFromGroupType> createRemoveRecipientsFromGroup(RemoveRecipientsFromGroupType value)
/*      */   {
/* 3181 */     return new JAXBElement(_RemoveRecipientsFromGroup_QNAME, RemoveRecipientsFromGroupType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="deleteSeverity")
/*      */   public JAXBElement<DeleteSeverityType> createDeleteSeverity(DeleteSeverityType value)
/*      */   {
/* 3190 */     return new JAXBElement(_DeleteSeverity_QNAME, DeleteSeverityType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="getPortUtilization")
/*      */   public JAXBElement<GetPortUtilizationType> createGetPortUtilization(GetPortUtilizationType value)
/*      */   {
/* 3199 */     return new JAXBElement(_GetPortUtilization_QNAME, GetPortUtilizationType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="searchIncidents")
/*      */   public JAXBElement<SearchIncidentsType> createSearchIncidents(SearchIncidentsType value)
/*      */   {
/* 3208 */     return new JAXBElement(_SearchIncidents_QNAME, SearchIncidentsType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="deleteNotifications")
/*      */   public JAXBElement<DeleteNotificationsType> createDeleteNotifications(DeleteNotificationsType value)
/*      */   {
/* 3217 */     return new JAXBElement(_DeleteNotifications_QNAME, DeleteNotificationsType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="downloadVoiceFile")
/*      */   public JAXBElement<DownloadVoiceFileType> createDownloadVoiceFile(DownloadVoiceFileType value)
/*      */   {
/* 3226 */     return new JAXBElement(_DownloadVoiceFile_QNAME, DownloadVoiceFileType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="getLoggedInUser")
/*      */   public JAXBElement<GetLoggedInUserType> createGetLoggedInUser(GetLoggedInUserType value)
/*      */   {
/* 3235 */     return new JAXBElement(_GetLoggedInUser_QNAME, GetLoggedInUserType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="hideReportsResponse")
/*      */   public JAXBElement<HideReportsResponseType> createHideReportsResponse(HideReportsResponseType value)
/*      */   {
/* 3244 */     return new JAXBElement(_HideReportsResponse_QNAME, HideReportsResponseType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="getAvailableTopicsResponse")
/*      */   public JAXBElement<GetAvailableTopicsResponseType> createGetAvailableTopicsResponse(GetAvailableTopicsResponseType value)
/*      */   {
/* 3253 */     return new JAXBElement(_GetAvailableTopicsResponse_QNAME, GetAvailableTopicsResponseType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="updatePriority")
/*      */   public JAXBElement<UpdatePriorityType> createUpdatePriority(UpdatePriorityType value)
/*      */   {
/* 3262 */     return new JAXBElement(_UpdatePriority_QNAME, UpdatePriorityType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="deleteCategory")
/*      */   public JAXBElement<DeleteCategoryType> createDeleteCategory(DeleteCategoryType value)
/*      */   {
/* 3271 */     return new JAXBElement(_DeleteCategory_QNAME, DeleteCategoryType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="getAvailableTopics")
/*      */   public JAXBElement<GetAvailableTopicsType> createGetAvailableTopics(GetAvailableTopicsType value)
/*      */   {
/* 3280 */     return new JAXBElement(_GetAvailableTopics_QNAME, GetAvailableTopicsType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="searchRecipients")
/*      */   public JAXBElement<SearchRecipientsType> createSearchRecipients(SearchRecipientsType value)
/*      */   {
/* 3289 */     return new JAXBElement(_SearchRecipients_QNAME, SearchRecipientsType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="searchVoiceFiles")
/*      */   public JAXBElement<SearchVoiceFilesType> createSearchVoiceFiles(SearchVoiceFilesType value)
/*      */   {
/* 3298 */     return new JAXBElement(_SearchVoiceFiles_QNAME, SearchVoiceFilesType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="addRecipientsToGroup")
/*      */   public JAXBElement<AddRecipientsToGroupType> createAddRecipientsToGroup(AddRecipientsToGroupType value)
/*      */   {
/* 3307 */     return new JAXBElement(_AddRecipientsToGroup_QNAME, AddRecipientsToGroupType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="addNewRecipients")
/*      */   public JAXBElement<AddNewRecipientsType> createAddNewRecipients(AddNewRecipientsType value)
/*      */   {
/* 3316 */     return new JAXBElement(_AddNewRecipients_QNAME, AddNewRecipientsType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="updateRecipients")
/*      */   public JAXBElement<UpdateRecipientsType> createUpdateRecipients(UpdateRecipientsType value)
/*      */   {
/* 3325 */     return new JAXBElement(_UpdateRecipients_QNAME, UpdateRecipientsType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="getRecipientScheduleDetail")
/*      */   public JAXBElement<GetRecipientScheduleDetailType> createGetRecipientScheduleDetail(GetRecipientScheduleDetailType value)
/*      */   {
/* 3334 */     return new JAXBElement(_GetRecipientScheduleDetail_QNAME, GetRecipientScheduleDetailType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="permissionsCheck")
/*      */   public JAXBElement<PermissionsCheckType> createPermissionsCheck(PermissionsCheckType value)
/*      */   {
/* 3343 */     return new JAXBElement(_PermissionsCheck_QNAME, PermissionsCheckType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="disableDevices")
/*      */   public JAXBElement<DisableDevicesType> createDisableDevices(DisableDevicesType value)
/*      */   {
/* 3352 */     return new JAXBElement(_DisableDevices_QNAME, DisableDevicesType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="deleteRecipients")
/*      */   public JAXBElement<DeleteRecipientsType> createDeleteRecipients(DeleteRecipientsType value)
/*      */   {
/* 3361 */     return new JAXBElement(_DeleteRecipients_QNAME, DeleteRecipientsType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="deleteNotificationGroups")
/*      */   public JAXBElement<DeleteNotificationGroupsType> createDeleteNotificationGroups(DeleteNotificationGroupsType value)
/*      */   {
/* 3370 */     return new JAXBElement(_DeleteNotificationGroups_QNAME, DeleteNotificationGroupsType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="uploadVoiceFile")
/*      */   public JAXBElement<UploadVoiceFileType> createUploadVoiceFile(UploadVoiceFileType value)
/*      */   {
/* 3379 */     return new JAXBElement(_UploadVoiceFile_QNAME, UploadVoiceFileType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="setNotificationSchedule")
/*      */   public JAXBElement<SetNotificationScheduleType> createSetNotificationSchedule(SetNotificationScheduleType value)
/*      */   {
/* 3388 */     return new JAXBElement(_SetNotificationSchedule_QNAME, SetNotificationScheduleType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="getNotificationScheduleResponse")
/*      */   public JAXBElement<GetNotificationScheduleResponseType> createGetNotificationScheduleResponse(GetNotificationScheduleResponseType value)
/*      */   {
/* 3397 */     return new JAXBElement(_GetNotificationScheduleResponse_QNAME, GetNotificationScheduleResponseType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="updateDivision")
/*      */   public JAXBElement<UpdateDivisionType> createUpdateDivision(UpdateDivisionType value)
/*      */   {
/* 3406 */     return new JAXBElement(_UpdateDivision_QNAME, UpdateDivisionType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="chooseNotificationResponseOption")
/*      */   public JAXBElement<ChooseNotificationResponseOptionRequestType> createChooseNotificationResponseOption(ChooseNotificationResponseOptionRequestType value)
/*      */   {
/* 3415 */     return new JAXBElement(_ChooseNotificationResponseOption_QNAME, ChooseNotificationResponseOptionRequestType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="response")
/*      */   public JAXBElement<ResponseType> createResponse(ResponseType value)
/*      */   {
/* 3424 */     return new JAXBElement(_Response_QNAME, ResponseType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="searchVoiceFilesResponse")
/*      */   public JAXBElement<SearchVoiceFilesResponseType> createSearchVoiceFilesResponse(SearchVoiceFilesResponseType value)
/*      */   {
/* 3433 */     return new JAXBElement(_SearchVoiceFilesResponse_QNAME, SearchVoiceFilesResponseType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="getNotificationGroupReport")
/*      */   public JAXBElement<GetNotificationGroupReportType> createGetNotificationGroupReport(GetNotificationGroupReportType value)
/*      */   {
/* 3442 */     return new JAXBElement(_GetNotificationGroupReport_QNAME, GetNotificationGroupReportType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="setAllowedStatus")
/*      */   public JAXBElement<SetAllowedStatusType> createSetAllowedStatus(SetAllowedStatusType value)
/*      */   {
/* 3451 */     return new JAXBElement(_SetAllowedStatus_QNAME, SetAllowedStatusType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="searchNotifications")
/*      */   public JAXBElement<SearchNotificationsType> createSearchNotifications(SearchNotificationsType value)
/*      */   {
/* 3460 */     return new JAXBElement(_SearchNotifications_QNAME, SearchNotificationsType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="deletePriority")
/*      */   public JAXBElement<DeletePriorityType> createDeletePriority(DeletePriorityType value)
/*      */   {
/* 3469 */     return new JAXBElement(_DeletePriority_QNAME, DeletePriorityType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="getEmailAttachment")
/*      */   public JAXBElement<GetEmailAttachmentType> createGetEmailAttachment(GetEmailAttachmentType value)
/*      */   {
/* 3478 */     return new JAXBElement(_GetEmailAttachment_QNAME, GetEmailAttachmentType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="setPhonePrefixes")
/*      */   public JAXBElement<SetPhonePrefixesType> createSetPhonePrefixes(SetPhonePrefixesType value)
/*      */   {
/* 3487 */     return new JAXBElement(_SetPhonePrefixes_QNAME, SetPhonePrefixesType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="deleteIncidents")
/*      */   public JAXBElement<DeleteIncidentsType> createDeleteIncidents(DeleteIncidentsType value)
/*      */   {
/* 3496 */     return new JAXBElement(_DeleteIncidents_QNAME, DeleteIncidentsType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="getRecipientGroups")
/*      */   public JAXBElement<GetRecipientGroupsType> createGetRecipientGroups(GetRecipientGroupsType value)
/*      */   {
/* 3505 */     return new JAXBElement(_GetRecipientGroups_QNAME, GetRecipientGroupsType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="searchReports")
/*      */   public JAXBElement<SearchReportsType> createSearchReports(SearchReportsType value)
/*      */   {
/* 3514 */     return new JAXBElement(_SearchReports_QNAME, SearchReportsType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="getDisabledDevices")
/*      */   public JAXBElement<GetDisabledDevicesType> createGetDisabledDevices(GetDisabledDevicesType value)
/*      */   {
/* 3523 */     return new JAXBElement(_GetDisabledDevices_QNAME, GetDisabledDevicesType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="getRecordedResponse")
/*      */   public JAXBElement<GetRecordedResponseType> createGetRecordedResponse(GetRecordedResponseType value)
/*      */   {
/* 3532 */     return new JAXBElement(_GetRecordedResponse_QNAME, GetRecordedResponseType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="executeCustomReport")
/*      */   public JAXBElement<ExecuteCustomReportType> createExecuteCustomReport(ExecuteCustomReportType value)
/*      */   {
/* 3541 */     return new JAXBElement(_ExecuteCustomReport_QNAME, ExecuteCustomReportType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="tdxWrapper")
/*      */   public JAXBElement<TdxWrapperType> createTdxWrapper(TdxWrapperType value)
/*      */   {
/* 3550 */     return new JAXBElement(_TdxWrapper_QNAME, TdxWrapperType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="prepareToRecordMessages")
/*      */   public JAXBElement<PrepareToRecordMessagesType> createPrepareToRecordMessages(PrepareToRecordMessagesType value)
/*      */   {
/* 3559 */     return new JAXBElement(_PrepareToRecordMessages_QNAME, PrepareToRecordMessagesType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="addNewRecipientGroups")
/*      */   public JAXBElement<AddNewRecipientGroupsType> createAddNewRecipientGroups(AddNewRecipientGroupsType value)
/*      */   {
/* 3568 */     return new JAXBElement(_AddNewRecipientGroups_QNAME, AddNewRecipientGroupsType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="getNotificationReportRecipients")
/*      */   public JAXBElement<GetNotificationReportRecipientsType> createGetNotificationReportRecipients(GetNotificationReportRecipientsType value)
/*      */   {
/* 3577 */     return new JAXBElement(_GetNotificationReportRecipients_QNAME, GetNotificationReportRecipientsType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="getNotificationReports")
/*      */   public JAXBElement<GetNotificationReportsType> createGetNotificationReports(GetNotificationReportsType value)
/*      */   {
/* 3586 */     return new JAXBElement(_GetNotificationReports_QNAME, GetNotificationReportsType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="deleteVoiceFilesResponse")
/*      */   public JAXBElement<DeleteVoiceFilesResponseType> createDeleteVoiceFilesResponse(DeleteVoiceFilesResponseType value)
/*      */   {
/* 3595 */     return new JAXBElement(_DeleteVoiceFilesResponse_QNAME, DeleteVoiceFilesResponseType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="updateCategory")
/*      */   public JAXBElement<UpdateCategoryType> createUpdateCategory(UpdateCategoryType value)
/*      */   {
/* 3604 */     return new JAXBElement(_UpdateCategory_QNAME, UpdateCategoryType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="getVerbiage")
/*      */   public JAXBElement<GetVerbiageType> createGetVerbiage(GetVerbiageType value)
/*      */   {
/* 3613 */     return new JAXBElement(_GetVerbiage_QNAME, GetVerbiageType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="getPhonePrefixes")
/*      */   public JAXBElement<GetPhonePrefixesType> createGetPhonePrefixes(GetPhonePrefixesType value)
/*      */   {
/* 3622 */     return new JAXBElement(_GetPhonePrefixes_QNAME, GetPhonePrefixesType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="getPagingCarriers")
/*      */   public JAXBElement<GetPagingCarriersType> createGetPagingCarriers(GetPagingCarriersType value)
/*      */   {
/* 3631 */     return new JAXBElement(_GetPagingCarriers_QNAME, GetPagingCarriersType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="deleteRecipientSchedules")
/*      */   public JAXBElement<DeleteRecipientSchedulesType> createDeleteRecipientSchedules(DeleteRecipientSchedulesType value)
/*      */   {
/* 3640 */     return new JAXBElement(_DeleteRecipientSchedules_QNAME, DeleteRecipientSchedulesType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="deleteRecipientGroups")
/*      */   public JAXBElement<DeleteRecipientGroupsType> createDeleteRecipientGroups(DeleteRecipientGroupsType value)
/*      */   {
/* 3649 */     return new JAXBElement(_DeleteRecipientGroups_QNAME, DeleteRecipientGroupsType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="setVerbiage")
/*      */   public JAXBElement<SetVerbiageType> createSetVerbiage(SetVerbiageType value)
/*      */   {
/* 3658 */     return new JAXBElement(_SetVerbiage_QNAME, SetVerbiageType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="addNewRecipientSchedules")
/*      */   public JAXBElement<AddNewRecipientSchedulesType> createAddNewRecipientSchedules(AddNewRecipientSchedulesType value)
/*      */   {
/* 3667 */     return new JAXBElement(_AddNewRecipientSchedules_QNAME, AddNewRecipientSchedulesType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="hideReports")
/*      */   public JAXBElement<HideReportsType> createHideReports(HideReportsType value)
/*      */   {
/* 3676 */     return new JAXBElement(_HideReports_QNAME, HideReportsType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="getAllowedSMSList")
/*      */   public JAXBElement<GetAllowedSMSListType> createGetAllowedSMSList(GetAllowedSMSListType value)
/*      */   {
/* 3685 */     return new JAXBElement(_GetAllowedSMSList_QNAME, GetAllowedSMSListType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="permissionsCheckResponse")
/*      */   public JAXBElement<PermissionsCheckResponseType> createPermissionsCheckResponse(PermissionsCheckResponseType value)
/*      */   {
/* 3694 */     return new JAXBElement(_PermissionsCheckResponse_QNAME, PermissionsCheckResponseType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="idConvert")
/*      */   public JAXBElement<IdConvertType> createIdConvert(IdConvertType value)
/*      */   {
/* 3703 */     return new JAXBElement(_IdConvert_QNAME, IdConvertType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="getBlockedSMSList")
/*      */   public JAXBElement<GetBlockedSMSListType> createGetBlockedSMSList(GetBlockedSMSListType value)
/*      */   {
/* 3712 */     return new JAXBElement(_GetBlockedSMSList_QNAME, GetBlockedSMSListType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="updateRecipientSchedules")
/*      */   public JAXBElement<UpdateRecipientSchedulesType> createUpdateRecipientSchedules(UpdateRecipientSchedulesType value)
/*      */   {
/* 3721 */     return new JAXBElement(_UpdateRecipientSchedules_QNAME, UpdateRecipientSchedulesType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="getBlockedStatus")
/*      */   public JAXBElement<GetBlockedStatusType> createGetBlockedStatus(GetBlockedStatusType value)
/*      */   {
/* 3730 */     return new JAXBElement(_GetBlockedStatus_QNAME, GetBlockedStatusType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="addNewNotifications")
/*      */   public JAXBElement<AddNewNotificationsType> createAddNewNotifications(AddNewNotificationsType value)
/*      */   {
/* 3739 */     return new JAXBElement(_AddNewNotifications_QNAME, AddNewNotificationsType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="downloadVoiceFileResponse")
/*      */   public JAXBElement<DownloadVoiceFileResponseType> createDownloadVoiceFileResponse(DownloadVoiceFileResponseType value)
/*      */   {
/* 3748 */     return new JAXBElement(_DownloadVoiceFileResponse_QNAME, DownloadVoiceFileResponseType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="addCategory")
/*      */   public JAXBElement<AddCategoryType> createAddCategory(AddCategoryType value)
/*      */   {
/* 3757 */     return new JAXBElement(_AddCategory_QNAME, AddCategoryType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="oneStepNotification")
/*      */   public JAXBElement<OneStepNotificationType> createOneStepNotification(OneStepNotificationType value)
/*      */   {
/* 3766 */     return new JAXBElement(_OneStepNotification_QNAME, OneStepNotificationType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="setEmailAttachment")
/*      */   public JAXBElement<SetEmailAttachmentType> createSetEmailAttachment(SetEmailAttachmentType value)
/*      */   {
/* 3775 */     return new JAXBElement(_SetEmailAttachment_QNAME, SetEmailAttachmentType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="terminateNotifications")
/*      */   public JAXBElement<TerminateNotificationsType> createTerminateNotifications(TerminateNotificationsType value)
/*      */   {
/* 3784 */     return new JAXBElement(_TerminateNotifications_QNAME, TerminateNotificationsType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="deleteVoiceFiles")
/*      */   public JAXBElement<DeleteVoiceFilesType> createDeleteVoiceFiles(DeleteVoiceFilesType value)
/*      */   {
/* 3793 */     return new JAXBElement(_DeleteVoiceFiles_QNAME, DeleteVoiceFilesType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="addSeverity")
/*      */   public JAXBElement<AddSeverityType> createAddSeverity(AddSeverityType value)
/*      */   {
/* 3802 */     return new JAXBElement(_AddSeverity_QNAME, AddSeverityType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="getDivisions")
/*      */   public JAXBElement<GetDivisionsType> createGetDivisions(GetDivisionsType value)
/*      */   {
/* 3811 */     return new JAXBElement(_GetDivisions_QNAME, GetDivisionsType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="initiateNotifications")
/*      */   public JAXBElement<InitiateNotificationsType> createInitiateNotifications(InitiateNotificationsType value)
/*      */   {
/* 3820 */     return new JAXBElement(_InitiateNotifications_QNAME, InitiateNotificationsType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="createSingleSignOnToken")
/*      */   public JAXBElement<CreateSingleSignOnTokenType> createCreateSingleSignOnToken(CreateSingleSignOnTokenType value)
/*      */   {
/* 3829 */     return new JAXBElement(_CreateSingleSignOnToken_QNAME, CreateSingleSignOnTokenType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="addPriority")
/*      */   public JAXBElement<AddPriorityType> createAddPriority(AddPriorityType value)
/*      */   {
/* 3838 */     return new JAXBElement(_AddPriority_QNAME, AddPriorityType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="getDynamicGroupMembers")
/*      */   public JAXBElement<GetDynamicGroupMembersType> createGetDynamicGroupMembers(GetDynamicGroupMembersType value)
/*      */   {
/* 3847 */     return new JAXBElement(_GetDynamicGroupMembers_QNAME, GetDynamicGroupMembersType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="initiateNotificationGroups")
/*      */   public JAXBElement<InitiateNotificationGroupsType> createInitiateNotificationGroups(InitiateNotificationGroupsType value)
/*      */   {
/* 3856 */     return new JAXBElement(_InitiateNotificationGroups_QNAME, InitiateNotificationGroupsType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="validateSingleSignOnToken")
/*      */   public JAXBElement<ValidateSingleSignOnTokenType> createValidateSingleSignOnToken(ValidateSingleSignOnTokenType value)
/*      */   {
/* 3865 */     return new JAXBElement(_ValidateSingleSignOnToken_QNAME, ValidateSingleSignOnTokenType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="getCustomVerbiageTypeByDivision")
/*      */   public JAXBElement<GetCustomVerbiageTypeDivisionType> createGetCustomVerbiageTypeByDivision(GetCustomVerbiageTypeDivisionType value)
/*      */   {
/* 3874 */     return new JAXBElement(_GetCustomVerbiageTypeByDivision_QNAME, GetCustomVerbiageTypeDivisionType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="updateSeverity")
/*      */   public JAXBElement<UpdateSeverityType> createUpdateSeverity(UpdateSeverityType value)
/*      */   {
/* 3883 */     return new JAXBElement(_UpdateSeverity_QNAME, UpdateSeverityType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="addNewNotificationGroups")
/*      */   public JAXBElement<AddNewNotificationGroupsType> createAddNewNotificationGroups(AddNewNotificationGroupsType value)
/*      */   {
/* 3892 */     return new JAXBElement(_AddNewNotificationGroups_QNAME, AddNewNotificationGroupsType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="updateIncidents")
/*      */   public JAXBElement<UpdateIncidentsType> createUpdateIncidents(UpdateIncidentsType value)
/*      */   {
/* 3901 */     return new JAXBElement(_UpdateIncidents_QNAME, UpdateIncidentsType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="addNewIncidents")
/*      */   public JAXBElement<AddNewIncidentsType> createAddNewIncidents(AddNewIncidentsType value)
/*      */   {
/* 3910 */     return new JAXBElement(_AddNewIncidents_QNAME, AddNewIncidentsType.class, null, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="recipientGroupTitle", scope=DynamicRecipients2Type.class)
/*      */   public JAXBElement<String> createDynamicRecipients2TypeRecipientGroupTitle(String value)
/*      */   {
/* 3919 */     return new JAXBElement(_DynamicRecipients2TypeRecipientGroupTitle_QNAME, String.class, DynamicRecipients2Type.class, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="recipientDetail", scope=DynamicRecipients2Type.class)
/*      */   public JAXBElement<RecipientDetailType> createDynamicRecipients2TypeRecipientDetail(RecipientDetailType value)
/*      */   {
/* 3928 */     return new JAXBElement(_DynamicRecipients2TypeRecipientDetail_QNAME, RecipientDetailType.class, DynamicRecipients2Type.class, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="recipient", scope=DynamicRecipients2Type.class)
/*      */   public JAXBElement<RecipientType> createDynamicRecipients2TypeRecipient(RecipientType value)
/*      */   {
/* 3937 */     return new JAXBElement(_DynamicRecipients2TypeRecipient_QNAME, RecipientType.class, DynamicRecipients2Type.class, value);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   @XmlElementDecl(namespace="http://www.mir3.com/ws", name="recipientScheduleTitle", scope=DynamicRecipients2Type.class)
/*      */   public JAXBElement<String> createDynamicRecipients2TypeRecipientScheduleTitle(String value)
/*      */   {
/* 3946 */     return new JAXBElement(_DynamicRecipients2TypeRecipientScheduleTitle_QNAME, String.class, DynamicRecipients2Type.class, value);
/*      */   }
/*      */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\ObjectFactory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */