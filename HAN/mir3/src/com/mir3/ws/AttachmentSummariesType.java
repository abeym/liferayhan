/*    */ package com.mir3.ws;
/*    */ 
/*    */ import java.util.ArrayList;
/*    */ import java.util.List;
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="attachmentSummariesType", propOrder={"attachmentSummaryType"})
/*    */ public class AttachmentSummariesType
/*    */ {
/*    */   @XmlElement(required=true)
/*    */   protected List<AttachmentSummaryType> attachmentSummaryType;
/*    */   
/*    */   public List<AttachmentSummaryType> getAttachmentSummaryType()
/*    */   {
/* 63 */     if (this.attachmentSummaryType == null) {
/* 64 */       this.attachmentSummaryType = new ArrayList();
/*    */     }
/* 66 */     return this.attachmentSummaryType;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\AttachmentSummariesType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */