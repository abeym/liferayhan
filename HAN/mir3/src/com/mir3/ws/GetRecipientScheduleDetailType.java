/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="getRecipientScheduleDetailType", propOrder={"recipientScheduleDetail"})
/*    */ public class GetRecipientScheduleDetailType
/*    */ {
/*    */   protected RecipientScheduleDetailType recipientScheduleDetail;
/*    */   
/*    */   public RecipientScheduleDetailType getRecipientScheduleDetail()
/*    */   {
/* 45 */     return this.recipientScheduleDetail;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setRecipientScheduleDetail(RecipientScheduleDetailType value)
/*    */   {
/* 57 */     this.recipientScheduleDetail = value;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\GetRecipientScheduleDetailType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */