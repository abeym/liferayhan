/*     */ package com.mir3.ws;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="updatePriorityType", propOrder={"apiVersion", "authorization", "persistData", "requireFeature", "priority", "priorityDetail"})
/*     */ public class UpdatePriorityType
/*     */ {
/*     */   @XmlElement(required=true)
/*     */   protected String apiVersion;
/*     */   protected AuthorizationType authorization;
/*     */   protected Boolean persistData;
/*     */   protected List<String> requireFeature;
/*     */   @XmlElement(required=true)
/*     */   protected String priority;
/*     */   @XmlElement(required=true)
/*     */   protected TopicType priorityDetail;
/*     */   
/*     */   public String getApiVersion()
/*     */   {
/*  63 */     return this.apiVersion;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setApiVersion(String value)
/*     */   {
/*  75 */     this.apiVersion = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public AuthorizationType getAuthorization()
/*     */   {
/*  87 */     return this.authorization;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAuthorization(AuthorizationType value)
/*     */   {
/*  99 */     this.authorization = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Boolean isPersistData()
/*     */   {
/* 111 */     return this.persistData;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPersistData(Boolean value)
/*     */   {
/* 123 */     this.persistData = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getRequireFeature()
/*     */   {
/* 149 */     if (this.requireFeature == null) {
/* 150 */       this.requireFeature = new ArrayList();
/*     */     }
/* 152 */     return this.requireFeature;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getPriority()
/*     */   {
/* 164 */     return this.priority;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPriority(String value)
/*     */   {
/* 176 */     this.priority = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public TopicType getPriorityDetail()
/*     */   {
/* 188 */     return this.priorityDetail;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPriorityDetail(TopicType value)
/*     */   {
/* 200 */     this.priorityDetail = value;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\UpdatePriorityType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */