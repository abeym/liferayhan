package com.mir3.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebParam.Mode;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.xml.ws.Holder;

@WebService(name="Mir3", targetNamespace="http://www.mir3.com/ws")
@SOAPBinding(parameterStyle=SOAPBinding.ParameterStyle.BARE)
public abstract interface Mir3
{
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType idConvertOp(@WebParam(name="idConvert", targetNamespace="http://www.mir3.com/ws", partName="idConvert") IdConvertType paramIdConvertType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType addCategoryOp(@WebParam(name="addCategory", targetNamespace="http://www.mir3.com/ws", partName="addCategory") AddCategoryType paramAddCategoryType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType addNewDivisionsOp(@WebParam(name="addNewDivisions", targetNamespace="http://www.mir3.com/ws", partName="addNewDivisions") AddNewDivisionsType paramAddNewDivisionsType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType addNewIncidentsOp(@WebParam(name="addNewIncidents", targetNamespace="http://www.mir3.com/ws", partName="addNewIncidents") AddNewIncidentsType paramAddNewIncidentsType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType addNewNotificationGroupsOp(@WebParam(name="addNewNotificationGroups", targetNamespace="http://www.mir3.com/ws", partName="addNewNotificationGroups") AddNewNotificationGroupsType paramAddNewNotificationGroupsType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType addNewNotificationsOp(@WebParam(name="addNewNotifications", targetNamespace="http://www.mir3.com/ws", partName="addNewNotifications") AddNewNotificationsType paramAddNewNotificationsType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType addNewRecipientGroupsOp(@WebParam(name="addNewRecipientGroups", targetNamespace="http://www.mir3.com/ws", partName="addNewRecipientGroups") AddNewRecipientGroupsType paramAddNewRecipientGroupsType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType addNewRecipientsOp(@WebParam(name="addNewRecipients", targetNamespace="http://www.mir3.com/ws", partName="addNewRecipients") AddNewRecipientsType paramAddNewRecipientsType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType addNewRecipientSchedulesOp(@WebParam(name="addNewRecipientSchedules", targetNamespace="http://www.mir3.com/ws", partName="addNewRecipientSchedules") AddNewRecipientSchedulesType paramAddNewRecipientSchedulesType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType addPriorityOp(@WebParam(name="addPriority", targetNamespace="http://www.mir3.com/ws", partName="addPriority") AddPriorityType paramAddPriorityType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType addRecipientsToGroupOp(@WebParam(name="addRecipientsToGroup", targetNamespace="http://www.mir3.com/ws", partName="addRecipientsToGroup") AddRecipientsToGroupType paramAddRecipientsToGroupType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType addSeverityOp(@WebParam(name="addSeverity", targetNamespace="http://www.mir3.com/ws", partName="addSeverity") AddSeverityType paramAddSeverityType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType changeIncidentsStatusOp(@WebParam(name="changeIncidentsStatus", targetNamespace="http://www.mir3.com/ws", partName="changeIncidentsStatus") ChangeIncidentsStatusType paramChangeIncidentsStatusType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType chooseNotificationResponseOptionOp(@WebParam(name="chooseNotificationResponseOption", targetNamespace="http://www.mir3.com/ws", partName="chooseNotificationResponseOption") ChooseNotificationResponseOptionRequestType paramChooseNotificationResponseOptionRequestType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType createSingleSignOnTokenOp(@WebParam(name="createSingleSignOnToken", targetNamespace="http://www.mir3.com/ws", partName="createSingleSignOnToken") CreateSingleSignOnTokenType paramCreateSingleSignOnTokenType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType deleteCategoryOp(@WebParam(name="deleteCategory", targetNamespace="http://www.mir3.com/ws", partName="deleteCategory") DeleteCategoryType paramDeleteCategoryType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType deleteDivisionsOp(@WebParam(name="deleteDivisions", targetNamespace="http://www.mir3.com/ws", partName="deleteDivisions") DeleteDivisionsType paramDeleteDivisionsType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType deleteIncidentsOp(@WebParam(name="deleteIncidents", targetNamespace="http://www.mir3.com/ws", partName="deleteIncidents") DeleteIncidentsType paramDeleteIncidentsType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType deleteNotificationGroupsOp(@WebParam(name="deleteNotificationGroups", targetNamespace="http://www.mir3.com/ws", partName="deleteNotificationGroups") DeleteNotificationGroupsType paramDeleteNotificationGroupsType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType deleteNotificationsOp(@WebParam(name="deleteNotifications", targetNamespace="http://www.mir3.com/ws", partName="deleteNotifications") DeleteNotificationsType paramDeleteNotificationsType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType deletePriorityOp(@WebParam(name="deletePriority", targetNamespace="http://www.mir3.com/ws", partName="deletePriority") DeletePriorityType paramDeletePriorityType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType deleteRecipientGroupsOp(@WebParam(name="deleteRecipientGroups", targetNamespace="http://www.mir3.com/ws", partName="deleteRecipientGroups") DeleteRecipientGroupsType paramDeleteRecipientGroupsType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType deleteRecipientsOp(@WebParam(name="deleteRecipients", targetNamespace="http://www.mir3.com/ws", partName="deleteRecipients") DeleteRecipientsType paramDeleteRecipientsType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType deleteRecipientSchedulesOp(@WebParam(name="deleteRecipientSchedules", targetNamespace="http://www.mir3.com/ws", partName="deleteRecipientSchedules") DeleteRecipientSchedulesType paramDeleteRecipientSchedulesType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType deleteSeverityOp(@WebParam(name="deleteSeverity", targetNamespace="http://www.mir3.com/ws", partName="deleteSeverity") DeleteSeverityType paramDeleteSeverityType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType deleteVoiceFilesOp(@WebParam(name="deleteVoiceFiles", targetNamespace="http://www.mir3.com/ws", partName="deleteVoiceFiles") DeleteVoiceFilesType paramDeleteVoiceFilesType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType deleteVoiceFilesResponseOp(@WebParam(name="deleteVoiceFilesResponse", targetNamespace="http://www.mir3.com/ws", partName="deleteVoiceFilesResponse") DeleteVoiceFilesResponseType paramDeleteVoiceFilesResponseType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType disableDevicesOp(@WebParam(name="disableDevices", targetNamespace="http://www.mir3.com/ws", partName="disableDevices") DisableDevicesType paramDisableDevicesType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType downloadVoiceFileOp(@WebParam(name="downloadVoiceFile", targetNamespace="http://www.mir3.com/ws", partName="downloadVoiceFile") DownloadVoiceFileType paramDownloadVoiceFileType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType downloadVoiceFileResponseOp(@WebParam(name="downloadVoiceFileResponse", targetNamespace="http://www.mir3.com/ws", partName="downloadVoiceFileResponse") DownloadVoiceFileResponseType paramDownloadVoiceFileResponseType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType executeCustomReportOp(@WebParam(name="executeCustomReport", targetNamespace="http://www.mir3.com/ws", partName="executeCustomReport") ExecuteCustomReportType paramExecuteCustomReportType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType getAllowedSMSListOp(@WebParam(name="getAllowedSMSList", targetNamespace="http://www.mir3.com/ws", partName="getAllowedSMSList") GetAllowedSMSListType paramGetAllowedSMSListType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType getAvailableTopicsOp(@WebParam(name="getAvailableTopics", targetNamespace="http://www.mir3.com/ws", partName="getAvailableTopics") GetAvailableTopicsType paramGetAvailableTopicsType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType getAvailableTopicsResponseOp(@WebParam(name="getAvailableTopicsResponse", targetNamespace="http://www.mir3.com/ws", partName="getAvailableTopicsResponse") GetAvailableTopicsResponseType paramGetAvailableTopicsResponseType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType getBlockedSMSListOp(@WebParam(name="getBlockedSMSList", targetNamespace="http://www.mir3.com/ws", partName="getBlockedSMSList") GetBlockedSMSListType paramGetBlockedSMSListType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType getBlockedStatusOp(@WebParam(name="getBlockedStatus", targetNamespace="http://www.mir3.com/ws", partName="getBlockedStatus") GetBlockedStatusType paramGetBlockedStatusType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType getDisabledDevicesOp(@WebParam(name="getDisabledDevices", targetNamespace="http://www.mir3.com/ws", partName="getDisabledDevices") GetDisabledDevicesType paramGetDisabledDevicesType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType getDivisionsOp(@WebParam(name="getDivisions", targetNamespace="http://www.mir3.com/ws", partName="getDivisions") GetDivisionsType paramGetDivisionsType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType getDynamicGroupMembersOp(@WebParam(name="getDynamicGroupMembers", targetNamespace="http://www.mir3.com/ws", partName="getDynamicGroupMembers") GetDynamicGroupMembersType paramGetDynamicGroupMembersType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType getEmailAttachmentOp(@WebParam(name="getEmailAttachment", targetNamespace="http://www.mir3.com/ws", partName="getEmailAttachment") GetEmailAttachmentType paramGetEmailAttachmentType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType getNotificationGroupReportOp(@WebParam(name="getNotificationGroupReport", targetNamespace="http://www.mir3.com/ws", partName="getNotificationGroupReport") GetNotificationGroupReportType paramGetNotificationGroupReportType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType getNotificationReportRecipientsOp(@WebParam(name="getNotificationReportRecipients", targetNamespace="http://www.mir3.com/ws", partName="getNotificationReportRecipients") GetNotificationReportRecipientsType paramGetNotificationReportRecipientsType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType getNotificationReportsOp(@WebParam(name="getNotificationReports", targetNamespace="http://www.mir3.com/ws", partName="getNotificationReports") GetNotificationReportsType paramGetNotificationReportsType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType getNotificationScheduleOp(@WebParam(name="getNotificationSchedule", targetNamespace="http://www.mir3.com/ws", partName="getNotificationSchedule") GetNotificationScheduleType paramGetNotificationScheduleType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType getNotificationScheduleResponseOp(@WebParam(name="getNotificationScheduleResponse", targetNamespace="http://www.mir3.com/ws", partName="getNotificationScheduleResponse") GetNotificationScheduleResponseType paramGetNotificationScheduleResponseType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType getPagingCarriersOp(@WebParam(name="getPagingCarriers", targetNamespace="http://www.mir3.com/ws", partName="getPagingCarriers") GetPagingCarriersType paramGetPagingCarriersType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType getPhonePrefixesOp(@WebParam(name="getPhonePrefixes", targetNamespace="http://www.mir3.com/ws", partName="getPhonePrefixes") GetPhonePrefixesType paramGetPhonePrefixesType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType getPortUtilizationOp(@WebParam(name="getPortUtilization", targetNamespace="http://www.mir3.com/ws", partName="getPortUtilization") GetPortUtilizationType paramGetPortUtilizationType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType getProviderInfoOp(@WebParam(name="getProviderInfo", targetNamespace="http://www.mir3.com/ws", partName="getProviderInfo") GetProviderInfoType paramGetProviderInfoType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType getRecipientRolesOp(@WebParam(name="getRecipientRoles", targetNamespace="http://www.mir3.com/ws", partName="getRecipientRoles") GetRecipientRolesType paramGetRecipientRolesType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType getRecipientScheduleDetailOp(@WebParam(name="getRecipientScheduleDetail", targetNamespace="http://www.mir3.com/ws", partName="getRecipientScheduleDetail") GetRecipientScheduleDetailType paramGetRecipientScheduleDetailType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType getRecordedResponseOp(@WebParam(name="getRecordedResponse", targetNamespace="http://www.mir3.com/ws", partName="getRecordedResponse") GetRecordedResponseType paramGetRecordedResponseType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType getVerbiageOp(@WebParam(name="getVerbiage", targetNamespace="http://www.mir3.com/ws", partName="getVerbiage") GetVerbiageType paramGetVerbiageType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType hideReportsOp(@WebParam(name="hideReports", targetNamespace="http://www.mir3.com/ws", partName="hideReports") HideReportsType paramHideReportsType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType hideReportsResponseOp(@WebParam(name="hideReportsResponse", targetNamespace="http://www.mir3.com/ws", partName="hideReportsResponse") HideReportsResponseType paramHideReportsResponseType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType initiateNotificationGroupsOp(@WebParam(name="initiateNotificationGroups", targetNamespace="http://www.mir3.com/ws", partName="initiateNotificationGroups") InitiateNotificationGroupsType paramInitiateNotificationGroupsType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType initiateNotificationsOp(@WebParam(name="initiateNotifications", targetNamespace="http://www.mir3.com/ws", partName="initiateNotifications") InitiateNotificationsType paramInitiateNotificationsType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType oneStepNotificationOp(@WebParam(name="oneStepNotification", targetNamespace="http://www.mir3.com/ws", partName="oneStepNotification") OneStepNotificationType paramOneStepNotificationType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType permissionsCheckOp(@WebParam(name="permissionsCheck", targetNamespace="http://www.mir3.com/ws", partName="permissionsCheck") PermissionsCheckType paramPermissionsCheckType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType permissionsCheckResponseOp(@WebParam(name="permissionsCheckResponse", targetNamespace="http://www.mir3.com/ws", partName="permissionsCheckResponse") PermissionsCheckResponseType paramPermissionsCheckResponseType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType prepareToRecordMessagesOp(@WebParam(name="prepareToRecordMessages", targetNamespace="http://www.mir3.com/ws", partName="prepareToRecordMessages") PrepareToRecordMessagesType paramPrepareToRecordMessagesType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType removeRecipientsFromGroupOp(@WebParam(name="removeRecipientsFromGroup", targetNamespace="http://www.mir3.com/ws", partName="removeRecipientsFromGroup") RemoveRecipientsFromGroupType paramRemoveRecipientsFromGroupType);
  
  @WebMethod
  public abstract void responseOp(@WebParam(name="response", targetNamespace="http://www.mir3.com/ws", mode=WebParam.Mode.INOUT, partName="response") Holder<ResponseType> paramHolder);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType searchIncidentsOp(@WebParam(name="searchIncidents", targetNamespace="http://www.mir3.com/ws", partName="searchIncidents") SearchIncidentsType paramSearchIncidentsType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType searchNotificationGroupsOp(@WebParam(name="searchNotificationGroups", targetNamespace="http://www.mir3.com/ws", partName="searchNotificationGroups") SearchNotificationGroupsType paramSearchNotificationGroupsType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType searchNotificationsOp(@WebParam(name="searchNotifications", targetNamespace="http://www.mir3.com/ws", partName="searchNotifications") SearchNotificationsType paramSearchNotificationsType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType searchRecipientGroupsOp(@WebParam(name="searchRecipientGroups", targetNamespace="http://www.mir3.com/ws", partName="searchRecipientGroups") SearchRecipientGroupsType paramSearchRecipientGroupsType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType searchRecipientsOp(@WebParam(name="searchRecipients", targetNamespace="http://www.mir3.com/ws", partName="searchRecipients") SearchRecipientsType paramSearchRecipientsType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType searchRecipientSchedulesOp(@WebParam(name="searchRecipientSchedules", targetNamespace="http://www.mir3.com/ws", partName="searchRecipientSchedules") SearchRecipientSchedulesType paramSearchRecipientSchedulesType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType searchReportsOp(@WebParam(name="searchReports", targetNamespace="http://www.mir3.com/ws", partName="searchReports") SearchReportsType paramSearchReportsType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType searchVoiceFilesOp(@WebParam(name="searchVoiceFiles", targetNamespace="http://www.mir3.com/ws", partName="searchVoiceFiles") SearchVoiceFilesType paramSearchVoiceFilesType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType searchVoiceFilesResponseOp(@WebParam(name="searchVoiceFilesResponse", targetNamespace="http://www.mir3.com/ws", partName="searchVoiceFilesResponse") SearchVoiceFilesResponseType paramSearchVoiceFilesResponseType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType setAllowedStatusOp(@WebParam(name="setAllowedStatus", targetNamespace="http://www.mir3.com/ws", partName="setAllowedStatus") SetAllowedStatusType paramSetAllowedStatusType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType setEmailAttachmentOp(@WebParam(name="setEmailAttachment", targetNamespace="http://www.mir3.com/ws", partName="setEmailAttachment") SetEmailAttachmentType paramSetEmailAttachmentType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType setNotificationScheduleOp(@WebParam(name="setNotificationSchedule", targetNamespace="http://www.mir3.com/ws", partName="setNotificationSchedule") SetNotificationScheduleType paramSetNotificationScheduleType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType setPhonePrefixesOp(@WebParam(name="setPhonePrefixes", targetNamespace="http://www.mir3.com/ws", partName="setPhonePrefixes") SetPhonePrefixesType paramSetPhonePrefixesType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType setVerbiageOp(@WebParam(name="setVerbiage", targetNamespace="http://www.mir3.com/ws", partName="setVerbiage") SetVerbiageType paramSetVerbiageType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType setCustomVerbiageTypeByDivisionOp(@WebParam(name="setCustomVerbiageTypeByDivision", targetNamespace="http://www.mir3.com/ws", partName="setCustomVerbiageTypeByDivision") SetCustomVerbiageTypeDivisionType paramSetCustomVerbiageTypeDivisionType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType getCustomVerbiageTypeByDivisionOp(@WebParam(name="getCustomVerbiageTypeByDivision", targetNamespace="http://www.mir3.com/ws", partName="getCustomVerbiageTypeByDivision") GetCustomVerbiageTypeDivisionType paramGetCustomVerbiageTypeDivisionType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType tdxWrapperOp(@WebParam(name="tdxWrapper", targetNamespace="http://www.mir3.com/ws", partName="tdxWrapper") TdxWrapperType paramTdxWrapperType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType terminateNotificationGroupsOp(@WebParam(name="terminateNotificationGroups", targetNamespace="http://www.mir3.com/ws", partName="terminateNotificationGroups") TerminateNotificationGroupsType paramTerminateNotificationGroupsType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType terminateNotificationsOp(@WebParam(name="terminateNotifications", targetNamespace="http://www.mir3.com/ws", partName="terminateNotifications") TerminateNotificationsType paramTerminateNotificationsType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType updateCategoryOp(@WebParam(name="updateCategory", targetNamespace="http://www.mir3.com/ws", partName="updateCategory") UpdateCategoryType paramUpdateCategoryType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType updateDivisionOp(@WebParam(name="updateDivision", targetNamespace="http://www.mir3.com/ws", partName="updateDivision") UpdateDivisionType paramUpdateDivisionType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType updateIncidentsOp(@WebParam(name="updateIncidents", targetNamespace="http://www.mir3.com/ws", partName="updateIncidents") UpdateIncidentsType paramUpdateIncidentsType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType updateNotificationGroupsOp(@WebParam(name="updateNotificationGroups", targetNamespace="http://www.mir3.com/ws", partName="updateNotificationGroups") UpdateNotificationGroupsType paramUpdateNotificationGroupsType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType updateNotificationsOp(@WebParam(name="updateNotifications", targetNamespace="http://www.mir3.com/ws", partName="updateNotifications") UpdateNotificationsType paramUpdateNotificationsType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType updatePriorityOp(@WebParam(name="updatePriority", targetNamespace="http://www.mir3.com/ws", partName="updatePriority") UpdatePriorityType paramUpdatePriorityType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType updateRecipientGroupsOp(@WebParam(name="updateRecipientGroups", targetNamespace="http://www.mir3.com/ws", partName="updateRecipientGroups") UpdateRecipientGroupsType paramUpdateRecipientGroupsType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType updateRecipientsOp(@WebParam(name="updateRecipients", targetNamespace="http://www.mir3.com/ws", partName="updateRecipients") UpdateRecipientsType paramUpdateRecipientsType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType updateRecipientSchedulesOp(@WebParam(name="updateRecipientSchedules", targetNamespace="http://www.mir3.com/ws", partName="updateRecipientSchedules") UpdateRecipientSchedulesType paramUpdateRecipientSchedulesType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType updateSeverityOp(@WebParam(name="updateSeverity", targetNamespace="http://www.mir3.com/ws", partName="updateSeverity") UpdateSeverityType paramUpdateSeverityType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType uploadVoiceFileOp(@WebParam(name="uploadVoiceFile", targetNamespace="http://www.mir3.com/ws", partName="uploadVoiceFile") UploadVoiceFileType paramUploadVoiceFileType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType uploadVoiceFileResponseOp(@WebParam(name="uploadVoiceFileResponse", targetNamespace="http://www.mir3.com/ws", partName="uploadVoiceFileResponse") UploadVoiceFileResponseType paramUploadVoiceFileResponseType);
  
  @WebMethod
  @WebResult(name="response", targetNamespace="http://www.mir3.com/ws", partName="response")
  public abstract ResponseType validateSingleSignOnTokenOp(@WebParam(name="validateSingleSignOnToken", targetNamespace="http://www.mir3.com/ws", partName="validateSingleSignOnToken") ValidateSingleSignOnTokenType paramValidateSingleSignOnTokenType);
}


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\Mir3.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */