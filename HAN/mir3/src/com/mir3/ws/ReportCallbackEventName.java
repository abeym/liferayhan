/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlEnum;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlType(name="reportCallbackEventName")
/*    */ @XmlEnum
/*    */ public enum ReportCallbackEventName
/*    */ {
/* 28 */   EXTRA_STATUS_INFO, 
/* 29 */   RESPONSE, 
/* 30 */   NOTIFICATION_CLOSED;
/*    */   
/*    */   public String value() {
/* 33 */     return name();
/*    */   }
/*    */   
/*    */   public static ReportCallbackEventName fromValue(String v) {
/* 37 */     return valueOf(v);
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\ReportCallbackEventName.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */