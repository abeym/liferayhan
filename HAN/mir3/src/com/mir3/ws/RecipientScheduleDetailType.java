/*     */ package com.mir3.ws;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="recipientScheduleDetailType", propOrder={"scheduleName", "division", "shifts", "subscriptions", "suppressedSubscriptions"})
/*     */ public class RecipientScheduleDetailType
/*     */ {
/*     */   @XmlElement(required=true)
/*     */   protected String scheduleName;
/*     */   protected String division;
/*     */   protected List<RecipientShiftType> shifts;
/*     */   protected SubscriptionsType subscriptions;
/*     */   protected SuppressedSubscriptionsType suppressedSubscriptions;
/*     */   
/*     */   public String getScheduleName()
/*     */   {
/*  61 */     return this.scheduleName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setScheduleName(String value)
/*     */   {
/*  73 */     this.scheduleName = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getDivision()
/*     */   {
/*  85 */     return this.division;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDivision(String value)
/*     */   {
/*  97 */     this.division = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<RecipientShiftType> getShifts()
/*     */   {
/* 123 */     if (this.shifts == null) {
/* 124 */       this.shifts = new ArrayList();
/*     */     }
/* 126 */     return this.shifts;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public SubscriptionsType getSubscriptions()
/*     */   {
/* 138 */     return this.subscriptions;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setSubscriptions(SubscriptionsType value)
/*     */   {
/* 150 */     this.subscriptions = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public SuppressedSubscriptionsType getSuppressedSubscriptions()
/*     */   {
/* 162 */     return this.suppressedSubscriptions;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setSuppressedSubscriptions(SuppressedSubscriptionsType value)
/*     */   {
/* 174 */     this.suppressedSubscriptions = value;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\RecipientScheduleDetailType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */