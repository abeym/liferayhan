/*     */ package com.mir3.ws;
/*     */ 
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="placeholderType", propOrder={"name", "optional", "text", "longText", "checkbox", "multipleChoice"})
/*     */ public class PlaceholderType
/*     */ {
/*     */   @XmlElement(required=true)
/*     */   protected String name;
/*     */   protected boolean optional;
/*     */   protected TextInputPlaceholderType text;
/*     */   protected TextInputPlaceholderType longText;
/*     */   protected CheckboxPlaceholderType checkbox;
/*     */   protected MultipleChoicePlaceholderType multipleChoice;
/*     */   
/*     */   public String getName()
/*     */   {
/*  64 */     return this.name;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setName(String value)
/*     */   {
/*  76 */     this.name = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean isOptional()
/*     */   {
/*  84 */     return this.optional;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setOptional(boolean value)
/*     */   {
/*  92 */     this.optional = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public TextInputPlaceholderType getText()
/*     */   {
/* 104 */     return this.text;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setText(TextInputPlaceholderType value)
/*     */   {
/* 116 */     this.text = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public TextInputPlaceholderType getLongText()
/*     */   {
/* 128 */     return this.longText;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLongText(TextInputPlaceholderType value)
/*     */   {
/* 140 */     this.longText = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public CheckboxPlaceholderType getCheckbox()
/*     */   {
/* 152 */     return this.checkbox;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCheckbox(CheckboxPlaceholderType value)
/*     */   {
/* 164 */     this.checkbox = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public MultipleChoicePlaceholderType getMultipleChoice()
/*     */   {
/* 176 */     return this.multipleChoice;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setMultipleChoice(MultipleChoicePlaceholderType value)
/*     */   {
/* 188 */     this.multipleChoice = value;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\PlaceholderType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */