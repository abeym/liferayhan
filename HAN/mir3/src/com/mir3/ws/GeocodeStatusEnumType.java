/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlEnum;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlType(name="geocodeStatusEnumType")
/*    */ @XmlEnum
/*    */ public enum GeocodeStatusEnumType
/*    */ {
/* 30 */   ERROR, 
/* 31 */   MULTIPLE_MATCH, 
/* 32 */   MANUAL, 
/* 33 */   GEOCODED, 
/* 34 */   NOT_ATTEMPTED;
/*    */   
/*    */   public String value() {
/* 37 */     return name();
/*    */   }
/*    */   
/*    */   public static GeocodeStatusEnumType fromValue(String v) {
/* 41 */     return valueOf(v);
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\GeocodeStatusEnumType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */