/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="broadcastInfoType", propOrder={})
/*    */ public class BroadcastInfoType
/*    */ {
/*    */   protected int broadcastDuration;
/*    */   protected NotificationRecipientsDetailsType recipients;
/*    */   
/*    */   public int getBroadcastDuration()
/*    */   {
/* 43 */     return this.broadcastDuration;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setBroadcastDuration(int value)
/*    */   {
/* 51 */     this.broadcastDuration = value;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public NotificationRecipientsDetailsType getRecipients()
/*    */   {
/* 63 */     return this.recipients;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setRecipients(NotificationRecipientsDetailsType value)
/*    */   {
/* 75 */     this.recipients = value;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\BroadcastInfoType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */