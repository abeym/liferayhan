/*     */ package com.mir3.ws;
/*     */ 
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="reportGeneralStatistics", propOrder={})
/*     */ public class ReportGeneralStatistics
/*     */ {
/*     */   protected int totalResponseCount;
/*     */   protected int totalRecipientCount;
/*     */   protected int totalContactedCount;
/*     */   protected Integer totalDevicesContacted;
/*     */   protected Integer totalPhoneSeconds;
/*     */   
/*     */   public int getTotalResponseCount()
/*     */   {
/*  49 */     return this.totalResponseCount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTotalResponseCount(int value)
/*     */   {
/*  57 */     this.totalResponseCount = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getTotalRecipientCount()
/*     */   {
/*  65 */     return this.totalRecipientCount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTotalRecipientCount(int value)
/*     */   {
/*  73 */     this.totalRecipientCount = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getTotalContactedCount()
/*     */   {
/*  81 */     return this.totalContactedCount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTotalContactedCount(int value)
/*     */   {
/*  89 */     this.totalContactedCount = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getTotalDevicesContacted()
/*     */   {
/* 101 */     return this.totalDevicesContacted;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTotalDevicesContacted(Integer value)
/*     */   {
/* 113 */     this.totalDevicesContacted = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getTotalPhoneSeconds()
/*     */   {
/* 125 */     return this.totalPhoneSeconds;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTotalPhoneSeconds(Integer value)
/*     */   {
/* 137 */     this.totalPhoneSeconds = value;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\ReportGeneralStatistics.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */