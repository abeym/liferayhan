/*    */ package com.mir3.ws;
/*    */ 
/*    */ import java.util.ArrayList;
/*    */ import java.util.List;
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="locationStatusesType", propOrder={"defaultStatus", "locationStatus"})
/*    */ public class LocationStatusesType
/*    */ {
/*    */   protected LocationStatusType defaultStatus;
/*    */   protected List<LocationStatusType> locationStatus;
/*    */   
/*    */   public LocationStatusType getDefaultStatus()
/*    */   {
/* 50 */     return this.defaultStatus;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setDefaultStatus(LocationStatusType value)
/*    */   {
/* 62 */     this.defaultStatus = value;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public List<LocationStatusType> getLocationStatus()
/*    */   {
/* 88 */     if (this.locationStatus == null) {
/* 89 */       this.locationStatus = new ArrayList();
/*    */     }
/* 91 */     return this.locationStatus;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\LocationStatusesType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */