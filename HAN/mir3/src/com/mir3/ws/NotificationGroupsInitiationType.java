/*     */ package com.mir3.ws;
/*     */ 
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlSchemaType;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ import javax.xml.datatype.XMLGregorianCalendar;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="notificationGroupsInitiationType", propOrder={"notificationGroupTitle", "notificationGroupReportId", "notificationGroupReportUUID", "timeInititated"})
/*     */ public class NotificationGroupsInitiationType
/*     */ {
/*     */   @XmlElement(required=true)
/*     */   protected String notificationGroupTitle;
/*     */   protected Integer notificationGroupReportId;
/*     */   protected String notificationGroupReportUUID;
/*     */   @XmlSchemaType(name="dateTime")
/*     */   protected XMLGregorianCalendar timeInititated;
/*     */   
/*     */   public String getNotificationGroupTitle()
/*     */   {
/*  59 */     return this.notificationGroupTitle;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setNotificationGroupTitle(String value)
/*     */   {
/*  71 */     this.notificationGroupTitle = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getNotificationGroupReportId()
/*     */   {
/*  83 */     return this.notificationGroupReportId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setNotificationGroupReportId(Integer value)
/*     */   {
/*  95 */     this.notificationGroupReportId = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getNotificationGroupReportUUID()
/*     */   {
/* 107 */     return this.notificationGroupReportUUID;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setNotificationGroupReportUUID(String value)
/*     */   {
/* 119 */     this.notificationGroupReportUUID = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public XMLGregorianCalendar getTimeInititated()
/*     */   {
/* 131 */     return this.timeInititated;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTimeInititated(XMLGregorianCalendar value)
/*     */   {
/* 143 */     this.timeInititated = value;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\NotificationGroupsInitiationType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */