/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="getPagingCarriersResponseType", propOrder={})
/*    */ public class GetPagingCarriersResponseType
/*    */ {
/*    */   @XmlElement(required=true)
/*    */   protected PagingCarriersType pagingCarriers;
/*    */   
/*    */   public PagingCarriersType getPagingCarriers()
/*    */   {
/* 47 */     return this.pagingCarriers;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setPagingCarriers(PagingCarriersType value)
/*    */   {
/* 59 */     this.pagingCarriers = value;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\GetPagingCarriersResponseType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */