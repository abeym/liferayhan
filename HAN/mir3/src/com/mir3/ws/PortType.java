/*     */ package com.mir3.ws;
/*     */ 
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="portType", propOrder={"contactId", "contactUUID", "notificationReportId", "notificationReportUUID", "phoneNumber", "contactResult", "provider"})
/*     */ public class PortType
/*     */ {
/*     */   protected Integer contactId;
/*     */   protected String contactUUID;
/*     */   protected Integer notificationReportId;
/*     */   protected String notificationReportUUID;
/*     */   @XmlElement(required=true)
/*     */   protected String phoneNumber;
/*     */   @XmlElement(required=true)
/*     */   protected ContactResultType contactResult;
/*     */   @XmlElement(required=true)
/*     */   protected String provider;
/*     */   
/*     */   public Integer getContactId()
/*     */   {
/*  71 */     return this.contactId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setContactId(Integer value)
/*     */   {
/*  83 */     this.contactId = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getContactUUID()
/*     */   {
/*  95 */     return this.contactUUID;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setContactUUID(String value)
/*     */   {
/* 107 */     this.contactUUID = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getNotificationReportId()
/*     */   {
/* 119 */     return this.notificationReportId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setNotificationReportId(Integer value)
/*     */   {
/* 131 */     this.notificationReportId = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getNotificationReportUUID()
/*     */   {
/* 143 */     return this.notificationReportUUID;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setNotificationReportUUID(String value)
/*     */   {
/* 155 */     this.notificationReportUUID = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getPhoneNumber()
/*     */   {
/* 167 */     return this.phoneNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPhoneNumber(String value)
/*     */   {
/* 179 */     this.phoneNumber = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public ContactResultType getContactResult()
/*     */   {
/* 191 */     return this.contactResult;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setContactResult(ContactResultType value)
/*     */   {
/* 203 */     this.contactResult = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getProvider()
/*     */   {
/* 215 */     return this.provider;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setProvider(String value)
/*     */   {
/* 227 */     this.provider = value;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\PortType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */