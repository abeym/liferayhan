package com.mir3.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name="updateDivisionResponseType")
public class UpdateDivisionResponseType {}


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\UpdateDivisionResponseType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */