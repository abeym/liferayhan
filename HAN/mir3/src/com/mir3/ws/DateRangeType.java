/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlSchemaType;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ import javax.xml.datatype.XMLGregorianCalendar;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="dateRangeType", propOrder={"dateBefore", "dateAfter"})
/*    */ public class DateRangeType
/*    */ {
/*    */   @XmlElement(required=true)
/*    */   @XmlSchemaType(name="dateTime")
/*    */   protected XMLGregorianCalendar dateBefore;
/*    */   @XmlElement(required=true)
/*    */   @XmlSchemaType(name="dateTime")
/*    */   protected XMLGregorianCalendar dateAfter;
/*    */   
/*    */   public XMLGregorianCalendar getDateBefore()
/*    */   {
/* 55 */     return this.dateBefore;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setDateBefore(XMLGregorianCalendar value)
/*    */   {
/* 67 */     this.dateBefore = value;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public XMLGregorianCalendar getDateAfter()
/*    */   {
/* 79 */     return this.dateAfter;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setDateAfter(XMLGregorianCalendar value)
/*    */   {
/* 91 */     this.dateAfter = value;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\DateRangeType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */