/*    */ package com.mir3.ws;
/*    */ 
/*    */ import java.util.ArrayList;
/*    */ import java.util.List;
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="pagingCarriersType", propOrder={"pagingCarrier"})
/*    */ public class PagingCarriersType
/*    */ {
/*    */   @XmlElement(required=true)
/*    */   protected List<PagingCarrierType> pagingCarrier;
/*    */   
/*    */   public List<PagingCarrierType> getPagingCarrier()
/*    */   {
/* 63 */     if (this.pagingCarrier == null) {
/* 64 */       this.pagingCarrier = new ArrayList();
/*    */     }
/* 66 */     return this.pagingCarrier;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\PagingCarriersType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */