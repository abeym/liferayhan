/*     */ package com.mir3.ws;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="searchNotificationGroupsResponseType", propOrder={"matchCount", "returnCount", "notificationGroupDetail", "notificationGroup"})
/*     */ public class SearchNotificationGroupsResponseType
/*     */ {
/*     */   protected int matchCount;
/*     */   protected int returnCount;
/*     */   protected List<NotificationGroupDetailType> notificationGroupDetail;
/*     */   protected List<String> notificationGroup;
/*     */   
/*     */   public int getMatchCount()
/*     */   {
/*  52 */     return this.matchCount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setMatchCount(int value)
/*     */   {
/*  60 */     this.matchCount = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getReturnCount()
/*     */   {
/*  68 */     return this.returnCount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setReturnCount(int value)
/*     */   {
/*  76 */     this.returnCount = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<NotificationGroupDetailType> getNotificationGroupDetail()
/*     */   {
/* 102 */     if (this.notificationGroupDetail == null) {
/* 103 */       this.notificationGroupDetail = new ArrayList();
/*     */     }
/* 105 */     return this.notificationGroupDetail;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getNotificationGroup()
/*     */   {
/* 131 */     if (this.notificationGroup == null) {
/* 132 */       this.notificationGroup = new ArrayList();
/*     */     }
/* 134 */     return this.notificationGroup;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\SearchNotificationGroupsResponseType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */