/*     */ package com.mir3.ws;
/*     */ 
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="recipientGroupDetailType", propOrder={"title", "division", "group", "dynamicGroup", "members", "dynamicQuery", "_protected", "subscriptions", "suppressedSubscriptions"})
/*     */ public class RecipientGroupDetailType
/*     */ {
/*     */   @XmlElement(required=true)
/*     */   protected String title;
/*     */   protected String division;
/*     */   protected String group;
/*     */   protected Boolean dynamicGroup;
/*     */   protected RecipientGroupMembersType members;
/*     */   protected DynamicQueryType dynamicQuery;
/*     */   @XmlElement(name="protected")
/*     */   protected Boolean _protected;
/*     */   protected SubscriptionsType subscriptions;
/*     */   protected SuppressedSubscriptionsType suppressedSubscriptions;
/*     */   
/*     */   public String getTitle()
/*     */   {
/*  72 */     return this.title;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTitle(String value)
/*     */   {
/*  84 */     this.title = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getDivision()
/*     */   {
/*  96 */     return this.division;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDivision(String value)
/*     */   {
/* 108 */     this.division = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getGroup()
/*     */   {
/* 120 */     return this.group;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setGroup(String value)
/*     */   {
/* 132 */     this.group = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Boolean isDynamicGroup()
/*     */   {
/* 144 */     return this.dynamicGroup;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDynamicGroup(Boolean value)
/*     */   {
/* 156 */     this.dynamicGroup = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public RecipientGroupMembersType getMembers()
/*     */   {
/* 168 */     return this.members;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setMembers(RecipientGroupMembersType value)
/*     */   {
/* 180 */     this.members = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public DynamicQueryType getDynamicQuery()
/*     */   {
/* 192 */     return this.dynamicQuery;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDynamicQuery(DynamicQueryType value)
/*     */   {
/* 204 */     this.dynamicQuery = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Boolean isProtected()
/*     */   {
/* 216 */     return this._protected;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setProtected(Boolean value)
/*     */   {
/* 228 */     this._protected = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public SubscriptionsType getSubscriptions()
/*     */   {
/* 240 */     return this.subscriptions;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setSubscriptions(SubscriptionsType value)
/*     */   {
/* 252 */     this.subscriptions = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public SuppressedSubscriptionsType getSuppressedSubscriptions()
/*     */   {
/* 264 */     return this.suppressedSubscriptions;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setSuppressedSubscriptions(SuppressedSubscriptionsType value)
/*     */   {
/* 276 */     this.suppressedSubscriptions = value;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\RecipientGroupDetailType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */