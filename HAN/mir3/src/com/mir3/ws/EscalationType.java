/*     */ package com.mir3.ws;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="escalationType", propOrder={"maxResponseTime", "recipient", "dynamicRecipient", "recipientGroup", "recipientSchedule"})
/*     */ public class EscalationType
/*     */ {
/*     */   protected Integer maxResponseTime;
/*     */   protected List<RecipientType> recipient;
/*     */   protected List<RecipientDetailType> dynamicRecipient;
/*     */   protected List<String> recipientGroup;
/*     */   protected List<String> recipientSchedule;
/*     */   
/*     */   public Integer getMaxResponseTime()
/*     */   {
/*  59 */     return this.maxResponseTime;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setMaxResponseTime(Integer value)
/*     */   {
/*  71 */     this.maxResponseTime = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<RecipientType> getRecipient()
/*     */   {
/*  97 */     if (this.recipient == null) {
/*  98 */       this.recipient = new ArrayList();
/*     */     }
/* 100 */     return this.recipient;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<RecipientDetailType> getDynamicRecipient()
/*     */   {
/* 126 */     if (this.dynamicRecipient == null) {
/* 127 */       this.dynamicRecipient = new ArrayList();
/*     */     }
/* 129 */     return this.dynamicRecipient;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getRecipientGroup()
/*     */   {
/* 155 */     if (this.recipientGroup == null) {
/* 156 */       this.recipientGroup = new ArrayList();
/*     */     }
/* 158 */     return this.recipientGroup;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getRecipientSchedule()
/*     */   {
/* 184 */     if (this.recipientSchedule == null) {
/* 185 */       this.recipientSchedule = new ArrayList();
/*     */     }
/* 187 */     return this.recipientSchedule;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\EscalationType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */