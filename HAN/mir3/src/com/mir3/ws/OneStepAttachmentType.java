/*    */ package com.mir3.ws;
/*    */ 
/*    */ import java.util.ArrayList;
/*    */ import java.util.List;
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="oneStepAttachmentType", propOrder={"oneStepAttachmentDetail"})
/*    */ public class OneStepAttachmentType
/*    */ {
/*    */   protected List<OneStepAttachmentDetailType> oneStepAttachmentDetail;
/*    */   
/*    */   public List<OneStepAttachmentDetailType> getOneStepAttachmentDetail()
/*    */   {
/* 61 */     if (this.oneStepAttachmentDetail == null) {
/* 62 */       this.oneStepAttachmentDetail = new ArrayList();
/*    */     }
/* 64 */     return this.oneStepAttachmentDetail;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\OneStepAttachmentType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */