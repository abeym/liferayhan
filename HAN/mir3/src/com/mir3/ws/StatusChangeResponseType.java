/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="statusChangeResponseType", propOrder={"incidentTitle", "statusChanged"})
/*    */ public class StatusChangeResponseType
/*    */ {
/*    */   @XmlElement(required=true)
/*    */   protected String incidentTitle;
/*    */   protected boolean statusChanged;
/*    */   
/*    */   public String getIncidentTitle()
/*    */   {
/* 50 */     return this.incidentTitle;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setIncidentTitle(String value)
/*    */   {
/* 62 */     this.incidentTitle = value;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public boolean isStatusChanged()
/*    */   {
/* 70 */     return this.statusChanged;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setStatusChanged(boolean value)
/*    */   {
/* 78 */     this.statusChanged = value;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\StatusChangeResponseType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */