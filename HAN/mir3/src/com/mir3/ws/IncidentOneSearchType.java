/*     */ package com.mir3.ws;
/*     */ 
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlSchemaType;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ import javax.xml.datatype.XMLGregorianCalendar;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="incidentOneSearchType", propOrder={"and", "or", "not", "closedBy", "closedDate", "currentCatSubcat", "currentPriority", "currentSeverity", "initialCatSubcat", "initialPriority", "initialSeverity", "currentlyAssignedTo", "division", "incidentTitle", "initiallyAssignedTo", "lastUpdatedBy", "lastUpdatedDate", "openIncident", "openedBy", "openedDate", "referenceNumber", "reopenedBy", "reopenedDate"})
/*     */ public class IncidentOneSearchType
/*     */ {
/*     */   protected IncidentManySearchType and;
/*     */   protected IncidentManySearchType or;
/*     */   protected IncidentOneSearchType not;
/*     */   protected String closedBy;
/*     */   @XmlSchemaType(name="date")
/*     */   protected XMLGregorianCalendar closedDate;
/*     */   protected CatSubcatType currentCatSubcat;
/*     */   protected String currentPriority;
/*     */   protected String currentSeverity;
/*     */   protected CatSubcatType initialCatSubcat;
/*     */   protected String initialPriority;
/*     */   protected String initialSeverity;
/*     */   protected String currentlyAssignedTo;
/*     */   protected String division;
/*     */   protected String incidentTitle;
/*     */   protected String initiallyAssignedTo;
/*     */   protected String lastUpdatedBy;
/*     */   @XmlSchemaType(name="date")
/*     */   protected XMLGregorianCalendar lastUpdatedDate;
/*     */   protected Boolean openIncident;
/*     */   protected String openedBy;
/*     */   @XmlSchemaType(name="date")
/*     */   protected XMLGregorianCalendar openedDate;
/*     */   protected String referenceNumber;
/*     */   protected String reopenedBy;
/*     */   @XmlSchemaType(name="date")
/*     */   protected XMLGregorianCalendar reopenedDate;
/*     */   
/*     */   public IncidentManySearchType getAnd()
/*     */   {
/* 117 */     return this.and;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAnd(IncidentManySearchType value)
/*     */   {
/* 129 */     this.and = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public IncidentManySearchType getOr()
/*     */   {
/* 141 */     return this.or;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setOr(IncidentManySearchType value)
/*     */   {
/* 153 */     this.or = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public IncidentOneSearchType getNot()
/*     */   {
/* 165 */     return this.not;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setNot(IncidentOneSearchType value)
/*     */   {
/* 177 */     this.not = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getClosedBy()
/*     */   {
/* 189 */     return this.closedBy;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setClosedBy(String value)
/*     */   {
/* 201 */     this.closedBy = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public XMLGregorianCalendar getClosedDate()
/*     */   {
/* 213 */     return this.closedDate;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setClosedDate(XMLGregorianCalendar value)
/*     */   {
/* 225 */     this.closedDate = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public CatSubcatType getCurrentCatSubcat()
/*     */   {
/* 237 */     return this.currentCatSubcat;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCurrentCatSubcat(CatSubcatType value)
/*     */   {
/* 249 */     this.currentCatSubcat = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCurrentPriority()
/*     */   {
/* 261 */     return this.currentPriority;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCurrentPriority(String value)
/*     */   {
/* 273 */     this.currentPriority = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCurrentSeverity()
/*     */   {
/* 285 */     return this.currentSeverity;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCurrentSeverity(String value)
/*     */   {
/* 297 */     this.currentSeverity = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public CatSubcatType getInitialCatSubcat()
/*     */   {
/* 309 */     return this.initialCatSubcat;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setInitialCatSubcat(CatSubcatType value)
/*     */   {
/* 321 */     this.initialCatSubcat = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getInitialPriority()
/*     */   {
/* 333 */     return this.initialPriority;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setInitialPriority(String value)
/*     */   {
/* 345 */     this.initialPriority = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getInitialSeverity()
/*     */   {
/* 357 */     return this.initialSeverity;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setInitialSeverity(String value)
/*     */   {
/* 369 */     this.initialSeverity = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCurrentlyAssignedTo()
/*     */   {
/* 381 */     return this.currentlyAssignedTo;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCurrentlyAssignedTo(String value)
/*     */   {
/* 393 */     this.currentlyAssignedTo = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getDivision()
/*     */   {
/* 405 */     return this.division;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDivision(String value)
/*     */   {
/* 417 */     this.division = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getIncidentTitle()
/*     */   {
/* 429 */     return this.incidentTitle;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setIncidentTitle(String value)
/*     */   {
/* 441 */     this.incidentTitle = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getInitiallyAssignedTo()
/*     */   {
/* 453 */     return this.initiallyAssignedTo;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setInitiallyAssignedTo(String value)
/*     */   {
/* 465 */     this.initiallyAssignedTo = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLastUpdatedBy()
/*     */   {
/* 477 */     return this.lastUpdatedBy;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLastUpdatedBy(String value)
/*     */   {
/* 489 */     this.lastUpdatedBy = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public XMLGregorianCalendar getLastUpdatedDate()
/*     */   {
/* 501 */     return this.lastUpdatedDate;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLastUpdatedDate(XMLGregorianCalendar value)
/*     */   {
/* 513 */     this.lastUpdatedDate = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Boolean isOpenIncident()
/*     */   {
/* 525 */     return this.openIncident;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setOpenIncident(Boolean value)
/*     */   {
/* 537 */     this.openIncident = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getOpenedBy()
/*     */   {
/* 549 */     return this.openedBy;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setOpenedBy(String value)
/*     */   {
/* 561 */     this.openedBy = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public XMLGregorianCalendar getOpenedDate()
/*     */   {
/* 573 */     return this.openedDate;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setOpenedDate(XMLGregorianCalendar value)
/*     */   {
/* 585 */     this.openedDate = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getReferenceNumber()
/*     */   {
/* 597 */     return this.referenceNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setReferenceNumber(String value)
/*     */   {
/* 609 */     this.referenceNumber = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getReopenedBy()
/*     */   {
/* 621 */     return this.reopenedBy;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setReopenedBy(String value)
/*     */   {
/* 633 */     this.reopenedBy = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public XMLGregorianCalendar getReopenedDate()
/*     */   {
/* 645 */     return this.reopenedDate;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setReopenedDate(XMLGregorianCalendar value)
/*     */   {
/* 657 */     this.reopenedDate = value;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\IncidentOneSearchType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */