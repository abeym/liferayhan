/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlEnum;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlType(name="notificationMethodType")
/*    */ @XmlEnum
/*    */ public enum NotificationMethodType
/*    */ {
/* 30 */   BROADCAST, 
/* 31 */   FIRST_RESPONSE, 
/* 32 */   CALL_OUT, 
/* 33 */   SMS_OPTIN, 
/* 34 */   BULLETIN_BOARD;
/*    */   
/*    */   public String value() {
/* 37 */     return name();
/*    */   }
/*    */   
/*    */   public static NotificationMethodType fromValue(String v) {
/* 41 */     return valueOf(v);
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\NotificationMethodType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */