/*     */ package com.mir3.ws;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlSchemaType;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ import javax.xml.datatype.XMLGregorianCalendar;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="reportRecipientsQueryType", propOrder={"queryType", "firstOrLast", "facility", "addressTypeName", "address1", "address2", "building", "city", "floor", "state", "zip", "jobTitle", "companyName", "firstName", "lastName", "province", "country", "username", "responseOptionReportId", "responseOptionReportUUID", "recipientReportId", "recipientReportUUID", "changedAfter", "employeeId", "userId", "userUUID"})
/*     */ public class ReportRecipientsQueryType
/*     */ {
/*     */   @XmlElement(required=true)
/*     */   protected ReportQueryType queryType;
/*     */   protected List<String> firstOrLast;
/*     */   protected List<String> facility;
/*     */   protected List<String> addressTypeName;
/*     */   protected List<String> address1;
/*     */   protected List<String> address2;
/*     */   protected List<String> building;
/*     */   protected List<String> city;
/*     */   protected List<String> floor;
/*     */   protected List<String> state;
/*     */   protected List<String> zip;
/*     */   protected List<String> jobTitle;
/*     */   protected List<String> companyName;
/*     */   protected List<String> firstName;
/*     */   protected List<String> lastName;
/*     */   protected List<String> province;
/*     */   protected List<String> country;
/*     */   protected List<String> username;
/*     */   protected Integer responseOptionReportId;
/*     */   protected String responseOptionReportUUID;
/*     */   protected Integer recipientReportId;
/*     */   protected String recipientReportUUID;
/*     */   @XmlSchemaType(name="dateTime")
/*     */   protected XMLGregorianCalendar changedAfter;
/*     */   protected List<String> employeeId;
/*     */   @XmlElement(type=Integer.class)
/*     */   protected List<Integer> userId;
/*     */   protected List<String> userUUID;
/*     */   
/*     */   public ReportQueryType getQueryType()
/*     */   {
/* 132 */     return this.queryType;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setQueryType(ReportQueryType value)
/*     */   {
/* 144 */     this.queryType = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getFirstOrLast()
/*     */   {
/* 170 */     if (this.firstOrLast == null) {
/* 171 */       this.firstOrLast = new ArrayList();
/*     */     }
/* 173 */     return this.firstOrLast;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getFacility()
/*     */   {
/* 199 */     if (this.facility == null) {
/* 200 */       this.facility = new ArrayList();
/*     */     }
/* 202 */     return this.facility;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getAddressTypeName()
/*     */   {
/* 228 */     if (this.addressTypeName == null) {
/* 229 */       this.addressTypeName = new ArrayList();
/*     */     }
/* 231 */     return this.addressTypeName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getAddress1()
/*     */   {
/* 257 */     if (this.address1 == null) {
/* 258 */       this.address1 = new ArrayList();
/*     */     }
/* 260 */     return this.address1;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getAddress2()
/*     */   {
/* 286 */     if (this.address2 == null) {
/* 287 */       this.address2 = new ArrayList();
/*     */     }
/* 289 */     return this.address2;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getBuilding()
/*     */   {
/* 315 */     if (this.building == null) {
/* 316 */       this.building = new ArrayList();
/*     */     }
/* 318 */     return this.building;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getCity()
/*     */   {
/* 344 */     if (this.city == null) {
/* 345 */       this.city = new ArrayList();
/*     */     }
/* 347 */     return this.city;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getFloor()
/*     */   {
/* 373 */     if (this.floor == null) {
/* 374 */       this.floor = new ArrayList();
/*     */     }
/* 376 */     return this.floor;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getState()
/*     */   {
/* 402 */     if (this.state == null) {
/* 403 */       this.state = new ArrayList();
/*     */     }
/* 405 */     return this.state;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getZip()
/*     */   {
/* 431 */     if (this.zip == null) {
/* 432 */       this.zip = new ArrayList();
/*     */     }
/* 434 */     return this.zip;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getJobTitle()
/*     */   {
/* 460 */     if (this.jobTitle == null) {
/* 461 */       this.jobTitle = new ArrayList();
/*     */     }
/* 463 */     return this.jobTitle;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getCompanyName()
/*     */   {
/* 489 */     if (this.companyName == null) {
/* 490 */       this.companyName = new ArrayList();
/*     */     }
/* 492 */     return this.companyName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getFirstName()
/*     */   {
/* 518 */     if (this.firstName == null) {
/* 519 */       this.firstName = new ArrayList();
/*     */     }
/* 521 */     return this.firstName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getLastName()
/*     */   {
/* 547 */     if (this.lastName == null) {
/* 548 */       this.lastName = new ArrayList();
/*     */     }
/* 550 */     return this.lastName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getProvince()
/*     */   {
/* 576 */     if (this.province == null) {
/* 577 */       this.province = new ArrayList();
/*     */     }
/* 579 */     return this.province;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getCountry()
/*     */   {
/* 605 */     if (this.country == null) {
/* 606 */       this.country = new ArrayList();
/*     */     }
/* 608 */     return this.country;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getUsername()
/*     */   {
/* 634 */     if (this.username == null) {
/* 635 */       this.username = new ArrayList();
/*     */     }
/* 637 */     return this.username;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getResponseOptionReportId()
/*     */   {
/* 649 */     return this.responseOptionReportId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setResponseOptionReportId(Integer value)
/*     */   {
/* 661 */     this.responseOptionReportId = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getResponseOptionReportUUID()
/*     */   {
/* 673 */     return this.responseOptionReportUUID;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setResponseOptionReportUUID(String value)
/*     */   {
/* 685 */     this.responseOptionReportUUID = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getRecipientReportId()
/*     */   {
/* 697 */     return this.recipientReportId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRecipientReportId(Integer value)
/*     */   {
/* 709 */     this.recipientReportId = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getRecipientReportUUID()
/*     */   {
/* 721 */     return this.recipientReportUUID;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRecipientReportUUID(String value)
/*     */   {
/* 733 */     this.recipientReportUUID = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public XMLGregorianCalendar getChangedAfter()
/*     */   {
/* 745 */     return this.changedAfter;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setChangedAfter(XMLGregorianCalendar value)
/*     */   {
/* 757 */     this.changedAfter = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getEmployeeId()
/*     */   {
/* 783 */     if (this.employeeId == null) {
/* 784 */       this.employeeId = new ArrayList();
/*     */     }
/* 786 */     return this.employeeId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<Integer> getUserId()
/*     */   {
/* 812 */     if (this.userId == null) {
/* 813 */       this.userId = new ArrayList();
/*     */     }
/* 815 */     return this.userId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getUserUUID()
/*     */   {
/* 841 */     if (this.userUUID == null) {
/* 842 */       this.userUUID = new ArrayList();
/*     */     }
/* 844 */     return this.userUUID;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\ReportRecipientsQueryType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */