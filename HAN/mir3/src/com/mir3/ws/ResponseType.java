/*      */ package com.mir3.ws;
/*      */ 
/*      */ import java.util.ArrayList;
/*      */ import java.util.List;
/*      */ import javax.xml.bind.annotation.XmlAccessType;
/*      */ import javax.xml.bind.annotation.XmlAccessorType;
/*      */ import javax.xml.bind.annotation.XmlType;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ @XmlAccessorType(XmlAccessType.FIELD)
/*      */ @XmlType(name="responseType", propOrder={"success", "error", "addNewRecipientsResponse", "updateRecipientsResponse", "deleteRecipientsResponse", "searchRecipientsResponse", "searchReportsResponse", "hideReportsResponse", "getRecipientRolesResponse", "getpagingCarriersResponse", "getPortUtilizationResponse", "getProviderInfoResponse", "addNewRecipientGroupsResponse", "updateRecipientGroupResponse", "deleteRecipientGroupsResponse", "addRecipientsToGroupResponse", "removeRecipientsFromGroupResponse", "searchRecipientGroupsResponse", "getRecipientGroupsResponse", "addNewNotificationsResponse", "updateNotificationsResponse", "deleteNotificationsResponse", "initiateNotificationsResponse", "terminateNotificationsResponse", "terminateNotificationGroupsResponse", "setEmailAttachmentResponse", "getEmailAttachmentResponse", "getRecordedResponseResponse", "uploadVoiceFileResponse", "downloadVoiceFileResponse", "deleteVoiceFilesResponse", "searchVoiceFilesResponse", "getNotificationReportsResponse", "notificationReportRecipientsResponse", "oneStepNotificationResponse", "searchNotificationsResponse", "addNewNotificationGroupsResponse", "updateNotificationGroupsResponse", "deleteNotificationGroupsResponse", "initiateNotificationGroupsResponse", "notificationGroupReportResponse", "searchNotificationGroupsResponse", "permissionsCheckResponse", "prepareToRecordMessagesResponse", "addNewDivisionsResponse", "updateDivisionResponse", "getDivisionsResponse", "getPhonePrefixesResponse", "setPhonePrefixesResponse", "deleteDivisionsResponse", "createSingleSignOnTokenResponse", "validateSingleSignOnTokenResponse", "getNotificationScheduleResponse", "addNewRecipientSchedulesResponse", "updateRecipientSchedulesResponse", "deleteRecipientSchedulesResponse", "searchRecipientSchedulesResponse", "getRecipientScheduleDetail", "addNewDynamicGroupsResponse", "updateDynamicGroupsResponse", "deleteDynamicGroupsResponse", "getDynamicGroupMembersResponse", "getAvailableTopicsResponse", "addNewIncidentsResponse", "updateIncidentsResponse", "deleteIncidentsResponse", "searchIncidentsResponse", "changeIncidentsStatusResponse", "getDisabledDevicesResponse", "disableDevicesResponse", "chooseNotificationResponseOptionResponse", "setAllowedStatusResponse", "getBlockedStatusResponse", "getAllowedSMSListResponse", "getBlockedSMSListResponse", "executeCustomReportResponse", "getVerbiageResponse", "setVerbiageResponse", "setCustomVerbiageTypeByDivisionResponse", "getCustomVerbiageTypeByDivisionResponse", "getLoggedInUserResponse", "idConvertResponse"})
/*      */ public class ResponseType
/*      */ {
/*      */   protected boolean success;
/*      */   protected List<ErrorType> error;
/*      */   protected AddNewRecipientsResponseType addNewRecipientsResponse;
/*      */   protected UpdateRecipientsResponseType updateRecipientsResponse;
/*      */   protected DeleteRecipientsResponseType deleteRecipientsResponse;
/*      */   protected SearchRecipientsResponseType searchRecipientsResponse;
/*      */   protected SearchReportsResponseType searchReportsResponse;
/*      */   protected HideReportsResponseType hideReportsResponse;
/*      */   protected GetRecipientRolesResponseType getRecipientRolesResponse;
/*      */   protected GetPagingCarriersResponseType getpagingCarriersResponse;
/*      */   protected GetPortUtilizationResponseType getPortUtilizationResponse;
/*      */   protected GetProviderInfoResponseType getProviderInfoResponse;
/*      */   protected AddNewRecipientGroupsResponseType addNewRecipientGroupsResponse;
/*      */   protected UpdateRecipientGroupsResponseType updateRecipientGroupResponse;
/*      */   protected DeleteRecipientGroupsResponseType deleteRecipientGroupsResponse;
/*      */   protected AddRecipientsToGroupResponseType addRecipientsToGroupResponse;
/*      */   protected RemoveRecipientsFromGroupResponseType removeRecipientsFromGroupResponse;
/*      */   protected SearchRecipientGroupsResponseType searchRecipientGroupsResponse;
/*      */   protected GetRecipientGroupsResponseType getRecipientGroupsResponse;
/*      */   protected AddNewNotificationsResponseType addNewNotificationsResponse;
/*      */   protected UpdateNotificationsResponseType updateNotificationsResponse;
/*      */   protected DeleteNotificationsResponseType deleteNotificationsResponse;
/*      */   protected InitiateNotificationsResponseType initiateNotificationsResponse;
/*      */   protected TerminateNotificationsResponseType terminateNotificationsResponse;
/*      */   protected TerminateNotificationGroupsResponseType terminateNotificationGroupsResponse;
/*      */   protected SetEmailAttachmentResponseType setEmailAttachmentResponse;
/*      */   protected GetEmailAttachmentResponseType getEmailAttachmentResponse;
/*      */   protected GetRecordedResponseResponseType getRecordedResponseResponse;
/*      */   protected UploadVoiceFileResponseType uploadVoiceFileResponse;
/*      */   protected DownloadVoiceFileResponseType downloadVoiceFileResponse;
/*      */   protected DeleteVoiceFilesResponseType deleteVoiceFilesResponse;
/*      */   protected SearchVoiceFilesResponseType searchVoiceFilesResponse;
/*      */   protected GetNotificationReportsResponseType getNotificationReportsResponse;
/*      */   protected ReportRecipientsResponseType notificationReportRecipientsResponse;
/*      */   protected OneStepNotificationResponseType oneStepNotificationResponse;
/*      */   protected SearchNotificationsResponseType searchNotificationsResponse;
/*      */   protected AddNewNotificationGroupsResponseType addNewNotificationGroupsResponse;
/*      */   protected UpdateNotificationGroupsResponseType updateNotificationGroupsResponse;
/*      */   protected DeleteNotificationGroupsResponseType deleteNotificationGroupsResponse;
/*      */   protected InitiateNotificationGroupsResponseType initiateNotificationGroupsResponse;
/*      */   protected NotificationGroupReportResponseType notificationGroupReportResponse;
/*      */   protected SearchNotificationGroupsResponseType searchNotificationGroupsResponse;
/*      */   protected PermissionsCheckResponseType permissionsCheckResponse;
/*      */   protected PrepareToRecordMessagesResponseType prepareToRecordMessagesResponse;
/*      */   protected AddNewDivisionsResponseType addNewDivisionsResponse;
/*      */   protected UpdateDivisionResponseType updateDivisionResponse;
/*      */   protected GetDivisionsResponseType getDivisionsResponse;
/*      */   protected GetPhonePrefixesResponseType getPhonePrefixesResponse;
/*      */   protected SetPhonePrefixesResponseType setPhonePrefixesResponse;
/*      */   protected DeleteDivisionsResponseType deleteDivisionsResponse;
/*      */   protected CreateSingleSignOnTokenResponseType createSingleSignOnTokenResponse;
/*      */   protected ValidateSingleSignOnTokenResponseType validateSingleSignOnTokenResponse;
/*      */   protected GetNotificationScheduleResponseType getNotificationScheduleResponse;
/*      */   protected AddNewRecipientSchedulesResponseType addNewRecipientSchedulesResponse;
/*      */   protected UpdateRecipientSchedulesResponseType updateRecipientSchedulesResponse;
/*      */   protected DeleteRecipientSchedulesResponseType deleteRecipientSchedulesResponse;
/*      */   protected SearchRecipientSchedulesResponseType searchRecipientSchedulesResponse;
/*      */   protected GetRecipientScheduleDetailType getRecipientScheduleDetail;
/*      */   protected AddNewDynamicGroupsResponseType addNewDynamicGroupsResponse;
/*      */   protected UpdateDynamicGroupsResponseType updateDynamicGroupsResponse;
/*      */   protected DeleteDynamicGroupsResponseType deleteDynamicGroupsResponse;
/*      */   protected GetDynamicGroupMembersResponseType getDynamicGroupMembersResponse;
/*      */   protected GetAvailableTopicsResponseType getAvailableTopicsResponse;
/*      */   protected AddNewIncidentsResponseType addNewIncidentsResponse;
/*      */   protected UpdateIncidentsResponseType updateIncidentsResponse;
/*      */   protected DeleteIncidentsResponseType deleteIncidentsResponse;
/*      */   protected SearchIncidentsResponseType searchIncidentsResponse;
/*      */   protected ChangeIncidentsStatusResponseType changeIncidentsStatusResponse;
/*      */   protected GetDisabledDevicesResponseType getDisabledDevicesResponse;
/*      */   protected DisableDevicesResponseType disableDevicesResponse;
/*      */   protected ChooseNotificationResponseOptionResponseType chooseNotificationResponseOptionResponse;
/*      */   protected SetAllowedStatusResponseType setAllowedStatusResponse;
/*      */   protected GetBlockedStatusResponseType getBlockedStatusResponse;
/*      */   protected GetAllowedSMSListResponseType getAllowedSMSListResponse;
/*      */   protected GetBlockedSMSListResponseType getBlockedSMSListResponse;
/*      */   protected ExecuteCustomReportResponseType executeCustomReportResponse;
/*      */   protected GetVerbiageResponseType getVerbiageResponse;
/*      */   protected SetVerbiageResponseType setVerbiageResponse;
/*      */   protected SetCustomVerbiageTypeByDivisionResponseType setCustomVerbiageTypeByDivisionResponse;
/*      */   protected GetCustomVerbiageTypeByDivisionResponseType getCustomVerbiageTypeByDivisionResponse;
/*      */   protected GetLoggedInUserResponseType getLoggedInUserResponse;
/*      */   protected IdConvertResponseType idConvertResponse;
/*      */   
/*      */   public boolean isSuccess()
/*      */   {
/*  286 */     return this.success;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSuccess(boolean value)
/*      */   {
/*  294 */     this.success = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<ErrorType> getError()
/*      */   {
/*  320 */     if (this.error == null) {
/*  321 */       this.error = new ArrayList();
/*      */     }
/*  323 */     return this.error;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public AddNewRecipientsResponseType getAddNewRecipientsResponse()
/*      */   {
/*  335 */     return this.addNewRecipientsResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setAddNewRecipientsResponse(AddNewRecipientsResponseType value)
/*      */   {
/*  347 */     this.addNewRecipientsResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public UpdateRecipientsResponseType getUpdateRecipientsResponse()
/*      */   {
/*  359 */     return this.updateRecipientsResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setUpdateRecipientsResponse(UpdateRecipientsResponseType value)
/*      */   {
/*  371 */     this.updateRecipientsResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DeleteRecipientsResponseType getDeleteRecipientsResponse()
/*      */   {
/*  383 */     return this.deleteRecipientsResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setDeleteRecipientsResponse(DeleteRecipientsResponseType value)
/*      */   {
/*  395 */     this.deleteRecipientsResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public SearchRecipientsResponseType getSearchRecipientsResponse()
/*      */   {
/*  407 */     return this.searchRecipientsResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSearchRecipientsResponse(SearchRecipientsResponseType value)
/*      */   {
/*  419 */     this.searchRecipientsResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public SearchReportsResponseType getSearchReportsResponse()
/*      */   {
/*  431 */     return this.searchReportsResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSearchReportsResponse(SearchReportsResponseType value)
/*      */   {
/*  443 */     this.searchReportsResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public HideReportsResponseType getHideReportsResponse()
/*      */   {
/*  455 */     return this.hideReportsResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setHideReportsResponse(HideReportsResponseType value)
/*      */   {
/*  467 */     this.hideReportsResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetRecipientRolesResponseType getGetRecipientRolesResponse()
/*      */   {
/*  479 */     return this.getRecipientRolesResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setGetRecipientRolesResponse(GetRecipientRolesResponseType value)
/*      */   {
/*  491 */     this.getRecipientRolesResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetPagingCarriersResponseType getGetpagingCarriersResponse()
/*      */   {
/*  503 */     return this.getpagingCarriersResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setGetpagingCarriersResponse(GetPagingCarriersResponseType value)
/*      */   {
/*  515 */     this.getpagingCarriersResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetPortUtilizationResponseType getGetPortUtilizationResponse()
/*      */   {
/*  527 */     return this.getPortUtilizationResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setGetPortUtilizationResponse(GetPortUtilizationResponseType value)
/*      */   {
/*  539 */     this.getPortUtilizationResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetProviderInfoResponseType getGetProviderInfoResponse()
/*      */   {
/*  551 */     return this.getProviderInfoResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setGetProviderInfoResponse(GetProviderInfoResponseType value)
/*      */   {
/*  563 */     this.getProviderInfoResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public AddNewRecipientGroupsResponseType getAddNewRecipientGroupsResponse()
/*      */   {
/*  575 */     return this.addNewRecipientGroupsResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setAddNewRecipientGroupsResponse(AddNewRecipientGroupsResponseType value)
/*      */   {
/*  587 */     this.addNewRecipientGroupsResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public UpdateRecipientGroupsResponseType getUpdateRecipientGroupResponse()
/*      */   {
/*  599 */     return this.updateRecipientGroupResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setUpdateRecipientGroupResponse(UpdateRecipientGroupsResponseType value)
/*      */   {
/*  611 */     this.updateRecipientGroupResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DeleteRecipientGroupsResponseType getDeleteRecipientGroupsResponse()
/*      */   {
/*  623 */     return this.deleteRecipientGroupsResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setDeleteRecipientGroupsResponse(DeleteRecipientGroupsResponseType value)
/*      */   {
/*  635 */     this.deleteRecipientGroupsResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public AddRecipientsToGroupResponseType getAddRecipientsToGroupResponse()
/*      */   {
/*  647 */     return this.addRecipientsToGroupResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setAddRecipientsToGroupResponse(AddRecipientsToGroupResponseType value)
/*      */   {
/*  659 */     this.addRecipientsToGroupResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public RemoveRecipientsFromGroupResponseType getRemoveRecipientsFromGroupResponse()
/*      */   {
/*  671 */     return this.removeRecipientsFromGroupResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setRemoveRecipientsFromGroupResponse(RemoveRecipientsFromGroupResponseType value)
/*      */   {
/*  683 */     this.removeRecipientsFromGroupResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public SearchRecipientGroupsResponseType getSearchRecipientGroupsResponse()
/*      */   {
/*  695 */     return this.searchRecipientGroupsResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSearchRecipientGroupsResponse(SearchRecipientGroupsResponseType value)
/*      */   {
/*  707 */     this.searchRecipientGroupsResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetRecipientGroupsResponseType getGetRecipientGroupsResponse()
/*      */   {
/*  719 */     return this.getRecipientGroupsResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setGetRecipientGroupsResponse(GetRecipientGroupsResponseType value)
/*      */   {
/*  731 */     this.getRecipientGroupsResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public AddNewNotificationsResponseType getAddNewNotificationsResponse()
/*      */   {
/*  743 */     return this.addNewNotificationsResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setAddNewNotificationsResponse(AddNewNotificationsResponseType value)
/*      */   {
/*  755 */     this.addNewNotificationsResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public UpdateNotificationsResponseType getUpdateNotificationsResponse()
/*      */   {
/*  767 */     return this.updateNotificationsResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setUpdateNotificationsResponse(UpdateNotificationsResponseType value)
/*      */   {
/*  779 */     this.updateNotificationsResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DeleteNotificationsResponseType getDeleteNotificationsResponse()
/*      */   {
/*  791 */     return this.deleteNotificationsResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setDeleteNotificationsResponse(DeleteNotificationsResponseType value)
/*      */   {
/*  803 */     this.deleteNotificationsResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public InitiateNotificationsResponseType getInitiateNotificationsResponse()
/*      */   {
/*  815 */     return this.initiateNotificationsResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setInitiateNotificationsResponse(InitiateNotificationsResponseType value)
/*      */   {
/*  827 */     this.initiateNotificationsResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public TerminateNotificationsResponseType getTerminateNotificationsResponse()
/*      */   {
/*  839 */     return this.terminateNotificationsResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTerminateNotificationsResponse(TerminateNotificationsResponseType value)
/*      */   {
/*  851 */     this.terminateNotificationsResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public TerminateNotificationGroupsResponseType getTerminateNotificationGroupsResponse()
/*      */   {
/*  863 */     return this.terminateNotificationGroupsResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTerminateNotificationGroupsResponse(TerminateNotificationGroupsResponseType value)
/*      */   {
/*  875 */     this.terminateNotificationGroupsResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public SetEmailAttachmentResponseType getSetEmailAttachmentResponse()
/*      */   {
/*  887 */     return this.setEmailAttachmentResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSetEmailAttachmentResponse(SetEmailAttachmentResponseType value)
/*      */   {
/*  899 */     this.setEmailAttachmentResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetEmailAttachmentResponseType getGetEmailAttachmentResponse()
/*      */   {
/*  911 */     return this.getEmailAttachmentResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setGetEmailAttachmentResponse(GetEmailAttachmentResponseType value)
/*      */   {
/*  923 */     this.getEmailAttachmentResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetRecordedResponseResponseType getGetRecordedResponseResponse()
/*      */   {
/*  935 */     return this.getRecordedResponseResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setGetRecordedResponseResponse(GetRecordedResponseResponseType value)
/*      */   {
/*  947 */     this.getRecordedResponseResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public UploadVoiceFileResponseType getUploadVoiceFileResponse()
/*      */   {
/*  959 */     return this.uploadVoiceFileResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setUploadVoiceFileResponse(UploadVoiceFileResponseType value)
/*      */   {
/*  971 */     this.uploadVoiceFileResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DownloadVoiceFileResponseType getDownloadVoiceFileResponse()
/*      */   {
/*  983 */     return this.downloadVoiceFileResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setDownloadVoiceFileResponse(DownloadVoiceFileResponseType value)
/*      */   {
/*  995 */     this.downloadVoiceFileResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DeleteVoiceFilesResponseType getDeleteVoiceFilesResponse()
/*      */   {
/* 1007 */     return this.deleteVoiceFilesResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setDeleteVoiceFilesResponse(DeleteVoiceFilesResponseType value)
/*      */   {
/* 1019 */     this.deleteVoiceFilesResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public SearchVoiceFilesResponseType getSearchVoiceFilesResponse()
/*      */   {
/* 1031 */     return this.searchVoiceFilesResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSearchVoiceFilesResponse(SearchVoiceFilesResponseType value)
/*      */   {
/* 1043 */     this.searchVoiceFilesResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetNotificationReportsResponseType getGetNotificationReportsResponse()
/*      */   {
/* 1055 */     return this.getNotificationReportsResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setGetNotificationReportsResponse(GetNotificationReportsResponseType value)
/*      */   {
/* 1067 */     this.getNotificationReportsResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public ReportRecipientsResponseType getNotificationReportRecipientsResponse()
/*      */   {
/* 1079 */     return this.notificationReportRecipientsResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setNotificationReportRecipientsResponse(ReportRecipientsResponseType value)
/*      */   {
/* 1091 */     this.notificationReportRecipientsResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public OneStepNotificationResponseType getOneStepNotificationResponse()
/*      */   {
/* 1103 */     return this.oneStepNotificationResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setOneStepNotificationResponse(OneStepNotificationResponseType value)
/*      */   {
/* 1115 */     this.oneStepNotificationResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public SearchNotificationsResponseType getSearchNotificationsResponse()
/*      */   {
/* 1127 */     return this.searchNotificationsResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSearchNotificationsResponse(SearchNotificationsResponseType value)
/*      */   {
/* 1139 */     this.searchNotificationsResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public AddNewNotificationGroupsResponseType getAddNewNotificationGroupsResponse()
/*      */   {
/* 1151 */     return this.addNewNotificationGroupsResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setAddNewNotificationGroupsResponse(AddNewNotificationGroupsResponseType value)
/*      */   {
/* 1163 */     this.addNewNotificationGroupsResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public UpdateNotificationGroupsResponseType getUpdateNotificationGroupsResponse()
/*      */   {
/* 1175 */     return this.updateNotificationGroupsResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setUpdateNotificationGroupsResponse(UpdateNotificationGroupsResponseType value)
/*      */   {
/* 1187 */     this.updateNotificationGroupsResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DeleteNotificationGroupsResponseType getDeleteNotificationGroupsResponse()
/*      */   {
/* 1199 */     return this.deleteNotificationGroupsResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setDeleteNotificationGroupsResponse(DeleteNotificationGroupsResponseType value)
/*      */   {
/* 1211 */     this.deleteNotificationGroupsResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public InitiateNotificationGroupsResponseType getInitiateNotificationGroupsResponse()
/*      */   {
/* 1223 */     return this.initiateNotificationGroupsResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setInitiateNotificationGroupsResponse(InitiateNotificationGroupsResponseType value)
/*      */   {
/* 1235 */     this.initiateNotificationGroupsResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public NotificationGroupReportResponseType getNotificationGroupReportResponse()
/*      */   {
/* 1247 */     return this.notificationGroupReportResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setNotificationGroupReportResponse(NotificationGroupReportResponseType value)
/*      */   {
/* 1259 */     this.notificationGroupReportResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public SearchNotificationGroupsResponseType getSearchNotificationGroupsResponse()
/*      */   {
/* 1271 */     return this.searchNotificationGroupsResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSearchNotificationGroupsResponse(SearchNotificationGroupsResponseType value)
/*      */   {
/* 1283 */     this.searchNotificationGroupsResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public PermissionsCheckResponseType getPermissionsCheckResponse()
/*      */   {
/* 1295 */     return this.permissionsCheckResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setPermissionsCheckResponse(PermissionsCheckResponseType value)
/*      */   {
/* 1307 */     this.permissionsCheckResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public PrepareToRecordMessagesResponseType getPrepareToRecordMessagesResponse()
/*      */   {
/* 1319 */     return this.prepareToRecordMessagesResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setPrepareToRecordMessagesResponse(PrepareToRecordMessagesResponseType value)
/*      */   {
/* 1331 */     this.prepareToRecordMessagesResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public AddNewDivisionsResponseType getAddNewDivisionsResponse()
/*      */   {
/* 1343 */     return this.addNewDivisionsResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setAddNewDivisionsResponse(AddNewDivisionsResponseType value)
/*      */   {
/* 1355 */     this.addNewDivisionsResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public UpdateDivisionResponseType getUpdateDivisionResponse()
/*      */   {
/* 1367 */     return this.updateDivisionResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setUpdateDivisionResponse(UpdateDivisionResponseType value)
/*      */   {
/* 1379 */     this.updateDivisionResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetDivisionsResponseType getGetDivisionsResponse()
/*      */   {
/* 1391 */     return this.getDivisionsResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setGetDivisionsResponse(GetDivisionsResponseType value)
/*      */   {
/* 1403 */     this.getDivisionsResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetPhonePrefixesResponseType getGetPhonePrefixesResponse()
/*      */   {
/* 1415 */     return this.getPhonePrefixesResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setGetPhonePrefixesResponse(GetPhonePrefixesResponseType value)
/*      */   {
/* 1427 */     this.getPhonePrefixesResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public SetPhonePrefixesResponseType getSetPhonePrefixesResponse()
/*      */   {
/* 1439 */     return this.setPhonePrefixesResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSetPhonePrefixesResponse(SetPhonePrefixesResponseType value)
/*      */   {
/* 1451 */     this.setPhonePrefixesResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DeleteDivisionsResponseType getDeleteDivisionsResponse()
/*      */   {
/* 1463 */     return this.deleteDivisionsResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setDeleteDivisionsResponse(DeleteDivisionsResponseType value)
/*      */   {
/* 1475 */     this.deleteDivisionsResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public CreateSingleSignOnTokenResponseType getCreateSingleSignOnTokenResponse()
/*      */   {
/* 1487 */     return this.createSingleSignOnTokenResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setCreateSingleSignOnTokenResponse(CreateSingleSignOnTokenResponseType value)
/*      */   {
/* 1499 */     this.createSingleSignOnTokenResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public ValidateSingleSignOnTokenResponseType getValidateSingleSignOnTokenResponse()
/*      */   {
/* 1511 */     return this.validateSingleSignOnTokenResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setValidateSingleSignOnTokenResponse(ValidateSingleSignOnTokenResponseType value)
/*      */   {
/* 1523 */     this.validateSingleSignOnTokenResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetNotificationScheduleResponseType getGetNotificationScheduleResponse()
/*      */   {
/* 1535 */     return this.getNotificationScheduleResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setGetNotificationScheduleResponse(GetNotificationScheduleResponseType value)
/*      */   {
/* 1547 */     this.getNotificationScheduleResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public AddNewRecipientSchedulesResponseType getAddNewRecipientSchedulesResponse()
/*      */   {
/* 1559 */     return this.addNewRecipientSchedulesResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setAddNewRecipientSchedulesResponse(AddNewRecipientSchedulesResponseType value)
/*      */   {
/* 1571 */     this.addNewRecipientSchedulesResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public UpdateRecipientSchedulesResponseType getUpdateRecipientSchedulesResponse()
/*      */   {
/* 1583 */     return this.updateRecipientSchedulesResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setUpdateRecipientSchedulesResponse(UpdateRecipientSchedulesResponseType value)
/*      */   {
/* 1595 */     this.updateRecipientSchedulesResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DeleteRecipientSchedulesResponseType getDeleteRecipientSchedulesResponse()
/*      */   {
/* 1607 */     return this.deleteRecipientSchedulesResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setDeleteRecipientSchedulesResponse(DeleteRecipientSchedulesResponseType value)
/*      */   {
/* 1619 */     this.deleteRecipientSchedulesResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public SearchRecipientSchedulesResponseType getSearchRecipientSchedulesResponse()
/*      */   {
/* 1631 */     return this.searchRecipientSchedulesResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSearchRecipientSchedulesResponse(SearchRecipientSchedulesResponseType value)
/*      */   {
/* 1643 */     this.searchRecipientSchedulesResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetRecipientScheduleDetailType getGetRecipientScheduleDetail()
/*      */   {
/* 1655 */     return this.getRecipientScheduleDetail;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setGetRecipientScheduleDetail(GetRecipientScheduleDetailType value)
/*      */   {
/* 1667 */     this.getRecipientScheduleDetail = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public AddNewDynamicGroupsResponseType getAddNewDynamicGroupsResponse()
/*      */   {
/* 1679 */     return this.addNewDynamicGroupsResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setAddNewDynamicGroupsResponse(AddNewDynamicGroupsResponseType value)
/*      */   {
/* 1691 */     this.addNewDynamicGroupsResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public UpdateDynamicGroupsResponseType getUpdateDynamicGroupsResponse()
/*      */   {
/* 1703 */     return this.updateDynamicGroupsResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setUpdateDynamicGroupsResponse(UpdateDynamicGroupsResponseType value)
/*      */   {
/* 1715 */     this.updateDynamicGroupsResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DeleteDynamicGroupsResponseType getDeleteDynamicGroupsResponse()
/*      */   {
/* 1727 */     return this.deleteDynamicGroupsResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setDeleteDynamicGroupsResponse(DeleteDynamicGroupsResponseType value)
/*      */   {
/* 1739 */     this.deleteDynamicGroupsResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetDynamicGroupMembersResponseType getGetDynamicGroupMembersResponse()
/*      */   {
/* 1751 */     return this.getDynamicGroupMembersResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setGetDynamicGroupMembersResponse(GetDynamicGroupMembersResponseType value)
/*      */   {
/* 1763 */     this.getDynamicGroupMembersResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetAvailableTopicsResponseType getGetAvailableTopicsResponse()
/*      */   {
/* 1775 */     return this.getAvailableTopicsResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setGetAvailableTopicsResponse(GetAvailableTopicsResponseType value)
/*      */   {
/* 1787 */     this.getAvailableTopicsResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public AddNewIncidentsResponseType getAddNewIncidentsResponse()
/*      */   {
/* 1799 */     return this.addNewIncidentsResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setAddNewIncidentsResponse(AddNewIncidentsResponseType value)
/*      */   {
/* 1811 */     this.addNewIncidentsResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public UpdateIncidentsResponseType getUpdateIncidentsResponse()
/*      */   {
/* 1823 */     return this.updateIncidentsResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setUpdateIncidentsResponse(UpdateIncidentsResponseType value)
/*      */   {
/* 1835 */     this.updateIncidentsResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DeleteIncidentsResponseType getDeleteIncidentsResponse()
/*      */   {
/* 1847 */     return this.deleteIncidentsResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setDeleteIncidentsResponse(DeleteIncidentsResponseType value)
/*      */   {
/* 1859 */     this.deleteIncidentsResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public SearchIncidentsResponseType getSearchIncidentsResponse()
/*      */   {
/* 1871 */     return this.searchIncidentsResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSearchIncidentsResponse(SearchIncidentsResponseType value)
/*      */   {
/* 1883 */     this.searchIncidentsResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public ChangeIncidentsStatusResponseType getChangeIncidentsStatusResponse()
/*      */   {
/* 1895 */     return this.changeIncidentsStatusResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setChangeIncidentsStatusResponse(ChangeIncidentsStatusResponseType value)
/*      */   {
/* 1907 */     this.changeIncidentsStatusResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetDisabledDevicesResponseType getGetDisabledDevicesResponse()
/*      */   {
/* 1919 */     return this.getDisabledDevicesResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setGetDisabledDevicesResponse(GetDisabledDevicesResponseType value)
/*      */   {
/* 1931 */     this.getDisabledDevicesResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DisableDevicesResponseType getDisableDevicesResponse()
/*      */   {
/* 1943 */     return this.disableDevicesResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setDisableDevicesResponse(DisableDevicesResponseType value)
/*      */   {
/* 1955 */     this.disableDevicesResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public ChooseNotificationResponseOptionResponseType getChooseNotificationResponseOptionResponse()
/*      */   {
/* 1967 */     return this.chooseNotificationResponseOptionResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setChooseNotificationResponseOptionResponse(ChooseNotificationResponseOptionResponseType value)
/*      */   {
/* 1979 */     this.chooseNotificationResponseOptionResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public SetAllowedStatusResponseType getSetAllowedStatusResponse()
/*      */   {
/* 1991 */     return this.setAllowedStatusResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSetAllowedStatusResponse(SetAllowedStatusResponseType value)
/*      */   {
/* 2003 */     this.setAllowedStatusResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetBlockedStatusResponseType getGetBlockedStatusResponse()
/*      */   {
/* 2015 */     return this.getBlockedStatusResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setGetBlockedStatusResponse(GetBlockedStatusResponseType value)
/*      */   {
/* 2027 */     this.getBlockedStatusResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetAllowedSMSListResponseType getGetAllowedSMSListResponse()
/*      */   {
/* 2039 */     return this.getAllowedSMSListResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setGetAllowedSMSListResponse(GetAllowedSMSListResponseType value)
/*      */   {
/* 2051 */     this.getAllowedSMSListResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetBlockedSMSListResponseType getGetBlockedSMSListResponse()
/*      */   {
/* 2063 */     return this.getBlockedSMSListResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setGetBlockedSMSListResponse(GetBlockedSMSListResponseType value)
/*      */   {
/* 2075 */     this.getBlockedSMSListResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public ExecuteCustomReportResponseType getExecuteCustomReportResponse()
/*      */   {
/* 2087 */     return this.executeCustomReportResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setExecuteCustomReportResponse(ExecuteCustomReportResponseType value)
/*      */   {
/* 2099 */     this.executeCustomReportResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetVerbiageResponseType getGetVerbiageResponse()
/*      */   {
/* 2111 */     return this.getVerbiageResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setGetVerbiageResponse(GetVerbiageResponseType value)
/*      */   {
/* 2123 */     this.getVerbiageResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public SetVerbiageResponseType getSetVerbiageResponse()
/*      */   {
/* 2135 */     return this.setVerbiageResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSetVerbiageResponse(SetVerbiageResponseType value)
/*      */   {
/* 2147 */     this.setVerbiageResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public SetCustomVerbiageTypeByDivisionResponseType getSetCustomVerbiageTypeByDivisionResponse()
/*      */   {
/* 2159 */     return this.setCustomVerbiageTypeByDivisionResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSetCustomVerbiageTypeByDivisionResponse(SetCustomVerbiageTypeByDivisionResponseType value)
/*      */   {
/* 2171 */     this.setCustomVerbiageTypeByDivisionResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetCustomVerbiageTypeByDivisionResponseType getGetCustomVerbiageTypeByDivisionResponse()
/*      */   {
/* 2183 */     return this.getCustomVerbiageTypeByDivisionResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setGetCustomVerbiageTypeByDivisionResponse(GetCustomVerbiageTypeByDivisionResponseType value)
/*      */   {
/* 2195 */     this.getCustomVerbiageTypeByDivisionResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetLoggedInUserResponseType getGetLoggedInUserResponse()
/*      */   {
/* 2207 */     return this.getLoggedInUserResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setGetLoggedInUserResponse(GetLoggedInUserResponseType value)
/*      */   {
/* 2219 */     this.getLoggedInUserResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public IdConvertResponseType getIdConvertResponse()
/*      */   {
/* 2231 */     return this.idConvertResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setIdConvertResponse(IdConvertResponseType value)
/*      */   {
/* 2243 */     this.idConvertResponse = value;
/*      */   }
/*      */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\ResponseType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */