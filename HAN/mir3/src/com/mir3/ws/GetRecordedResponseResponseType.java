/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="getRecordedResponseResponseType", propOrder={"recordedResponse"})
/*    */ public class GetRecordedResponseResponseType
/*    */ {
/*    */   @XmlElement(required=true)
/*    */   protected RecordedResponseType recordedResponse;
/*    */   
/*    */   public RecordedResponseType getRecordedResponse()
/*    */   {
/* 47 */     return this.recordedResponse;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setRecordedResponse(RecordedResponseType value)
/*    */   {
/* 59 */     this.recordedResponse = value;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\GetRecordedResponseResponseType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */