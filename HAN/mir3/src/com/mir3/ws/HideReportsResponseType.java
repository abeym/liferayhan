/*     */ package com.mir3.ws;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="hideReportsResponseType", propOrder={"notificationReportId", "notificationReportUUID", "notificationGroupReportId", "notificationGroupReportUUID"})
/*     */ public class HideReportsResponseType
/*     */ {
/*     */   @XmlElement(type=Integer.class)
/*     */   protected List<Integer> notificationReportId;
/*     */   protected List<String> notificationReportUUID;
/*     */   @XmlElement(type=Integer.class)
/*     */   protected List<Integer> notificationGroupReportId;
/*     */   protected List<String> notificationGroupReportUUID;
/*     */   
/*     */   public List<Integer> getNotificationReportId()
/*     */   {
/*  73 */     if (this.notificationReportId == null) {
/*  74 */       this.notificationReportId = new ArrayList();
/*     */     }
/*  76 */     return this.notificationReportId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getNotificationReportUUID()
/*     */   {
/* 102 */     if (this.notificationReportUUID == null) {
/* 103 */       this.notificationReportUUID = new ArrayList();
/*     */     }
/* 105 */     return this.notificationReportUUID;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<Integer> getNotificationGroupReportId()
/*     */   {
/* 131 */     if (this.notificationGroupReportId == null) {
/* 132 */       this.notificationGroupReportId = new ArrayList();
/*     */     }
/* 134 */     return this.notificationGroupReportId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getNotificationGroupReportUUID()
/*     */   {
/* 160 */     if (this.notificationGroupReportUUID == null) {
/* 161 */       this.notificationGroupReportUUID = new ArrayList();
/*     */     }
/* 163 */     return this.notificationGroupReportUUID;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\HideReportsResponseType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */