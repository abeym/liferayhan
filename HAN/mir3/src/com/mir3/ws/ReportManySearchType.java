/*     */ package com.mir3.ws;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlSchemaType;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ import javax.xml.datatype.XMLGregorianCalendar;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="reportManySearchType", propOrder={"and", "or", "not", "title", "division", "initiatorUserId", "initiatorUserUUID", "initiatorFirstName", "initiatorLastName", "recipientFirstName", "recipientLastName", "initiatedBefore", "initiatedAfter", "messageText", "completed", "catSubcat", "priorityTopic", "severity", "employeeId", "userId", "userUUID"})
/*     */ public class ReportManySearchType
/*     */ {
/*     */   protected List<ReportManySearchType> and;
/*     */   protected List<ReportManySearchType> or;
/*     */   protected List<ReportOneSearchType> not;
/*     */   protected List<String> title;
/*     */   protected List<String> division;
/*     */   @XmlElement(type=Integer.class)
/*     */   protected List<Integer> initiatorUserId;
/*     */   protected List<String> initiatorUserUUID;
/*     */   protected List<String> initiatorFirstName;
/*     */   protected List<String> initiatorLastName;
/*     */   protected List<String> recipientFirstName;
/*     */   protected List<String> recipientLastName;
/*     */   @XmlSchemaType(name="dateTime")
/*     */   protected List<XMLGregorianCalendar> initiatedBefore;
/*     */   @XmlSchemaType(name="dateTime")
/*     */   protected List<XMLGregorianCalendar> initiatedAfter;
/*     */   protected List<String> messageText;
/*     */   @XmlElement(type=Boolean.class)
/*     */   protected List<Boolean> completed;
/*     */   protected List<CatSubcatType> catSubcat;
/*     */   protected List<String> priorityTopic;
/*     */   protected List<String> severity;
/*     */   protected List<String> employeeId;
/*     */   @XmlElement(type=Integer.class)
/*     */   protected List<Integer> userId;
/*     */   protected List<String> userUUID;
/*     */   
/*     */   public List<ReportManySearchType> getAnd()
/*     */   {
/* 129 */     if (this.and == null) {
/* 130 */       this.and = new ArrayList();
/*     */     }
/* 132 */     return this.and;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<ReportManySearchType> getOr()
/*     */   {
/* 158 */     if (this.or == null) {
/* 159 */       this.or = new ArrayList();
/*     */     }
/* 161 */     return this.or;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<ReportOneSearchType> getNot()
/*     */   {
/* 187 */     if (this.not == null) {
/* 188 */       this.not = new ArrayList();
/*     */     }
/* 190 */     return this.not;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getTitle()
/*     */   {
/* 216 */     if (this.title == null) {
/* 217 */       this.title = new ArrayList();
/*     */     }
/* 219 */     return this.title;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getDivision()
/*     */   {
/* 245 */     if (this.division == null) {
/* 246 */       this.division = new ArrayList();
/*     */     }
/* 248 */     return this.division;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<Integer> getInitiatorUserId()
/*     */   {
/* 274 */     if (this.initiatorUserId == null) {
/* 275 */       this.initiatorUserId = new ArrayList();
/*     */     }
/* 277 */     return this.initiatorUserId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getInitiatorUserUUID()
/*     */   {
/* 303 */     if (this.initiatorUserUUID == null) {
/* 304 */       this.initiatorUserUUID = new ArrayList();
/*     */     }
/* 306 */     return this.initiatorUserUUID;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getInitiatorFirstName()
/*     */   {
/* 332 */     if (this.initiatorFirstName == null) {
/* 333 */       this.initiatorFirstName = new ArrayList();
/*     */     }
/* 335 */     return this.initiatorFirstName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getInitiatorLastName()
/*     */   {
/* 361 */     if (this.initiatorLastName == null) {
/* 362 */       this.initiatorLastName = new ArrayList();
/*     */     }
/* 364 */     return this.initiatorLastName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getRecipientFirstName()
/*     */   {
/* 390 */     if (this.recipientFirstName == null) {
/* 391 */       this.recipientFirstName = new ArrayList();
/*     */     }
/* 393 */     return this.recipientFirstName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getRecipientLastName()
/*     */   {
/* 419 */     if (this.recipientLastName == null) {
/* 420 */       this.recipientLastName = new ArrayList();
/*     */     }
/* 422 */     return this.recipientLastName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<XMLGregorianCalendar> getInitiatedBefore()
/*     */   {
/* 448 */     if (this.initiatedBefore == null) {
/* 449 */       this.initiatedBefore = new ArrayList();
/*     */     }
/* 451 */     return this.initiatedBefore;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<XMLGregorianCalendar> getInitiatedAfter()
/*     */   {
/* 477 */     if (this.initiatedAfter == null) {
/* 478 */       this.initiatedAfter = new ArrayList();
/*     */     }
/* 480 */     return this.initiatedAfter;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getMessageText()
/*     */   {
/* 506 */     if (this.messageText == null) {
/* 507 */       this.messageText = new ArrayList();
/*     */     }
/* 509 */     return this.messageText;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<Boolean> getCompleted()
/*     */   {
/* 535 */     if (this.completed == null) {
/* 536 */       this.completed = new ArrayList();
/*     */     }
/* 538 */     return this.completed;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<CatSubcatType> getCatSubcat()
/*     */   {
/* 564 */     if (this.catSubcat == null) {
/* 565 */       this.catSubcat = new ArrayList();
/*     */     }
/* 567 */     return this.catSubcat;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getPriorityTopic()
/*     */   {
/* 593 */     if (this.priorityTopic == null) {
/* 594 */       this.priorityTopic = new ArrayList();
/*     */     }
/* 596 */     return this.priorityTopic;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getSeverity()
/*     */   {
/* 622 */     if (this.severity == null) {
/* 623 */       this.severity = new ArrayList();
/*     */     }
/* 625 */     return this.severity;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getEmployeeId()
/*     */   {
/* 651 */     if (this.employeeId == null) {
/* 652 */       this.employeeId = new ArrayList();
/*     */     }
/* 654 */     return this.employeeId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<Integer> getUserId()
/*     */   {
/* 680 */     if (this.userId == null) {
/* 681 */       this.userId = new ArrayList();
/*     */     }
/* 683 */     return this.userId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getUserUUID()
/*     */   {
/* 709 */     if (this.userUUID == null) {
/* 710 */       this.userUUID = new ArrayList();
/*     */     }
/* 712 */     return this.userUUID;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\ReportManySearchType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */