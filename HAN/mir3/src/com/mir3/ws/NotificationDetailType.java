/*      */ package com.mir3.ws;
/*      */ 
/*      */ import javax.xml.bind.annotation.XmlAccessType;
/*      */ import javax.xml.bind.annotation.XmlAccessorType;
/*      */ import javax.xml.bind.annotation.XmlElement;
/*      */ import javax.xml.bind.annotation.XmlType;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ @XmlAccessorType(XmlAccessType.FIELD)
/*      */ @XmlType(name="notificationDetailType", propOrder={})
/*      */ public class NotificationDetailType
/*      */ {
/*      */   protected Integer contactCycleDelay;
/*      */   protected Integer contactAttemptCycles;
/*      */   protected Integer textDeviceDelay;
/*      */   protected NotificationPriorityType priority;
/*      */   protected Boolean useAlternates;
/*      */   protected Boolean playGreeting;
/*      */   protected String division;
/*      */   @XmlElement(required=true)
/*      */   protected String title;
/*      */   protected String description;
/*      */   protected String message;
/*      */   protected String recordedMessageTitle;
/*      */   protected VerbiagePerMessageType verbiage;
/*      */   protected Boolean requiresPin;
/*      */   protected BroadcastInfoType broadcastInfo;
/*      */   protected FirstResponseInfoType firstResponseInfo;
/*      */   protected CalloutInfoType calloutInfo;
/*      */   protected OneStepInfoType oneStepInfo;
/*      */   protected BroadcastInfoType smsOptInInfo;
/*      */   protected BroadcastInfoType bulletinBoardInfo;
/*      */   protected LeaveMessageType leaveMessage;
/*      */   protected Boolean validateRecipient;
/*      */   protected Boolean replayMessage;
/*      */   protected Boolean stopIfFullHuman;
/*      */   protected Boolean stopIfPartialHuman;
/*      */   protected Boolean stopIfFullMachine;
/*      */   protected Boolean stopIfPartialMachine;
/*      */   protected Boolean callAnalysis;
/*      */   protected LocationOverrideType locationOverride;
/*      */   protected NotificationResponseOptionsType responseOptions;
/*      */   protected AttachmentSummaryType attachmentSummary;
/*      */   protected AttachmentSummariesType attachmentSummaries;
/*      */   protected Boolean reportRecipientsFlag;
/*      */   protected Boolean selectLanguage;
/*      */   protected NotificationRecipientsDetailsType reportRecipients;
/*      */   protected ScheduledNotificationReportType scheduledNotificationReport;
/*      */   protected String category;
/*      */   protected String subcategory;
/*      */   protected String priorityTopic;
/*      */   protected String severity;
/*      */   protected Boolean useTopics;
/*      */   protected Boolean sendToSubscribers;
/*      */   protected String userData;
/*      */   protected String userUniqueId;
/*      */   protected TimeConstraintType timeConstraint;
/*      */   protected Boolean strictDeviceDelay;
/*      */   protected Boolean textDevicesOnce;
/*      */   protected Boolean identicalDeviceSuppression;
/*      */   protected Boolean useAlias;
/*      */   protected String initiatorAlias;
/*      */   protected Boolean anonymize;
/*      */   protected Boolean callRoutingRollover;
/*      */   protected EmailImportanceType emailImportance;
/*      */   protected Boolean confirmResponse;
/*      */   protected PlaceholdersType placeholders;
/*      */   protected ReportCallbacksType reportCallbacks;
/*      */   protected Boolean currentUserCanViewAllRecipients;
/*      */   protected Boolean currentUserCanViewAllTopics;
/*      */   protected Boolean currentUserCanEditNotification;
/*      */   protected Boolean currentUserCanSendNotification;
/*      */   protected Boolean currentUserCanViewThisNotificationReport;
/*      */   protected Boolean suppressWarningsForEmail;
/*      */   protected DesktopAlertInfosType desktopAlertInfos;
/*      */   
/*      */   public Integer getContactCycleDelay()
/*      */   {
/*  169 */     return this.contactCycleDelay;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setContactCycleDelay(Integer value)
/*      */   {
/*  181 */     this.contactCycleDelay = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Integer getContactAttemptCycles()
/*      */   {
/*  193 */     return this.contactAttemptCycles;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setContactAttemptCycles(Integer value)
/*      */   {
/*  205 */     this.contactAttemptCycles = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Integer getTextDeviceDelay()
/*      */   {
/*  217 */     return this.textDeviceDelay;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTextDeviceDelay(Integer value)
/*      */   {
/*  229 */     this.textDeviceDelay = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public NotificationPriorityType getPriority()
/*      */   {
/*  241 */     return this.priority;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setPriority(NotificationPriorityType value)
/*      */   {
/*  253 */     this.priority = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Boolean isUseAlternates()
/*      */   {
/*  265 */     return this.useAlternates;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setUseAlternates(Boolean value)
/*      */   {
/*  277 */     this.useAlternates = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Boolean isPlayGreeting()
/*      */   {
/*  289 */     return this.playGreeting;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setPlayGreeting(Boolean value)
/*      */   {
/*  301 */     this.playGreeting = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getDivision()
/*      */   {
/*  313 */     return this.division;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setDivision(String value)
/*      */   {
/*  325 */     this.division = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTitle()
/*      */   {
/*  337 */     return this.title;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTitle(String value)
/*      */   {
/*  349 */     this.title = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getDescription()
/*      */   {
/*  361 */     return this.description;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setDescription(String value)
/*      */   {
/*  373 */     this.description = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getMessage()
/*      */   {
/*  385 */     return this.message;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setMessage(String value)
/*      */   {
/*  397 */     this.message = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getRecordedMessageTitle()
/*      */   {
/*  409 */     return this.recordedMessageTitle;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setRecordedMessageTitle(String value)
/*      */   {
/*  421 */     this.recordedMessageTitle = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public VerbiagePerMessageType getVerbiage()
/*      */   {
/*  433 */     return this.verbiage;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setVerbiage(VerbiagePerMessageType value)
/*      */   {
/*  445 */     this.verbiage = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Boolean isRequiresPin()
/*      */   {
/*  457 */     return this.requiresPin;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setRequiresPin(Boolean value)
/*      */   {
/*  469 */     this.requiresPin = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public BroadcastInfoType getBroadcastInfo()
/*      */   {
/*  481 */     return this.broadcastInfo;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setBroadcastInfo(BroadcastInfoType value)
/*      */   {
/*  493 */     this.broadcastInfo = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public FirstResponseInfoType getFirstResponseInfo()
/*      */   {
/*  505 */     return this.firstResponseInfo;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setFirstResponseInfo(FirstResponseInfoType value)
/*      */   {
/*  517 */     this.firstResponseInfo = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public CalloutInfoType getCalloutInfo()
/*      */   {
/*  529 */     return this.calloutInfo;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setCalloutInfo(CalloutInfoType value)
/*      */   {
/*  541 */     this.calloutInfo = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public OneStepInfoType getOneStepInfo()
/*      */   {
/*  553 */     return this.oneStepInfo;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setOneStepInfo(OneStepInfoType value)
/*      */   {
/*  565 */     this.oneStepInfo = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public BroadcastInfoType getSmsOptInInfo()
/*      */   {
/*  577 */     return this.smsOptInInfo;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSmsOptInInfo(BroadcastInfoType value)
/*      */   {
/*  589 */     this.smsOptInInfo = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public BroadcastInfoType getBulletinBoardInfo()
/*      */   {
/*  601 */     return this.bulletinBoardInfo;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setBulletinBoardInfo(BroadcastInfoType value)
/*      */   {
/*  613 */     this.bulletinBoardInfo = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public LeaveMessageType getLeaveMessage()
/*      */   {
/*  625 */     return this.leaveMessage;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setLeaveMessage(LeaveMessageType value)
/*      */   {
/*  637 */     this.leaveMessage = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Boolean isValidateRecipient()
/*      */   {
/*  649 */     return this.validateRecipient;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setValidateRecipient(Boolean value)
/*      */   {
/*  661 */     this.validateRecipient = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Boolean isReplayMessage()
/*      */   {
/*  673 */     return this.replayMessage;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setReplayMessage(Boolean value)
/*      */   {
/*  685 */     this.replayMessage = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Boolean isStopIfFullHuman()
/*      */   {
/*  697 */     return this.stopIfFullHuman;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setStopIfFullHuman(Boolean value)
/*      */   {
/*  709 */     this.stopIfFullHuman = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Boolean isStopIfPartialHuman()
/*      */   {
/*  721 */     return this.stopIfPartialHuman;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setStopIfPartialHuman(Boolean value)
/*      */   {
/*  733 */     this.stopIfPartialHuman = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Boolean isStopIfFullMachine()
/*      */   {
/*  745 */     return this.stopIfFullMachine;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setStopIfFullMachine(Boolean value)
/*      */   {
/*  757 */     this.stopIfFullMachine = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Boolean isStopIfPartialMachine()
/*      */   {
/*  769 */     return this.stopIfPartialMachine;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setStopIfPartialMachine(Boolean value)
/*      */   {
/*  781 */     this.stopIfPartialMachine = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Boolean isCallAnalysis()
/*      */   {
/*  793 */     return this.callAnalysis;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setCallAnalysis(Boolean value)
/*      */   {
/*  805 */     this.callAnalysis = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public LocationOverrideType getLocationOverride()
/*      */   {
/*  817 */     return this.locationOverride;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setLocationOverride(LocationOverrideType value)
/*      */   {
/*  829 */     this.locationOverride = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public NotificationResponseOptionsType getResponseOptions()
/*      */   {
/*  841 */     return this.responseOptions;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setResponseOptions(NotificationResponseOptionsType value)
/*      */   {
/*  853 */     this.responseOptions = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public AttachmentSummaryType getAttachmentSummary()
/*      */   {
/*  865 */     return this.attachmentSummary;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setAttachmentSummary(AttachmentSummaryType value)
/*      */   {
/*  877 */     this.attachmentSummary = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public AttachmentSummariesType getAttachmentSummaries()
/*      */   {
/*  889 */     return this.attachmentSummaries;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setAttachmentSummaries(AttachmentSummariesType value)
/*      */   {
/*  901 */     this.attachmentSummaries = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Boolean isReportRecipientsFlag()
/*      */   {
/*  913 */     return this.reportRecipientsFlag;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setReportRecipientsFlag(Boolean value)
/*      */   {
/*  925 */     this.reportRecipientsFlag = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Boolean isSelectLanguage()
/*      */   {
/*  937 */     return this.selectLanguage;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSelectLanguage(Boolean value)
/*      */   {
/*  949 */     this.selectLanguage = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public NotificationRecipientsDetailsType getReportRecipients()
/*      */   {
/*  961 */     return this.reportRecipients;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setReportRecipients(NotificationRecipientsDetailsType value)
/*      */   {
/*  973 */     this.reportRecipients = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public ScheduledNotificationReportType getScheduledNotificationReport()
/*      */   {
/*  985 */     return this.scheduledNotificationReport;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setScheduledNotificationReport(ScheduledNotificationReportType value)
/*      */   {
/*  997 */     this.scheduledNotificationReport = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getCategory()
/*      */   {
/* 1009 */     return this.category;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setCategory(String value)
/*      */   {
/* 1021 */     this.category = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getSubcategory()
/*      */   {
/* 1033 */     return this.subcategory;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSubcategory(String value)
/*      */   {
/* 1045 */     this.subcategory = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getPriorityTopic()
/*      */   {
/* 1057 */     return this.priorityTopic;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setPriorityTopic(String value)
/*      */   {
/* 1069 */     this.priorityTopic = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getSeverity()
/*      */   {
/* 1081 */     return this.severity;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSeverity(String value)
/*      */   {
/* 1093 */     this.severity = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Boolean isUseTopics()
/*      */   {
/* 1105 */     return this.useTopics;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setUseTopics(Boolean value)
/*      */   {
/* 1117 */     this.useTopics = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Boolean isSendToSubscribers()
/*      */   {
/* 1129 */     return this.sendToSubscribers;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSendToSubscribers(Boolean value)
/*      */   {
/* 1141 */     this.sendToSubscribers = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getUserData()
/*      */   {
/* 1153 */     return this.userData;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setUserData(String value)
/*      */   {
/* 1165 */     this.userData = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getUserUniqueId()
/*      */   {
/* 1177 */     return this.userUniqueId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setUserUniqueId(String value)
/*      */   {
/* 1189 */     this.userUniqueId = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public TimeConstraintType getTimeConstraint()
/*      */   {
/* 1201 */     return this.timeConstraint;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTimeConstraint(TimeConstraintType value)
/*      */   {
/* 1213 */     this.timeConstraint = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Boolean isStrictDeviceDelay()
/*      */   {
/* 1225 */     return this.strictDeviceDelay;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setStrictDeviceDelay(Boolean value)
/*      */   {
/* 1237 */     this.strictDeviceDelay = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Boolean isTextDevicesOnce()
/*      */   {
/* 1249 */     return this.textDevicesOnce;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTextDevicesOnce(Boolean value)
/*      */   {
/* 1261 */     this.textDevicesOnce = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Boolean isIdenticalDeviceSuppression()
/*      */   {
/* 1273 */     return this.identicalDeviceSuppression;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setIdenticalDeviceSuppression(Boolean value)
/*      */   {
/* 1285 */     this.identicalDeviceSuppression = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Boolean isUseAlias()
/*      */   {
/* 1297 */     return this.useAlias;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setUseAlias(Boolean value)
/*      */   {
/* 1309 */     this.useAlias = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getInitiatorAlias()
/*      */   {
/* 1321 */     return this.initiatorAlias;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setInitiatorAlias(String value)
/*      */   {
/* 1333 */     this.initiatorAlias = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Boolean isAnonymize()
/*      */   {
/* 1345 */     return this.anonymize;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setAnonymize(Boolean value)
/*      */   {
/* 1357 */     this.anonymize = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Boolean isCallRoutingRollover()
/*      */   {
/* 1369 */     return this.callRoutingRollover;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setCallRoutingRollover(Boolean value)
/*      */   {
/* 1381 */     this.callRoutingRollover = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public EmailImportanceType getEmailImportance()
/*      */   {
/* 1393 */     return this.emailImportance;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setEmailImportance(EmailImportanceType value)
/*      */   {
/* 1405 */     this.emailImportance = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Boolean isConfirmResponse()
/*      */   {
/* 1417 */     return this.confirmResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setConfirmResponse(Boolean value)
/*      */   {
/* 1429 */     this.confirmResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public PlaceholdersType getPlaceholders()
/*      */   {
/* 1441 */     return this.placeholders;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setPlaceholders(PlaceholdersType value)
/*      */   {
/* 1453 */     this.placeholders = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public ReportCallbacksType getReportCallbacks()
/*      */   {
/* 1465 */     return this.reportCallbacks;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setReportCallbacks(ReportCallbacksType value)
/*      */   {
/* 1477 */     this.reportCallbacks = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Boolean isCurrentUserCanViewAllRecipients()
/*      */   {
/* 1489 */     return this.currentUserCanViewAllRecipients;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setCurrentUserCanViewAllRecipients(Boolean value)
/*      */   {
/* 1501 */     this.currentUserCanViewAllRecipients = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Boolean isCurrentUserCanViewAllTopics()
/*      */   {
/* 1513 */     return this.currentUserCanViewAllTopics;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setCurrentUserCanViewAllTopics(Boolean value)
/*      */   {
/* 1525 */     this.currentUserCanViewAllTopics = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Boolean isCurrentUserCanEditNotification()
/*      */   {
/* 1537 */     return this.currentUserCanEditNotification;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setCurrentUserCanEditNotification(Boolean value)
/*      */   {
/* 1549 */     this.currentUserCanEditNotification = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Boolean isCurrentUserCanSendNotification()
/*      */   {
/* 1561 */     return this.currentUserCanSendNotification;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setCurrentUserCanSendNotification(Boolean value)
/*      */   {
/* 1573 */     this.currentUserCanSendNotification = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Boolean isCurrentUserCanViewThisNotificationReport()
/*      */   {
/* 1585 */     return this.currentUserCanViewThisNotificationReport;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setCurrentUserCanViewThisNotificationReport(Boolean value)
/*      */   {
/* 1597 */     this.currentUserCanViewThisNotificationReport = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Boolean isSuppressWarningsForEmail()
/*      */   {
/* 1609 */     return this.suppressWarningsForEmail;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSuppressWarningsForEmail(Boolean value)
/*      */   {
/* 1621 */     this.suppressWarningsForEmail = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DesktopAlertInfosType getDesktopAlertInfos()
/*      */   {
/* 1633 */     return this.desktopAlertInfos;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setDesktopAlertInfos(DesktopAlertInfosType value)
/*      */   {
/* 1645 */     this.desktopAlertInfos = value;
/*      */   }
/*      */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\NotificationDetailType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */