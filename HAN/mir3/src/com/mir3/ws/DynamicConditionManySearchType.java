/*      */ package com.mir3.ws;
/*      */ 
/*      */ import java.util.ArrayList;
/*      */ import java.util.List;
/*      */ import javax.xml.bind.annotation.XmlAccessType;
/*      */ import javax.xml.bind.annotation.XmlAccessorType;
/*      */ import javax.xml.bind.annotation.XmlElement;
/*      */ import javax.xml.bind.annotation.XmlType;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ @XmlAccessorType(XmlAccessType.FIELD)
/*      */ @XmlType(name="dynamicConditionManySearchType", propOrder={"and", "or", "not", "addressTypeName", "address1", "address2", "building", "blackberry", "city", "company", "country", "disabledDevice", "division", "email", "employeeId", "floor", "facility", "fax", "firstName", "firstOrLast", "groupName", "groupId", "groupUUID", "lastName", "loginEnabled", "pager", "phoneNumber", "province", "role", "scheduleName", "sms", "state", "title", "username", "userId", "userUUID", "zip", "customField", "catSubcat", "priority", "severity", "roleTemplate", "timeZone", "smsOptInStatus"})
/*      */ public class DynamicConditionManySearchType
/*      */ {
/*      */   protected List<DynamicConditionManySearchType> and;
/*      */   protected List<DynamicConditionManySearchType> or;
/*      */   protected List<DynamicConditionSearchType> not;
/*      */   protected List<String> addressTypeName;
/*      */   protected List<String> address1;
/*      */   protected List<String> address2;
/*      */   protected List<String> building;
/*      */   protected List<String> blackberry;
/*      */   protected List<String> city;
/*      */   protected List<String> company;
/*      */   protected List<String> country;
/*      */   @XmlElement(type=Boolean.class)
/*      */   protected List<Boolean> disabledDevice;
/*      */   protected List<String> division;
/*      */   protected List<String> email;
/*      */   protected List<String> employeeId;
/*      */   protected List<String> floor;
/*      */   protected List<String> facility;
/*      */   protected List<String> fax;
/*      */   protected List<String> firstName;
/*      */   protected List<String> firstOrLast;
/*      */   protected List<String> groupName;
/*      */   @XmlElement(type=Integer.class)
/*      */   protected List<Integer> groupId;
/*      */   protected List<String> groupUUID;
/*      */   protected List<String> lastName;
/*      */   @XmlElement(type=Boolean.class)
/*      */   protected List<Boolean> loginEnabled;
/*      */   protected List<String> pager;
/*      */   protected List<String> phoneNumber;
/*      */   protected List<String> province;
/*      */   protected List<String> role;
/*      */   protected List<String> scheduleName;
/*      */   protected List<String> sms;
/*      */   protected List<String> state;
/*      */   protected List<String> title;
/*      */   protected List<String> username;
/*      */   @XmlElement(type=Integer.class)
/*      */   protected List<Integer> userId;
/*      */   protected List<String> userUUID;
/*      */   protected List<String> zip;
/*      */   protected List<CustomFieldType> customField;
/*      */   protected List<CatSubcatType> catSubcat;
/*      */   protected List<String> priority;
/*      */   protected List<String> severity;
/*      */   protected List<String> roleTemplate;
/*      */   protected List<TimeZoneType> timeZone;
/*      */   protected List<SmsOptInStatusType> smsOptInStatus;
/*      */   
/*      */   public List<DynamicConditionManySearchType> getAnd()
/*      */   {
/*  195 */     if (this.and == null) {
/*  196 */       this.and = new ArrayList();
/*      */     }
/*  198 */     return this.and;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<DynamicConditionManySearchType> getOr()
/*      */   {
/*  224 */     if (this.or == null) {
/*  225 */       this.or = new ArrayList();
/*      */     }
/*  227 */     return this.or;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<DynamicConditionSearchType> getNot()
/*      */   {
/*  253 */     if (this.not == null) {
/*  254 */       this.not = new ArrayList();
/*      */     }
/*  256 */     return this.not;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getAddressTypeName()
/*      */   {
/*  282 */     if (this.addressTypeName == null) {
/*  283 */       this.addressTypeName = new ArrayList();
/*      */     }
/*  285 */     return this.addressTypeName;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getAddress1()
/*      */   {
/*  311 */     if (this.address1 == null) {
/*  312 */       this.address1 = new ArrayList();
/*      */     }
/*  314 */     return this.address1;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getAddress2()
/*      */   {
/*  340 */     if (this.address2 == null) {
/*  341 */       this.address2 = new ArrayList();
/*      */     }
/*  343 */     return this.address2;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getBuilding()
/*      */   {
/*  369 */     if (this.building == null) {
/*  370 */       this.building = new ArrayList();
/*      */     }
/*  372 */     return this.building;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getBlackberry()
/*      */   {
/*  398 */     if (this.blackberry == null) {
/*  399 */       this.blackberry = new ArrayList();
/*      */     }
/*  401 */     return this.blackberry;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getCity()
/*      */   {
/*  427 */     if (this.city == null) {
/*  428 */       this.city = new ArrayList();
/*      */     }
/*  430 */     return this.city;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getCompany()
/*      */   {
/*  456 */     if (this.company == null) {
/*  457 */       this.company = new ArrayList();
/*      */     }
/*  459 */     return this.company;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getCountry()
/*      */   {
/*  485 */     if (this.country == null) {
/*  486 */       this.country = new ArrayList();
/*      */     }
/*  488 */     return this.country;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<Boolean> getDisabledDevice()
/*      */   {
/*  514 */     if (this.disabledDevice == null) {
/*  515 */       this.disabledDevice = new ArrayList();
/*      */     }
/*  517 */     return this.disabledDevice;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getDivision()
/*      */   {
/*  543 */     if (this.division == null) {
/*  544 */       this.division = new ArrayList();
/*      */     }
/*  546 */     return this.division;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getEmail()
/*      */   {
/*  572 */     if (this.email == null) {
/*  573 */       this.email = new ArrayList();
/*      */     }
/*  575 */     return this.email;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getEmployeeId()
/*      */   {
/*  601 */     if (this.employeeId == null) {
/*  602 */       this.employeeId = new ArrayList();
/*      */     }
/*  604 */     return this.employeeId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getFloor()
/*      */   {
/*  630 */     if (this.floor == null) {
/*  631 */       this.floor = new ArrayList();
/*      */     }
/*  633 */     return this.floor;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getFacility()
/*      */   {
/*  659 */     if (this.facility == null) {
/*  660 */       this.facility = new ArrayList();
/*      */     }
/*  662 */     return this.facility;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getFax()
/*      */   {
/*  688 */     if (this.fax == null) {
/*  689 */       this.fax = new ArrayList();
/*      */     }
/*  691 */     return this.fax;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getFirstName()
/*      */   {
/*  717 */     if (this.firstName == null) {
/*  718 */       this.firstName = new ArrayList();
/*      */     }
/*  720 */     return this.firstName;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getFirstOrLast()
/*      */   {
/*  746 */     if (this.firstOrLast == null) {
/*  747 */       this.firstOrLast = new ArrayList();
/*      */     }
/*  749 */     return this.firstOrLast;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getGroupName()
/*      */   {
/*  775 */     if (this.groupName == null) {
/*  776 */       this.groupName = new ArrayList();
/*      */     }
/*  778 */     return this.groupName;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<Integer> getGroupId()
/*      */   {
/*  804 */     if (this.groupId == null) {
/*  805 */       this.groupId = new ArrayList();
/*      */     }
/*  807 */     return this.groupId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getGroupUUID()
/*      */   {
/*  833 */     if (this.groupUUID == null) {
/*  834 */       this.groupUUID = new ArrayList();
/*      */     }
/*  836 */     return this.groupUUID;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getLastName()
/*      */   {
/*  862 */     if (this.lastName == null) {
/*  863 */       this.lastName = new ArrayList();
/*      */     }
/*  865 */     return this.lastName;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<Boolean> getLoginEnabled()
/*      */   {
/*  891 */     if (this.loginEnabled == null) {
/*  892 */       this.loginEnabled = new ArrayList();
/*      */     }
/*  894 */     return this.loginEnabled;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getPager()
/*      */   {
/*  920 */     if (this.pager == null) {
/*  921 */       this.pager = new ArrayList();
/*      */     }
/*  923 */     return this.pager;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getPhoneNumber()
/*      */   {
/*  949 */     if (this.phoneNumber == null) {
/*  950 */       this.phoneNumber = new ArrayList();
/*      */     }
/*  952 */     return this.phoneNumber;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getProvince()
/*      */   {
/*  978 */     if (this.province == null) {
/*  979 */       this.province = new ArrayList();
/*      */     }
/*  981 */     return this.province;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getRole()
/*      */   {
/* 1007 */     if (this.role == null) {
/* 1008 */       this.role = new ArrayList();
/*      */     }
/* 1010 */     return this.role;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getScheduleName()
/*      */   {
/* 1036 */     if (this.scheduleName == null) {
/* 1037 */       this.scheduleName = new ArrayList();
/*      */     }
/* 1039 */     return this.scheduleName;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getSms()
/*      */   {
/* 1065 */     if (this.sms == null) {
/* 1066 */       this.sms = new ArrayList();
/*      */     }
/* 1068 */     return this.sms;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getState()
/*      */   {
/* 1094 */     if (this.state == null) {
/* 1095 */       this.state = new ArrayList();
/*      */     }
/* 1097 */     return this.state;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getTitle()
/*      */   {
/* 1123 */     if (this.title == null) {
/* 1124 */       this.title = new ArrayList();
/*      */     }
/* 1126 */     return this.title;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getUsername()
/*      */   {
/* 1152 */     if (this.username == null) {
/* 1153 */       this.username = new ArrayList();
/*      */     }
/* 1155 */     return this.username;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<Integer> getUserId()
/*      */   {
/* 1181 */     if (this.userId == null) {
/* 1182 */       this.userId = new ArrayList();
/*      */     }
/* 1184 */     return this.userId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getUserUUID()
/*      */   {
/* 1210 */     if (this.userUUID == null) {
/* 1211 */       this.userUUID = new ArrayList();
/*      */     }
/* 1213 */     return this.userUUID;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getZip()
/*      */   {
/* 1239 */     if (this.zip == null) {
/* 1240 */       this.zip = new ArrayList();
/*      */     }
/* 1242 */     return this.zip;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<CustomFieldType> getCustomField()
/*      */   {
/* 1268 */     if (this.customField == null) {
/* 1269 */       this.customField = new ArrayList();
/*      */     }
/* 1271 */     return this.customField;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<CatSubcatType> getCatSubcat()
/*      */   {
/* 1297 */     if (this.catSubcat == null) {
/* 1298 */       this.catSubcat = new ArrayList();
/*      */     }
/* 1300 */     return this.catSubcat;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getPriority()
/*      */   {
/* 1326 */     if (this.priority == null) {
/* 1327 */       this.priority = new ArrayList();
/*      */     }
/* 1329 */     return this.priority;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getSeverity()
/*      */   {
/* 1355 */     if (this.severity == null) {
/* 1356 */       this.severity = new ArrayList();
/*      */     }
/* 1358 */     return this.severity;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getRoleTemplate()
/*      */   {
/* 1384 */     if (this.roleTemplate == null) {
/* 1385 */       this.roleTemplate = new ArrayList();
/*      */     }
/* 1387 */     return this.roleTemplate;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<TimeZoneType> getTimeZone()
/*      */   {
/* 1413 */     if (this.timeZone == null) {
/* 1414 */       this.timeZone = new ArrayList();
/*      */     }
/* 1416 */     return this.timeZone;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<SmsOptInStatusType> getSmsOptInStatus()
/*      */   {
/* 1442 */     if (this.smsOptInStatus == null) {
/* 1443 */       this.smsOptInStatus = new ArrayList();
/*      */     }
/* 1445 */     return this.smsOptInStatus;
/*      */   }
/*      */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\DynamicConditionManySearchType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */