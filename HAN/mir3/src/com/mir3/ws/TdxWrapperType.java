/*    */ package com.mir3.ws;
/*    */ 
/*    */ import java.util.ArrayList;
/*    */ import java.util.List;
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="tdxWrapperType", propOrder={"requestHeader", "wrappedRequest"})
/*    */ public class TdxWrapperType
/*    */ {
/*    */   protected List<String> requestHeader;
/*    */   @XmlElement(required=true)
/*    */   protected WrappedRequestType wrappedRequest;
/*    */   
/*    */   public List<String> getRequestHeader()
/*    */   {
/* 66 */     if (this.requestHeader == null) {
/* 67 */       this.requestHeader = new ArrayList();
/*    */     }
/* 69 */     return this.requestHeader;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public WrappedRequestType getWrappedRequest()
/*    */   {
/* 81 */     return this.wrappedRequest;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setWrappedRequest(WrappedRequestType value)
/*    */   {
/* 93 */     this.wrappedRequest = value;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\TdxWrapperType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */