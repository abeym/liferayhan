/*     */ package com.mir3.ws;
/*     */ 
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="incidentStatusType", propOrder={"incidentTitle", "status", "updateInformation"})
/*     */ public class IncidentStatusType
/*     */ {
/*     */   @XmlElement(required=true)
/*     */   protected String incidentTitle;
/*     */   @XmlElement(required=true)
/*     */   protected IncidentStatusEnumType status;
/*     */   protected String updateInformation;
/*     */   
/*     */   public String getIncidentTitle()
/*     */   {
/*  54 */     return this.incidentTitle;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setIncidentTitle(String value)
/*     */   {
/*  66 */     this.incidentTitle = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public IncidentStatusEnumType getStatus()
/*     */   {
/*  78 */     return this.status;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setStatus(IncidentStatusEnumType value)
/*     */   {
/*  90 */     this.status = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getUpdateInformation()
/*     */   {
/* 102 */     return this.updateInformation;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setUpdateInformation(String value)
/*     */   {
/* 114 */     this.updateInformation = value;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\IncidentStatusType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */