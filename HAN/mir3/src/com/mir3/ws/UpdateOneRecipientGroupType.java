/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="updateOneRecipientGroupType", propOrder={})
/*    */ public class UpdateOneRecipientGroupType
/*    */ {
/*    */   @XmlElement(required=true)
/*    */   protected String recipientGroup;
/*    */   @XmlElement(required=true)
/*    */   protected RecipientGroupDetailType recipientGroupDetail;
/*    */   
/*    */   public String getRecipientGroup()
/*    */   {
/* 50 */     return this.recipientGroup;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setRecipientGroup(String value)
/*    */   {
/* 62 */     this.recipientGroup = value;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public RecipientGroupDetailType getRecipientGroupDetail()
/*    */   {
/* 74 */     return this.recipientGroupDetail;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setRecipientGroupDetail(RecipientGroupDetailType value)
/*    */   {
/* 86 */     this.recipientGroupDetail = value;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\UpdateOneRecipientGroupType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */