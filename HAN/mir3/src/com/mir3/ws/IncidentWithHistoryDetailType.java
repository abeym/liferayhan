/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="incidentWithHistoryDetailType", propOrder={"incident", "history"})
/*    */ public class IncidentWithHistoryDetailType
/*    */ {
/*    */   @XmlElement(required=true)
/*    */   protected IncidentDetailType incident;
/*    */   protected IncidentHistoryType history;
/*    */   
/*    */   public IncidentDetailType getIncident()
/*    */   {
/* 50 */     return this.incident;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setIncident(IncidentDetailType value)
/*    */   {
/* 62 */     this.incident = value;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public IncidentHistoryType getHistory()
/*    */   {
/* 74 */     return this.history;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setHistory(IncidentHistoryType value)
/*    */   {
/* 86 */     this.history = value;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\IncidentWithHistoryDetailType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */