/*    */ package com.mir3.ws;
/*    */ 
/*    */ import java.util.ArrayList;
/*    */ import java.util.List;
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="getDynamicGroupMembersResponseType", propOrder={"dynamicGroupDetail"})
/*    */ public class GetDynamicGroupMembersResponseType
/*    */ {
/*    */   protected List<ResolvedDynamicGroupDetailType> dynamicGroupDetail;
/*    */   
/*    */   public List<ResolvedDynamicGroupDetailType> getDynamicGroupDetail()
/*    */   {
/* 61 */     if (this.dynamicGroupDetail == null) {
/* 62 */       this.dynamicGroupDetail = new ArrayList();
/*    */     }
/* 64 */     return this.dynamicGroupDetail;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\GetDynamicGroupMembersResponseType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */