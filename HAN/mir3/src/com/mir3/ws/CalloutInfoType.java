/*     */ package com.mir3.ws;
/*     */ 
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="calloutInfoType", propOrder={})
/*     */ public class CalloutInfoType
/*     */ {
/*     */   protected int callOutSuccessTotal;
/*     */   protected Integer callOutWindow;
/*     */   protected Boolean contactAll;
/*     */   protected Boolean notificationClosedNotice;
/*     */   protected CallingStrategyType callingStrategy;
/*     */   protected CallingOrderType callingOrder;
/*     */   @XmlElement(required=true)
/*     */   protected NotificationEscalationsType escalations;
/*     */   
/*     */   public int getCallOutSuccessTotal()
/*     */   {
/*  55 */     return this.callOutSuccessTotal;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCallOutSuccessTotal(int value)
/*     */   {
/*  63 */     this.callOutSuccessTotal = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getCallOutWindow()
/*     */   {
/*  75 */     return this.callOutWindow;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCallOutWindow(Integer value)
/*     */   {
/*  87 */     this.callOutWindow = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Boolean isContactAll()
/*     */   {
/*  99 */     return this.contactAll;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setContactAll(Boolean value)
/*     */   {
/* 111 */     this.contactAll = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Boolean isNotificationClosedNotice()
/*     */   {
/* 123 */     return this.notificationClosedNotice;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setNotificationClosedNotice(Boolean value)
/*     */   {
/* 135 */     this.notificationClosedNotice = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public CallingStrategyType getCallingStrategy()
/*     */   {
/* 147 */     return this.callingStrategy;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCallingStrategy(CallingStrategyType value)
/*     */   {
/* 159 */     this.callingStrategy = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public CallingOrderType getCallingOrder()
/*     */   {
/* 171 */     return this.callingOrder;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCallingOrder(CallingOrderType value)
/*     */   {
/* 183 */     this.callingOrder = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public NotificationEscalationsType getEscalations()
/*     */   {
/* 195 */     return this.escalations;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setEscalations(NotificationEscalationsType value)
/*     */   {
/* 207 */     this.escalations = value;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\CalloutInfoType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */