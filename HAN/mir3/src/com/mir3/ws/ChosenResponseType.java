/*     */ package com.mir3.ws;
/*     */ 
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="chosenResponseType", propOrder={"option", "freeForm", "recordedResponse"})
/*     */ public class ChosenResponseType
/*     */ {
/*     */   protected Integer option;
/*     */   protected String freeForm;
/*     */   protected byte[] recordedResponse;
/*     */   
/*     */   public Integer getOption()
/*     */   {
/*  51 */     return this.option;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setOption(Integer value)
/*     */   {
/*  63 */     this.option = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getFreeForm()
/*     */   {
/*  75 */     return this.freeForm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setFreeForm(String value)
/*     */   {
/*  87 */     this.freeForm = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public byte[] getRecordedResponse()
/*     */   {
/*  98 */     return this.recordedResponse;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRecordedResponse(byte[] value)
/*     */   {
/* 109 */     this.recordedResponse = value;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\ChosenResponseType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */