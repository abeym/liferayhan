/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlEnum;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlType(name="customVerbiageTypeDivision")
/*    */ @XmlEnum
/*    */ public enum CustomVerbiageTypeDivision
/*    */ {
/* 28 */   INITIATORDIVISION, 
/* 29 */   RECIPIENTDIVISION, 
/* 30 */   NOTIFICATIONDIVISION;
/*    */   
/*    */   public String value() {
/* 33 */     return name();
/*    */   }
/*    */   
/*    */   public static CustomVerbiageTypeDivision fromValue(String v) {
/* 37 */     return valueOf(v);
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\CustomVerbiageTypeDivision.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */