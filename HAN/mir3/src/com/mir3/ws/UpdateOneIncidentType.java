/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="updateOneIncidentType", propOrder={"incidentTitle", "incident"})
/*    */ public class UpdateOneIncidentType
/*    */ {
/*    */   @XmlElement(required=true)
/*    */   protected String incidentTitle;
/*    */   @XmlElement(required=true)
/*    */   protected IncidentDetailType incident;
/*    */   
/*    */   public String getIncidentTitle()
/*    */   {
/* 51 */     return this.incidentTitle;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setIncidentTitle(String value)
/*    */   {
/* 63 */     this.incidentTitle = value;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public IncidentDetailType getIncident()
/*    */   {
/* 75 */     return this.incident;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setIncident(IncidentDetailType value)
/*    */   {
/* 87 */     this.incident = value;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\UpdateOneIncidentType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */