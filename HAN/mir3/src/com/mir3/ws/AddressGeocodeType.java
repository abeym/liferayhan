/*     */ package com.mir3.ws;
/*     */ 
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="addressGeocodeType", propOrder={})
/*     */ public class AddressGeocodeType
/*     */ {
/*     */   protected Double latitude;
/*     */   protected Double longitude;
/*     */   @XmlElement(required=true)
/*     */   protected GeocodeStatusEnumType geocodeStatus;
/*     */   
/*     */   public Double getLatitude()
/*     */   {
/*  51 */     return this.latitude;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLatitude(Double value)
/*     */   {
/*  63 */     this.latitude = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Double getLongitude()
/*     */   {
/*  75 */     return this.longitude;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLongitude(Double value)
/*     */   {
/*  87 */     this.longitude = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public GeocodeStatusEnumType getGeocodeStatus()
/*     */   {
/*  99 */     return this.geocodeStatus;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setGeocodeStatus(GeocodeStatusEnumType value)
/*     */   {
/* 111 */     this.geocodeStatus = value;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\AddressGeocodeType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */