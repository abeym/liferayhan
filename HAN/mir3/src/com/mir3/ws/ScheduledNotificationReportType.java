/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="scheduledNotificationReportType", propOrder={"periodInSeconds"})
/*    */ public class ScheduledNotificationReportType
/*    */ {
/*    */   protected int periodInSeconds;
/*    */   
/*    */   public int getPeriodInSeconds()
/*    */   {
/* 41 */     return this.periodInSeconds;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setPeriodInSeconds(int value)
/*    */   {
/* 49 */     this.periodInSeconds = value;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\ScheduledNotificationReportType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */