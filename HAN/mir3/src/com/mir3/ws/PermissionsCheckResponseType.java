/*    */ package com.mir3.ws;
/*    */ 
/*    */ import java.util.ArrayList;
/*    */ import java.util.List;
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="permissionsCheckResponseType", propOrder={"hasPermission"})
/*    */ public class PermissionsCheckResponseType
/*    */ {
/*    */   @XmlElement(type=Boolean.class)
/*    */   protected List<Boolean> hasPermission;
/*    */   
/*    */   public List<Boolean> getHasPermission()
/*    */   {
/* 63 */     if (this.hasPermission == null) {
/* 64 */       this.hasPermission = new ArrayList();
/*    */     }
/* 66 */     return this.hasPermission;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\PermissionsCheckResponseType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */