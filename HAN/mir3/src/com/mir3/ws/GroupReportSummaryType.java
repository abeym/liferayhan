/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="groupReportSummaryType", propOrder={"notificationGroupReportId", "notificationGroupReportUUID"})
/*    */ public class GroupReportSummaryType
/*    */   extends BaseReportType
/*    */ {
/*    */   protected Integer notificationGroupReportId;
/*    */   protected String notificationGroupReportUUID;
/*    */   
/*    */   public Integer getNotificationGroupReportId()
/*    */   {
/* 50 */     return this.notificationGroupReportId;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setNotificationGroupReportId(Integer value)
/*    */   {
/* 62 */     this.notificationGroupReportId = value;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public String getNotificationGroupReportUUID()
/*    */   {
/* 74 */     return this.notificationGroupReportUUID;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setNotificationGroupReportUUID(String value)
/*    */   {
/* 86 */     this.notificationGroupReportUUID = value;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\GroupReportSummaryType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */