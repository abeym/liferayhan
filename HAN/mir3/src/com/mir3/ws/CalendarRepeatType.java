/*     */ package com.mir3.ws;
/*     */ 
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlSchemaType;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ import javax.xml.datatype.XMLGregorianCalendar;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="calendarRepeatType", propOrder={"time", "index", "dayOfWeek", "timeUnit"})
/*     */ public class CalendarRepeatType
/*     */ {
/*     */   @XmlElement(required=true)
/*     */   @XmlSchemaType(name="time")
/*     */   protected XMLGregorianCalendar time;
/*     */   @XmlElement(required=true)
/*     */   protected CalendarIndexType index;
/*     */   @XmlElement(required=true)
/*     */   protected DayOfWeekType dayOfWeek;
/*     */   @XmlElement(required=true)
/*     */   protected CalendarUnitType timeUnit;
/*     */   
/*     */   public XMLGregorianCalendar getTime()
/*     */   {
/*  62 */     return this.time;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTime(XMLGregorianCalendar value)
/*     */   {
/*  74 */     this.time = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public CalendarIndexType getIndex()
/*     */   {
/*  86 */     return this.index;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setIndex(CalendarIndexType value)
/*     */   {
/*  98 */     this.index = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public DayOfWeekType getDayOfWeek()
/*     */   {
/* 110 */     return this.dayOfWeek;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDayOfWeek(DayOfWeekType value)
/*     */   {
/* 122 */     this.dayOfWeek = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public CalendarUnitType getTimeUnit()
/*     */   {
/* 134 */     return this.timeUnit;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTimeUnit(CalendarUnitType value)
/*     */   {
/* 146 */     this.timeUnit = value;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\CalendarRepeatType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */