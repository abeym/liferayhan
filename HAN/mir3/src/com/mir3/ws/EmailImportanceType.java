/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlEnum;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlType(name="emailImportanceType")
/*    */ @XmlEnum
/*    */ public enum EmailImportanceType
/*    */ {
/* 30 */   LOWEST, 
/* 31 */   LOW, 
/* 32 */   NORMAL, 
/* 33 */   HIGH, 
/* 34 */   HIGHEST;
/*    */   
/*    */   public String value() {
/* 37 */     return name();
/*    */   }
/*    */   
/*    */   public static EmailImportanceType fromValue(String v) {
/* 41 */     return valueOf(v);
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\EmailImportanceType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */