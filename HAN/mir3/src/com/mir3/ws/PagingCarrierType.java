/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="pagingCarrierType", propOrder={})
/*    */ public class PagingCarrierType
/*    */ {
/*    */   @XmlElement(required=true)
/*    */   protected String pagingCarrierName;
/*    */   protected int pagingCarrierId;
/*    */   
/*    */   public String getPagingCarrierName()
/*    */   {
/* 49 */     return this.pagingCarrierName;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setPagingCarrierName(String value)
/*    */   {
/* 61 */     this.pagingCarrierName = value;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public int getPagingCarrierId()
/*    */   {
/* 69 */     return this.pagingCarrierId;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setPagingCarrierId(int value)
/*    */   {
/* 77 */     this.pagingCarrierId = value;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\PagingCarrierType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */