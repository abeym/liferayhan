/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="searchReportsResultType", propOrder={"reportSummary", "groupReportSummary"})
/*    */ public class SearchReportsResultType
/*    */ {
/*    */   protected ReportSummaryType reportSummary;
/*    */   protected GroupReportSummaryType groupReportSummary;
/*    */   
/*    */   public ReportSummaryType getReportSummary()
/*    */   {
/* 48 */     return this.reportSummary;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setReportSummary(ReportSummaryType value)
/*    */   {
/* 60 */     this.reportSummary = value;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public GroupReportSummaryType getGroupReportSummary()
/*    */   {
/* 72 */     return this.groupReportSummary;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setGroupReportSummary(GroupReportSummaryType value)
/*    */   {
/* 84 */     this.groupReportSummary = value;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\SearchReportsResultType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */