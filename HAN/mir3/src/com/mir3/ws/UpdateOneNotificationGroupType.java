/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="updateOneNotificationGroupType", propOrder={})
/*    */ public class UpdateOneNotificationGroupType
/*    */ {
/*    */   @XmlElement(required=true)
/*    */   protected String notificationGroup;
/*    */   @XmlElement(required=true)
/*    */   protected NotificationGroupDetailType notificationGroupDetail;
/*    */   
/*    */   public String getNotificationGroup()
/*    */   {
/* 50 */     return this.notificationGroup;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setNotificationGroup(String value)
/*    */   {
/* 62 */     this.notificationGroup = value;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public NotificationGroupDetailType getNotificationGroupDetail()
/*    */   {
/* 74 */     return this.notificationGroupDetail;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setNotificationGroupDetail(NotificationGroupDetailType value)
/*    */   {
/* 86 */     this.notificationGroupDetail = value;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\UpdateOneNotificationGroupType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */