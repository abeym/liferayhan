/*    */ package com.mir3.ws;
/*    */ 
/*    */ import java.util.ArrayList;
/*    */ import java.util.List;
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="terminateNotificationGroupsResponseType", propOrder={"notificationGroupReportId", "notificationGroupReportUUID"})
/*    */ public class TerminateNotificationGroupsResponseType
/*    */ {
/*    */   @XmlElement(type=Integer.class)
/*    */   protected List<Integer> notificationGroupReportId;
/*    */   protected List<String> notificationGroupReportUUID;
/*    */   
/*    */   public List<Integer> getNotificationGroupReportId()
/*    */   {
/* 66 */     if (this.notificationGroupReportId == null) {
/* 67 */       this.notificationGroupReportId = new ArrayList();
/*    */     }
/* 69 */     return this.notificationGroupReportId;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public List<String> getNotificationGroupReportUUID()
/*    */   {
/* 95 */     if (this.notificationGroupReportUUID == null) {
/* 96 */       this.notificationGroupReportUUID = new ArrayList();
/*    */     }
/* 98 */     return this.notificationGroupReportUUID;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\TerminateNotificationGroupsResponseType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */