/*    */ package com.mir3.ws;
/*    */ 
/*    */ import java.util.ArrayList;
/*    */ import java.util.List;
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="desktopAlertInfosType", propOrder={"desktopInfo"})
/*    */ public class DesktopAlertInfosType
/*    */ {
/*    */   protected List<DesktopInfo> desktopInfo;
/*    */   
/*    */   public List<DesktopInfo> getDesktopInfo()
/*    */   {
/* 61 */     if (this.desktopInfo == null) {
/* 62 */       this.desktopInfo = new ArrayList();
/*    */     }
/* 64 */     return this.desktopInfo;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\DesktopAlertInfosType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */