/*      */ package com.mir3.ws;
/*      */ 
/*      */ import javax.xml.bind.annotation.XmlAccessType;
/*      */ import javax.xml.bind.annotation.XmlAccessorType;
/*      */ import javax.xml.bind.annotation.XmlSchemaType;
/*      */ import javax.xml.bind.annotation.XmlType;
/*      */ import javax.xml.datatype.XMLGregorianCalendar;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ @XmlAccessorType(XmlAccessType.FIELD)
/*      */ @XmlType(name="recipientOneSearchType", propOrder={"and", "or", "not", "firstName", "lastName", "username", "userId", "userUUID", "telephonyId", "groupTitle", "title", "company", "division", "addressTypeName", "address1", "address2", "building", "floor", "city", "state", "zip", "facility", "country", "province", "phoneNumber", "pager", "email", "fax", "sms", "smsOptInStatus", "role", "deviceType", "disabledDevice", "loginEnabled", "lastModifiedBefore", "lastModifiedAfter", "customField", "employeeId", "permission", "catSubcat", "priority", "severity", "roleTemplate", "timeZone", "geocodeStatus"})
/*      */ public class RecipientOneSearchType
/*      */ {
/*      */   protected RecipientManySearchType and;
/*      */   protected RecipientManySearchType or;
/*      */   protected RecipientOneSearchType not;
/*      */   protected String firstName;
/*      */   protected String lastName;
/*      */   protected String username;
/*      */   protected Integer userId;
/*      */   protected String userUUID;
/*      */   protected String telephonyId;
/*      */   protected String groupTitle;
/*      */   protected String title;
/*      */   protected String company;
/*      */   protected String division;
/*      */   protected String addressTypeName;
/*      */   protected String address1;
/*      */   protected String address2;
/*      */   protected String building;
/*      */   protected String floor;
/*      */   protected String city;
/*      */   protected String state;
/*      */   protected String zip;
/*      */   protected String facility;
/*      */   protected String country;
/*      */   protected String province;
/*      */   protected String phoneNumber;
/*      */   protected String pager;
/*      */   protected String email;
/*      */   protected String fax;
/*      */   protected String sms;
/*      */   protected SmsOptInStatusType smsOptInStatus;
/*      */   protected String role;
/*      */   protected String deviceType;
/*      */   protected Boolean disabledDevice;
/*      */   protected Boolean loginEnabled;
/*      */   @XmlSchemaType(name="dateTime")
/*      */   protected XMLGregorianCalendar lastModifiedBefore;
/*      */   @XmlSchemaType(name="dateTime")
/*      */   protected XMLGregorianCalendar lastModifiedAfter;
/*      */   protected CustomFieldType customField;
/*      */   protected String employeeId;
/*      */   protected String permission;
/*      */   protected CatSubcatType catSubcat;
/*      */   protected String priority;
/*      */   protected String severity;
/*      */   protected String roleTemplate;
/*      */   protected TimeZoneType timeZone;
/*      */   protected GeocodeStatusEnumType geocodeStatus;
/*      */   
/*      */   public RecipientManySearchType getAnd()
/*      */   {
/*  181 */     return this.and;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setAnd(RecipientManySearchType value)
/*      */   {
/*  193 */     this.and = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public RecipientManySearchType getOr()
/*      */   {
/*  205 */     return this.or;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setOr(RecipientManySearchType value)
/*      */   {
/*  217 */     this.or = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public RecipientOneSearchType getNot()
/*      */   {
/*  229 */     return this.not;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setNot(RecipientOneSearchType value)
/*      */   {
/*  241 */     this.not = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getFirstName()
/*      */   {
/*  253 */     return this.firstName;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setFirstName(String value)
/*      */   {
/*  265 */     this.firstName = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getLastName()
/*      */   {
/*  277 */     return this.lastName;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setLastName(String value)
/*      */   {
/*  289 */     this.lastName = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getUsername()
/*      */   {
/*  301 */     return this.username;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setUsername(String value)
/*      */   {
/*  313 */     this.username = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Integer getUserId()
/*      */   {
/*  325 */     return this.userId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setUserId(Integer value)
/*      */   {
/*  337 */     this.userId = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getUserUUID()
/*      */   {
/*  349 */     return this.userUUID;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setUserUUID(String value)
/*      */   {
/*  361 */     this.userUUID = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTelephonyId()
/*      */   {
/*  373 */     return this.telephonyId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTelephonyId(String value)
/*      */   {
/*  385 */     this.telephonyId = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getGroupTitle()
/*      */   {
/*  397 */     return this.groupTitle;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setGroupTitle(String value)
/*      */   {
/*  409 */     this.groupTitle = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTitle()
/*      */   {
/*  421 */     return this.title;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTitle(String value)
/*      */   {
/*  433 */     this.title = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getCompany()
/*      */   {
/*  445 */     return this.company;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setCompany(String value)
/*      */   {
/*  457 */     this.company = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getDivision()
/*      */   {
/*  469 */     return this.division;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setDivision(String value)
/*      */   {
/*  481 */     this.division = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getAddressTypeName()
/*      */   {
/*  493 */     return this.addressTypeName;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setAddressTypeName(String value)
/*      */   {
/*  505 */     this.addressTypeName = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getAddress1()
/*      */   {
/*  517 */     return this.address1;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setAddress1(String value)
/*      */   {
/*  529 */     this.address1 = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getAddress2()
/*      */   {
/*  541 */     return this.address2;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setAddress2(String value)
/*      */   {
/*  553 */     this.address2 = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getBuilding()
/*      */   {
/*  565 */     return this.building;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setBuilding(String value)
/*      */   {
/*  577 */     this.building = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getFloor()
/*      */   {
/*  589 */     return this.floor;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setFloor(String value)
/*      */   {
/*  601 */     this.floor = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getCity()
/*      */   {
/*  613 */     return this.city;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setCity(String value)
/*      */   {
/*  625 */     this.city = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getState()
/*      */   {
/*  637 */     return this.state;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setState(String value)
/*      */   {
/*  649 */     this.state = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getZip()
/*      */   {
/*  661 */     return this.zip;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setZip(String value)
/*      */   {
/*  673 */     this.zip = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getFacility()
/*      */   {
/*  685 */     return this.facility;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setFacility(String value)
/*      */   {
/*  697 */     this.facility = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getCountry()
/*      */   {
/*  709 */     return this.country;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setCountry(String value)
/*      */   {
/*  721 */     this.country = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getProvince()
/*      */   {
/*  733 */     return this.province;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setProvince(String value)
/*      */   {
/*  745 */     this.province = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getPhoneNumber()
/*      */   {
/*  757 */     return this.phoneNumber;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setPhoneNumber(String value)
/*      */   {
/*  769 */     this.phoneNumber = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getPager()
/*      */   {
/*  781 */     return this.pager;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setPager(String value)
/*      */   {
/*  793 */     this.pager = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getEmail()
/*      */   {
/*  805 */     return this.email;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setEmail(String value)
/*      */   {
/*  817 */     this.email = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getFax()
/*      */   {
/*  829 */     return this.fax;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setFax(String value)
/*      */   {
/*  841 */     this.fax = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getSms()
/*      */   {
/*  853 */     return this.sms;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSms(String value)
/*      */   {
/*  865 */     this.sms = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public SmsOptInStatusType getSmsOptInStatus()
/*      */   {
/*  877 */     return this.smsOptInStatus;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSmsOptInStatus(SmsOptInStatusType value)
/*      */   {
/*  889 */     this.smsOptInStatus = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getRole()
/*      */   {
/*  901 */     return this.role;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setRole(String value)
/*      */   {
/*  913 */     this.role = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getDeviceType()
/*      */   {
/*  925 */     return this.deviceType;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setDeviceType(String value)
/*      */   {
/*  937 */     this.deviceType = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Boolean isDisabledDevice()
/*      */   {
/*  949 */     return this.disabledDevice;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setDisabledDevice(Boolean value)
/*      */   {
/*  961 */     this.disabledDevice = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Boolean isLoginEnabled()
/*      */   {
/*  973 */     return this.loginEnabled;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setLoginEnabled(Boolean value)
/*      */   {
/*  985 */     this.loginEnabled = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public XMLGregorianCalendar getLastModifiedBefore()
/*      */   {
/*  997 */     return this.lastModifiedBefore;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setLastModifiedBefore(XMLGregorianCalendar value)
/*      */   {
/* 1009 */     this.lastModifiedBefore = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public XMLGregorianCalendar getLastModifiedAfter()
/*      */   {
/* 1021 */     return this.lastModifiedAfter;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setLastModifiedAfter(XMLGregorianCalendar value)
/*      */   {
/* 1033 */     this.lastModifiedAfter = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public CustomFieldType getCustomField()
/*      */   {
/* 1045 */     return this.customField;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setCustomField(CustomFieldType value)
/*      */   {
/* 1057 */     this.customField = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getEmployeeId()
/*      */   {
/* 1069 */     return this.employeeId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setEmployeeId(String value)
/*      */   {
/* 1081 */     this.employeeId = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getPermission()
/*      */   {
/* 1093 */     return this.permission;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setPermission(String value)
/*      */   {
/* 1105 */     this.permission = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public CatSubcatType getCatSubcat()
/*      */   {
/* 1117 */     return this.catSubcat;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setCatSubcat(CatSubcatType value)
/*      */   {
/* 1129 */     this.catSubcat = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getPriority()
/*      */   {
/* 1141 */     return this.priority;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setPriority(String value)
/*      */   {
/* 1153 */     this.priority = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getSeverity()
/*      */   {
/* 1165 */     return this.severity;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSeverity(String value)
/*      */   {
/* 1177 */     this.severity = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getRoleTemplate()
/*      */   {
/* 1189 */     return this.roleTemplate;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setRoleTemplate(String value)
/*      */   {
/* 1201 */     this.roleTemplate = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public TimeZoneType getTimeZone()
/*      */   {
/* 1213 */     return this.timeZone;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTimeZone(TimeZoneType value)
/*      */   {
/* 1225 */     this.timeZone = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public GeocodeStatusEnumType getGeocodeStatus()
/*      */   {
/* 1237 */     return this.geocodeStatus;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setGeocodeStatus(GeocodeStatusEnumType value)
/*      */   {
/* 1249 */     this.geocodeStatus = value;
/*      */   }
/*      */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\RecipientOneSearchType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */