/*     */ package com.mir3.ws;
/*     */ 
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlSchemaType;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ import javax.xml.datatype.XMLGregorianCalendar;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="ruleType", propOrder={"ruleTimeZone", "isException", "ruleException", "workOnHoliday", "startTime", "durationInMinutes", "startDate", "endDate", "onlyOnce", "intervalRepeat", "calendarRepeat", "daysRepeat", "everyHoliday", "escalations"})
/*     */ public class RuleType
/*     */ {
/*     */   @XmlElement(required=true)
/*     */   protected TimeZoneType ruleTimeZone;
/*     */   protected Boolean isException;
/*     */   protected RuleExceptionType ruleException;
/*     */   protected boolean workOnHoliday;
/*     */   @XmlElement(required=true)
/*     */   @XmlSchemaType(name="time")
/*     */   protected XMLGregorianCalendar startTime;
/*     */   protected int durationInMinutes;
/*     */   @XmlElement(required=true)
/*     */   @XmlSchemaType(name="date")
/*     */   protected XMLGregorianCalendar startDate;
/*     */   @XmlSchemaType(name="date")
/*     */   protected XMLGregorianCalendar endDate;
/*     */   protected Boolean onlyOnce;
/*     */   protected IntervalRepeatForScheduleType intervalRepeat;
/*     */   protected CalendarRepeatForScheduleType calendarRepeat;
/*     */   protected DayRepeatForScheduleType daysRepeat;
/*     */   protected Boolean everyHoliday;
/*     */   @XmlElement(required=true)
/*     */   protected NotificationEscalationsType escalations;
/*     */   
/*     */   public TimeZoneType getRuleTimeZone()
/*     */   {
/*  96 */     return this.ruleTimeZone;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRuleTimeZone(TimeZoneType value)
/*     */   {
/* 108 */     this.ruleTimeZone = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Boolean isIsException()
/*     */   {
/* 120 */     return this.isException;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setIsException(Boolean value)
/*     */   {
/* 132 */     this.isException = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public RuleExceptionType getRuleException()
/*     */   {
/* 144 */     return this.ruleException;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRuleException(RuleExceptionType value)
/*     */   {
/* 156 */     this.ruleException = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean isWorkOnHoliday()
/*     */   {
/* 164 */     return this.workOnHoliday;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setWorkOnHoliday(boolean value)
/*     */   {
/* 172 */     this.workOnHoliday = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public XMLGregorianCalendar getStartTime()
/*     */   {
/* 184 */     return this.startTime;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setStartTime(XMLGregorianCalendar value)
/*     */   {
/* 196 */     this.startTime = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getDurationInMinutes()
/*     */   {
/* 204 */     return this.durationInMinutes;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDurationInMinutes(int value)
/*     */   {
/* 212 */     this.durationInMinutes = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public XMLGregorianCalendar getStartDate()
/*     */   {
/* 224 */     return this.startDate;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setStartDate(XMLGregorianCalendar value)
/*     */   {
/* 236 */     this.startDate = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public XMLGregorianCalendar getEndDate()
/*     */   {
/* 248 */     return this.endDate;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setEndDate(XMLGregorianCalendar value)
/*     */   {
/* 260 */     this.endDate = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Boolean isOnlyOnce()
/*     */   {
/* 272 */     return this.onlyOnce;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setOnlyOnce(Boolean value)
/*     */   {
/* 284 */     this.onlyOnce = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public IntervalRepeatForScheduleType getIntervalRepeat()
/*     */   {
/* 296 */     return this.intervalRepeat;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setIntervalRepeat(IntervalRepeatForScheduleType value)
/*     */   {
/* 308 */     this.intervalRepeat = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public CalendarRepeatForScheduleType getCalendarRepeat()
/*     */   {
/* 320 */     return this.calendarRepeat;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCalendarRepeat(CalendarRepeatForScheduleType value)
/*     */   {
/* 332 */     this.calendarRepeat = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public DayRepeatForScheduleType getDaysRepeat()
/*     */   {
/* 344 */     return this.daysRepeat;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDaysRepeat(DayRepeatForScheduleType value)
/*     */   {
/* 356 */     this.daysRepeat = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Boolean isEveryHoliday()
/*     */   {
/* 368 */     return this.everyHoliday;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setEveryHoliday(Boolean value)
/*     */   {
/* 380 */     this.everyHoliday = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public NotificationEscalationsType getEscalations()
/*     */   {
/* 392 */     return this.escalations;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setEscalations(NotificationEscalationsType value)
/*     */   {
/* 404 */     this.escalations = value;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\RuleType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */