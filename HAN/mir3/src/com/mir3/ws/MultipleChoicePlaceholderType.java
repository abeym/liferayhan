/*    */ package com.mir3.ws;
/*    */ 
/*    */ import java.util.ArrayList;
/*    */ import java.util.List;
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="multipleChoicePlaceholderType", propOrder={"option", "defaultLabel"})
/*    */ public class MultipleChoicePlaceholderType
/*    */ {
/*    */   @XmlElement(required=true)
/*    */   protected List<PlaceholderOptionType> option;
/*    */   protected String defaultLabel;
/*    */   
/*    */   public List<PlaceholderOptionType> getOption()
/*    */   {
/* 66 */     if (this.option == null) {
/* 67 */       this.option = new ArrayList();
/*    */     }
/* 69 */     return this.option;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public String getDefaultLabel()
/*    */   {
/* 81 */     return this.defaultLabel;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setDefaultLabel(String value)
/*    */   {
/* 93 */     this.defaultLabel = value;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\MultipleChoicePlaceholderType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */