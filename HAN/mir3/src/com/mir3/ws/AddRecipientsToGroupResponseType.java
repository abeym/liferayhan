/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="addRecipientsToGroupResponseType", propOrder={"addedMembers"})
/*    */ public class AddRecipientsToGroupResponseType
/*    */ {
/*    */   @XmlElement(required=true)
/*    */   protected RecipientGroupMembersType addedMembers;
/*    */   
/*    */   public RecipientGroupMembersType getAddedMembers()
/*    */   {
/* 47 */     return this.addedMembers;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setAddedMembers(RecipientGroupMembersType value)
/*    */   {
/* 59 */     this.addedMembers = value;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\AddRecipientsToGroupResponseType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */