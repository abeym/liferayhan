/*     */ package com.mir3.ws;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlSchemaType;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ import javax.xml.datatype.XMLGregorianCalendar;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="notificationReportType", propOrder={"notificationReportId", "notificationReportUUID", "title", "type", "timeSent", "timeClosed", "closedCause", "initiator", "division", "generalStatistics", "messageText", "additionalText", "verbiage", "priority", "status", "contactAttempts", "contactsCompleted", "leaveMessage", "emailAttachment", "recordedResponse", "responseOptions", "category", "categoryNameAtLaunch", "subcategory", "subcategoryNameAtLaunch", "priorityTopic", "priorityNameAtLaunch", "severity", "severityNameAtLaunch", "incidentTitle", "incidentTitleAtLaunch", "userData", "placeholders"})
/*     */ public class NotificationReportType
/*     */ {
/*     */   protected Integer notificationReportId;
/*     */   protected String notificationReportUUID;
/*     */   @XmlElement(required=true)
/*     */   protected String title;
/*     */   @XmlElement(required=true)
/*     */   protected NotificationMethodType type;
/*     */   @XmlElement(required=true)
/*     */   @XmlSchemaType(name="dateTime")
/*     */   protected XMLGregorianCalendar timeSent;
/*     */   @XmlSchemaType(name="dateTime")
/*     */   protected XMLGregorianCalendar timeClosed;
/*     */   protected ClosedCauseType closedCause;
/*     */   @XmlElement(required=true)
/*     */   protected NotificationResponseInitiatorType initiator;
/*     */   protected String division;
/*     */   @XmlElement(required=true)
/*     */   protected ReportGeneralStatistics generalStatistics;
/*     */   protected String messageText;
/*     */   protected String additionalText;
/*     */   protected VerbiagePerMessageType verbiage;
/*     */   @XmlElement(required=true)
/*     */   protected NotificationPriorityType priority;
/*     */   protected ReportStatusType status;
/*     */   protected Integer contactAttempts;
/*     */   @XmlSchemaType(name="dateTime")
/*     */   protected XMLGregorianCalendar contactsCompleted;
/*     */   protected Boolean leaveMessage;
/*     */   protected List<EmailAttachmentType> emailAttachment;
/*     */   protected RecordedResponseType recordedResponse;
/*     */   @XmlElement(required=true)
/*     */   protected ReportResponseOptionsType responseOptions;
/*     */   protected String category;
/*     */   protected String categoryNameAtLaunch;
/*     */   protected String subcategory;
/*     */   protected String subcategoryNameAtLaunch;
/*     */   protected String priorityTopic;
/*     */   protected String priorityNameAtLaunch;
/*     */   protected String severity;
/*     */   protected String severityNameAtLaunch;
/*     */   protected String incidentTitle;
/*     */   protected String incidentTitleAtLaunch;
/*     */   protected String userData;
/*     */   protected PlaceholdersReportType placeholders;
/*     */   
/*     */   public Integer getNotificationReportId()
/*     */   {
/* 158 */     return this.notificationReportId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setNotificationReportId(Integer value)
/*     */   {
/* 170 */     this.notificationReportId = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getNotificationReportUUID()
/*     */   {
/* 182 */     return this.notificationReportUUID;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setNotificationReportUUID(String value)
/*     */   {
/* 194 */     this.notificationReportUUID = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTitle()
/*     */   {
/* 206 */     return this.title;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTitle(String value)
/*     */   {
/* 218 */     this.title = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public NotificationMethodType getType()
/*     */   {
/* 230 */     return this.type;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setType(NotificationMethodType value)
/*     */   {
/* 242 */     this.type = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public XMLGregorianCalendar getTimeSent()
/*     */   {
/* 254 */     return this.timeSent;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTimeSent(XMLGregorianCalendar value)
/*     */   {
/* 266 */     this.timeSent = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public XMLGregorianCalendar getTimeClosed()
/*     */   {
/* 278 */     return this.timeClosed;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTimeClosed(XMLGregorianCalendar value)
/*     */   {
/* 290 */     this.timeClosed = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public ClosedCauseType getClosedCause()
/*     */   {
/* 302 */     return this.closedCause;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setClosedCause(ClosedCauseType value)
/*     */   {
/* 314 */     this.closedCause = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public NotificationResponseInitiatorType getInitiator()
/*     */   {
/* 326 */     return this.initiator;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setInitiator(NotificationResponseInitiatorType value)
/*     */   {
/* 338 */     this.initiator = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getDivision()
/*     */   {
/* 350 */     return this.division;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDivision(String value)
/*     */   {
/* 362 */     this.division = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public ReportGeneralStatistics getGeneralStatistics()
/*     */   {
/* 374 */     return this.generalStatistics;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setGeneralStatistics(ReportGeneralStatistics value)
/*     */   {
/* 386 */     this.generalStatistics = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getMessageText()
/*     */   {
/* 398 */     return this.messageText;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setMessageText(String value)
/*     */   {
/* 410 */     this.messageText = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getAdditionalText()
/*     */   {
/* 422 */     return this.additionalText;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAdditionalText(String value)
/*     */   {
/* 434 */     this.additionalText = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public VerbiagePerMessageType getVerbiage()
/*     */   {
/* 446 */     return this.verbiage;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setVerbiage(VerbiagePerMessageType value)
/*     */   {
/* 458 */     this.verbiage = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public NotificationPriorityType getPriority()
/*     */   {
/* 470 */     return this.priority;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPriority(NotificationPriorityType value)
/*     */   {
/* 482 */     this.priority = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public ReportStatusType getStatus()
/*     */   {
/* 494 */     return this.status;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setStatus(ReportStatusType value)
/*     */   {
/* 506 */     this.status = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getContactAttempts()
/*     */   {
/* 518 */     return this.contactAttempts;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setContactAttempts(Integer value)
/*     */   {
/* 530 */     this.contactAttempts = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public XMLGregorianCalendar getContactsCompleted()
/*     */   {
/* 542 */     return this.contactsCompleted;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setContactsCompleted(XMLGregorianCalendar value)
/*     */   {
/* 554 */     this.contactsCompleted = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Boolean isLeaveMessage()
/*     */   {
/* 566 */     return this.leaveMessage;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLeaveMessage(Boolean value)
/*     */   {
/* 578 */     this.leaveMessage = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<EmailAttachmentType> getEmailAttachment()
/*     */   {
/* 604 */     if (this.emailAttachment == null) {
/* 605 */       this.emailAttachment = new ArrayList();
/*     */     }
/* 607 */     return this.emailAttachment;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public RecordedResponseType getRecordedResponse()
/*     */   {
/* 619 */     return this.recordedResponse;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRecordedResponse(RecordedResponseType value)
/*     */   {
/* 631 */     this.recordedResponse = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public ReportResponseOptionsType getResponseOptions()
/*     */   {
/* 643 */     return this.responseOptions;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setResponseOptions(ReportResponseOptionsType value)
/*     */   {
/* 655 */     this.responseOptions = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCategory()
/*     */   {
/* 667 */     return this.category;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCategory(String value)
/*     */   {
/* 679 */     this.category = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCategoryNameAtLaunch()
/*     */   {
/* 691 */     return this.categoryNameAtLaunch;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCategoryNameAtLaunch(String value)
/*     */   {
/* 703 */     this.categoryNameAtLaunch = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getSubcategory()
/*     */   {
/* 715 */     return this.subcategory;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setSubcategory(String value)
/*     */   {
/* 727 */     this.subcategory = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getSubcategoryNameAtLaunch()
/*     */   {
/* 739 */     return this.subcategoryNameAtLaunch;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setSubcategoryNameAtLaunch(String value)
/*     */   {
/* 751 */     this.subcategoryNameAtLaunch = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getPriorityTopic()
/*     */   {
/* 763 */     return this.priorityTopic;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPriorityTopic(String value)
/*     */   {
/* 775 */     this.priorityTopic = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getPriorityNameAtLaunch()
/*     */   {
/* 787 */     return this.priorityNameAtLaunch;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPriorityNameAtLaunch(String value)
/*     */   {
/* 799 */     this.priorityNameAtLaunch = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getSeverity()
/*     */   {
/* 811 */     return this.severity;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setSeverity(String value)
/*     */   {
/* 823 */     this.severity = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getSeverityNameAtLaunch()
/*     */   {
/* 835 */     return this.severityNameAtLaunch;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setSeverityNameAtLaunch(String value)
/*     */   {
/* 847 */     this.severityNameAtLaunch = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getIncidentTitle()
/*     */   {
/* 859 */     return this.incidentTitle;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setIncidentTitle(String value)
/*     */   {
/* 871 */     this.incidentTitle = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getIncidentTitleAtLaunch()
/*     */   {
/* 883 */     return this.incidentTitleAtLaunch;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setIncidentTitleAtLaunch(String value)
/*     */   {
/* 895 */     this.incidentTitleAtLaunch = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getUserData()
/*     */   {
/* 907 */     return this.userData;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setUserData(String value)
/*     */   {
/* 919 */     this.userData = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public PlaceholdersReportType getPlaceholders()
/*     */   {
/* 931 */     return this.placeholders;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPlaceholders(PlaceholdersReportType value)
/*     */   {
/* 943 */     this.placeholders = value;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\NotificationReportType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */