/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="errorType", propOrder={})
/*    */ public class ErrorType
/*    */ {
/*    */   protected int errorCode;
/*    */   @XmlElement(required=true)
/*    */   protected String errorMessage;
/*    */   
/*    */   public int getErrorCode()
/*    */   {
/* 45 */     return this.errorCode;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setErrorCode(int value)
/*    */   {
/* 53 */     this.errorCode = value;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public String getErrorMessage()
/*    */   {
/* 65 */     return this.errorMessage;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setErrorMessage(String value)
/*    */   {
/* 77 */     this.errorMessage = value;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\ErrorType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */