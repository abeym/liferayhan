/*     */ package com.mir3.ws;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="locationScheduleDetailsType", propOrder={"sun", "mon", "tues", "wed", "thurs", "fri", "sat", "everyDay", "monWedFri", "tuesThurs", "monToFri", "satSun"})
/*     */ public class LocationScheduleDetailsType
/*     */ {
/*     */   protected List<LocationScheduleType> sun;
/*     */   protected List<LocationScheduleType> mon;
/*     */   protected List<LocationScheduleType> tues;
/*     */   protected List<LocationScheduleType> wed;
/*     */   protected List<LocationScheduleType> thurs;
/*     */   protected List<LocationScheduleType> fri;
/*     */   protected List<LocationScheduleType> sat;
/*     */   protected List<LocationScheduleType> everyDay;
/*     */   @XmlElement(name="mon-wed-fri")
/*     */   protected List<LocationScheduleType> monWedFri;
/*     */   @XmlElement(name="tues-thurs")
/*     */   protected List<LocationScheduleType> tuesThurs;
/*     */   @XmlElement(name="mon-to-fri")
/*     */   protected List<LocationScheduleType> monToFri;
/*     */   @XmlElement(name="sat-sun")
/*     */   protected List<LocationScheduleType> satSun;
/*     */   
/*     */   public List<LocationScheduleType> getSun()
/*     */   {
/*  99 */     if (this.sun == null) {
/* 100 */       this.sun = new ArrayList();
/*     */     }
/* 102 */     return this.sun;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<LocationScheduleType> getMon()
/*     */   {
/* 128 */     if (this.mon == null) {
/* 129 */       this.mon = new ArrayList();
/*     */     }
/* 131 */     return this.mon;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<LocationScheduleType> getTues()
/*     */   {
/* 157 */     if (this.tues == null) {
/* 158 */       this.tues = new ArrayList();
/*     */     }
/* 160 */     return this.tues;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<LocationScheduleType> getWed()
/*     */   {
/* 186 */     if (this.wed == null) {
/* 187 */       this.wed = new ArrayList();
/*     */     }
/* 189 */     return this.wed;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<LocationScheduleType> getThurs()
/*     */   {
/* 215 */     if (this.thurs == null) {
/* 216 */       this.thurs = new ArrayList();
/*     */     }
/* 218 */     return this.thurs;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<LocationScheduleType> getFri()
/*     */   {
/* 244 */     if (this.fri == null) {
/* 245 */       this.fri = new ArrayList();
/*     */     }
/* 247 */     return this.fri;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<LocationScheduleType> getSat()
/*     */   {
/* 273 */     if (this.sat == null) {
/* 274 */       this.sat = new ArrayList();
/*     */     }
/* 276 */     return this.sat;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<LocationScheduleType> getEveryDay()
/*     */   {
/* 302 */     if (this.everyDay == null) {
/* 303 */       this.everyDay = new ArrayList();
/*     */     }
/* 305 */     return this.everyDay;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<LocationScheduleType> getMonWedFri()
/*     */   {
/* 331 */     if (this.monWedFri == null) {
/* 332 */       this.monWedFri = new ArrayList();
/*     */     }
/* 334 */     return this.monWedFri;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<LocationScheduleType> getTuesThurs()
/*     */   {
/* 360 */     if (this.tuesThurs == null) {
/* 361 */       this.tuesThurs = new ArrayList();
/*     */     }
/* 363 */     return this.tuesThurs;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<LocationScheduleType> getMonToFri()
/*     */   {
/* 389 */     if (this.monToFri == null) {
/* 390 */       this.monToFri = new ArrayList();
/*     */     }
/* 392 */     return this.monToFri;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<LocationScheduleType> getSatSun()
/*     */   {
/* 418 */     if (this.satSun == null) {
/* 419 */       this.satSun = new ArrayList();
/*     */     }
/* 421 */     return this.satSun;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\LocationScheduleDetailsType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */