/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlEnum;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlType(name="idType")
/*    */ @XmlEnum
/*    */ public enum IdType
/*    */ {
/* 34 */   USER, 
/* 35 */   NOTIFICATION_REPORT, 
/* 36 */   NOTIFICATION_GROUP_REPORT, 
/* 37 */   CONTACT, 
/* 38 */   REPORT_RECIPIENT, 
/* 39 */   RESPONSE_OPTION_REPORT, 
/* 40 */   FOLLOWUP_REPORT, 
/* 41 */   RECORDED_RESPONSE, 
/* 42 */   GROUP;
/*    */   
/*    */   public String value() {
/* 45 */     return name();
/*    */   }
/*    */   
/*    */   public static IdType fromValue(String v) {
/* 49 */     return valueOf(v);
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\IdType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */