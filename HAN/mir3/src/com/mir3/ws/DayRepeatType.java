/*    */ package com.mir3.ws;
/*    */ 
/*    */ import java.util.ArrayList;
/*    */ import java.util.List;
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlSchemaType;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ import javax.xml.datatype.XMLGregorianCalendar;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="dayRepeatType", propOrder={"startTime", "workingDay"})
/*    */ public class DayRepeatType
/*    */ {
/*    */   @XmlElement(required=true)
/*    */   @XmlSchemaType(name="dateTime")
/*    */   protected XMLGregorianCalendar startTime;
/*    */   @XmlElement(type=Boolean.class)
/*    */   protected List<Boolean> workingDay;
/*    */   
/*    */   public XMLGregorianCalendar getStartTime()
/*    */   {
/* 56 */     return this.startTime;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setStartTime(XMLGregorianCalendar value)
/*    */   {
/* 68 */     this.startTime = value;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public List<Boolean> getWorkingDay()
/*    */   {
/* 94 */     if (this.workingDay == null) {
/* 95 */       this.workingDay = new ArrayList();
/*    */     }
/* 97 */     return this.workingDay;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\DayRepeatType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */