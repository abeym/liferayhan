/*      */ package com.mir3.ws;
/*      */ 
/*      */ import javax.xml.bind.annotation.XmlAccessType;
/*      */ import javax.xml.bind.annotation.XmlAccessorType;
/*      */ import javax.xml.bind.annotation.XmlType;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ @XmlAccessorType(XmlAccessType.FIELD)
/*      */ @XmlType(name="wrappedRequestType", propOrder={"response", "addNewRecipients", "updateRecipients", "deleteRecipients", "searchRecipients", "searchReports", "hideReports", "getRecipientRoles", "permissionsCheck", "getPagingCarriers", "addNewRecipientGroups", "updateRecipientGroups", "addRecipientsToGroup", "removeRecipientsFromGroup", "deleteRecipientGroups", "searchRecipientGroups", "getRecipientGroups", "addNewNotifications", "updateNotifications", "deleteNotifications", "terminateNotifications", "initiateNotifications", "setEmailAttachment", "getEmailAttachment", "getRecordedResponse", "prepareToRecordMessages", "uploadVoiceFile", "downloadVoiceFile", "deleteVoiceFiles", "searchVoiceFiles", "getNotificationReports", "getNotificationReportRecipients", "oneStepNotification", "searchNotifications", "addNewNotificationGroups", "updateNotificationGroups", "deleteNotificationGroups", "terminateNotificationGroups", "initiateNotificationGroups", "getNotificationGroupReport", "searchNotificationGroups", "addNewDivisions", "updateDivision", "getDivisions", "getPhonePrefixes", "setPhonePrefixes", "deleteDivisions", "createSingleSignOnToken", "validateSingleSignOnToken", "setNotificationSchedule", "getNotificationSchedule", "addNewRecipientSchedules", "updateRecipientSchedules", "deleteRecipientSchedules", "searchRecipientSchedules", "executeCustomReport", "getVerbiage", "setVerbiage", "setCustomVerbiageTypeByDivision", "getCustomVerbiageTypeByDivision", "getLoggedInUser", "idConvert"})
/*      */ public class WrappedRequestType
/*      */ {
/*      */   protected ResponseType response;
/*      */   protected AddNewRecipientsType addNewRecipients;
/*      */   protected UpdateRecipientsType updateRecipients;
/*      */   protected DeleteRecipientsType deleteRecipients;
/*      */   protected SearchRecipientsType searchRecipients;
/*      */   protected SearchReportsType searchReports;
/*      */   protected HideReportsType hideReports;
/*      */   protected GetRecipientRolesType getRecipientRoles;
/*      */   protected PermissionsCheckType permissionsCheck;
/*      */   protected GetPagingCarriersType getPagingCarriers;
/*      */   protected AddNewRecipientGroupsType addNewRecipientGroups;
/*      */   protected UpdateRecipientGroupsType updateRecipientGroups;
/*      */   protected AddRecipientsToGroupType addRecipientsToGroup;
/*      */   protected RemoveRecipientsFromGroupType removeRecipientsFromGroup;
/*      */   protected DeleteRecipientGroupsType deleteRecipientGroups;
/*      */   protected SearchRecipientGroupsType searchRecipientGroups;
/*      */   protected GetRecipientGroupsType getRecipientGroups;
/*      */   protected AddNewNotificationsType addNewNotifications;
/*      */   protected UpdateNotificationsType updateNotifications;
/*      */   protected DeleteNotificationsType deleteNotifications;
/*      */   protected TerminateNotificationsType terminateNotifications;
/*      */   protected InitiateNotificationsType initiateNotifications;
/*      */   protected SetEmailAttachmentType setEmailAttachment;
/*      */   protected GetEmailAttachmentType getEmailAttachment;
/*      */   protected GetRecordedResponseType getRecordedResponse;
/*      */   protected PrepareToRecordMessagesType prepareToRecordMessages;
/*      */   protected UploadVoiceFileType uploadVoiceFile;
/*      */   protected DownloadVoiceFileType downloadVoiceFile;
/*      */   protected DeleteVoiceFilesType deleteVoiceFiles;
/*      */   protected SearchVoiceFilesType searchVoiceFiles;
/*      */   protected GetNotificationReportsType getNotificationReports;
/*      */   protected GetNotificationReportRecipientsType getNotificationReportRecipients;
/*      */   protected OneStepNotificationType oneStepNotification;
/*      */   protected SearchNotificationsType searchNotifications;
/*      */   protected AddNewNotificationGroupsType addNewNotificationGroups;
/*      */   protected UpdateNotificationGroupsType updateNotificationGroups;
/*      */   protected DeleteNotificationGroupsType deleteNotificationGroups;
/*      */   protected TerminateNotificationGroupsType terminateNotificationGroups;
/*      */   protected InitiateNotificationGroupsType initiateNotificationGroups;
/*      */   protected GetNotificationGroupReportType getNotificationGroupReport;
/*      */   protected SearchNotificationGroupsType searchNotificationGroups;
/*      */   protected AddNewDivisionsType addNewDivisions;
/*      */   protected UpdateDivisionType updateDivision;
/*      */   protected GetDivisionsType getDivisions;
/*      */   protected GetPhonePrefixesType getPhonePrefixes;
/*      */   protected SetPhonePrefixesType setPhonePrefixes;
/*      */   protected DeleteDivisionsType deleteDivisions;
/*      */   protected CreateSingleSignOnTokenType createSingleSignOnToken;
/*      */   protected ValidateSingleSignOnTokenType validateSingleSignOnToken;
/*      */   protected SetNotificationScheduleType setNotificationSchedule;
/*      */   protected GetNotificationScheduleType getNotificationSchedule;
/*      */   protected AddNewRecipientSchedulesType addNewRecipientSchedules;
/*      */   protected UpdateRecipientSchedulesType updateRecipientSchedules;
/*      */   protected DeleteRecipientSchedulesType deleteRecipientSchedules;
/*      */   protected SearchRecipientSchedulesType searchRecipientSchedules;
/*      */   protected ExecuteCustomReportType executeCustomReport;
/*      */   protected GetVerbiageType getVerbiage;
/*      */   protected SetVerbiageType setVerbiage;
/*      */   protected SetCustomVerbiageTypeDivisionType setCustomVerbiageTypeByDivision;
/*      */   protected GetCustomVerbiageTypeDivisionType getCustomVerbiageTypeByDivision;
/*      */   protected GetLoggedInUserType getLoggedInUser;
/*      */   protected IdConvertType idConvert;
/*      */   
/*      */   public ResponseType getResponse()
/*      */   {
/*  228 */     return this.response;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setResponse(ResponseType value)
/*      */   {
/*  240 */     this.response = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public AddNewRecipientsType getAddNewRecipients()
/*      */   {
/*  252 */     return this.addNewRecipients;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setAddNewRecipients(AddNewRecipientsType value)
/*      */   {
/*  264 */     this.addNewRecipients = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public UpdateRecipientsType getUpdateRecipients()
/*      */   {
/*  276 */     return this.updateRecipients;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setUpdateRecipients(UpdateRecipientsType value)
/*      */   {
/*  288 */     this.updateRecipients = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DeleteRecipientsType getDeleteRecipients()
/*      */   {
/*  300 */     return this.deleteRecipients;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setDeleteRecipients(DeleteRecipientsType value)
/*      */   {
/*  312 */     this.deleteRecipients = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public SearchRecipientsType getSearchRecipients()
/*      */   {
/*  324 */     return this.searchRecipients;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSearchRecipients(SearchRecipientsType value)
/*      */   {
/*  336 */     this.searchRecipients = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public SearchReportsType getSearchReports()
/*      */   {
/*  348 */     return this.searchReports;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSearchReports(SearchReportsType value)
/*      */   {
/*  360 */     this.searchReports = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public HideReportsType getHideReports()
/*      */   {
/*  372 */     return this.hideReports;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setHideReports(HideReportsType value)
/*      */   {
/*  384 */     this.hideReports = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetRecipientRolesType getGetRecipientRoles()
/*      */   {
/*  396 */     return this.getRecipientRoles;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setGetRecipientRoles(GetRecipientRolesType value)
/*      */   {
/*  408 */     this.getRecipientRoles = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public PermissionsCheckType getPermissionsCheck()
/*      */   {
/*  420 */     return this.permissionsCheck;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setPermissionsCheck(PermissionsCheckType value)
/*      */   {
/*  432 */     this.permissionsCheck = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetPagingCarriersType getGetPagingCarriers()
/*      */   {
/*  444 */     return this.getPagingCarriers;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setGetPagingCarriers(GetPagingCarriersType value)
/*      */   {
/*  456 */     this.getPagingCarriers = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public AddNewRecipientGroupsType getAddNewRecipientGroups()
/*      */   {
/*  468 */     return this.addNewRecipientGroups;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setAddNewRecipientGroups(AddNewRecipientGroupsType value)
/*      */   {
/*  480 */     this.addNewRecipientGroups = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public UpdateRecipientGroupsType getUpdateRecipientGroups()
/*      */   {
/*  492 */     return this.updateRecipientGroups;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setUpdateRecipientGroups(UpdateRecipientGroupsType value)
/*      */   {
/*  504 */     this.updateRecipientGroups = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public AddRecipientsToGroupType getAddRecipientsToGroup()
/*      */   {
/*  516 */     return this.addRecipientsToGroup;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setAddRecipientsToGroup(AddRecipientsToGroupType value)
/*      */   {
/*  528 */     this.addRecipientsToGroup = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public RemoveRecipientsFromGroupType getRemoveRecipientsFromGroup()
/*      */   {
/*  540 */     return this.removeRecipientsFromGroup;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setRemoveRecipientsFromGroup(RemoveRecipientsFromGroupType value)
/*      */   {
/*  552 */     this.removeRecipientsFromGroup = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DeleteRecipientGroupsType getDeleteRecipientGroups()
/*      */   {
/*  564 */     return this.deleteRecipientGroups;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setDeleteRecipientGroups(DeleteRecipientGroupsType value)
/*      */   {
/*  576 */     this.deleteRecipientGroups = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public SearchRecipientGroupsType getSearchRecipientGroups()
/*      */   {
/*  588 */     return this.searchRecipientGroups;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSearchRecipientGroups(SearchRecipientGroupsType value)
/*      */   {
/*  600 */     this.searchRecipientGroups = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetRecipientGroupsType getGetRecipientGroups()
/*      */   {
/*  612 */     return this.getRecipientGroups;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setGetRecipientGroups(GetRecipientGroupsType value)
/*      */   {
/*  624 */     this.getRecipientGroups = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public AddNewNotificationsType getAddNewNotifications()
/*      */   {
/*  636 */     return this.addNewNotifications;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setAddNewNotifications(AddNewNotificationsType value)
/*      */   {
/*  648 */     this.addNewNotifications = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public UpdateNotificationsType getUpdateNotifications()
/*      */   {
/*  660 */     return this.updateNotifications;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setUpdateNotifications(UpdateNotificationsType value)
/*      */   {
/*  672 */     this.updateNotifications = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DeleteNotificationsType getDeleteNotifications()
/*      */   {
/*  684 */     return this.deleteNotifications;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setDeleteNotifications(DeleteNotificationsType value)
/*      */   {
/*  696 */     this.deleteNotifications = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public TerminateNotificationsType getTerminateNotifications()
/*      */   {
/*  708 */     return this.terminateNotifications;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTerminateNotifications(TerminateNotificationsType value)
/*      */   {
/*  720 */     this.terminateNotifications = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public InitiateNotificationsType getInitiateNotifications()
/*      */   {
/*  732 */     return this.initiateNotifications;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setInitiateNotifications(InitiateNotificationsType value)
/*      */   {
/*  744 */     this.initiateNotifications = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public SetEmailAttachmentType getSetEmailAttachment()
/*      */   {
/*  756 */     return this.setEmailAttachment;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSetEmailAttachment(SetEmailAttachmentType value)
/*      */   {
/*  768 */     this.setEmailAttachment = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetEmailAttachmentType getGetEmailAttachment()
/*      */   {
/*  780 */     return this.getEmailAttachment;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setGetEmailAttachment(GetEmailAttachmentType value)
/*      */   {
/*  792 */     this.getEmailAttachment = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetRecordedResponseType getGetRecordedResponse()
/*      */   {
/*  804 */     return this.getRecordedResponse;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setGetRecordedResponse(GetRecordedResponseType value)
/*      */   {
/*  816 */     this.getRecordedResponse = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public PrepareToRecordMessagesType getPrepareToRecordMessages()
/*      */   {
/*  828 */     return this.prepareToRecordMessages;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setPrepareToRecordMessages(PrepareToRecordMessagesType value)
/*      */   {
/*  840 */     this.prepareToRecordMessages = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public UploadVoiceFileType getUploadVoiceFile()
/*      */   {
/*  852 */     return this.uploadVoiceFile;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setUploadVoiceFile(UploadVoiceFileType value)
/*      */   {
/*  864 */     this.uploadVoiceFile = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DownloadVoiceFileType getDownloadVoiceFile()
/*      */   {
/*  876 */     return this.downloadVoiceFile;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setDownloadVoiceFile(DownloadVoiceFileType value)
/*      */   {
/*  888 */     this.downloadVoiceFile = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DeleteVoiceFilesType getDeleteVoiceFiles()
/*      */   {
/*  900 */     return this.deleteVoiceFiles;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setDeleteVoiceFiles(DeleteVoiceFilesType value)
/*      */   {
/*  912 */     this.deleteVoiceFiles = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public SearchVoiceFilesType getSearchVoiceFiles()
/*      */   {
/*  924 */     return this.searchVoiceFiles;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSearchVoiceFiles(SearchVoiceFilesType value)
/*      */   {
/*  936 */     this.searchVoiceFiles = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetNotificationReportsType getGetNotificationReports()
/*      */   {
/*  948 */     return this.getNotificationReports;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setGetNotificationReports(GetNotificationReportsType value)
/*      */   {
/*  960 */     this.getNotificationReports = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetNotificationReportRecipientsType getGetNotificationReportRecipients()
/*      */   {
/*  972 */     return this.getNotificationReportRecipients;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setGetNotificationReportRecipients(GetNotificationReportRecipientsType value)
/*      */   {
/*  984 */     this.getNotificationReportRecipients = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public OneStepNotificationType getOneStepNotification()
/*      */   {
/*  996 */     return this.oneStepNotification;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setOneStepNotification(OneStepNotificationType value)
/*      */   {
/* 1008 */     this.oneStepNotification = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public SearchNotificationsType getSearchNotifications()
/*      */   {
/* 1020 */     return this.searchNotifications;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSearchNotifications(SearchNotificationsType value)
/*      */   {
/* 1032 */     this.searchNotifications = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public AddNewNotificationGroupsType getAddNewNotificationGroups()
/*      */   {
/* 1044 */     return this.addNewNotificationGroups;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setAddNewNotificationGroups(AddNewNotificationGroupsType value)
/*      */   {
/* 1056 */     this.addNewNotificationGroups = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public UpdateNotificationGroupsType getUpdateNotificationGroups()
/*      */   {
/* 1068 */     return this.updateNotificationGroups;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setUpdateNotificationGroups(UpdateNotificationGroupsType value)
/*      */   {
/* 1080 */     this.updateNotificationGroups = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DeleteNotificationGroupsType getDeleteNotificationGroups()
/*      */   {
/* 1092 */     return this.deleteNotificationGroups;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setDeleteNotificationGroups(DeleteNotificationGroupsType value)
/*      */   {
/* 1104 */     this.deleteNotificationGroups = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public TerminateNotificationGroupsType getTerminateNotificationGroups()
/*      */   {
/* 1116 */     return this.terminateNotificationGroups;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTerminateNotificationGroups(TerminateNotificationGroupsType value)
/*      */   {
/* 1128 */     this.terminateNotificationGroups = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public InitiateNotificationGroupsType getInitiateNotificationGroups()
/*      */   {
/* 1140 */     return this.initiateNotificationGroups;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setInitiateNotificationGroups(InitiateNotificationGroupsType value)
/*      */   {
/* 1152 */     this.initiateNotificationGroups = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetNotificationGroupReportType getGetNotificationGroupReport()
/*      */   {
/* 1164 */     return this.getNotificationGroupReport;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setGetNotificationGroupReport(GetNotificationGroupReportType value)
/*      */   {
/* 1176 */     this.getNotificationGroupReport = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public SearchNotificationGroupsType getSearchNotificationGroups()
/*      */   {
/* 1188 */     return this.searchNotificationGroups;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSearchNotificationGroups(SearchNotificationGroupsType value)
/*      */   {
/* 1200 */     this.searchNotificationGroups = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public AddNewDivisionsType getAddNewDivisions()
/*      */   {
/* 1212 */     return this.addNewDivisions;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setAddNewDivisions(AddNewDivisionsType value)
/*      */   {
/* 1224 */     this.addNewDivisions = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public UpdateDivisionType getUpdateDivision()
/*      */   {
/* 1236 */     return this.updateDivision;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setUpdateDivision(UpdateDivisionType value)
/*      */   {
/* 1248 */     this.updateDivision = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetDivisionsType getGetDivisions()
/*      */   {
/* 1260 */     return this.getDivisions;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setGetDivisions(GetDivisionsType value)
/*      */   {
/* 1272 */     this.getDivisions = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetPhonePrefixesType getGetPhonePrefixes()
/*      */   {
/* 1284 */     return this.getPhonePrefixes;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setGetPhonePrefixes(GetPhonePrefixesType value)
/*      */   {
/* 1296 */     this.getPhonePrefixes = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public SetPhonePrefixesType getSetPhonePrefixes()
/*      */   {
/* 1308 */     return this.setPhonePrefixes;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSetPhonePrefixes(SetPhonePrefixesType value)
/*      */   {
/* 1320 */     this.setPhonePrefixes = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DeleteDivisionsType getDeleteDivisions()
/*      */   {
/* 1332 */     return this.deleteDivisions;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setDeleteDivisions(DeleteDivisionsType value)
/*      */   {
/* 1344 */     this.deleteDivisions = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public CreateSingleSignOnTokenType getCreateSingleSignOnToken()
/*      */   {
/* 1356 */     return this.createSingleSignOnToken;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setCreateSingleSignOnToken(CreateSingleSignOnTokenType value)
/*      */   {
/* 1368 */     this.createSingleSignOnToken = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public ValidateSingleSignOnTokenType getValidateSingleSignOnToken()
/*      */   {
/* 1380 */     return this.validateSingleSignOnToken;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setValidateSingleSignOnToken(ValidateSingleSignOnTokenType value)
/*      */   {
/* 1392 */     this.validateSingleSignOnToken = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public SetNotificationScheduleType getSetNotificationSchedule()
/*      */   {
/* 1404 */     return this.setNotificationSchedule;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSetNotificationSchedule(SetNotificationScheduleType value)
/*      */   {
/* 1416 */     this.setNotificationSchedule = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetNotificationScheduleType getGetNotificationSchedule()
/*      */   {
/* 1428 */     return this.getNotificationSchedule;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setGetNotificationSchedule(GetNotificationScheduleType value)
/*      */   {
/* 1440 */     this.getNotificationSchedule = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public AddNewRecipientSchedulesType getAddNewRecipientSchedules()
/*      */   {
/* 1452 */     return this.addNewRecipientSchedules;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setAddNewRecipientSchedules(AddNewRecipientSchedulesType value)
/*      */   {
/* 1464 */     this.addNewRecipientSchedules = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public UpdateRecipientSchedulesType getUpdateRecipientSchedules()
/*      */   {
/* 1476 */     return this.updateRecipientSchedules;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setUpdateRecipientSchedules(UpdateRecipientSchedulesType value)
/*      */   {
/* 1488 */     this.updateRecipientSchedules = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DeleteRecipientSchedulesType getDeleteRecipientSchedules()
/*      */   {
/* 1500 */     return this.deleteRecipientSchedules;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setDeleteRecipientSchedules(DeleteRecipientSchedulesType value)
/*      */   {
/* 1512 */     this.deleteRecipientSchedules = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public SearchRecipientSchedulesType getSearchRecipientSchedules()
/*      */   {
/* 1524 */     return this.searchRecipientSchedules;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSearchRecipientSchedules(SearchRecipientSchedulesType value)
/*      */   {
/* 1536 */     this.searchRecipientSchedules = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public ExecuteCustomReportType getExecuteCustomReport()
/*      */   {
/* 1548 */     return this.executeCustomReport;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setExecuteCustomReport(ExecuteCustomReportType value)
/*      */   {
/* 1560 */     this.executeCustomReport = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetVerbiageType getGetVerbiage()
/*      */   {
/* 1572 */     return this.getVerbiage;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setGetVerbiage(GetVerbiageType value)
/*      */   {
/* 1584 */     this.getVerbiage = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public SetVerbiageType getSetVerbiage()
/*      */   {
/* 1596 */     return this.setVerbiage;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSetVerbiage(SetVerbiageType value)
/*      */   {
/* 1608 */     this.setVerbiage = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public SetCustomVerbiageTypeDivisionType getSetCustomVerbiageTypeByDivision()
/*      */   {
/* 1620 */     return this.setCustomVerbiageTypeByDivision;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSetCustomVerbiageTypeByDivision(SetCustomVerbiageTypeDivisionType value)
/*      */   {
/* 1632 */     this.setCustomVerbiageTypeByDivision = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetCustomVerbiageTypeDivisionType getGetCustomVerbiageTypeByDivision()
/*      */   {
/* 1644 */     return this.getCustomVerbiageTypeByDivision;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setGetCustomVerbiageTypeByDivision(GetCustomVerbiageTypeDivisionType value)
/*      */   {
/* 1656 */     this.getCustomVerbiageTypeByDivision = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public GetLoggedInUserType getGetLoggedInUser()
/*      */   {
/* 1668 */     return this.getLoggedInUser;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setGetLoggedInUser(GetLoggedInUserType value)
/*      */   {
/* 1680 */     this.getLoggedInUser = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public IdConvertType getIdConvert()
/*      */   {
/* 1692 */     return this.idConvert;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setIdConvert(IdConvertType value)
/*      */   {
/* 1704 */     this.idConvert = value;
/*      */   }
/*      */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\WrappedRequestType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */