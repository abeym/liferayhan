/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="userDeviceType", propOrder={"user"})
/*    */ public class UserDeviceType
/*    */ {
/*    */   @XmlElement(required=true)
/*    */   protected UserDeviceDetailType user;
/*    */   
/*    */   public UserDeviceDetailType getUser()
/*    */   {
/* 47 */     return this.user;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setUser(UserDeviceDetailType value)
/*    */   {
/* 59 */     this.user = value;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\UserDeviceType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */