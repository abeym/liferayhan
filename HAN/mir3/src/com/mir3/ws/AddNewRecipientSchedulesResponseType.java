/*    */ package com.mir3.ws;
/*    */ 
/*    */ import java.util.ArrayList;
/*    */ import java.util.List;
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="addNewRecipientSchedulesResponseType", propOrder={"scheduleName"})
/*    */ public class AddNewRecipientSchedulesResponseType
/*    */ {
/*    */   protected List<String> scheduleName;
/*    */   
/*    */   public List<String> getScheduleName()
/*    */   {
/* 61 */     if (this.scheduleName == null) {
/* 62 */       this.scheduleName = new ArrayList();
/*    */     }
/* 64 */     return this.scheduleName;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\AddNewRecipientSchedulesResponseType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */