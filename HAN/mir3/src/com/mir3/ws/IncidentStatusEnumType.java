/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlEnum;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlType(name="incidentStatusEnumType")
/*    */ @XmlEnum
/*    */ public enum IncidentStatusEnumType
/*    */ {
/* 27 */   REOPEN, 
/* 28 */   CLOSE;
/*    */   
/*    */   public String value() {
/* 31 */     return name();
/*    */   }
/*    */   
/*    */   public static IncidentStatusEnumType fromValue(String v) {
/* 35 */     return valueOf(v);
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\IncidentStatusEnumType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */