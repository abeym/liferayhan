/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlEnum;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlType(name="dayOfWeekType")
/*    */ @XmlEnum
/*    */ public enum DayOfWeekType
/*    */ {
/* 35 */   DAY, 
/* 36 */   SUNDAY, 
/* 37 */   MONDAY, 
/* 38 */   TUESDAY, 
/* 39 */   WEDNESDAY, 
/* 40 */   THURSDAY, 
/* 41 */   FRIDAY, 
/* 42 */   SATURDAY, 
/* 43 */   WEEKDAY, 
/* 44 */   WEEKEND;
/*    */   
/*    */   public String value() {
/* 47 */     return name();
/*    */   }
/*    */   
/*    */   public static DayOfWeekType fromValue(String v) {
/* 51 */     return valueOf(v);
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\DayOfWeekType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */