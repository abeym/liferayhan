/*     */ package com.mir3.ws;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="addRecipientsToGroupType", propOrder={"apiVersion", "authorization", "persistData", "requireFeature", "recipientGroup", "addMembers"})
/*     */ public class AddRecipientsToGroupType
/*     */ {
/*     */   @XmlElement(required=true)
/*     */   protected String apiVersion;
/*     */   protected AuthorizationType authorization;
/*     */   protected Boolean persistData;
/*     */   protected List<String> requireFeature;
/*     */   @XmlElement(required=true)
/*     */   protected String recipientGroup;
/*     */   @XmlElement(required=true)
/*     */   protected RecipientGroupMembersType addMembers;
/*     */   
/*     */   public String getApiVersion()
/*     */   {
/*  63 */     return this.apiVersion;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setApiVersion(String value)
/*     */   {
/*  75 */     this.apiVersion = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public AuthorizationType getAuthorization()
/*     */   {
/*  87 */     return this.authorization;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAuthorization(AuthorizationType value)
/*     */   {
/*  99 */     this.authorization = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Boolean isPersistData()
/*     */   {
/* 111 */     return this.persistData;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPersistData(Boolean value)
/*     */   {
/* 123 */     this.persistData = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getRequireFeature()
/*     */   {
/* 149 */     if (this.requireFeature == null) {
/* 150 */       this.requireFeature = new ArrayList();
/*     */     }
/* 152 */     return this.requireFeature;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getRecipientGroup()
/*     */   {
/* 164 */     return this.recipientGroup;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRecipientGroup(String value)
/*     */   {
/* 176 */     this.recipientGroup = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public RecipientGroupMembersType getAddMembers()
/*     */   {
/* 188 */     return this.addMembers;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAddMembers(RecipientGroupMembersType value)
/*     */   {
/* 200 */     this.addMembers = value;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\AddRecipientsToGroupType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */