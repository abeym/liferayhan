/*      */ package com.mir3.ws;
/*      */ 
/*      */ import javax.xml.bind.annotation.XmlAccessType;
/*      */ import javax.xml.bind.annotation.XmlAccessorType;
/*      */ import javax.xml.bind.annotation.XmlType;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ @XmlAccessorType(XmlAccessType.FIELD)
/*      */ @XmlType(name="dynamicConditionSearchType", propOrder={"and", "or", "not", "addressTypeName", "address1", "address2", "building", "blackberry", "city", "company", "country", "disabledDevice", "division", "email", "employeeId", "floor", "facility", "fax", "firstName", "firstOrLast", "groupName", "groupId", "groupUUID", "lastName", "loginEnabled", "pager", "phoneNumber", "province", "role", "scheduleName", "sms", "state", "title", "username", "userId", "userUUID", "zip", "customField", "catSubcat", "priority", "severity", "roleTemplate", "timeZone", "smsOptInStatus"})
/*      */ public class DynamicConditionSearchType
/*      */ {
/*      */   protected DynamicConditionManySearchType and;
/*      */   protected DynamicConditionManySearchType or;
/*      */   protected DynamicConditionSearchType not;
/*      */   protected String addressTypeName;
/*      */   protected String address1;
/*      */   protected String address2;
/*      */   protected String building;
/*      */   protected String blackberry;
/*      */   protected String city;
/*      */   protected String company;
/*      */   protected String country;
/*      */   protected Boolean disabledDevice;
/*      */   protected String division;
/*      */   protected String email;
/*      */   protected String employeeId;
/*      */   protected String floor;
/*      */   protected String facility;
/*      */   protected String fax;
/*      */   protected String firstName;
/*      */   protected String firstOrLast;
/*      */   protected String groupName;
/*      */   protected Integer groupId;
/*      */   protected String groupUUID;
/*      */   protected String lastName;
/*      */   protected Boolean loginEnabled;
/*      */   protected String pager;
/*      */   protected String phoneNumber;
/*      */   protected String province;
/*      */   protected String role;
/*      */   protected String scheduleName;
/*      */   protected String sms;
/*      */   protected String state;
/*      */   protected String title;
/*      */   protected String username;
/*      */   protected Integer userId;
/*      */   protected String userUUID;
/*      */   protected String zip;
/*      */   protected CustomFieldType customField;
/*      */   protected CatSubcatType catSubcat;
/*      */   protected String priority;
/*      */   protected String severity;
/*      */   protected String roleTemplate;
/*      */   protected TimeZoneType timeZone;
/*      */   protected SmsOptInStatusType smsOptInStatus;
/*      */   
/*      */   public DynamicConditionManySearchType getAnd()
/*      */   {
/*  174 */     return this.and;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setAnd(DynamicConditionManySearchType value)
/*      */   {
/*  186 */     this.and = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DynamicConditionManySearchType getOr()
/*      */   {
/*  198 */     return this.or;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setOr(DynamicConditionManySearchType value)
/*      */   {
/*  210 */     this.or = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public DynamicConditionSearchType getNot()
/*      */   {
/*  222 */     return this.not;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setNot(DynamicConditionSearchType value)
/*      */   {
/*  234 */     this.not = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getAddressTypeName()
/*      */   {
/*  246 */     return this.addressTypeName;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setAddressTypeName(String value)
/*      */   {
/*  258 */     this.addressTypeName = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getAddress1()
/*      */   {
/*  270 */     return this.address1;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setAddress1(String value)
/*      */   {
/*  282 */     this.address1 = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getAddress2()
/*      */   {
/*  294 */     return this.address2;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setAddress2(String value)
/*      */   {
/*  306 */     this.address2 = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getBuilding()
/*      */   {
/*  318 */     return this.building;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setBuilding(String value)
/*      */   {
/*  330 */     this.building = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getBlackberry()
/*      */   {
/*  342 */     return this.blackberry;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setBlackberry(String value)
/*      */   {
/*  354 */     this.blackberry = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getCity()
/*      */   {
/*  366 */     return this.city;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setCity(String value)
/*      */   {
/*  378 */     this.city = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getCompany()
/*      */   {
/*  390 */     return this.company;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setCompany(String value)
/*      */   {
/*  402 */     this.company = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getCountry()
/*      */   {
/*  414 */     return this.country;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setCountry(String value)
/*      */   {
/*  426 */     this.country = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Boolean isDisabledDevice()
/*      */   {
/*  438 */     return this.disabledDevice;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setDisabledDevice(Boolean value)
/*      */   {
/*  450 */     this.disabledDevice = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getDivision()
/*      */   {
/*  462 */     return this.division;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setDivision(String value)
/*      */   {
/*  474 */     this.division = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getEmail()
/*      */   {
/*  486 */     return this.email;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setEmail(String value)
/*      */   {
/*  498 */     this.email = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getEmployeeId()
/*      */   {
/*  510 */     return this.employeeId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setEmployeeId(String value)
/*      */   {
/*  522 */     this.employeeId = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getFloor()
/*      */   {
/*  534 */     return this.floor;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setFloor(String value)
/*      */   {
/*  546 */     this.floor = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getFacility()
/*      */   {
/*  558 */     return this.facility;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setFacility(String value)
/*      */   {
/*  570 */     this.facility = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getFax()
/*      */   {
/*  582 */     return this.fax;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setFax(String value)
/*      */   {
/*  594 */     this.fax = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getFirstName()
/*      */   {
/*  606 */     return this.firstName;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setFirstName(String value)
/*      */   {
/*  618 */     this.firstName = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getFirstOrLast()
/*      */   {
/*  630 */     return this.firstOrLast;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setFirstOrLast(String value)
/*      */   {
/*  642 */     this.firstOrLast = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getGroupName()
/*      */   {
/*  654 */     return this.groupName;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setGroupName(String value)
/*      */   {
/*  666 */     this.groupName = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Integer getGroupId()
/*      */   {
/*  678 */     return this.groupId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setGroupId(Integer value)
/*      */   {
/*  690 */     this.groupId = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getGroupUUID()
/*      */   {
/*  702 */     return this.groupUUID;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setGroupUUID(String value)
/*      */   {
/*  714 */     this.groupUUID = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getLastName()
/*      */   {
/*  726 */     return this.lastName;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setLastName(String value)
/*      */   {
/*  738 */     this.lastName = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Boolean isLoginEnabled()
/*      */   {
/*  750 */     return this.loginEnabled;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setLoginEnabled(Boolean value)
/*      */   {
/*  762 */     this.loginEnabled = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getPager()
/*      */   {
/*  774 */     return this.pager;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setPager(String value)
/*      */   {
/*  786 */     this.pager = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getPhoneNumber()
/*      */   {
/*  798 */     return this.phoneNumber;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setPhoneNumber(String value)
/*      */   {
/*  810 */     this.phoneNumber = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getProvince()
/*      */   {
/*  822 */     return this.province;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setProvince(String value)
/*      */   {
/*  834 */     this.province = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getRole()
/*      */   {
/*  846 */     return this.role;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setRole(String value)
/*      */   {
/*  858 */     this.role = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getScheduleName()
/*      */   {
/*  870 */     return this.scheduleName;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setScheduleName(String value)
/*      */   {
/*  882 */     this.scheduleName = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getSms()
/*      */   {
/*  894 */     return this.sms;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSms(String value)
/*      */   {
/*  906 */     this.sms = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getState()
/*      */   {
/*  918 */     return this.state;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setState(String value)
/*      */   {
/*  930 */     this.state = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTitle()
/*      */   {
/*  942 */     return this.title;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTitle(String value)
/*      */   {
/*  954 */     this.title = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getUsername()
/*      */   {
/*  966 */     return this.username;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setUsername(String value)
/*      */   {
/*  978 */     this.username = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Integer getUserId()
/*      */   {
/*  990 */     return this.userId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setUserId(Integer value)
/*      */   {
/* 1002 */     this.userId = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getUserUUID()
/*      */   {
/* 1014 */     return this.userUUID;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setUserUUID(String value)
/*      */   {
/* 1026 */     this.userUUID = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getZip()
/*      */   {
/* 1038 */     return this.zip;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setZip(String value)
/*      */   {
/* 1050 */     this.zip = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public CustomFieldType getCustomField()
/*      */   {
/* 1062 */     return this.customField;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setCustomField(CustomFieldType value)
/*      */   {
/* 1074 */     this.customField = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public CatSubcatType getCatSubcat()
/*      */   {
/* 1086 */     return this.catSubcat;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setCatSubcat(CatSubcatType value)
/*      */   {
/* 1098 */     this.catSubcat = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getPriority()
/*      */   {
/* 1110 */     return this.priority;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setPriority(String value)
/*      */   {
/* 1122 */     this.priority = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getSeverity()
/*      */   {
/* 1134 */     return this.severity;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSeverity(String value)
/*      */   {
/* 1146 */     this.severity = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getRoleTemplate()
/*      */   {
/* 1158 */     return this.roleTemplate;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setRoleTemplate(String value)
/*      */   {
/* 1170 */     this.roleTemplate = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public TimeZoneType getTimeZone()
/*      */   {
/* 1182 */     return this.timeZone;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTimeZone(TimeZoneType value)
/*      */   {
/* 1194 */     this.timeZone = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public SmsOptInStatusType getSmsOptInStatus()
/*      */   {
/* 1206 */     return this.smsOptInStatus;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSmsOptInStatus(SmsOptInStatusType value)
/*      */   {
/* 1218 */     this.smsOptInStatus = value;
/*      */   }
/*      */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\DynamicConditionSearchType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */