/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlEnum;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlType(name="deviceCategory")
/*    */ @XmlEnum
/*    */ public enum DeviceCategory
/*    */ {
/* 37 */   PHONE, 
/* 38 */   EMAIL, 
/* 39 */   SMS, 
/* 40 */   PAGER_ONE_WAY, 
/* 41 */   PAGER_TWO_WAY, 
/* 42 */   PAGER_NUMERIC, 
/* 43 */   FAX, 
/* 44 */   TTY_PHONE, 
/* 45 */   DESKTOP, 
/* 46 */   RIM_PTWOP, 
/* 47 */   MOBILE, 
/* 48 */   DEFAULT;
/*    */   
/*    */   public String value() {
/* 51 */     return name();
/*    */   }
/*    */   
/*    */   public static DeviceCategory fromValue(String v) {
/* 55 */     return valueOf(v);
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\DeviceCategory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */