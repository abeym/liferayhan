/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="blockedDeviceStatusType", propOrder={"address", "blocked"})
/*    */ public class BlockedDeviceStatusType
/*    */ {
/*    */   @XmlElement(required=true)
/*    */   protected String address;
/*    */   protected boolean blocked;
/*    */   
/*    */   public String getAddress()
/*    */   {
/* 50 */     return this.address;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setAddress(String value)
/*    */   {
/* 62 */     this.address = value;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public boolean isBlocked()
/*    */   {
/* 70 */     return this.blocked;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setBlocked(boolean value)
/*    */   {
/* 78 */     this.blocked = value;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\BlockedDeviceStatusType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */