/*    */ package com.mir3.ws;
/*    */ 
/*    */ import java.util.ArrayList;
/*    */ import java.util.List;
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="reportCallbacksType", propOrder={"endpoint"})
/*    */ public class ReportCallbacksType
/*    */ {
/*    */   @XmlElement(required=true)
/*    */   protected List<ReportCallbackEndpointType> endpoint;
/*    */   
/*    */   public List<ReportCallbackEndpointType> getEndpoint()
/*    */   {
/* 63 */     if (this.endpoint == null) {
/* 64 */       this.endpoint = new ArrayList();
/*    */     }
/* 66 */     return this.endpoint;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\ReportCallbacksType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */