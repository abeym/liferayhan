/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="updateOneRecipientType", propOrder={})
/*    */ public class UpdateOneRecipientType
/*    */ {
/*    */   @XmlElement(required=true)
/*    */   protected RecipientType recipient;
/*    */   @XmlElement(required=true)
/*    */   protected RecipientDetailType recipientDetail;
/*    */   
/*    */   public RecipientType getRecipient()
/*    */   {
/* 50 */     return this.recipient;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setRecipient(RecipientType value)
/*    */   {
/* 62 */     this.recipient = value;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public RecipientDetailType getRecipientDetail()
/*    */   {
/* 74 */     return this.recipientDetail;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setRecipientDetail(RecipientDetailType value)
/*    */   {
/* 86 */     this.recipientDetail = value;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\UpdateOneRecipientType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */