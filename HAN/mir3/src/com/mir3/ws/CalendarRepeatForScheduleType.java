/*     */ package com.mir3.ws;
/*     */ 
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="calendarRepeatForScheduleType", propOrder={"index", "dayOfWeek", "timeUnit"})
/*     */ public class CalendarRepeatForScheduleType
/*     */ {
/*     */   @XmlElement(required=true)
/*     */   protected CalendarIndexType index;
/*     */   @XmlElement(required=true)
/*     */   protected DayOfWeekType dayOfWeek;
/*     */   @XmlElement(required=true)
/*     */   protected CalendarUnitType timeUnit;
/*     */   
/*     */   public CalendarIndexType getIndex()
/*     */   {
/*  55 */     return this.index;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setIndex(CalendarIndexType value)
/*     */   {
/*  67 */     this.index = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public DayOfWeekType getDayOfWeek()
/*     */   {
/*  79 */     return this.dayOfWeek;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDayOfWeek(DayOfWeekType value)
/*     */   {
/*  91 */     this.dayOfWeek = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public CalendarUnitType getTimeUnit()
/*     */   {
/* 103 */     return this.timeUnit;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTimeUnit(CalendarUnitType value)
/*     */   {
/* 115 */     this.timeUnit = value;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\CalendarRepeatForScheduleType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */