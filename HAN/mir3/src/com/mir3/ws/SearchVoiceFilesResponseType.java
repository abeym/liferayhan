/*     */ package com.mir3.ws;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="searchVoiceFilesResponseType", propOrder={"matchCount", "returnCount", "division", "voiceFile"})
/*     */ public class SearchVoiceFilesResponseType
/*     */ {
/*     */   protected int matchCount;
/*     */   protected int returnCount;
/*     */   @XmlElement(required=true)
/*     */   protected String division;
/*     */   protected List<VoiceFileType> voiceFile;
/*     */   
/*     */   public int getMatchCount()
/*     */   {
/*  54 */     return this.matchCount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setMatchCount(int value)
/*     */   {
/*  62 */     this.matchCount = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getReturnCount()
/*     */   {
/*  70 */     return this.returnCount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setReturnCount(int value)
/*     */   {
/*  78 */     this.returnCount = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getDivision()
/*     */   {
/*  90 */     return this.division;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDivision(String value)
/*     */   {
/* 102 */     this.division = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<VoiceFileType> getVoiceFile()
/*     */   {
/* 128 */     if (this.voiceFile == null) {
/* 129 */       this.voiceFile = new ArrayList();
/*     */     }
/* 131 */     return this.voiceFile;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\SearchVoiceFilesResponseType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */