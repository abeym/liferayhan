/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="removeRecipientsFromGroupResponseType", propOrder={"removedMembers"})
/*    */ public class RemoveRecipientsFromGroupResponseType
/*    */ {
/*    */   @XmlElement(required=true)
/*    */   protected RecipientGroupMembersType removedMembers;
/*    */   
/*    */   public RecipientGroupMembersType getRemovedMembers()
/*    */   {
/* 47 */     return this.removedMembers;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setRemovedMembers(RecipientGroupMembersType value)
/*    */   {
/* 59 */     this.removedMembers = value;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\RemoveRecipientsFromGroupResponseType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */