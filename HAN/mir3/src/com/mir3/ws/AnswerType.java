/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlEnum;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlType(name="answerType")
/*    */ @XmlEnum
/*    */ public enum AnswerType
/*    */ {
/* 31 */   MULTIPLE_CHOICE, 
/* 32 */   STANDARD_TIME, 
/* 33 */   MILITARY_TIME, 
/* 34 */   PHONE_NUMBER, 
/* 35 */   RECORDED_RESPONSE, 
/* 36 */   UNTYPED;
/*    */   
/*    */   public String value() {
/* 39 */     return name();
/*    */   }
/*    */   
/*    */   public static AnswerType fromValue(String v) {
/* 43 */     return valueOf(v);
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\AnswerType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */