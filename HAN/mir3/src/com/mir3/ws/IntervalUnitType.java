/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlEnum;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlType(name="intervalUnitType")
/*    */ @XmlEnum
/*    */ public enum IntervalUnitType
/*    */ {
/* 32 */   HOUR, 
/* 33 */   DAY, 
/* 34 */   WEEK, 
/* 35 */   MONTH, 
/* 36 */   YEAR, 
/* 37 */   WEEKDAY, 
/* 38 */   WEEKEND;
/*    */   
/*    */   public String value() {
/* 41 */     return name();
/*    */   }
/*    */   
/*    */   public static IntervalUnitType fromValue(String v) {
/* 45 */     return valueOf(v);
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\IntervalUnitType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */