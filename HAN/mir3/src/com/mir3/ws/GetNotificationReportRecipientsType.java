/*     */ package com.mir3.ws;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="getNotificationReportRecipientsType", propOrder={"apiVersion", "authorization", "persistData", "requireFeature", "notificationReportId", "notificationReportUUID", "maxResults", "startIndex", "includeDeviceContacts", "query"})
/*     */ public class GetNotificationReportRecipientsType
/*     */ {
/*     */   @XmlElement(required=true)
/*     */   protected String apiVersion;
/*     */   protected AuthorizationType authorization;
/*     */   protected Boolean persistData;
/*     */   protected List<String> requireFeature;
/*     */   protected Integer notificationReportId;
/*     */   protected String notificationReportUUID;
/*     */   protected Integer maxResults;
/*     */   protected Integer startIndex;
/*     */   protected Boolean includeDeviceContacts;
/*     */   @XmlElement(required=true)
/*     */   protected ReportRecipientsQueryType query;
/*     */   
/*     */   public String getApiVersion()
/*     */   {
/*  76 */     return this.apiVersion;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setApiVersion(String value)
/*     */   {
/*  88 */     this.apiVersion = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public AuthorizationType getAuthorization()
/*     */   {
/* 100 */     return this.authorization;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAuthorization(AuthorizationType value)
/*     */   {
/* 112 */     this.authorization = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Boolean isPersistData()
/*     */   {
/* 124 */     return this.persistData;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPersistData(Boolean value)
/*     */   {
/* 136 */     this.persistData = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getRequireFeature()
/*     */   {
/* 162 */     if (this.requireFeature == null) {
/* 163 */       this.requireFeature = new ArrayList();
/*     */     }
/* 165 */     return this.requireFeature;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getNotificationReportId()
/*     */   {
/* 177 */     return this.notificationReportId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setNotificationReportId(Integer value)
/*     */   {
/* 189 */     this.notificationReportId = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getNotificationReportUUID()
/*     */   {
/* 201 */     return this.notificationReportUUID;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setNotificationReportUUID(String value)
/*     */   {
/* 213 */     this.notificationReportUUID = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getMaxResults()
/*     */   {
/* 225 */     return this.maxResults;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setMaxResults(Integer value)
/*     */   {
/* 237 */     this.maxResults = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getStartIndex()
/*     */   {
/* 249 */     return this.startIndex;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setStartIndex(Integer value)
/*     */   {
/* 261 */     this.startIndex = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Boolean isIncludeDeviceContacts()
/*     */   {
/* 273 */     return this.includeDeviceContacts;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setIncludeDeviceContacts(Boolean value)
/*     */   {
/* 285 */     this.includeDeviceContacts = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public ReportRecipientsQueryType getQuery()
/*     */   {
/* 297 */     return this.query;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setQuery(ReportRecipientsQueryType value)
/*     */   {
/* 309 */     this.query = value;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\GetNotificationReportRecipientsType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */