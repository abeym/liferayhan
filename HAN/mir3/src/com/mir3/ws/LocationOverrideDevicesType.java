/*     */ package com.mir3.ws;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="locationOverrideDevicesType", propOrder={"workPhone", "homePhone", "mobilePhone", "workEmail", "homeEmail", "pager", "sms", "fax", "ttyPhone", "emergencyPhone", "override"})
/*     */ public class LocationOverrideDevicesType
/*     */ {
/*     */   protected String workPhone;
/*     */   protected String homePhone;
/*     */   protected String mobilePhone;
/*     */   protected String workEmail;
/*     */   protected String homeEmail;
/*     */   protected String pager;
/*     */   protected String sms;
/*     */   protected String fax;
/*     */   protected String ttyPhone;
/*     */   protected String emergencyPhone;
/*     */   protected List<OverrideType> override;
/*     */   
/*     */   public String getWorkPhone()
/*     */   {
/*  77 */     return this.workPhone;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setWorkPhone(String value)
/*     */   {
/*  89 */     this.workPhone = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getHomePhone()
/*     */   {
/* 101 */     return this.homePhone;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setHomePhone(String value)
/*     */   {
/* 113 */     this.homePhone = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getMobilePhone()
/*     */   {
/* 125 */     return this.mobilePhone;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setMobilePhone(String value)
/*     */   {
/* 137 */     this.mobilePhone = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getWorkEmail()
/*     */   {
/* 149 */     return this.workEmail;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setWorkEmail(String value)
/*     */   {
/* 161 */     this.workEmail = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getHomeEmail()
/*     */   {
/* 173 */     return this.homeEmail;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setHomeEmail(String value)
/*     */   {
/* 185 */     this.homeEmail = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getPager()
/*     */   {
/* 197 */     return this.pager;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPager(String value)
/*     */   {
/* 209 */     this.pager = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getSms()
/*     */   {
/* 221 */     return this.sms;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setSms(String value)
/*     */   {
/* 233 */     this.sms = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getFax()
/*     */   {
/* 245 */     return this.fax;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setFax(String value)
/*     */   {
/* 257 */     this.fax = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTtyPhone()
/*     */   {
/* 269 */     return this.ttyPhone;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTtyPhone(String value)
/*     */   {
/* 281 */     this.ttyPhone = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getEmergencyPhone()
/*     */   {
/* 293 */     return this.emergencyPhone;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setEmergencyPhone(String value)
/*     */   {
/* 305 */     this.emergencyPhone = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<OverrideType> getOverride()
/*     */   {
/* 331 */     if (this.override == null) {
/* 332 */       this.override = new ArrayList();
/*     */     }
/* 334 */     return this.override;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\LocationOverrideDevicesType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */