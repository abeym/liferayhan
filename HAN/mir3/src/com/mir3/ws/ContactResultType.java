/*     */ package com.mir3.ws;
/*     */ 
/*     */ import javax.xml.bind.annotation.XmlEnum;
/*     */ import javax.xml.bind.annotation.XmlEnumValue;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlType(name="contactResultType")
/*     */ @XmlEnum
/*     */ public enum ContactResultType
/*     */ {
/*  84 */   CONNECTED("CONNECTED"), 
/*  85 */   CONNECTED_COMPLETE("CONNECTED_COMPLETE"), 
/*  86 */   BUSY("BUSY"), 
/*  87 */   NETWORK_BUSY("NETWORK_BUSY"), 
/*  88 */   NO_ANSWER("NO_ANSWER"), 
/*  89 */   FAX("FAX"), 
/*  90 */   MACHINE_CONNECTED("MACHINE_CONNECTED"), 
/*  91 */   MACHINE_DISCONNECTED("MACHINE_DISCONNECTED"), 
/*  92 */   MACHINE_UNDELIVERED("MACHINE_UNDELIVERED"), 
/*  93 */   OTHER_MACHINE("OTHER_MACHINE"), 
/*  94 */   INVALID_PHONE_NUMBER("INVALID_PHONE_NUMBER"), 
/*  95 */   CALL_FAILED("CALL_FAILED"), 
/*  96 */   NO_RESOURCES("NO_RESOURCES"), 
/*  97 */   UNKNOWN_STATUS("UNKNOWN_STATUS"), 
/*  98 */   OPERATOR_INTERCEPT("OPERATOR_INTERCEPT"), 
/*  99 */   DISCONNECTED("DISCONNECTED"), 
/* 100 */   DISCONNECTED_COMPLETE("DISCONNECTED_COMPLETE"), 
/* 101 */   LEFT_MESSAGE("LEFT_MESSAGE"), 
/* 102 */   WRONG_ADDRESS("WRONG_ADDRESS"), 
/* 103 */   NOT_HERE("NOT_HERE"), 
/* 104 */   CALL_REJECTED("CALL_REJECTED"), 
/* 105 */   RESTRICTED_TELNO("RESTRICTED_TELNO"), 
/* 106 */   PAGE_SENT("PAGE_SENT"), 
/* 107 */   PAGE_REJECTED("PAGE_REJECTED"), 
/* 108 */   SMS_SENT("SMS_SENT"), 
/* 109 */   SMS_REJECTED("SMS_REJECTED"), 
/* 110 */   SMS_SERVER_ACK("SMS_SERVER_ACK"), 
/* 111 */   SMS_SERVER_C_ACK("SMS_SERVER_C_ACK"), 
/* 112 */   SMS_HANDSET_ACK("SMS_HANDSET_ACK"), 
/* 113 */   SMS_SECURE_HANDSET_ACK("SMS_SECURE_HANDSET_ACK"), 
/* 114 */   SMS_SECURE_OPEN_ACK("SMS_SECURE_OPEN_ACK"), 
/* 115 */   SMS_INVALID_RESPONSE("SMS_INVALID_RESPONSE"), 
/* 116 */   NOTIFICATION_RETRIEVAL("NOTIFICATION_RETRIEVAL"), 
/* 117 */   INVALID_RESPONSE("INVALID_RESPONSE"), 
/* 118 */   EMAIL_SENT("EMAIL_SENT"), 
/* 119 */   AUTO_RESPONSE("AUTO_RESPONSE"), 
/* 120 */   FAX_SENT("FAX_SENT"), 
/* 121 */   DESKTOP_SENT("DESKTOP_SENT"), 
/* 122 */   RESPONDED("RESPONDED"), 
/* 123 */   SENDING_FAILED("SENDING_FAILED"), 
/* 124 */   CONFIRMATION("CONFIRMATION"), 
/* 125 */   TDD_CONNECTED_PARTIAL("TDD_CONNECTED_PARTIAL"), 
/* 126 */   TDD_CONNECTED_COMPLETE("TDD_CONNECTED_COMPLETE"), 
/* 127 */   TDD_DISCONNECTED_PARTIAL("TDD_DISCONNECTED_PARTIAL"), 
/* 128 */   TDD_DISCONNECTED_COMPLETE("TDD_DISCONNECTED_COMPLETE"), 
/* 129 */   TDD_UNDELIVERED("TDD_UNDELIVERED"), 
/* 130 */   SMS_DEACTIVATED("SMS_DEACTIVATED"), 
/* 131 */   NOT_OPTED_IN("NOT_OPTED_IN"), 
/* 132 */   RIMP_2_P_SENT(
/* 133 */     "RIMP2P_SENT"), 
/* 134 */   RIMP_2_P_REJECTED(
/* 135 */     "RIMP2P_REJECTED"), 
/* 136 */   DIALING("DIALING"), 
/* 137 */   RINGING("RINGING"), 
/* 138 */   HUMAN_DETECTED("HUMAN_DETECTED"), 
/* 139 */   VOICEMAIL_DETECTED("VOICEMAIL_DETECTED"), 
/* 140 */   IDENTITY_VALIDATED("IDENTITY_VALIDATED"), 
/* 141 */   MOBILEAPP_SENT("MOBILEAPP_SENT"), 
/* 142 */   MOBILEAPP_REJECTED("MOBILEAPP_REJECTED"), 
/* 143 */   IDENTICAL_SUPPRESSED("IDENTICAL_SUPPRESSED");
/*     */   
/*     */   private final String value;
/*     */   
/* 147 */   private ContactResultType(String v) { this.value = v; }
/*     */   
/*     */ 
/*     */ 
/* 151 */   public String value() { return this.value; }
/*     */   
/*     */   public static ContactResultType fromValue(String v) {
/*     */     ContactResultType[] arrayOfContactResultType;
/* 155 */     int j = (arrayOfContactResultType = values()).length; for (int i = 0; i < j; i++) { ContactResultType c = arrayOfContactResultType[i];
/* 156 */       if (c.value.equals(v)) {
/* 157 */         return c;
/*     */       }
/*     */     }
/* 160 */     throw new IllegalArgumentException(v);
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\ContactResultType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */