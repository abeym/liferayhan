/*    */ package com.mir3.ws;
/*    */ 
/*    */ import java.util.ArrayList;
/*    */ import java.util.List;
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="notificationGroupDetailType", propOrder={"notificationGroupTitle", "notification"})
/*    */ public class NotificationGroupDetailType
/*    */ {
/*    */   @XmlElement(required=true)
/*    */   protected String notificationGroupTitle;
/*    */   protected List<String> notification;
/*    */   
/*    */   public String getNotificationGroupTitle()
/*    */   {
/* 52 */     return this.notificationGroupTitle;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setNotificationGroupTitle(String value)
/*    */   {
/* 64 */     this.notificationGroupTitle = value;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public List<String> getNotification()
/*    */   {
/* 90 */     if (this.notification == null) {
/* 91 */       this.notification = new ArrayList();
/*    */     }
/* 93 */     return this.notification;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\NotificationGroupDetailType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */