/*     */ package com.mir3.ws;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="notificationGroupManySearchType", propOrder={"and", "or", "not", "notificationGroupTitle", "notificationTitle"})
/*     */ public class NotificationGroupManySearchType
/*     */ {
/*     */   protected List<NotificationGroupManySearchType> and;
/*     */   protected List<NotificationGroupManySearchType> or;
/*     */   protected List<NotificationGroupOneSearchType> not;
/*     */   protected List<String> notificationGroupTitle;
/*     */   protected List<String> notificationTitle;
/*     */   
/*     */   public List<NotificationGroupManySearchType> getAnd()
/*     */   {
/*  73 */     if (this.and == null) {
/*  74 */       this.and = new ArrayList();
/*     */     }
/*  76 */     return this.and;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<NotificationGroupManySearchType> getOr()
/*     */   {
/* 102 */     if (this.or == null) {
/* 103 */       this.or = new ArrayList();
/*     */     }
/* 105 */     return this.or;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<NotificationGroupOneSearchType> getNot()
/*     */   {
/* 131 */     if (this.not == null) {
/* 132 */       this.not = new ArrayList();
/*     */     }
/* 134 */     return this.not;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getNotificationGroupTitle()
/*     */   {
/* 160 */     if (this.notificationGroupTitle == null) {
/* 161 */       this.notificationGroupTitle = new ArrayList();
/*     */     }
/* 163 */     return this.notificationGroupTitle;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getNotificationTitle()
/*     */   {
/* 189 */     if (this.notificationTitle == null) {
/* 190 */       this.notificationTitle = new ArrayList();
/*     */     }
/* 192 */     return this.notificationTitle;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\NotificationGroupManySearchType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */