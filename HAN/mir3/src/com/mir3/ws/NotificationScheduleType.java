/*     */ package com.mir3.ws;
/*     */ 
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlSchemaType;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ import javax.xml.datatype.XMLGregorianCalendar;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="notificationScheduleType", propOrder={"stopDateTime", "initiator", "scheduleTimeZone", "oneTimeSend", "intervalRepeat", "calendarRepeat", "dayRepeat"})
/*     */ public class NotificationScheduleType
/*     */ {
/*     */   @XmlSchemaType(name="dateTime")
/*     */   protected XMLGregorianCalendar stopDateTime;
/*     */   protected RecipientType initiator;
/*     */   protected TimeZoneType scheduleTimeZone;
/*     */   protected OneTimeSendType oneTimeSend;
/*     */   protected IntervalRepeatType intervalRepeat;
/*     */   protected CalendarRepeatType calendarRepeat;
/*     */   protected DayRepeatType dayRepeat;
/*     */   
/*     */   public XMLGregorianCalendar getStopDateTime()
/*     */   {
/*  68 */     return this.stopDateTime;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setStopDateTime(XMLGregorianCalendar value)
/*     */   {
/*  80 */     this.stopDateTime = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public RecipientType getInitiator()
/*     */   {
/*  92 */     return this.initiator;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setInitiator(RecipientType value)
/*     */   {
/* 104 */     this.initiator = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public TimeZoneType getScheduleTimeZone()
/*     */   {
/* 116 */     return this.scheduleTimeZone;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setScheduleTimeZone(TimeZoneType value)
/*     */   {
/* 128 */     this.scheduleTimeZone = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public OneTimeSendType getOneTimeSend()
/*     */   {
/* 140 */     return this.oneTimeSend;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setOneTimeSend(OneTimeSendType value)
/*     */   {
/* 152 */     this.oneTimeSend = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public IntervalRepeatType getIntervalRepeat()
/*     */   {
/* 164 */     return this.intervalRepeat;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setIntervalRepeat(IntervalRepeatType value)
/*     */   {
/* 176 */     this.intervalRepeat = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public CalendarRepeatType getCalendarRepeat()
/*     */   {
/* 188 */     return this.calendarRepeat;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCalendarRepeat(CalendarRepeatType value)
/*     */   {
/* 200 */     this.calendarRepeat = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public DayRepeatType getDayRepeat()
/*     */   {
/* 212 */     return this.dayRepeat;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDayRepeat(DayRepeatType value)
/*     */   {
/* 224 */     this.dayRepeat = value;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\NotificationScheduleType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */