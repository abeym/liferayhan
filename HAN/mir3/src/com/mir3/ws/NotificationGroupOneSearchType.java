/*     */ package com.mir3.ws;
/*     */ 
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="notificationGroupOneSearchType", propOrder={"and", "or", "not", "notificationGroupTitle", "notificationTitle"})
/*     */ public class NotificationGroupOneSearchType
/*     */ {
/*     */   protected NotificationGroupManySearchType and;
/*     */   protected NotificationGroupManySearchType or;
/*     */   protected NotificationGroupOneSearchType not;
/*     */   protected String notificationGroupTitle;
/*     */   protected String notificationTitle;
/*     */   
/*     */   public NotificationGroupManySearchType getAnd()
/*     */   {
/*  57 */     return this.and;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAnd(NotificationGroupManySearchType value)
/*     */   {
/*  69 */     this.and = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public NotificationGroupManySearchType getOr()
/*     */   {
/*  81 */     return this.or;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setOr(NotificationGroupManySearchType value)
/*     */   {
/*  93 */     this.or = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public NotificationGroupOneSearchType getNot()
/*     */   {
/* 105 */     return this.not;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setNot(NotificationGroupOneSearchType value)
/*     */   {
/* 117 */     this.not = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getNotificationGroupTitle()
/*     */   {
/* 129 */     return this.notificationGroupTitle;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setNotificationGroupTitle(String value)
/*     */   {
/* 141 */     this.notificationGroupTitle = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getNotificationTitle()
/*     */   {
/* 153 */     return this.notificationTitle;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setNotificationTitle(String value)
/*     */   {
/* 165 */     this.notificationTitle = value;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\NotificationGroupOneSearchType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */