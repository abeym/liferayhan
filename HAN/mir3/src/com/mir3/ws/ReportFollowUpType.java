/*     */ package com.mir3.ws;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="reportFollowUpType", propOrder={"followUpReportId", "followUpReportUUID", "question", "verbiage", "type", "choice"})
/*     */ public class ReportFollowUpType
/*     */ {
/*     */   protected Integer followUpReportId;
/*     */   protected String followUpReportUUID;
/*     */   protected String question;
/*     */   protected VerbiagePerMessageType verbiage;
/*     */   @XmlElement(required=true)
/*     */   protected AnswerType type;
/*     */   protected List<ReportFollowUpChoiceType> choice;
/*     */   
/*     */   public Integer getFollowUpReportId()
/*     */   {
/*  64 */     return this.followUpReportId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setFollowUpReportId(Integer value)
/*     */   {
/*  76 */     this.followUpReportId = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getFollowUpReportUUID()
/*     */   {
/*  88 */     return this.followUpReportUUID;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setFollowUpReportUUID(String value)
/*     */   {
/* 100 */     this.followUpReportUUID = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getQuestion()
/*     */   {
/* 112 */     return this.question;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setQuestion(String value)
/*     */   {
/* 124 */     this.question = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public VerbiagePerMessageType getVerbiage()
/*     */   {
/* 136 */     return this.verbiage;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setVerbiage(VerbiagePerMessageType value)
/*     */   {
/* 148 */     this.verbiage = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public AnswerType getType()
/*     */   {
/* 160 */     return this.type;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setType(AnswerType value)
/*     */   {
/* 172 */     this.type = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<ReportFollowUpChoiceType> getChoice()
/*     */   {
/* 198 */     if (this.choice == null) {
/* 199 */       this.choice = new ArrayList();
/*     */     }
/* 201 */     return this.choice;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\ReportFollowUpType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */