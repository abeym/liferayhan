/*    */ package com.mir3.ws;
/*    */ 
/*    */ import java.util.ArrayList;
/*    */ import java.util.List;
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="verbiagePerMessageType", propOrder={"text", "recordingTitle"})
/*    */ public class VerbiagePerMessageType
/*    */ {
/*    */   @XmlElement(required=true)
/*    */   protected List<StringWithMessageTypeAndLocale> text;
/*    */   protected List<StringWithMessageTypeAndLocale> recordingTitle;
/*    */   
/*    */   public List<StringWithMessageTypeAndLocale> getText()
/*    */   {
/* 66 */     if (this.text == null) {
/* 67 */       this.text = new ArrayList();
/*    */     }
/* 69 */     return this.text;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public List<StringWithMessageTypeAndLocale> getRecordingTitle()
/*    */   {
/* 95 */     if (this.recordingTitle == null) {
/* 96 */       this.recordingTitle = new ArrayList();
/*    */     }
/* 98 */     return this.recordingTitle;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\VerbiagePerMessageType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */