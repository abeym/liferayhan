/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlEnum;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlType(name="ruleExceptionType")
/*    */ @XmlEnum
/*    */ public enum RuleExceptionType
/*    */ {
/* 28 */   NON_EXCEPTION, 
/* 29 */   REPLACEMENT_EXCEPTION, 
/* 30 */   TEMPORARY_EXCEPTION;
/*    */   
/*    */   public String value() {
/* 33 */     return name();
/*    */   }
/*    */   
/*    */   public static RuleExceptionType fromValue(String v) {
/* 37 */     return valueOf(v);
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\RuleExceptionType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */