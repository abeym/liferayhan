/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlEnum;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlType(name="calendarIndexType")
/*    */ @XmlEnum
/*    */ public enum CalendarIndexType
/*    */ {
/* 30 */   FIRST, 
/* 31 */   SECOND, 
/* 32 */   THIRD, 
/* 33 */   FOURTH, 
/* 34 */   LAST;
/*    */   
/*    */   public String value() {
/* 37 */     return name();
/*    */   }
/*    */   
/*    */   public static CalendarIndexType fromValue(String v) {
/* 41 */     return valueOf(v);
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\CalendarIndexType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */