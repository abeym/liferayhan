/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="getCustomVerbiageTypeByDivisionResponseType", propOrder={"customVerbiageTypeDivision"})
/*    */ public class GetCustomVerbiageTypeByDivisionResponseType
/*    */ {
/*    */   @XmlElement(required=true)
/*    */   protected CustomVerbiageTypeDivision customVerbiageTypeDivision;
/*    */   
/*    */   public CustomVerbiageTypeDivision getCustomVerbiageTypeDivision()
/*    */   {
/* 47 */     return this.customVerbiageTypeDivision;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setCustomVerbiageTypeDivision(CustomVerbiageTypeDivision value)
/*    */   {
/* 59 */     this.customVerbiageTypeDivision = value;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\GetCustomVerbiageTypeByDivisionResponseType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */