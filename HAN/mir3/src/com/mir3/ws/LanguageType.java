/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlEnum;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlType(name="languageType")
/*    */ @XmlEnum
/*    */ public enum LanguageType
/*    */ {
/* 26 */   ENGLISH;
/*    */   
/*    */   public String value() {
/* 29 */     return name();
/*    */   }
/*    */   
/*    */   public static LanguageType fromValue(String v) {
/* 33 */     return valueOf(v);
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\LanguageType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */