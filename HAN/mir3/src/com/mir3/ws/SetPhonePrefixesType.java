/*     */ package com.mir3.ws;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="setPhonePrefixesType", propOrder={"apiVersion", "authorization", "persistData", "requireFeature", "phonePrefix"})
/*     */ public class SetPhonePrefixesType
/*     */ {
/*     */   @XmlElement(required=true)
/*     */   protected String apiVersion;
/*     */   protected AuthorizationType authorization;
/*     */   protected Boolean persistData;
/*     */   protected List<String> requireFeature;
/*     */   protected List<PhonePrefixType> phonePrefix;
/*     */   
/*     */   public String getApiVersion()
/*     */   {
/*  58 */     return this.apiVersion;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setApiVersion(String value)
/*     */   {
/*  70 */     this.apiVersion = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public AuthorizationType getAuthorization()
/*     */   {
/*  82 */     return this.authorization;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAuthorization(AuthorizationType value)
/*     */   {
/*  94 */     this.authorization = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Boolean isPersistData()
/*     */   {
/* 106 */     return this.persistData;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPersistData(Boolean value)
/*     */   {
/* 118 */     this.persistData = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getRequireFeature()
/*     */   {
/* 144 */     if (this.requireFeature == null) {
/* 145 */       this.requireFeature = new ArrayList();
/*     */     }
/* 147 */     return this.requireFeature;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<PhonePrefixType> getPhonePrefix()
/*     */   {
/* 173 */     if (this.phonePrefix == null) {
/* 174 */       this.phonePrefix = new ArrayList();
/*     */     }
/* 176 */     return this.phonePrefix;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\SetPhonePrefixesType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */