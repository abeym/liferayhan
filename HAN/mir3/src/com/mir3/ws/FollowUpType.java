/*     */ package com.mir3.ws;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="followUpType", propOrder={"question", "recordedFollowUpTitle", "verbiage", "answerType", "choice"})
/*     */ public class FollowUpType
/*     */ {
/*     */   protected String question;
/*     */   protected String recordedFollowUpTitle;
/*     */   protected VerbiagePerMessageType verbiage;
/*     */   @XmlElement(required=true)
/*     */   protected AnswerType answerType;
/*     */   protected List<ResponseChoiceType> choice;
/*     */   
/*     */   public String getQuestion()
/*     */   {
/*  61 */     return this.question;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setQuestion(String value)
/*     */   {
/*  73 */     this.question = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getRecordedFollowUpTitle()
/*     */   {
/*  85 */     return this.recordedFollowUpTitle;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRecordedFollowUpTitle(String value)
/*     */   {
/*  97 */     this.recordedFollowUpTitle = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public VerbiagePerMessageType getVerbiage()
/*     */   {
/* 109 */     return this.verbiage;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setVerbiage(VerbiagePerMessageType value)
/*     */   {
/* 121 */     this.verbiage = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public AnswerType getAnswerType()
/*     */   {
/* 133 */     return this.answerType;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAnswerType(AnswerType value)
/*     */   {
/* 145 */     this.answerType = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<ResponseChoiceType> getChoice()
/*     */   {
/* 171 */     if (this.choice == null) {
/* 172 */       this.choice = new ArrayList();
/*     */     }
/* 174 */     return this.choice;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\FollowUpType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */