/*    */ package com.mir3.ws;
/*    */ 
/*    */ import java.util.ArrayList;
/*    */ import java.util.List;
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElements;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="reportRowType", propOrder={"reportRowGroup"})
/*    */ public class ReportRowType
/*    */ {
/*    */   @XmlElements({@javax.xml.bind.annotation.XmlElement(name="dateValue", type=javax.xml.datatype.XMLGregorianCalendar.class), @javax.xml.bind.annotation.XmlElement(name="intValue", type=Integer.class), @javax.xml.bind.annotation.XmlElement(name="value", type=String.class)})
/*    */   protected List<Object> reportRowGroup;
/*    */   
/*    */   public List<Object> getReportRowGroup()
/*    */   {
/* 69 */     if (this.reportRowGroup == null) {
/* 70 */       this.reportRowGroup = new ArrayList();
/*    */     }
/* 72 */     return this.reportRowGroup;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\ReportRowType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */