/*     */ package com.mir3.ws;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="searchRecipientsResponseType", propOrder={"matchCount", "returnCount", "recipientDetail", "recipient"})
/*     */ public class SearchRecipientsResponseType
/*     */ {
/*     */   protected int matchCount;
/*     */   protected int returnCount;
/*     */   protected List<RecipientDetailType> recipientDetail;
/*     */   protected List<RecipientType> recipient;
/*     */   
/*     */   public int getMatchCount()
/*     */   {
/*  52 */     return this.matchCount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setMatchCount(int value)
/*     */   {
/*  60 */     this.matchCount = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getReturnCount()
/*     */   {
/*  68 */     return this.returnCount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setReturnCount(int value)
/*     */   {
/*  76 */     this.returnCount = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<RecipientDetailType> getRecipientDetail()
/*     */   {
/* 102 */     if (this.recipientDetail == null) {
/* 103 */       this.recipientDetail = new ArrayList();
/*     */     }
/* 105 */     return this.recipientDetail;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<RecipientType> getRecipient()
/*     */   {
/* 131 */     if (this.recipient == null) {
/* 132 */       this.recipient = new ArrayList();
/*     */     }
/* 134 */     return this.recipient;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\SearchRecipientsResponseType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */