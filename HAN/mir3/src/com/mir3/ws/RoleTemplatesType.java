/*    */ package com.mir3.ws;
/*    */ 
/*    */ import java.util.ArrayList;
/*    */ import java.util.List;
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="roleTemplatesType", propOrder={"roleTemplate"})
/*    */ public class RoleTemplatesType
/*    */ {
/*    */   @XmlElement(required=true)
/*    */   protected List<RoleTemplateType> roleTemplate;
/*    */   
/*    */   public List<RoleTemplateType> getRoleTemplate()
/*    */   {
/* 63 */     if (this.roleTemplate == null) {
/* 64 */       this.roleTemplate = new ArrayList();
/*    */     }
/* 66 */     return this.roleTemplate;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\RoleTemplatesType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */