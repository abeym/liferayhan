/*     */ package com.mir3.ws;
/*     */ 
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="initiateOneNotificationType", propOrder={})
/*     */ public class InitiateOneNotificationType
/*     */ {
/*     */   @XmlElement(required=true)
/*     */   protected String notification;
/*     */   protected String activeIncident;
/*     */   protected String additionalText;
/*     */   protected String userData;
/*     */   protected String userUniqueId;
/*     */   protected DynamicRecipientsType dynamicRecipients;
/*     */   protected PlaceholderValuesType placeholderValues;
/*     */   
/*     */   public String getNotification()
/*     */   {
/*  59 */     return this.notification;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setNotification(String value)
/*     */   {
/*  71 */     this.notification = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getActiveIncident()
/*     */   {
/*  83 */     return this.activeIncident;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setActiveIncident(String value)
/*     */   {
/*  95 */     this.activeIncident = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getAdditionalText()
/*     */   {
/* 107 */     return this.additionalText;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAdditionalText(String value)
/*     */   {
/* 119 */     this.additionalText = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getUserData()
/*     */   {
/* 131 */     return this.userData;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setUserData(String value)
/*     */   {
/* 143 */     this.userData = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getUserUniqueId()
/*     */   {
/* 155 */     return this.userUniqueId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setUserUniqueId(String value)
/*     */   {
/* 167 */     this.userUniqueId = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public DynamicRecipientsType getDynamicRecipients()
/*     */   {
/* 179 */     return this.dynamicRecipients;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDynamicRecipients(DynamicRecipientsType value)
/*     */   {
/* 191 */     this.dynamicRecipients = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public PlaceholderValuesType getPlaceholderValues()
/*     */   {
/* 203 */     return this.placeholderValues;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPlaceholderValues(PlaceholderValuesType value)
/*     */   {
/* 215 */     this.placeholderValues = value;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\InitiateOneNotificationType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */