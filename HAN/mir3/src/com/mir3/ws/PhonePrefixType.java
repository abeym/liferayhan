/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="phonePrefixType", propOrder={"prefix", "ports"})
/*    */ public class PhonePrefixType
/*    */ {
/*    */   @XmlElement(required=true)
/*    */   protected String prefix;
/*    */   protected int ports;
/*    */   
/*    */   public String getPrefix()
/*    */   {
/* 50 */     return this.prefix;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setPrefix(String value)
/*    */   {
/* 62 */     this.prefix = value;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public int getPorts()
/*    */   {
/* 70 */     return this.ports;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setPorts(int value)
/*    */   {
/* 78 */     this.ports = value;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\PhonePrefixType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */