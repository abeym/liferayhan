/*     */ package com.mir3.ws;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="devicesType", propOrder={"workPhone", "homePhone", "mobilePhone", "ttyPhone", "workEmail", "homeEmail", "numericPager", "onewayPager", "twowayPager", "fax", "sms", "emergencyPhone", "device"})
/*     */ public class DevicesType
/*     */ {
/*     */   protected List<PhoneType> workPhone;
/*     */   protected List<PhoneType> homePhone;
/*     */   protected List<PhoneType> mobilePhone;
/*     */   protected List<PhoneType> ttyPhone;
/*     */   protected List<EmailAndFaxType> workEmail;
/*     */   protected List<EmailAndFaxType> homeEmail;
/*     */   protected List<PagerType> numericPager;
/*     */   protected List<PagerType> onewayPager;
/*     */   protected List<PagerType> twowayPager;
/*     */   protected List<EmailAndFaxType> fax;
/*     */   protected List<PhoneType> sms;
/*     */   protected List<PhoneType> emergencyPhone;
/*     */   protected List<Device> device;
/*     */   
/*     */   public List<PhoneType> getWorkPhone()
/*     */   {
/*  97 */     if (this.workPhone == null) {
/*  98 */       this.workPhone = new ArrayList();
/*     */     }
/* 100 */     return this.workPhone;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<PhoneType> getHomePhone()
/*     */   {
/* 126 */     if (this.homePhone == null) {
/* 127 */       this.homePhone = new ArrayList();
/*     */     }
/* 129 */     return this.homePhone;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<PhoneType> getMobilePhone()
/*     */   {
/* 155 */     if (this.mobilePhone == null) {
/* 156 */       this.mobilePhone = new ArrayList();
/*     */     }
/* 158 */     return this.mobilePhone;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<PhoneType> getTtyPhone()
/*     */   {
/* 184 */     if (this.ttyPhone == null) {
/* 185 */       this.ttyPhone = new ArrayList();
/*     */     }
/* 187 */     return this.ttyPhone;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<EmailAndFaxType> getWorkEmail()
/*     */   {
/* 213 */     if (this.workEmail == null) {
/* 214 */       this.workEmail = new ArrayList();
/*     */     }
/* 216 */     return this.workEmail;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<EmailAndFaxType> getHomeEmail()
/*     */   {
/* 242 */     if (this.homeEmail == null) {
/* 243 */       this.homeEmail = new ArrayList();
/*     */     }
/* 245 */     return this.homeEmail;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<PagerType> getNumericPager()
/*     */   {
/* 271 */     if (this.numericPager == null) {
/* 272 */       this.numericPager = new ArrayList();
/*     */     }
/* 274 */     return this.numericPager;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<PagerType> getOnewayPager()
/*     */   {
/* 300 */     if (this.onewayPager == null) {
/* 301 */       this.onewayPager = new ArrayList();
/*     */     }
/* 303 */     return this.onewayPager;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<PagerType> getTwowayPager()
/*     */   {
/* 329 */     if (this.twowayPager == null) {
/* 330 */       this.twowayPager = new ArrayList();
/*     */     }
/* 332 */     return this.twowayPager;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<EmailAndFaxType> getFax()
/*     */   {
/* 358 */     if (this.fax == null) {
/* 359 */       this.fax = new ArrayList();
/*     */     }
/* 361 */     return this.fax;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<PhoneType> getSms()
/*     */   {
/* 387 */     if (this.sms == null) {
/* 388 */       this.sms = new ArrayList();
/*     */     }
/* 390 */     return this.sms;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<PhoneType> getEmergencyPhone()
/*     */   {
/* 416 */     if (this.emergencyPhone == null) {
/* 417 */       this.emergencyPhone = new ArrayList();
/*     */     }
/* 419 */     return this.emergencyPhone;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<Device> getDevice()
/*     */   {
/* 445 */     if (this.device == null) {
/* 446 */       this.device = new ArrayList();
/*     */     }
/* 448 */     return this.device;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\DevicesType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */