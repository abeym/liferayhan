/*    */ package com.mir3.ws;
/*    */ 
/*    */ import java.util.ArrayList;
/*    */ import java.util.List;
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="dynamicRecipientsType", propOrder={"recipientDetail"})
/*    */ public class DynamicRecipientsType
/*    */ {
/*    */   protected List<RecipientDetailType> recipientDetail;
/*    */   
/*    */   public List<RecipientDetailType> getRecipientDetail()
/*    */   {
/* 61 */     if (this.recipientDetail == null) {
/* 62 */       this.recipientDetail = new ArrayList();
/*    */     }
/* 64 */     return this.recipientDetail;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\DynamicRecipientsType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */