/*     */ package com.mir3.ws;
/*     */ 
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="getAvailableTopicsResponseType", propOrder={"categories", "priorities", "severities"})
/*     */ public class GetAvailableTopicsResponseType
/*     */ {
/*     */   @XmlElement(required=true)
/*     */   protected CategoriesType categories;
/*     */   @XmlElement(required=true)
/*     */   protected PrioritiesType priorities;
/*     */   @XmlElement(required=true)
/*     */   protected SeveritiesType severities;
/*     */   
/*     */   public CategoriesType getCategories()
/*     */   {
/*  55 */     return this.categories;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCategories(CategoriesType value)
/*     */   {
/*  67 */     this.categories = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public PrioritiesType getPriorities()
/*     */   {
/*  79 */     return this.priorities;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPriorities(PrioritiesType value)
/*     */   {
/*  91 */     this.priorities = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public SeveritiesType getSeverities()
/*     */   {
/* 103 */     return this.severities;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setSeverities(SeveritiesType value)
/*     */   {
/* 115 */     this.severities = value;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\GetAvailableTopicsResponseType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */