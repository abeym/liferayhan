/*    */ package com.mir3.ws;
/*    */ 
/*    */ import java.util.ArrayList;
/*    */ import java.util.List;
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="suppressedSubscriptionsType", propOrder={"suppressedSubscription"})
/*    */ public class SuppressedSubscriptionsType
/*    */ {
/*    */   protected List<SuppressedSubscriptionType> suppressedSubscription;
/*    */   
/*    */   public List<SuppressedSubscriptionType> getSuppressedSubscription()
/*    */   {
/* 61 */     if (this.suppressedSubscription == null) {
/* 62 */       this.suppressedSubscription = new ArrayList();
/*    */     }
/* 64 */     return this.suppressedSubscription;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\SuppressedSubscriptionsType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */