/*     */ package com.mir3.ws;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="searchReportsResponseType", propOrder={"matchCount", "returnCount", "searchResult"})
/*     */ public class SearchReportsResponseType
/*     */ {
/*     */   protected int matchCount;
/*     */   protected int returnCount;
/*     */   protected List<SearchReportsResultType> searchResult;
/*     */   
/*     */   public int getMatchCount()
/*     */   {
/*  49 */     return this.matchCount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setMatchCount(int value)
/*     */   {
/*  57 */     this.matchCount = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getReturnCount()
/*     */   {
/*  65 */     return this.returnCount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setReturnCount(int value)
/*     */   {
/*  73 */     this.returnCount = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<SearchReportsResultType> getSearchResult()
/*     */   {
/*  99 */     if (this.searchResult == null) {
/* 100 */       this.searchResult = new ArrayList();
/*     */     }
/* 102 */     return this.searchResult;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\SearchReportsResponseType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */