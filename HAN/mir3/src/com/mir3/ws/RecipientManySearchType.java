/*      */ package com.mir3.ws;
/*      */ 
/*      */ import java.util.ArrayList;
/*      */ import java.util.List;
/*      */ import javax.xml.bind.annotation.XmlAccessType;
/*      */ import javax.xml.bind.annotation.XmlAccessorType;
/*      */ import javax.xml.bind.annotation.XmlElement;
/*      */ import javax.xml.bind.annotation.XmlSchemaType;
/*      */ import javax.xml.bind.annotation.XmlType;
/*      */ import javax.xml.datatype.XMLGregorianCalendar;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ @XmlAccessorType(XmlAccessType.FIELD)
/*      */ @XmlType(name="recipientManySearchType", propOrder={"and", "or", "not", "firstName", "lastName", "username", "userId", "userUUID", "groupTitle", "title", "company", "division", "addressTypeName", "address1", "address2", "building", "floor", "city", "state", "zip", "facility", "country", "province", "phoneNumber", "pager", "email", "fax", "sms", "role", "deviceType", "disabledDevice", "loginEnabled", "lastModifiedBefore", "lastModifiedAfter", "customField", "employeeId", "permission", "catSubcat", "priority", "severity", "roleTemplate", "timeZone", "geocodeStatus", "smsOptInStatus"})
/*      */ public class RecipientManySearchType
/*      */ {
/*      */   protected List<RecipientManySearchType> and;
/*      */   protected List<RecipientManySearchType> or;
/*      */   protected List<RecipientOneSearchType> not;
/*      */   protected List<String> firstName;
/*      */   protected List<String> lastName;
/*      */   protected List<String> username;
/*      */   @XmlElement(type=Integer.class)
/*      */   protected List<Integer> userId;
/*      */   protected List<String> userUUID;
/*      */   protected List<String> groupTitle;
/*      */   protected List<String> title;
/*      */   protected List<String> company;
/*      */   protected List<String> division;
/*      */   protected List<String> addressTypeName;
/*      */   protected List<String> address1;
/*      */   protected List<String> address2;
/*      */   protected List<String> building;
/*      */   protected List<String> floor;
/*      */   protected List<String> city;
/*      */   protected List<String> state;
/*      */   protected List<String> zip;
/*      */   protected List<String> facility;
/*      */   protected List<String> country;
/*      */   protected List<String> province;
/*      */   protected List<String> phoneNumber;
/*      */   protected List<String> pager;
/*      */   protected List<String> email;
/*      */   protected List<String> fax;
/*      */   protected List<String> sms;
/*      */   protected List<String> role;
/*      */   protected List<String> deviceType;
/*      */   @XmlElement(type=Boolean.class)
/*      */   protected List<Boolean> disabledDevice;
/*      */   @XmlElement(type=Boolean.class)
/*      */   protected List<Boolean> loginEnabled;
/*      */   @XmlSchemaType(name="dateTime")
/*      */   protected List<XMLGregorianCalendar> lastModifiedBefore;
/*      */   @XmlSchemaType(name="dateTime")
/*      */   protected List<XMLGregorianCalendar> lastModifiedAfter;
/*      */   protected List<CustomFieldType> customField;
/*      */   protected List<String> employeeId;
/*      */   protected List<String> permission;
/*      */   protected List<CatSubcatType> catSubcat;
/*      */   protected List<String> priority;
/*      */   protected List<String> severity;
/*      */   protected List<String> roleTemplate;
/*      */   protected List<TimeZoneType> timeZone;
/*      */   protected List<GeocodeStatusEnumType> geocodeStatus;
/*      */   protected List<SmsOptInStatusType> smsOptInStatus;
/*      */   
/*      */   public List<RecipientManySearchType> getAnd()
/*      */   {
/*  198 */     if (this.and == null) {
/*  199 */       this.and = new ArrayList();
/*      */     }
/*  201 */     return this.and;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<RecipientManySearchType> getOr()
/*      */   {
/*  227 */     if (this.or == null) {
/*  228 */       this.or = new ArrayList();
/*      */     }
/*  230 */     return this.or;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<RecipientOneSearchType> getNot()
/*      */   {
/*  256 */     if (this.not == null) {
/*  257 */       this.not = new ArrayList();
/*      */     }
/*  259 */     return this.not;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getFirstName()
/*      */   {
/*  285 */     if (this.firstName == null) {
/*  286 */       this.firstName = new ArrayList();
/*      */     }
/*  288 */     return this.firstName;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getLastName()
/*      */   {
/*  314 */     if (this.lastName == null) {
/*  315 */       this.lastName = new ArrayList();
/*      */     }
/*  317 */     return this.lastName;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getUsername()
/*      */   {
/*  343 */     if (this.username == null) {
/*  344 */       this.username = new ArrayList();
/*      */     }
/*  346 */     return this.username;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<Integer> getUserId()
/*      */   {
/*  372 */     if (this.userId == null) {
/*  373 */       this.userId = new ArrayList();
/*      */     }
/*  375 */     return this.userId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getUserUUID()
/*      */   {
/*  401 */     if (this.userUUID == null) {
/*  402 */       this.userUUID = new ArrayList();
/*      */     }
/*  404 */     return this.userUUID;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getGroupTitle()
/*      */   {
/*  430 */     if (this.groupTitle == null) {
/*  431 */       this.groupTitle = new ArrayList();
/*      */     }
/*  433 */     return this.groupTitle;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getTitle()
/*      */   {
/*  459 */     if (this.title == null) {
/*  460 */       this.title = new ArrayList();
/*      */     }
/*  462 */     return this.title;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getCompany()
/*      */   {
/*  488 */     if (this.company == null) {
/*  489 */       this.company = new ArrayList();
/*      */     }
/*  491 */     return this.company;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getDivision()
/*      */   {
/*  517 */     if (this.division == null) {
/*  518 */       this.division = new ArrayList();
/*      */     }
/*  520 */     return this.division;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getAddressTypeName()
/*      */   {
/*  546 */     if (this.addressTypeName == null) {
/*  547 */       this.addressTypeName = new ArrayList();
/*      */     }
/*  549 */     return this.addressTypeName;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getAddress1()
/*      */   {
/*  575 */     if (this.address1 == null) {
/*  576 */       this.address1 = new ArrayList();
/*      */     }
/*  578 */     return this.address1;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getAddress2()
/*      */   {
/*  604 */     if (this.address2 == null) {
/*  605 */       this.address2 = new ArrayList();
/*      */     }
/*  607 */     return this.address2;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getBuilding()
/*      */   {
/*  633 */     if (this.building == null) {
/*  634 */       this.building = new ArrayList();
/*      */     }
/*  636 */     return this.building;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getFloor()
/*      */   {
/*  662 */     if (this.floor == null) {
/*  663 */       this.floor = new ArrayList();
/*      */     }
/*  665 */     return this.floor;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getCity()
/*      */   {
/*  691 */     if (this.city == null) {
/*  692 */       this.city = new ArrayList();
/*      */     }
/*  694 */     return this.city;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getState()
/*      */   {
/*  720 */     if (this.state == null) {
/*  721 */       this.state = new ArrayList();
/*      */     }
/*  723 */     return this.state;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getZip()
/*      */   {
/*  749 */     if (this.zip == null) {
/*  750 */       this.zip = new ArrayList();
/*      */     }
/*  752 */     return this.zip;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getFacility()
/*      */   {
/*  778 */     if (this.facility == null) {
/*  779 */       this.facility = new ArrayList();
/*      */     }
/*  781 */     return this.facility;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getCountry()
/*      */   {
/*  807 */     if (this.country == null) {
/*  808 */       this.country = new ArrayList();
/*      */     }
/*  810 */     return this.country;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getProvince()
/*      */   {
/*  836 */     if (this.province == null) {
/*  837 */       this.province = new ArrayList();
/*      */     }
/*  839 */     return this.province;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getPhoneNumber()
/*      */   {
/*  865 */     if (this.phoneNumber == null) {
/*  866 */       this.phoneNumber = new ArrayList();
/*      */     }
/*  868 */     return this.phoneNumber;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getPager()
/*      */   {
/*  894 */     if (this.pager == null) {
/*  895 */       this.pager = new ArrayList();
/*      */     }
/*  897 */     return this.pager;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getEmail()
/*      */   {
/*  923 */     if (this.email == null) {
/*  924 */       this.email = new ArrayList();
/*      */     }
/*  926 */     return this.email;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getFax()
/*      */   {
/*  952 */     if (this.fax == null) {
/*  953 */       this.fax = new ArrayList();
/*      */     }
/*  955 */     return this.fax;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getSms()
/*      */   {
/*  981 */     if (this.sms == null) {
/*  982 */       this.sms = new ArrayList();
/*      */     }
/*  984 */     return this.sms;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getRole()
/*      */   {
/* 1010 */     if (this.role == null) {
/* 1011 */       this.role = new ArrayList();
/*      */     }
/* 1013 */     return this.role;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getDeviceType()
/*      */   {
/* 1039 */     if (this.deviceType == null) {
/* 1040 */       this.deviceType = new ArrayList();
/*      */     }
/* 1042 */     return this.deviceType;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<Boolean> getDisabledDevice()
/*      */   {
/* 1068 */     if (this.disabledDevice == null) {
/* 1069 */       this.disabledDevice = new ArrayList();
/*      */     }
/* 1071 */     return this.disabledDevice;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<Boolean> getLoginEnabled()
/*      */   {
/* 1097 */     if (this.loginEnabled == null) {
/* 1098 */       this.loginEnabled = new ArrayList();
/*      */     }
/* 1100 */     return this.loginEnabled;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<XMLGregorianCalendar> getLastModifiedBefore()
/*      */   {
/* 1126 */     if (this.lastModifiedBefore == null) {
/* 1127 */       this.lastModifiedBefore = new ArrayList();
/*      */     }
/* 1129 */     return this.lastModifiedBefore;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<XMLGregorianCalendar> getLastModifiedAfter()
/*      */   {
/* 1155 */     if (this.lastModifiedAfter == null) {
/* 1156 */       this.lastModifiedAfter = new ArrayList();
/*      */     }
/* 1158 */     return this.lastModifiedAfter;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<CustomFieldType> getCustomField()
/*      */   {
/* 1184 */     if (this.customField == null) {
/* 1185 */       this.customField = new ArrayList();
/*      */     }
/* 1187 */     return this.customField;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getEmployeeId()
/*      */   {
/* 1213 */     if (this.employeeId == null) {
/* 1214 */       this.employeeId = new ArrayList();
/*      */     }
/* 1216 */     return this.employeeId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getPermission()
/*      */   {
/* 1242 */     if (this.permission == null) {
/* 1243 */       this.permission = new ArrayList();
/*      */     }
/* 1245 */     return this.permission;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<CatSubcatType> getCatSubcat()
/*      */   {
/* 1271 */     if (this.catSubcat == null) {
/* 1272 */       this.catSubcat = new ArrayList();
/*      */     }
/* 1274 */     return this.catSubcat;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getPriority()
/*      */   {
/* 1300 */     if (this.priority == null) {
/* 1301 */       this.priority = new ArrayList();
/*      */     }
/* 1303 */     return this.priority;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getSeverity()
/*      */   {
/* 1329 */     if (this.severity == null) {
/* 1330 */       this.severity = new ArrayList();
/*      */     }
/* 1332 */     return this.severity;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<String> getRoleTemplate()
/*      */   {
/* 1358 */     if (this.roleTemplate == null) {
/* 1359 */       this.roleTemplate = new ArrayList();
/*      */     }
/* 1361 */     return this.roleTemplate;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<TimeZoneType> getTimeZone()
/*      */   {
/* 1387 */     if (this.timeZone == null) {
/* 1388 */       this.timeZone = new ArrayList();
/*      */     }
/* 1390 */     return this.timeZone;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<GeocodeStatusEnumType> getGeocodeStatus()
/*      */   {
/* 1416 */     if (this.geocodeStatus == null) {
/* 1417 */       this.geocodeStatus = new ArrayList();
/*      */     }
/* 1419 */     return this.geocodeStatus;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<SmsOptInStatusType> getSmsOptInStatus()
/*      */   {
/* 1445 */     if (this.smsOptInStatus == null) {
/* 1446 */       this.smsOptInStatus = new ArrayList();
/*      */     }
/* 1448 */     return this.smsOptInStatus;
/*      */   }
/*      */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\RecipientManySearchType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */