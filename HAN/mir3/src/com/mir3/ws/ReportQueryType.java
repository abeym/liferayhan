/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlEnum;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlType(name="reportQueryType")
/*    */ @XmlEnum
/*    */ public enum ReportQueryType
/*    */ {
/* 32 */   ALL_CONTACTED, 
/* 33 */   ALL_RESPONDED, 
/* 34 */   NO_RESPONSE, 
/* 35 */   RESPONSE_OPTION, 
/* 36 */   ALL_RECIPIENTS, 
/* 37 */   NOT_CONTACTED, 
/* 38 */   RECIPIENT;
/*    */   
/*    */   public String value() {
/* 41 */     return name();
/*    */   }
/*    */   
/*    */   public static ReportQueryType fromValue(String v) {
/* 45 */     return valueOf(v);
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\ReportQueryType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */