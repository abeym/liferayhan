/*     */ package com.mir3.ws;
/*     */ 
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="reportStatisticsType", propOrder={})
/*     */ public class ReportStatisticsType
/*     */ {
/*     */   protected Integer totalCalls;
/*     */   protected Integer totalEmails;
/*     */   protected Integer totalFaxes;
/*     */   protected Integer totalSms;
/*     */   protected Integer totalPages;
/*     */   protected Integer totalRecipients;
/*     */   protected Integer answeringMachineCount;
/*     */   protected Integer answeringMachineDisconnectedCount;
/*     */   protected Integer answeringMachineConnectedCount;
/*     */   protected Integer answeringMachineUndeliveredCount;
/*     */   protected Integer busyCount;
/*     */   protected Integer networkBusyCount;
/*     */   protected Integer hangUpCount;
/*     */   protected Integer hangUpCompleteCount;
/*     */   protected Integer invalidResponseCount;
/*     */   protected Integer leftMessageCount;
/*     */   protected Integer noAnswerCount;
/*     */   protected Integer notAtThisLocationCount;
/*     */   protected Integer responseCount;
/*     */   protected Integer wrongAddressCount;
/*     */   protected Integer tddCompleteCount;
/*     */   protected Integer tddPartialCount;
/*     */   protected Integer tddUndeliveredCount;
/*     */   protected Integer otherCount;
/*     */   protected Integer blackberryCount;
/*     */   protected Integer totalMobileAppDevices;
/*     */   
/*     */   public Integer getTotalCalls()
/*     */   {
/*  95 */     return this.totalCalls;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTotalCalls(Integer value)
/*     */   {
/* 107 */     this.totalCalls = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getTotalEmails()
/*     */   {
/* 119 */     return this.totalEmails;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTotalEmails(Integer value)
/*     */   {
/* 131 */     this.totalEmails = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getTotalFaxes()
/*     */   {
/* 143 */     return this.totalFaxes;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTotalFaxes(Integer value)
/*     */   {
/* 155 */     this.totalFaxes = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getTotalSms()
/*     */   {
/* 167 */     return this.totalSms;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTotalSms(Integer value)
/*     */   {
/* 179 */     this.totalSms = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getTotalPages()
/*     */   {
/* 191 */     return this.totalPages;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTotalPages(Integer value)
/*     */   {
/* 203 */     this.totalPages = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getTotalRecipients()
/*     */   {
/* 215 */     return this.totalRecipients;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTotalRecipients(Integer value)
/*     */   {
/* 227 */     this.totalRecipients = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getAnsweringMachineCount()
/*     */   {
/* 239 */     return this.answeringMachineCount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAnsweringMachineCount(Integer value)
/*     */   {
/* 251 */     this.answeringMachineCount = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getAnsweringMachineDisconnectedCount()
/*     */   {
/* 263 */     return this.answeringMachineDisconnectedCount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAnsweringMachineDisconnectedCount(Integer value)
/*     */   {
/* 275 */     this.answeringMachineDisconnectedCount = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getAnsweringMachineConnectedCount()
/*     */   {
/* 287 */     return this.answeringMachineConnectedCount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAnsweringMachineConnectedCount(Integer value)
/*     */   {
/* 299 */     this.answeringMachineConnectedCount = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getAnsweringMachineUndeliveredCount()
/*     */   {
/* 311 */     return this.answeringMachineUndeliveredCount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAnsweringMachineUndeliveredCount(Integer value)
/*     */   {
/* 323 */     this.answeringMachineUndeliveredCount = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getBusyCount()
/*     */   {
/* 335 */     return this.busyCount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setBusyCount(Integer value)
/*     */   {
/* 347 */     this.busyCount = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getNetworkBusyCount()
/*     */   {
/* 359 */     return this.networkBusyCount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setNetworkBusyCount(Integer value)
/*     */   {
/* 371 */     this.networkBusyCount = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getHangUpCount()
/*     */   {
/* 383 */     return this.hangUpCount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setHangUpCount(Integer value)
/*     */   {
/* 395 */     this.hangUpCount = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getHangUpCompleteCount()
/*     */   {
/* 407 */     return this.hangUpCompleteCount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setHangUpCompleteCount(Integer value)
/*     */   {
/* 419 */     this.hangUpCompleteCount = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getInvalidResponseCount()
/*     */   {
/* 431 */     return this.invalidResponseCount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setInvalidResponseCount(Integer value)
/*     */   {
/* 443 */     this.invalidResponseCount = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getLeftMessageCount()
/*     */   {
/* 455 */     return this.leftMessageCount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLeftMessageCount(Integer value)
/*     */   {
/* 467 */     this.leftMessageCount = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getNoAnswerCount()
/*     */   {
/* 479 */     return this.noAnswerCount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setNoAnswerCount(Integer value)
/*     */   {
/* 491 */     this.noAnswerCount = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getNotAtThisLocationCount()
/*     */   {
/* 503 */     return this.notAtThisLocationCount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setNotAtThisLocationCount(Integer value)
/*     */   {
/* 515 */     this.notAtThisLocationCount = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getResponseCount()
/*     */   {
/* 527 */     return this.responseCount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setResponseCount(Integer value)
/*     */   {
/* 539 */     this.responseCount = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getWrongAddressCount()
/*     */   {
/* 551 */     return this.wrongAddressCount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setWrongAddressCount(Integer value)
/*     */   {
/* 563 */     this.wrongAddressCount = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getTddCompleteCount()
/*     */   {
/* 575 */     return this.tddCompleteCount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTddCompleteCount(Integer value)
/*     */   {
/* 587 */     this.tddCompleteCount = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getTddPartialCount()
/*     */   {
/* 599 */     return this.tddPartialCount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTddPartialCount(Integer value)
/*     */   {
/* 611 */     this.tddPartialCount = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getTddUndeliveredCount()
/*     */   {
/* 623 */     return this.tddUndeliveredCount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTddUndeliveredCount(Integer value)
/*     */   {
/* 635 */     this.tddUndeliveredCount = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getOtherCount()
/*     */   {
/* 647 */     return this.otherCount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setOtherCount(Integer value)
/*     */   {
/* 659 */     this.otherCount = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getBlackberryCount()
/*     */   {
/* 671 */     return this.blackberryCount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setBlackberryCount(Integer value)
/*     */   {
/* 683 */     this.blackberryCount = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getTotalMobileAppDevices()
/*     */   {
/* 695 */     return this.totalMobileAppDevices;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTotalMobileAppDevices(Integer value)
/*     */   {
/* 707 */     this.totalMobileAppDevices = value;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\ReportStatisticsType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */