/*     */ package com.mir3.ws;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="notificationRecipientsDetailsType", propOrder={"recipient", "dynamicRecipient", "recipientGroup", "recipientSchedule"})
/*     */ public class NotificationRecipientsDetailsType
/*     */ {
/*     */   protected List<RecipientType> recipient;
/*     */   protected List<RecipientDetailType> dynamicRecipient;
/*     */   protected List<String> recipientGroup;
/*     */   protected List<String> recipientSchedule;
/*     */   
/*     */   public List<RecipientType> getRecipient()
/*     */   {
/*  70 */     if (this.recipient == null) {
/*  71 */       this.recipient = new ArrayList();
/*     */     }
/*  73 */     return this.recipient;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<RecipientDetailType> getDynamicRecipient()
/*     */   {
/*  99 */     if (this.dynamicRecipient == null) {
/* 100 */       this.dynamicRecipient = new ArrayList();
/*     */     }
/* 102 */     return this.dynamicRecipient;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getRecipientGroup()
/*     */   {
/* 128 */     if (this.recipientGroup == null) {
/* 129 */       this.recipientGroup = new ArrayList();
/*     */     }
/* 131 */     return this.recipientGroup;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getRecipientSchedule()
/*     */   {
/* 157 */     if (this.recipientSchedule == null) {
/* 158 */       this.recipientSchedule = new ArrayList();
/*     */     }
/* 160 */     return this.recipientSchedule;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\NotificationRecipientsDetailsType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */