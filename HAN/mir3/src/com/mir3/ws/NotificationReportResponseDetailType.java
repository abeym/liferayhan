/*    */ package com.mir3.ws;
/*    */ 
/*    */ import java.util.ArrayList;
/*    */ import java.util.List;
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="notificationReportResponseDetailType", propOrder={"notificationReportSummary"})
/*    */ public class NotificationReportResponseDetailType
/*    */ {
/*    */   protected List<GroupReportMemberType> notificationReportSummary;
/*    */   
/*    */   public List<GroupReportMemberType> getNotificationReportSummary()
/*    */   {
/* 61 */     if (this.notificationReportSummary == null) {
/* 62 */       this.notificationReportSummary = new ArrayList();
/*    */     }
/* 64 */     return this.notificationReportSummary;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\NotificationReportResponseDetailType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */