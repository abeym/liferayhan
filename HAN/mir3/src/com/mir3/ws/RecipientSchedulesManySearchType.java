/*     */ package com.mir3.ws;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="recipientSchedulesManySearchType", propOrder={"and", "or", "not", "scheduleName", "division"})
/*     */ public class RecipientSchedulesManySearchType
/*     */ {
/*     */   protected List<RecipientSchedulesManySearchType> and;
/*     */   protected List<RecipientSchedulesManySearchType> or;
/*     */   protected List<RecipientSchedulesOneSearchType> not;
/*     */   protected List<String> scheduleName;
/*     */   protected List<String> division;
/*     */   
/*     */   public List<RecipientSchedulesManySearchType> getAnd()
/*     */   {
/*  73 */     if (this.and == null) {
/*  74 */       this.and = new ArrayList();
/*     */     }
/*  76 */     return this.and;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<RecipientSchedulesManySearchType> getOr()
/*     */   {
/* 102 */     if (this.or == null) {
/* 103 */       this.or = new ArrayList();
/*     */     }
/* 105 */     return this.or;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<RecipientSchedulesOneSearchType> getNot()
/*     */   {
/* 131 */     if (this.not == null) {
/* 132 */       this.not = new ArrayList();
/*     */     }
/* 134 */     return this.not;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getScheduleName()
/*     */   {
/* 160 */     if (this.scheduleName == null) {
/* 161 */       this.scheduleName = new ArrayList();
/*     */     }
/* 163 */     return this.scheduleName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getDivision()
/*     */   {
/* 189 */     if (this.division == null) {
/* 190 */       this.division = new ArrayList();
/*     */     }
/* 192 */     return this.division;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\RecipientSchedulesManySearchType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */