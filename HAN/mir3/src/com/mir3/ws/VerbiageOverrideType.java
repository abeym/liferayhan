/*    */ package com.mir3.ws;
/*    */ 
/*    */ import java.util.ArrayList;
/*    */ import java.util.List;
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="verbiageOverrideType", propOrder={"verbiageOverrideDetail"})
/*    */ public class VerbiageOverrideType
/*    */ {
/*    */   protected List<VerbiageOverrideDetailType> verbiageOverrideDetail;
/*    */   
/*    */   public List<VerbiageOverrideDetailType> getVerbiageOverrideDetail()
/*    */   {
/* 61 */     if (this.verbiageOverrideDetail == null) {
/* 62 */       this.verbiageOverrideDetail = new ArrayList();
/*    */     }
/* 64 */     return this.verbiageOverrideDetail;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\VerbiageOverrideType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */