/*     */ package com.mir3.ws;
/*     */ 
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="reportFollowUpChoiceType", propOrder={})
/*     */ public class ReportFollowUpChoiceType
/*     */ {
/*     */   protected Integer responseOptionReportId;
/*     */   protected String responseOptionReportUUID;
/*     */   protected String optionText;
/*     */   protected VerbiagePerMessageType verbiage;
/*     */   protected String cascadeTitle;
/*     */   protected int responseCount;
/*     */   protected Boolean successFlag;
/*     */   
/*     */   public Integer getResponseOptionReportId()
/*     */   {
/*  57 */     return this.responseOptionReportId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setResponseOptionReportId(Integer value)
/*     */   {
/*  69 */     this.responseOptionReportId = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getResponseOptionReportUUID()
/*     */   {
/*  81 */     return this.responseOptionReportUUID;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setResponseOptionReportUUID(String value)
/*     */   {
/*  93 */     this.responseOptionReportUUID = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getOptionText()
/*     */   {
/* 105 */     return this.optionText;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setOptionText(String value)
/*     */   {
/* 117 */     this.optionText = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public VerbiagePerMessageType getVerbiage()
/*     */   {
/* 129 */     return this.verbiage;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setVerbiage(VerbiagePerMessageType value)
/*     */   {
/* 141 */     this.verbiage = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCascadeTitle()
/*     */   {
/* 153 */     return this.cascadeTitle;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCascadeTitle(String value)
/*     */   {
/* 165 */     this.cascadeTitle = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getResponseCount()
/*     */   {
/* 173 */     return this.responseCount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setResponseCount(int value)
/*     */   {
/* 181 */     this.responseCount = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Boolean isSuccessFlag()
/*     */   {
/* 193 */     return this.successFlag;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setSuccessFlag(Boolean value)
/*     */   {
/* 205 */     this.successFlag = value;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\ReportFollowUpChoiceType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */