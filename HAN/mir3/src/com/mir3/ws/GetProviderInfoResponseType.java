/*     */ package com.mir3.ws;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="getProviderInfoResponseType", propOrder={"organizationPortLimit", "organizationPortsUsed", "provider"})
/*     */ public class GetProviderInfoResponseType
/*     */ {
/*     */   protected int organizationPortLimit;
/*     */   protected int organizationPortsUsed;
/*     */   protected List<ProviderType> provider;
/*     */   
/*     */   public int getOrganizationPortLimit()
/*     */   {
/*  49 */     return this.organizationPortLimit;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setOrganizationPortLimit(int value)
/*     */   {
/*  57 */     this.organizationPortLimit = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getOrganizationPortsUsed()
/*     */   {
/*  65 */     return this.organizationPortsUsed;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setOrganizationPortsUsed(int value)
/*     */   {
/*  73 */     this.organizationPortsUsed = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<ProviderType> getProvider()
/*     */   {
/*  99 */     if (this.provider == null) {
/* 100 */       this.provider = new ArrayList();
/*     */     }
/* 102 */     return this.provider;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\GetProviderInfoResponseType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */