/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlEnum;
/*    */ import javax.xml.bind.annotation.XmlEnumValue;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlType(name="sourceType")
/*    */ @XmlEnum
/*    */ public enum SourceType
/*    */ {
/* 28 */   INCONNECT("INCONNECT"), 
/* 29 */   MIR_3(
/* 30 */     "MIR3");
/*    */   
/*    */   private final String value;
/*    */   
/* 34 */   private SourceType(String v) { this.value = v; }
/*    */   
/*    */ 
/*    */ 
/* 38 */   public String value() { return this.value; }
/*    */   
/*    */   public static SourceType fromValue(String v) {
/*    */     SourceType[] arrayOfSourceType;
/* 42 */     int j = (arrayOfSourceType = values()).length; for (int i = 0; i < j; i++) { SourceType c = arrayOfSourceType[i];
/* 43 */       if (c.value.equals(v)) {
/* 44 */         return c;
/*    */       }
/*    */     }
/* 47 */     throw new IllegalArgumentException(v);
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\SourceType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */