/*    */ package com.mir3.ws;
/*    */ 
/*    */ import java.util.ArrayList;
/*    */ import java.util.List;
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="reportHeaderType", propOrder={"reportHeaderGroup"})
/*    */ public class ReportHeaderType
/*    */ {
/*    */   @XmlElement(name="columnName")
/*    */   protected List<String> reportHeaderGroup;
/*    */   
/*    */   public List<String> getReportHeaderGroup()
/*    */   {
/* 61 */     if (this.reportHeaderGroup == null) {
/* 62 */       this.reportHeaderGroup = new ArrayList();
/*    */     }
/* 64 */     return this.reportHeaderGroup;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\ReportHeaderType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */