/*     */ package com.mir3.ws;
/*     */ 
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="reportChosenResponseType", propOrder={})
/*     */ public class ReportChosenResponseType
/*     */ {
/*     */   protected Integer responseOptionReportId;
/*     */   protected String responseOptionReportUUID;
/*     */   protected Integer freeFormId;
/*     */   protected String freeFormUUID;
/*     */   protected ReportCascadeType cascade;
/*     */   protected String responseData;
/*     */   protected Long recordedResponseId;
/*     */   protected String recordedResponseUUID;
/*     */   
/*     */   public Integer getResponseOptionReportId()
/*     */   {
/*  59 */     return this.responseOptionReportId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setResponseOptionReportId(Integer value)
/*     */   {
/*  71 */     this.responseOptionReportId = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getResponseOptionReportUUID()
/*     */   {
/*  83 */     return this.responseOptionReportUUID;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setResponseOptionReportUUID(String value)
/*     */   {
/*  95 */     this.responseOptionReportUUID = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getFreeFormId()
/*     */   {
/* 107 */     return this.freeFormId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setFreeFormId(Integer value)
/*     */   {
/* 119 */     this.freeFormId = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getFreeFormUUID()
/*     */   {
/* 131 */     return this.freeFormUUID;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setFreeFormUUID(String value)
/*     */   {
/* 143 */     this.freeFormUUID = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public ReportCascadeType getCascade()
/*     */   {
/* 155 */     return this.cascade;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCascade(ReportCascadeType value)
/*     */   {
/* 167 */     this.cascade = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getResponseData()
/*     */   {
/* 179 */     return this.responseData;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setResponseData(String value)
/*     */   {
/* 191 */     this.responseData = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Long getRecordedResponseId()
/*     */   {
/* 203 */     return this.recordedResponseId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRecordedResponseId(Long value)
/*     */   {
/* 215 */     this.recordedResponseId = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getRecordedResponseUUID()
/*     */   {
/* 227 */     return this.recordedResponseUUID;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRecordedResponseUUID(String value)
/*     */   {
/* 239 */     this.recordedResponseUUID = value;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\ReportChosenResponseType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */