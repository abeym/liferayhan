/*     */ package com.mir3.ws;
/*     */ 
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlSchemaType;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ import javax.xml.datatype.XMLGregorianCalendar;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="locationScheduleType", propOrder={})
/*     */ public class LocationScheduleType
/*     */ {
/*     */   protected Boolean allDay;
/*     */   @XmlElement(name="from-time")
/*     */   @XmlSchemaType(name="time")
/*     */   protected XMLGregorianCalendar fromTime;
/*     */   @XmlElement(name="to-time")
/*     */   @XmlSchemaType(name="time")
/*     */   protected XMLGregorianCalendar toTime;
/*     */   @XmlElement(name="from-date")
/*     */   @XmlSchemaType(name="date")
/*     */   protected XMLGregorianCalendar fromDate;
/*     */   @XmlElement(name="to-date")
/*     */   @XmlSchemaType(name="date")
/*     */   protected XMLGregorianCalendar toDate;
/*     */   
/*     */   public Boolean isAllDay()
/*     */   {
/*  64 */     return this.allDay;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAllDay(Boolean value)
/*     */   {
/*  76 */     this.allDay = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public XMLGregorianCalendar getFromTime()
/*     */   {
/*  88 */     return this.fromTime;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setFromTime(XMLGregorianCalendar value)
/*     */   {
/* 100 */     this.fromTime = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public XMLGregorianCalendar getToTime()
/*     */   {
/* 112 */     return this.toTime;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setToTime(XMLGregorianCalendar value)
/*     */   {
/* 124 */     this.toTime = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public XMLGregorianCalendar getFromDate()
/*     */   {
/* 136 */     return this.fromDate;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setFromDate(XMLGregorianCalendar value)
/*     */   {
/* 148 */     this.fromDate = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public XMLGregorianCalendar getToDate()
/*     */   {
/* 160 */     return this.toDate;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setToDate(XMLGregorianCalendar value)
/*     */   {
/* 172 */     this.toDate = value;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\LocationScheduleType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */