/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlEnum;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlType(name="smsOptInStatusType")
/*    */ @XmlEnum
/*    */ public enum SmsOptInStatusType
/*    */ {
/* 29 */   OPTIN_NOTREQUESTED, 
/* 30 */   OPTIN_NORESPONSE, 
/* 31 */   OPTIN_DECLINED, 
/* 32 */   OPTIN_ACCEPTED;
/*    */   
/*    */   public String value() {
/* 35 */     return name();
/*    */   }
/*    */   
/*    */   public static SmsOptInStatusType fromValue(String v) {
/* 39 */     return valueOf(v);
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\SmsOptInStatusType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */