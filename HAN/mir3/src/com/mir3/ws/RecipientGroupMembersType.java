/*     */ package com.mir3.ws;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="recipientGroupMembersType", propOrder={"recipient", "recipientGroup", "recipientSchedule"})
/*     */ public class RecipientGroupMembersType
/*     */ {
/*     */   protected List<RecipientType> recipient;
/*     */   protected List<String> recipientGroup;
/*     */   protected List<String> recipientSchedule;
/*     */   
/*     */   public List<RecipientType> getRecipient()
/*     */   {
/*  67 */     if (this.recipient == null) {
/*  68 */       this.recipient = new ArrayList();
/*     */     }
/*  70 */     return this.recipient;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getRecipientGroup()
/*     */   {
/*  96 */     if (this.recipientGroup == null) {
/*  97 */       this.recipientGroup = new ArrayList();
/*     */     }
/*  99 */     return this.recipientGroup;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getRecipientSchedule()
/*     */   {
/* 125 */     if (this.recipientSchedule == null) {
/* 126 */       this.recipientSchedule = new ArrayList();
/*     */     }
/* 128 */     return this.recipientSchedule;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\RecipientGroupMembersType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */