/*     */ package com.mir3.ws;
/*     */ 
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="firstResponseInfoType", propOrder={})
/*     */ public class FirstResponseInfoType
/*     */ {
/*     */   protected Boolean notificationClosedNotice;
/*     */   protected CallingStrategyType callingStrategy;
/*     */   protected CallingOrderType callingOrder;
/*     */   @XmlElement(required=true)
/*     */   protected NotificationEscalationsType escalations;
/*     */   
/*     */   public Boolean isNotificationClosedNotice()
/*     */   {
/*  53 */     return this.notificationClosedNotice;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setNotificationClosedNotice(Boolean value)
/*     */   {
/*  65 */     this.notificationClosedNotice = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public CallingStrategyType getCallingStrategy()
/*     */   {
/*  77 */     return this.callingStrategy;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCallingStrategy(CallingStrategyType value)
/*     */   {
/*  89 */     this.callingStrategy = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public CallingOrderType getCallingOrder()
/*     */   {
/* 101 */     return this.callingOrder;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCallingOrder(CallingOrderType value)
/*     */   {
/* 113 */     this.callingOrder = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public NotificationEscalationsType getEscalations()
/*     */   {
/* 125 */     return this.escalations;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setEscalations(NotificationEscalationsType value)
/*     */   {
/* 137 */     this.escalations = value;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\FirstResponseInfoType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */