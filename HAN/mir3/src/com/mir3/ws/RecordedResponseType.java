/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="recordedResponseType", propOrder={})
/*    */ public class RecordedResponseType
/*    */ {
/*    */   @XmlElement(required=true)
/*    */   protected byte[] recordedResponseBytes;
/*    */   
/*    */   public byte[] getRecordedResponseBytes()
/*    */   {
/* 46 */     return this.recordedResponseBytes;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setRecordedResponseBytes(byte[] value)
/*    */   {
/* 57 */     this.recordedResponseBytes = value;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\RecordedResponseType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */