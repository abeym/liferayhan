/*    */ package com.mir3.ws;
/*    */ 
/*    */ import java.util.ArrayList;
/*    */ import java.util.List;
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="verbiageType", propOrder={"text", "recordingTitle"})
/*    */ public class VerbiageType
/*    */ {
/*    */   @XmlElement(required=true)
/*    */   protected List<StringWithLocale> text;
/*    */   protected List<StringWithLocale> recordingTitle;
/*    */   
/*    */   public List<StringWithLocale> getText()
/*    */   {
/* 66 */     if (this.text == null) {
/* 67 */       this.text = new ArrayList();
/*    */     }
/* 69 */     return this.text;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public List<StringWithLocale> getRecordingTitle()
/*    */   {
/* 95 */     if (this.recordingTitle == null) {
/* 96 */       this.recordingTitle = new ArrayList();
/*    */     }
/* 98 */     return this.recordingTitle;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\VerbiageType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */