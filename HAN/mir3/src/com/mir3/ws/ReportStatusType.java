/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlEnum;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlType(name="reportStatusType")
/*    */ @XmlEnum
/*    */ public enum ReportStatusType
/*    */ {
/* 28 */   INITIATED, 
/* 29 */   ALL_CONTACTS_SENT, 
/* 30 */   COMPLETED;
/*    */   
/*    */   public String value() {
/* 33 */     return name();
/*    */   }
/*    */   
/*    */   public static ReportStatusType fromValue(String v) {
/* 37 */     return valueOf(v);
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\ReportStatusType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */