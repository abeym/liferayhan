/*     */ package com.mir3.ws;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="deleteVoiceFilesType", propOrder={"apiVersion", "authorization", "persistData", "requireFeature", "division", "title"})
/*     */ public class DeleteVoiceFilesType
/*     */ {
/*     */   @XmlElement(required=true)
/*     */   protected String apiVersion;
/*     */   protected AuthorizationType authorization;
/*     */   protected Boolean persistData;
/*     */   protected List<String> requireFeature;
/*     */   protected String division;
/*     */   @XmlElement(required=true)
/*     */   protected List<String> title;
/*     */   
/*     */   public String getApiVersion()
/*     */   {
/*  62 */     return this.apiVersion;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setApiVersion(String value)
/*     */   {
/*  74 */     this.apiVersion = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public AuthorizationType getAuthorization()
/*     */   {
/*  86 */     return this.authorization;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAuthorization(AuthorizationType value)
/*     */   {
/*  98 */     this.authorization = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Boolean isPersistData()
/*     */   {
/* 110 */     return this.persistData;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPersistData(Boolean value)
/*     */   {
/* 122 */     this.persistData = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getRequireFeature()
/*     */   {
/* 148 */     if (this.requireFeature == null) {
/* 149 */       this.requireFeature = new ArrayList();
/*     */     }
/* 151 */     return this.requireFeature;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getDivision()
/*     */   {
/* 163 */     return this.division;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDivision(String value)
/*     */   {
/* 175 */     this.division = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getTitle()
/*     */   {
/* 201 */     if (this.title == null) {
/* 202 */       this.title = new ArrayList();
/*     */     }
/* 204 */     return this.title;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\DeleteVoiceFilesType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */