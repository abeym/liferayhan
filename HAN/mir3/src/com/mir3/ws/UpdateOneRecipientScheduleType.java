/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="updateOneRecipientScheduleType", propOrder={"recipientSchedule", "recipientScheduleDetail"})
/*    */ public class UpdateOneRecipientScheduleType
/*    */ {
/*    */   @XmlElement(required=true)
/*    */   protected String recipientSchedule;
/*    */   @XmlElement(required=true)
/*    */   protected RecipientScheduleDetailType recipientScheduleDetail;
/*    */   
/*    */   public String getRecipientSchedule()
/*    */   {
/* 51 */     return this.recipientSchedule;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setRecipientSchedule(String value)
/*    */   {
/* 63 */     this.recipientSchedule = value;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public RecipientScheduleDetailType getRecipientScheduleDetail()
/*    */   {
/* 75 */     return this.recipientScheduleDetail;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setRecipientScheduleDetail(RecipientScheduleDetailType value)
/*    */   {
/* 87 */     this.recipientScheduleDetail = value;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\UpdateOneRecipientScheduleType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */