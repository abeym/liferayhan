package com.mir3.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name="noContent")
public class NoContent {}


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\NoContent.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */