/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="intervalRepeatForScheduleType", propOrder={"every", "timeUnit"})
/*    */ public class IntervalRepeatForScheduleType
/*    */ {
/*    */   protected int every;
/*    */   @XmlElement(required=true)
/*    */   protected IntervalUnitType timeUnit;
/*    */   
/*    */   public int getEvery()
/*    */   {
/* 46 */     return this.every;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setEvery(int value)
/*    */   {
/* 54 */     this.every = value;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public IntervalUnitType getTimeUnit()
/*    */   {
/* 66 */     return this.timeUnit;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setTimeUnit(IntervalUnitType value)
/*    */   {
/* 78 */     this.timeUnit = value;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\IntervalRepeatForScheduleType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */