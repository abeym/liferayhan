/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="getLoggedInUserResponseType", propOrder={"recipientDetail"})
/*    */ public class GetLoggedInUserResponseType
/*    */ {
/*    */   @XmlElement(required=true)
/*    */   protected RecipientDetailType recipientDetail;
/*    */   
/*    */   public RecipientDetailType getRecipientDetail()
/*    */   {
/* 47 */     return this.recipientDetail;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setRecipientDetail(RecipientDetailType value)
/*    */   {
/* 59 */     this.recipientDetail = value;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\GetLoggedInUserResponseType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */