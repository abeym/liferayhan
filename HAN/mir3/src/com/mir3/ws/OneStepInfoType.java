/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="oneStepInfoType", propOrder={})
/*    */ public class OneStepInfoType
/*    */ {
/*    */   protected int broadcastDuration;
/*    */   
/*    */   public int getBroadcastDuration()
/*    */   {
/* 41 */     return this.broadcastDuration;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setBroadcastDuration(int value)
/*    */   {
/* 49 */     this.broadcastDuration = value;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\OneStepInfoType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */