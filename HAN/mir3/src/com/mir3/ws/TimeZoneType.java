/*     */ package com.mir3.ws;
/*     */ 
/*     */ import javax.xml.bind.annotation.XmlEnum;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlType(name="timeZoneType")
/*     */ @XmlEnum
/*     */ public enum TimeZoneType
/*     */ {
/*  70 */   ENIWETOK, 
/*  71 */   PACIFIC_MIDWAY, 
/*  72 */   HAWAII, 
/*  73 */   ALASKA, 
/*  74 */   PACIFIC_USA, 
/*  75 */   ARIZONA, 
/*  76 */   MOUNTAIN_USA, 
/*  77 */   CENTRAL_USA, 
/*  78 */   EASTERN_USA, 
/*  79 */   INDIANA_USA, 
/*  80 */   PUERTO_RICO, 
/*  81 */   NEWFOUNDLAND, 
/*  82 */   BUENOS_ARIES, 
/*  83 */   SAO_PAULO, 
/*  84 */   ATLANTIC, 
/*  85 */   AZORES, 
/*  86 */   GMT, 
/*  87 */   LONDON, 
/*  88 */   WESTERN_EUROPE, 
/*  89 */   WEST_AFRICA, 
/*  90 */   CENTRAL_EUROPE, 
/*  91 */   CENTRAL_AFRICA, 
/*  92 */   ISRAEL, 
/*  93 */   EASTERN_EUROPE, 
/*  94 */   EAST_AFRICA, 
/*  95 */   DOHA, 
/*  96 */   TEHRAN, 
/*  97 */   DUBAI, 
/*  98 */   MOSCOW, 
/*  99 */   AFGHANISTAN, 
/* 100 */   KARACHI, 
/* 101 */   CALCUTTA, 
/* 102 */   ALMATY, 
/* 103 */   COCOS, 
/* 104 */   BANGKOK_HANOI, 
/* 105 */   CHINESE_TAIPEI, 
/* 106 */   WEST_AUSTRALIA, 
/* 107 */   JAPAN, 
/* 108 */   CENTRAL_AUSTRALIA, 
/* 109 */   EASTERN_AUSTRALIA, 
/* 110 */   LHI, 
/* 111 */   SOLOMON_ISLANDS, 
/* 112 */   NORFOLK, 
/* 113 */   NEW_ZEALAND, 
/* 114 */   ENDERBURY;
/*     */   
/*     */   public String value() {
/* 117 */     return name();
/*     */   }
/*     */   
/*     */   public static TimeZoneType fromValue(String v) {
/* 121 */     return valueOf(v);
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\TimeZoneType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */