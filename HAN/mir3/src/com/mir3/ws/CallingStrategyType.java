/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlEnum;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlType(name="callingStrategyType")
/*    */ @XmlEnum
/*    */ public enum CallingStrategyType
/*    */ {
/* 26 */   ROUND_ROBIN;
/*    */   
/*    */   public String value() {
/* 29 */     return name();
/*    */   }
/*    */   
/*    */   public static CallingStrategyType fromValue(String v) {
/* 33 */     return valueOf(v);
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\CallingStrategyType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */