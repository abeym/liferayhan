/*     */ package com.mir3.ws;
/*     */ 
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="recipientSchedulesOneSearchType", propOrder={"and", "or", "not", "scheduleName", "division"})
/*     */ public class RecipientSchedulesOneSearchType
/*     */ {
/*     */   protected RecipientSchedulesManySearchType and;
/*     */   protected RecipientSchedulesManySearchType or;
/*     */   protected RecipientSchedulesOneSearchType not;
/*     */   protected String scheduleName;
/*     */   protected String division;
/*     */   
/*     */   public RecipientSchedulesManySearchType getAnd()
/*     */   {
/*  57 */     return this.and;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAnd(RecipientSchedulesManySearchType value)
/*     */   {
/*  69 */     this.and = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public RecipientSchedulesManySearchType getOr()
/*     */   {
/*  81 */     return this.or;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setOr(RecipientSchedulesManySearchType value)
/*     */   {
/*  93 */     this.or = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public RecipientSchedulesOneSearchType getNot()
/*     */   {
/* 105 */     return this.not;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setNot(RecipientSchedulesOneSearchType value)
/*     */   {
/* 117 */     this.not = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getScheduleName()
/*     */   {
/* 129 */     return this.scheduleName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setScheduleName(String value)
/*     */   {
/* 141 */     this.scheduleName = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getDivision()
/*     */   {
/* 153 */     return this.division;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDivision(String value)
/*     */   {
/* 165 */     this.division = value;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\RecipientSchedulesOneSearchType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */