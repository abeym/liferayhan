/*     */ package com.mir3.ws;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="customVerbiageType", propOrder={"key", "metadata", "customVerbiage", "currentVerbiage", "defaultVerbiage", "params"})
/*     */ public class CustomVerbiageType
/*     */ {
/*     */   protected String key;
/*     */   @XmlElement(required=true)
/*     */   protected String metadata;
/*     */   protected List<VerbiageInfoType> customVerbiage;
/*     */   protected List<VerbiageInfoType> currentVerbiage;
/*     */   protected List<VerbiageInfoType> defaultVerbiage;
/*     */   protected List<VerbiageParamType> params;
/*     */   
/*     */   public String getKey()
/*     */   {
/*  64 */     return this.key;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setKey(String value)
/*     */   {
/*  76 */     this.key = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getMetadata()
/*     */   {
/*  88 */     return this.metadata;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setMetadata(String value)
/*     */   {
/* 100 */     this.metadata = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<VerbiageInfoType> getCustomVerbiage()
/*     */   {
/* 126 */     if (this.customVerbiage == null) {
/* 127 */       this.customVerbiage = new ArrayList();
/*     */     }
/* 129 */     return this.customVerbiage;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<VerbiageInfoType> getCurrentVerbiage()
/*     */   {
/* 155 */     if (this.currentVerbiage == null) {
/* 156 */       this.currentVerbiage = new ArrayList();
/*     */     }
/* 158 */     return this.currentVerbiage;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<VerbiageInfoType> getDefaultVerbiage()
/*     */   {
/* 184 */     if (this.defaultVerbiage == null) {
/* 185 */       this.defaultVerbiage = new ArrayList();
/*     */     }
/* 187 */     return this.defaultVerbiage;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<VerbiageParamType> getParams()
/*     */   {
/* 213 */     if (this.params == null) {
/* 214 */       this.params = new ArrayList();
/*     */     }
/* 216 */     return this.params;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\CustomVerbiageType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */