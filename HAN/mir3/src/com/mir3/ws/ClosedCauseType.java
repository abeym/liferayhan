/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlEnum;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlType(name="closedCauseType")
/*    */ @XmlEnum
/*    */ public enum ClosedCauseType
/*    */ {
/* 29 */   TERMINATED, 
/* 30 */   TIME_OUT, 
/* 31 */   RESPONDED, 
/* 32 */   UNKNOWN;
/*    */   
/*    */   public String value() {
/* 35 */     return name();
/*    */   }
/*    */   
/*    */   public static ClosedCauseType fromValue(String v) {
/* 39 */     return valueOf(v);
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\ClosedCauseType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */