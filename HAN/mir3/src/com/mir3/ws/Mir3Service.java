/*    */ package com.mir3.ws;
/*    */ 
/*    */ import java.net.URL;
/*    */ import javax.xml.namespace.QName;
/*    */ import javax.xml.ws.Service;
/*    */ import javax.xml.ws.WebEndpoint;
/*    */ import javax.xml.ws.WebServiceClient;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @WebServiceClient(name="Mir3Service", targetNamespace="http://www.mir3.com/ws", wsdlLocation="inWebServices-4_5.wsdl")
/*    */ public class Mir3Service
/*    */   extends Service
/*    */ {
/*    */   private static final URL MIR3SERVICE_WSDL_LOCATION;
/*    */   
/*    */   static
/*    */   {
/* 27 */     URL url = Mir3Service.class.getResource("inWebServices-4_5.wsdl");
/* 28 */     MIR3SERVICE_WSDL_LOCATION = url;
/*    */   }
/*    */   
/*    */   public Mir3Service(URL wsdlLocation, QName serviceName) {
/* 32 */     super(wsdlLocation, serviceName);
/*    */   }
/*    */   
/*    */   public Mir3Service() {
/* 36 */     super(MIR3SERVICE_WSDL_LOCATION, new QName("http://www.mir3.com/ws", "Mir3Service"));
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   @WebEndpoint(name="mir3")
/*    */   public Mir3 getMir3()
/*    */   {
/* 46 */     return (Mir3)super.getPort(new QName("http://www.mir3.com/ws", "mir3"), Mir3.class);
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\Mir3Service.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */