/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="locationOverrideType", propOrder={})
/*    */ public class LocationOverrideType
/*    */ {
/*    */   protected Boolean overrideDefaultStatusOnly;
/*    */   @XmlElement(required=true)
/*    */   protected LocationOverrideDevicesType devices;
/*    */   
/*    */   public Boolean isOverrideDefaultStatusOnly()
/*    */   {
/* 49 */     return this.overrideDefaultStatusOnly;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setOverrideDefaultStatusOnly(Boolean value)
/*    */   {
/* 61 */     this.overrideDefaultStatusOnly = value;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public LocationOverrideDevicesType getDevices()
/*    */   {
/* 73 */     return this.devices;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setDevices(LocationOverrideDevicesType value)
/*    */   {
/* 85 */     this.devices = value;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\LocationOverrideType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */