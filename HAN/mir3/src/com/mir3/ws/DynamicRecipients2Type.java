/*    */ package com.mir3.ws;
/*    */ 
/*    */ import java.util.ArrayList;
/*    */ import java.util.List;
/*    */ import javax.xml.bind.JAXBElement;
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElementRefs;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="dynamicRecipients2Type", propOrder={"dynamicRecipientsContent"})
/*    */ public class DynamicRecipients2Type
/*    */ {
/*    */   @XmlElementRefs({@javax.xml.bind.annotation.XmlElementRef(name="recipientDetail", namespace="http://www.mir3.com/ws", type=JAXBElement.class), @javax.xml.bind.annotation.XmlElementRef(name="recipientGroupTitle", namespace="http://www.mir3.com/ws", type=JAXBElement.class), @javax.xml.bind.annotation.XmlElementRef(name="recipientScheduleTitle", namespace="http://www.mir3.com/ws", type=JAXBElement.class), @javax.xml.bind.annotation.XmlElementRef(name="recipient", namespace="http://www.mir3.com/ws", type=JAXBElement.class)})
/*    */   protected List<JAXBElement<?>> dynamicRecipientsContent;
/*    */   
/*    */   public List<JAXBElement<?>> getDynamicRecipientsContent()
/*    */   {
/* 71 */     if (this.dynamicRecipientsContent == null) {
/* 72 */       this.dynamicRecipientsContent = new ArrayList();
/*    */     }
/* 74 */     return this.dynamicRecipientsContent;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\DynamicRecipients2Type.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */