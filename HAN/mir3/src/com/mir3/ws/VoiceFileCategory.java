/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlEnum;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlType(name="voiceFileCategory")
/*    */ @XmlEnum
/*    */ public enum VoiceFileCategory
/*    */ {
/* 28 */   MESSAGE, 
/* 29 */   RESPONSE_OPTION, 
/* 30 */   FOLLOWUP;
/*    */   
/*    */   public String value() {
/* 33 */     return name();
/*    */   }
/*    */   
/*    */   public static VoiceFileCategory fromValue(String v) {
/* 37 */     return valueOf(v);
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\VoiceFileCategory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */