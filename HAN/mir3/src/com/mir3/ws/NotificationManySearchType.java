/*     */ package com.mir3.ws;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="notificationManySearchType", propOrder={"and", "or", "not", "notificationTitle", "message", "response", "callBridge", "firstName", "lastName", "leaveMessage", "validateRecipient", "verifyPin", "catSubcat", "priority", "severity"})
/*     */ public class NotificationManySearchType
/*     */ {
/*     */   protected List<NotificationManySearchType> and;
/*     */   protected List<NotificationManySearchType> or;
/*     */   protected List<NotificationOneSearchType> not;
/*     */   protected List<String> notificationTitle;
/*     */   protected List<String> message;
/*     */   protected List<String> response;
/*     */   protected List<String> callBridge;
/*     */   protected List<String> firstName;
/*     */   protected List<String> lastName;
/*     */   @XmlElement(type=Boolean.class)
/*     */   protected List<Boolean> leaveMessage;
/*     */   @XmlElement(type=Boolean.class)
/*     */   protected List<Boolean> validateRecipient;
/*     */   @XmlElement(type=Boolean.class)
/*     */   protected List<Boolean> verifyPin;
/*     */   protected List<CatSubcatType> catSubcat;
/*     */   protected List<String> priority;
/*     */   protected List<String> severity;
/*     */   
/*     */   public List<NotificationManySearchType> getAnd()
/*     */   {
/* 107 */     if (this.and == null) {
/* 108 */       this.and = new ArrayList();
/*     */     }
/* 110 */     return this.and;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<NotificationManySearchType> getOr()
/*     */   {
/* 136 */     if (this.or == null) {
/* 137 */       this.or = new ArrayList();
/*     */     }
/* 139 */     return this.or;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<NotificationOneSearchType> getNot()
/*     */   {
/* 165 */     if (this.not == null) {
/* 166 */       this.not = new ArrayList();
/*     */     }
/* 168 */     return this.not;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getNotificationTitle()
/*     */   {
/* 194 */     if (this.notificationTitle == null) {
/* 195 */       this.notificationTitle = new ArrayList();
/*     */     }
/* 197 */     return this.notificationTitle;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getMessage()
/*     */   {
/* 223 */     if (this.message == null) {
/* 224 */       this.message = new ArrayList();
/*     */     }
/* 226 */     return this.message;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getResponse()
/*     */   {
/* 252 */     if (this.response == null) {
/* 253 */       this.response = new ArrayList();
/*     */     }
/* 255 */     return this.response;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getCallBridge()
/*     */   {
/* 281 */     if (this.callBridge == null) {
/* 282 */       this.callBridge = new ArrayList();
/*     */     }
/* 284 */     return this.callBridge;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getFirstName()
/*     */   {
/* 310 */     if (this.firstName == null) {
/* 311 */       this.firstName = new ArrayList();
/*     */     }
/* 313 */     return this.firstName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getLastName()
/*     */   {
/* 339 */     if (this.lastName == null) {
/* 340 */       this.lastName = new ArrayList();
/*     */     }
/* 342 */     return this.lastName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<Boolean> getLeaveMessage()
/*     */   {
/* 368 */     if (this.leaveMessage == null) {
/* 369 */       this.leaveMessage = new ArrayList();
/*     */     }
/* 371 */     return this.leaveMessage;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<Boolean> getValidateRecipient()
/*     */   {
/* 397 */     if (this.validateRecipient == null) {
/* 398 */       this.validateRecipient = new ArrayList();
/*     */     }
/* 400 */     return this.validateRecipient;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<Boolean> getVerifyPin()
/*     */   {
/* 426 */     if (this.verifyPin == null) {
/* 427 */       this.verifyPin = new ArrayList();
/*     */     }
/* 429 */     return this.verifyPin;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<CatSubcatType> getCatSubcat()
/*     */   {
/* 455 */     if (this.catSubcat == null) {
/* 456 */       this.catSubcat = new ArrayList();
/*     */     }
/* 458 */     return this.catSubcat;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getPriority()
/*     */   {
/* 484 */     if (this.priority == null) {
/* 485 */       this.priority = new ArrayList();
/*     */     }
/* 487 */     return this.priority;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getSeverity()
/*     */   {
/* 513 */     if (this.severity == null) {
/* 514 */       this.severity = new ArrayList();
/*     */     }
/* 516 */     return this.severity;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\NotificationManySearchType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */