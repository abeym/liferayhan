/*     */ package com.mir3.ws;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlSchemaType;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ import javax.xml.datatype.XMLGregorianCalendar;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="incidentManySearchType", propOrder={"and", "or", "not", "closedBy", "closedDate", "currentCatSubCat", "currentPriority", "currentSeverity", "currentlyAssignedTo", "division", "incidentTitle", "initialCatSubCat", "initialPriority", "initialSeverity", "initiallyAssignedTo", "lastUpdatedBy", "lastUpdatedDate", "openIncident", "openedBy", "openedDate", "referenceNumber", "reopenedBy", "reopenedDate"})
/*     */ public class IncidentManySearchType
/*     */ {
/*     */   protected List<IncidentManySearchType> and;
/*     */   protected List<IncidentManySearchType> or;
/*     */   protected List<IncidentOneSearchType> not;
/*     */   protected List<String> closedBy;
/*     */   @XmlSchemaType(name="date")
/*     */   protected List<XMLGregorianCalendar> closedDate;
/*     */   protected List<CatSubcatType> currentCatSubCat;
/*     */   protected List<String> currentPriority;
/*     */   protected List<String> currentSeverity;
/*     */   protected List<String> currentlyAssignedTo;
/*     */   protected List<String> division;
/*     */   protected List<String> incidentTitle;
/*     */   protected List<CatSubcatType> initialCatSubCat;
/*     */   protected List<String> initialPriority;
/*     */   protected List<String> initialSeverity;
/*     */   protected List<String> initiallyAssignedTo;
/*     */   protected List<String> lastUpdatedBy;
/*     */   @XmlSchemaType(name="date")
/*     */   protected List<XMLGregorianCalendar> lastUpdatedDate;
/*     */   @XmlElement(type=Boolean.class)
/*     */   protected List<Boolean> openIncident;
/*     */   protected List<String> openedBy;
/*     */   @XmlSchemaType(name="date")
/*     */   protected List<XMLGregorianCalendar> openedDate;
/*     */   protected List<String> referenceNumber;
/*     */   protected List<String> reopenedBy;
/*     */   @XmlSchemaType(name="date")
/*     */   protected List<XMLGregorianCalendar> reopenedDate;
/*     */   
/*     */   public List<IncidentManySearchType> getAnd()
/*     */   {
/* 135 */     if (this.and == null) {
/* 136 */       this.and = new ArrayList();
/*     */     }
/* 138 */     return this.and;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<IncidentManySearchType> getOr()
/*     */   {
/* 164 */     if (this.or == null) {
/* 165 */       this.or = new ArrayList();
/*     */     }
/* 167 */     return this.or;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<IncidentOneSearchType> getNot()
/*     */   {
/* 193 */     if (this.not == null) {
/* 194 */       this.not = new ArrayList();
/*     */     }
/* 196 */     return this.not;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getClosedBy()
/*     */   {
/* 222 */     if (this.closedBy == null) {
/* 223 */       this.closedBy = new ArrayList();
/*     */     }
/* 225 */     return this.closedBy;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<XMLGregorianCalendar> getClosedDate()
/*     */   {
/* 251 */     if (this.closedDate == null) {
/* 252 */       this.closedDate = new ArrayList();
/*     */     }
/* 254 */     return this.closedDate;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<CatSubcatType> getCurrentCatSubCat()
/*     */   {
/* 280 */     if (this.currentCatSubCat == null) {
/* 281 */       this.currentCatSubCat = new ArrayList();
/*     */     }
/* 283 */     return this.currentCatSubCat;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getCurrentPriority()
/*     */   {
/* 309 */     if (this.currentPriority == null) {
/* 310 */       this.currentPriority = new ArrayList();
/*     */     }
/* 312 */     return this.currentPriority;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getCurrentSeverity()
/*     */   {
/* 338 */     if (this.currentSeverity == null) {
/* 339 */       this.currentSeverity = new ArrayList();
/*     */     }
/* 341 */     return this.currentSeverity;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getCurrentlyAssignedTo()
/*     */   {
/* 367 */     if (this.currentlyAssignedTo == null) {
/* 368 */       this.currentlyAssignedTo = new ArrayList();
/*     */     }
/* 370 */     return this.currentlyAssignedTo;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getDivision()
/*     */   {
/* 396 */     if (this.division == null) {
/* 397 */       this.division = new ArrayList();
/*     */     }
/* 399 */     return this.division;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getIncidentTitle()
/*     */   {
/* 425 */     if (this.incidentTitle == null) {
/* 426 */       this.incidentTitle = new ArrayList();
/*     */     }
/* 428 */     return this.incidentTitle;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<CatSubcatType> getInitialCatSubCat()
/*     */   {
/* 454 */     if (this.initialCatSubCat == null) {
/* 455 */       this.initialCatSubCat = new ArrayList();
/*     */     }
/* 457 */     return this.initialCatSubCat;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getInitialPriority()
/*     */   {
/* 483 */     if (this.initialPriority == null) {
/* 484 */       this.initialPriority = new ArrayList();
/*     */     }
/* 486 */     return this.initialPriority;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getInitialSeverity()
/*     */   {
/* 512 */     if (this.initialSeverity == null) {
/* 513 */       this.initialSeverity = new ArrayList();
/*     */     }
/* 515 */     return this.initialSeverity;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getInitiallyAssignedTo()
/*     */   {
/* 541 */     if (this.initiallyAssignedTo == null) {
/* 542 */       this.initiallyAssignedTo = new ArrayList();
/*     */     }
/* 544 */     return this.initiallyAssignedTo;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getLastUpdatedBy()
/*     */   {
/* 570 */     if (this.lastUpdatedBy == null) {
/* 571 */       this.lastUpdatedBy = new ArrayList();
/*     */     }
/* 573 */     return this.lastUpdatedBy;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<XMLGregorianCalendar> getLastUpdatedDate()
/*     */   {
/* 599 */     if (this.lastUpdatedDate == null) {
/* 600 */       this.lastUpdatedDate = new ArrayList();
/*     */     }
/* 602 */     return this.lastUpdatedDate;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<Boolean> getOpenIncident()
/*     */   {
/* 628 */     if (this.openIncident == null) {
/* 629 */       this.openIncident = new ArrayList();
/*     */     }
/* 631 */     return this.openIncident;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getOpenedBy()
/*     */   {
/* 657 */     if (this.openedBy == null) {
/* 658 */       this.openedBy = new ArrayList();
/*     */     }
/* 660 */     return this.openedBy;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<XMLGregorianCalendar> getOpenedDate()
/*     */   {
/* 686 */     if (this.openedDate == null) {
/* 687 */       this.openedDate = new ArrayList();
/*     */     }
/* 689 */     return this.openedDate;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getReferenceNumber()
/*     */   {
/* 715 */     if (this.referenceNumber == null) {
/* 716 */       this.referenceNumber = new ArrayList();
/*     */     }
/* 718 */     return this.referenceNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getReopenedBy()
/*     */   {
/* 744 */     if (this.reopenedBy == null) {
/* 745 */       this.reopenedBy = new ArrayList();
/*     */     }
/* 747 */     return this.reopenedBy;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<XMLGregorianCalendar> getReopenedDate()
/*     */   {
/* 773 */     if (this.reopenedDate == null) {
/* 774 */       this.reopenedDate = new ArrayList();
/*     */     }
/* 776 */     return this.reopenedDate;
/*     */   }
/*     */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\IncidentManySearchType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */