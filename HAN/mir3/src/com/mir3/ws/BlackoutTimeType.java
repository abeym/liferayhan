/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlSchemaType;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ import javax.xml.datatype.XMLGregorianCalendar;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="blackoutTimeType", propOrder={"fromLocalTime", "toLocalTime"})
/*    */ public class BlackoutTimeType
/*    */ {
/*    */   @XmlElement(required=true)
/*    */   @XmlSchemaType(name="time")
/*    */   protected XMLGregorianCalendar fromLocalTime;
/*    */   @XmlElement(required=true)
/*    */   @XmlSchemaType(name="time")
/*    */   protected XMLGregorianCalendar toLocalTime;
/*    */   
/*    */   public XMLGregorianCalendar getFromLocalTime()
/*    */   {
/* 55 */     return this.fromLocalTime;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setFromLocalTime(XMLGregorianCalendar value)
/*    */   {
/* 67 */     this.fromLocalTime = value;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public XMLGregorianCalendar getToLocalTime()
/*    */   {
/* 79 */     return this.toLocalTime;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setToLocalTime(XMLGregorianCalendar value)
/*    */   {
/* 91 */     this.toLocalTime = value;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\BlackoutTimeType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */