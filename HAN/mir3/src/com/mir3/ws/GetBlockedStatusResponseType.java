/*    */ package com.mir3.ws;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="getBlockedStatusResponseType", propOrder={"devices"})
/*    */ public class GetBlockedStatusResponseType
/*    */ {
/*    */   @XmlElement(required=true)
/*    */   protected BlockedStatusType devices;
/*    */   
/*    */   public BlockedStatusType getDevices()
/*    */   {
/* 47 */     return this.devices;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setDevices(BlockedStatusType value)
/*    */   {
/* 59 */     this.devices = value;
/*    */   }
/*    */ }


/* Location:              C:\projects\mir3\hanwsclient.jar!\com\mir3\ws\GetBlockedStatusResponseType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */