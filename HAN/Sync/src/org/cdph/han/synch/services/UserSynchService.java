package org.cdph.han.synch.services;

import java.util.List;

import org.cdph.han.synch.dto.Mir3RecipientResponse;
import org.cdph.han.synch.dto.Mir3UserTO;

public interface UserSynchService {
	List<Mir3RecipientResponse> addRecipients(List<Mir3UserTO> recipients, String userName, String password, String key);
	void deleteRecipients(List<String> names, String userName, String password, String key);
	Mir3UserTO searchRecipient(String email, String userName, String password, String key);
}
