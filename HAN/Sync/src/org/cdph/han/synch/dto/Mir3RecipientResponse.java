package org.cdph.han.synch.dto;

public class Mir3RecipientResponse {
	private int userId;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}
}
