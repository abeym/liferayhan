package org.cdph.han.synch.dto;

import java.util.ArrayList;
import java.util.List;

import org.cdph.han.synch.dto.Mir3DeviceTO;

public class Mir3UserTO {
	private String areaField;
	private String department;
	private String firstName;
	private String lastName;
	private String organization;
	private String organizationType;
	private String password;
	private String division;
	private String language;
	private String mir3Role;
	private String timezone;
	private String role;
	private String title;
	private String username;
	private String telephonyId;
	private String pin;
	private String jobTitle;
	private String mir3Id;
	private String address1;
	private String address2;
	private String city;
	private String country; 
	private String floor;
	private String state;
	private String zip;
	List<String> alternates; 
	
	private List<Mir3DeviceTO> devices = new ArrayList<Mir3DeviceTO>();

	public List<Mir3DeviceTO> getDevices() {
		return this.devices;
	}

	public String getOrganizationType() {
		return organizationType;
	}
	public void setOrganizationType(String organizationType) {
		this.organizationType = organizationType;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getAreaField() {
		return areaField;
	}
	public void setAreaField(String areaField) {
		this.areaField = areaField;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getOrganization() {
		return organization;
	}
	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getMir3Role() {
		return mir3Role;
	}
	public void setMir3Role(String mir3Role) {
		this.mir3Role = mir3Role;
	}

	public String getTimezone() {
		return timezone;
	}
	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	public void setDevices(List<Mir3DeviceTO> devices) {
		this.devices = devices;
	}

	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}

	public String getTelephonyId() {
		return telephonyId;
	}
	public void setTelephonyId(String telephonyId) {
		this.telephonyId = telephonyId;
	}

	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getMir3Id() {
		return mir3Id;
	}

	public void setMir3Id(String mir3Id) {
		this.mir3Id = mir3Id;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getFloor() {
		return floor;
	}

	public void setFloor(String floor) {
		this.floor = floor;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public List<String> getAlternates() {
		return alternates;
	}

	public void setAlternates(List<String> alternates) {
		this.alternates = alternates;
	}

	
}
