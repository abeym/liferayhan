package org.cdph.han.devpref.beans;

import java.util.ArrayList;
import java.util.List;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.persistence.EmailAddress;
import org.cdph.han.persistence.PhoneDevice;
import org.cdph.han.persistence.UserData;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
//import org.springframework.orm.jpa.JpaTemplate;

/**
 * This backed bean retrieves all the contact devices associated to the user.
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class ContactDevicesBean {
	/** Class logger */
	private static final Log log = LogFactory.getLog (ContactDevicesBean.class);
	/** Persistence context */
	//private EntityManager em;

	/**
	 * @return the devices
	 */
	public List<SelectItem> getDevices () {
		log.debug ("Retrieving devices.");
		List<SelectItem> devices = new ArrayList<SelectItem> ();
		/*try {
			ApplicationContext ctx = new ClassPathXmlApplicationContext ("/han_beans.xml", ContactDevicesBean.class);
//			JpaTemplate jpaTemplate = (JpaTemplate) ctx.getBean ("jpaTemplate");
			//em = jpaTemplate.getEntityManagerFactory ().createEntityManager ();
			EntityManagerFactory entityManagerFactory = (EntityManagerFactory) ctx.getBean ("entityManagerFactory");
			em = entityManagerFactory.createEntityManager ();
			ExternalContext context = FacesContext.getCurrentInstance ().getExternalContext ();
			Object requestObj = context.getRequest ();
			String userId = null;
			if (requestObj instanceof PortletRequest) {
				PortletRequest portletRequest = (PortletRequest) requestObj;
				userId = portletRequest.getRemoteUser ();
			} else if (requestObj instanceof HttpServletRequest) {
				HttpServletRequest request = (HttpServletRequest) requestObj;
				userId = request.getRemoteUser ();
			}
			UserData userData = em.find (UserData.class, Long.parseLong (userId));
			SelectItem device = new SelectItem ();
			device.setValue ("PRIMARY");
			device.setLabel ("Primary Email" + " - " + userData.getEmail ());
			devices.add (device);
			for (PhoneDevice phoneDevice : userData.getContact ().getPhoneDevices ()) {
				device = new SelectItem ();
				if (phoneDevice.getFriendlyName () != null && phoneDevice.getFriendlyName ().length () > 0) {
					device.setLabel (phoneDevice.getFriendlyName () + " - " + phoneDevice.getPhoneNumber ());
				} else {
					device.setLabel (phoneDevice.getPhoneNumber ());
				}
				device.setValue ("P:" + phoneDevice.getPhoneId ());
				devices.add (device);
			}
			for (EmailAddress email : userData.getContact ().getEmailAddresses ()) {
				device = new SelectItem ();
				if (email.getFriendlyName () != null && email.getFriendlyName ().length () > 0) {
					device.setLabel (email.getFriendlyName () + " - " + email.getAddress ());
				} else {
					device.setLabel (email.getAddress ());
				}
				device.setValue ("M:" + email.getEmailId ());
				devices.add (device);
			}
		} catch (Exception ex) {
			log.error ("Error retrieving devices.", ex);
		}*/
		return devices;
	}

}
