/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.cdph.han.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.cdph.han.model.TypeOrganizationRoles;
import org.cdph.han.service.TypeOrganizationRolesLocalServiceUtil;
import org.cdph.han.service.base.TypeOrganizationRolesLocalServiceBaseImpl;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactory;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.portlet.PortletClassLoaderUtil;

/**
 * The implementation of the type organization roles local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link org.cdph.han.service.TypeOrganizationRolesLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author zanonil
 * @see org.cdph.han.service.base.TypeOrganizationRolesLocalServiceBaseImpl
 * @see org.cdph.han.service.TypeOrganizationRolesLocalServiceUtil
 */
public class TypeOrganizationRolesLocalServiceImpl
	extends TypeOrganizationRolesLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link org.cdph.han.service.TypeOrganizationRolesLocalServiceUtil} to access the type organization roles local service.
	 */
	public List<TypeOrganizationRoles> findByOrganizationId(long orgTypeId) {
		List<TypeOrganizationRoles> orgRoleList = null;
		List<TypeOrganizationRoles> retval = new ArrayList<TypeOrganizationRoles>();
		try {
		    DynamicQuery roleQuery = DynamicQueryFactoryUtil.forClass(
		    		TypeOrganizationRoles.class, PortletClassLoaderUtil.getClassLoader())
//		    		.add(PropertyFactoryUtil.forName("organizationTypeId").eq(orgTypeId))
		    		;
		    PropertyFactory pf = PropertyFactoryUtil.getPropertyFactory();
		   // log.debug("role query: " + roleQuery + ", pf: " + pf);
			orgRoleList = TypeOrganizationRolesLocalServiceUtil.dynamicQuery(roleQuery);
			for (TypeOrganizationRoles orgRole : orgRoleList ) {
				if (orgRole.getOrganizationTypeId() == orgTypeId) retval.add(orgRole);
			}
		} catch (SystemException se) {
			//log.error("System Error: " + se);
		}
		return retval;
	}
}