package org.cdph.liferay.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
   
public class PwdGenerator {

	private static String KEY1 = "0123456789";
	private static String KEY2 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private static String KEY3 = "abcdefghijklmnopqrstuvwxyz";
	private static String KEY4 = "`~!@#$%^&*()_+-={}|\\:\";'<>?,./";
	

	public static String getPassword() {
		
		StringBuilder sb = new StringBuilder();
		
		List<String> keys = getRandomKeys();
		int randomKeyIndex;
		
		while(keys.size() > 0){
			
			randomKeyIndex = (int) (Math.random() * keys.size());
			String randomKey = keys.get(randomKeyIndex);
			
			for (int i = 0; i < 2; i++) {
				sb.append(randomKey.charAt((int) (Math.random() * randomKey.length())));
			}
			
			keys.remove(randomKeyIndex);
		}		
		
		if(sb.charAt(0) == '!' || sb.charAt(0) == '?'){
			randomKeyIndex = (int) (Math.random() * keys.size());
			sb.setCharAt(0, KEY1.charAt(randomKeyIndex));
		}

		return sb.toString();
	}


	private static ArrayList<String> getRandomKeys(){
		String[] keys = new String[4];
		
		List<Integer> indexes = new ArrayList<Integer>(4);
		indexes.add(0);
		indexes.add(1);
		indexes.add(2);
		indexes.add(3);
		
		int randomKeyIndex = (int) (Math.random() * indexes.size());	
		keys[indexes.get(randomKeyIndex)] = KEY1;
		indexes.remove(randomKeyIndex);
		
		randomKeyIndex = (int) (Math.random() * indexes.size());		
		keys[indexes.get(randomKeyIndex)] = KEY2;
		indexes.remove(randomKeyIndex);
		
		randomKeyIndex = (int) (Math.random() * indexes.size());		
		keys[indexes.get(randomKeyIndex)] = KEY3;
		indexes.remove(randomKeyIndex);
		
		randomKeyIndex = (int) (Math.random() * indexes.size());		
		keys[indexes.get(randomKeyIndex)] = KEY4;
		indexes.remove(randomKeyIndex);
		
		ArrayList<String> tmpArr =  new ArrayList<String>();
		
		for (int i = 0; i < keys.length; i++) {
			tmpArr.add(keys[i]);
		}
		
		return tmpArr;
	}
	
}
