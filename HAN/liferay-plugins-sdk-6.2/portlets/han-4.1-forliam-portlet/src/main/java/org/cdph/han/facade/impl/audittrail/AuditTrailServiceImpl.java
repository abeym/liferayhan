package org.cdph.han.facade.impl.audittrail;

import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.Order;
import com.liferay.portal.kernel.dao.orm.OrderFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.portlet.PortletClassLoaderUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.audittrail.dto.AuditTrailDTO;
import org.cdph.han.audittrail.dto.AuditTrailTO;
import org.cdph.han.audittrail.service.AuditTrailService;
import org.cdph.han.dto.HanLazyDataModel;
import org.cdph.han.model.AuditTrail;
import org.cdph.han.service.AuditTrailLocalServiceUtil;
import org.cdph.han.service.ClpSerializer;
import org.cdph.han.service.HanAuditTrailLocalServiceUtil;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Repository
@Service("auditTrailService")
public class AuditTrailServiceImpl implements AuditTrailService {

	/*@PostConstruct
	public void init() {
		try {
			PropertyMap<AuditTrail, AuditTrailTO> propertyMap = new PropertyMap<AuditTrail, AuditTrailTO>() {
				protected void configure() {
					try {
						map().setUser(source.getUserUuid());
					} catch (SystemException e) {
						log.error(e);
					}
				}
			};
			modelMapper.addMappings(propertyMap);
		} catch (Exception e) {
			log.error(e);
		}
	}*/

	/** Class logger */
	private static Log log = LogFactory.getLog(AuditTrailServiceImpl.class);
	@Autowired
	private ModelMapper modelMapper;

	//abey ...@Transactional(readOnly = true)
	public Integer getAuditTrailCount(HanLazyDataModel criteria1) {
		log.debug("Entry getAuditTrailCount:"+criteria1);

		try {
			AuditTrailDTO criteria = (AuditTrailDTO) criteria1;
			return (int) AuditTrailLocalServiceUtil.dynamicQueryCount(createQuery(criteria, true));
		} catch (SystemException e) {
			log.error(e);
		}
		return 0;
	}

	
	public void writeAuditRecord(String actionPerformed,long userId,long alertId, Date dateMod){
		long aid;
		try {
			aid = CounterLocalServiceUtil.increment();		
			org.cdph.han.model.HanAuditTrail  auditTrail  = HanAuditTrailLocalServiceUtil.createHanAuditTrail(aid);
			auditTrail.setActionPerformed(actionPerformed);
			auditTrail.setAlertId(alertId);
			auditTrail.setUserId(userId);
			auditTrail.setAuditTrailId (aid);
			auditTrail.setDate(dateMod);
			
			
			HanAuditTrailLocalServiceUtil.addHanAuditTrail (auditTrail);
			
			//HanAuditTrailLocalServiceUtil.updateHanAuditTrail(auditTrail);
			//impl
			
		} catch (SystemException e) {
			log.error(e);
		}
	}
	
	/*@Transactional(readOnly = true)
	public Integer getAuditTrailCount(AuditTrailDTO criteria) {

		Query query = createQuery(criteria, true);

		return ((Number) query.getSingleResult()).intValue();
	}*/

	@SuppressWarnings("unchecked")
	public List<AuditTrailTO> getAuditTrailRecords(HanLazyDataModel criteria1,
			int startIndex, int finishIndex) {
		log.debug("Entry getAuditTrailRecords:"+criteria1 +":"+startIndex+":"+finishIndex);
		AuditTrailDTO criteria = (AuditTrailDTO) criteria1;
		List<AuditTrailTO> returnList = new ArrayList<AuditTrailTO>();
		try {
			DynamicQuery query = createQuery(criteria, false);
			List<AuditTrail> auditTrails = AuditTrailLocalServiceUtil.dynamicQuery(query, startIndex, finishIndex);
			for(AuditTrail trail:auditTrails)
			{
				returnList.add(convertDTO(trail));
			}
		} catch (SystemException e) {
			log.error(e);
		}
		catch (Exception e) {
			log.error(e);
		}
		return returnList;
	}

	/*@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<AuditTrailTO> getAuditTrailRecords(AuditTrailDTO criteria,
			int startIndex, int maxResults) {

		Query query = createQuery(criteria, false);
		final List<AuditTrailTO> result = new ArrayList<AuditTrailTO> ();
		final List<AuditTrail> dbResult = query.setFirstResult (startIndex
				).setMaxResults (maxResults).getResultList ();
		AuditTrailTO dto;
		UserData userData;
		DeletedUser deletedUser;
		for (AuditTrail trail : dbResult) {
			dto = new AuditTrailTO ();
			dto.setActionPerformed (trail.getActionPerformed ());
			dto.setAlertId (trail.getAlertId ());
			dto.setDatePerformed (trail.getDatePerformed ());
			userData = em.find (UserData.class, trail.getUser ());
			if (userData != null) {
				dto.setUser (userData.getEmail ());
			} else {
				deletedUser = em.find (DeletedUser.class, trail.getUser ());
				if (deletedUser == null) {
					dto.setUser ("User Deleted (ID: " + trail.getUser () + ")");
				} else {
					dto.setUser ("User Deleted (" + deletedUser.getEmail () + ")");
				}
			}
			result.add (dto);
		}

		return result;
	}*/

	private AuditTrailTO convertDTO(org.cdph.han.model.AuditTrail trail)
	{
		AuditTrailTO dto = modelMapper.map(trail, AuditTrailTO.class);
		return dto;
	}
	
	/*private AuditTrailTO convertDTO_1(org.cdph.han.model.AuditTrail trail)
	{
		AuditTrailTO dto = new AuditTrailTO ();
		//dto.setActionPerformed (trail.getActionPerformed ());
		dto.setAlertId (trail.getAlertId ());
		dto.setDatePerformed (trail.getDatePerformed ());
		//userData = em.find (UserData.class, trail.getUser ());
		UserData userData = new UserData();
		if (userData != null) {
			dto.setUser (userData.getEmail ());
		} else {
			//deletedUser = em.find (DeletedUser.class, trail.getUser ());
			DeletedUser deletedUser = null;
			if (deletedUser == null) {
				dto.setUser ("User Deleted (ID: " + trail.getUserId() + ")");
			} else {
				dto.setUser ("User Deleted (" + deletedUser.getEmail () + ")");
			}
		}
		return dto;
	}*/

	private DynamicQuery createQuery(AuditTrailDTO criteria, boolean count){
		log.debug("Entry createQuery:"+criteria +":"+count);


		//DynamicQuery query = DynamicQueryFactoryUtil.forClass(AuditTrail.class);
		//DynamicQuery query = DynamicQueryFactoryUtil.forClass(AuditTrail.class, AuditTrail.class.getClassLoader());
		//DynamicQuery query = DynamicQueryFactoryUtil.forClass(AuditTrail.class, PortletClassLoaderUtil.getClassLoader());
		ClassLoader classLoader = (ClassLoader)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),"portletClassLoader");
		DynamicQuery query = DynamicQueryFactoryUtil.forClass(AuditTrail.class, classLoader);
		
		    Order pubOrder = OrderFactoryUtil.desc("datePerformed");
		    query.addOrder(pubOrder);
		StringBuffer queryStr = new StringBuffer();
		
		if(count){
			query.setProjection(ProjectionFactoryUtil.rowCount());
		}

		int paramCount = 0;

		if (criteria.getStartDate() != null) {
			query.add(PropertyFactoryUtil.forName("datePerformed").ge(criteria.getStartDate()));
		}

		if (criteria.getEndDate() != null) {
			query.add(PropertyFactoryUtil.forName("datePerformed").lt(new Date(criteria.getEndDate().getTime() + (1000 * 60 * 60 * 24))));
		}

		if (!emptyString(criteria.getPerformedBy())) {
			query.add(PropertyFactoryUtil.forName("userId").eq(criteria.getPerformedBy()));
		}
		if (criteria.getActionPerformed() != null) {
			query.add(PropertyFactoryUtil.forName("actionPerformed").eq(criteria.getActionPerformed().getDisplayFormat()));
		}
		return query;
	}


	/*private Query createQuery_1(AuditTrailDTO criteria, boolean count){
		
		StringBuffer queryStr = new StringBuffer();
		
		if(count){
			queryStr.append("Select count(a) from AuditTrail a");
		}else{
			queryStr.append("Select a from AuditTrail a");
		}

		StringBuffer params = new StringBuffer();
		
		boolean[] paramPresent = new boolean[5];
		int paramCount = 0;

		if (criteria.getStartDate() != null) {
			params.append(" a.datePerformed >= ?" + ++paramCount);
			paramPresent[0] = true;
		}

		if (criteria.getEndDate() != null) {
			if(params.length() > 0){
				params.append(" AND");
			}
			params.append(" a.datePerformed < ?" + ++paramCount);
			paramPresent[1] = true;
		}

		if (!emptyString(criteria.getPerformedBy())) {
			if(params.length() > 0){
				params.append(" AND");
			}
			params.append(" a.user IN (Select u.userId from UserData u where u.email LIKE ?" + ++paramCount + ")");
			paramPresent[2] = true;
		}

		if (criteria.getActionPerformed() != null) {
			if(params.length() > 0){
				params.append(" AND");
			}
			params.append(" a.actionPerformed LIKE ?" + ++paramCount);
			paramPresent[3] = true;
		}

		if (!emptyString(criteria.getResult())) {
			if(params.length() > 0){
				params.append(" AND");
			}
			params.append(" a.result LIKE ?" + ++paramCount);
			paramPresent[4] = true;
		}

		if(params.length() > 0){
			params.insert(0, " WHERE ");
		}
		
		queryStr.append(params.toString());
		
		queryStr.append(" ORDER BY a.datePerformed DESC");

		log.debug(queryStr);
		//TODO for compilation...
		EntityManager em = null;
		Query query = em.createQuery(queryStr.toString());

		paramCount = 0;
		
		if (paramPresent[0]) {
			log.debug("Start Date: " + criteria.getStartDate());
			query.setParameter(++paramCount, criteria.getStartDate(), TemporalType.DATE);
		}
		if (paramPresent[1]) {
			Calendar cal = new GregorianCalendar();
			cal.setTime(criteria.getEndDate());
			cal.add(Calendar.DAY_OF_YEAR, 1);	
			log.debug("End Date: " + cal.getTime());
			query.setParameter(++paramCount, cal.getTime(), TemporalType.DATE);
		}
		if (paramPresent[2]) {
			query.setParameter(++paramCount, criteria.getPerformedBy());
		}
		if (paramPresent[3]) {
			query.setParameter(++paramCount, criteria.getActionPerformed());
		}
		if (paramPresent[4]) {
			query.setParameter(++paramCount, criteria.getResult());
		}
		
		return query;
		
	}*/

	private boolean emptyString(String value) {
		return (value == null || value.trim().length() == 0);
	}

}
