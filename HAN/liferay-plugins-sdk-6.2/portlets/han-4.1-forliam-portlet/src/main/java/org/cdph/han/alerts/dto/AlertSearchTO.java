package org.cdph.han.alerts.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * Base class for all the variants in the alert search functionality
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public abstract class AlertSearchTO implements Serializable {
	/** Class serial version UID */
	private static final long serialVersionUID = -4476427890754896821L;
	/** The alert id */
	private long alertId;
	/** The alert title */
	private String title;
	/** The alert topic */
	private String topic;
	/** Ident style, used for grouping */
	private String identStyle;
	
	//abey - for compiling
	private boolean archived;
	private Date publicationDate;
	protected Date lastModifiedDate = new Date();
	protected Date creationDate = new Date();
	
	
	private String author;
	private String contact;
	private String modifier;
	private Date dateCreated;
	private Date datePublished;
	private String publisher;
	private Date dateModified;
	private Date dateScheduled;
	private String name;
	private Date dateExpired;
	private boolean validateRecipients;
	private boolean replayMessages;
	private boolean oneTextContact;
	private boolean requiresPIN;
	private boolean confirmResponse;
	private boolean reportRecipients;
	private boolean playGreeting;
	private boolean useAlternates;
	private boolean selectLanguage;
	private boolean useTopics;
	private boolean sendToSubscribers;
	private String category;
	private String priority;
	private String severity;
	private String contactCycleDelay;
	private String textDeviceDelay;
	private String contactAttempCycles;
	private boolean expeditedDelivery;
	private String leaveMessage;
	private String topicId;
	private Date incidentDate;
	private String duration;
	private boolean entireMachine;
	private boolean entireMessage;
	private boolean answerPhone;
	private String notificationMethod;
	private String voiceDelivery;
	private boolean voiceFile;
	private boolean readyToPublish;
	private boolean deleted;
	private boolean overrideDefaultsOnly;
	private boolean locationOverride;

	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean equal = false;
		if (obj != null && obj instanceof AlertSearchTO) {
			final AlertSearchTO other = (AlertSearchTO) obj;
			equal = other.getAlertId () == alertId;
		}
		return equal;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		return Long.valueOf (alertId).hashCode ();
	}

	/**
	 * @return the alertId
	 */
	public long getAlertId () {
		return alertId;
	}

	/**
	 * @param alertId the alertId to set
	 */
	public void setAlertId (long alertId) {
		this.alertId = alertId;
	}

	/**
	 * @return the title
	 */
	public String getTitle () {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle (String title) {
		this.title = title;
	}

	/**
	 * @return the topic
	 */
	public String getTopic () {
		return topic;
	}

	/**
	 * @param topic the topic to set
	 */
	public void setTopic (String topic) {
		this.topic = topic;
	}

	/**
	 * @return the identStyle
	 */
	public String getIdentStyle () {
		return identStyle;
	}

	/**
	 * @param identStyle the identStyle to set
	 */
	public void setIdentStyle (String identStyle) {
		this.identStyle = identStyle;
	}

	/**
	 * @return the specialized
	 */
	public boolean isSpecialized () {
		return false;
	}

	public boolean isArchived() {
		return archived;
	}

	public void setArchived(boolean archived) {
		this.archived = archived;
	}

	public Date getPublicationDate() {
		return publicationDate;
	}

	public void setPublicationDate(Date publicationDate) {
		this.publicationDate = publicationDate;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDatePublished() {
		return datePublished;
	}

	public void setDatePublished(Date datePublished) {
		this.datePublished = datePublished;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public Date getDateModified() {
		return dateModified;
	}

	public void setDateModified(Date dateModified) {
		this.dateModified = dateModified;
	}

	public Date getDateScheduled() {
		return dateScheduled;
	}

	public void setDateScheduled(Date dateScheduled) {
		this.dateScheduled = dateScheduled;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDateExpired() {
		return dateExpired;
	}

	public void setDateExpired(Date dateExpired) {
		this.dateExpired = dateExpired;
	}

	public boolean isValidateRecipients() {
		return validateRecipients;
	}

	public void setValidateRecipients(boolean validateRecipients) {
		this.validateRecipients = validateRecipients;
	}

	public boolean isReplayMessages() {
		return replayMessages;
	}

	public void setReplayMessages(boolean replayMessages) {
		this.replayMessages = replayMessages;
	}

	public boolean isOneTextContact() {
		return oneTextContact;
	}

	public void setOneTextContact(boolean oneTextContact) {
		this.oneTextContact = oneTextContact;
	}

	public boolean isRequiresPIN() {
		return requiresPIN;
	}

	public void setRequiresPIN(boolean requiresPIN) {
		this.requiresPIN = requiresPIN;
	}

	public boolean isConfirmResponse() {
		return confirmResponse;
	}

	public void setConfirmResponse(boolean confirmResponse) {
		this.confirmResponse = confirmResponse;
	}

	public boolean isReportRecipients() {
		return reportRecipients;
	}

	public void setReportRecipients(boolean reportRecipients) {
		this.reportRecipients = reportRecipients;
	}

	public boolean isPlayGreeting() {
		return playGreeting;
	}

	public void setPlayGreeting(boolean playGreeting) {
		this.playGreeting = playGreeting;
	}

	public boolean isUseAlternates() {
		return useAlternates;
	}

	public void setUseAlternates(boolean useAlternates) {
		this.useAlternates = useAlternates;
	}

	public boolean isSelectLanguage() {
		return selectLanguage;
	}

	public void setSelectLanguage(boolean selectLanguage) {
		this.selectLanguage = selectLanguage;
	}

	public boolean isUseTopics() {
		return useTopics;
	}

	public void setUseTopics(boolean useTopics) {
		this.useTopics = useTopics;
	}

	public boolean isSendToSubscribers() {
		return sendToSubscribers;
	}

	public void setSendToSubscribers(boolean sendToSubscribers) {
		this.sendToSubscribers = sendToSubscribers;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getSeverity() {
		return severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}

	public String getContactCycleDelay() {
		return contactCycleDelay;
	}

	public void setContactCycleDelay(String contactCycleDelay) {
		this.contactCycleDelay = contactCycleDelay;
	}

	public String getTextDeviceDelay() {
		return textDeviceDelay;
	}

	public void setTextDeviceDelay(String textDeviceDelay) {
		this.textDeviceDelay = textDeviceDelay;
	}

	public String getContactAttempCycles() {
		return contactAttempCycles;
	}

	public void setContactAttempCycles(String contactAttempCycles) {
		this.contactAttempCycles = contactAttempCycles;
	}

	public boolean isExpeditedDelivery() {
		return expeditedDelivery;
	}

	public void setExpeditedDelivery(boolean expeditedDelivery) {
		this.expeditedDelivery = expeditedDelivery;
	}

	public String getLeaveMessage() {
		return leaveMessage;
	}

	public void setLeaveMessage(String leaveMessage) {
		this.leaveMessage = leaveMessage;
	}

	public String getTopicId() {
		return topicId;
	}

	public void setTopicId(String topicId) {
		this.topicId = topicId;
	}

	public Date getIncidentDate() {
		return incidentDate;
	}

	public void setIncidentDate(Date incidentDate) {
		this.incidentDate = incidentDate;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public boolean isEntireMachine() {
		return entireMachine;
	}

	public void setEntireMachine(boolean entireMachine) {
		this.entireMachine = entireMachine;
	}

	public boolean isEntireMessage() {
		return entireMessage;
	}

	public void setEntireMessage(boolean entireMessage) {
		this.entireMessage = entireMessage;
	}

	public boolean isAnswerPhone() {
		return answerPhone;
	}

	public void setAnswerPhone(boolean answerPhone) {
		this.answerPhone = answerPhone;
	}

	public String getNotificationMethod() {
		return notificationMethod;
	}

	public void setNotificationMethod(String notificationMethod) {
		this.notificationMethod = notificationMethod;
	}

	public String getVoiceDelivery() {
		return voiceDelivery;
	}

	public void setVoiceDelivery(String voiceDelivery) {
		this.voiceDelivery = voiceDelivery;
	}

	public boolean isVoiceFile() {
		return voiceFile;
	}

	public void setVoiceFile(boolean voiceFile) {
		this.voiceFile = voiceFile;
	}

	public boolean isReadyToPublish() {
		return readyToPublish;
	}

	public void setReadyToPublish(boolean readyToPublish) {
		this.readyToPublish = readyToPublish;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public boolean isOverrideDefaultsOnly() {
		return overrideDefaultsOnly;
	}

	public void setOverrideDefaultsOnly(boolean overrideDefaultsOnly) {
		this.overrideDefaultsOnly = overrideDefaultsOnly;
	}

	public boolean isLocationOverride() {
		return locationOverride;
	}

	public void setLocationOverride(boolean locationOverride) {
		this.locationOverride = locationOverride;
	}

	
}
