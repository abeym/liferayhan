package org.cdph.han.dto;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.icefaces.ace.model.table.LazyDataModel;
import org.icefaces.ace.model.table.SortCriteria;

public abstract class HanLazyDataModel<E> extends LazyDataModel<E> {

	private static final long serialVersionUID = 1L;
	/** Class logger */
	private static final Log log = LogFactory.getLog(HanLazyDataModel.class);

	@Override
	public List<E> load(int first, int pageSize, SortCriteria[] sortCriteria,
			Map<String, String> filters) {
		return findRows(first, first+pageSize-1);
	}


	public abstract List<E> findRows(int startRow, int finishRow);
	
	/*
	// Private Constants
	private static final String DEFAULT_SORT_CRITERIA = "lastName";

	// Private Data Members
	private long companyId;
	private int rowsPerPage;

	public UserLazyDataModel(long companyId, int rowsPerPage) {

		this.companyId = companyId;
		setRowsPerPage(rowsPerPage);
		setRowCount(countRows());
	}

	public int countRows() {

		int totalCount = 0;

		try {
			LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
			params.put("expandoAttributes", null);

			Sort sort = SortFactoryUtil.getSort(User.class, DEFAULT_SORT_CRITERIA, "asc");

			boolean andSearch = true;
			int status = WorkflowConstants.STATUS_ANY;

			String firstName = null;
			String middleName = null;
			String lastName = null;
			String screenName = null;
			String emailAddress = null;

			Hits hits = UserLocalServiceUtil.search(companyId, firstName, middleName, lastName, screenName,
					emailAddress, status, params, andSearch, QueryUtil.ALL_POS, QueryUtil.ALL_POS, sort);
			totalCount = hits.getLength();

		}
		catch (Exception e) {
			logger.error(e.getMessage(), e);
		}

		return totalCount;
	}

	/**
	 * This method is called by the ICEfaces {@link DataTable} according to the rows specified in the currently
	 * displayed page of data.
	 *
	 * @param  first          The zero-relative first row index.
	 * @param  pageSize       The number of rows to fetch.
	 * @param  sortCriterias  The array of sort criteria objects. Note that since the Liferay API does not support
	 *                        multiple sort criterias, the length of this array will never be greater than one.
	 * @param  filters        The query criteria. Note that in order for the filtering to work with the Liferay API, the
	 *                        end-user must specify complete, matching words. Wildcards and partial matches are not
	 *                        supported.
	 * /
	@Override
	public List<User> load(int first, int pageSize, SortCriteria[] sortCriterias, Map<String, String> filters) {

		List<User> users = null;

		Sort sort;

		// sort
		if ((sortCriterias != null) && (sortCriterias.length != 0)) {

			if (!sortCriterias[0].isAscending()) {
				sort = SortFactoryUtil.getSort(User.class, sortCriterias[0].getPropertyName(), "desc");
			}
			else {
				sort = SortFactoryUtil.getSort(User.class, sortCriterias[0].getPropertyName(), "asc");
			}
		}
		else {
			sort = SortFactoryUtil.getSort(User.class, DEFAULT_SORT_CRITERIA, "asc");
		}

		try {
			LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
			int liferayOneRelativeFinishRow = first + pageSize + 1;

			boolean andSearch = true;
			int status = WorkflowConstants.STATUS_ANY;

			String firstName = trimExpresssion(filters.get("firstName"));
			String middleName = trimExpresssion(filters.get("middleName"));
			String lastName = trimExpresssion(filters.get("lastName"));
			String screenName = trimExpresssion(filters.get("screenName"));
			String emailAddress = trimExpresssion(filters.get("emailAddress"));

			// For the sake of speed, search for users in the index rather than querying the database directly.
			Hits hits = UserLocalServiceUtil.search(companyId, firstName, middleName, lastName, screenName,
					emailAddress, status, params, andSearch, first, liferayOneRelativeFinishRow, sort);

			List<Document> documentHits = hits.toList();

			logger.debug(
				("filters firstName=[{0}] middleName=[{1}] lastName=[{2}] screenName=[{3}] emailAddress=[{4}] active=[{5}] andSearch=[{6}] startRow=[{7}] liferayOneRelativeFinishRow=[{8}] sortColumn=[{9}] reverseOrder=[{10}] hitCount=[{11}]"),
				firstName, middleName, lastName, screenName, emailAddress, status, andSearch, first,
				liferayOneRelativeFinishRow, sort.getFieldName(), sort.isReverse(), documentHits.size());

			// Convert the results from the search index into a list of user objects.
			users = new ArrayList<User>(documentHits.size());

			for (Document document : documentHits) {

				long userId = GetterUtil.getLong(document.get(Field.USER_ID));

				try {
					User user = UserLocalServiceUtil.getUserById(userId);
					users.add(user);
				}
				catch (NoSuchUserException nsue) {
					logger.error("User with userId=[{0}] does not exist in the search index. Please reindex.");
				}
			}

		}
		catch (Exception e) {
			logger.error(e.getMessage(), e);
		}

		return users;

	}

	protected String trimExpresssion(String value) {

		String expression = null;

		if (value != null) {
			String trimmedValue = value.trim();

			if (trimmedValue.length() > 0) {
				expression = trimmedValue.toLowerCase();
			}
		}

		return expression;
	}

	public int getRowsPerPage() {
		return rowsPerPage;
	}

	public void setRowsPerPage(int rowsPerPage) {
		this.rowsPerPage = rowsPerPage;
	}

	 */
	/*@Override
	public void deleteRow(Object primaryKey) throws IOException {
		List<E> wrappedData = getWrappedData();
		int i = 0;
		for (E d : wrappedData) {
			if (getPrimaryKey(d).equals(primaryKey)) {
				wrappedData.remove(i);
				break;
			}
			i++;
		}
	}

	public void clearCache() {
		setRowCount(-1);
		setWrappedData(null);
		setWrappedDataStartRowIndex(-1);
		setWrappedDataFinishRowIndex(-1);
	}

	@Override
	public Object getPrimaryKey(E e) {
		Field[] fields = e.getClass().getDeclaredFields();
		StringBuilder result = new StringBuilder();
		for (Field field : fields) {
			try {
				result.append(field.get(this)).append(":");
			} catch (IllegalAccessException ex) {
				log.error(ex);
			}
		}
		return "";
	}*/

}