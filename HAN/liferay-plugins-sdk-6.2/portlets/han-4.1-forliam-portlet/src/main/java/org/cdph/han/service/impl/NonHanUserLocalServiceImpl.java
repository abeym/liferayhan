/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.cdph.han.service.impl;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.portlet.PortletClassLoaderUtil;

import java.util.List;

import org.cdph.han.model.NonHanUser;
import org.cdph.han.service.NonHanUserLocalServiceUtil;
import org.cdph.han.service.base.NonHanUserLocalServiceBaseImpl;

/**
 * The implementation of the non han user local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link org.cdph.han.service.NonHanUserLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author zanonil
 * @see org.cdph.han.service.base.NonHanUserLocalServiceBaseImpl
 * @see org.cdph.han.service.NonHanUserLocalServiceUtil
 */
public class NonHanUserLocalServiceImpl extends NonHanUserLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link org.cdph.han.service.NonHanUserLocalServiceUtil} to access the non han user local service.
	 */

	public List<NonHanUser> findByGroupIds(String[] groupIds) {
		List<NonHanUser> userList = null;
		System.out.println("Group Ids: " + groupIds);
		try {
		    DynamicQuery query = DynamicQueryFactoryUtil.forClass(
		    		NonHanUser.class, PortletClassLoaderUtil.getClassLoader())
			.add(PropertyFactoryUtil.forName("nonHanGroupId").in(groupIds));
			userList = NonHanUserLocalServiceUtil.dynamicQuery(query);
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return userList;
	}
}