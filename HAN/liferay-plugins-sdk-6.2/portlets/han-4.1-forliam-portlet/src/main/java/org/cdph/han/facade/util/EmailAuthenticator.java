package org.cdph.han.facade.util;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

/**
 * Custom authenticator for the email service
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class EmailAuthenticator extends Authenticator {
	/** The username to use for the email server */
	private String userName;
	/** The username password */
	private String password;

	/**
	 * Constructor to set the user name and password
	 * @param userName user name
	 * @param password password
	 */
	public EmailAuthenticator (final String userName, final String password) {
		this.userName = userName;
		this.password = password;
	}

	/* (non-Javadoc)
	 * @see javax.mail.Authenticator#getPasswordAuthentication()
	 */
	@Override
	protected PasswordAuthentication getPasswordAuthentication () {
		return new PasswordAuthentication (userName, password);
	}

}
