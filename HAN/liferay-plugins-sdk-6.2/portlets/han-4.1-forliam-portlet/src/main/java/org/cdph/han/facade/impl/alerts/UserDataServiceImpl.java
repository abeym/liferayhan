package org.cdph.han.facade.impl.alerts;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;
//import javax.persistence.EntityManager;
//import javax.persistence.PersistenceContext;
//import javax.persistence.Query;
import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.alerts.services.*;
import org.cdph.han.custgroups.dto.UserDisplayTO;
import org.cdph.han.facade.impl.devpref.ContactDevicesServiceImpl;
import org.cdph.han.persistence.UserData;
import org.cdph.han.persistence.UserOrganizationalData;
import org.cdph.han.service.AlternateContactLocalServiceUtil;
import org.cdph.han.service.ContactPreferenceLocalServiceUtil;
import org.cdph.han.service.HanUserDataLocalServiceUtil;
import org.cdph.han.service.PhoneDeviceLocalServiceUtil;
import org.cdph.han.service.UserDataLocalServiceUtil;
import org.cdph.han.service.UserOrganizationalDataLocalServiceUtil;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.InfrastructureUtil;
import com.liferay.portal.model.Contact;
import com.liferay.portal.model.EmailAddress;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.Phone;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.model.UserGroup;
import com.liferay.portal.model.UserGroupRole;
import com.liferay.portal.service.EmailAddressLocalServiceUtil;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.UserGroupLocalServiceUtil;
import com.liferay.portal.service.UserGroupRoleLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;

/**
 * Implementation of the UserDataService contract, this implementation uses JSF
 * @author Abey Mani
 * @version 1.0
 */
@Repository
@Service ("userService")
public class UserDataServiceImpl implements UserDataService {
	private static final Log log = LogFactory.getLog (UserDataServiceImpl.class);
	@Autowired
	private ModelMapper modelMapper;

	/** JPA entity manager */
	//private EntityManager entityManager;

	/**
	 * Setter to inject the persistence context
	 * @param em JPA Entity Manager
	 */
	//abey...
	//@PersistenceContext
/*	public void setEntityManager (final EntityManager entityManager) {
		//this.entityManager = entityManager;
	}
*/
	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.UserDataService#getCurrentUserData()
	 */
	//abey ...@Transactional (propagation = Propagation.SUPPORTS)
	public UserData getCurrentUserData () {
		final Object requestObj = FacesContext.getCurrentInstance ().getExternalContext ().getRequest ();
		String userId;
		UserData userData = null;
		if (requestObj instanceof HttpServletRequest) {
			userId = ((HttpServletRequest) requestObj).getRemoteUser ();
		} else if (requestObj instanceof PortletRequest) {
			userId = ((PortletRequest) requestObj).getRemoteUser ();
		} else {
			userId = null;
		}
		if (userId != null) {
			long primaryKey = Long.parseLong (userId);
			userData = getUser (primaryKey);
		}
		return userData;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.UserDataService#getAlertInitiators()
	 */
	//abey ...@Transactional (readOnly = true)
	public List<?> getAlertInitiators () {
		List<UserData> foundUsers=new ArrayList<UserData>();
		//Query q = entityManager.createNamedQuery ("UserData.FindAlertInitiator");
		//final List<?> foundUsers = q.getResultList ();
		//TODO: add intiator query, this data filters for users that can send an alert uses UserData.FindAlertInitiator
		try {
			List<org.cdph.han.model.UserData>lUsers = UserDataLocalServiceUtil.getUserDatas(-1, -1);
			for(Object o:lUsers){
				org.cdph.han.model.UserData lu = (org.cdph.han.model.UserData)o;
				UserData  ud =this.buildUpUser(lu);
				foundUsers.add(ud);
			}
		} catch (SystemException e) {
			log.error(e);
		}
		return foundUsers;//foundUsers;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.UserDataService#findUsersByFirstAndLastName(java.lang.String, java.lang.String)
	 */
	//abey ...@Transactional (readOnly = true)
	public List<UserDisplayTO> findUsersByFirstAndLastName (final String firstName, final String lastName) {
		final List<UserDisplayTO> foundUsers = new ArrayList<UserDisplayTO> ();
		log.debug("Run> UserData.FindByFNLN with first name " + firstName + " and last name " + lastName);
		DataSource liferayDataSource = InfrastructureUtil.getDataSource();
		try {
			Statement s = liferayDataSource.getConnection().createStatement();
			String sql = "select u.*, c.*, uo.organizationId, uaf.roleId"
					+ "  from Contact_ c, User_ u "
					+ "   LEFT JOIN Users_Orgs uo on u.userId=uo.userId"
					+ "    LEFT JOIN users_areafields uaf on uo.userId=uaf.userId and uo.organizationId=uaf.organizationId"
					+" where u.contactId = c.contactId";							//  and u.userId = ur.userId
			//and c.firstName like ? and c.lastName like ? ";
			final boolean filterByFirstName = firstName != null && firstName.length () > 0;
			final boolean filterByLastName = lastName != null && lastName.length () > 0;
			if (filterByFirstName) {
				sql += " and upper(c.firstName) like \"" + firstName.toUpperCase () + "%\"";
				log.trace("Query add first>  " + firstName.toUpperCase () + "%");
			}
			if (filterByLastName) {
				sql += " and upper(c.lastName) like \"" + lastName.toUpperCase () + "%\"";
				log.trace("Query add last>  " + lastName.toUpperCase () + "%");
			}
			log.debug("Run>  " + sql);
			ResultSet result = s.executeQuery(sql);

			while (result.next()) {
				log.debug("Role Id: " + result.getLong("roleId"));
				String orgStr = "-";
				String roleStr = "-";
				if (result.getString("uo.organizationId") != null && result.getLong("uo.organizationId") > 0) {
					Organization org = OrganizationLocalServiceUtil.getOrganization(result.getLong("uo.organizationId"));
					orgStr = org.getName();
				}
				if (result.getString("uaf.roleId") != null && result.getLong("uaf.roleId") > 0) {
					Role role = RoleLocalServiceUtil.getRole(result.getLong("uaf.roleId"));
					roleStr = role.getName();
				}
				foundUsers.add (new UserDisplayTO (result.getLong("u.userId"),
						result.getString("c.firstName"),
						result.getString("c.lastName"),
						orgStr, roleStr));
			}
			result.close();
			s.close();
		} catch (Exception e) {
			System.err.println("ERROR Class not found: " + e);
		}
		return foundUsers;
	}
	
	private UserData buildUpUser(org.cdph.han.model.UserData u){
		UserData ud = new UserData();
		try {
			
			User luser = UserLocalServiceUtil.getUser(u.getUserId());
			Contact lc = luser.getContact();
			org.cdph.han.persistence.Contact hc = new org.cdph.han.persistence.Contact();
			ud.setDepartment(u.getDepartment());
			ud.setEmail(u.getEmailPassword());
			ud.setEmailPassword(u.getEmailPassword());
			ud.setMir3Id(u.getMir3Id());
			ud.setUserId(u.getUserId());
			ud.setScreenName(luser.getScreenName());
			//TODO: not sure of this 
			ud.setActive(luser.getStatus()==0);
			//ud.set(u.getUserUuid());
			hc.setContactId(lc.getContactId());
			//TODO: work through this
			//hc.setEmailAddresses(lc.getEmailAddress());
			hc.setFirstName(lc.getFirstName());
			hc.setJobTitle(lc.getJobTitle());
			hc.setLastName(lc.getLastName());
			hc.setMiddleName(lc.getMiddleName());
			//TODO:need a join (or something) to a HAN table
			//hc.setPhoneDevices(lc.getP);
			ud.setContact(hc);
			
		} catch (PortalException e) {
			log.error(e);
		} catch (SystemException e) {
			log.error(e);
		}catch (Exception e) {
			log.error(e);
		}
		return ud;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.UserDataService#getUser(long)
	 */
	//abey ...@Transactional (readOnly = true)
	public UserData getUser (final long userId) {
		UserData ud = new UserData();
		try {
			User luser = UserLocalServiceUtil.getUser(userId);
			org.cdph.han.model.UserData u = UserDataLocalServiceUtil.getUserData(userId);
			ud.setDepartment(u.getDepartment());
			ud.setEmailPassword(u.getEmailPassword());
			ud.setMir3Id(u.getMir3Id());
			ud.setTelephonyId(u.getTelephonyId());
			ud.setTelephonyPassword(u.getTelephonyPassword());
			ud.setTelephonyPIN(u.getTelephonyPIN());
			ud.setScreenName(luser.getScreenName());
			//TODO: not sure of this 
			//ud.setActive(u.get);
		} catch (PortalException e) {
			log.error(e);
		} catch (SystemException e) {
			log.error(e);
		}
		try {
			User luser = UserLocalServiceUtil.getUser(userId);
			Contact lc = luser.getContact();
			org.cdph.han.persistence.Contact hc = new org.cdph.han.persistence.Contact();
			ud.setUserId(userId);
			//ud.set(u.getUserUuid());
			hc.setContactId(lc.getContactId());
			//TODO: work through this
			//hc.setEmailAddresses(lc.getEmailAddress());
			ud.setEmail(lc.getEmailAddress());
			hc.setFirstName(lc.getFirstName());
			hc.setJobTitle(lc.getJobTitle());
			hc.setLastName(lc.getLastName());
			hc.setMiddleName(lc.getMiddleName());
			List<EmailAddress> emailAddress = new ArrayList<EmailAddress>();
			if (luser.getEmailAddresses() != null) {
				hc.setEmailAddresses(new ArrayList<org.cdph.han.persistence.EmailAddress>());
				for (EmailAddress email : luser.getEmailAddresses()) {
					org.cdph.han.persistence.EmailAddress persistEmail = new org.cdph.han.persistence.EmailAddress();
					persistEmail.setAddress(email.getAddress());
					persistEmail.setFriendlyName("Friendly " + email.getAddress());
					persistEmail.setEmailId(email.getPrimaryKey());
					hc.getEmailAddresses().add(persistEmail);
				}
			}
			ud.setContact(hc);
			ud.setActive(luser.getStatus()==0);

			log.info("Phone devices: " + luser.getPhones());
			if (luser.getPhones() != null) {
				hc.setPhoneDevices(new ArrayList<org.cdph.han.persistence.PhoneDevice>());
				for (Phone phone : luser.getPhones()) {
					org.cdph.han.persistence.PhoneDevice phoneDevice = new org.cdph.han.persistence.PhoneDevice();
					phoneDevice.setPhoneNumber(phone.getNumber());
					phoneDevice.setExtension(phone.getExtension());
					phoneDevice.setTypeId(phone.getTypeId());
					phoneDevice.setPhoneId(phone.getPhoneId());
					log.debug("Add phone device: " + phoneDevice.getPhoneId() + " - " + phoneDevice.getPhoneNumber() + ", type " + phoneDevice.getTypeId());
					hc.getPhoneDevices().add(phoneDevice);
				}
			}
			List<UserGroup> userGroups = UserGroupLocalServiceUtil.getUserUserGroups(userId);
			if (userGroups != null) {
			}
			List<Role> userRoles = RoleLocalServiceUtil.getUserRoles(userId);
			if (userRoles != null) {
				ud.setRoles(new ArrayList<org.cdph.han.persistence.Role>());
				for (Role role : userRoles) {
					org.cdph.han.persistence.Role pRole = new org.cdph.han.persistence.Role();
					pRole.setRoleId(role.getRoleId());
					pRole.setName(role.getName());
					ud.getRoles().add(pRole);
				}
			}
			List<UserGroupRole> userGroupRoles = UserGroupRoleLocalServiceUtil.getUserGroupRoles(userId);
			if (userGroupRoles != null) {
				ud.setGroupRoles(new ArrayList<org.cdph.han.persistence.Role>());
				for (UserGroupRole ugRole : userGroupRoles) {
					org.cdph.han.persistence.Role pRole = new org.cdph.han.persistence.Role();
					pRole.setRoleId(ugRole.getRoleId());
					List<org.cdph.han.persistence.Organization> pOrgs = new ArrayList<org.cdph.han.persistence.Organization>();
					for (Organization org : ugRole.getUser().getOrganizations()) {
						org.cdph.han.persistence.Organization pOrg = new org.cdph.han.persistence.Organization();
						pOrg.setName(org.getName());
						pOrg.setOrganizationId(org.getOrganizationId());
						pOrgs.add(pOrg);
					}
					pRole.setOrganizations(pOrgs);
					ud.getGroupRoles().add(pRole);
				}
			}

			List<org.cdph.han.model.ContactPreference> contactPreferenceModels = ContactPreferenceLocalServiceUtil.findByUserId(userId);
			ud.setContactPreferences(new ArrayList<org.cdph.han.persistence.ContactPreference>());
			log.debug("Contact Preference Models: "+ contactPreferenceModels);
			for(org.cdph.han.model.ContactPreference pref : contactPreferenceModels) {
				org.cdph.han.persistence.ContactPreference cp = new org.cdph.han.persistence.ContactPreference();
				if (pref.getEmailAddressId() > 0) {
					com.liferay.portal.model.EmailAddress emailAddressModel =
							EmailAddressLocalServiceUtil.getEmailAddress(pref.getEmailAddressId());
					org.cdph.han.persistence.EmailAddress pEmailAddress = ContactDevicesServiceImpl.convertEmailAddress(emailAddressModel);
					cp.setEmailAddress(pEmailAddress);		// email address
				}
				if (pref.getPhoneId() > 0) {
					PhoneDeviceLocalServiceUtil.custByUser(userId);
					org.cdph.han.model.PhoneDevice phoneDevice = PhoneDeviceLocalServiceUtil.getPhoneDevice(pref.getPhoneId());
					cp.setPhoneDevice(ContactDevicesServiceImpl.convertPhoneDevice(phoneDevice));
					log.debug("(add) Phone Contact Preference " + cp.getPhoneDevice().getPhoneNumber());
				}
				cp.setId(pref.getId());
				cp.setIndex(pref.getIndex());
				cp.setPrimaryEmail(pref.getPrimaryEmail());
				log.debug("Add Contact Preference " + cp.getIndex() + ": P email " + cp.getPrimaryEmail() + ", email " + cp.getEmailAddress() + ", Phone " + cp.getPhoneDevice());
				ud.getContactPreferences().add(cp);
			}

			List<org.cdph.han.model.AlternateContact> alternateContacts = AlternateContactLocalServiceUtil.findByUserId(userId);
			List<org.cdph.han.persistence.AlternateContact> altContacts = new ArrayList<org.cdph.han.persistence.AlternateContact>();
			log.debug("Get alternate Contact");
			for(org.cdph.han.model.AlternateContact ac : alternateContacts) {
				org.cdph.han.persistence.AlternateContact oneContact = new org.cdph.han.persistence.AlternateContact();

				User altUser = UserLocalServiceUtil.getUser(ac.getAlternateUserId());
				UserData alternateUd = new UserData();
				alternateUd.setUserId(ac.getAlternateUserId());
				oneContact.setId(ac.getId());
				oneContact.setIndex(ac.getPreference());
				org.cdph.han.persistence.Contact contact = new org.cdph.han.persistence.Contact();
				contact.setFirstName(altUser.getFirstName());
				contact.setLastName(altUser.getLastName());
				contact.setContactId(altUser.getUserId());
//				contact.setEmailAddresses(altUser.getEmailAddress());
				alternateUd.setContact(contact);
				oneContact.setAlternateContact(alternateUd);
				altContacts.add(oneContact);
			}
			ud.setAlternateContacts(altContacts);
		} catch (PortalException e) {
			log.error(e);
		} catch (SystemException e) {
			log.error(e);
		}catch (Exception e) {
			log.error(e);
		}

		return ud;//entityManager.find (UserData.class, userId);
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.UserDataService#getAllUsers()
	 */
	@Override
	//abey ...@Transactional (readOnly = true)
	@SuppressWarnings ("unchecked")
	public List<UserData> getAllUsers () {
		try {
			List<org.cdph.han.model.UserData> userDatas = UserDataLocalServiceUtil.getUserDatas(1, 10000);
			List<UserData> retval = new ArrayList<UserData>();
			for (org.cdph.han.model.UserData ud : userDatas) {
				UserData uData = new UserData();
				uData.setUserId(ud.getUserId());
				uData.setQuotaWarned(ud.getQuotaWarned());
				uData.setMir3Id(ud.getMir3Id());
				uData.setUserId(ud.getUserId());
				retval.add(uData);
			}
			return retval;
		} catch (SystemException e) {
			log.error(e);
		}
		//final Query q = null;//entityManager.createQuery ("select u from UserData u");
		return null;//(List<UserData>) q.getResultList ();
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.UserDataService#toggleWarnedUser(long, boolean)
	 */
	@Override
	//abey ...@Transactional (propagation = Propagation.REQUIRED)
	public void toggleWarnedUser (long userId, boolean warned) {
		/*final Query q = entityManager.createNativeQuery (
				"UPDATE han_user_data SET quota_warned = ? WHERE userid = ?");
		q.setParameter (1, warned);
		q.setParameter (2, userId);
		q.executeUpdate ();*/
	}

}