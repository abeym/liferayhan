package org.cdph.han.persistence;

import java.io.Serializable;

import javax.persistence.Id;

/**
 * Primary Key class for the ContactPreference entity.
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class ContactPreferencePK implements Serializable {
	/** Class serial version UID */
	private static final long serialVersionUID = 369919259475042047L;
	/** The user ID */
	@Id
	private long userId;
	/** The index of the device */
	@Id
	private int index;

	/**
	 * @return the userId
	 */
	public long getUserId () {
		return userId;
	}

	/**
	 * @return the index
	 */
	public int getIndex () {
		return index;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId (final long userId) {
		this.userId = userId;
	}

	/**
	 * @param index the index to set
	 */
	public void setIndex (final int index) {
		this.index = index;
	}
}
