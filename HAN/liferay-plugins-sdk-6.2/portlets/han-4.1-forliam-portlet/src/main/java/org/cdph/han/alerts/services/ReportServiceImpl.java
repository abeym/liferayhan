package org.cdph.han.alerts.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



/*import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
*/import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.alerts.dto.ReportSummaryTO;
import org.cdph.han.alerts.dto.ReportDeviceTO;
import org.cdph.han.alerts.dto.ReportRecipientTO;
import org.cdph.han.alerts.dto.ResponseTO;
import org.cdph.han.persistence.AlertModel;
import org.cdph.han.persistence.AlertOptionModel;
import org.cdph.han.persistence.AlertReport;
import org.cdph.han.persistence.AlertReportDevice;
import org.cdph.han.persistence.AlertReportDeviceModel;
import org.cdph.han.persistence.AlertReportModel;
import org.cdph.han.persistence.AlertReportRecipient;
import org.cdph.han.persistence.AlertReportRecipientModel;
import org.cdph.han.persistence.UserData;
import org.cdph.han.service.AlertOptionLocalServiceUtil;
import org.cdph.han.service.AlertReportLocalServiceUtil;
import org.cdph.han.service.ArchivedAlertLocalServiceUtil;
import org.cdph.han.service.ArchivedAlertOptionLocalServiceUtil;
import org.cdph.han.service.ArchivedAlertReportLocalServiceUtil;
import org.cdph.han.util.services.HanPropertiesService;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.mir3.ws.AuthorizationType;
import com.mir3.ws.ErrorType;
import com.mir3.ws.GetNotificationReportRecipientsType;
import com.mir3.ws.GetNotificationReportsType;
import com.mir3.ws.Mir3;
import com.mir3.ws.Mir3Service;
import com.mir3.ws.NotificationReportType;
import com.mir3.ws.ReportContactType;
import com.mir3.ws.ReportQueryType;
import com.mir3.ws.ReportRecipientType;
import com.mir3.ws.ReportRecipientsQueryType;
import com.mir3.ws.ReportRecipientsResponseType;
import com.mir3.ws.ReportResponseOptionType;
import com.mir3.ws.ResponseType;

/**
 * Implementation of the ReportService interface, the implementation uses the mir3 webservices to
 * retrieve the information, once the notification is finished at mir3 the service stores the response
 * in the local db
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
//@Repository
//@Service ("reportService")
public class ReportServiceImpl implements ReportService {
	/** Class log */
	private static final Log log = LogFactory.getLog (ReportServiceImpl.class);
	/** MIR3 API Version key */
	private static final String API_VERSION_KEY = "MIR3_API_VERSION";
	/** Service for retrieving HAN DB properties */
	//abey...
	//@Autowired
	private HanPropertiesService hanPropertiesService;
	/** Persistance context */
//	private EntityManager entityManager;
	/** Mir3 webservice endpoint */
	private Mir3 service;
	/** Map used to match responses between mir3 and the local DB */
	private Map<Integer, ResponseTO> responses;

	/**
	 * This default constructor prepares the mir3 service to be used
	 */
	public ReportServiceImpl () {
		Mir3Service mir3Service = new Mir3Service ();
		service = mir3Service.getMir3 ();
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.ReportService#findReport(long)
	 */
	//abey ...@Transactional (propagation = Propagation.REQUIRED)
	public ReportSummaryTO findReport (final long alertId, final boolean archived) {
		return findReport (alertId, archived, false);
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.ReportService#findReport(long)
	 */
	//abey ...@Transactional (propagation = Propagation.REQUIRED)
	public ReportSummaryTO findReport (final long alertId, final boolean archived, final boolean fromWeb) {
		ReportSummaryTO report = null;
		final List<ReportSummaryTO> reports = new ArrayList<ReportSummaryTO> ();
		try {
			org.cdph.han.model.ArchivedAlert alertArchiveModel = ArchivedAlertLocalServiceUtil.getArchivedAlert(alertId);
			report = new ReportSummaryTO ();
			if (archived) {
				List<org.cdph.han.model.ArchivedAlertReport> aars = ArchivedAlertReportLocalServiceUtil.getAlertReportsByAlert(alertId);
				List<org.cdph.han.model.ArchivedAlertOption> aaos = ArchivedAlertOptionLocalServiceUtil.getAlertOptionsByAlert(alertId);
				for (org.cdph.han.model.ArchivedAlertReportModel alertReport : aars) {
					if (!alertReport.isNotificationFinished () && !archived && !fromWeb) {
						report = retrieveMir3Report ((int)alertReport.getNotificationReportId (),
								convertArchivedAlertOption(aaos), ((AlertReport) alertReport).getAlert ().getPublisher (),
								alertReport.getLastFetch ());
						if (report != null) {
							updateDBReport ((AlertReport) alertReport, report);
						}
					} else {
					}
				}
			} else {
				List<org.cdph.han.model.AlertReport> ars = AlertReportLocalServiceUtil.getAlertReportsByAlert(alertId);
				List<org.cdph.han.model.AlertOption> aos = AlertOptionLocalServiceUtil.getAlertOptionsByAlert(alertId);
				for (org.cdph.han.model.AlertReportModel alertReport : ars) {
					if (!alertReport.isNotificationFinished () && !archived && !fromWeb) {
						report = retrieveMir3Report (alertReport.getNotificationReportId (),
								convertAlertOption(aos), ((AlertReport) alertReport).getAlert ().getPublisher (),
								alertReport.getLastFetch ());
						if (report != null) {
							updateDBReport ((AlertReport) alertReport, report);
							AlertReportLocalServiceUtil.updateAlertReport((org.cdph.han.model.AlertReport)alertReport);
							reports.add (report);
						}
					} else {
					}
				}
			}
		} catch (PortalException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		/*final AlertModel alert = archived ? entityManager.find (ArchivedAlert.class, alertId)
				: entityManager.find (Alert.class, alertId);
		if (alert.getAlertReports () != null && !alert.getAlertReports ().isEmpty ()) {
			report = new ReportSummaryTO ();
			for (AlertReportModel alertReport : alert.getAlertReports ()) {
				// If the alert is not finished, check mir3 site for updated information
				if (!alertReport.isNotificationFinished () && !archived && !fromWeb) {
					report = retrieveMir3Report (alertReport.getNotificationReportId (),
							alert.getOptions (), ((AlertReport) alertReport).getAlert ().getPublisher (),
							alertReport.getLastFetch ());
					if (report != null) {
						// If the report is available update the db report
						updateDBReport ((AlertReport) alertReport, report);
						entityManager.merge ((AlertReport) alertReport);
						reports.add (report);
					}
				} else {
					// If the alert is finished retrieve it from the db
					report = retrieveReport (alertReport);
					report.setResponseOptions (loadResponseOptions (alert.getOptions ()));
					addResponse (report.getResponseOptions (), report.getRecipients ());
					reports.add (report);
				}
			}
			// Consolidate the report
			report = aggregateReports (reports, alert);
		}*/
		return report;
	}

	private List<org.cdph.han.persistence.AlertOption> convertAlertOption(List<org.cdph.han.model.AlertOption> aos) {
		if (aos == null) return null;
		List<org.cdph.han.persistence.AlertOption> retVal= new ArrayList<org.cdph.han.persistence.AlertOption>();
		for (org.cdph.han.model.AlertOption ao : aos) {
			org.cdph.han.persistence.AlertOption persistAlertOption = new org.cdph.han.persistence.AlertOption();
			persistAlertOption.setId(ao.getId());
			persistAlertOption.setCallBridge(ao.getCallBridge());
			persistAlertOption.setAction(ao.getAction());
			persistAlertOption.setCascade(ao.getCascade());
			persistAlertOption.setMessage(ao.getMessage());
			persistAlertOption.setOptionId(ao.getOptionId());
			retVal.add(persistAlertOption);
		}
		return retVal;
	}
	private List<org.cdph.han.persistence.AlertOption> convertArchivedAlertOption(List<org.cdph.han.model.ArchivedAlertOption> aaos) {
		if (aaos == null) return null;
		List<org.cdph.han.persistence.AlertOption> retVal= new ArrayList<org.cdph.han.persistence.AlertOption>();
		for (org.cdph.han.model.ArchivedAlertOption ao : aaos) {
			org.cdph.han.persistence.AlertOption persistAlertOption = new org.cdph.han.persistence.AlertOption();
			persistAlertOption.setId(ao.getId());
			persistAlertOption.setCallBridge(ao.getCallBridge());
			persistAlertOption.setAction(ao.getAction());
			persistAlertOption.setCascade(ao.getCascade());
			persistAlertOption.setMessage(ao.getMessage());
			persistAlertOption.setOptionId(Integer.parseInt(ao.getOptionId()));
			retVal.add(persistAlertOption);
		}
		return retVal;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.ReportService#updateReport(long, int)
	 */
	//abey ...@Transactional (propagation = Propagation.REQUIRED)
	public boolean updateReport (long alertId, int notificationReportId) {
		/*final Query q = entityManager.createNamedQuery ("AlertReport.FindByAlertReport");
		q.setParameter ("alertId", alertId);
		q.setParameter ("notificationReportId", notificationReportId);
		final AlertReport report = (AlertReport) q.getSingleResult ();
		if (!report.isNotificationFinished ()) {
			ReportSummaryTO reportSummary =
				retrieveMir3Report (notificationReportId, report.getAlert ().getOptions (),
						report.getAlert ().getPublisher (), report.getLastFetch ());
			updateDBReport (report, reportSummary);
			entityManager.merge (report);
			entityManager.flush ();
		}
		return report.isNotificationFinished ();*/
		return false;
	}

	/**
	 * This method aggregates all the reports optained from mir3 or the local db
	 * @param reports List of reports to aggregate
	 * @param alert
	 * @return
	 */
	private ReportSummaryTO aggregateReports (final List<ReportSummaryTO> reports, final AlertModel alert) {
		final ReportSummaryTO report = new ReportSummaryTO ();
		// Agregate all reports
		report.setRecipients (new ArrayList<ReportRecipientTO> ());
		report.setInitiatedBy (alert.getPublisher ().getContact ().getFirstName () + " "
				+ alert.getPublisher ().getContact ().getLastName ());
		report.setExpeditedDelivery ("Normal");
		report.setType ("Broadcast"); // Currently one step notification only supports broadcast
		report.setReportId (alert.getId ());
		report.setIssued (alert.getDatePublished ());
		report.setMessage (alert.getMessages ().iterator ().next ().getTelephonyMessage ());
		report.setName (alert.getName ());
		//report.setResponseOptions (loadResponseOptions (alert.getOptions ()));
		for (ReportSummaryTO currentReport : reports) {
			addResponseOptions (report, currentReport);
			report.setAnsweringMachine (currentReport.getAnsweringMachine () + report.getAnsweringMachine ());
			report.setBusy (currentReport.getBusy () + report.getBusy ());
			report.setCompleted (currentReport.getCompleted ());
			report.setHangUp (currentReport.getHangUp () + report.getHangUp ());
			report.setInvalidResponse (currentReport.getInvalidResponse () + report.getInvalidResponse ());
			report.setLeftMessages (currentReport.getLeftMessages () + report.getLeftMessages ());
			report.setNoAnswer (currentReport.getNoAnswer () + report.getNoAnswer ());
			report.setNotAtThisLocation (currentReport.getNotAtThisLocation () + report.getNotAtThisLocation ());
			report.setOther (currentReport.getOther () + report.getOther ());
			report.setPhoneTime (currentReport.getPhoneTime () + report.getPhoneTime ());
			report.getRecipients ().addAll (currentReport.getRecipients ());
			report.setSms (currentReport.getSms () + report.getSms ());
			report.setTotalCalls (currentReport.getTotalCalls () + report.getTotalCalls ());
			report.setTotalContacted (currentReport.getTotalContacted () + report.getTotalContacted ());
			report.setTotalEmails (currentReport.getTotalEmails () + report.getTotalEmails ());
			report.setTotalFaxes (currentReport.getTotalFaxes () + report.getTotalFaxes ());
			report.setTotalPages (currentReport.getTotalPages () + report.getTotalPages ());
			report.setTotalRecipients (currentReport.getTotalRecipients () + report.getTotalRecipients ());
			report.setTotalResponded (currentReport.getTotalResponded () + report.getTotalResponded ());
			report.setWrongAddressTel (currentReport.getWrongAddressTel () + report.getWrongAddressTel ());
			//addResponse (report.getResponseOptions (), currentReport.getRecipients ());
		}
		return report;
	}

	/**
	 * This method merges the two list of response options
	 * @param destReport source report
	 * @param srcReport target report
	 */
	private void addResponseOptions (ReportSummaryTO destReport,
			ReportSummaryTO srcReport) {
		if (destReport.getResponseOptions () == null) {
			destReport.setResponseOptions (srcReport.getResponseOptions ());
		} else {
			for (ResponseTO response : destReport.getResponseOptions ()) {
				for (ResponseTO srcResponse : srcReport.getResponseOptions ()) {
					if (response.getMir3Id () == srcResponse.getMir3Id ()) {
						response.setTotal (response.getTotal () + srcResponse.getTotal ());
						break;
					}
				}
			}
		}
	}

	/**
	 * Add the response to the list of responses (if any)
	 * @param responseOptions List of available responses
	 * @param recipients List of recipients
	 */
	private void addResponse (final List<ResponseTO> responseOptions,
			final List<ReportRecipientTO> recipients) {
		for (ReportRecipientTO recipient : recipients) {
			for (ReportDeviceTO device : recipient.getDevices ()) {
				if (device.getResponse () != null) {
					responseOptions.get (device.getResponse () - 1).setTotal (
							responseOptions.get (device.getResponse () - 1).getTotal () + 1);
					break;
				}
			}
		}
	}

	/**
	 * This method reads all available responses from the DB
	 * @param options list of options in the database
	 * @return list of responses
	 */
	private List<ResponseTO> loadResponseOptions (final Collection<? extends AlertOptionModel> options) {
		final List<ResponseTO> responses = new ArrayList<ResponseTO> ();
		ResponseTO response;
		int index = 1;
		for (AlertOptionModel option : options) {
			response = new ResponseTO ();
			response.setIndex (index++);
			response.setResponse (option.getMessage ());
			response.setTotal (0);
			response.setCallbridge (option.getCallBridge ());
			responses.add (response);
		}
		return responses;
	}

	/**
	 * This method retrieves a report from the HAN DB
	 * @param alertReport AlertReportModel with the report in the DB
	 * @return ReportSummaryTO with the report
	 */
	private ReportSummaryTO retrieveReport (final AlertReportModel alertReport) {
		final ReportSummaryTO report = new ReportSummaryTO ();
		report.setExpeditedDelivery ("Normal");
		report.setAnsweringMachine (alertReport.getAnsweringMachine () != null ?
				alertReport.getAnsweringMachine () : 0);
		report.setBusy (alertReport.getBusy () != null ? alertReport.getBusy () : 0);
		report.setCompleted (alertReport.getCompleted ());
		report.setHangUp (alertReport.getHangUp () != null ? alertReport.getHangUp () : 0);
		report.setInvalidResponse (alertReport.getInvalidResponse () != null ? alertReport.getInvalidResponse () : 0);
		report.setLeftMessages (alertReport.getLeftMessage () != null ? alertReport.getLeftMessage () : 0);
		report.setNoAnswer (alertReport.getNoAnswer () != null ? alertReport.getNoAnswer () : 0);
		report.setNotAtThisLocation (alertReport.getNotAtThisLocation () != null ?
				alertReport.getNotAtThisLocation () : 0);
		report.setOther (alertReport.getOther () != null ? alertReport.getOther () : 0);
		report.setPhoneTime (alertReport.getPhoneTime () != null ? alertReport.getPhoneTime () : 0);
		report.setRecipients (loadRecipients (alertReport.getRecipients ()));
		report.setReportId (alertReport.getNotificationReportId ());
		report.setSms (alertReport.getTotalSms () != null ? alertReport.getTotalSms () : 0);
		report.setTotalCalls (alertReport.getTotalCalls () != null ? alertReport.getTotalCalls () : 0);
		report.setTotalContacted (alertReport.getTotalContacted () != null ? alertReport.getTotalContacted () : 0);
		report.setTotalEmails (alertReport.getTotalEmails () != null ? alertReport.getTotalEmails () : 0);
		report.setTotalFaxes (alertReport.getTotalFaxes () != null ? alertReport.getTotalFaxes () : 0);
		report.setTotalPages (alertReport.getTotalPages () != null ? alertReport.getTotalPages () : 0);
		report.setTotalRecipients (alertReport.getTotalRecipients () != null ? alertReport.getTotalRecipients () : 0);
		report.setTotalResponded (alertReport.getTotalResponded () != null ? alertReport.getTotalResponded () : 0);
		report.setWrongAddressTel (alertReport.getWrongAddressTel () != null ? alertReport.getWrongAddressTel () :0);
		return report;
	}

	/**
	 * This method creates a new list of ReportRecipientTO and loads all the available recipients in the DB
	 * @param recipients Collection of recipients in DB
	 * @return list with the found recipients, empty if no recipients
	 */
	private List<ReportRecipientTO> loadRecipients (
			final Collection<? extends AlertReportRecipientModel> recipients) {
		final List<ReportRecipientTO> recipientsList = new ArrayList<ReportRecipientTO> ();
		ReportRecipientTO loadedRecipient;
		for (AlertReportRecipientModel recipient : recipients) {
			loadedRecipient = new ReportRecipientTO ();
			loadedRecipient.setDevices (loadDevices (recipient.getDevices ()));
			loadedRecipient.setFirstName (recipient.getFirstName ());
			loadedRecipient.setLastName (recipient.getLastName ());
			loadedRecipient.setRecipientReportId (recipient.getRecipientReportId ());
			recipientsList.add (loadedRecipient);
		}
		return recipientsList;
	}

	/**
	 * This method creates a new list of devices 
	 * @param devices Collection of devices to load into DTOs
	 * @return List of devices found
	 */
	private List<ReportDeviceTO> loadDevices (final Collection<? extends AlertReportDeviceModel> devices) {
		final List<ReportDeviceTO> loadedDevices = new ArrayList<ReportDeviceTO> ();
		ReportDeviceTO loadedDevice;
		for (AlertReportDeviceModel device : devices) {
			loadedDevice = new ReportDeviceTO ();
			loadedDevice.setAddress (device.getAddress ());
			loadedDevice.setContactId (device.getContactId ());
			loadedDevice.setContactResult (device.getResult ());
			loadedDevice.setDescription (device.getDescription ());
			loadedDevice.setDuration (device.getDuration ());
			loadedDevice.setTimeResponded (device.getTimeResponded ());
			loadedDevice.setTimeSent (device.getTimeSent ());
			loadedDevice.setResponse (device.getResponse ());
			loadedDevices.add (loadedDevice);
		}
		return loadedDevices;
	}

	/**
	 * This method merges the information provided by the ReportSummaryTO object into the AlertReport object
	 * @param alertReport destination of the information
	 * @param summary source of the information
	 */
	private void updateDBReport (final AlertReport alertReport, final ReportSummaryTO summary) {
		alertReport.setAnsweringMachine (summary.getAnsweringMachine ());
		alertReport.setBusy (summary.getBusy ());
		alertReport.setCompleted (summary.getCompleted ());
		alertReport.setHangUp (summary.getHangUp ());
		alertReport.setInvalidResponse (summary.getInvalidResponse ());
		alertReport.setLeftMessage (summary.getLeftMessages ());
		alertReport.setNoAnswer (summary.getNoAnswer ());
		alertReport.setNotAtThisLocation (summary.getNotAtThisLocation ());
		alertReport.setNotificationFinished (summary.getCompleted () != null);
		alertReport.setOther (summary.getOther ());
		alertReport.setPhoneTime (summary.getPhoneTime ()); // Do not aggregate result
		alertReport.setTotalCalls (summary.getTotalCalls ());
		alertReport.setTotalContacted (summary.getTotalContacted ()); // Do not aggregate result
		alertReport.setTotalEmails (summary.getTotalEmails ());
		alertReport.setTotalFaxes (summary.getTotalFaxes ());
		alertReport.setTotalPages (summary.getTotalPages ());
		//alertReport.setTotalRecipients (summary.getTotalRecipients () == 0 ?
		//		alertReport.getAlert ().getRecipients ().size () : summary.getTotalRecipients ());
		alertReport.setTotalRecipients (summary.getTotalRecipients ()); // Do not aggregate result
		alertReport.setTotalResponded (summary.getTotalResponded ()); // Do not aggregate result
		alertReport.setTotalSms (summary.getSms ());
		alertReport.setWrongAddressTel (summary.getWrongAddressTel ());
		alertReport.setLastFetch (summary.getLastFetch ());
		if (alertReport.getRecipients () == null) {
			alertReport.setRecipients (new ArrayList<AlertReportRecipient> ());
		}
		updateDBRecipients (alertReport, summary.getRecipients ());
	}

	/**
	 * This method merges the recipients provided by mir3 with the recipients currently stored in the db
	 * @param alertReport at local db
	 * @param reportRecipients provided by mir3
	 */
	private void updateDBRecipients (final AlertReport alertReport,
			final List<ReportRecipientTO> reportRecipients) {
		final Collection<AlertReportRecipient> recipients = alertReport.getRecipients ();
		AlertReportRecipient dbRecipient;
		for (ReportRecipientTO recipient : reportRecipients) {
			dbRecipient = getRecipient (recipients, recipient.getRecipientReportId ());
			if (dbRecipient == null) {
				dbRecipient = new AlertReportRecipient ();
				dbRecipient.setReport (alertReport);
				recipients.add (dbRecipient);
			}
			dbRecipient.setFirstName (recipient.getFirstName ());
			dbRecipient.setLastName (recipient.getLastName ());
			dbRecipient.setRecipientReportId (recipient.getRecipientReportId ());
			dbRecipient.setMir3Id (recipient.getMir3Id ());
			if (dbRecipient.getDevices () == null) {
				dbRecipient.setDevices (new ArrayList<AlertReportDevice> ());
			}
			updateDBDevices (dbRecipient, recipient.getDevices ());
		}
	}

	/**
	 * This method searches for the given recipientReportId in the available recipients
	 * @param recipients available in the db
	 * @param recipientReportId id of the recipient to search
	 * @return found recipient
	 */
	private AlertReportRecipient getRecipient (final Collection<AlertReportRecipient> recipients,
			final int recipientReportId) {
		AlertReportRecipient foundRecipient = null;
		for (AlertReportRecipient currentRecipient : recipients) {
			if (currentRecipient.getRecipientReportId () == recipientReportId) {
				foundRecipient = currentRecipient;
				break;
			}
		}
		return foundRecipient;
	}

	/**
	 * This method updates the provided mir3 devices statistics with the database statistics
	 * @param recipint to wich the devices belong
	 * @param reportDevices list of mir3 devices
	 */
	private void updateDBDevices (final AlertReportRecipient recipient,
			final List<ReportDeviceTO> reportDevices) {
		final Collection<AlertReportDevice> devices = recipient.getDevices ();
		AlertReportDevice dbDevice;
		for (ReportDeviceTO device : reportDevices) {
			dbDevice = getDevice (devices, device.getContactId ());
			if (dbDevice == null) {
				dbDevice = new AlertReportDevice ();
				dbDevice.setRecipient (recipient);
				devices.add (dbDevice);
			}
			dbDevice.setAddress (device.getAddress ());
			dbDevice.setContactId (device.getContactId ());
			dbDevice.setDescription (device.getDescription ());
			dbDevice.setDuration (device.getDuration ());
			dbDevice.setResult (device.getContactResult ());
			dbDevice.setTimeResponded (device.getTimeResponded ());
			dbDevice.setTimeSent (device.getTimeSent ());
			if (device.getResponse () != null) {
				dbDevice.setResponse (responses.get (device.getResponse ()).getIndex ());
				device.setResponse (dbDevice.getResponse ());
			}
		}
	}

	/**
	 * This method searches for a device containing the given address 
	 * @param devices list of available devices in the DB.
	 * @param contactId id of the device to search (mir3 id)
	 * @return the found device or null if none exists
	 */
	private AlertReportDevice getDevice (final Collection<AlertReportDevice> devices,
			final int contactId) {
		AlertReportDevice foundDevice = null;
		for (AlertReportDevice currentDevice : devices) {
			if (currentDevice.getContactId () == contactId) {
				foundDevice = currentDevice;
				break;
			}
		}
		return foundDevice;
	}

	/**
	 * This method uses mir3 services to retrieve the information from their site
	 * @param reportSummary ReportSummaryTO object to add the information
	 * @param notificationReportId the id of the notification report provided by mir3
	 * @param credentials the user information to use as credentials
	 * @return the summary of the report
	 */
	private ReportSummaryTO retrieveMir3Report (final int notificationReportId,
			final Collection<? extends AlertOptionModel> options, final UserData credentials,
			final Date lastFetch) {
		ReportSummaryTO reportSummary = null;
		// Retrieve current user information to create the authorization object
		final AuthorizationType authorization = new AuthorizationType ();
		authorization.setPassword (credentials.getTelephonyPassword ());
		authorization.setUsername (credentials.getEmail ());
		final GetNotificationReportsType reportRequest = new GetNotificationReportsType ();
		// Query the notification report
		reportRequest.setApiVersion (hanPropertiesService.getValue (API_VERSION_KEY));
		reportRequest.setAuthorization (authorization);
		reportRequest.getNotificationReportId ().add (notificationReportId);
		// Execute query
		ResponseType response = service.getNotificationReportsOp (reportRequest);
		if (response.getError () == null || response.getError ().isEmpty ()) {
			// The response should provide only one response
			final NotificationReportType report =
				response.getGetNotificationReportsResponse ().getNotificationReport ().get (0);
			reportSummary = new ReportSummaryTO ();
			reportSummary.setCompleted (report.getTimeClosed () == null ? null :
				report.getTimeClosed ().toGregorianCalendar ().getTime ());
			reportSummary.setPhoneTime (report.getGeneralStatistics ().getTotalPhoneSeconds ());
			reportSummary.setTotalContacted (report.getGeneralStatistics ().getTotalContactedCount ());
			reportSummary.setTotalResponded (report.getGeneralStatistics ().getTotalResponseCount ());
			reportSummary.setTotalRecipients (report.getGeneralStatistics ().getTotalRecipientCount ());
			reportSummary.setResponseOptions (loadResponseOptions (options));
			ResponseTO responseTO;
			responses = new HashMap<Integer, ResponseTO> ();
			for (ReportResponseOptionType responseOpt : report.getResponseOptions ().getResponseOption ()) {
				responseTO = getResponse (reportSummary.getResponseOptions (),
						responseOpt.getVerbiage ().getText ().get (0).getValue ());
				responseTO.setMir3Id (responseOpt.getResponseOptionReportId ());
				responseTO.setTotal (responseOpt.getResponseCount ());
				responseTO.setCallbridge (responseOpt.getCallBridge ());
				responses.put (responseTO.getMir3Id (), responseTO);
			}
			// The number of total pages
			final int totalPages = reportSummary.getTotalRecipients () / 100 +
					((reportSummary.getTotalRecipients () % 100) == 0 ? 0 : 1);
			// Query the notification recipients report
			final GetNotificationReportRecipientsType request = new GetNotificationReportRecipientsType ();
			request.setAuthorization (authorization);
			// Set the API Version
			request.setApiVersion (hanPropertiesService.getValue (API_VERSION_KEY));
			// Include devices for the report
			request.setIncludeDeviceContacts (true);
			// Set the notification report id
			request.setNotificationReportId (notificationReportId);
			// Create the query
			final ReportRecipientsQueryType query = new ReportRecipientsQueryType ();
			query.setQueryType (ReportQueryType.ALL_RECIPIENTS);
			request.setQuery (query);
			if (lastFetch != null) {
				GregorianCalendar gCal = (GregorianCalendar) GregorianCalendar.getInstance ();
				gCal.setTime (lastFetch);
				XMLGregorianCalendar cal;
				try {
					cal = DatatypeFactory.newInstance ().newXMLGregorianCalendar (gCal);
					query.setChangedAfter (cal);
				} catch (DatatypeConfigurationException ex) {
					log.error ("Requesting entire report. Couldn't create calendar.", ex);
				}
			}
			reportSummary.setRecipients (new ArrayList<ReportRecipientTO> ());
			for (int i = 0; i < totalPages; i++) {
				request.setMaxResults (100);
				request.setStartIndex ((100 * i) + 1);
				response = service.getNotificationReportRecipientsOp (request);
				if (response.getError () == null || response.getError ().isEmpty ()) {
					final ReportRecipientsResponseType reportResponse =
						response.getNotificationReportRecipientsResponse ();
					reportSummary.setLastFetch (reportResponse.getAsOf ().toGregorianCalendar ().getTime ());
					reportSummary.setAnsweringMachine (
							(reportResponse.getStatistics ().getAnsweringMachineConnectedCount () == null
							? 0 : reportResponse.getStatistics ().getAnsweringMachineConnectedCount ())
							+ (reportResponse.getStatistics ().getAnsweringMachineDisconnectedCount () == null ?
							0 : reportResponse.getStatistics ().getAnsweringMachineDisconnectedCount ())
							+ (reportResponse.getStatistics ().getAnsweringMachineUndeliveredCount () == null ?
							0 : reportResponse.getStatistics ().getAnsweringMachineUndeliveredCount ()));
					//reportSummary.setBlackBerry (reportResponse.getStatistics ());
					reportSummary.setBusy (reportResponse.getStatistics ().getBusyCount () == null ? 0
							: reportResponse.getStatistics ().getBusyCount ());
					reportSummary.setHangUp (reportResponse.getStatistics ().getHangUpCompleteCount () == null ? 0
							: reportResponse.getStatistics ().getHangUpCompleteCount ());
					reportSummary.setInvalidResponse (
							reportResponse.getStatistics ().getInvalidResponseCount () == null
							? 0 : reportResponse.getStatistics ().getInvalidResponseCount ());
					reportSummary.setLeftMessages (reportResponse.getStatistics ().getLeftMessageCount () == null
							? 0 : reportResponse.getStatistics ().getLeftMessageCount ());
					reportSummary.setNoAnswer (reportResponse.getStatistics ().getNoAnswerCount () == null
							? 0 : reportResponse.getStatistics ().getNoAnswerCount ());
					reportSummary.setNotAtThisLocation (
							reportResponse.getStatistics ().getNotAtThisLocationCount () == null
							? 0 : reportResponse.getStatistics ().getNotAtThisLocationCount ());
					reportSummary.setOther (reportResponse.getStatistics ().getOtherCount () == null
							? 0 : reportResponse.getStatistics ().getOtherCount ());
					reportSummary.setReportId (notificationReportId);
					reportSummary.setSms (reportResponse.getStatistics ().getTotalSms () == null
							? 0 : reportResponse.getStatistics ().getTotalSms ());
					reportSummary.setTotalCalls (reportResponse.getStatistics ().getTotalCalls () == null
							? 0 : reportResponse.getStatistics ().getTotalCalls ());
					reportSummary.setTotalEmails (reportResponse.getStatistics ().getTotalEmails () == null
							? 0 : reportResponse.getStatistics ().getTotalEmails ());
					reportSummary.setTotalFaxes (reportResponse.getStatistics ().getTotalFaxes () == null
							? 0 : reportResponse.getStatistics ().getTotalFaxes ());
					reportSummary.setTotalPages (reportResponse.getStatistics ().getTotalPages () == null
							? 0 : reportResponse.getStatistics ().getTotalPages ());
					//reportSummary.setTotalRecipients (reportResponse.getStatistics ().getTotalRecipients () == null
					//		? 0 : reportResponse.getStatistics ().getTotalRecipients ());
					// For now all notifications are of broadcast type
					reportSummary.setType ("Broadcast");
					ReportDeviceTO recipientDevice;
					ReportRecipientTO reportRecipient;
					for (ReportRecipientType recipient : reportResponse.getRecipients ().getRecipient ()) {
						reportRecipient = new ReportRecipientTO ();
						reportRecipient.setFirstName (recipient.getFirstName ());
						reportRecipient.setLastName (recipient.getLastName ());
						reportRecipient.setRecipientReportId (recipient.getRecipientReportId ());
						reportRecipient.setDevices (new ArrayList<ReportDeviceTO> ());
						reportRecipient.setMir3Id (recipient.getUserId ());
						reportSummary.getRecipients ().add (reportRecipient);
						if (recipient.getDeviceContacts () != null
								&& recipient.getDeviceContacts ().getContact () != null) {
							for (ReportContactType device : recipient.getDeviceContacts ().getContact ()) {
								recipientDevice = new ReportDeviceTO ();
								recipientDevice.setAddress (device.getAddress ());
								recipientDevice.setContactResult (device.getContactResult ().value ());
								recipientDevice.setTimeSent (device.getTimeSent ().toGregorianCalendar ().getTime ());
								recipientDevice.setTimeResponded (device.getTimeResponded () == null ? null
										: device.getTimeResponded ().toGregorianCalendar ().getTime ());
								recipientDevice.setDuration (device.getDuration () == null ? 0 : device.getDuration ());
								recipientDevice.setDescription (device.getDescription ());
								recipientDevice.setContactId (device.getContactId ());
								if (!device.getResponse ().isEmpty ()) {
									recipientDevice.setResponse (
											device.getResponse ().get (0).getResponseOptionReportId ());
								}
								reportRecipient.getDevices ().add (recipientDevice);
							}
						} else {
							recipientDevice = new ReportDeviceTO ();
							recipientDevice.setAddress ("");
							recipientDevice.setContactResult ("NOT_CONTACTED");
							recipientDevice.setTimeSent (null);
							recipientDevice.setTimeResponded (null);
							recipientDevice.setDuration (0);
							recipientDevice.setDescription ("");
							recipientDevice.setContactId (0);
							reportRecipient.getDevices ().add (recipientDevice);
						}
					}
				} else {
					for (ErrorType error : response.getError ()) {
						log.error ("Error found: " + error.getErrorMessage ()
								+ " Error code: " + error.getErrorCode ());
					}
				}
			}
		} else {
			for (ErrorType error : response.getError ()) {
				log.error ("Found error: " + error.getErrorMessage ()
						+ " Error code: " + error.getErrorCode ());
			}
		}
		return reportSummary;
	}

	/**
	 * Returns the response option with the given text
	 * @param responseOptions List of available response options
	 * @param value text to search in the available options
	 * @return the corresponding AsynchResponseTO
	 */
	private ResponseTO getResponse (final List<ResponseTO> responseOptions,
			final String value) {
		ResponseTO foundTO = null;
		for (ResponseTO current : responseOptions) {
			if (current.getResponse ().trim ().equals (value.trim ())) {
				foundTO = current;
				break;
			}
		}
		return foundTO;
	}

	/**
	 * @param entityManager the entityManager to set
	 */
	//abey...
	//@PersistenceContext
	/*public void setEntityManager (final EntityManager entityManager) {
		this.entityManager = entityManager;
	}*/

}
