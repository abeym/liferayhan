package org.cdph.han.alerts.services;

import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;
//import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

//import org.cdph.han.persistence.AlertLocale;
import org.cdph.han.persistence.AlertTopic;
import org.cdph.han.util.dto.ListTypeTO;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of the topic service, the implementation uses JPA to manipulate the data in the HAN DB.
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
//@Repository
//@Service ("topicsService")
public class TopicsServiceImpl implements TopicsService {
	/** Entity manager */
	//private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.TopicsService#insertNewTopic(long, java.lang.String)
	 */
	//abey ...@Transactional
	public long insertNewTopic (final int localeId, final String topic) {
		final AlertTopic alertTopic = new AlertTopic ();
		alertTopic.setTitle (topic);
		//alertTopic.setLocale (entityManager.find (AlertLocale.class, localeId));
	//	entityManager.persist (alertTopic);
		return alertTopic.getId ();
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.TopicsService#listAvailableTopics(long)
	 */
	//abey ...@Transactional (readOnly = true)
	@SuppressWarnings ("unchecked")
	public List<AlertTopic> listAvailableTopics (final int localeId) {
		/*final Query q = entityManager.createNamedQuery ("AlertTopic.findByLocaleId");
		q.setParameter ("id", localeId);*/
		//return (List<AlertTopic>) q.getResultList ();
		return new ArrayList<AlertTopic> ();
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.TopicsService#checkAvailableTopic(long, java.lang.String)
	 */
	public boolean checkAvailableTopic (final int localeId, final String topic) {
		boolean available = false;
		/*final Query q = entityManager.createNamedQuery ("AlertTopic.findByTL");
		q.setParameter ("id", localeId);
		q.setParameter ("title", topic.toUpperCase ());
		final List<?> result = q.getResultList ();
		available = result == null || result.isEmpty ();*/
		return available;
	}

	/**
	 * @param entityManager the entityManager to set
	 */
	//abey...
	//@PersistenceContext
//	public void setEntityManager (final EntityManager entityManager) {
		//this.entityManager = entityManager;
//	}

}
