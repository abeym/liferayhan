package org.cdph.han.custgroups.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.custgroups.dto.FilterCriteriaTO;
import org.cdph.han.custgroups.dto.RecipientTO;
import org.cdph.han.custgroups.dto.RecipientType;
import org.cdph.han.persistence.AreaField;
import org.cdph.han.persistence.CustomGroupCriteria;
import org.cdph.han.persistence.CustomGroupMember;
import org.cdph.han.persistence.NonHanUser;
import org.cdph.han.persistence.NonHanUserGroup;
import org.cdph.han.persistence.Organization;
import org.cdph.han.persistence.Role;
import org.cdph.han.persistence.UserCustomGroup;
import org.cdph.han.persistence.UserData;
import org.cdph.han.service.UserDataLocalServiceUtil;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * Implementation of the GroupManagementService that uses JPA
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
//@Repository
//@Service ("groupManagementService")
public class GroupManagementServiceImpl implements GroupManagementService {
	private static final Log log = LogFactory.getLog (GroupManagementServiceImpl.class);
	/** Persistence context */
	private EntityManager entityManager;

	public UserData lrToHanUserData(org.cdph.han.model.UserData lud){
		UserData ud = new UserData();
		//TODO: need to get this active flag
		ud.setActive(true);
		ud.setDepartment(lud.getDepartment());
		ud.setUserId(lud.getUserId());
		return ud;
	}
	
	/* (non-Javadoc)
	 * @see org.cdph.han.custgroups.services.GroupManagementService#saveGroup(java.lang.String, java.util.List)
	 */
	@Override
	//abey ...@Transactional (propagation = Propagation.REQUIRED)
	public long saveGroup (final long userId, final String name, final List<RecipientTO> recipients) {
		//final UserData user = entityManager.find (UserData.class, userId);
		org.cdph.han.model.UserData lud =null;;
		try {
			lud = org.cdph.han.service.UserDataLocalServiceUtil.getUserData(userId);
		} catch (PortalException e) {
			// 
			log.error(e);
		} catch (SystemException e) {
			log.error(e);
		}
		
		//for(org.cdph.han.model.UserData hud:luds){
			
		//}
	/*	UserCustomGroup group = new UserCustomGroup ();
		group.setCreatedDate (new Date ());
		group.setName (name);
		group.setUserData (user);
		*/
		UserCustomGroup group = new UserCustomGroup ();
		group.setCreatedDate (new Date ());
		group.setName (name);
		UserData ud = new UserData();
		ud = lrToHanUserData(lud);
		group.setUserData (ud);
		OrganizationSearchLayerService osls = new OrganizationSearchLayerService();
		osls.addGroup(group);
		//entityManager.persist (group);
		CustomGroupMember member;
		CustomGroupCriteria dbCriteria;
		for (RecipientTO recipient : recipients) {
			member = new CustomGroupMember();
			if (RecipientType.AREA_FIELD.equals (recipient.getType ())) {
				member.setAreaField (entityManager.find (AreaField.class, (int) recipient.getId ()));
			} else if (RecipientType.GROUP.equals (recipient.getType ())) {
				member.setChildGroup (entityManager.find (UserCustomGroup.class, recipient.getId ()));
			} else if (RecipientType.NON_HAN_GROUP.equals (recipient.getType ())) {
				member.setNonHanUserGroup (entityManager.find (NonHanUserGroup.class, recipient.getId ()));
			} else if (RecipientType.NON_HAN_USER.equals (recipient.getType ())) {
				member.setNonHanUser (entityManager.find (NonHanUser.class, recipient.getId ()));
			} else if (RecipientType.ORGANIZATION.equals (recipient.getType ())
					|| RecipientType.ORGANIZATION_TYPE.equals (recipient.getType ())) {
				member.setOrganization (entityManager.find (Organization.class, recipient.getId ()));
			} else if (RecipientType.ROLE.equals (recipient.getType ())) {
				member.setRole (entityManager.find (Role.class, recipient.getId ()));
			} else if (RecipientType.USER.equals (recipient.getType ())) {
				member.setUserData (entityManager.find (UserData.class, recipient.getId ()));
			} else if (RecipientType.CRITERIA.equals (recipient.getType ())) {
				member.setChicagoOnly (recipient.isChicagoOnly ());
				member.setCriterias (new ArrayList<CustomGroupCriteria> ());
				for (FilterCriteriaTO criteria : recipient.getCriteria ()) {
					dbCriteria = new CustomGroupCriteria ();
					dbCriteria.setOrganizationType (entityManager.find (Organization.class,
							criteria.getOrganizationTypeId ()));
					dbCriteria.setAreaField (criteria.getAreaFieldId () == null ? null : 
						entityManager.find (AreaField.class, criteria.getAreaFieldId ().intValue ()));
					dbCriteria.setOrganization (criteria.getOrganizationId () == null ? null :
						entityManager.find (Organization.class, criteria.getOrganizationId ()));
					dbCriteria.setRole (criteria.getRoleId () == null ? null :
						entityManager.find (Role.class, criteria.getRoleId ()));
					dbCriteria.setMember (member);
					member.getCriterias ().add (dbCriteria);
				}
			}
			member.setUserCustomGroup (group);
			entityManager.persist (member);
		}
		entityManager.flush ();
		return group.getGroupId ();
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.custgroups.services.GroupManagementService#updateGroup(long, long, java.util.List)
	 */
	@Override
	//abey ...@Transactional (propagation = Propagation.REQUIRED)
	public void updateGroup (final long userId, final long groupId, final List<RecipientTO> recipients) {
		final UserData user = entityManager.find (UserData.class, userId);
		final UserCustomGroup group = entityManager.find (UserCustomGroup.class, groupId);
		group.setModifiedBy (user);
		group.setDateModified (new Date ());
		entityManager.merge (group);
		CustomGroupMember[] membersInDB = group.getMembers ().toArray (
				new CustomGroupMember[group.getMembers ().size ()]);
		RecipientTO[] updatedRecipients = recipients.toArray (
				new RecipientTO[recipients.size ()]);
		RecipientTO recipient;
		CustomGroupMember member;
		CustomGroupCriteria dbCriteria;
		if (membersInDB.length > updatedRecipients.length) {
			for (int i = 0; i < membersInDB.length; i++) {
				member = membersInDB[i];
				if (i < updatedRecipients.length) {
					recipient = updatedRecipients[i];
					if (RecipientType.AREA_FIELD.equals (recipient.getType ())) {
						member.setAreaField (entityManager.find (AreaField.class, (int) recipient.getId ()));
						member.setChildGroup (null);
						member.setNonHanUser (null);
						member.setNonHanUserGroup (null);
						member.setOrganization (null);
						member.setRole (null);
						member.setUserData (null);
					} else if (RecipientType.GROUP.equals (recipient.getType ())) {
						member.setChildGroup (entityManager.find (UserCustomGroup.class, recipient.getId ()));
						member.setAreaField (null);
						member.setNonHanUser (null);
						member.setNonHanUserGroup (null);
						member.setOrganization (null);
						member.setRole (null);
						member.setUserData (null);
					} else if (RecipientType.NON_HAN_GROUP.equals (recipient.getType ())) {
						member.setNonHanUserGroup (entityManager.find (NonHanUserGroup.class, recipient.getId ()));
						member.setChildGroup (null);
						member.setNonHanUser (null);
						member.setAreaField (null);
						member.setOrganization (null);
						member.setRole (null);
						member.setUserData (null);
					} else if (RecipientType.NON_HAN_USER.equals (recipient.getType ())) {
						member.setNonHanUser (entityManager.find (NonHanUser.class, recipient.getId ()));
						member.setChildGroup (null);
						member.setAreaField (null);
						member.setNonHanUserGroup (null);
						member.setOrganization (null);
						member.setRole (null);
						member.setUserData (null);
					} else if (RecipientType.ORGANIZATION.equals (recipient.getType ())
							|| RecipientType.ORGANIZATION_TYPE.equals (recipient.getType ())) {
						member.setOrganization (entityManager.find (Organization.class, recipient.getId ()));
						member.setChildGroup (null);
						member.setNonHanUser (null);
						member.setNonHanUserGroup (null);
						member.setAreaField (null);
						member.setRole (null);
						member.setUserData (null);
					} else if (RecipientType.ROLE.equals (recipient.getType ())) {
						member.setRole (entityManager.find (Role.class, recipient.getId ()));
						member.setChildGroup (null);
						member.setNonHanUser (null);
						member.setNonHanUserGroup (null);
						member.setOrganization (null);
						member.setAreaField (null);
						member.setUserData (null);
					} else if (RecipientType.USER.equals (recipient.getType ())) {
						member.setUserData (entityManager.find (UserData.class, recipient.getId ()));
						member.setChildGroup (null);
						member.setNonHanUser (null);
						member.setNonHanUserGroup (null);
						member.setOrganization (null);
						member.setRole (null);
						member.setAreaField (null);
					} else if (RecipientType.CRITERIA.equals (recipient.getType ())) {
						member.setChicagoOnly (recipient.isChicagoOnly ());
						for (CustomGroupCriteria criteria : member.getCriterias ()) {
							entityManager.remove (criteria);
						}
						member.setCriterias (new ArrayList<CustomGroupCriteria> ());
						for (FilterCriteriaTO criteria : recipient.getCriteria ()) {
							dbCriteria = new CustomGroupCriteria ();
							dbCriteria.setOrganizationType (entityManager.find (Organization.class,
									criteria.getOrganizationTypeId ()));
							dbCriteria.setAreaField (criteria.getAreaFieldId () == null ? null : 
								entityManager.find (AreaField.class, criteria.getAreaFieldId ().intValue ()));
							dbCriteria.setOrganization (criteria.getOrganizationId () == null ? null :
								entityManager.find (Organization.class, criteria.getOrganizationId ()));
							dbCriteria.setRole (criteria.getRoleId () == null ? null :
								entityManager.find (Role.class, criteria.getRoleId ()));
							dbCriteria.setMember (member);
							member.getCriterias ().add (dbCriteria);
						}
					}
					entityManager.merge (member);
				} else {
					entityManager.remove (member);
				}
			}
		} else {
			for (int i = 0; i < updatedRecipients.length; i++) {
				recipient = updatedRecipients[i];
				if (i < membersInDB.length) {
					member = membersInDB[i];
					if (RecipientType.AREA_FIELD.equals (recipient.getType ())) {
						member.setAreaField (entityManager.find (AreaField.class, (int) recipient.getId ()));
						member.setChildGroup (null);
						member.setNonHanUser (null);
						member.setNonHanUserGroup (null);
						member.setOrganization (null);
						member.setRole (null);
						member.setUserData (null);
					} else if (RecipientType.GROUP.equals (recipient.getType ())) {
						member.setChildGroup (entityManager.find (UserCustomGroup.class, recipient.getId ()));
						member.setAreaField (null);
						member.setNonHanUser (null);
						member.setNonHanUserGroup (null);
						member.setOrganization (null);
						member.setRole (null);
						member.setUserData (null);
					} else if (RecipientType.NON_HAN_GROUP.equals (recipient.getType ())) {
						member.setNonHanUserGroup (entityManager.find (NonHanUserGroup.class, recipient.getId ()));
						member.setChildGroup (null);
						member.setNonHanUser (null);
						member.setAreaField (null);
						member.setOrganization (null);
						member.setRole (null);
						member.setUserData (null);
					} else if (RecipientType.NON_HAN_USER.equals (recipient.getType ())) {
						member.setNonHanUser (entityManager.find (NonHanUser.class, recipient.getId ()));
						member.setChildGroup (null);
						member.setAreaField (null);
						member.setNonHanUserGroup (null);
						member.setOrganization (null);
						member.setRole (null);
						member.setUserData (null);
					} else if (RecipientType.ORGANIZATION.equals (recipient.getType ())
							|| RecipientType.ORGANIZATION_TYPE.equals (recipient.getType ())) {
						member.setOrganization (entityManager.find (Organization.class, recipient.getId ()));
						member.setChildGroup (null);
						member.setNonHanUser (null);
						member.setNonHanUserGroup (null);
						member.setAreaField (null);
						member.setRole (null);
						member.setUserData (null);
					} else if (RecipientType.ROLE.equals (recipient.getType ())) {
						member.setRole (entityManager.find (Role.class, recipient.getId ()));
						member.setChildGroup (null);
						member.setNonHanUser (null);
						member.setNonHanUserGroup (null);
						member.setOrganization (null);
						member.setAreaField (null);
						member.setUserData (null);
					} else if (RecipientType.USER.equals (recipient.getType ())) {
						member.setUserData (entityManager.find (UserData.class, recipient.getId ()));
						member.setChildGroup (null);
						member.setNonHanUser (null);
						member.setNonHanUserGroup (null);
						member.setOrganization (null);
						member.setRole (null);
						member.setAreaField (null);
					} else if (RecipientType.CRITERIA.equals (recipient.getType ())) {
						member.setChicagoOnly (recipient.isChicagoOnly ());
						for (CustomGroupCriteria criteria : member.getCriterias ()) {
							entityManager.remove (criteria);
						}
						member.setCriterias (new ArrayList<CustomGroupCriteria> ());
						for (FilterCriteriaTO criteria : recipient.getCriteria ()) {
							dbCriteria = new CustomGroupCriteria ();
							dbCriteria.setOrganizationType (entityManager.find (Organization.class,
									criteria.getOrganizationTypeId ()));
							dbCriteria.setAreaField (criteria.getAreaFieldId () == null ? null : 
								entityManager.find (AreaField.class, criteria.getAreaFieldId ().intValue ()));
							dbCriteria.setOrganization (criteria.getOrganizationId () == null ? null :
								entityManager.find (Organization.class, criteria.getOrganizationId ()));
							dbCriteria.setRole (criteria.getRoleId () == null ? null :
								entityManager.find (Role.class, criteria.getRoleId ()));
							dbCriteria.setMember (member);
							member.getCriterias ().add (dbCriteria);
						}
					}
					entityManager.merge (member);
				} else {
					member = new CustomGroupMember ();
					member.setUserCustomGroup (group);
					if (RecipientType.AREA_FIELD.equals (recipient.getType ())) {
						member.setAreaField (entityManager.find (AreaField.class, (int) recipient.getId ()));
						member.setChildGroup (null);
						member.setNonHanUser (null);
						member.setNonHanUserGroup (null);
						member.setOrganization (null);
						member.setRole (null);
						member.setUserData (null);
					} else if (RecipientType.GROUP.equals (recipient.getType ())) {
						member.setChildGroup (entityManager.find (UserCustomGroup.class, recipient.getId ()));
						member.setAreaField (null);
						member.setNonHanUser (null);
						member.setNonHanUserGroup (null);
						member.setOrganization (null);
						member.setRole (null);
						member.setUserData (null);
					} else if (RecipientType.NON_HAN_GROUP.equals (recipient.getType ())) {
						member.setNonHanUserGroup (entityManager.find (NonHanUserGroup.class, recipient.getId ()));
						member.setChildGroup (null);
						member.setNonHanUser (null);
						member.setAreaField (null);
						member.setOrganization (null);
						member.setRole (null);
						member.setUserData (null);
					} else if (RecipientType.NON_HAN_USER.equals (recipient.getType ())) {
						member.setNonHanUser (entityManager.find (NonHanUser.class, recipient.getId ()));
						member.setChildGroup (null);
						member.setAreaField (null);
						member.setNonHanUserGroup (null);
						member.setOrganization (null);
						member.setRole (null);
						member.setUserData (null);
					} else if (RecipientType.ORGANIZATION.equals (recipient.getType ())
							|| RecipientType.ORGANIZATION_TYPE.equals (recipient.getType ())) {
						member.setOrganization (entityManager.find (Organization.class, recipient.getId ()));
						member.setChildGroup (null);
						member.setNonHanUser (null);
						member.setNonHanUserGroup (null);
						member.setAreaField (null);
						member.setRole (null);
						member.setUserData (null);
					} else if (RecipientType.ROLE.equals (recipient.getType ())) {
						member.setRole (entityManager.find (Role.class, recipient.getId ()));
						member.setChildGroup (null);
						member.setNonHanUser (null);
						member.setNonHanUserGroup (null);
						member.setOrganization (null);
						member.setAreaField (null);
						member.setUserData (null);
					} else if (RecipientType.USER.equals (recipient.getType ())) {
						member.setUserData (entityManager.find (UserData.class, recipient.getId ()));
						member.setChildGroup (null);
						member.setNonHanUser (null);
						member.setNonHanUserGroup (null);
						member.setOrganization (null);
						member.setRole (null);
						member.setAreaField (null);
					} else if (RecipientType.CRITERIA.equals (recipient.getType ())) {
						member.setChicagoOnly (recipient.isChicagoOnly ());
						member.setCriterias (new ArrayList<CustomGroupCriteria> ());
						for (FilterCriteriaTO criteria : recipient.getCriteria ()) {
							dbCriteria = new CustomGroupCriteria ();
							dbCriteria.setOrganizationType (entityManager.find (Organization.class,
									criteria.getOrganizationTypeId ()));
							dbCriteria.setAreaField (criteria.getAreaFieldId () == null ? null : 
								entityManager.find (AreaField.class, criteria.getAreaFieldId ().intValue ()));
							dbCriteria.setOrganization (criteria.getOrganizationId () == null ? null :
								entityManager.find (Organization.class, criteria.getOrganizationId ()));
							dbCriteria.setRole (criteria.getRoleId () == null ? null :
								entityManager.find (Role.class, criteria.getRoleId ()));
							dbCriteria.setMember (member);
							member.getCriterias ().add (dbCriteria);
						}
					}
					entityManager.persist (member);
				}
			}
		}
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.custgroups.services.GroupManagementService#deleteGroup(long, long)
	 */
	@Override
	//abey ...@Transactional (propagation = Propagation.REQUIRED)
	public DeleteResult deleteGroup (long userId, long groupId) {
		final UserCustomGroup group = entityManager.find (UserCustomGroup.class, groupId);
		DeleteResult result;
		if (group.getUserData ().getUserId () == userId) {
			final Query q = entityManager.createQuery (
					"select m from CustomGroupMember m where m.childGroup.groupId = " + group.getGroupId ());
			final List<?> ocurrences = q.getResultList ();
			if (ocurrences.isEmpty ()) {
				group.setDeleted (true);
				entityManager.merge (group);
				entityManager.flush ();
				result = DeleteResult.SUCCESS;
			} else {
				result = DeleteResult.GROUP_IN_USE;
			}
		} else {
			result = DeleteResult.NOT_OWNER;
		}
		return result;
	}

	/**
	 * @param entityManager the entityManager to set
	 */
	//abey...
	//@PersistenceContext
	public void setEntityManager (EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
