package org.cdph.han.audittrail.dto;

import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.audittrail.service.AuditTrailService;
import org.cdph.han.dto.HanLazyDataModel;
//import org.cdph.han.dto.LazyDataModel;
import org.cdph.han.persistence.AuditTrailAction;
import org.springframework.beans.factory.annotation.Autowired;

public class AuditTrailDTO<AuditTrailTO> extends HanLazyDataModel<AuditTrailTO>{
	
	public AuditTrailDTO()
	{
		
	}

	/** Class logger */
	private static final Log log = LogFactory
			.getLog(AuditTrailDTO.class);
	
	private static final int ROWS_PER_PAGE = 20;
	
	private Date startDate;
	private Date endDate;
	private String performedBy;
	private AuditTrailAction actionPerformed;
	private String result;
	
	@Autowired
	private AuditTrailService service;
	
	public AuditTrailDTO(AuditTrailService service) {
		this.service = service;
	}

	//@Override
	public int countRows() {
		//return 0;
		return service.getAuditTrailCount(this);		
	}

	@Override
	public List<AuditTrailTO> findRows(int startRow, int finishRow) {
		return (List<AuditTrailTO>) service.getAuditTrailRecords(this, startRow,
				finishRow);
		//return null;
	}
	
	//@Override
	public int getRowsPerPage() {
		return ROWS_PER_PAGE;
	}

	public AuditTrailService getService() {
		return service;
		//return null;
	}

	public void setService(AuditTrailService service) {
		this.service = service;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getPerformedBy() {
		return performedBy;
	}

	public void setPerformedBy(String performedBy) {
		this.performedBy = performedBy;
	}

	public AuditTrailAction getActionPerformed() {
		return actionPerformed;
	}

	public void setActionPerformed(AuditTrailAction actionPerformed) {
		this.actionPerformed = actionPerformed;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

}
