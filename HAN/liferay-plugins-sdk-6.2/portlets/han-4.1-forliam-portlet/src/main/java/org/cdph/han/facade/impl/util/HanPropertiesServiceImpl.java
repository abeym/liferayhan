package org.cdph.han.facade.impl.util;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.portlet.PortletClassLoaderUtil;

import java.util.List;

import javax.persistence.EntityManager;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.persistence.HanProperty;
import org.cdph.han.service.HanPropertyLocalServiceUtil;
import org.cdph.han.util.services.HanPropertiesService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

/**
 * Implementation of the HanPropertiesService using JPA
 * @author Abey Mani
 * @version 1.0
 */

@Repository
@Service ("hanPropertiesService")
public class HanPropertiesServiceImpl implements HanPropertiesService {
	/** Class logger */
	private static Log log = LogFactory.getLog(HanPropertiesServiceImpl.class);
	@Autowired
	private ModelMapper modelMapper;
	/** Entity manager */
	/*private EntityManager entityManager;*/

	/* (non-Javadoc)
	 * @see org.cdph.han.util.services.HanPropertiesService#deleteProperty(java.lang.String)
	 */
	//abey ...@Transactional
	public void deleteProperty (final String name) {
		final HanProperty property = searchProperty (name);
		/*entityManager.remove (property);
		entityManager.flush ();*/
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.util.services.HanPropertiesService#getValue(java.lang.String)
	 */
	//abey ...@Transactional (readOnly = true)
	public String getValue (final String name) {
		HanProperty pval = searchProperty (name);
		String sp = null;
		if(pval != null)sp = pval.getValue ();
		return sp;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.util.services.HanPropertiesService#insertProperty(java.lang.String, java.lang.String)
	 */
	//abey ...@Transactional
	public void insertProperty (final String name, final String value) {
		final HanProperty property = new HanProperty ();
		property.setName (name);
		property.setValue (value);
		/*entityManager.persist (property);
		entityManager.flush ();*/
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.util.services.HanPropertiesService#updateProperty(java.lang.String, java.lang.String)
	 */
	//abey ...@Transactional
	public void updateProperty (final String name, final String value) {
		final HanProperty property = searchProperty (name);
		property.setValue (value);
		/*entityManager.merge (property);
		entityManager.flush ();*/
	}
	public void ensureServices(){
		modelMapper=modelMapper==null?new ModelMapper():modelMapper;
	}
	/**
	 * Searches a HAN property by name
	 * @param name String with the name of the property
	 * @return String with the value of the property
	 */
	private HanProperty searchProperty (final String name) {
		/*final Query q = entityManager.createNamedQuery ("HanProperty.FindByName");
		q.setParameter ("name", name);
		return (HanProperty) q.getSingleResult ();*/
		//		@NamedQuery (name = "HanProperty.FindByName", query = "select p from HanProperty p where p.name = :name")
	    /*DynamicQuery query = DynamicQueryFactoryUtil.forClass(AuditTrail.class, PortletClassLoaderUtil.getClassLoader());
	    query.add(PropertyFactoryUtil.forName("datePublished").isNotNull())
				.add(PropertyFactoryUtil.forName("readyToPublish").eq(scheduled ? 1 : 0));
		    Order levelOrder = OrderFactoryUtil.desc("topicId");
		    query.addOrder(levelOrder);
		    Order pubOrder = OrderFactoryUtil.desc("datePublished");
		    query.addOrder(pubOrder);
		    //List<Alert> alerts = AlertLocalServiceUtil.dynamicQuery(publishQuery);
        <entity name="HanProperty" local-service="true" table="han_properties">
        		<column name="id" type="long" primary="true" db-name="id" />
        		<column name="name" type="String" db-name="name" />
        		<column name="value" type="String" db-name="value" />
        		<finder return-type="HanProperty" name="Name">
        			<finder-column name="name" />
        		</finder>
        </entity>
		     *
		     */
		HanProperty hp = new HanProperty();
		try {
			ensureServices();
			DynamicQuery query = DynamicQueryFactoryUtil.forClass(org.cdph.han.model.HanProperty.class, PortletClassLoaderUtil.getClassLoader()).add(PropertyFactoryUtil.forName("name").eq(name));
			//List hanProps = HanPropertyLocalServiceUtil.dynamicQuery(query);
			org.cdph.han.model.HanProperty hanProps = HanPropertyLocalServiceUtil.getValue(name);
			if((hanProps!=null) ){
				log.debug(hanProps.getValue());
				hp = new HanProperty();
				hp.setName(hanProps.getName());
				hp.setValue(hanProps.getValue());
				return modelMapper.map(hanProps, HanProperty.class);
			}
		} catch (Exception e) {
			log.error(e);
		}
		return hp;
	}

	/**
	 * @param entityManager the entityManager to set
	 */
	//abey cocmmenting to deploy
	//@PersistenceContext
	public void setEntityManager (final EntityManager entityManager) {
		/*this.entityManager = entityManager;*/
	}

}
