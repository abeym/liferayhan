package org.cdph.han.custgroups.services;

import java.util.List;

import org.cdph.han.custgroups.dto.GroupDisplayTO;
import org.cdph.han.custgroups.dto.KeyDescriptionTO;
import org.cdph.han.custgroups.dto.UserDisplayTO;

public class OrganizationalSearchServiceLayer implements
		OrganizationalSearchService {

	@Override
	public KeyDescriptionTO[] getOrganizationTypes() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public KeyDescriptionTO[] getOrganizations(long orgTypeId,
			boolean onlyChicagoEntities) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public KeyDescriptionTO[] getAreaFields(long orgTypeId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public KeyDescriptionTO[] getRoles(long orgTypeId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<UserDisplayTO> getUsersInOrganization(long organizationId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<UserDisplayTO> filterUsers(long[] organizationIds,
			long[] areaFieldIds, long[] roleIds) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<UserDisplayTO> getUsersByOrganizationType(long orgTypeId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UserDisplayTO[] searchUsersInBulkUpload(List<Long> listIds) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UserDisplayTO[] searchUsersInGroup(List<Long> groupIds) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UserDisplayTO[] searchIndividuals(String firstName, String lastName,
			boolean chicagoOnly) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GroupDisplayTO[] getBulkUploadNames(String nameFilter) {
		// TODO Auto-generated method stub
		return null;
	}

}
