package org.cdph.han.alerts.services;

import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.alerts.dto.AlertSearchTO;
import org.cdph.han.alerts.dto.AlertTO;
import org.cdph.han.model.Alert;
import org.cdph.han.model.AlertTopic;
import org.cdph.han.service.AlertLocalServiceUtil;
import org.cdph.han.service.AlertTopicLocalServiceUtil;

public class AlertLayerImpl {
	private static final Log log = LogFactory.getLog (AlertLayerImpl.class);

	public List<AlertSearchTO> searchCurrentByDate(Long userId) {
		List<AlertSearchTO> foundAlerts = null;
		try {
			if (userId == -1) {
				System.err.println("\nUser Id is -1.\n");
				return null;
			}
			User user = UserLocalServiceUtil.getUser(userId);
			log.trace("Alert Layer> User: " + userId + ", User " + user + ".\n");
			if (isAdmin(userId)) {
				log.debug("User is an administrator.\n");
				List<Alert> alerts = AlertLocalServiceUtil.findPublishedByDate();
				foundAlerts = buildAlertList(alerts);
			} else {
				log.debug("User is not an administrator. Run FindByRecDate query.\n");
				List<Alert> alerts = AlertLocalServiceUtil.findByRecDate(userId);
				foundAlerts = buildAlertList(alerts);
			}
		} catch (PortalException e) {
			log.error(e);
		} catch (SystemException e) {
			log.error(e);
		}
		return foundAlerts;
	}

	public List<AlertSearchTO> findScheduled(Long userId, boolean scheduled) {
		List<AlertSearchTO> foundAlerts = null;
		try {
			if (userId == -1) {
				System.err.println("\nUser Id is -1.\n");
				return null;
			}
			User user = UserLocalServiceUtil.getUser(userId);
			log.trace("Alert Layer> User: " + userId + ", User " + user + ".\n");
			if (isAdmin(userId)) {
				System.out.println("User is an administrator.\n");
				List<Alert> alerts;
				if (scheduled) {
					alerts = AlertLocalServiceUtil.findScheduled();
				} else {
					alerts = AlertLocalServiceUtil.findDrafts();
				}
				foundAlerts = buildAlertList(alerts);
				} else {
					System.out.println("User is not an administrator.\n");
					List<Alert> alerts = AlertLocalServiceUtil.findByRecLevel(userId);
					foundAlerts = buildAlertList(alerts);
				}
		} catch (PortalException e) {
		} catch (SystemException e) {
		}
		return foundAlerts;
	}

	public List<AlertSearchTO> searchCurrentByDateByTopic(Long userId) {
		List<AlertSearchTO> foundAlerts = null;
		try {
			if (userId == -1) {
				System.err.println("\nUser Id is -1.\n");
				return null;
			}
			User user = UserLocalServiceUtil.getUser(userId);
			System.out.println("Alert Layer> User: " + userId + ", User " + user + ".\n");
			if (isAdmin(userId)) {
				System.out.println("User is an administrator.\n");
				List<Alert> alerts = AlertLocalServiceUtil.findPublishedByTopic();
				foundAlerts = buildAlertList(alerts);
			} else {
				System.out.println("User is not an administrator.\n");
				List<Alert> alerts = AlertLocalServiceUtil.findByRecTopic(userId);
				foundAlerts = buildAlertList(alerts);
			}
		} catch (PortalException e) {
		} catch (SystemException e) {
		}
		return foundAlerts;
	}

	public List<AlertSearchTO> searchCurrentByDateByLevel(Long userId) {
		List<AlertSearchTO> foundAlerts = null;
		try {
			if (userId == -1) {
				System.err.println("\nUser Id is -1.\n");
				return null;
			}
			User user = UserLocalServiceUtil.getUser(userId);
			System.out.println("Alert Layer> User: " + userId + ", User " + user + ".\n");
			if (isAdmin(userId)) {
				System.out.println("User is an administrator.\n");
				List<Alert> alerts = AlertLocalServiceUtil.findPublishedByLevel();
				foundAlerts = buildAlertList(alerts);
			} else {
				System.out.println("User is not an administrator: Alert.FindByRecLevel.\n");
				List<Alert> alerts = AlertLocalServiceUtil.findByRecLevel(userId);
				foundAlerts = buildAlertList(alerts);
			}
		} catch (PortalException e) {
		} catch (SystemException e) {
		}
		return foundAlerts;
	}

	private boolean isAdmin(Long userId)
		throws SystemException, PortalException {
		User user = UserLocalServiceUtil.getUser(userId);
		log.debug("Alert Layer> User: " + userId + ", User " + user + ".\n");
		boolean adminRole = false;
		if (user == null) {
			System.err.println("User with Id " + userId + " could not be found.");
			return adminRole;
		} else {
			for (Role role : user.getRoles()) {
				if (role.getName().indexOf("Administrator") > -1) {
					adminRole = true;
					break;
				}
			}
		}
		return adminRole;
	}

	private long[] getOrganizations(Long userId)
			throws SystemException, PortalException {
		User user = UserLocalServiceUtil.getUser(userId);
		if (user == null) {
			System.err.println("User with Id " + userId + " could not be found.");
			return null;
		}
		return user.getOrganizationIds();
	}

	private List<Long> getRoles(Long userId)
			throws SystemException, PortalException {
			User user = UserLocalServiceUtil.getUser(userId);
			List<Long> roleList = new ArrayList<Long>();
			if (user == null) {
				System.err.println("User with Id " + userId + " could not be found.");
				return roleList;
			} else {
				for (Role role : user.getRoles()) {
					if (role.getName().indexOf("Administrator") > -1) {
						roleList.add(role.getRoleId());
					}
				}
			}
			return roleList;
		}

	private List<AlertSearchTO> buildAlertList(List<Alert> alerts) {
		log.trace("AlertLayerImpl>");
		if (alerts == null) {
			System.out.println("No records retrieved from AlertLocalServiceUtil");
			return null;
		}
		log.trace("Alert Layer> Alerts: " + alerts.size());
		List<AlertSearchTO> foundAlerts = new ArrayList<AlertSearchTO>();
		try {
			List<AlertTopic> alertTopics = AlertTopicLocalServiceUtil.getAlertTopics(QueryUtil.ALL_POS, QueryUtil.ALL_POS);
			log.info("Alert Layer> topics: " + alertTopics.size());
			for (Alert alert : alerts) {
				//log.debug("AlertLayer> add: " + alert.getName() + " / " + alert.getCategory());
				AlertSearchTO ato = new AlertSearchTO() {};
				ato.setIdentStyle("identStyle");
				ato.setTitle(alert.getName());
				ato.setAlertId(alert.getId());
				ato.setPublicationDate(alert.getDatePublished());
				ato.setTopic(getAlertTopic(alertTopics, alert.getTopicId()).getTitle());
				foundAlerts.add(ato);
			}
		} catch (SystemException e) {
		}
		return foundAlerts;
	}

	private AlertTopic getAlertTopic(List<AlertTopic> alertTopics, long alertTopicId) {
		for (AlertTopic alertTopic : alertTopics) {
			if (alertTopic.getId() == alertTopicId) return alertTopic;
		}
		return null;
	}

	public Alert saveAlert(AlertTO ato){
		Alert alm=null;
		
		/*HashMap saveMap = new HashMap();
		saveMap.put("id",Long.valueOf(ato.getContactUserId()));
		//alm.setAato.getAbstractText()
		saveMap.put("DateCreated",new Date());
		saveMap.put("DateExpired",ato.getExpirationDate());
		saveMap.put("DatePublished",ato.getPublishedDate());
		saveMap.put("DateScheduled",ato.getScheduledDate());*/
	

		try {
			long id = CounterLocalServiceUtil.increment();
			alm=AlertLocalServiceUtil.createAlert(id);
			alm.setDateCreated(ato.getCreatedDate());
			//alm.setAnswerPhone(ato.getA);
			alm.setName(ato.getAlertName());
			//alm.set
			//alm=AlertLocalServiceUtil.saveAlert(saveMap);
			AlertLocalServiceUtil.addAlert(alm);
			// TODO : save to AlertMessage entity
		} catch (SystemException e) {
			log.error(e);
		}
		
		return alm;
	}
}
