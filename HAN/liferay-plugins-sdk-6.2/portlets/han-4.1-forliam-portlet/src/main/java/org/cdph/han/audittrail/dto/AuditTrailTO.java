package org.cdph.han.audittrail.dto;

import java.io.Serializable;
import java.util.Date;

import org.cdph.han.persistence.AuditTrailAction;

/**
 * Audit trail transfer object
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class AuditTrailTO implements Serializable {
	/** Class serial version UID */
	private static final long serialVersionUID = 2206979492504772239L;
	/** Date the action was performed */
	private Date datePerformed;
	/** ID of the user that performed the action (email or ID if deleted) */
	private String user;
	/** The action performed by the user */
	private AuditTrailAction actionPerformed;
	/** The alert id if the action was performed over an alert */
	private Long alertId;

	/**
	 * @return the datePerformed
	 */
	public Date getDatePerformed () {
		return datePerformed;
	}

	/**
	 * @param datePerformed the datePerformed to set
	 */
	public void setDatePerformed (Date datePerformed) {
		this.datePerformed = datePerformed;
	}

	/**
	 * @return the user
	 */
	public String getUser () {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser (String user) {
		this.user = user;
	}

	/**
	 * @return the actionPerformed
	 */
	public AuditTrailAction getActionPerformed () {
		return actionPerformed;
	}

	/**
	 * @param actionPerformed the actionPerformed to set
	 */
	public void setActionPerformed (AuditTrailAction actionPerformed) {
		this.actionPerformed = actionPerformed;
	}

	/**
	 * @return the alertId
	 */
	public Long getAlertId () {
		return alertId;
	}

	/**
	 * @param alertId the alertId to set
	 */
	public void setAlertId (Long alertId) {
		this.alertId = alertId;
	}

}
