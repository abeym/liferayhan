package org.cdph.han.util.asynch.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * This transfer objects will transport the response af an asynchronous request
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class AsynchResponseTO implements Serializable {
	/** Class serial version UID */
	private static final long serialVersionUID = -9123272554253729109L;
	/** Map containing the result of the operation, this allows multiple responses in the same message */
	private Map<String, Serializable> result;
	/** Group ID to update */
	private String groupId;

	/**
	 * Default constructor
	 */
	public AsynchResponseTO (final String groupId) {
		this.groupId = groupId;
		result = new HashMap<String, Serializable> ();
	}

	/**
	 * Constructor to set the result map
	 */
	public AsynchResponseTO (final String groupId, final Map<String, Serializable> result) {
		this.groupId = groupId;
		this.result = result;
	}

	/**
	 * @return the result
	 */
	public Map<String, Serializable> getResult () {
		return result;
	}

	/**
	 * @return the groupId
	 */
	public String getGroupId () {
		return groupId;
	}

}
