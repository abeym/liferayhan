package org.cdph.han.util.asynch.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.util.asynch.beans.UpdatableBean;
import org.cdph.han.util.asynch.dto.AsynchRequestTO;
import org.cdph.han.util.asynch.dto.AsynchResponseTO;
import org.cdph.han.util.asynch.jms.RequestMessageCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.JmsTemplate;

/**
 * Implementation of the AsynchRefreshService, the implementation must be configured in Spring as a singleton to work
 * properly
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class AsynchRefreshServiceImpl implements AsynchRefreshService {
	/** Class log */
	private static final Log log = LogFactory.getLog (AsynchRefreshServiceImpl.class);
	/** Map with the registered beans for one time update */
	private Map<String, UpdatableBean> oneTimeUpdate;
	/** Map with the registered beans for subscriber updates */
	private Map<String, List<UpdatableBean>> subscribers;
	/** JMS Template used to request one time updates */
	//abey...
	//@Autowired
	//@Qualifier ("hanAsynchReq")
	private JmsTemplate jmsTemplate;

	/**
	 * Initializes the maps
	 */
	public AsynchRefreshServiceImpl () {
		log.info ("Creating instance of the AsynchRefreshService.");
		oneTimeUpdate = Collections.synchronizedMap (new HashMap<String, UpdatableBean> ());
		subscribers = Collections.synchronizedMap (new HashMap<String, List<UpdatableBean>> ());
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.util.asynch.services.AsynchRefreshService#addOneTimeUpdatableBean(org.cdph.han.util.asynch.UpdatableBean, java.lang.String)
	 */
	@Override
	public void addOneTimeUpdatableBean (final UpdatableBean updatableBean,
			final AsynchRequestTO request) {
		final RequestMessageCreator creator = new RequestMessageCreator (request);
		jmsTemplate.send (creator);
		oneTimeUpdate.put (request.getCorrelationID (), updatableBean);
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.util.asynch.services.AsynchRefreshService#addSubscriberUpdatableBean(org.cdph.han.util.asynch.UpdatableBean, java.lang.String)
	 */
	@Override
	public void addSubscriberUpdatableBean (final UpdatableBean updatableBean,
			final String groupId) {
		if (subscribers.containsKey (groupId)) {
			if (!subscribers.get (groupId).contains (updatableBean)) {
				subscribers.get (groupId).add (updatableBean);
			}
		} else {
			final List<UpdatableBean> list = (List<UpdatableBean>)
					Collections.synchronizedList (new ArrayList<UpdatableBean> ());
			if (!list.add (updatableBean)) {
				list.add (updatableBean);
			}
			subscribers.put (groupId, list);
		}
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.util.asynch.services.AsynchRefreshService#removeSubscriberUpdatableBean(org.cdph.han.util.asynch.UpdatableBean, java.lang.String)
	 */
	@Override
	public void removeSubscriberUpdatableBean (final UpdatableBean updatableBean,
			final String groupId) {
		if (subscribers.containsKey (groupId)) {
			subscribers.get (groupId).remove (updatableBean);
		}
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.util.asynch.services.AsynchRefreshService#propagateUpdate(java.lang.String, java.lang.String, java.lang.Object)
	 */
	@Override
	public void propagateUpdate (final String correlationID, final String groupId,
			final AsynchResponseTO updatedInfo) {
		if (correlationID != null && oneTimeUpdate.containsKey (correlationID)) {
			final UpdatableBean toUpdate = oneTimeUpdate.get (correlationID);
			oneTimeUpdate.remove (correlationID);
			toUpdate.update (updatedInfo);
		}
		if (groupId != null && subscribers.containsKey (groupId)) {
			for (UpdatableBean toUpdate : subscribers.get (groupId)) {
				toUpdate.update (updatedInfo);
			}
		}
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.util.asynch.services.AsynchRefreshService#removeBean(org.cdph.han.util.asynch.UpdatableBean)
	 */
	@Override
	public void removeBean (final UpdatableBean toRemove) {
		for (List<UpdatableBean> updatableBeans : subscribers.values ()) {
			updatableBeans.remove (toRemove);
		}
	}

}
