package org.cdph.han.alerts.jobs;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.alerts.services.AlertJPAService;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.SimpleTrigger;
import org.quartz.TriggerBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

/**
 * Class representing a Job used to publish an alert in a future time.
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Component ("alertPublicationJob")
public class AlertPublicationJob extends QuartzJobBean {
	/** Class log */
	private static final Log log = LogFactory.getLog (AlertPublicationJob.class);

	/* (non-Javadoc)
	 * @see org.springframework.scheduling.quartz.QuartzJobBean#executeInternal(org.quartz.JobExecutionContext)
	 */
	@Override
	protected void executeInternal (final JobExecutionContext jobContext) throws JobExecutionException {
		try {
			final ApplicationContext appCtx =
				(ApplicationContext) jobContext.getScheduler ().getContext ().get ("applicationContext");
			final AlertJPAService alertJPAService = (AlertJPAService) appCtx.getBean ("alertJPAService");
			final long alertId = jobContext.getJobDetail ().getJobDataMap ().getLong ("alertId");
			final int notificationReportId = alertJPAService.publishAlert (alertId);
			
			JobDetail jobDetail = JobBuilder.newJob(ReportQueryJob.class)
					.withIdentity("QUERY_REPORT", "ALERT_PUBLICATION_GROUP")
					.usingJobData("alertId", alertId)
					.usingJobData("notificationReportId", notificationReportId)
					.build();
					
			SimpleTrigger simpleTrigger = (SimpleTrigger) TriggerBuilder.newTrigger().withIdentity("QueryReport-" + alertId + ":" + notificationReportId,
					"ALERT_PUBLICATION_GROUP")
					.withPriority(5)
					.forJob(jobDetail)
					.withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInMilliseconds(30000).repeatForever())
					.build();
			
			jobContext.getScheduler ().scheduleJob (jobDetail, simpleTrigger);
		} catch (Exception ex) {
			log.error ("Error publishing alert.", ex);
			throw new JobExecutionException ("Error publishing alert.", ex, false);
		}
	}

}
