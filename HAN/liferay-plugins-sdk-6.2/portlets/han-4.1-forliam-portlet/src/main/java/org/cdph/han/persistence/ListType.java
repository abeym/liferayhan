package org.cdph.han.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Entity class representing a row in the ListType table of Liferay
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Entity
@Table (name = "ListType", schema = "lportal")
@NamedQueries (
	value = {
		@NamedQuery (name = "ListType.FindByType", query = "select l from ListType l where l.type like :type")
	}
)
public class ListType implements Serializable {
	/** Class serial version UID */
	private static final long serialVersionUID = -7206471920071556607L;
	/** The id of the list type */
	@Id
	@Column (name = "listTypeId")
	@GeneratedValue (strategy = GenerationType.TABLE)
	private long id;
	/** The name to show to the user */
	@Column (name = "name", length = 75)
	private String name;
	/** The name of the type */
	@Column (name = "type_", length = 75)
	private String type;

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode ());
		result = prime * result + ((type == null) ? 0 : type.hashCode ());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass () != obj.getClass ())
			return false;
		final ListType other = (ListType) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals (other.name))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals (other.type))
			return false;
		return true;
	}

	/**
	 * @return the id
	 */
	public long getId () {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId (long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName () {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName (String name) {
		this.name = name;
	}

	/**
	 * @return the type
	 */
	public String getType () {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType (String type) {
		this.type = type;
	}

}
