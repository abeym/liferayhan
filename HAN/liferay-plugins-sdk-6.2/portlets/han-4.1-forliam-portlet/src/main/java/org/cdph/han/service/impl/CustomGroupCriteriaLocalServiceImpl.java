/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.cdph.han.service.impl;

import java.util.List;

import org.cdph.han.model.CustomGroupCriteria;
import org.cdph.han.model.impl.CustomGroupCriteriaImpl;
import org.cdph.han.service.CustomGroupCriteriaLocalServiceUtil;
import org.cdph.han.service.base.CustomGroupCriteriaLocalServiceBaseImpl;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.portlet.PortletClassLoaderUtil;

/**
 * The implementation of the custom group criteria local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link org.cdph.han.service.CustomGroupCriteriaLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author zanonil
 * @see org.cdph.han.service.base.CustomGroupCriteriaLocalServiceBaseImpl
 * @see org.cdph.han.service.CustomGroupCriteriaLocalServiceUtil
 */
public class CustomGroupCriteriaLocalServiceImpl
	extends CustomGroupCriteriaLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link org.cdph.han.service.CustomGroupCriteriaLocalServiceUtil} to access the custom group criteria local service.
	 */
	
	public CustomGroupCriteria add() {
		try {
		    DynamicQuery query = DynamicQueryFactoryUtil.forClass(
		    		CustomGroupCriteria.class, "maxQuery", PortletClassLoaderUtil.getClassLoader())
					.setProjection(ProjectionFactoryUtil.max("memberId"));
			List<CustomGroupCriteria> customGroupCriteriaList = CustomGroupCriteriaLocalServiceUtil.dynamicQuery(query);
			String str = String.format("%s", customGroupCriteriaList.get(0));
			Long l = Long.parseLong(str);
			CustomGroupCriteria cgc = new CustomGroupCriteriaImpl();
			cgc.setMemberId(l+1);
			System.out.println("Adding Custom Group Criteria: " + l);
			return CustomGroupCriteriaLocalServiceUtil.addCustomGroupCriteria(cgc);
		} catch (Exception e) {
			System.err.println("ERROR Class not found: " + e);
			return null;
		}
	}

	public List<CustomGroupCriteria> findByMemberId (long memberId) {
		try {
		    DynamicQuery query = DynamicQueryFactoryUtil.forClass(
		    		CustomGroupCriteria.class, PortletClassLoaderUtil.getClassLoader())
					.add(PropertyFactoryUtil.forName("memberId").eq(memberId));
			List<CustomGroupCriteria> customGroupCriteriaList = CustomGroupCriteriaLocalServiceUtil.dynamicQuery(query);
			return customGroupCriteriaList;
		} catch (Exception e) {
			System.err.println("ERROR Class not found: " + e);
			return null;
		}
	}

}