/**
 * Copyright (c) 2000-2015 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package org.cdph.han.audittrail.dto;

import com.liferay.faces.util.lang.StringPool;
import com.liferay.faces.util.logging.Logger;
import com.liferay.faces.util.logging.LoggerFactory;
import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.Order;
import com.liferay.portal.kernel.dao.orm.OrderFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.cdph.han.model.AuditTrail;
import org.cdph.han.persistence.AuditTrailAction;
import org.cdph.han.service.AuditTrailLocalServiceUtil;
import org.cdph.han.service.ClpSerializer;
import org.icefaces.ace.component.datatable.DataTable;
import org.icefaces.ace.model.table.LazyDataModel;
import org.icefaces.ace.model.table.SortCriteria;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * This class extends the ICEfaces {@link LazyDataModel} in order to provide a lazy-loaded list of {@link User} objects
 * to the ace:dataTable in the users.xhtml Facelet view.
 *
 */
public class AuditTrailDataModel extends LazyDataModel<AuditTrailTO> implements Serializable {

	// Logger
	private static final Logger log = LoggerFactory.getLogger(AuditTrailDataModel.class);

	// serialVersionUID
	private static final long serialVersionUID = 9147332173339935105L;

	// Private Constants
	private static final String DEFAULT_SORT_CRITERIA = "datePerformed";

	// Private Constants
	private static final int ROWS_PER_PAGE = 20;

	// Private Data Members
	private int rowsPerPage;

	@Autowired
	private ModelMapper modelMapper = new ModelMapper();

	private Date startDate;
	private Date endDate;
	private String performedBy;
	private AuditTrailAction actionPerformed;
	private String result;

	public AuditTrailDataModel() {
		setRowsPerPage(ROWS_PER_PAGE);
		setRowCount(countRows(null));
	}

	public int countRows(SortCriteria[] sortCriterias) {

		try {
			return (int) AuditTrailLocalServiceUtil.dynamicQueryCount(createQuery(sortCriterias, true));
		} catch (SystemException e) {
			log.error(e);
		}
		return 0;

		/*int totalCount = 0;

		try {

			LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
			params.put("expandoAttributes", null);

			Sort sort = SortFactoryUtil.getSort(AuditTrail.class, DEFAULT_SORT_CRITERIA, "desc");

			boolean andSearch = true;
			int status = WorkflowConstants.STATUS_ANY;

			String firstName = null;
			String middleName = null;
			String lastName = null;
			String screenName = null;
			String emailAddress = null;

			Hits hits = AuditTrailLocalServiceUtil.search(companyId, firstName, middleName, lastName, screenName,
					emailAddress, status, params, andSearch, QueryUtil.ALL_POS, QueryUtil.ALL_POS, sort);
			totalCount = hits.getLength();

			Query query = createQuery(criteria, true);

			return ((Number) query.getSingleResult()).intValue();

		}
		catch (Exception e) {
			logger.error(e.getMessage(), e);
		}

		return totalCount;*/
	}

	private DynamicQuery createQuery(SortCriteria[] sortCriterias,  boolean count){
		log.debug("Entry createQuery:"+sortCriterias +":"+count);


		Object o = ClpSerializer.getServletContextName();
		log.info("clpcontext: "+o.toString());
		ClassLoader classLoader = (ClassLoader)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),"portletClassLoader");
		DynamicQuery query = DynamicQueryFactoryUtil.forClass(AuditTrail.class, classLoader);

		// sort
		Order sort;
		if ((sortCriterias != null) && (sortCriterias.length != 0)) {
			if (!sortCriterias[0].isAscending()) {
				sort = OrderFactoryUtil.desc(sortCriterias[0].getPropertyName());
			}
			else {
				sort = OrderFactoryUtil.asc(sortCriterias[0].getPropertyName());
			}
		}
		else {
			sort = OrderFactoryUtil.desc(DEFAULT_SORT_CRITERIA);
		}
		query.addOrder(sort);
		
		
		if(count){
			query.setProjection(ProjectionFactoryUtil.rowCount());
		}

		/*if(filters!=null)
		{*/
			try {
				/*if (trimExpresssion(filters.get("datePerformed")) != null) {
					query.add(PropertyFactoryUtil.forName("datePerformed").ge(trimDate(filters.get("datePerformed"))));
				}*/
				
				if (this.startDate != null) {
					query.add(PropertyFactoryUtil.forName("datePerformed").ge(this.startDate));
				}
	
				if (this.endDate != null) {
					query.add(PropertyFactoryUtil.forName("datePerformed").lt(new Date(this.endDate.getTime() + (1000 * 60 * 60 * 24))));
					//query.add(PropertyFactoryUtil.forName("datePerformed").le(this.endDate));
				}
	
				if (trimExpresssion(this.performedBy) != null) {
					DynamicQuery userQuery = DynamicQueryFactoryUtil.forClass(User.class, classLoader);
					userQuery.add(RestrictionsFactoryUtil.ilike("emailAddress", "%"+this.performedBy+"%"));
					List<User> users = UserLocalServiceUtil.dynamicQuery(userQuery);
					if(users!=null && users.size()>0)
					{
						Criterion criterion = null;
						for(User user: users)
						{
							if(user !=null)
							{
								if(criterion == null)
								{
									criterion = RestrictionsFactoryUtil.eq("userId", Long.valueOf(user.getUserId()));
								}
								else
								{
									criterion = RestrictionsFactoryUtil.or(criterion , RestrictionsFactoryUtil.eq("userId", Long.valueOf(user.getUserId())));
								}
							}
						}
						query.add(criterion);
					}
					/*User user = UserLocalServiceUtil.fetchUserByEmailAddress(LiferayFacesContext.getInstance().getCompanyId(), this.performedBy);
					if(user!=null)
					{
						query.add(RestrictionsFactoryUtil.eq("userId", Long.valueOf(user.getUserId())));
					}
					else
					{
						query.add(RestrictionsFactoryUtil.eq("userId", Long.valueOf("-1")));
					}*/
				}
				if ( (this.actionPerformed!=null) && trimExpresssion(this.actionPerformed.getDisplayFormat()) != null) {
					query.add(RestrictionsFactoryUtil.ilike("actionPerformed", trimAction(this.actionPerformed.getDisplayFormat())));
				}
			} catch (SystemException e) {
				log.error(e);
			}
		//}
		log.debug("Returning query:"+query);
		return query;
	}

	private boolean emptyString(String value) {
		return (value == null || value.trim().length() == 0);
	}

	private AuditTrailTO convertDTO(org.cdph.han.model.AuditTrail trail)
	{
		AuditTrailTO dto = modelMapper.map(trail, AuditTrailTO.class);
		return dto;
	}

	/**
	 * This method is called by the ICEfaces {@link DataTable} according to the rows specified in the currently
	 * displayed page of data.
	 *
	 * @param  first          The zero-relative first row index.
	 * @param  pageSize       The number of rows to fetch.
	 * @param  sortCriterias  The array of sort criteria objects. Note that since the Liferay API does not support
	 *                        multiple sort criterias, the length of this array will never be greater than one.
	 * @param  filters        The query criteria. Note that in order for the filtering to work with the Liferay API, the
	 *                        end-user must specify complete, matching words. Wildcards and partial matches are not
	 *                        supported.
	 */
	@Override
	public List<AuditTrailTO> load(int first, int pageSize, SortCriteria[] sortCriterias, Map<String, String> filters) {

		log.debug("Entry load:"+first +":"+pageSize+":"+sortCriterias+":"+filters);
		this.setRowCount(countRows(sortCriterias));
		List<AuditTrailTO> returnList = new ArrayList<AuditTrailTO>();
		try {
			DynamicQuery query = createQuery(sortCriterias, false);
			List<AuditTrail> auditTrails = AuditTrailLocalServiceUtil.dynamicQuery(query, first, first+pageSize);
			log.debug("auditTrails size:"+auditTrails.size());
			for(AuditTrail trail:auditTrails)
			{
				try{
					User user = UserLocalServiceUtil.getUser(trail.getUserId());
					trail.setUserUuid(user.getEmailAddress());
				}
				catch(Exception e)
				{
					log.error(e);
				}
				returnList.add(convertDTO(trail));
			}
		} catch (SystemException e) {
			log.error(e);
		}
		catch (Exception e) {
			log.error(e);
		}
		log.debug("load size:"+returnList.size());
		return returnList;


		/*List<User> users = null;

		Sort sort;

		// sort
		if ((sortCriterias != null) && (sortCriterias.length != 0)) {

			if (!sortCriterias[0].isAscending()) {
				sort = SortFactoryUtil.getSort(User.class, sortCriterias[0].getPropertyName(), "desc");
			}
			else {
				sort = SortFactoryUtil.getSort(User.class, sortCriterias[0].getPropertyName(), "asc");
			}
		}
		else {
			sort = SortFactoryUtil.getSort(User.class, DEFAULT_SORT_CRITERIA, "asc");
		}

		try {
			LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
			int liferayOneRelativeFinishRow = first + pageSize + 1;

			boolean andSearch = true;
			int status = WorkflowConstants.STATUS_ANY;

			String firstName = trimExpresssion(filters.get("firstName"));
			String middleName = trimExpresssion(filters.get("middleName"));
			String lastName = trimExpresssion(filters.get("lastName"));
			String screenName = trimExpresssion(filters.get("screenName"));
			String emailAddress = trimExpresssion(filters.get("emailAddress"));

			// For the sake of speed, search for users in the index rather than querying the database directly.
			Hits hits = UserLocalServiceUtil.search(companyId, firstName, middleName, lastName, screenName,
					emailAddress, status, params, andSearch, first, liferayOneRelativeFinishRow, sort);

			List<Document> documentHits = hits.toList();

			log.debug(
				("filters firstName=[{0}] middleName=[{1}] lastName=[{2}] screenName=[{3}] emailAddress=[{4}] active=[{5}] andSearch=[{6}] startRow=[{7}] liferayOneRelativeFinishRow=[{8}] sortColumn=[{9}] reverseOrder=[{10}] hitCount=[{11}]"),
				firstName, middleName, lastName, screenName, emailAddress, status, andSearch, first,
				liferayOneRelativeFinishRow, sort.getFieldName(), sort.isReverse(), documentHits.size());

			// Convert the results from the search index into a list of user objects.
			users = new ArrayList<User>(documentHits.size());

			for (Document document : documentHits) {

				long userId = GetterUtil.getLong(document.get(Field.USER_ID));

				try {
					User user = UserLocalServiceUtil.getUserById(userId);
					users.add(user);
				}
				catch (NoSuchUserException nsue) {
					log.error("User with userId=[{0}] does not exist in the search index. Please reindex.");
				}
			}

		}
		catch (Exception e) {
			log.error(e.getMessage(), e);
		}

		return users;*/

	}

	protected String trimExpresssion(String value) {
		String expression = null;
		if (value != null) {
			String trimmedValue = value.trim();
			if (trimmedValue.length() > 0) {
				expression = trimmedValue.toLowerCase();
			}
		}
		return expression;
	}

	protected String trimAction(String value) {
		String expression = trimExpresssion(value);
		if(expression!=null)
		{
			expression = expression.replace(" ", "_");
		}
		return expression;
	}
	
	protected Date trimDate(String value) {
		Date retVal = null;
		value = trimExpresssion(value);
		if(value!=null)
		{
			try {
				retVal = DateFormat.getInstance().parse(value);
				return retVal;
			} catch (ParseException e1) {}
			String[] dfs = {"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd", "MMM dd, yyyy hh:mm:ss Z", "yyyy-MM-dd'T'HH:mm:ss'Z'"};
			for(String df: dfs)
			{
				try {
					retVal = new SimpleDateFormat(df).parse(value);
					return retVal;
				} catch (ParseException e) {}
			}
		}
		return retVal;
	}

	public int getRowsPerPage() {
		return rowsPerPage;
	}

	public void setRowsPerPage(int rowsPerPage) {
		this.rowsPerPage = rowsPerPage;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getPerformedBy() {
		return performedBy;
	}

	public void setPerformedBy(String performedBy) {
		this.performedBy = performedBy;
	}

	public AuditTrailAction getActionPerformed() {
		return actionPerformed;
	}

	public void setActionPerformed(AuditTrailAction actionPerformed) {
		this.actionPerformed = actionPerformed;
	}


	private String getDateStr(Date date)
	{
		if (date !=null) return new SimpleDateFormat("yyy-mm-dd").format(date);
		return null;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public void resetFilters() {
		this.startDate = null;
		this.endDate = null;
		this.actionPerformed = null;
		this.performedBy = null;
	}
}
