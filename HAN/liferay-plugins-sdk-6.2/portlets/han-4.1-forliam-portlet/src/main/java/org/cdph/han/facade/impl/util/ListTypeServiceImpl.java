package org.cdph.han.facade.impl.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.cdph.han.persistence.ListType;
import org.cdph.han.util.dto.ListTypeTO;
//import org.cdph.han.util.dto.ListTypeTO;
import org.cdph.han.util.services.*;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional; 

/**
 * Implementation of the ListTypeService, this implementation uses JPA to retrieve the data
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Repository
@Service ("listTypeService")
public class ListTypeServiceImpl implements ListTypeService {
	/** Persistence context */
	//private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.cdph.han.util.services.ListTypeService#loadListForType(java.lang.String)
	 */
	@Override
	//abey ...@Transactional (readOnly = true)
	public ListTypeTO[] loadListForType (final String type) {
		final List<ListTypeTO> foundValues = new ArrayList<ListTypeTO> ();
		
		/*
		final Query q = entityManager.createNamedQuery ("ListType.FindByType");
		q.setParameter ("type", type);
		final List<?> results = q.getResultList ();
		ListType listType;
		*/
		
		//TODO: ListService here
		/*
		 * com.liferay.portal.model.Contact.phone
		 * 1	Work Phone	com.liferay.portal.model.Contact.phone
2	Mobile Phone	com.liferay.portal.model.Contact.phone
3	Home Phone	com.liferay.portal.model.Contact.phone
4	TTY Phone	com.liferay.portal.model.Contact.phone
5	1-Way Pager	com.liferay.portal.model.Contact.phone
6	2-Way Pager	com.liferay.portal.model.Contact.phone
7	Numeric Pager	com.liferay.portal.model.Contact.phone
8	SMS	com.liferay.portal.model.Contact.phone
9	Fax	com.liferay.portal.model.Contact.phone
		
		 */
		
		/*
 *LIST TYPES
 * com.liferay.portal.model.Account.address
com.liferay.portal.model.Account.emailAddress
com.liferay.portal.model.Account.phone
com.liferay.portal.model.Account.website
com.liferay.portal.model.Contact.address
com.liferay.portal.model.Contact.emailAddress
com.liferay.portal.model.Contact.phone
com.liferay.portal.model.Contact.prefix
com.liferay.portal.model.Contact.suffix
com.liferay.portal.model.Contact.website
com.liferay.portal.model.Organization.address
com.liferay.portal.model.Organization.emailAddress
com.liferay.portal.model.Organization.phone
com.liferay.portal.model.Organization.service
com.liferay.portal.model.Organization.status
com.liferay.portal.model.Organization.website
		 */
		
		final List<ListTypeTO> results = new ArrayList<ListTypeTO>();
		ListTypeTO lto1 = new ListTypeTO();
		lto1.setId(1);
		lto1.setName("Work Phone");
		lto1.setType("com.liferay.portal.model.Contact.phone");
		results.add(lto1);
		ListTypeTO lto2 = new ListTypeTO();
		lto2.setId(2);
		lto2.setName("Mobile Phone");
		lto2.setType("com.liferay.portal.model.Contact.phone");
		results.add(lto2);
		ListTypeTO lto3 = new ListTypeTO();
		lto3.setId(3);
		lto3.setName("Home Phone");
		lto3.setType("com.liferay.portal.model.Contact.phone");
		results.add(lto3);
		ListTypeTO lto4 = new ListTypeTO();
		lto4.setId(4);
		lto4.setName("TTY Phone");
		lto4.setType("com.liferay.portal.model.Contact.phone");
		results.add(lto4);
		ListTypeTO lto5 = new ListTypeTO();
		lto5.setId(5);
		lto5.setName("Work Phone");
		lto5.setType("com.liferay.portal.model.Contact.phone");
		results.add(lto5);
		ListTypeTO lto6 = new ListTypeTO();
		lto6.setId(6);
		lto6.setName("1-Way Pager");
		lto6.setType("com.liferay.portal.model.Contact.phone");
		results.add(lto6);
		ListTypeTO lto7 = new ListTypeTO();
		lto7.setId(7);
		lto7.setName("2-Way Pager");
		lto7.setType("com.liferay.portal.model.Contact.phone");
		results.add(lto7);
		ListTypeTO lto8 = new ListTypeTO();
		lto8.setId(8);
		lto8.setName("SMS");
		lto8.setType("com.liferay.portal.model.Contact.phone");
		results.add(lto8);
		ListTypeTO lto9 = new ListTypeTO();
		lto9.setId(9);
		lto9.setName("Fax");
		lto9.setType("com.liferay.portal.model.Contact.phone");
		results.add(lto9);
		//final List<?> results =
		for (Object obj : results) {
			ListTypeTO listType = (ListTypeTO) obj;
			foundValues.add (new ListTypeTO (listType.getId (), listType.getName (), listType.getType ()));
		}
		return foundValues.toArray (new ListTypeTO[foundValues.size ()]);
	}

	/**
	 * @param entityManager the entityManager to set
	 */
/*	//@PersistenceContext
	public void setEntityManager (EntityManager entityManager) {
		this.entityManager = entityManager;
	}
*/
}