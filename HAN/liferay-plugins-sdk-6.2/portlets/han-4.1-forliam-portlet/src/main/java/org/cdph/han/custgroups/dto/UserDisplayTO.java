package org.cdph.han.custgroups.dto;

/**
 * Class representing an entry in the found users table.
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class UserDisplayTO implements Comparable<UserDisplayTO> {
	/** User ID */
	private long id;
	/** User First Name */
	private String firstName;
	/** User Last Name */
	private String lastName;
	/** User Organization Name */
	private String organization;
	/** User Role Name */
	private String role;
	/** Check for selected users */
	private boolean selected;
	/** The contact is not a HAN user */
	private boolean notHanUser;

	/**
	 * Empty constructor
	 */
	public UserDisplayTO () {}

	/**
	 * Initializes the object with the given parameters
	 * @param id users id
	 * @param firstName users first name
	 * @param lastName users last name
	 * @param organization users organization name
	 * @param role users role
	 */
	public UserDisplayTO (final long id, final String firstName, final String lastName,
			final String organization, final String role) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.organization = organization;
		this.role = role;
	}

	/**
	 * Initializes the object with the given parameters
	 * @param id users id
	 * @param firstName users first name
	 * @param lastName users last name
	 * @param organization users organization name
	 * @param role users role
	 */
	public UserDisplayTO (final long id, final String firstName, final String lastName,
			final String organization, final String role, final boolean notHanUser) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.organization = organization;
		this.role = role;
		this.notHanUser = notHanUser;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean equal = false;
		if (obj != null && obj instanceof UserDisplayTO) {
			UserDisplayTO other = (UserDisplayTO) obj;
			equal = other.getId () == id;
			equal = equal && !(other.isNotHanUser () ^ notHanUser);
			equal = equal && (other.getFirstName () != null ? other.getFirstName ().equals (firstName) : firstName == null);
			equal = equal && (other.getLastName () != null ? other.getLastName ().equals (lastName) : lastName == null);
			equal = equal && (other.getOrganization () != null ? other.getOrganization ().equals (organization) : organization == null);
			equal = equal && (other.getRole () != null ? other.getRole ().equals (role) : role == null);
		}
		return equal;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		return Long.valueOf (id).hashCode () + (firstName != null ? firstName.hashCode () : 0);
	}

	/**
	 * Compares two UserDisplayTO objects for sorting by last name.
	 */
	public int compareTo (final UserDisplayTO other) {
		int value = 0;
		if (!lastName.equals (other.getLastName ())) {
			value = lastName.compareTo (other.getLastName ());
		} else {
			if (!firstName.equals (other.getFirstName ())) {
				value = firstName.compareTo (other.getFirstName ());
			} else {
				value = organization.compareTo (other.getOrganization ());
			}
		}
		return value;
	}

	/**
	 * @return the id
	 */
	public long getId () {
		return id;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName () {
		return firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName () {
		return lastName;
	}

	/**
	 * @return the organization
	 */
	public String getOrganization () {
		return organization;
	}

	/**
	 * @return the role
	 */
	public String getRole () {
		return role;
	}

	/**
	 * @param id the id to set
	 */
	public void setId (long id) {
		this.id = id;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName (String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName (String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @param organization the organization to set
	 */
	public void setOrganization (String organization) {
		this.organization = organization;
	}

	/**
	 * @param role the role to set
	 */
	public void setRole (String role) {
		this.role = role;
	}

	/**
	 * @return the selected
	 */
	public boolean isSelected () {
		return selected;
	}

	/**
	 * @param selected the selected to set
	 */
	public void setSelected (boolean selected) {
		this.selected = selected;
	}

	/**
	 * @return the notHanUser
	 */
	public boolean isNotHanUser () {
		return notHanUser;
	}

	/**
	 * @param notHanUser the notHanUser to set
	 */
	public void setNotHanUser (boolean notHanUser) {
		this.notHanUser = notHanUser;
	}

}
