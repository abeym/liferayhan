/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.cdph.han.service.impl;

import java.util.List;

import org.cdph.han.model.Alert;
import org.cdph.han.model.impl.AlertImpl;
import org.cdph.han.service.AlertLocalServiceUtil;
import org.cdph.han.service.base.AlertLocalServiceBaseImpl;

import com.liferay.portal.kernel.bean.PortalBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.Order;
import com.liferay.portal.kernel.dao.orm.OrderFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.portlet.PortletClassLoaderUtil;
import com.liferay.util.dao.orm.CustomSQLUtil;

/**
 * The implementation of the alert local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link org.cdph.han.service.AlertLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author zanonil
 * @see org.cdph.han.service.base.AlertLocalServiceBaseImpl
 * @see org.cdph.han.service.AlertLocalServiceUtil
 */
public class AlertLocalServiceImpl extends AlertLocalServiceBaseImpl {

	final String SQL_ALL_BY_PUBLISH_DATE = "select * from Alerts a where a.date_Published is not null order by a.date_published desc";
	final String SQL_ALL_BY_TOPIC = "select * from Alerts a where a.date_Published is not null order by a.alert_topic_id, a.datePublished desc";
	final String SQL_ALL_BY_LEVEL = "select * from Alerts a where a.date_Published is not null order by a.severity, a.datePublished desc";
	final String SQL_SCHEDULED = "select * from Alert a where a.datePublished is null and a.readyToPublish = true order by a.dateScheduled desc, a.author.contact.firstName, a.author.contact.lastName, a.topic.title";

	public List<Alert> findPublishedByDate() {
		System.out.println("Run Dynamic query of " + Alert.class + " with order of date_published.");

		List<Alert> alertList = null;
		try {
		    DynamicQuery publishQuery = DynamicQueryFactoryUtil.forClass(
				Alert.class, PortletClassLoaderUtil.getClassLoader())
				.add(PropertyFactoryUtil.forName("datePublished").isNotNull());
		    Order order = OrderFactoryUtil.desc("datePublished");
		    publishQuery.addOrder(order);
			alertList = AlertLocalServiceUtil.dynamicQuery(publishQuery);
		} catch (SystemException e) {
			e.getStackTrace();
		} catch (Exception e) {
			e.getStackTrace();
		}
		return alertList;
	}
	
	public List<Alert> findPublishedByLevel() {
		System.out.println("Run Dynamic query of " + Alert.class + " with order of severity, date_published.");
		List<Alert> alertList = null;
		try {
		    DynamicQuery publishQuery = DynamicQueryFactoryUtil.forClass(
				Alert.class, PortletClassLoaderUtil.getClassLoader())
				.add(PropertyFactoryUtil.forName("datePublished").isNotNull());
		    Order levelOrder = OrderFactoryUtil.desc("severity");
		    publishQuery.addOrder(levelOrder);
		    Order pubOrder = OrderFactoryUtil.desc("datePublished");
		    publishQuery.addOrder(pubOrder);
			alertList = AlertLocalServiceUtil.dynamicQuery(publishQuery);
		} catch (SystemException e) {
		}
		return alertList;
	}		
	
	public List<Alert> findPublishedByTopic() {
		System.out.println("Run Dynamic query of " + Alert.class + " with order of topic, date_published.");
		List<Alert> alertList = null;
		try {
		    DynamicQuery publishQuery = DynamicQueryFactoryUtil.forClass(
				Alert.class, PortletClassLoaderUtil.getClassLoader())
				.add(PropertyFactoryUtil.forName("datePublished").isNotNull());
		    Order levelOrder = OrderFactoryUtil.desc("topicId");
		    publishQuery.addOrder(levelOrder);
		    Order pubOrder = OrderFactoryUtil.desc("datePublished");
		    publishQuery.addOrder(pubOrder);
			alertList = AlertLocalServiceUtil.dynamicQuery(publishQuery);
		} catch (SystemException e) {
		}
		return alertList;
	}
	
	public List<Alert> findScheduled() {
		// 	a.datePublished is null and a.readyToPublish = true
		// order by a.dateScheduled desc, a.author.contact.firstName, a.author.contact.lastName, a.topic.title";
		System.out.println("Run Dynamic query of " + Alert.class + " for ready to published.");
		List<Alert> alertList = null;
		try {
		    DynamicQuery publishQuery = DynamicQueryFactoryUtil.forClass(
				Alert.class, PortletClassLoaderUtil.getClassLoader())
				.add(PropertyFactoryUtil.forName("datePublished").isNotNull())
				.add(PropertyFactoryUtil.forName("readyToPublish").eq(1));
		    Order levelOrder = OrderFactoryUtil.desc("topicId");
		    publishQuery.addOrder(levelOrder);
		    Order pubOrder = OrderFactoryUtil.desc("datePublished");
		    publishQuery.addOrder(pubOrder);
			alertList = AlertLocalServiceUtil.dynamicQuery(publishQuery);
		} catch (SystemException e) {
		}
		return alertList;
	}

	public List<Alert> findDrafts() {
		// List<Alert> alerts = ;
		return null;
	}

	public List<Alert> findByAlertRecipients(long userId, long alertId) {
		List<Alert> alerts = null; 
		SessionFactory factory =  (SessionFactory)PortalBeanLocatorUtil.locate("liferaySessionFactory");
		Session session = null;
		try {
			System.out.println("Run> Alert.FindByAlertRecipient with User " + userId);
			session = factory.openSession();
			String sql = CustomSQLUtil.get("Alert.FindByAlertRecipient");
			System.out.println("Run>  " + sql);
			SQLQuery q = session.createSQLQuery(sql);
			q.setCacheable(false);
			q.addEntity("alerts", AlertImpl.class);

			System.out.println("Query>  " + q);
			QueryPos qPos = QueryPos.getInstance(q);
			qPos.add(alertId);
			qPos.add(userId);	qPos.add(userId);
			qPos.add(userId);	qPos.add(userId);
			System.out.println("Query Pos>  " + qPos.getPos());
			alerts = (List<Alert>) QueryUtil.list(q, factory.getDialect(), 1, QueryUtil.ALL_POS);
		} catch (Exception e) {
			System.err.println("ERROR Class not found: " + e);
		} finally {
			if (session != null) factory.closeSession(session);
		}
		return alerts;
	}
	
	public List<Alert> findByRecDate(long userId) {
		List<Alert> alerts = null; 
		SessionFactory factory =  (SessionFactory)PortalBeanLocatorUtil.locate("liferaySessionFactory");
		Session session = null;
		try {
			System.out.println("Run> Alert.FindByRecipient with User " + userId);
			session = factory.openSession();
			String sql = CustomSQLUtil.get("Alert.FindByRecipient");
			System.out.println("Run>  " + sql);
			SQLQuery q = session.createSQLQuery(sql);
			q.setCacheable(false);
			q.addEntity("alerts", AlertImpl.class);

			System.out.println("Query>  " + q);
			QueryPos qPos = QueryPos.getInstance(q);
			qPos.add(userId);	qPos.add(userId);
			qPos.add(userId);	qPos.add(userId);
			System.out.println("Query Pos>  " + qPos.getPos());
			alerts = (List<Alert>) QueryUtil.list(q, factory.getDialect(), 1, QueryUtil.ALL_POS);
		} catch (Exception e) {
			System.err.println("ERROR Class not found: " + e);
		} finally {
			if (session != null) factory.closeSession(session);
		}
		return alerts;
	}

	public List<Alert> findByRecLevel(long userId) {
		System.out.println("Run> " + SQL_ALL_BY_LEVEL);
		// List<Alert> alerts = ;
		return null;
	}

	public List<Alert> findByRecTopic(long userId) {
		System.out.println("Run> " + SQL_ALL_BY_TOPIC);
		// List<Alert> alerts = ;
		return null;
	}

	public List<Alert> findByRecAction(long userId) {
		List<Alert> alerts = null; 
		SessionFactory factory =  (SessionFactory)PortalBeanLocatorUtil.locate("liferaySessionFactory");
		Session session = null;
		try {
			System.out.println("Run> Alert.FindByReqAction with User " + userId);
			session = factory.openSession();
			String sql = CustomSQLUtil.get("Alert.FindByReqAction");
			System.out.println("Run>  " + sql);
			SQLQuery q = session.createSQLQuery(sql);
			q.setCacheable(false);
			q.addEntity("alerts", AlertImpl.class);

			System.out.println("Query>  " + q);
			QueryPos qPos = QueryPos.getInstance(q);
			qPos.add(userId);	qPos.add(userId);
			System.out.println("Query Pos>  " + qPos.getPos());
			alerts = (List<Alert>) QueryUtil.list(q, factory.getDialect(), 1, QueryUtil.ALL_POS);
		} catch (Exception e) {
			System.err.println("ERROR Class not found: " + e);
		} finally {
			if (session != null) factory.closeSession(session);
		}
		return alerts;
	}

	public List<Alert> findByRecIdAction(long userId, long alertId) {
		List<Alert> alerts = null; 
		SessionFactory factory =  (SessionFactory)PortalBeanLocatorUtil.locate("liferaySessionFactory");
		Session session = null;
		try {
			System.out.println("Run> Alert.FindByReqIdAction with User " + userId);
			session = factory.openSession();
			String sql = CustomSQLUtil.get("Alert.FindByIDReqAction");
			System.out.println("Run>  " + sql);
			SQLQuery q = session.createSQLQuery(sql);
			q.setCacheable(false);
			q.addEntity("alerts", AlertImpl.class);

			System.out.println("Query>  " + q);
			QueryPos qPos = QueryPos.getInstance(q);
			qPos.add(alertId);
			qPos.add(userId);	qPos.add(userId);
			System.out.println("Query Pos>  " + qPos.getPos());
			alerts = (List<Alert>) QueryUtil.list(q, factory.getDialect(), 1, QueryUtil.ALL_POS);
		} catch (Exception e) {
			System.err.println("ERROR Class not found: " + e);
		} finally {
			if (session != null) factory.closeSession(session);
		}
		return alerts;
	}

	public List<Alert> findByReqTopic(long userId) {
		List<Alert> alerts = null; 
		SessionFactory factory =  (SessionFactory)PortalBeanLocatorUtil.locate("liferaySessionFactory");
		Session session = null;
		try {
			System.out.println("Run> Alert.FindByReqTopic with User " + userId);
			session = factory.openSession();
			String sql = CustomSQLUtil.get("Alert.FindByReqTopic");
			System.out.println("Run>  " + sql);
			SQLQuery q = session.createSQLQuery(sql);
			q.setCacheable(false);
			q.addEntity("alerts", AlertImpl.class);

			System.out.println("Query>  " + q);
			QueryPos qPos = QueryPos.getInstance(q);
			qPos.add(userId);	qPos.add(userId);
			qPos.add(userId);	qPos.add(userId);
			System.out.println("Query Pos>  " + qPos.getPos());
			alerts = (List<Alert>) QueryUtil.list(q, factory.getDialect(), 1, QueryUtil.ALL_POS);
		} catch (Exception e) {
			System.err.println("ERROR Class not found: " + e);
		} finally {
			if (session != null) factory.closeSession(session);
		}
		return alerts;
	}
	
		
	
	
	
	
}