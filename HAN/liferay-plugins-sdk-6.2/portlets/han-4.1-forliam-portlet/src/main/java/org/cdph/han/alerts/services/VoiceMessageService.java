package org.cdph.han.alerts.services;

import java.io.Serializable;

/**
 * Service definition for retrieving recorded voice messages
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public interface VoiceMessageService extends Serializable {

	/**
	 * Retrieves the list of available recorded files in the mir3 site
	 * @return Array with the name of the recordings
	 */
	String[] recordedFiles ();

}
