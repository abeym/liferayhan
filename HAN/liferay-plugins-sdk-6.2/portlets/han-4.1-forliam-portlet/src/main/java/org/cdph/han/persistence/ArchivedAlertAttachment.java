package org.cdph.han.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
/**
 * Entity class representing an attachment of an archived alert
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Entity
@Table (name = "alert_attachments_archive", schema = "lportal")
public class ArchivedAlertAttachment implements Serializable, AlertAttachmentModel {
	/** Class serial version UID */
	private static final long serialVersionUID = 1751095792040335667L;
	/** Id of the attachment */
	@Id
	@Column (name = "id")
	@GeneratedValue (strategy = GenerationType.TABLE)
	private long id;
	/** Alert to which this attachment belongs */
	@ManyToOne
	@JoinColumn (name = "archived_alert_id", nullable = false)
	private ArchivedAlert archivedAlert;
	/** Byte array of the attachment */
	@Lob
	@Column (name = "attachment", nullable = false)
	private byte[] attachment;
	/** MIME type of the attachment */
	@Column (name = "mime_type", nullable = false, length = 100)
	private String mimeType;
	/** The attachment name */
	@Column (name = "name", nullable = false, length = 100)
	private String name;

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean equal = false;
		if (obj != null && obj instanceof ArchivedAlertAttachment) {
			ArchivedAlertAttachment other = (ArchivedAlertAttachment) obj;
			equal = other.getId () == id;
		}
		return equal;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		return Long.valueOf (id).hashCode ();
	}

	/**
	 * @return the id
	 */
	public long getId () {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId (long id) {
		this.id = id;
	}

	/**
	 * @return the alert
	 */
	public ArchivedAlert getArchivedAlert () {
		return archivedAlert;
	}

	/**
	 * @param alert the alert to set
	 */
	public void setArchivedAlert (ArchivedAlert archivedAlert) {
		this.archivedAlert = archivedAlert;
	}

	/**
	 * @return the attachment
	 */
	public byte[] getAttachment () {
		return attachment;
	}

	/**
	 * @param attachment the attachment to set
	 */
	public void setAttachment (byte[] attachment) {
		this.attachment = attachment;
	}

	/**
	 * @return the mimeType
	 */
	public String getMimeType () {
		return mimeType;
	}

	/**
	 * @param mimeType the mimeType to set
	 */
	public void setMimeType (String mimeType) {
		this.mimeType = mimeType;
	}

	/**
	 * @return the name
	 */
	public String getName () {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName (String name) {
		this.name = name;
	}

}
