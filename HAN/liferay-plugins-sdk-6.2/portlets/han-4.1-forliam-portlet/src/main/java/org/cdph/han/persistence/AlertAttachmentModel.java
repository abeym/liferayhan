package org.cdph.han.persistence;

public interface AlertAttachmentModel {

	/**
	 * @return the id
	 */
	long getId ();

	/**
	 * @return the attachment
	 */
	byte[] getAttachment ();

	/**
	 * @return the mimeType
	 */
	String getMimeType ();

	/**
	 * @return the name
	 */
	String getName ();

}