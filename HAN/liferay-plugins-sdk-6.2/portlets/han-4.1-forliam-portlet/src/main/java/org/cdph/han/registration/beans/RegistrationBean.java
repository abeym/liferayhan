package org.cdph.han.registration.beans;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIInput;
import javax.faces.component.UISelectOne;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.alerts.dto.FileTO;
import org.cdph.han.beans.BaseBean;
import org.cdph.han.facade.impl.email.EmailSenderServiceImpl;
import org.cdph.han.facade.impl.registration.RegistrationServiceImpl;
import org.cdph.han.facade.impl.util.HanPropertiesServiceImpl;
import org.cdph.han.service.HanUserDataLocalServiceUtil;
import org.cdph.han.service.RegistrationRequestLocalServiceUtil;
import org.cdph.han.synch.dto.Mir3UserTO;
import org.cdph.han.synch.services.UserSynchService;
import org.cdph.han.synch.services.UserSynchServiceImpl;
import org.cdph.han.persistence.AreaField;
import org.cdph.han.persistence.Organization;
import org.cdph.han.persistence.RegistrationRequestStatus;
import org.cdph.han.persistence.RequestForm;
import org.cdph.han.persistence.Role;
import org.cdph.han.registration.exception.EmailAlreadyExistsException;
import org.cdph.han.registration.service.RegistrationService;
import org.cdph.han.service.AreaFieldLocalServiceUtil;
import org.cdph.han.util.services.HanPropertiesService;
import org.cdph.han.util.ui.HanUtil;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;



public class RegistrationBean extends BaseBean{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5506716995992992968L;

	/** Class logger */
	private static Log log = LogFactory.getLog(RegistrationBean.class);

	private String cat;
	
	private String firstName;
	private String lastName;
	private String middleName;
	private String userName;
	private Long organizationType;
	private Long organization;
	private String organizationName; 
	private String organizationDesc;
	private String title;
	private String department;
	private Long areaField;
	private Long role;
	private String telephone;
	private String telephoneExt;
	private String email;
	private String email2;
	private String purpose;
	private String referralName;
	private String referralTitle;
	private String referralTelephone;
	private String referralTelephoneExt;
	
	private Long requestNumber;
	
	private SelectItem[] organizationTypes;
	private SelectItem[] organizations;
	private SelectItem[] areaFields;
	private SelectItem[] roles;
	
	private String userNameCheckMessage;
	private String userNameCheckStyle;
	
	private String emailCheckMessage;
	private String emailCheckStyle;
	
	private boolean otherOrganization;
	private String orgTypeDesc;
	private String orgDesc;
	private String areaFieldDesc;
	private String roleDesc;
	
	private boolean popUpRendered = false;

	private UISelectOne organizationTypeComp;
	private UIInput firstNameComp;
	private UIInput lastNameComp;
	private UIInput middleNameComp;
	private UIInput emailComp;
	private UIInput emailComp2;
	
	@Autowired
	private ModelMapper modelMapper;
		
	//abey...
	@Autowired
	private RegistrationService service;	
	
	
	public SelectItem[] getOrganizationTypes(){
		service = gauranteeService();
		List<Organization> orgTypes = service.getOrganizationTypes();
		ArrayList<SelectItem> items = new ArrayList<SelectItem>();
		
		//items.add(new SelectItem(-1,""));
		for (Organization organization : orgTypes) {
			//items.add(new SelectItem(organization.getOrganizationId(),
					//organization.getName()));
			items.add(new SelectItem(String.valueOf(organization.getOrganizationId()),
					organization.getName()));
		}
		SelectItem[] itemsUI = new SelectItem[items.size()];
	/*	itemsUI=(SelectItem[]) items.toArray();
		//itemsUI=Arrays.asList(items);
		for (int i = 0; i < orgTypes.size(); i++) {
			Organization organization = orgTypes.get(i);
			itemsUI[i] = new SelectItem(String.valueOf(organization.getOrganizationId()),
					organization.getName());
			}
		
		*/
		 items = new ArrayList<SelectItem>();
		
		for (Organization organization : orgTypes) {
			items.add(new SelectItem(String.valueOf(organization.getOrganizationId()),
					organization.getName()));
		}
		//items.add(new SelectItem(-1, "Other"));
		itemsUI = new SelectItem[items.size()];
		this.organizationTypes = items.toArray(itemsUI);
		
		
		return itemsUI;
		
	}
	
	public SelectItem[] getOrganizations(){
		return organizations;
		
	}
	
	/**
     * Value change listener for the organization type change event.
     *organizationtype
     * @param event value change event
     */

    public void orgTypeChanged(ValueChangeEvent event) {//orgType
    	log.debug("In orgTypeChanged()... " + event);
    	/*log.debug("firstNameComp:"+firstNameComp.getValue());
    	log.debug("lastNameComp:"+lastNameComp.getValue());
    	log.debug("lastNameComp:"+lastNameComp.getValue());*/
    	//PhaseId phaseId = event.getPhaseId();
//    	long oldValue = (Long) event.getOldValue();
//    	long newValue = (Long) event.getNewValue();
//       	log.debug("old: " + oldValue + ", new " + newValue);
    //	if (phaseId.equals(PhaseId.ANY_PHASE))
    	//{//this.formSubmitButtonAction();
    	//event.setPhaseId(PhaseId.UPDATE_MODEL_VALUES);
    	//event.queue();
    	//} else if (phaseId.equals(PhaseId.INVOKE_APPLICATION)) {
    	service = gauranteeService();
        Long newOrgTypeID = Long.parseLong(event.getNewValue().toString());
        log.debug("Getting info for Org ID: " + newOrgTypeID);
        
        if(newOrgTypeID == -1){
        	return;
        }else{
        	this.setOrganizationType(newOrgTypeID);
        	this.setOrgTypeDesc(newOrgTypeID.toString());
        	//OrganizationTypeLocalServiceutil.
        }
    	
        //Get Organizations
    	List<Organization> orgsTmp = service.getSuborganizations(newOrgTypeID);
		List<SelectItem> items = HanUtil.getSelectList();
		
		for (Organization organization : orgsTmp) {
			items.add(new SelectItem(organization.getOrganizationId(),
					organization.getName()));
		}
		items.add(new SelectItem(-1, "Other"));
		SelectItem[] itemsUI = new SelectItem[items.size()];
		organizations = items.toArray(itemsUI);
		log.debug("Getting info for areaFields for newOrgTypeID: " + newOrgTypeID);
		//Get AreaFields
		List<AreaField> areaFieldsTmp = service.getAreaFields(newOrgTypeID);
		log.debug("areaFieldsTmp for newOrgTypeID: " + areaFieldsTmp);
		if (areaFieldsTmp == null) {
			log.debug("Retrieved areaFields: is null.");
		} else {
			log.info("Retrieved areaFields: " + areaFieldsTmp.size());
			for (AreaField areaField : areaFieldsTmp) {
				log.info("areaField> " + areaField.getOrganization() + " - " + areaField.getDescription());
			}
		}
		items = HanUtil.getSelectList();
		
		for (AreaField areaField : areaFieldsTmp) {
			items.add(new SelectItem(areaField.getAreaFieldId(),
					areaField.getDescription()));
		}
		itemsUI = new SelectItem[items.size()];
		areaFields = items.toArray(itemsUI);
		log.debug("getOrganizationalRoles for newOrgTypeID: " + newOrgTypeID);
		//Get Roles
		List<Role> rolesTmp = service.getOrganizationalRoles(newOrgTypeID);
		items = HanUtil.getSelectList();
		log.debug("rolesTmp: " + rolesTmp);
		for (Role role : rolesTmp) {
			items.add(new SelectItem(role.getRoleId(),
					role.getName()));
		}
		itemsUI = new SelectItem[items.size()];
		roles = items.toArray(itemsUI);
    	//}
    }
    
    /**
     * Value change listener for the organization change event.
     *
     * @param event value change event
     */
    public void organizationChanged(ValueChangeEvent event) {
    	log.debug("In organizationChanged()...");
        
        Long newOrgID = Long.parseLong(event.getNewValue().toString());
        
        if(newOrgID == -1){
        	//Enable Organization Name and Description
        	otherOrganization = true;
        	popUpRendered = true;
        }else{
        	otherOrganization = false;
        	organizationName = null;
        	organizationDesc = null;
        	popUpRendered = false;
        }
    }

    /*public void organizationChanged(ValueChangeEvent event) {
    	log.debug("In organizationChanged()... " + event);
    	//PhaseId phaseId = event.getPhaseId();
        Long newOrgID = Long.parseLong(event.getNewValue().toString());
       com.liferay.portal.model.Organization  lorg=null;
        try {
        	lorg=	OrganizationLocalServiceUtil.getOrganization(newOrgID);
        	this.setOrganizationDesc(lorg.getName());
        	this.setOrganizationName(lorg.getName());
		} catch (PortalException e) {
			// 
			log.error(e);
		} catch (Exception e) {
			// 
			log.error(e);
		}
        this.setOrganization(newOrgID);
        this.setOrganizationDesc(organizationDesc);
        if(newOrgID == -1){
        	//Enable Organization Name and Description
        	otherOrganization = true;
        	popUpRendered = true;
        	organizationName = null;
        	organizationDesc = null;
        }else{
        	otherOrganization = false;
        	popUpRendered = false;
        }
    }*/
    
   public void  areaFieldChanged(ValueChangeEvent event) {
   	log.debug("In areaFieldChanged()... " + event);
	   try{
		   Object o=event.getComponent().getValueBinding("areaField");
		   Long newAfId = Long.parseLong(event.getNewValue().toString());
		   org.cdph.han.model.AreaField af = AreaFieldLocalServiceUtil.getAreaField(newAfId);
		   AreaField haf = new AreaField();
		   modelMapper.map(af, haf);
		   this.setAreaField(newAfId);
		   this.setAreaFieldDesc(af.getDescription());
	   }catch(Exception e){
		   log.error(e);
	   }
   }
    
    /**
     * Action for the form submit button click action.
     *
     * @param e click action event.
     */
    public String formSubmitButtonAction() {
    	log.debug("In formSubmitButtonAction...");

    	if(organizationType == -1){
    		addFacesMessage(organizationTypeComp.getClientId(FacesContext.getCurrentInstance()),
    				"registration.request.validation.organization.type.missing");
    		return "";
    	}
    	
    	emailCheckMessage = "";
		emailCheckStyle = "";
    	if(!email.equals(email2)){
    		addFacesMessage(emailComp2.getClientId(FacesContext.getCurrentInstance()),
				"registration.request.validation.email.confirmation");
    		return "";
    	}
    	
    	if(emailExist(email)){    		
    		addFacesMessage(emailComp.getClientId(FacesContext.getCurrentInstance()),
    				"registration.request.validation.email.exists");
    		return "";
    	}
    	
    	if(otherOrganization && 
    			(organizationName == null || organizationName.trim().equals("") ||
    			 organizationDesc == null || organizationDesc.trim().equals(""))){
    		popUpRendered = true;
    		return "";
    	}

    	if((this.getOrganization()!=null) && (this.getOrganization().longValue() != -1)){
    		this.setOrganizationName(service.getOrganization(organization).getName());
    	}

    	orgTypeDesc = service.getOrganization(organizationType).getName();

    	if(organization == -1){
    		orgDesc = "Other";
    	}else{
    		orgDesc= service.getOrganization(organization).getName();
    	}

    	areaFieldDesc = service.getAreaField(areaField.intValue()).getDescription();
    	roleDesc = service.getRole(role).getName();
    	
    	//this.setOrganizationType(organizationType);
    	/*this.setOrgTypeDesc(this.getOrganizationType().toString());
    	
    	this.setAreaFieldDesc(this.getAreaField().toString());
    	this.setRoleDesc(this.getRole().toString());*/
    	
    	return "confirm";
    }
    
    /**
     * Action for the form cancel button click action.
     * @param e click action event.
     *
     */
    public String formCancelButtonAction() {

    	cleanUp();
    	
    	return "back";
    }
    
    /**
     * Action for the form submit button click action.
     *
     * @param e click action event.
     */
    public String confirmSubmitButtonAction() {
    	log.debug("In confirmSubmitButtonAction...");
    	String outcome = "success";
    	    	    	
    	try{
    		RequestForm requestForm = new RequestForm();
	    	
	    	requestForm.setFirstName(firstName);
	    	requestForm.setLastName(lastName);
	    	requestForm.setMiddleName(middleName);
	    	
	    	//Organization type
	    	Organization orgType = new Organization();
	    	orgType.setOrganizationId(organizationType);
	    	this.setOrganizationType(organizationType);
	    	this.setOrgTypeDesc(organizationType.toString());
	    	requestForm.setOrganizationType(orgType);
	    	
	    	//Organization
	    	if(organization != -1){
	    		//
	    		//com.liferay.portal.model.Organization lorg = OrganizationLocalServiceUtil.getOrganization(organization);
	    		Organization org = new Organization();
		    	org.setOrganizationId(organization);
		    	/*org.setName(lorg.getName());
		    	this.setOrganizationDesc(lorg.getName());
		    	this.setOrganizationName(lorg.getName());*/
		    	requestForm.setOrganization(org);
	    	}
	    	  	

	    	requestForm.setOrganizationName(organizationName);
	    	requestForm.setOrganizationDescription(organizationDesc);
	    	requestForm.setTitle(title);
	    	requestForm.setDepartment(department);	 
	    	
	    	//AreaField
	    	AreaField af = new AreaField();
	    	//TODO: lookup areField
	    	af.setAreaFieldId(areaField.intValue());
	    	af.setDescription(areaField.toString());
	    	requestForm.setAreaField(af);
	    	
	    	//Role
	    	Role rl = new Role();
	    	rl.setRoleId(role);
	    	requestForm.setRole(rl);
	    	
	    	requestForm.setWorkTelephone(telephone);
	    	requestForm.setWorkTelephoneExt(telephoneExt);
	    	requestForm.setEmail(email);

	    	requestForm.setReferralPurpose(purpose);
	    	requestForm.setReferralName(referralName);
	    	requestForm.setReferralTitle(referralTitle);
	    	requestForm.setReferralTelephone(referralTelephone);
	    	requestForm.setReferralTelephoneExt(referralTelephoneExt);
	    	
	    	requestForm.setStatus(RegistrationRequestStatus.SUBMITTED);
	    	
	    	service = gauranteeService();
	    	requestNumber = service.registerRequest(requestForm);
	    	log.debug("Request registration #" + requestNumber);
	    	service.enqueueNotificationEmail(requestNumber);
	    	
	    	cleanUp();
	    	
    	}catch (EmailAlreadyExistsException eaex) {
    		log.error(eaex.getMessage(), eaex);
    		addFacesMessage(emailComp.getClientId(FacesContext.getCurrentInstance()),
				"registration.request.validation.email.exists");
    		outcome = "";
		}catch(Exception ex){
    		log.error(ex.getMessage(), ex);
    		addFacesMessageNoBundle(ex.getMessage());
    		outcome = "";
    	}
    	return outcome;
    }
    
    public String confirmSubmitButtonActionBak() {
    	log.debug("In confirmSubmitButtonAction...");
    	String outcome = "success";
    	    	    	
    	try{
    		RequestForm requestForm = new RequestForm();
	    	
	    	requestForm.setFirstName(firstName);
	    	requestForm.setLastName(lastName);
	    	requestForm.setMiddleName(middleName);
	    	
	    	//Organization type
	    	Organization orgType = new Organization();
	    	orgType.setOrganizationId(organizationType);
	    	requestForm.setOrganizationType(orgType);
	    	
	    	//Organization
	    	if(organization != -1){
	    		Organization org = new Organization();
		    	org.setOrganizationId(organization);
		    	requestForm.setOrganization(org);
	    	}
	    		    	
	    	requestForm.setOrganizationName(organizationName);
	    	requestForm.setOrganizationDescription(organizationDesc);
	    	requestForm.setTitle(title);
	    	requestForm.setDepartment(department);	 
	    	
	    	//AreaField
	    	AreaField af = new AreaField();
	    	af.setAreaFieldId(areaField.intValue());
	    	requestForm.setAreaField(af);
	    	
	    	//Role
	    	Role rl = new Role();
	    	rl.setRoleId(role);
	    	requestForm.setRole(rl);
	    	
	    	requestForm.setWorkTelephone(telephone);
	    	requestForm.setWorkTelephoneExt(telephoneExt);
	    	requestForm.setEmail(email);

	    	requestForm.setReferralPurpose(purpose);
	    	requestForm.setReferralName(referralName);
	    	requestForm.setReferralTitle(referralTitle);
	    	requestForm.setReferralTelephone(referralTelephone);
	    	requestForm.setReferralTelephoneExt(referralTelephoneExt);
	    	
	    	requestForm.setStatus(RegistrationRequestStatus.SUBMITTED);
	    	
	    	requestNumber = service.registerRequest(requestForm);
	    	log.debug("Request registration #" + requestNumber + ":" + requestForm);
	    	service.enqueueNotificationEmail(requestNumber);
	    	
	    	cleanUp();
	    	
    	}catch (EmailAlreadyExistsException eaex) {
    		log.error(eaex.getMessage(), eaex);
    		addFacesMessage(emailComp.getClientId(FacesContext.getCurrentInstance()),
				"registration.request.validation.email.exists");
    		outcome = "";
		}catch(Exception ex){
    		log.error(ex.getMessage(), ex);
    		addFacesMessageNoBundle(ex.getMessage());
    		outcome = "";
    	}
    	return outcome;
    }
    
    //Dead code?
    public void userNameValueChangeListener(ValueChangeEvent vce){
    	log.debug("In userNameValueChangeListener...");
    	
    	String newValue = vce.getNewValue().toString();
    	log.debug("New Value: " + newValue);
    	if(newValue == null || newValue.trim().equals("")){
    		userNameCheckMessage = "";
    		userNameCheckStyle = "";
    		return;
    	}
    	
    	usernameExist(newValue);   	
    	
    }
    
    public void formFirstNameValueChangeListener(ValueChangeEvent vce){
    	log.debug("In userNameValueChangeListener...");
    	
    	String newValue = vce.getNewValue().toString();
    	log.debug("New Value: " + newValue);
    	if(newValue == null || newValue.trim().equals("")){
    		userNameCheckMessage = "";
    		userNameCheckStyle = "";
    		return;
    	}else{
    		this.setFirstName(newValue);
    	}
    	 	
    	
    }
    
    public void emailValueChangeListener(ValueChangeEvent vce){
    	log.debug("In userNameValueChangeListener...");    	
    	
    	if(vce.getNewValue() == null ){
    		emailCheckMessage = "";
    		emailCheckStyle = "";
    		return;
    	}
    	
    	String newValue = vce.getNewValue().toString();
    	
    	if (newValue.trim().equals("")){
    		emailCheckMessage = "";
    		emailCheckStyle = "";
    		return;
    	}   	

    	boolean exists = emailExist(newValue);
    	log.debug("Email exists? " + exists);
    	
    	if(exists){
    		addFacesMessage(vce.getComponent().getClientId(FacesContext.getCurrentInstance()),
				"registration.request.validation.email.exists");
    	}else{
    		emailCheckMessage = getBundleMessage("registration.request.validation.email.available");
    		emailCheckStyle = "color:blue; font-weight:bold;";
    	}
    	
    }
    
    public void emailConfirmationValueChangeListener(ValueChangeEvent vce){
    	log.debug("In emailConfirmationValueChangeListener...");
    	        	
    	if( vce.getNewValue() == null ){
    		return;
    	}
    	
    	String newValue = vce.getNewValue().toString();
    	
    	if (newValue.trim().equals("")){
    		return;
    	}  
    	
    	log.debug("Comparing: " + newValue + " == " +  email);
    	
    	if(!newValue.equals(email)){
    		addFacesMessage(vce.getComponent().getClientId(FacesContext.getCurrentInstance()),
    				"registration.request.validation.email.confirmation");
    	}
    	
    }

    public RegistrationService gauranteeService(){
    	return service = service==null?new RegistrationServiceImpl():service;
    }
    
    private boolean usernameExist(String userId){
    	/*long companyId = JSFPortletUtil.getCompanyId(
    			FacesContext.getCurrentInstance());
    	
    			
    			FacesContext facesContext = FacesContext.getCurrentInstance();
			ExternalContext externalContext = facesContext.getExternalContext();
			PortletRequest portletRequest = (PortletRequest) externalContext.getRequest();
			long companyId = PortalUtil.getCompanyId(portletRequest);
    			*
    			*/
    	service = gauranteeService();
    	ResourceBundle bundle = ResourceBundle.getBundle("messages",
				FacesContext.getCurrentInstance().getViewRoot().getLocale());	
    	
    	boolean flag = true;
    	
    	try {
			flag = service.usernameExist(userId);
		} catch (Exception ex) {
			userNameCheckMessage = ex.getMessage();
			log.error(ex.getMessage(),ex);
		} 
		
		if(flag){
			userNameCheckMessage = bundle
				.getString("registration.request.validation.username.exists");
			userNameCheckStyle = "color:red; font-weight:bold;";
		}else{
			
			userNameCheckMessage = bundle
				.getString("registration.request.validation.username.available");
			userNameCheckStyle = "color:blue; font-weight:bold;";
		}			
		
    	log.debug("Username exists? " + flag);
    	return flag;
    }
    
    /*private boolean emailExist(String email){
		////emailSenderService.sendAdminEmail (new long[]{user.getUserId ()}, new long[]{},
				//content, subject, attachments);
    	
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ExternalContext externalContext = facesContext.getExternalContext();
		PortletRequest portletRequest = (PortletRequest) externalContext.getRequest();
		long companyId = PortalUtil.getCompanyId(portletRequest);
    	//UserLocalServiceUtil.fetchUserByEmailAddress(companyId, email);
    	RegistrationRequestLocalServiceUtil.emailExist(email);
    	
   /* 	try{
    		
    		//TODO: remove this test for mail services
    		long[] uid=new long[]{14143l};
    		long[] dunno=new long[]{14143l};
    		String content="";
    		String subject="";
    		List<FileTO> attachments = null;  		
    		EmailSenderServiceImpl emailSenderService = new EmailSenderServiceImpl();
    		log.debug(" emailExist emailSenderService.sendAdminEmail: uid[0]: "+uid[0]);
    		//emailSenderService.sendAdminEmail(uid, dunno, content, subject, attachments);
    
    	}catch(Exception e){
    		log.error("emailSenderService.sendAdminEmail error: "+e);
    	}
    	
    	*/
    	/*boolean flag = true;
    	    	
    	try {
			flag = gauranteeService().emailExist(email);
		} catch (Exception ex) {
			addFacesMessageNoBundle(ex.getMessage());
		} 
    	return flag;
    }*/

    private boolean emailExist(String email){
    	
    	boolean flag = true;
    	    	
    	try {
			flag = service.emailExist(email);
		} catch (Exception ex) {
			addFacesMessageNoBundle(ex.getMessage());
		} 
    	return flag;
    }    


	public void setService(RegistrationService service) {
		this.service = service;
	}

	public String getFirstName() {
		//log.debug("getFirstName:"+firstName);
		return firstName;
	}

	public void setFirstName(String firstName) {
		//log.debug("setFirstName:"+firstName);
		this.firstName = firstName;
	}

	public String getLastName() {
		//log.debug("getLastName:"+lastName);
		return lastName;
	}

	public void setLastName(String lastName) {
		//log.debug("setLastName:"+lastName);
		this.lastName = lastName;
	}

	public String getMiddleName() {
		//log.debug("getMiddleName:"+middleName);
		return middleName;
	}

	public void setMiddleName(String middleName) {
		//log.debug("setMiddleName:"+middleName);
		this.middleName = middleName;
	}

	public String getUserName() {
		//log.debug("getUserName:"+userName);
		return userName;
	}

	public void setUserName(String userName) {
		//log.debug("setUserName:"+userName);
		this.userName = userName;
	}

	public Long getOrganizationType() {
		//log.debug("getOrganizationType:"+organizationType);
		return organizationType;
	}

	public void setOrganizationType(Long organizationType) {
		//log.debug("setOrganizationType:"+organizationType);
		this.organizationType = organizationType;
	}

	public Long getOrganization() {
		//log.debug("getOrganization:"+organization);
		return organization;
	}

	public void setOrganization(Long organization) {
		//log.debug("setOrganization:"+organization);
		this.organization = organization;
	}

	public String getOrganizationName() {
		//log.debug("getOrganizationName:"+organizationName);
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		//log.debug("setOrganizationName:"+organizationName);
		this.organizationName = organizationName;
	}

	public String getOrganizationDesc() {
		//log.debug("getOrganizationDesc:"+organizationDesc);
		return organizationDesc;
	}

	public void setOrganizationDesc(String organizationDesc) {
		//log.debug("setOrganizationDesc:"+organizationDesc);
		this.organizationDesc = organizationDesc;
	}

	public String getTitle() {
		//log.debug("getTitle:"+title);
		return title;
	}

	public void setTitle(String title) {
		//log.debug("setTitle:"+title);
		this.title = title;
	}

	public String getDepartment() {
		//log.debug("getDepartment:"+department);
		return department;
	}

	public void setDepartment(String department) {
		log.debug("setDepartment:"+department);
		this.department = department;
	}

	public Long getAreaField() {
		//log.debug("getAreaField:"+areaField);
		return areaField;
	}

	public void setAreaField(Long areaField) {
	//	log.debug("setAreaField:"+areaField);
		this.areaField = areaField;
	}

	public Long getRole() {
		return role;
	}

	public void setRole(Long role) {
		this.role = role;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public String getReferralName() {
		return referralName;
	}

	public void setReferralName(String referralName) {
		this.referralName = referralName;
	}

	public String getReferralTitle() {
		return referralTitle;
	}

	public void setReferralTitle(String referralTitle) {
		this.referralTitle = referralTitle;
	}

	public String getReferralTelephone() {
		return referralTelephone;
	}

	public void setReferralTelephone(String referralTelephone) {
		this.referralTelephone = referralTelephone;
	}

	public SelectItem[] getAreaFields() {
		return areaFields;
	}

	public SelectItem[] getRoles() {
		return roles;
	}

	public String getUserNameCheckMessage() {
		return userNameCheckMessage;
	}

	public void setUserCheckNameMessage(String userNameCheckMessage) {
		this.userNameCheckMessage = userNameCheckMessage;
	}

	public String getUserNameCheckStyle() {
		return userNameCheckStyle;
	}

	public void setUserCheckNameStyle(String userNameCheckStyle) {
		this.userNameCheckStyle = userNameCheckStyle;
	}

	public Long getRequestNumber() {
		return requestNumber;
	}

	public void setRequestNumber(Long requestNumber) {
		this.requestNumber = requestNumber;
	}

	public String getTelephoneExt() {
		return telephoneExt;
	}

	public void setTelephoneExt(String telephoneExt) {
		this.telephoneExt = telephoneExt;
	}

	public String getReferralTelephoneExt() {
		return referralTelephoneExt;
	}

	public void setReferralTelephoneExt(String referralTelephoneExt) {
		this.referralTelephoneExt = referralTelephoneExt;
	}

	public boolean isOtherOrganization() {
		return otherOrganization;
	}

	public void setOtherOrganization(boolean otherOrganization) {
		this.otherOrganization = otherOrganization;
	}

	public String getOrgTypeDesc() {
		return orgTypeDesc;
	}

	public void setOrgTypeDesc(String orgTypeDesc) {
		this.orgTypeDesc = orgTypeDesc;
	}

	public String getOrgDesc() {
		return orgDesc;
	}

	public void setOrgDesc(String orgDesc) {
		this.orgDesc = orgDesc;
	}

	public String getAreaFieldDesc() {
		return areaFieldDesc;
	}

	public void setAreaFieldDesc(String areaFieldDesc) {
		this.areaFieldDesc = areaFieldDesc;
	}

	public String getRoleDesc() {
		return roleDesc;
	}

	public void setRoleDesc(String roleDesc) {
		this.roleDesc = roleDesc;
	}
	
	/**
	 * Cleans the beans fields
	 */
	private void cleanUp(){
		firstName = null;
		lastName = null;
		middleName = null;
		userName = null;
		organizationType = null;
		organization = null;
		organizationName = null;
		organizationDesc = null;
		title = null;
		department = null;
		areaField = null;
		role = null;
		telephone = null;
		telephoneExt = null;
		email = null;
		email2 = null;
		purpose = null;
		referralName = null;
		referralTitle = null;
		referralTelephone = null;
		referralTelephoneExt = null;
		
		//requestNumber = null;
		
		organizations = null;
		areaFields = null;
		roles = null;
		
		userNameCheckMessage = null;
		userNameCheckStyle = null;
		
		emailCheckMessage = null;
		emailCheckStyle = null;
		
		otherOrganization = false;
		orgTypeDesc = null;
		orgDesc = null;
		areaFieldDesc = null;
		roleDesc = null;
	}

	public String getEmailCheckMessage() {
		return emailCheckMessage;
	}

	public void setEmailCheckMessage(String emailCheckMessage) {
		this.emailCheckMessage = emailCheckMessage;
	}

	public String getEmailCheckStyle() {
		return emailCheckStyle;
	}

	public void setEmailCheckStyle(String emailCheckStyle) {
		this.emailCheckStyle = emailCheckStyle;
	}

	public String getEmail2() {
		return email2;
	}

	public void setEmail2(String email2) {
		this.email2 = email2;
	}
	
	public void togglePopUp(ActionEvent event) {
		log.debug("togglePopUp:" + popUpRendered + ":" + event);
		popUpRendered = !popUpRendered;
    }
	
	public void togglePopUp() {
		log.debug("togglePopUp:" + popUpRendered + ";" );
		popUpRendered = !popUpRendered;
    }

	public boolean getPopUpRendered() {
        return popUpRendered;
    }

	public UISelectOne getOrganizationTypeComp() {
		return organizationTypeComp;
	}

	public void setOrganizationTypeComp(UISelectOne organizationTypeComp) {
		this.organizationTypeComp = organizationTypeComp;
	}

	public UIInput getEmailComp() {
		return emailComp;
	}

	public void setEmailComp(UIInput emailComp) {
		this.emailComp = emailComp;
	}
	
	public UIInput getEmailComp2() {
		return emailComp2;
	}

	public void setEmailComp2(UIInput emailComp) {
		this.emailComp2 = emailComp;
	}

	public String getCat() {
		return cat;
	}

	public void setCat(String cat) {
		this.cat = cat;
	}

	public UIInput getFirstNameComp() {
		return firstNameComp;
	}

	public void setFirstNameComp(UIInput firstNameComp) {
		this.firstNameComp = firstNameComp;
	}

	public UIInput getLastNameComp() {
		return lastNameComp;
	}

	public void setLastNameComp(UIInput lastNameComp) {
		this.lastNameComp = lastNameComp;
	}

	public UIInput getMiddleNameComp() {
		return middleNameComp;
	}

	public void setMiddleNameComp(UIInput middleNameComp) {
		this.middleNameComp = middleNameComp;
	}

/*	public void setOrganizationTypes(SelectItem[] organizationTypes) {
		this.organizationTypes = organizationTypes;
	}*/
}
