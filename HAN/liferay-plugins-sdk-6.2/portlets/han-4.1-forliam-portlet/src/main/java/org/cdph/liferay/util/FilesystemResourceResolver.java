package org.cdph.liferay.util;

import java.net.MalformedURLException;
import java.net.URL;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.facelets.ResourceResolver;
/**
 * 
 * @author abeym
 *
 */
public class FilesystemResourceResolver extends ResourceResolver {
	public URL resolveUrl(String s) {
		try {
			ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
			/*System.out.println("1:"+ec.getContext());
			System.out.println("2:"+ec.getRealPath(s));
			System.out.println("3:"+ec.getResource(s));
			String dir = ec.getRequestContextPath();*/
			return ec.getResource(s);
			//return new URL("file", "",ec.getResource(s));
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		}
	}

}
