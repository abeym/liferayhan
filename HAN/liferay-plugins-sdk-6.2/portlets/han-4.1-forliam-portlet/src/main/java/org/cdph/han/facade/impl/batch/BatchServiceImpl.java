package org.cdph.han.facade.impl.batch;


import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.EntityManager;
import javax.persistence.NamedQuery;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.batch.BatchException;
import org.cdph.han.batch.dao.NonHanUserDao;
import org.cdph.han.batch.service.*;
import org.cdph.han.batch.validator.BatchUploadLineValidator;
import org.cdph.han.persistence.BatchStatus;
import org.cdph.han.persistence.ContactDeviceType;
import org.cdph.han.persistence.NonHanUser;
import org.cdph.han.persistence.NonHanUserContactDevice;
import org.cdph.han.persistence.NonHanUserGroup;
import org.cdph.han.persistence.UserData;
import org.cdph.han.service.ClpSerializer;
import org.cdph.han.service.NonHanUserGroupLocalServiceUtil;
import org.cdph.han.service.NonHanUserLocalServiceUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Service("batchService")
public class BatchServiceImpl implements BatchService {

	/** Class logger */
	private static Log log = LogFactory.getLog(BatchServiceImpl.class);
	
	private final static String NON_HAN_USER_SEQUENCE_NAME = "non_han_user";
	private final static String NON_HAN_USER_CONTACT_DEV_SEQUENCE_NAME = "non_han_user_contact_device";

	private EntityManager em;

	//abey..
	//@Autowired
	//@Qualifier("batch")
	private JmsTemplate jmsTemplate;
	
	//abey..
	//@Autowired
	private NonHanUserDao nonHanuserDao;

	private Pattern fieldPattern = Pattern
			.compile("(\"((\\w|\\-|\\.|\\@|\\u0020)+)\")");

	private Matcher fieldMatcher;

	//abey commenting...
	//@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	/**
	 * Queries by ID for a Non Han User
	 * 
	 * @param userId
	 *            user identifier
	 * @return UserData
	 */
	public UserData getUserDataById(long userId) {
		log.debug("In createNonHanGroup(NonHanUserGroup group)...");
		return (UserData) em.find(UserData.class, userId);
	}

	//abey ...@Transactional
	/*
	 * Creates a new Non Han User Group in DB
	 * 
	 * @param group to create in DB
	 */
	public void createNonHanGroup(NonHanUserGroup group) {
		log.debug("In createNonHanGroup(NonHanUserGroup group)...");
		if (group.getUser() != null) {
			group.setUser((UserData) em.find(UserData.class, group.getUser()
					.getUserId()));
		}
		em.persist(group);
	}
	
	//abey ...@Transactional(propagation=Propagation.REQUIRES_NEW)
	/*
	 * Updates the non han user group
	 * 
	 * @param group to update
	 */
	public void updateNonHanGroup(NonHanUserGroup group) {
		log.debug("In updateNonHanGroup(NonHanUserGroup group)...");
		em.merge(group);
	}
	

	//abey ...@Transactional
	/**
	 * Soft deletes a Non Han User group. This means the record stays on DB
	 * but the status changes to DELETE.
	 */
	public void softDeleteNonHanGroup(NonHanUserGroup group) {
		group.setStatus(BatchStatus.SOFT_DELETED);
		updateNonHanGroup(group);
		
	}

	//abey ...@Transactional(readOnly = true)
	/*
	 * Gets a non han user group by id
	 * 
	 * @param nonHanGroupId id of the group to search for
	 * 
	 * @return list with all the non han user groups
	 */
	public NonHanUserGroup getNonHanUserGroup(Long nonHanGroupId) {
		log.debug("In getNonHanUserGroup(" + nonHanGroupId + ")...");
		return em.find(NonHanUserGroup.class, nonHanGroupId);
	}

	@SuppressWarnings("unchecked")
	//abey ...@Transactional(readOnly = true)
	/**
	 * Gets all the non han users that belong to a certain group.
	 * @param nonHanGroupId the id of the batch group
	 * @return collection of non han users
	 */
	public List<NonHanUser> getNonHanUserGroupMembers(Long nonHanGroupId) {
		log.debug("In getNonHanUserGroupMembers(" + nonHanGroupId + ")...");
		
		return em.createNamedQuery("NonHanUser.FindByGroupId")
				.setParameter("groupId", nonHanGroupId)
				.getResultList();

	}
	
	//abey ...@Transactional(readOnly = true)
	/**
	 * Gets the count of non han users that belong to a certain group.
	 * @param nonHanGroupId the id of the batch group
	 * @return count of non han users
	 */
	public Integer getNonHanUserGroupMembersCount(Long nonHanGroupId) {
		if(log.isDebugEnabled()){
			log.debug("In getNonHanUserGroupMembersCount(" + nonHanGroupId + ")...");
		}
		
		return ((Number) em.createNamedQuery("NonHanUser.GroupMembersCount")
				.setParameter("groupId", nonHanGroupId)
				.getSingleResult()).intValue();

	}	
	
	@SuppressWarnings("unchecked")
	//abey ...@Transactional(readOnly = true)
	/**
	 * Paginated method that getse non han users that belong to a certain group.
	 * @param nonHanGroupId the id of the batch group
	 * @return collection of non han users
	 */
	public List<NonHanUser> getNonHanUserGroupMembers(Long nonHanGroupId, int startIndex, int maxResults) {
		if(log.isDebugEnabled()){
			log.debug("In getNonHanUserGroupMembers(" + nonHanGroupId + ", " + startIndex + ", " + maxResults + ")...");
		}
		
		return em.createNamedQuery("NonHanUser.FindByGroupId")
			.setParameter("groupId", nonHanGroupId)
			.setFirstResult(startIndex)
			.setMaxResults(maxResults)
			.getResultList();

	}

	@SuppressWarnings("unchecked")
	//abey ...@Transactional(readOnly = true)
	/*
	 * Gets all the non Han User Groups
	 * 
	 * @return list with all the non han user groups
	 */
	public List<NonHanUserGroup> getAllNonHanUserGroup() {
		log.debug("In getAllNonHanUserGroup()...");
		return em.createNamedQuery("BatchGroup.FindAllExceptSoftDeleted")
				.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	//abey ...@Transactional(readOnly = true)
	/*
	 * Gets the Non Han User Groups (paginated)
	 * 
	 * @return list with all the non han user groups
	 */
	public List<NonHanUserGroup> getNonHanUserGroups(int startIndex, int maxResults){
		
		log.debug("In getNonHanUserGroups(" + startIndex + ", " + maxResults + ")...");
		return em.createNamedQuery("BatchGroup.FindAllExceptSoftDeleted")
				.setFirstResult(startIndex)
				.setMaxResults(maxResults)
				.getResultList();
		
	}

	@SuppressWarnings("unchecked")
	//abey ...@Transactional(readOnly = true)
	/*
	 * Searches for non Han User Groups specifying the criteria through the
	 * parameters
	 * 
	 * @param groupName the non han user group name
	 * 
	 * @param groupDescription the non han user group description
	 * 
	 * @return list with the non han user groups matching the criteria
	 */
	public List<NonHanUserGroup> searchNonHanUserGroup(String groupName,
			String groupDescription) {
		log.debug("In searchNonHanUserGroup()...");
		return em
				.createNamedQuery("BatchGroup.FindByNameDescAndStatus")
				.setParameter("groupName", "%" + groupName + "%")
				.setParameter(
						"groupDesc", "%" + groupDescription + "%")
				.setParameter("status", BatchStatus.SOFT_DELETED)
				.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	//abey ...@Transactional(readOnly = true)
	/*
	 * Searches for non Han User Groups specifying the criteria through the
	 * parameters. Paginates.
	 * 
	 * @param groupName the non han user group name 
	 * @param groupDescription the non han user group description 
	 * @param startIndex where to start fetching the first result
	 * @param maxResults max results returned
	 * 
	 * @return list with the non han user groups matching the criteria
	 */
	public List<NonHanUserGroup> searchNonHanUserGroup(String groupName,
			String groupDescription, int startIndex, int maxResults) {
		log.debug("In searchNonHanUserGroup()...");
		return em.createNamedQuery("BatchGroup.FindByNameDescAndStatus")
				.setParameter("groupName", "%" + groupName + "%")
				.setParameter("groupDesc", "%" + groupDescription + "%")
				.setParameter("status", BatchStatus.SOFT_DELETED)
				.setFirstResult(startIndex)
				.setMaxResults(maxResults)
				.getResultList();
	}
	
	//abey ...@Transactional(readOnly = true)
	/**
	 * Gets the count of non han user groups
	 * @return count of non han user groups
	 */
	public Integer getNonHanUserGroupCount(){
		
		
		try {
			ClassLoader classLoader = (ClassLoader)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),"portletClassLoader");
			DynamicQuery query = DynamicQueryFactoryUtil.forClass(org.cdph.han.model.NonHanUserGroup.class, classLoader)
					.add(RestrictionsFactoryUtil.ne("status", 4))
					.setProjection(ProjectionFactoryUtil.count("status"));
			return (int) NonHanUserGroupLocalServiceUtil.dynamicQueryCount(query);
		} catch (SystemException e) {
			log.error(e);
		}
		return 0;
		//NonHanUserLocalServiceUtil.getNonHanUsersCount();
	}
	
	@SuppressWarnings("unchecked")
	//abey ...@Transactional(readOnly = true)
	/*
	 * Obtains the count for non Han User Groups specifying the criteria through the
	 * parameters
	 * 
	 * @param groupName the non han user group name
	 * 
	 * @param groupDescription the non han user group description
	 * 
	 * @return count of non han user groups matching the criteria
	 */
	public Integer getNonHanUserGroupCount(String groupName,
			String groupDescription) {
		log.debug("In getNonHanUserGroupCount()...");
		return ((Number)em.createNamedQuery("BatchGroup.FindByNameDescAndStatusCount")
				.setParameter("groupName", "%" + groupName + "%")
				.setParameter(
						"groupDesc", "%" + groupDescription + "%")
				.setParameter("status", BatchStatus.SOFT_DELETED)
				.getSingleResult()).intValue();
		
	}
	
	

	//abey ...@Transactional
	/*
	 * Creates the non Han User Group and publish a message to the upload
	 * processing Queue for Async processing
	 * 
	 * @param group with non han user group information
	 */
	public void registerUploadBatchRequest(NonHanUserGroup group)
			throws BatchException {
		log.debug("In registerUploadBatchRequest...");
		createNonHanGroup(group);

	}

	//abey ...@Transactional(propagation=Propagation.REQUIRES_NEW, rollbackFor={BatchException.class, RuntimeException.class})
	/*
	 * Processes a Non HAN User Group. This method reads the uploaded file from
	 * the location specified, parses, validates and batch updates the Non HAN
	 * group, user and contact tables.
	 * 
	 * @param group batch group uploaded data
	 * 
	 * @throws BatchException in case of error
	 */
	public void processUploadBatch(NonHanUserGroup group) throws BatchException {
		// exceptions
		log.debug("processUploadBatch...");
	
		FileReader fr = null;
		BufferedReader br = null;
		String line = null;
		NonHanUser user = null;
		List<NonHanUser> users = new ArrayList<NonHanUser>();
		
		StringBuffer errorDesc = new StringBuffer();
		int lineNumber = 0;
		
		Integer WORK_PHONE_PRIORITY = null;
		Integer WORK_EXT_PRIORITY = new Integer(-1);
		Integer WORK_EMAIL_PRIORITY = null;
		Integer CELL_PHONE_PRIORITY = null;
		Integer HOME_PHONE_PRIORITY = null;
		Integer PERSONAL_EMAIL_PRIORITY = null;

		try {
			 
			StringReader sr = new StringReader(new String(group.getFile()));
			br = new BufferedReader(sr);
			
			//Setting the priority of each device
			StringTokenizer prefSt = new StringTokenizer(group.getContactPreferences(), ",");			
			
			int prefCounter = 1;
			while(prefSt.hasMoreTokens()){
				//Personal EMail,Home Phone,Work EMail,Work Phone,Cell Phone
				String pref = prefSt.nextToken();
				if(pref.equals("Work Phone")){
					WORK_PHONE_PRIORITY = new Integer(prefCounter);
				}
				if(pref.equals("Work EMail")){
					WORK_EMAIL_PRIORITY = new Integer(prefCounter);
				}
				if(pref.equals("Cell Phone")){
					CELL_PHONE_PRIORITY = new Integer(prefCounter);
				}
				if(pref.equals("Home Phone")){
					HOME_PHONE_PRIORITY = new Integer(prefCounter);
				}
				if(pref.equals("Personal EMail")){
					PERSONAL_EMAIL_PRIORITY = new Integer(prefCounter);
				}
				
				prefCounter++;
			}

			// First Line
			errorDesc.append("Reading first line (header)...\n");
			lineNumber++;
			line = br.readLine();
			errorDesc.append("Finish reading first line...\n");
			log.debug("First Line of file:\n" + line);

			// Rest of the file
			log.debug("Reading rest of file...");
			errorDesc.append("Reading rest of file...\n");
			
			long startTime = System.currentTimeMillis();
			while ((line = br.readLine()) != null) {
				lineNumber++;
				
				BatchUploadLineValidator.validateLine(line);
				
				// Parse the line
				String[] fieldArray = line.split("\\t",-1);
				//StringTokenizer st = new StringTokenizer(line, "\t");
				
				user = new NonHanUser();
				user.setId(getNextSequenceId(NON_HAN_USER_SEQUENCE_NAME));
				user.setNonHanGroup(group);
				
				/*
				 * "Last Name" "First Name" "Middle Initial" "Work Phone"
				 * "Work Extension" "Work E-mail" "Cell Phone" "Address Line 1"
				 * "Address Line 2" "City" "State" "Zip" "Home Telephone"
				 * "Personal E-mail"
				 */
				user.setLastName(removeQuotes(fieldArray[0]));
				user.setFirstName(removeQuotes(fieldArray[1]));
				
				if(!isEmpty(fieldArray[2])){
					user.setMiddleInitial(removeQuotes(fieldArray[2]));
				}
				
				
				if(!isEmpty(fieldArray[3])){
					user.getNonHanContactDevices().add(
							new NonHanUserContactDevice(
									getNextSequenceId(NON_HAN_USER_CONTACT_DEV_SEQUENCE_NAME),
									removeQuotes(fieldArray[3]),
									ContactDeviceType.WORK_PHONE, user,
									WORK_PHONE_PRIORITY));
				}
				if(!isEmpty(fieldArray[4])){
					user.getNonHanContactDevices().add(
							new NonHanUserContactDevice(
									getNextSequenceId(NON_HAN_USER_CONTACT_DEV_SEQUENCE_NAME),
									removeQuotes(fieldArray[4]),
									ContactDeviceType.WORK_EXT, user, 
									WORK_EXT_PRIORITY));
				}
				if(!isEmpty(fieldArray[5])){
					user.getNonHanContactDevices().add(
							new NonHanUserContactDevice(
									getNextSequenceId(NON_HAN_USER_CONTACT_DEV_SEQUENCE_NAME),
									removeQuotes(fieldArray[5]),
									ContactDeviceType.WORK_EMAIL, user,
									WORK_EMAIL_PRIORITY));
				}
				if(!isEmpty(fieldArray[6])){
					user.getNonHanContactDevices().add(
							new NonHanUserContactDevice(
									getNextSequenceId(NON_HAN_USER_CONTACT_DEV_SEQUENCE_NAME),
									removeQuotes(fieldArray[6]),
									ContactDeviceType.CELL_PHONE, user,
									CELL_PHONE_PRIORITY));
				}
				if(!isEmpty(fieldArray[7])){
					user.setAddressLine1(removeQuotes(fieldArray[7]));
				}
				if(!isEmpty(fieldArray[8])){
					user.setAddressLine2(removeQuotes(fieldArray[8]));
				}
				if(!isEmpty(fieldArray[9])){
					user.setCity(removeQuotes(fieldArray[9]));
				}
				if(!isEmpty(fieldArray[10])){
					user.setState(removeQuotes(fieldArray[10]));
				}
				if(!isEmpty(fieldArray[11])){
					user.setZip(removeQuotes(fieldArray[11]));
				}
				
				if(!isEmpty(fieldArray[12])){
					user.getNonHanContactDevices().add(
							new NonHanUserContactDevice(
									getNextSequenceId(NON_HAN_USER_CONTACT_DEV_SEQUENCE_NAME),
									removeQuotes(fieldArray[12]),
									ContactDeviceType.HOME_PHONE, user,
									HOME_PHONE_PRIORITY));
				}
				if(!isEmpty(fieldArray[13])){
					user.getNonHanContactDevices().add(
							new NonHanUserContactDevice(
									getNextSequenceId(NON_HAN_USER_CONTACT_DEV_SEQUENCE_NAME),
									removeQuotes(fieldArray[13]),
									ContactDeviceType.PERSONAL_EMAIL, user,
									PERSONAL_EMAIL_PRIORITY));
				}

				// Add user to collection
				users.add(user);
			}
			
			if(lineNumber <= 1){
				throw new BatchException("The file only included the header. No records were found.");
			}
			
			long endTime = System.currentTimeMillis();
			
			if (log.isDebugEnabled()) {
				log.debug("Parsing of file and user collection done. Time: "
						+ (endTime - startTime) + " ms");
			}
			errorDesc.append("Parsing of file and user collection done. Time: "
						+ (endTime - startTime) + " ms\n");
			
			errorDesc.append("Starting Batch update to DB\n");
			nonHanuserDao.batchUpdate(users);
			errorDesc.append("Finished Batch update to DB\n");
		} catch (Exception ex) {
			log.warn(ex.getMessage(), ex);
			errorDesc.append("Line: " + lineNumber + ": " + ex.getMessage());
			throw new BatchException(errorDesc.toString());
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException ioe) {
					log.warn(ioe.getMessage());
				}
			}
			if (fr != null) {
				try {
					fr.close();
				} catch (IOException ioe) {
					log.warn(ioe.getMessage());
				}
			}
		}
	}	
	
	private boolean isEmpty(String value){
		return (value == null || value.trim().length() == 0);
	}
	
	//abey ...@Transactional
	/**
	 * Method needed to obtain primary keys.
	 * @param sequence_name in the hibernate_sequences DB table
	 * @return next id for that sequence_name
	 */
	public Long getNextSequenceId(String sequenceName){
		return nonHanuserDao.getNextSequenceID(sequenceName);
	}

	/**
	 * Strips the tab delimited file field from the enclosing quotes (in case
	 * the field has double quotes).
	 * 
	 * @param field
	 *            the record field
	 * @return String without the quotes
	 */
	private String removeQuotes(String field) {
		fieldMatcher = fieldPattern.matcher(field);
		if (fieldMatcher.find()) {
			return fieldMatcher.group(2);
		}
		return field;
	}

	//abey ...@Transactional
	/**
	 * Publishes a message to the uploadBatch processing Queue
	 * 
	 * @param groupId batch upload group id
	 * @throws BatchException
	 *             in case of error
	 */
	public void enqueueBatchUploadRequest(Long groupId) throws BatchException {
		log.debug("publishtoQueue...");
		if (groupId == null ) {
			throw new BatchException("In order to send the JMS message, the "
					+ "non han user group must not be null.");
		}		
		jmsTemplate.convertAndSend(groupId);
	}
}
