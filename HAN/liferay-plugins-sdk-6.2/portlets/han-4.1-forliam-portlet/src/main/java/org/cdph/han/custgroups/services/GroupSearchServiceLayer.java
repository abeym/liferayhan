package org.cdph.han.custgroups.services;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.custgroups.dto.GroupDisplayTO;
import org.cdph.han.custgroups.dto.GroupTO;

public class GroupSearchServiceLayer implements GroupSearchService {
	private Log log = LogFactory.getLog(GroupSearchServiceLayer.class);

	@Override
	public GroupDisplayTO[] searchMyGroups(long userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isNameAvailable(long userId, String name) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public GroupDisplayTO[] searchUserGroups(String name) {
		log.debug("Search user groups (layer): " + name);
		return null;
	}

	@Override
	public GroupTO loadGroup(long id) {
		// TODO Auto-generated method stub
		return null;
	}

}
