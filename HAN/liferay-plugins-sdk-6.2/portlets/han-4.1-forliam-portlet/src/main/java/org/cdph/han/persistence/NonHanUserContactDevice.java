package org.cdph.han.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "non_han_user_contact_device", schema = "lportal")
public class NonHanUserContactDevice implements Serializable {

	private static final long serialVersionUID = 6146267706183333427L;

	/** Primary Key */
	@Id
	@Column(name = "id", nullable = false)
	@GeneratedValue(strategy = GenerationType.TABLE)
	private Long id;
	/** The user contact device value (email,telephone, ext) */
	@Column(name = "contactDevice", nullable = false, length = 100)
	private String contactDevice;
	/** The user contact type */
	@Column(name = "contacType", nullable = false )
	private ContactDeviceType contactType;
	
	/** The user contact type */
	@Column(name = "priority", nullable = false )
	private Integer priority;

	/** Non-Han User */
	@ManyToOne
	@JoinColumn(name = "non_han_user_id")
	private NonHanUser nonHanUser;
	
	public NonHanUserContactDevice() {}
	
	public NonHanUserContactDevice(Long id , String contactDevice, ContactDeviceType contactType, NonHanUser nonHanUser, Integer priority ) {
		this(contactDevice, contactType, nonHanUser ,priority);
		this.id = id;		
	}

	public NonHanUserContactDevice(String contactDevice, ContactDeviceType contactType, NonHanUser nonHanUser, Integer priority ) {
		this.contactDevice = contactDevice;
		this.contactType = contactType;
		this.nonHanUser = nonHanUser;
		this.priority = priority;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getContactDevice() {
		return contactDevice;
	}

	public void setContactDevice(String contactDevice) {
		this.contactDevice = contactDevice;
	}

	public NonHanUser getNonHanUser() {
		return nonHanUser;
	}

	public void setNonHanUser(NonHanUser nonHanUser) {
		this.nonHanUser = nonHanUser;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public ContactDeviceType getContactType() {
		return contactType;
	}

	public void setContactType(ContactDeviceType contactType) {
		this.contactType = contactType;
	}

}
