/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.cdph.han.service.impl;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.portlet.PortletClassLoaderUtil;

import java.util.List;

import org.cdph.han.model.ArchivedAlertOption;
import org.cdph.han.service.ArchivedAlertOptionLocalServiceUtil;
import org.cdph.han.service.base.ArchivedAlertOptionLocalServiceBaseImpl;

/**
 * The implementation of the archived alert option local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link org.cdph.han.service.ArchivedAlertOptionLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author zanonil
 * @see org.cdph.han.service.base.ArchivedAlertOptionLocalServiceBaseImpl
 * @see org.cdph.han.service.ArchivedAlertOptionLocalServiceUtil
 */
public class ArchivedAlertOptionLocalServiceImpl
	extends ArchivedAlertOptionLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link org.cdph.han.service.ArchivedAlertOptionLocalServiceUtil} to access the archived alert option local service.
	 */
	public List<ArchivedAlertOption> getAlertOptionsByAlert(long alertId) {
		List<ArchivedAlertOption> alertReportList = null;
		try {
		    DynamicQuery query = DynamicQueryFactoryUtil.forClass(
		    	ArchivedAlertOption.class, PortletClassLoaderUtil.getClassLoader())
				.add(PropertyFactoryUtil.forName("alertId").eq(alertId));
			alertReportList = ArchivedAlertOptionLocalServiceUtil.dynamicQuery(query);
		} catch (SystemException e) {
		}
		return alertReportList;
	}
}