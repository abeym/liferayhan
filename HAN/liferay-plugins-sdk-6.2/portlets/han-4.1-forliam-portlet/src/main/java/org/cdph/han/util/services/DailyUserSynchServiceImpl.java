package org.cdph.han.util.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.model.HanUserData;
import org.cdph.han.persistence.Address;
import org.cdph.han.persistence.AlternateContact;
import org.cdph.han.persistence.ContactPreference;
import org.cdph.han.persistence.EmailAddress;
import org.cdph.han.persistence.ListType;
import org.cdph.han.persistence.Organization;
import org.cdph.han.persistence.PhoneDevice;
import org.cdph.han.persistence.UserData;
import org.cdph.han.persistence.UserOrganizationalData;
import org.cdph.han.synch.dto.Mir3DeviceTO;
import org.cdph.han.synch.dto.Mir3RecipientResponse;
import org.cdph.han.synch.dto.Mir3UserTO;
import org.cdph.han.synch.services.UserSynchService;
import org.cdph.han.util.converters.UserDataConv;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.cdph.han.service.HanUserDataLocalServiceUtil;
import org.cdph.han.service.UserDataLocalServiceUtil;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;

/**
 * Implementation of the user synchronization service
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Repository
@Service ("dailyUserSynchService")
public class DailyUserSynchServiceImpl implements DailyUserSynchService {
	/** Class log */
	private static Log log = LogFactory.getLog (DailyUserSynchService.class);
	/** Persistence context */
	private EntityManager entityManager;
	/** Service to synchronize users */
	//abey commneting to deploy
	//@Autowired
	private UserSynchService userSynchService;
	/** Max length for custom fields */
	private static final int MAX_LENGTH = 100;
	/** Security Service */
	//abey commneting to deploy
	//@Autowired
	private SecurityService securityService;
	/** User limit */
	private int MAX_USER_COUNT = 20;
	/** Service to retrieve the mir3 user credentials */
	//abey commneting to deploy
	//@Autowired
	private HanPropertiesService hanPropertiesService;

	/* (non-Javadoc)
	 * @see org.cdph.han.util.services.DailyUserSynchService#synchronizeUsers(java.util.Date)
	 */
	@Override
	@SuppressWarnings ("unchecked")
	//abey ...@Transactional (propagation = Propagation.REQUIRED)
	public void synchronizeUsers (final Date fromDate) {
		log.info ("Synchronizing users.");
		/*final Query query = entityManager.createNamedQuery ("UserData.FindModifiedIds");
		query.setParameter ("fromDate", fromDate);
		final Query deleteQuery = entityManager.createNativeQuery (
				"UPDATE han_user_data SET mir3_id = null WHERE userid = " +
				"(SELECT userId FROM User_ WHERE emailAddress = :emailAddress)");
		final List<UserData> users = query.getResultList ();*/
		List<org.cdph.han.model.HanUserData> hudusers;
		try {
				hudusers = HanUserDataLocalServiceUtil.getHanUserDatas(QueryUtil.ALL_POS, QueryUtil.ALL_POS);		
				
				final List<UserData> users = new ArrayList<UserData>();
				
				for(org.cdph.han.model.HanUserData hud:hudusers){	
					try{
					User lu = UserLocalServiceUtil.fetchUser(hud.getUserId());
					Date d = lu.getModifiedDate();
					if(d.after(fromDate)){
						UserDataConv udc = new UserDataConv();
						UserData ud = udc.buildUpUser(hud);
						users.add(ud);
					}
					}catch(Exception e){
						log.warn(e);
					}
				}
		
				final List<Mir3UserTO> usersToUpdate = new ArrayList<Mir3UserTO> ();
				final List<String> usersToDelete = new ArrayList<String> ();
				final List<Mir3RecipientResponse> responses = new ArrayList<Mir3RecipientResponse> ();
				final List<Mir3RecipientResponse> deleteResponses = new ArrayList<Mir3RecipientResponse> ();
				int userCount = 0;
				int deleteCount = 0;
				for (UserData userData : users) {
					if (userData.isActive ()) {
						usersToUpdate.add (prepareUserForSynch (userData));
						++userCount;
						if (userCount == MAX_USER_COUNT) {
							//abey - commenting for compilation
							//
							responses.addAll (userSynchService.updateRecipients (usersToUpdate,
									hanPropertiesService.getValue ("MIR3_ADMIN_USER"),
									hanPropertiesService.getValue ("MIR3_ADMIN_PASS"),
									hanPropertiesService.getValue ("MIR3_API_VERSION"),
									true));
							userCount = 0;
							usersToUpdate.clear ();
						}
					} else {
						//deleteQuery.setParameter ("emailAddress", userData.getEmail ());
						//deleteQuery.executeUpdate ();
						HanUserData hanUserData = HanUserDataLocalServiceUtil.getHanUserData(userData.getUserId());
						hanUserData.setMir3_id(0);
						HanUserDataLocalServiceUtil.updateHanUserData(hanUserData);
						usersToDelete.add (userData.getEmail ());
						++deleteCount;
						if (deleteCount == MAX_USER_COUNT) {
							//abey - commenting for compilation
							//
							deleteResponses.addAll (userSynchService.deleteRecipients (usersToDelete,
									hanPropertiesService.getValue ("MIR3_ADMIN_USER"),
									hanPropertiesService.getValue ("MIR3_ADMIN_PASS"),
									hanPropertiesService.getValue ("MIR3_API_VERSION")));
							deleteCount = 0;
							usersToDelete.clear ();
							
						}
					}
				}
				if (!usersToUpdate.isEmpty ()) {
					//abey - commenting for compilation
					//
					
					responses.addAll (userSynchService.updateRecipients (usersToUpdate,
							hanPropertiesService.getValue ("MIR3_ADMIN_USER"),
							hanPropertiesService.getValue ("MIR3_ADMIN_PASS"),
							hanPropertiesService.getValue ("MIR3_API_VERSION"),
							true));
							
				}
				if (!responses.isEmpty ()) {
					final Query q = entityManager.createQuery ("Select u From UserData u Where u.email like :email");
					final Query updateQ = entityManager.createNativeQuery (
							"UPDATE han_user_data SET mir3_id = :mir3Id WHERE userid = :userId");
					UserData toUpdate;
					for (Mir3RecipientResponse response : responses) {
						if (response.getUserId () > 0) {
							//abey - commenting for compilation
							//
							
							log.error ("Updating user: " + response.getUsername ());
							q.setParameter ("email", response.getUsername ());
							toUpdate = (UserData) q.getSingleResult ();
							if (toUpdate.getMir3Id () == null || !toUpdate.getMir3Id ().equals (response.getUserId ())) {
								updateQ.setParameter ("userId", toUpdate.getUserId ());
								updateQ.setParameter ("mir3Id", response.getUserId ());
								updateQ.executeUpdate ();
							}
							
						} else {
							//abey - commenting for compilation
							//
							log.error ("Error updating user: " + response.getUsername ());
							log.error ("Error Code: " + response.getErrorCode () +
									" Error Message: " + response.getErrorMessage ());
						}
					}
					//entityManager.flush ();
				}
				if (!deleteResponses.isEmpty ()) {
					for (Mir3RecipientResponse response : deleteResponses) {
						//abey - commenting for compilation
						//
		/*				if (response.getErrorCode () != null) {
							log.error ("Error deleting user: " + response.getUsername ());
							log.error ("Error Code: " + response.getErrorCode () +
									" Error Message: " + response.getErrorMessage ());
						}
		*/			}
				}
		} catch (Exception e) {
			log.error(e);
		}
	}

	public void ensureServices(){
		securityService= securityService==null?new SecurityServiceImpl():securityService;
	}
	/**
	 * Prepares the user information to be sent for update at mir3
	 * @param user with the user information
	 * @return the object to be used for updating the mir3 entry
	 */
	@SuppressWarnings("unchecked")
	private Mir3UserTO prepareUserForSynch (final UserData user) {
		log.info ("Synchronizing User: " + user.getContact ().getFirstName () +
				" " + user.getContact ().getLastName ());
		final Mir3UserTO userTO = new Mir3UserTO ();
		userTO.setUsername (user.getEmail ());
		userTO.setFirstName (user.getContact ().getFirstName ());
		userTO.setLastName (user.getContact ().getLastName ());
		userTO.setLanguage ("ENGLISH");
		userTO.setTimezone ("CENTRAL_USA");
		userTO.setTitle (user.getContact ().getJobTitle ());
		userTO.setJobTitle (user.getContact ().getJobTitle ());
		userTO.setDepartment (user.getDepartment ());
		ensureServices();
		userTO.setMir3Role (securityService.getMir3UserRole (user.getUserId ()));
		if (user.getMir3Id () != null) {
			//abey - commenting for compilation
			//userTO.setMir3Id (user.getMir3Id ());
		}
		final Query q = entityManager.createNamedQuery ("Address.findByClassPK");
		q.setParameter ("classNameId", 14005L);
		q.setParameter ("classPK", user.getContact ().getContactId ());
		final List<Address> addresses = q.getResultList ();
		// Set the user address
		if (addresses != null && !addresses.isEmpty ()) {
			Address address = addresses.get (0);
			userTO.setAddress1 (address.getStreet1 ());
			userTO.setAddress2 (address.getStreet2 ());
			userTO.setCity (address.getCity ());
			userTO.setCountry ("USA");
			userTO.setFloor (address.getFloor ());
			if (address.getRegion () != null) {
				userTO.setState (address.getRegion ().getName ());
			}
			userTO.setZip (address.getZip ());
		}
		// Set the user alternates
		userTO.setAlternates (new ArrayList<String> ());
		for (AlternateContact contact : user.getAlternateContacts ()) {
			userTO.getAlternates ().add (contact.getAlternateContact ().getEmail ());
		}

		// Set the organizational data
		final StringBuilder organization = new StringBuilder ();
		final StringBuilder areaField = new StringBuilder ();
		final StringBuilder role = new StringBuilder ();
		final StringBuilder orgType = new StringBuilder ();
		for (UserOrganizationalData data : user.getUserOrganizationalData ()) {
			organization.append (data.getOrganization ().getName ()).append (',');
			areaField.append (data.getAreaField ().getDescription ()).append (',');
			role.append (data.getRole ().getName ()).append (',');
			orgType.append (entityManager.find (Organization.class,
					data.getOrganization ().getParentOrganizationId ()).getName ()).append (',');
		}
		// Check that none of the values exceeds the 100 length limit
		userTO.setRole (role.toString ().length () > MAX_LENGTH ?
				role.toString ().substring (0, MAX_LENGTH) : role.toString ().substring (0, role.length () - 1));
		userTO.setOrganization (organization.toString ().length () > MAX_LENGTH ?
				organization.toString ().substring (0, MAX_LENGTH) :
					organization.toString ().substring (0, organization.length () - 1));
		userTO.setOrganizationType (orgType.toString ().length () > MAX_LENGTH ?
				orgType.toString ().substring (0, MAX_LENGTH) :
					orgType.toString ().substring (0, orgType.length () - 1));
		userTO.setAreaField (areaField.toString ().length () > MAX_LENGTH ?
				areaField.toString ().substring (0, MAX_LENGTH) :
					areaField.toString ().substring (0, areaField.length () - 1));
		userTO.setDivision ("/");
		userTO.setDepartment (user.getDepartment ());
		userTO.setTelephonyId (user.getTelephonyId ());
		userTO.setPassword (user.getTelephonyPassword ());
		userTO.setPin (user.getTelephonyPIN ());
		if (user.getMir3Id () != null) {
			//abey - commenting for compilation
			//userTO.setMir3Id (user.getMir3Id ());
		}
		userTO.setDevices (new ArrayList<Mir3DeviceTO> ());
		addDevices (userTO.getDevices (), user.getEmail (), user.getContact ().getEmailAddresses (),
				user.getContact ().getPhoneDevices (), user.getContactPreferences ());
		return userTO;
	}

	/**
	 * Returns the index of the phone device in the devices list
	 * @param phoneDevice to search
	 * @param devices list to search
	 * @return the index in the list of the phone device
	 */
	private int findPhone (final PhoneDevice phoneDevice, final Collection<ContactPreference> preferences) {
		int index = -1;
		for (ContactPreference preference : preferences) {
			if (preference.getPhoneDevice () != null && preference.getPhoneDevice ().equals (phoneDevice)) {
				index = preference.getIndex () + 1;
				break;
			}
		}
		return index;
	}

	/**
	 * Search for the index in the given list of the email address
	 * @param emailAddress to search
	 * @param devices list to search into
	 * @return index in the list
	 */
	private int findEmail (final EmailAddress emailAddress, final Collection<ContactPreference> preferences) {
		int index = -1;
		for (ContactPreference preference : preferences) {
			if (preference.getEmailAddress () != null && preference.getEmailAddress ().equals (emailAddress)) {
				index = preference.getIndex () + 1;
				break;
			}
		}
		return index;
	}

	/**
	 * Adds all the available devices into the user object
	 * @param devices the list to add the devices
	 * @param emailAddresses the email addresses the user has configured
	 * @param phoneDevices the phone devices the user has configured
	 */
	private void addDevices (final List<Mir3DeviceTO> devices, final String emailAddress,
			final Collection<EmailAddress> emailAddresses, final Collection<PhoneDevice> phoneDevices,
			final Collection<ContactPreference> preferences) {
		Mir3DeviceTO device = new Mir3DeviceTO ();
		device.setAddress (emailAddress);
		device.setDescription ("Primary Work Email");
		device.setType ("Work Email");
		int priority = getPrimaryEmailPriority (preferences);
		if (priority > 0) {
			device.setPriority (String.valueOf (priority));
		} else {
			device.setPriority ("OFF");
		}
		devices.add (device);
		for (PhoneDevice phone : phoneDevices) {
			device = new Mir3DeviceTO ();
			priority = findPhone (phone, preferences);
			if (priority > 0) {
				device.setPriority (String.valueOf (priority));
			} else {
				device.setPriority ("OFF");
			}
			if (phone.getExtension () != null && phone.getExtension ().length () > 0) {
				device.setAddress (phone.getPhoneNumber () + 'x' + phone.getExtension ());
			} else {
				device.setAddress (phone.getPhoneNumber ());
			}
			device.setDescription (phone.getFriendlyName ());
			if (phone.getPagerCarrier () != null) {
				//abey - commenting for compilation
				//device.setPagerCarrier (phone.getPagerCarrier ().getMir3Id ());
			}
			device.setType (entityManager.find (ListType.class, Long.valueOf (phone.getTypeId ())).getName ());
			devices.add (device);
		}
		for (EmailAddress email : emailAddresses) {
			device = new Mir3DeviceTO ();
			priority = findEmail (email, preferences);
			if (priority > 0) {
				device.setPriority (String.valueOf (priority));
			} else {
				device.setPriority ("OFF");
			}
			device.setAddress (email.getAddress ());
			device.setDescription (email.getFriendlyName ());
			device.setType (entityManager.find (ListType.class, Long.valueOf (email.getTypeId ())).getName ());
			devices.add (device);
		}
	}

	/**
	 * Checks if the primary email is in the device preferences
	 * @param preferences the user contact prefernces
	 * @return the rank in the preferences or -1 if not found
	 */
	private int getPrimaryEmailPriority (final Collection<ContactPreference> preferences) {
		int index = -1;
		for (ContactPreference preference : preferences) {
			if (preference.getPrimaryEmail ()) {
				index = preference.getIndex () + 1;
			}
		}
		return index;
	}

	/**
	 * @param entityManager the entityManager to set
	 */
	//abey commenting to deploy
	//@PersistenceContext
	public void setEntityManager (EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
