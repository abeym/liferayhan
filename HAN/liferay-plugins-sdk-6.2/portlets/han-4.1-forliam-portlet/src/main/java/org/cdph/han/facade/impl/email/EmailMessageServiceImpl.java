package org.cdph.han.facade.impl.email;

import com.icesoft.faces.component.inputfile.FileInfo;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.faces.context.FacesContext;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Flags.Flag;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.alerts.dto.FileTO;
import org.cdph.han.alerts.services.UserDataService;
import org.cdph.han.email.dto.EmailAddressTO;
import org.cdph.han.email.dto.EmailAttachmentTO;
import org.cdph.han.email.dto.EmailSummaryTO;
import org.cdph.han.email.dto.EmailTO;
import org.cdph.han.email.services.EmailAuthenticator;
import org.cdph.han.email.services.EmailMessageService;
import org.cdph.han.email.services.EmailOperationOutcome;
import org.cdph.han.persistence.UserData;
import org.cdph.han.util.services.HanPropertiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implementation of the EmailMessageService
 * @author Abey Mani
 * @version 1.0
 */
@Service ("emailMessageService")
public class EmailMessageServiceImpl implements EmailMessageService {
	/** Class log */
	private static final Log log = LogFactory.getLog (EmailMessageServiceImpl.class);
	/** Domain */
	private static final String HAN_EMAIL_DOMAIN_KEY = "HAN_EMAIL_DOMAIN";
	/** Upload directory */
	private static final String FILE_UPLOAD_DIRECTORY = "upload";
	/** Email properties */
	private final Properties props = new Properties ();
	/** Service used to retrieve han properties */
	@Autowired
	private HanPropertiesService hanPropertiesService;
	/** Service to retrieve user information */
	@Autowired
	private UserDataService userDataService;

	/**
	 * Default constructor
	 */
	public EmailMessageServiceImpl () {
		try {
			props.load (this.getClass ().getResourceAsStream ("/email.properties"));
		} catch (Exception ex) {
			log.fatal ("Error loading email properties.", ex);
		}
	}

	/**
	 * Create a set of addresses with the given string array
	 * @param addresses to create
	 * @return addresses created
	 * @throws AddressException in case an address is not in the correct format
	 * @throws UnsupportedEncodingException in case of using an unsupported encoding in the character set
	 */
	private Address[] createAddresses (final long[] userIds) throws AddressException, UnsupportedEncodingException {
		Address[] inetAddresses = new Address[userIds.length];
		UserData userData;
		for (int i = 0; i < userIds.length; i++) {
			userData = userDataService.getUser (userIds[i]);
			inetAddresses[i] = new InternetAddress (userData.getScreenName () + '@'
					+ hanPropertiesService.getValue (HAN_EMAIL_DOMAIN_KEY),
					userData.getContact ().getFirstName () + " " + userData.getContact ().getLastName ());
		}
		return inetAddresses;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.email.services.EmailMessageService#listMessagesInFolder(java.lang.String)
	 */
	@Override
	public EmailSummaryTO[] listMessagesInFolder (final long userId, final String folderName)
			throws MessagingException {
		final UserData userData = userDataService.getUser (userId);
		final Session session = Session.getInstance (props, new EmailAuthenticator (
				userData.getScreenName (), userData.getEmailPassword ()));
		final Store store = session.getStore ("imap");
		store.connect ();
		final Folder folder = store.getFolder (folderName);
		folder.open (Folder.READ_ONLY);
		final List<EmailSummaryTO> messages = new ArrayList<EmailSummaryTO> ();
		EmailSummaryTO summary;
		if (folder.getMessageCount () > 0) {
			Message[] messagesInFolder = folder.getMessages ();
			InternetAddress address;
			for (Message message : messagesInFolder) {
				address = message.getFrom () != null ? (InternetAddress) message.getFrom ()[0] : null;
				summary = new EmailSummaryTO (new EmailAddressTO (address != null ? address.getPersonal () : "",
						address != null ? address.getAddress () : ""),
						message.getSubject (), message.getSentDate (), message.getSize (),
						message.getContentType ().startsWith ("multipart"), message.getMessageNumber (),
						!message.isSet (Flag.SEEN));
				messages.add (summary);
			}
		}
		return messages.toArray (new EmailSummaryTO[messages.size ()]);
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.email.services.EmailMessageService#moveMessageToFolder(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public EmailOperationOutcome moveMessageToFolder (final long userId, final int[] messageNumbers,
			final String fromFolder, final String toFolder) throws MessagingException {
		EmailOperationOutcome outcome;
		final UserData userData = userDataService.getUser (userId);
		final Session session = Session.getInstance (props, new EmailAuthenticator (userData.getScreenName (),
				userData.getEmailPassword ()));
		final Store store = session.getStore ("imap");
		store.connect ();
		final Folder srcFolder = store.getFolder (fromFolder);
		final Folder dstFolder = store.getFolder (toFolder);
		if (srcFolder.exists ()) {
			srcFolder.open (Folder.READ_WRITE);
			if (dstFolder.exists ()) {
				dstFolder.open (Folder.READ_WRITE);
				final Message[] messages = srcFolder.getMessages (messageNumbers);
				for (Message message : messages) {
					message.setFlag (Flag.DELETED, false);
				}
				srcFolder.copyMessages (messages, dstFolder);
				for (Message message : messages) {
					message.setFlag (Flag.DELETED, true);
				}
				srcFolder.close (true);
				outcome = EmailOperationOutcome.MESSAGE_MOVED;
			} else {
				outcome = EmailOperationOutcome.FOLDER_DOES_NOT_EXISTS;
			}
		} else {
			outcome = EmailOperationOutcome.FOLDER_DOES_NOT_EXISTS;
		}
		return outcome;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.email.services.EmailMessageService#saveAsDraft(long, long[], long[], java.lang.String, java.lang.String, java.util.List)
	 */
	@Override
	public boolean saveAsDraft (final long from, final long[] to, final long[] cc,
			final String message, final String subject, final List<FileTO> attachments) {
		boolean success = true;
		final UserData userData = userDataService.getUser (from);
		final Session session = Session.getInstance (props, new EmailAuthenticator (
				userData.getScreenName (), userData.getEmailPassword ()));
		final MimeMessage mimeMessage = new MimeMessage (session);
		try {
			if (attachments == null || attachments.isEmpty ()) {
				mimeMessage.setContent (message, "text/html");
			} else {
				Multipart multipart = new MimeMultipart ();
				BodyPart bodyPart = new MimeBodyPart ();
				bodyPart.setContent (message, "text/html");
				multipart.addBodyPart (bodyPart);
				DataSource dataSource;
				for (FileTO attachment : attachments) {
					dataSource = new FileDataSource (attachment.getFile ());
					bodyPart = new MimeBodyPart ();
					bodyPart.setDataHandler (new DataHandler (dataSource));
					bodyPart.setFileName (attachment.getFileInfo ().getFileName ());
					multipart.addBodyPart (bodyPart);
				}
				mimeMessage.setContent (multipart);
			}
			mimeMessage.setSubject (subject);
			mimeMessage.setRecipients (RecipientType.TO, createAddresses (to));
			if (cc != null && cc.length > 0) {
				mimeMessage.setRecipients (RecipientType.CC, createAddresses (cc));
			}
			mimeMessage.setFrom (new InternetAddress (userData.getScreenName () + '@'
					+ hanPropertiesService.getValue (HAN_EMAIL_DOMAIN_KEY),
					userData.getContact ().getFirstName () + " " + userData.getContact ().getLastName ()));
			final Store store = session.getStore ("imap");
			store.connect ();
			final Folder drafts = store.getDefaultFolder ().getFolder ("INBOX").getFolder ("Drafts");
			if (!drafts.exists ()) {
				drafts.create (Folder.HOLDS_MESSAGES);
			}
			drafts.appendMessages (new Message[]{mimeMessage});
			mimeMessage.saveChanges ();
			mimeMessage.setFlag (Flag.DRAFT, true);
			mimeMessage.saveChanges ();
		} catch (AddressException ex) {
			log.error ("Error creating addresses", ex);
		} catch (MessagingException ex) {
			log.error ("Error creating email message.", ex);
		} catch (UnsupportedEncodingException ex) {
			log.error ("Unsupported encoding found while creating addresses.", ex);
		}
		return success;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.email.services.EmailMessageService#retrieveMessage(long, java.lang.String, int)
	 */
	@Override
	public EmailTO retrieveMessage (final long userId, final String folderName, final int messageNumber)
	throws MessagingException {
		boolean html = true;
		final UserData userData = userDataService.getUser (userId);
		final Session session = Session.getInstance (props, new EmailAuthenticator (
				userData.getScreenName (), userData.getEmailPassword ()));
		final Store store = session.getStore ("imap");
		store.connect ();
		final Folder folder = store.getFolder (folderName);
		folder.open (Folder.READ_ONLY);
		final Message message = folder.getMessage (messageNumber);
		final Address[] toAddresses = message.getRecipients (RecipientType.TO);
		final Address[] ccAddresses = message.getRecipients (RecipientType.CC);
		final EmailAddressTO[] to = new EmailAddressTO[toAddresses != null ? toAddresses.length : 0];
		final EmailAddressTO[] cc = new EmailAddressTO[ccAddresses != null ? ccAddresses.length : 0];
		InternetAddress address;
		for (int i = 0; i < to.length; i++) {
			address = (InternetAddress) toAddresses[i];
			to[i] = new EmailAddressTO (address.getPersonal (), address.getAddress ());
		}
		for (int i = 0; i < cc.length; i++) {
			address = (InternetAddress) ccAddresses[i];
			cc[i] = new EmailAddressTO (address.getPersonal (), address.getAddress ());
		}
		final List<EmailAttachmentTO> attachments = new ArrayList<EmailAttachmentTO> ();
		address = (InternetAddress) message.getFrom ()[0];
		final EmailAddressTO from = new EmailAddressTO (address.getPersonal (), address.getAddress ());
		final String subject = message.getSubject ();
		final Date sent = message.getSentDate ();
		byte[] temp;
		String content = "";
		try {
			if (message.getContent () instanceof String) {
				content = (String) message.getContent ();
				html = message.isMimeType ("text/html");
			} else if (message.getContent () instanceof Multipart) {
				final Multipart multipart = (Multipart) message.getContent ();
				Part part;
				for (int i = 0; i < multipart.getCount (); i++) {
					part = multipart.getBodyPart (i);
					if (Part.ATTACHMENT.equalsIgnoreCase (part.getDisposition ()) ||
							Part.INLINE.equalsIgnoreCase (part.getDisposition ())) {
						InputStream input = part.getDataHandler ().getInputStream ();
						temp = new byte[part.getSize ()];
						input.read (temp);
						attachments.add (new EmailAttachmentTO (part.getFileName (),
								part.getContentType ().substring (0, part.getContentType ().lastIndexOf (';')),
								temp));
					} else {
						MimeBodyPart mbPart = (MimeBodyPart) part;
						if (mbPart.isMimeType ("text/plain") || mbPart.isMimeType ("text/html")) {
							content = (String) mbPart.getContent ();
							html = mbPart.isMimeType ("text/html");
						} else if (mbPart.isMimeType ("message/rfc822")) {
							// The message has another message as attachment
							final MimeMessage attached = (MimeMessage) mbPart.getContent ();
							final StringBuilder contentBuilder = new StringBuilder (
									"<p>&nbsp;</p><p><hr />-- Original Message Begins --</p>");
							final SimpleDateFormat sdf = new SimpleDateFormat ("MM/dd/yyyy hh:mm:ss a");
							contentBuilder.append ("<p><strong>From:</strong> ").append (
									((InternetAddress) attached.getFrom ()[0]).getPersonal ()).append ("<br/>");
							contentBuilder.append ("<strong>Date:</strong> ").append (
									sdf.format (attached.getSentDate ())).append ("<br/>");
							contentBuilder.append ("<strong>To:</strong> ").append (
									concatenateAddresses (attached.getRecipients (RecipientType.TO))).append ("<br/>");
							contentBuilder.append ("<strong>CC:</strong> ").append (
									concatenateAddresses (attached.getRecipients (RecipientType.CC))).append ("<br/>");
							contentBuilder.append ("<strong>Subject:</strong> ").append (
									attached.getSubject ()).append ("</p>");
							contentBuilder.append (attached.getContent ().toString ());
							temp = contentBuilder.toString ().getBytes ();
							attachments.add (new EmailAttachmentTO ("originalMessage.html", "text/html", temp));
						} else {
							log.warn ("Unhandled MIME type: " + mbPart.getContentType ());
						}
					}
				}
			}
		} catch (IOException ex) {
			log.error ("Error retrieving message content", ex);
		}
		return new EmailTO (to, cc, from, attachments.toArray (new EmailAttachmentTO[attachments.size ()]), content,
				folderName, sent, subject, messageNumber, html);
	}

	/**
	 * Concatenate the recipients into a single string
	 * @param recipients to concatenate
	 * @return the concatenated recipients
	 */
	private String concatenateAddresses (final Address[] recipients) {
		final StringBuilder builder = new StringBuilder ();
		if (recipients != null) {
			for (Address address : recipients) {
				builder.append (((InternetAddress) address).getPersonal ()).append (", ");
			}
			if (builder.length () > 0) {
				builder.delete (builder.length () - 2, builder.length ());
			}
		}
		return builder.toString ();
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.email.services.EmailMessageService#deleteMessage(long, java.lang.String, int)
	 */
	@Override
	public void deleteMessages (final long userId, final String folderName, final int[] messageNumbers,
			final boolean moveToTrash) throws MessagingException {
		final UserData userData = userDataService.getUser (userId);
		final Session session = Session.getInstance (props, new EmailAuthenticator (
				userData.getScreenName (), userData.getEmailPassword ()));
		final Store store = session.getStore ("imap");
		store.connect ();
		final Folder folder = store.getFolder (folderName);
		folder.open (Folder.READ_WRITE);
		final Message[] messages = folder.getMessages (messageNumbers);
		for (Message message : messages) {
			message.setFlag (Flag.DELETED, true);
		}
		if (moveToTrash) {
			final Folder trash = store.getDefaultFolder ().getFolder ("INBOX").getFolder ("Trash");
			trash.open (Folder.READ_WRITE);
			trash.appendMessages (messages);
			trash.close (false);
		}
		folder.close (true);
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.email.services.EmailMessageService#makeEditable(org.cdph.han.email.dto.EmailAttachmentTO[])
	 */
	@Override
	public List<FileTO> makeEditable (EmailAttachmentTO[] attachments)
			throws Exception {
		final List<FileTO> editables = new ArrayList<FileTO> ();
		final Object reqObj = FacesContext.getCurrentInstance ().getExternalContext ().getRequest ();
		String uploadDir = "/tmp/upload";
		if (reqObj instanceof HttpServletRequest) {
			String appDir = ((HttpServletRequest) reqObj).getSession ().getServletContext ().getRealPath (
					((HttpServletRequest) reqObj).getSession ().getServletContext ().getServletContextName ());
			uploadDir = appDir.substring (0, appDir.lastIndexOf ('/')) + '/' + FILE_UPLOAD_DIRECTORY + '/'
					+ ((HttpServletRequest) reqObj).getSession ().getId ();
		} else if (reqObj instanceof PortletRequest) {
			String appDir = ((PortletRequest) reqObj).getPortletSession ().getPortletContext ().getRealPath (
					((PortletRequest) reqObj).getPortletSession ().getPortletContext (
							).getPortletContextName ());
			uploadDir = appDir.substring (0, appDir.lastIndexOf ('/')) + '/' + FILE_UPLOAD_DIRECTORY + '/' +
					((PortletRequest) reqObj).getPortletSession ().getId ();
		}
		final File uploadPath = new File (uploadDir);
		if (!uploadPath.exists ()) {
			uploadPath.mkdirs ();
		}
		if (attachments != null) {
			FileTO fileTO;
			FileInfo fileInfo;
			File currentFile;
			WritableByteChannel channel;
			ByteBuffer buffer = ByteBuffer.allocate (20971520);
			for (EmailAttachmentTO attachment : attachments) {
				fileTO = new FileTO ();
				fileInfo = new FileInfo ();
				fileInfo.setContentType (attachment.getMimeType ());
				fileInfo.setFileName (attachment.getFileName ());
				fileInfo.setPercent (100);
				//fileInfo.setPhysicalPath (uploadDir + '/' + fileInfo.getFileName ());
				fileTO.setFileInfo (fileInfo);
				currentFile = new File (uploadDir + '/' + fileInfo.getFileName ());
				if (!currentFile.exists ()) {
					channel = Channels.newChannel (new FileOutputStream (currentFile));
					buffer.clear ();
					buffer.put (attachment.getContent ());
					buffer.flip ();
					channel.write (buffer);
					channel.close ();
				}
				fileTO.setFile (currentFile);
				editables.add (fileTO);
			}
		}
		return editables;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.email.services.EmailMessageService#markAsSeen(long, java.lang.String, int[])
	 */
	@Override
	public void markAsSeen (final long userId, final String folderName, final int[] messageNumbers)
			throws MessagingException {
		final UserData userData = userDataService.getUser (userId);
		final Session session = Session.getInstance (props, new EmailAuthenticator (
				userData.getScreenName (), userData.getEmailPassword ()));
		final Store store = session.getStore ("imap");
		store.connect ();
		final Folder folder = store.getFolder (folderName);
		folder.open (Folder.READ_WRITE);
		final Message[] messages = folder.getMessages (messageNumbers);
		for (Message message: messages) {
			message.setFlag (Flag.SEEN, true);
		}
		folder.close (false);
	}

}
