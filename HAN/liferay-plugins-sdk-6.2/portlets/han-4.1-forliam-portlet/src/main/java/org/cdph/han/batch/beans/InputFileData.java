package org.cdph.han.batch.beans;

import java.io.File;

import org.cdph.han.alerts.dto.FileTO;
import org.icefaces.ace.component.fileentry.FileEntry;
import org.icefaces.ace.component.fileentry.FileEntryResults;
import org.icefaces.ace.component.fileentry.FileEntryStatus;

import com.icesoft.faces.component.inputfile.FileInfo;
//import com.icesoft.faces.component.inputfile.InputFile;

public class InputFileData {
	
    private FileEntry inputFile;
    
    public InputFileData(FileEntry inputFile){
    	this.inputFile = inputFile;
    }

    public org.icefaces.ace.component.fileentry.FileEntryResults.FileInfo getFileInfo() {
    	for (FileEntryResults.FileInfo fileInfo : inputFile.getResults().getFiles()) {
	        if (fileInfo.isSaved()) {
	        	return fileInfo;
	        }
    	}
        return null;
    }


    public File getFile() {
    	for(FileEntryResults.FileInfo i : inputFile.getResults().getFiles())
    	{
    		if(i.isSaved())
    		{
    			return i.getFile();
    		}
    	}
        return null;
    }
    
    public FileEntryStatus getStatus(){
    	for(FileEntryResults.FileInfo i : inputFile.getResults().getFiles())
    	{
    		if(i.isSaved())
    		{
    			return i.getStatus();
    		}
    	}
        return null;
    	//return inputFile.getStatus();
    }

    
    /**
     * Method to return the file size as a formatted string
     * For example, 4000 bytes would be returned as 4kb
     *
     *@return formatted file size
     */
    public String getSizeFormatted() {
        long ourLength = getFile().length();
        
        // Generate formatted label, such as 4kb, instead of just a plain number
        if (ourLength >= BatchUploadBean.MEGABYTE_LENGTH_BYTES) {
            return ourLength / BatchUploadBean.MEGABYTE_LENGTH_BYTES + "mb";
        }
        else if (ourLength >= BatchUploadBean.KILOBYTE_LENGTH_BYTES) {
            return ourLength / BatchUploadBean.KILOBYTE_LENGTH_BYTES + "kb";
        }
        else if (ourLength == 0) {
            return "0";
        }
        else if (ourLength < BatchUploadBean.KILOBYTE_LENGTH_BYTES) {
            return ourLength + "b";
        }
        
        return Long.toString(ourLength);
    }    

}
