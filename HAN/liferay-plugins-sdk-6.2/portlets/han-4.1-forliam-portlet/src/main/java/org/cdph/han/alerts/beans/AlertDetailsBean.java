package org.cdph.han.alerts.beans;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.alerts.dto.AlertTO;
import org.cdph.han.alerts.dto.FileTO;
import org.cdph.han.alerts.dto.OptionTO;
import org.cdph.han.alerts.services.AlertActionsService;
import org.cdph.han.custgroups.dto.RecipientTO;
import org.cdph.han.custgroups.dto.RecipientType;
import org.cdph.han.model.Alert;
import org.cdph.han.model.AlertAttachment;
import org.cdph.han.model.AlertRecipient;
import org.cdph.han.persistence.AlertAttachmentModel;
import org.cdph.han.util.asynch.beans.UpdatableBean;
import org.cdph.han.util.asynch.dto.AsynchRequestTO;
import org.cdph.han.util.asynch.dto.AsynchResponseTO;
import org.cdph.han.util.asynch.services.AsynchRefreshService;
import org.cdph.han.util.asynch.services.AsynchRequestType;

import com.icesoft.faces.component.inputfile.FileInfo;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.Group;
import com.liferay.portal.model.User;
import com.liferay.portal.service.GroupLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;
import java.util.ArrayList;
import java.util.List;

import org.cdph.han.service.AlertAttachmentLocalServiceUtil;
import org.cdph.han.service.AlertLocalServiceUtil;
import org.cdph.han.service.AlertRecipientLocalServiceUtil;

/*import com.icesoft.faces.async.render.Disposable; 
import com.icesoft.faces.async.render.OnDemandRenderer;
import com.icesoft.faces.async.render.RenderManager;
import com.icesoft.faces.async.render.Renderable;
import com.icesoft.faces.webapp.xmlhttp.PersistentFacesState;
import com.icesoft.faces.webapp.xmlhttp.RenderingException;*/

/**
 * This class is the controller for the JSF actions to render the alert details, also implements the Renderable
 * interface to provide asynchronous page refresh, the class also implements the Disposable interface to free resources
 * when the object is discarded and implements the UpdatableBean to receive asynchronous responses from webservices.
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class AlertDetailsBean extends AbstractAlertSearchBean implements /*Renderable, Disposable,*/ UpdatableBean {
	/** Class serial version UID */
	private static final long serialVersionUID = -10543518726250271L;
	/** Class log */
	private static final Log log = LogFactory.getLog (AlertDetailsBean.class);
	/** The Faces persistent state */
//	private PersistentFacesState state;
    /** Render manager */
//    private RenderManager renderManager;
    /** Service to register for updates while tracking an alert */
    private AsynchRefreshService asynchRefreshService;
    /** Renderer used to request renders on the page */
//    private OnDemandRenderer renderer;
    /** The alert with the details */
    private AlertTO alert;
    /** The previous alert requested */
    private long previousAlert;
    /** If the previous selected alert was archived */
    private boolean previousArchived;
    /** Flag for showing the response options */
    private boolean showOptions;
    /** The notification report ID */
	private Integer notificationReportId;
	/** The ID of the recipient in the notification report */
	private Integer recipientReportId;
	/** Service to check if the alert requires further action */
	private AlertActionsService alertActionsService;
	/** Used to identify if the alert has the final response */
	private boolean finalResponse;
	/** Value to denote activity in the progress bar */
	private int value;

	/**
	 * @param alertActionsService the alertActionsService to set
	 */
	public void setAlertActionsService (AlertActionsService alertActionsService) {
		this.alertActionsService = alertActionsService;
	}

	/**
	 * Constructor to set initial resources
	 */
	public AlertDetailsBean () {
//		state = PersistentFacesState.getInstance ();
		value = 0;
	}

	/**
	 * Action listener that retrieves all the alert details and fires an asynchronous request to check if the alert
	 * requires further action by the user (only if the alert is not finished)
	 * @param event ActionEvent generated by the user
	 */
	public void retrieveAlertDetails (final ActionEvent event) {
		searchTitle = "alerts.detail.title";
		final String alertIdString = (String) FacesContext.getCurrentInstance (
				).getExternalContext ().getRequestParameterMap ().get ("alertId");
		final String notificationReportIdString = (String) FacesContext.getCurrentInstance (
				).getExternalContext ().getRequestParameterMap ().get ("notificationReportId");
		final String recipientReportIdString = (String) FacesContext.getCurrentInstance (
				).getExternalContext ().getRequestParameterMap ().get ("recipientReportId");
		final String archivedString = (String) FacesContext.getCurrentInstance (
				).getExternalContext ().getRequestParameterMap ().get ("archived");
		final long alertId = alertIdString != null && alertIdString.length () > 0
				? Long.parseLong (alertIdString) : previousAlert;
		final boolean archived = archivedString != null && archivedString.length () > 0
				? Boolean.parseBoolean (archivedString) : previousArchived;
		value = 0;
		// Retrieve the alert
		Alert al;
		try {
			al = AlertLocalServiceUtil.getAlert(alertId);
			alert = transferAlert(al);
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//alertJPAService.loadAlert (alertId, archived);
		//TODO: pop recipients
		if(alert.getRecipients ()!=null){
			for (RecipientTO recipient : alert.getRecipients ()) {
				System.out.println (recipient.getName ());
			}
		}
		// Set the service to the file objects to enable the attachment download
//		for (FileTO file : alert.getAttachments ()) {
//			file.setAlertJPAService (alertJPAService);
//			file.setArchived (archived);
//		}
		// If the alert is not archived check if requires further action
		if (!alert.isArchived ()) {
			Integer reportIds[] = null;
			boolean requiresAction = false;
			// Check if the alert requires further action from the user
			if (recipientReportIdString != null && !recipientReportIdString.equals ("")) {
				reportIds = new Integer[2];
				reportIds[0] = Integer.parseInt (notificationReportIdString);
				reportIds[1] = Integer.parseInt (recipientReportIdString);
				requiresAction = true;
			} else {
				// Check if the alert requires further action in using the local data
				requiresAction = alertActionsService.checkForRequiredAction (alertId);
			}
			// Check if the alert only contains one response option and already have the ids
			if (requiresAction && reportIds != null && alert.getResponseOptions ().size () == 1) {
				showOptions = false;
				alertActionsService.respondAction (reportIds[0], reportIds[1], 1);
			} else if (requiresAction && reportIds != null) { // Requires further action and already have ids
				showOptions = true;
				notificationReportId = reportIds[0];
				recipientReportId = reportIds[1];
			} else if (requiresAction) { // May require further action by local data
				// Remove previous instance if any
				value = 10;
				showOptions = true;
				finalResponse = false;
				asynchRefreshService.removeBean (this);
				/*if (renderer != null) {
					renderer.remove (this);
					renderer.dispose ();
				}
				renderer = renderManager.getOnDemandRenderer (getSessionId());
				renderer.add (this);*/
				final AsynchRequestTO request = new AsynchRequestTO (null,
						AsynchRequestType.ALERT_REQUIRES_ACTION, getSessionId ());
				request.getRequest ().put ("USER_ID", FacesContext.getCurrentInstance ().getExternalContext (
						).getUserPrincipal ().getName ());
				request.getRequest ().put ("ALERT_ID", alertId);
				request.getRequest ().put ("ONE_OPTION", alert.getResponseOptions ().size () == 1);
				asynchRefreshService.addOneTimeUpdatableBean (this, request);
			}
		}
		previousAlert = alertId;
		previousArchived = archived;
	}

	private AlertTO transferAlert(Alert al) {
		System.out.println("Loading alert for " + al.getId() + " / " + al.getPrimaryKey());
		String uploadDir = "/tmp/upload";
		AlertTO to = new AlertTO();
		to.setAlertId(al.getId());
		to.setAlertName(al.getName());
		to.setPublishedDate(al.getDatePublished());
		to.setTopic("topicId: "+al.getTopicId());
		try {
			List<AlertRecipient> alertRecipients = AlertRecipientLocalServiceUtil.getByAlertId(al.getId());
			if (alertRecipients != null) {
				List<RecipientTO> alertRecipientTOs = new ArrayList<RecipientTO>();
				System.out.println("Alert Recipients " + alertRecipients.size());
				for (AlertRecipient alertRecipient : alertRecipients) {
					alertRecipientTOs.add(createAlertRecipient(alertRecipient));
				}
				to.setRecipients(alertRecipientTOs);
			}
			List<AlertAttachment> alertAttachments = null;//AlertAttachmentLocalServiceUtil.getByAlertId(al.getId());
			if (alertAttachments != null) {
				to.setAttachments(new ArrayList<FileTO>());
//				System.out.println("Alert Attachments " + alertAttachments.size());
//				List<FileTO> alertFileTOs = new ArrayList<FileTO>();
//				FileTO fileTO;
//				FileInfo fileInfo;
//				File currentFile;
//				WritableByteChannel channel;
//				ByteBuffer buffer = ByteBuffer.allocate (20971520);
//				int index = 0;
//				for (AlertAttachment alertAttachment : alertAttachments) {
//					fileTO = new FileTO ();
//					fileInfo = new FileInfo ();
//					fileInfo.setContentType (alertAttachment.getMimeType ());
//					fileInfo.setFileName (alertAttachment.getName ());
//					fileInfo.setPercent (100);
//					fileTO.setFileInfo (fileInfo);
//					currentFile = new File (uploadDir + '/' + fileInfo.getFileName ());
//					if (!currentFile.exists ()) {
//						try {
//							channel = Channels.newChannel (new FileOutputStream (currentFile));
//							buffer.clear ();
//							if (alertAttachment.getAttachment().length() > 0) {
//								buffer.put (alertAttachment.getAttachment().getBytes(), 0, alertAttachment.getAttachment().length());
//								buffer.flip ();
//								channel.write (buffer);
//							}
//							channel.close ();
//						} catch (IOException e) {
//							e.printStackTrace();
//						}
//					}
//					fileTO.setFile (currentFile);
// //					if (alertAttachment.equals (telAttachment)) {
// //						attachmentIndex = index;
// //					}
//					++index;
//					to.getAttachments ().add (fileTO);
				}
//			}
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (Exception e) {
		}
		return to;
	}

	private RecipientTO createAlertRecipient (AlertRecipient alertRecipient)
		throws SystemException {
		RecipientTO retval = new RecipientTO();
		if (alertRecipient.getUserId() > 0) {
			retval.setType(RecipientType.USER);
			try {
				User user = UserLocalServiceUtil.getUser(alertRecipient.getUserId());
				retval.setName(user.getFullName());
			} catch (PortalException pe) {}
		} else if (alertRecipient.getNonHanUserId() > 0) {
			retval.setType(RecipientType.NON_HAN_USER);
			retval.setName("" + alertRecipient.getNonHanUserId());
		} else if (alertRecipient.getGroupId() > 0) {
			retval.setType(RecipientType.GROUP);
			try {
				Group group = GroupLocalServiceUtil.getGroup(alertRecipient.getGroupId());
				retval.setName(group.getName());
			} catch (PortalException pe) {	}
			retval.setName("" + alertRecipient.getGroupId());
		} else if (alertRecipient.getNonHanUserGroupId() > 0) {
			retval.setType(RecipientType.NON_HAN_GROUP);
			retval.setName("" + alertRecipient.getNonHanUserGroupId());
		} else {
			retval.setType(RecipientType.ORGANIZATION_TYPE);
		}
		retval.setChicagoOnly(alertRecipient.getChicagoOnly());
		retval.setNotified(alertRecipient.getNotified());
		return retval;
	}

	/**
	 * Action listener invoked to perform actions on a given alert
	 * @param event ActionEvent generated by the users action
	 */
	public void performAction (final ActionEvent event) {
		final String reportIdString = (String) FacesContext.getCurrentInstance ().getExternalContext (
				).getRequestParameterMap ().get ("reportId");
		final String recipientReportIdString = (String) FacesContext.getCurrentInstance ().getExternalContext (
				).getRequestParameterMap ().get ("recipientReportId");
		final String optionSelected = (String) FacesContext.getCurrentInstance ().getExternalContext (
				).getRequestParameterMap ().get ("optionSelected");
		final int reportId = Integer.parseInt (reportIdString);
		final int recipientReportId = Integer.parseInt (recipientReportIdString);
		int optionIndex = 0;
		for (OptionTO option : alert.getResponseOptions ()) {
			if (option.getOptionText ().equals (optionSelected)) {
				break;
			} else {
				++optionIndex;
			}
		}
		alertActionsService.respondAction (reportId, recipientReportId, optionIndex);
	}

	/**
	 * Action listener used to respond an alert with the selected option
	 * @param event ActionEvent
	 */
	public void respondAlert (final ActionEvent event) {
		try {
			final String notificationReportIdString = (String) FacesContext.getCurrentInstance (
					).getExternalContext ().getRequestParameterMap ().get ("reportId");
			final String recipientReportIdString = (String) FacesContext.getCurrentInstance (
					).getExternalContext ().getRequestParameterMap ().get ("recipientReportId");
			final String optionSelectedString = (String) FacesContext.getCurrentInstance (
					).getExternalContext ().getRequestParameterMap ().get ("optionSelected");
			final int notificationReportId = Integer.parseInt (notificationReportIdString);
			final int recipientReportId = Integer.parseInt (recipientReportIdString);
			int index = 1;
			for (OptionTO option : alert.getResponseOptions ()) {
				if (option.getOptionText ().equals (optionSelectedString)) {
					break;
				} else {
					++index;
				}
			}
			showOptions = false;
			alertActionsService.respondAction (notificationReportId, recipientReportId, index);
		} catch (Exception ex) {
			log.error ("Error responding to notification", ex);
			
		}
	}

	/* (non-Javadoc)
	 * @see com.icesoft.faces.async.render.Disposable#dispose()
	 */
	/*@Override
	public void dispose () {
		asynchRefreshService.removeBean (this);
		if (renderer != null) {
			renderer.remove (this);
			renderer.dispose ();
		}
	}*/

	/* (non-Javadoc)
	 * @see com.icesoft.faces.async.render.Renderable#getState()
	 */
	/*@Override
	public PersistentFacesState getState () {
		return state;
	}*/

	/* (non-Javadoc)
	 * @see com.icesoft.faces.async.render.Renderable#renderingException(com.icesoft.faces.webapp.xmlhttp.RenderingException)
	 */
	/*@Override
	public void renderingException (RenderingException arg0) {
		log.info ("Got rendering exception.");
		if (renderer != null) {
			renderer.remove (this);
			renderer.dispose ();
		}
		asynchRefreshService.removeBean (this);
	}*/

	/* (non-Javadoc)
	 * @see org.cdph.han.util.asynch.beans.UpdatableBean#update(org.cdph.han.util.asynch.dto.AsynchResponseTO)
	 */
	@Override
	public void update (final AsynchResponseTO updatedInfo) {
		finalResponse = true;
		value = 0;
		Integer notificationReportId = (Integer) updatedInfo.getResult ().get ("NOTIFICATION_REPORT_ID");
		Integer recipientReportId = (Integer) updatedInfo.getResult ().get ("RECIPIENT_REPORT_ID");
		if (notificationReportId != null) {
			this.notificationReportId = notificationReportId;
			this.recipientReportId = recipientReportId;
			showOptions = true;
		} else {
			showOptions = false;
		}
		/*renderer.requestRender ();
		renderer.remove (this);
		renderer.dispose ();*/
	}

	/**
	 * @param renderManager the renderManager to set
	 */
	/*public void setRenderManager (RenderManager renderManager) {
		this.renderManager = renderManager;
	}*/

	/**
	 * @param asynchRefreshService the asynchRefreshService to set
	 */
	public void setAsynchRefreshService (AsynchRefreshService asynchRefreshService) {
		this.asynchRefreshService = asynchRefreshService;
	}

	/**
	 * @return the alert
	 */
	public AlertTO getAlert () {
		return alert;
	}

	/**
	 * @param alert the alert to set
	 */
	public void setAlert (AlertTO alert) {
		this.alert = alert;
	}

	/**
	 * @return the showOptions
	 */
	public boolean isShowOptions () {
		return showOptions;
	}

	/**
	 * @param showOptions the showOptions to set
	 */
	public void setShowOptions (boolean showOptions) {
		this.showOptions = showOptions;
	}

	/**
	 * @return the notificationReportId
	 */
	public Integer getNotificationReportId () {
		return notificationReportId;
	}

	/**
	 * @param notificationReportId the notificationReportId to set
	 */
	public void setNotificationReportId (Integer notificationReportId) {
		this.notificationReportId = notificationReportId;
	}

	/**
	 * @return the recipientReportId
	 */
	public Integer getRecipientReportId () {
		return recipientReportId;
	}

	/**
	 * @param recipientReportId the recipientReportId to set
	 */
	public void setRecipientReportId (Integer recipientReportId) {
		this.recipientReportId = recipientReportId;
	}

	/**
	 * @return the finalResponse
	 */
	public boolean isFinalResponse () {
		return finalResponse;
	}

	/**
	 * @param finalResponse the finalResponse to set
	 */
	public void setFinalResponse (boolean finalResponse) {
		this.finalResponse = finalResponse;
	}

	/**
	 * @return the alertStatus
	 */
	public String getAlertStatus () {
		String status = "";
		if (alert.getPublishedDate () != null) {
			status = "alerts.detail.status.current";
		} else if (alert.isScheduled ()) {
			status = "alerts.detail.status.scheduled";
		} else {
			status = "alerts.detail.status.draft";
		}
		return status;
	}

	/**
	 * @return the value
	 */
	public int getValue () {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue (int value) {
		this.value = value;
	}

}
