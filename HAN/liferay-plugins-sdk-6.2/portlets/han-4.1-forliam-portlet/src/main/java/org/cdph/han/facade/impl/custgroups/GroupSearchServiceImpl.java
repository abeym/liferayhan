package org.cdph.han.facade.impl.custgroups;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.custgroups.dto.FilterCriteriaTO;
import org.cdph.han.custgroups.dto.GroupDisplayTO;
import org.cdph.han.custgroups.dto.GroupTO;
import org.cdph.han.custgroups.dto.RecipientTO;
import org.cdph.han.custgroups.dto.RecipientType;
import org.cdph.han.custgroups.services.*;
import org.cdph.han.model.impl.UserCustomGroupImpl;
import org.cdph.han.persistence.UserData;
import org.cdph.han.persistence.AreaField;
import org.cdph.han.persistence.Contact;
import org.cdph.han.persistence.CustomGroupCriteria;
import org.cdph.han.persistence.CustomGroupMember;
import org.cdph.han.persistence.NonHanUser;
import org.cdph.han.persistence.NonHanUserGroup;
import org.cdph.han.persistence.Organization;
import org.cdph.han.persistence.Role;
import org.cdph.han.persistence.UserCustomGroup;
import org.cdph.han.service.AreaFieldLocalServiceUtil;
import org.cdph.han.service.CustomGroupMemberLocalServiceUtil;
import org.cdph.han.service.NonHanUserGroupLocalServiceUtil;
import org.cdph.han.service.NonHanUserLocalServiceUtil;
import org.cdph.han.service.UserCustomGroupLocalServiceUtil;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.InfrastructureUtil;
import com.liferay.portal.model.User;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;

/**
 * Implementation of the GroupSearchService using JPA
 * @author Abey Mani
 * @version 1.0
 */
@Repository
@Service ("groupSearchService")
public class GroupSearchServiceImpl implements GroupSearchService {
	private static final Log log = LogFactory.getLog (GroupSearchServiceImpl.class);
	@Autowired
	private ModelMapper modelMapper;
	/** Persistence context */

	/* (non-Javadoc)
	 * @see org.cdph.han.custgroups.services.GroupSearchService#searchMyGroups(long)
	 */
	@Override
	public GroupDisplayTO[] searchMyGroups (final long userId) {
		List<org.cdph.han.model.UserCustomGroup> foundGroups =null;
		try{
			foundGroups = UserCustomGroupLocalServiceUtil.findByUser(userId);
		}catch(Exception e){
			log.error(e);
		}
		final List<GroupDisplayTO> foundCustomGroups = loadCustomGroups(foundGroups);
		return foundCustomGroups.toArray (new GroupDisplayTO[foundCustomGroups.size ()]);
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.custgroups.services.GroupSearchService#isNameAvailable(long, java.lang.String)
	 */
	@Override
	//abey ...@Transactional (readOnly = true)
	public boolean isNameAvailable (final long userId, final String name) {
		List<?> foundGroups = UserCustomGroupLocalServiceUtil.findByUserAndName(userId, name);
		return foundGroups.isEmpty();
	}

	
	public UserCustomGroup lrCustGroupToHan(org.cdph.han.model.UserCustomGroup lucg){
		UserCustomGroup ucg = new UserCustomGroup();
		ucg.setCreatedDate(new Date(lucg.getCreatedDate()));
		ucg.setDateModified(lucg.getDateModified());
		modelMapper = modelMapper==null?new ModelMapper():modelMapper;
		//modelMapper.map(lucg,ucg);
		//TODO: this could be the wrong groupId
		ucg.setGroupId(lucg.getGroupId());
		//TODO: get the mebers of the group
		//ucg.setMembers(lucg.);
		ucg.setName(lucg.getName());

		try {
			log.debug("User " + lucg.getUserId() + ", modified by " + lucg.getModifiedBy());
			User createdUser = UserLocalServiceUtil.getUser(lucg.getUserId());
			org.cdph.han.persistence.UserData userData = new org.cdph.han.persistence.UserData();
			org.cdph.han.persistence.Contact contact = new org.cdph.han.persistence.Contact();
			contact.setFirstName(createdUser.getFirstName());
			contact.setLastName(createdUser.getLastName());
			contact.setContactId(createdUser.getUserId());
			userData.setContact(contact);
			ucg.setUserData(userData);
			if (lucg.getModifiedBy() > 0) {
				User modifyingUser = UserLocalServiceUtil.getUser(lucg.getModifiedBy());
				userData = new org.cdph.han.persistence.UserData();
				contact = new org.cdph.han.persistence.Contact();
				contact.setFirstName(modifyingUser.getFirstName());
				contact.setLastName(modifyingUser.getLastName());
				contact.setContactId(modifyingUser.getUserId());
				userData.setContact(contact);
				ucg.setModifiedBy(userData);
			}
		} catch (SystemException se) {log.error("System ERROR: " + se);
		} catch (PortalException pe) {log.error("Portal ERROR: " + pe);
		}
		
	/*	for(org.cdph.han.model.AlertTopic lat:lats){
			AlertTopic at = new AlertTopic();
			modelMapper.map(lat, at);
			hats.add(at);
		}*/
		return ucg;
		
	}
	
	/* (non-Javadoc)
	 * @see org.cdph.han.custgroups.services.GroupSearchService#searchUserGroups(java.lang.String)
	 */
	@Override
	public GroupDisplayTO[] searchUserGroups (String name) {
		log.debug("Search Users: " + name);
		List<org.cdph.han.model.UserCustomGroup> customGroups = new ArrayList<org.cdph.han.model.UserCustomGroup>();
		String sql = "select groupId, userId, name, created_date, modified_by_userId, date_modified"
				+ " from user_cust_groups where deleted = 0 ";
		if (name != null && !name.isEmpty()) {
			sql += "and upper (name) like \"" + name.toUpperCase() + "%\"";
		} else {
			sql += "and 1 = 3";
		}
		sql += " order by name";
		DataSource liferayDataSource = InfrastructureUtil.getDataSource();
		try {
			Statement statement = liferayDataSource.getConnection().createStatement();
			ResultSet result = statement.executeQuery(sql);
			log.debug("Run user: " + sql);
			while (result.next()) {
				org.cdph.han.model.UserCustomGroup userCustomGroup = new UserCustomGroupImpl();
				userCustomGroup.setGroupId(result.getLong("groupId"));
				userCustomGroup.setUserId(result.getLong("userId"));
				userCustomGroup.setName(result.getString("name"));
				userCustomGroup.setCreatedDate(result.getString("created_date"));
				userCustomGroup.setModifiedBy(result.getLong("modified_by_userId"));
				userCustomGroup.setDateModified(result.getDate("date_modified"));
				userCustomGroup.setDeleted(false);
				log.debug("Add userCustomGroup " + userCustomGroup);
				customGroups.add(userCustomGroup);
			}
			result.close();
			statement.close();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		final List<GroupDisplayTO> foundCustomGroups = loadCustomGroups(customGroups);
		return foundCustomGroups.toArray (new GroupDisplayTO[foundCustomGroups.size ()]);
	}

	private List<GroupDisplayTO> loadCustomGroups(List<org.cdph.han.model.UserCustomGroup> customGroups) {
		final List<GroupDisplayTO> foundCustomGroups = new ArrayList<GroupDisplayTO> ();
		UserCustomGroup customGroup;
		for (org.cdph.han.model.UserCustomGroup custGroup : customGroups) {
			try{
				customGroup = lrCustGroupToHan(custGroup);
				log.debug("Add custom group: " + customGroup.getName() + ", modified by: " + customGroup.getModifiedBy());
				if (customGroup.getModifiedBy () != null) {
					log.debug("Add with ModifyBy: " + customGroup + ".");
					foundCustomGroups.add (new GroupDisplayTO (customGroup.getGroupId (),
						customGroup.getName (),
						customGroup.getUserData ().getContact ().getFirstName () +
						" " + customGroup.getUserData ().getContact ().getLastName (),
						customGroup.getCreatedDate (),
						customGroup.getModifiedBy ().getContact ().getFirstName () + " " +
						customGroup.getModifiedBy ().getContact ().getLastName (),
						customGroup.getDateModified ()));
				} else {
					log.debug("Add unidentified: " + customGroup.getGroupId()
						+ ") " + customGroup.getName() + " : " + customGroup.getCreatedDate()
						+ ", user Data: " + customGroup.getUserData());
					foundCustomGroups.add (new GroupDisplayTO (customGroup.getGroupId (), customGroup.getName (),
						"customGroup/UserData /Contact/FirstName & LastName",
						customGroup.getCreatedDate (), "First Last", new Date()));
				}
			} catch(Exception e){
				log.error(e);
			}
		}
		return foundCustomGroups;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.custgroups.services.GroupSearchService#loadGroup(long)
	 */
	@Override
	public GroupTO loadGroup (long id) {
		/*final UserCustomGroup group = entityManager.find (UserCustomGroup.class, id);*/
		
		UserCustomGroup hgroup =null;
		org.cdph.han.model.UserCustomGroup lgroup =null;
		try{
			lgroup =  UserCustomGroupLocalServiceUtil.getUserCustomGroup(id);
		}catch(Exception e){
			log.error(e);
		}
		
		hgroup=transferUserGroup(lgroup);
		
		final List<RecipientTO> recipients = new ArrayList<RecipientTO> ();
		for (CustomGroupMember member : hgroup.getMembers ()) {//group.getMembers ()
			log.debug("member: " + member.getMemberId() + ": " + member.getNonHanUser() + ", " + member.getAreaField());
			if (member.getAreaField () != null) {
				recipients.add (new RecipientTO (member.getAreaField ().getAreaFieldId (),
						member.getAreaField ().getDescription (), RecipientType.AREA_FIELD));
			} else if (member.getChildGroup () != null) {
				recipients.add (new RecipientTO (member.getChildGroup ().getGroupId (),
						member.getChildGroup ().getName (), RecipientType.GROUP));
			} else if (member.getNonHanUser () != null) {
				recipients.add (new RecipientTO (member.getNonHanUser ().getId (),
						member.getNonHanUser ().getFirstName () + " " + member.getNonHanUser ().getLastName (),
						RecipientType.NON_HAN_USER));
			} else if (member.getNonHanUserGroup () != null) {
				recipients.add (new RecipientTO (member.getNonHanUserGroup ().getId (),
						member.getNonHanUserGroup ().getName (), RecipientType.NON_HAN_GROUP));
			} else if (member.getOrganization () != null) {
				recipients.add (new RecipientTO (member.getOrganization ().getOrganizationId (),
						member.getOrganization ().getName (), member.getOrganization ().getParentOrganizationId () == 0 ?
								RecipientType.ORGANIZATION_TYPE : RecipientType.ORGANIZATION));
			} else if (member.getRole () != null) {
				recipients.add (new RecipientTO (member.getRole ().getRoleId (),
						member.getRole ().getName (), RecipientType.ROLE));
			} else if (member.getUserData () != null) {
				recipients.add (new RecipientTO (member.getUserData ().getUserId (),
						member.getUserData ().getContact ().getFirstName () + " " +
						member.getUserData ().getContact ().getLastName (), RecipientType.USER));
			} else if (member.getCriterias () != null && !member.getCriterias ().isEmpty ()) {
				StringBuilder organization = new StringBuilder ();
				StringBuilder role = new StringBuilder ();
				StringBuilder areaField = new StringBuilder ();
				String organizationType = null;
				FilterCriteriaTO criteria = null;
				RecipientTO recipientTO = new RecipientTO ();
				for (CustomGroupCriteria dbCriteria : member.getCriterias ()) {
					criteria = new FilterCriteriaTO ();
					criteria.setOrganizationTypeId (dbCriteria.getOrganizationType ().getOrganizationId ());
					if (dbCriteria.getAreaField () != null) {
						areaField.append (dbCriteria.getAreaField ().getDescription ()).append (", ");
						criteria.setAreaFieldId ((long) dbCriteria.getAreaField ().getAreaFieldId ());
					}
					if (dbCriteria.getOrganization () != null) {
						organization.append (dbCriteria.getOrganization ().getName ()).append (", ");
						criteria.setOrganizationId (dbCriteria.getOrganization ().getOrganizationId ());
					}
					if (dbCriteria.getRole () != null) {
						role.append (dbCriteria.getRole ().getName ()).append (", ");
						criteria.setRoleId (dbCriteria.getRole ().getRoleId ());
					}
					if (organizationType == null) {
						organizationType = dbCriteria.getOrganizationType ().getName ();
					}
					recipientTO.getCriteria ().add (criteria);
				}
				StringBuilder caption = new StringBuilder ();
				caption.append (organizationType).append (" / ");
				if (organization.length () > 0) {
					caption.append (organization.substring (0, organization.lastIndexOf (", ")));
				}
				caption.append (" / ");
				if (areaField.length () > 0) {
					caption.append (areaField.substring (0, areaField.lastIndexOf (", ")));
				}
				caption.append (" / ");
				if (role.length () > 0) {
					caption.append (role.substring (0, role.lastIndexOf (", ")));
				}
				recipientTO.setName (caption.toString ());
				recipientTO.setType (RecipientType.CRITERIA);
				recipientTO.setChicagoOnly (member.isChicagoOnly ());
				recipientTO.setId (0);
				recipients.add (recipientTO);
			}
		}
		return new GroupTO (hgroup.getGroupId (), hgroup.getName (), recipients);//group.getGroupId (), group.getName ()
	}

	private UserCustomGroup transferUserGroup(org.cdph.han.model.UserCustomGroup lgroup) {
		UserCustomGroup hgroup = new UserCustomGroup();
		try{
			hgroup.setCreatedDate(new Date(lgroup.getCreatedDate()));
		}catch(Exception e){
			log.error(e);
		}
		hgroup.setDateModified(lgroup.getDateModified());
		hgroup.setDeleted(lgroup.getDeleted());
		hgroup.setGroupId(lgroup.getGroupId());
		List<org.cdph.han.model.CustomGroupMember> cgMems = CustomGroupMemberLocalServiceUtil.findByGroupId(lgroup.getGroupId());
		List<CustomGroupMember> hcgMems = new ArrayList<CustomGroupMember>();
		for(org.cdph.han.model.CustomGroupMember mem : cgMems){
			CustomGroupMember hmem = new CustomGroupMember();
			log.debug("AF: " + mem.getAreaFieldId() + ", NonHanUserGroup: " + mem.getNonHanUserGroupId() + ", NH-User: " + mem.getNonHanUserId() +
					", org: " + mem.getOrganizationId() + ", role: " + mem.getRoleId());
			if (mem.getAreaFieldId() > 0) {
				int aid = (int)mem.getAreaFieldId();
				try {
					org.cdph.han.model.AreaField mAreaField = AreaFieldLocalServiceUtil.getAreaField(mem.getAreaFieldId());
					AreaField af = new AreaField();
					af.setAreaFieldId(aid);
					af.setDescription(mAreaField.getDescription());
					hmem.setAreaField(af);
				} catch (SystemException se) { log.error("SYSTEM ERROR: " + se);
				} catch (PortalException pe) { log.error("PORTAL ERROR: " + pe); }
			}

			if (mem.getNonHanUserId() > 0) {
				try {
					org.cdph.han.model.NonHanUser nhu = NonHanUserLocalServiceUtil.getNonHanUser(mem.getNonHanUserId());
					NonHanUser nonHanUser = new NonHanUser();
					nonHanUser.setAddressLine1(nhu.getAddressLine1());
					nonHanUser.setAddressLine2(nhu.getAddressLine2());
					nonHanUser.setCity(nhu.getCity());
					nonHanUser.setFirstName(nhu.getFirstName());
					nonHanUser.setLastName(nhu.getLastName());
					nonHanUser.setId(nhu.getId_());
					nonHanUser.setMiddleInitial(nhu.getMiddleInitial());
					nonHanUser.setState(nhu.getState());
					hmem.setNonHanUser(nonHanUser);
				} catch (SystemException se) { log.error("SYSTEM ERROR: " + se);
				} catch (PortalException pe) { log.error("PORTAL ERROR: " + pe); }
			}
			if (mem.getNonHanUserGroupId() > 0) {
				try {
					org.cdph.han.model.NonHanUserGroup nhug = NonHanUserGroupLocalServiceUtil.getNonHanUserGroup(mem.getNonHanUserGroupId());
					NonHanUserGroup nonHanUserGroup = new NonHanUserGroup();
					nonHanUserGroup.setId(mem.getNonHanUserGroupId());
					nonHanUserGroup.setName(nhug.getName());
					nonHanUserGroup.setDescription(nhug.getDescription());
					nonHanUserGroup.setErrorLog(nhug.getErrorLog());
//					nonHanUserGroup.setStatus(nhug.getStatus());
					hmem.setNonHanUserGroup(nonHanUserGroup);
				} catch (SystemException se) { log.error("SYSTEM ERROR: " + se);
				} catch (PortalException pe) { log.error("PORTAL ERROR: " + pe); }
			}

			if (mem.getUserId() > 0) {
				try {
					User user = UserLocalServiceUtil.getUser(mem.getUserId());
					UserData ud = new UserData();
					ud.setUserId(mem.getUserId());
					Contact contact = new Contact();
					contact.setFirstName(user.getFirstName());
					contact.setLastName(user.getLastName());
					contact.setMiddleName(user.getMiddleName());
//					contact.setEmailAddresses(user.getEmailAddress());
					ud.setContact(contact);
					hmem.setUserData(ud);
				} catch (SystemException se) { log.error("SYSTEM ERROR: " + se);
				} catch (PortalException pe) { log.error("PORTAL ERROR: " + pe); }
			}
			if (mem.getOrganizationId() > 0) {
				try {
					com.liferay.portal.model.Organization org = OrganizationLocalServiceUtil.getOrganization(mem.getOrganizationId());
					Organization pOrg = new Organization();
					pOrg.setName(org.getName());
					pOrg.setOrganizationId(mem.getOrganizationId());
					pOrg.setParentOrganizationId(org.getParentOrganizationId());
					hmem.setOrganization(pOrg);
				} catch (SystemException se) { log.error("SYSTEM ERROR: " + se);
				} catch (PortalException pe) { log.error("PORTAL ERROR: " + pe); }
			}
			if (mem.getRoleId() > 0) {
				try {
					com.liferay.portal.model.Role role = RoleLocalServiceUtil.getRole(mem.getRoleId());
					Role pRole = new Role();
					pRole.setName(role.getName());
					pRole.setRoleId(mem.getRoleId());
					pRole.setType(role.getType());
					hmem.setRole(pRole);
				} catch (SystemException se) { log.error("SYSTEM ERROR: " + se);
				} catch (PortalException pe) { log.error("PORTAL ERROR: " + pe); }
			}
			hcgMems.add(hmem);
		}
		hgroup.setMembers(hcgMems);

		//TODO: need a query, dyn query, or finder, also 
		org.cdph.han.persistence.UserData ud = new org.cdph.han.persistence.UserData();
		ud.setUserId(lgroup.getModifiedBy());
		hgroup.setModifiedBy(ud);
		hgroup.setName(lgroup.getName());
		//TODO: this may be different than modified by
		hgroup.setUserData(ud);
		return hgroup;
	}

	/**
	 * @param entityManager the entityManager to set
	 */
	//abey...
	//@PersistenceContext
	/*public void setEntityManager (EntityManager entityManager) {
		this.entityManager = entityManager;
	}*/

}
