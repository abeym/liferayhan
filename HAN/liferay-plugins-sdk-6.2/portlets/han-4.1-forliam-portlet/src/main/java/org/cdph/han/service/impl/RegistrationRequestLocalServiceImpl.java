/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.cdph.han.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.cdph.han.facade.util.EmailFolderServiceUtil;
import org.cdph.han.facade.util.HANServiceConstants;
import org.cdph.han.model.AreaField;
import org.cdph.han.model.RegistrationRequest;
import org.cdph.han.service.RegistrationRequestLocalServiceUtil;
import org.cdph.han.service.base.RegistrationRequestLocalServiceBaseImpl;
import org.cdph.han.synch.dto.Mir3DeviceTO;
import org.cdph.han.synch.dto.Mir3UserTO;
import org.cdph.han.synch.services.UserSynchService;
import org.cdph.han.synch.services.UserSynchServiceImpl;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.PortletClassLoaderUtil;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.security.auth.CompanyThreadLocal;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;

/**
 * The implementation of the registration request local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link org.cdph.han.service.RegistrationRequestLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author zanonil
 * @see org.cdph.han.service.base.RegistrationRequestLocalServiceBaseImpl
 * @see org.cdph.han.service.RegistrationRequestLocalServiceUtil
 */
public class RegistrationRequestLocalServiceImpl
	extends RegistrationRequestLocalServiceBaseImpl {
	private static final Log LOGGER = LogFactoryUtil.getLog(RegistrationRequestLocalServiceImpl.class);
	/** Regular Organization */
	private static final int ORGANIZATION_TYPE = 1;
	/** No Region */
	private static final int ORGANIZATION_REGION_ID = 0;
	/** United States */
	private static final int ORGANIZATION_COUNTRY_ID = 19;
	/** Full Member on table ListType */
	private static final int ORGANIZATION_STATUS_ID = 12017;

	/*
	 * Key of the registered email used as primary dev pref during the
	 * registration process.
	 *
	private final String DEV_PREFERENCE_EMAIL_KEY = "PRIMARY";
	/**
	 * Key of the registered phone used as secondary dev pref during the
	 * registration process.
	 *
	private final String DEV_PREFERENCE_PHONE_KEY = "P:";*/
	/** MIR3 Admin User */
	private static final String MIR3_ADMIN_USER = "MIR3_ADMIN_USER";
	/** MIR3 Admin User Password */
	private static final String MIR3_ADMIN_PASS = "MIR3_ADMIN_PASS";
	/** MIR3 API Version key */
	private static final String API_VERSION_KEY = "MIR3_API_VERSION";
	/** MIR3 User Timezone */
	private static final String MIR3_USER_TIMEZONE = "CENTRAL_USA"; 
	/** MIR3 Default Role */
	private static final String MIR3_DEFAULT_ROLE = "Recipient";
	/** MIR3 Default Language */
	private static final String MIR3_DEFAULT_LANGUAGE = "ENGLISH";
	/** MIR3 Default Division */
	private static final String MIR3_DEFAULT_DIVISION = "/";
	/** MIR3 Default Device Description */
	private static final String MIR3_DEFAULT_DEVICE_DESC = "Office Email";
	/** MIR3 Default Device Type */
	private static final String MIR3_DEFAULT_DEVICE_TYPE = "Work Email";
	/** Key to retrieve the email domain */
	private static final String EMAIL_DOMAIN_KEY = "HAN_EMAIL_DOMAIN";
	/** Default Timezone for users */
	private static final String USER_DEFAULT_TIMEZONE_ID = "America/Chicago";	

	//TODO: these are not used and may be causing bean container failure. Need to know why htey are here?
	//private static EmailFolderServiceUtil emailFolderService  = new EmailFolderServiceUtil();
	//private static UserSynchService mir3Service = new UserSynchServiceImpl();
	
	public void custUpdate(RegistrationRequest rrm){
		
	/*	try {
			registrationRequestPersistence.update(rrm);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
		try {
			registrationRequestPersistence.update(rrm, false);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			registrationRequestPersistence.update(rrm, true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public List<RegistrationRequest> getPendingRegistrationRequests() {
		List<RegistrationRequest> registrationRequest = null;
		try {
			registrationRequest =  registrationRequestPersistence.findBystatus(HANServiceConstants.RegistrationRequestStatus.SUBMITTED.toInt());
		} catch (SystemException e) {
			LOGGER.error(e.getMessage());
		}
		return registrationRequest;
	}
	
	public List<RegistrationRequest> getPendingRegistrationRequests(int startIndex,
			int maxResults) {
		List<RegistrationRequest> registrationRequest = null;
		try {
			registrationRequest =  registrationRequestPersistence.findBystatus(HANServiceConstants.RegistrationRequestStatus.SUBMITTED.toInt(), startIndex, startIndex+maxResults);
		} catch (SystemException e) {
			LOGGER.error(e.getMessage());
		}
		return registrationRequest;
	}
	
	public int getPendingRegistrationRequestsCount() {
		int count = 0;
		try {
			count = registrationRequestPersistence.countBystatus(HANServiceConstants.RegistrationRequestStatus.SUBMITTED.toInt());
		} catch (SystemException e) {
			LOGGER.error(e.getMessage());
		}
		return count;
	}

	public Organization getOrganization(Long id) {
		
		try {
			return OrganizationLocalServiceUtil.getOrganization(id);
		} catch (PortalException e) {
			LOGGER.error(e.getMessage());
		} catch (SystemException e) {
			LOGGER.error(e.getMessage());
		}
		return null;
	}
	
	public AreaField getAreaField(Integer id) {
		try {
			return areaFieldPersistence.fetchByPrimaryKey(id);
		} catch (SystemException e) {
			LOGGER.error(e.getMessage());
		}
		return null;
	}
	
	public Role getRole(Long id) {
		try {
			return RoleLocalServiceUtil.getRole(id);
		} catch (PortalException e) {
			LOGGER.error(e.getMessage());
		} catch (SystemException e) {
			LOGGER.error(e.getMessage());
		}
		return null;
	}
	
	public List<Organization> getOrganizationTypes() {
		List<Organization> organizationTypes = Collections.EMPTY_LIST;

		try {
			organizationTypes = OrganizationLocalServiceUtil.getOrganizations(CompanyThreadLocal.getCompanyId(), 0L);
		} catch (SystemException e) {
			LOGGER.error(e.getMessage());
		}

		return organizationTypes;
	}
	
	public List<Organization> getSuborganizations(Long parentOrgId) {
		List<Organization> organizationTypes = Collections.EMPTY_LIST;

		try {
			organizationTypes = OrganizationLocalServiceUtil.getOrganizations(CompanyThreadLocal.getCompanyId(), parentOrgId);
		} catch (SystemException e) {
			LOGGER.error(e.getMessage());
		}

		return organizationTypes;
	}
	
	public List<Role> getOrganizationalRoles(Long orgTypeId) {
		try {
			final Organization orgType = OrganizationLocalServiceUtil.getOrganization(orgTypeId);
		} catch (PortalException e) {
			LOGGER.error(e.getMessage());
		} catch (SystemException e) {
			LOGGER.error(e.getMessage());
		}
		final List<Role> roles = new ArrayList<Role> ();
		//for (Role role : orgType.getRoles ()) {
		//roles.add (role);
		//}
		return roles;
	}
	
	public List<AreaField> getAreaFields(Long organizationType) {
		try {
			return areaFieldPersistence.findByorganizationId(organizationType);
		} catch (SystemException e) {
			LOGGER.error(e.getMessage());
		}
		return null;
	}
	
	public boolean usernameExist(String username) {
		 User user = null;
		try {
			user = UserLocalServiceUtil.getUserByScreenName(CompanyThreadLocal.getCompanyId(),username);
		} catch (PortalException e) {
			LOGGER.error(e.getMessage());
		} catch (SystemException e) {
			LOGGER.error(e.getMessage());
		}
		if (user !=null)
			return true;
		
		DynamicQuery dq = DynamicQueryFactoryUtil.forClass(RegistrationRequest.class, PortletClassLoaderUtil.getClassLoader());
		dq.add(RestrictionsFactoryUtil.ne("status", HANServiceConstants.RegistrationRequestStatus.REJECTED.toInt()));
		dq.add(RestrictionsFactoryUtil.eq("userName", username));
		dq.setProjection(ProjectionFactoryUtil.rowCount());
		long registrationReqCount = 0L;
		try {
			registrationReqCount = registrationRequestPersistence.countWithDynamicQuery(dq);
		} catch (SystemException e) {
			LOGGER.error(e.getMessage());
		}
		return registrationReqCount > 0;
	}
	
	
	public boolean emailExist(String email) {
		 User user = null;
		try {
			user = UserLocalServiceUtil.getUserByEmailAddress(CompanyThreadLocal.getCompanyId(),email);
		} catch (PortalException e) {
			LOGGER.error(e.getMessage());
		} catch (SystemException e) {
			LOGGER.error(e.getMessage());
		}
		if (user !=null)
			return true;
		
		DynamicQuery dq = DynamicQueryFactoryUtil.forClass(RegistrationRequest.class, PortletClassLoaderUtil.getClassLoader());
		dq.add(RestrictionsFactoryUtil.ne("status", HANServiceConstants.RegistrationRequestStatus.REJECTED.toInt()));
		dq.add(RestrictionsFactoryUtil.ne("status", HANServiceConstants.RegistrationRequestStatus.APPROVED.toInt()));
		dq.add(RestrictionsFactoryUtil.eq("email", email));
		dq.setProjection(ProjectionFactoryUtil.rowCount());
		long registrationReqCount = 0L;
		try {
			registrationReqCount = registrationRequestPersistence.countWithDynamicQuery(dq);
		} catch (SystemException e) {
			LOGGER.error(e.getMessage());
		}
		return registrationReqCount > 0;
	}
	
	// This method needs to be moved in controller as services are available in han-alerts-portlet
	



private List<Mir3UserTO> createMir3TO(RegistrationRequest request, String generatedPassword){
		
		Mir3UserTO mir3User = new Mir3UserTO();
		AreaField areaField = null;
		try {
			areaField = areaFieldPersistence.fetchByPrimaryKey(request.getAreaField());
		} catch (SystemException e) {
			LOGGER.error(e.getMessage());
		}
		if(areaField==null) {
			return null;
		}
		mir3User.setAreaField(areaField.getDescription());
		mir3User.setDepartment(request.getDepartment());
		mir3User.setFirstName(request.getFirstName());
		mir3User.setLastName(request.getLastName());
		mir3User.setOrganization(request.getOrganizationName());
		Organization type = getOrganization(request.getOrganizationType());
		if(type!=null)
		mir3User.setOrganizationType(type.getName());
		mir3User.setPassword(generatedPassword);
		Role role =null;
		try {
			role = RoleLocalServiceUtil.getRole(request.getRole());
		} catch (PortalException e) {
			LOGGER.error(e.getMessage());
		} catch (SystemException e) {
			LOGGER.error(e.getMessage());
		}
		if(role!=null) {
			mir3User.setRole(role.getName());
		}
		mir3User.setDivision(MIR3_DEFAULT_DIVISION);
		mir3User.setLanguage(MIR3_DEFAULT_LANGUAGE);
		mir3User.setMir3Role(MIR3_DEFAULT_ROLE);
		mir3User.setTimezone(MIR3_USER_TIMEZONE);
		mir3User.setTitle(request.getTitle());
		mir3User.setUsername(request.getEmail());

		mir3User.setDevices(new ArrayList<Mir3DeviceTO>());

		Mir3DeviceTO device = new Mir3DeviceTO();
		device.setAddress(request.getEmail());
		device.setDescription(MIR3_DEFAULT_DEVICE_DESC);
		device.setType(MIR3_DEFAULT_DEVICE_TYPE);
		device.setPriority ("1"); // Moved default preference to the device
		mir3User.getDevices().add(device);
		device = new Mir3DeviceTO ();
		if (request.getWorkTelephoneExt () != null && request.getWorkTelephoneExt ().length () > 0) {
			device.setAddress (request.getWorkTelephone () + 'x' + request.getWorkTelephoneExt ());
		} else {
			device.setAddress (request.getWorkTelephone ());
		}
		device.setDescription ("Work Phone");
		device.setPriority ("2");
		device.setType ("Work Phone");
		mir3User.getDevices().add(device);

		/*
		mir3User.setPreferences(new ArrayList<Mir3DevicePriorityTO>());
		Mir3DevicePriorityTO priority = new Mir3DevicePriorityTO();
		priority.setIndex(1);
		priority.setPriority(1);
		mir3User.getPreferences().add(priority);
		*/			 

		List<Mir3UserTO> recipients = new ArrayList<Mir3UserTO>();
		recipients.add(mir3User);
		
		return recipients;
	}
	
	// TODO :  JMS Template is pending to be implemented
	public void enqueueNotificationEmail(Long registrationReqId) {
		LOGGER.info("In sendNotificationEmail(Long registrationReqId)...");
		//jmsTemplate.convertAndSend(registrationReqId);
	}	
	
	
	

	

}