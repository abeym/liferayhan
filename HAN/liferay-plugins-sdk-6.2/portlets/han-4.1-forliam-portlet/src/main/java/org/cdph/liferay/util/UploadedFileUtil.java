package org.cdph.liferay.util;

/**
 * @author  Abey Mani
 */
public class UploadedFileUtil {

	// Public Constants
	public static final String USER_PORTRAIT = "user-portrait-";

	// Private Constants
	private static final String JAVA_IO_TMPDIR = "java.io.tmpdir";
	private static final String TMP = ".tmp";

	public static String createFileName(String uploadedFileId) {
		return USER_PORTRAIT + uploadedFileId + TMP;
	}

	public static String getTempDir() {
		return System.getProperty(JAVA_IO_TMPDIR);
	}
}
