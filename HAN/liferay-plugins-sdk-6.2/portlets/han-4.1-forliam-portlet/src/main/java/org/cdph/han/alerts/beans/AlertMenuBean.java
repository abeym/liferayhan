package org.cdph.han.alerts.beans;

import java.io.IOException;
import java.util.Enumeration;

import javax.faces.context.FacesContext;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import org.cdph.han.alerts.components.MenuUserObject;
import org.cdph.han.alerts.components.MenuUserObject.SearchType;
import org.cdph.han.util.services.SecurityService;
import org.cdph.han.util.services.SecurityServiceImpl;

/**
 * Baked bean used to manage the state of the Tree Menu for the alert sorting
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class AlertMenuBean {
	/** Tree model */
	private DefaultTreeModel model;
	/** Current selected search */
	private MenuUserObject currentSelection;
	/** The last outcome */
	private String lastOutcome = "searchCurrentDate";
	/** Keep track of the previously selected search */
	private String previousOutcome = "searchCurrentDate";
	/** Service for looking up the users role */
	private SecurityService securityService;
	/** Last executed search */
	private SearchType lastExecuted;
	/** The search to execute */
	private SearchType toExecute;
	/** The previously executed search */
	private SearchType previouslyExecuted;

	/**
	 * Default constructor
	 */
	public AlertMenuBean () {
	}

	/**
	 * Adds a new node to the provided parent node
	 * @param parent DefaultMutableTreeNode with the parent
	 * @param title String with the title of the node
	 * @param leaf flag to distinguish leaf from branches
	 * @param expanded flag to render the branch expanded
	 * @return DefaultMutableTreeNode after being initialized
	 */
	private DefaultMutableTreeNode addNode (final DefaultMutableTreeNode parent, final String title,
			final boolean leaf, final boolean expanded, final MenuUserObject.SearchType type) {
		DefaultMutableTreeNode node = new DefaultMutableTreeNode ();
		MenuUserObject userObject = new MenuUserObject (node);
		userObject.setType (type);
		userObject.setLeaf (leaf);
		userObject.setExpanded (expanded);
		userObject.setText (title);
		node.setUserObject (userObject);
		if (parent != null) {
			parent.add (node);
		}
		return node;
	}

	public void ensureService(){
		securityService= securityService==null?new SecurityServiceImpl():securityService;
	}
	
	/**
	 * Populates the menu tree
	 */
	private void init () {
		// Get the users profile
		// The root branch
		ensureService();
		final boolean admin = securityService.isUserAdmin ();
		final DefaultMutableTreeNode root = addNode (null, "Alerts", false, true, SearchType.ROOT_MENU);
		root.setAllowsChildren (true);
		model = new DefaultTreeModel (root);
		currentSelection = (MenuUserObject) root.getUserObject ();
		currentSelection.setExpanded (true);
		// My Notifications search
		addNode (root, "Alerts Requiring Action", true, true, SearchType.ALERTS_REQUIRING_ACTION).setAllowsChildren (false);
		// Current By searches branch
		DefaultMutableTreeNode node = addNode (root, "Current By", false, false, SearchType.CURRENT_MENU);
		node.setAllowsChildren (true);
		addNode (node, "Publication Date", true, true, SearchType.CURRENT_BY_PUBLICATION_DATE).setAllowsChildren (
				false);
		addNode (node, "Topic", true, true, SearchType.CURRENT_BY_TOPIC).setAllowsChildren (false);
		addNode (node, "Level of Alert", true, true, SearchType.CURRENT_BY_LEVEL_OF_ALERT).setAllowsChildren (
				false);
		// Options only available to admin users
		if (admin) {
			// Archived By searches branch
			node = addNode (root, "Archived By", false, false, SearchType.ARCHIVED_MENU);
			node.setAllowsChildren (true);
			addNode (node, "Publication Date", true, true, SearchType.ARCHIVED_BY_PUBLICATION_DATE).setAllowsChildren (
					false);
			addNode (node, "Topic", true, true, SearchType.ARCHIVED_BY_TOPIC).setAllowsChildren (false);
			addNode (node, "Level of Alert", true, true, SearchType.ARCHIVED_BY_LEVEL_OF_ALERT).setAllowsChildren (
					false);
			// Draft By search branch
			node = addNode (root, "Draft By", false, false, SearchType.DRAFT_MENU);
			node.setAllowsChildren (true);
			addNode (node, "Last Modified Date", true, true,
					SearchType.DRAFT_BY_LAST_MODIFIED).setAllowsChildren (false);
			// Scheduled By search branch
			node = addNode (root, "Scheduled By", false, false, SearchType.SCHEDULED_MENU);
			node.setAllowsChildren (true);
			addNode (node, "Scheduled Date", true, true,
					SearchType.SCHEDULED_BY_SCHEDULED_DATE).setAllowsChildren (false);
			// Abort Track Alerts
			addNode (root, "Abort Track Alerts", true, true,
					SearchType.ABORT_TRACK_ALERTS).setAllowsChildren (false);
		}
	}

	/**
	 * Action for retrieving the previous outcome, used on operations like alert archiving and deleting
	 * @return previous outcome
	 */
	public String previouslySelectedSearch () {
		return previousOutcome;
	}

	/**
	 * Action for retrieving the last outcome
	 * @return last outcome
	 */
	public String lastSearch () {
		return lastOutcome;
	}

	/**
	 * Action for selecting the outcome of the search performed
	 * @return outcome
	 * @throws IOException 
	 */
	public String searchSelected () throws IOException {
		String outcome = lastOutcome;
		final String selected = (String) FacesContext.getCurrentInstance ().getExternalContext (
				).getRequestParameterMap ().get ("selectedSearch");
		SearchType type = selected == null || selected.equals ("") ? null : SearchType.valueOf (selected);
		if (type == null) {
			type = toExecute;
			toExecute = null;
		}
		final DefaultMutableTreeNode node = findNode (type);
		if (node != null) {
			currentSelection = (MenuUserObject) node.getUserObject ();
			if (!node.isLeaf ()) {
				currentSelection.setExpanded (!currentSelection.isExpanded ());
			}
		}
		if (SearchType.DRAFT_BY_LAST_MODIFIED.equals (type)) {
			outcome = "searchDrafts";
			previousOutcome = lastOutcome;
			previouslyExecuted = lastExecuted;
			lastOutcome = outcome;
			lastExecuted = type;
		} else if (SearchType.CURRENT_BY_PUBLICATION_DATE.equals (type)) {
			outcome = "searchCurrentDate";
			previousOutcome = lastOutcome;
			lastOutcome = outcome;
			previouslyExecuted = lastExecuted;
			lastExecuted = type;
		} else if (SearchType.CURRENT_BY_TOPIC.equals (type)) {
			outcome = "searchCurrentTopic";
			previousOutcome = lastOutcome;
			lastOutcome = outcome;
			previouslyExecuted = lastExecuted;
			lastExecuted = type;
		} else if (SearchType.CURRENT_BY_LEVEL_OF_ALERT.equals (type)) {
			outcome = "searchCurrentLevel";
			previousOutcome = lastOutcome;
			lastOutcome = outcome;
			previouslyExecuted = lastExecuted;
			lastExecuted = type;
		} else if (SearchType.ARCHIVED_BY_PUBLICATION_DATE.equals (type)) {
			outcome = "searchCurrentDate";
			previousOutcome = lastOutcome;
			lastOutcome = outcome;
			previouslyExecuted = lastExecuted;
			lastExecuted = type;
		} else if (SearchType.ARCHIVED_BY_TOPIC.equals (type)) {
			outcome = "searchCurrentTopic";
			previousOutcome = lastOutcome;
			lastOutcome = outcome;
			previouslyExecuted = lastExecuted;
			lastExecuted = type;
		} else if (SearchType.ARCHIVED_BY_LEVEL_OF_ALERT.equals (type)) {
			outcome = "searchCurrentLevel";
			previousOutcome = lastOutcome;
			lastOutcome = outcome;
			previouslyExecuted = lastExecuted;
			lastExecuted = type;
		} else if (SearchType.SCHEDULED_BY_SCHEDULED_DATE.equals (type)) {
			outcome = "searchScheduled";
			previousOutcome = lastOutcome;
			lastOutcome = outcome;
			previouslyExecuted = lastExecuted;
			lastExecuted = type;
		} else if (SearchType.SHOW_ALERT_DETAIL.equals (type)) {
			outcome = "showDetails";
			previousOutcome = lastOutcome;
			lastOutcome = outcome;
			previouslyExecuted = lastExecuted;
			lastExecuted = type;
		} else if (SearchType.ALERTS_REQUIRING_ACTION.equals (type)) {
			outcome = "requiringAction";
			previousOutcome = lastOutcome;
			lastOutcome = outcome;
			previouslyExecuted = lastExecuted;
			lastExecuted = type;
		} else if (SearchType.ABORT_TRACK_ALERTS.equals (type)) {
			outcome = "trackingResults";
			previousOutcome = lastOutcome;
			lastOutcome = outcome;
			previouslyExecuted = lastExecuted;
			lastExecuted = type;
		} else if (SearchType.ABORT_TRACK_ALERTS_DETAIL.equals (type)) {
			outcome = "trackingDetail";
			previousOutcome = lastOutcome;
			lastOutcome = outcome;
			previouslyExecuted = lastExecuted;
			lastExecuted = type;
		} else if (SearchType.ABORT_TRACK_ALERTS_EXTENDED_DETAIL.equals (type)) {
			outcome = "trackingExtDetail";
			previousOutcome = lastOutcome;
			lastOutcome = outcome;
			previouslyExecuted = lastExecuted;
			lastExecuted = type;
		}
		return outcome;
	}

	/**
	 * Search for the node with the given SearchType
	 * @param type SearchType for finding the node
	 * @return DefaultMutableTreeNode with the given SearchType
	 */
	private DefaultMutableTreeNode findNode (final SearchType type) {
		if(model==null)init ();
		final DefaultMutableTreeNode rootNode = (DefaultMutableTreeNode) model.getRoot ();
		DefaultMutableTreeNode node = null;
		final Enumeration<?> children = rootNode.depthFirstEnumeration ();
		while (children.hasMoreElements ()) {
			node = (DefaultMutableTreeNode) children.nextElement ();
			if (((MenuUserObject) node.getUserObject ()).getType ().equals (type)) {
				break;
			}
			node = null;
		}
		return node;
	}

	/**
	 * @return the model
	 */
	public DefaultTreeModel getModel () {
		return model;
	}

	/**
	 * @param model the model to set
	 */
	public void setModel (DefaultTreeModel model) {
		this.model = model;
	}

	/**
	 * @return the currentSelection
	 */
	public MenuUserObject getCurrentSelection () {
		return currentSelection;
	}

	/**
	 * @param currentSelection the currentSelection to set
	 */
	public void setCurrentSelection (MenuUserObject currentSelection) {
		this.currentSelection = currentSelection;
	}

	/**
	 * @return the lastOutcome
	 */
	public String getLastOutcome () {
		return lastOutcome;
	}

	/**
	 * @param lastOutcome the lastOutcome to set
	 */
	public void setLastOutcome (String lastOutcome) {
		this.lastOutcome = lastOutcome;
	}

	/**
	 * @param securityService the securityService to set
	 */
	public void setSecurityService (SecurityService securityService) {
		/*this.securityService = securityService;
		if(this.securityService==null){*/
			this.securityService = new SecurityServiceImpl();
		//}
		init ();
	}

	/**
	 * @return if there is an alert to copy
	 */
	public boolean isCanCopy () {
		return SearchType.SHOW_ALERT_DETAIL.equals (lastExecuted);
	}

	/**
	 * @return the toExecute
	 */
	public String getToExecute () {
		return toExecute.toString ();
	}

	/**
	 * @param toExecute the toExecute to set
	 */
	public void setToExecute (String toExecute) {
		this.toExecute = SearchType.valueOf (toExecute);
	}

	/**
	 * @return the lastExecuted
	 */
	public SearchType getLastExecuted () {
		return lastExecuted;
	}

	/**
	 * @return the previouslyExecuted
	 */
	public SearchType getPreviouslyExecuted () {
		return previouslyExecuted;
	}

}
