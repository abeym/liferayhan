package org.cdph.han.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SecondaryTable;
import javax.persistence.SecondaryTables;
import javax.persistence.Table;

/**
 * Entity class representing an address in the HAN system.
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Entity
@Table (name = "Address", schema = "lportal")
@SecondaryTables ({
	@SecondaryTable (name = "address_han_data", schema = "lportal",
		pkJoinColumns = @PrimaryKeyJoinColumn (referencedColumnName = "addressId", name = "address_id"))
})
@NamedQueries (value = {
	@NamedQuery (
			name = "Address.findByClassIdPK",
			query = "select a from Address a where a.classNameId = :classNameId and a.classPK = :classPK and upper (a.city) like :city"
	),
	@NamedQuery (
			name = "Address.findByClassPK",
			query = "select a from Address a where a.classNameId = :classNameId and a.classPK = :classPK"
	)
})
public class Address implements Serializable {
	/** Class serial version UID */
	private static final long serialVersionUID = 5122443548238850989L;
	/** Address ID */
	@Id
	@Column (name = "addressId")
	private long addressId;
	/** Class name ID */
	@Column (name = "classNameId")
	private long classNameId;
	/** Class primary key */
	@Column (name = "classPK")
	private long classPK;
	/** Name of the city */
	@Column (name = "city")
	private String city;
	/** Street Address 1 */
	@Column (name = "street1")
	private String street1;
	/** Street Address 2 */
	@Column (name = "street2")
	private String street2;
	/** Street Address 3 */
	@Column (name = "street3")
	private String street3;
	/** Address Zip code */
	@Column (name = "zip")
	private String zip;
	/** Address floor */
	@Column (name = "floor", table = "address_han_data")
	private String floor;
	/** Region */
	@OneToOne
	@JoinColumn (name = "regionId")
	private Region region;

	/**
	 * @return the addressId
	 */
	public long getAddressId () {
		return addressId;
	}

	/**
	 * @param addressId the addressId to set
	 */
	public void setAddressId (long addressId) {
		this.addressId = addressId;
	}

	/**
	 * @return the classNameId
	 */
	public long getClassNameId () {
		return classNameId;
	}

	/**
	 * @param classNameId the classNameId to set
	 */
	public void setClassNameId (long classNameId) {
		this.classNameId = classNameId;
	}

	/**
	 * @return the classPK
	 */
	public long getClassPK () {
		return classPK;
	}

	/**
	 * @param classPK the classPK to set
	 */
	public void setClassPK (long classPK) {
		this.classPK = classPK;
	}

	/**
	 * @return the city
	 */
	public String getCity () {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity (String city) {
		this.city = city;
	}

	/**
	 * @return the street1
	 */
	public String getStreet1 () {
		return street1;
	}

	/**
	 * @param street1 the street1 to set
	 */
	public void setStreet1 (String street1) {
		this.street1 = street1;
	}

	/**
	 * @return the street2
	 */
	public String getStreet2 () {
		return street2;
	}

	/**
	 * @param street2 the street2 to set
	 */
	public void setStreet2 (String street2) {
		this.street2 = street2;
	}

	/**
	 * @return the street3
	 */
	public String getStreet3 () {
		return street3;
	}

	/**
	 * @param street3 the street3 to set
	 */
	public void setStreet3 (String street3) {
		this.street3 = street3;
	}

	/**
	 * @return the zip
	 */
	public String getZip () {
		return zip;
	}

	/**
	 * @param zip the zip to set
	 */
	public void setZip (String zip) {
		this.zip = zip;
	}

	/**
	 * @return the floor
	 */
	public String getFloor () {
		return floor;
	}

	/**
	 * @param floor the floor to set
	 */
	public void setFloor (String floor) {
		this.floor = floor;
	}

	/**
	 * @return the region
	 */
	public Region getRegion () {
		return region;
	}

	/**
	 * @param region the region to set
	 */
	public void setRegion (Region region) {
		this.region = region;
	}

}
