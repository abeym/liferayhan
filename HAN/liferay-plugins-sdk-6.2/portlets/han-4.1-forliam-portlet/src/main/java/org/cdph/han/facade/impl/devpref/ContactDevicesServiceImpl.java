package org.cdph.han.facade.impl.devpref;

import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.service.EmailAddressLocalServiceUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.alerts.services.UserDataService;
import org.cdph.han.custgroups.dto.UserDisplayTO;
import org.cdph.han.devpref.services.*;
import org.cdph.han.persistence.Address;
import org.cdph.han.persistence.AlternateContact;
import org.cdph.han.persistence.Contact;
import org.cdph.han.persistence.ContactPreference;
import org.cdph.han.persistence.EmailAddress;
import org.cdph.han.persistence.ListType;
import org.cdph.han.persistence.Organization;
import org.cdph.han.persistence.PhoneDevice;
import org.cdph.han.persistence.UserData;
import org.cdph.han.persistence.UserOrganizationalData;
import org.cdph.han.service.AlternateContactLocalServiceUtil;
import org.cdph.han.service.ContactPreferenceLocalServiceUtil;
import org.cdph.han.service.PhoneDeviceLocalServiceUtil;
import org.cdph.han.service.UserDataLocalServiceUtil;
import org.cdph.han.synch.dto.Mir3DeviceTO;
import org.cdph.han.synch.dto.Mir3UserTO;
import org.cdph.han.synch.services.UserSynchService;
import org.cdph.han.util.services.HanPropertiesService;
import org.cdph.han.util.services.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

/**
 * Implementation of the ContactDevicesService interface using the UserDataService to get the current user information
 * @author Abey Mani
 * @version 1.0
 */
@Repository
@Service ("contactDevicesService")
public class ContactDevicesServiceImpl implements ContactDevicesService {
	/** Class logger */
	private static final Log log = LogFactory.getLog (ContactDevicesServiceImpl.class);
	public ContactDevicesServiceImpl()
	{
		System.out.println("\n\n >>>>>>>>>>>>ContactDevicesServiceImpl:"+new Throwable("For testing !!!"));
	}
	/** Max length for custom fields */
	private static final int MAX_LENGTH = 100;
	/** UserDataService used to get access to the current user data */
	@Autowired
	private UserDataService userDataService;
	/** Persistence context */
	private EntityManager entityManager;
	/** Service to synchronize users */
	@Autowired
	private UserSynchService userSynchService;
	/** Service to retrieve the mir3 user credentials */
	@Autowired
	private HanPropertiesService hanPropertiesService;
	/** Service to check the user role */
	@Autowired
	private SecurityService securityService;

	/* (non-Javadoc)
	 * @see org.cdph.han.devpref.services.ContactDevicesService#loadContactDevices(org.cdph.han.alerts.services.UserDataService)
	 */
	public List<SelectItem> loadContactDevices () {
		log.info("In loadContactDevices");
		final List<SelectItem> devices = new ArrayList<SelectItem> ();
		final UserData userData = userDataService.getCurrentUserData ();
		log.info("userData: " + userData + ", userData.email: " + userData.getEmail());
		SelectItem device = new SelectItem ("PRIMARY", userData.getEmail ());
		devices.add (device);
		if (userData.getContact() != null) {
			log.info("userData.contact: last " + userData.getContact().getLastName() + ", " + userData.getContact().getFirstName());
		} else 	log.info("userData.contact: " + userData.getContact());
		log.info("userData.contact:phonedevices: " + userData.getContact().getPhoneDevices());
		for (PhoneDevice phoneDevice : userData.getContact ().getPhoneDevices ()) {
			device = new SelectItem ();
			if (phoneDevice.getFriendlyName () != null && phoneDevice.getFriendlyName ().length () > 0) {
				device.setLabel (phoneDevice.getFriendlyName () + " - " + phoneDevice.getPhoneNumber ());
			} else {
				device.setLabel (phoneDevice.getPhoneNumber ());
			}
			device.setValue ("P:" + phoneDevice.getPhoneId ());
			devices.add (device);
		}
		for (EmailAddress email : userData.getContact ().getEmailAddresses ()) {
			device = new SelectItem ();
			if (email.getFriendlyName () != null && email.getFriendlyName ().length () > 0) {
				device.setLabel (email.getFriendlyName () + " - " + email.getAddress ());
			} else {
				device.setLabel (email.getAddress ());
			}
			device.setValue ("M:" + email.getEmailId ());
			devices.add (device);
		}
		return devices;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.devpref.services.ContactDevicesService#loadAlternateContacts()
	 */
	public List<UserDisplayTO> loadAlternateContacts () {
		final List<UserDisplayTO> alternateContacts = new ArrayList<UserDisplayTO> ();
		final UserData userData = userDataService.getCurrentUserData ();
		if (userData.getAlternateContacts() == null) return alternateContacts;
		for (AlternateContact alternateContact : userData.getAlternateContacts ()) {
			alternateContacts.add (new UserDisplayTO (alternateContact.getAlternateContact ().getUserId (),
					alternateContact.getAlternateContact ().getContact ().getFirstName (),
					alternateContact.getAlternateContact ().getContact ().getLastName (),
					"", ""));
		}
		return alternateContacts;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.devpref.services.ContactDevicesService#loadPreferences()
	 */
	public String[] loadPreferences () {
		log.info("load Preferences");
		final String[] preferences = new String[6];
		final UserData userData = userDataService.getCurrentUserData ();
		for (ContactPreference contactPreference : userData.getContactPreferences ()) {
			log.info("(Load) Contact Preference: " + contactPreference.getIndex ()
						+ " phone " + contactPreference.getPhoneDevice() + " email " + contactPreference.getEmailAddress());
			if (contactPreference.getPrimaryEmail ()) {
				preferences[contactPreference.getIndex ()] = "PRIMARY";
				log.info("(Load) Contact Primary Preference: " + contactPreference.getIndex () + ", " + preferences[contactPreference.getIndex()]);
			} else if (contactPreference.getEmailAddress () != null) {
				preferences[contactPreference.getIndex ()] = "M:" + contactPreference.getEmailAddress ().getEmailId ();
			} else if (contactPreference.getPhoneDevice () != null) {
				preferences[contactPreference.getIndex ()] = "P:" + contactPreference.getPhoneDevice ().getPhoneId ();
				log.info("(Load) Contact Phone Preference " + contactPreference.getIndex () + ", " + preferences[contactPreference.getIndex()]);
			}
		}
		log.info("(Load) Contact Preferences: " + preferences.length);
		return preferences;
	}

	public void savePreferences (final String[] preferences, final List<UserDisplayTO> alternateContacts,
			final Long userId) {
		try {
			savePreferences (preferences, alternateContacts, null, null, null, userId);
		} catch (SystemException se) { log.error(se);
		} catch (PortalException pe) { log.error(pe);}
	}
	
	/* (non-Javadoc)
	 * @see org.cdph.han.devpref.services.ContactDevicesService#savePreferences(java.lang.String[], java.util.List)
	 */
	public void savePreferences (final String[] preferences, final List<UserDisplayTO> alternateContacts,
			final String telephonyId, final String telephonyPIN, final String telephonyPass) {
		final UserData userData = userDataService.getCurrentUserData ();
		try {
			savePreferences (preferences, alternateContacts, telephonyId,
					telephonyPIN, telephonyPass, userData.getUserId());
		} catch (SystemException se) { log.error(se);
		} catch (PortalException pe) { log.error(pe);}
	}

	/**
	 * @param entityManager the entityManager to set
	 */
	public void setEntityManager (EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	/**
	 * Saves the devices preferences and updates the telephony data
	 * @param preferences list
	 * @param alternateContacts list
	 * @param telephonyId new id
	 * @param telephonyPIN new pin
	 * @param telephonyPass new pass
	 * @param userId
	 */
	private void savePreferences (final String[] preferences, final List<UserDisplayTO> alternateContacts,
			final String telephonyId, final String telephonyPIN, final String telephonyPass, final Long userId)
			throws SystemException, PortalException {
		final UserData userData = userDataService.getUser(userId);
		// Retrieve the current contact preferences
		Collection<ContactPreference> dbPreferences = userData.getContactPreferences ();
		List<ContactPreference> toRemove = new ArrayList<ContactPreference> ();
		if (dbPreferences != null && !dbPreferences.isEmpty ()) {	// Determine which existing preferences need to be updated and which are to be removed.
			for (ContactPreference preference : dbPreferences) { 	// Update existing Contact Preference.
				if (preferences[preference.getIndex ()] != null && !preferences[preference.getIndex ()].equals ("")) {
					preference.setUserData(userData);
					if (preferences[preference.getIndex ()].equals ("PRIMARY")) {
						preference.setPrimaryEmail (true);
						preference.setPhoneDevice (new PhoneDevice());
						preference.setEmailAddress (new EmailAddress());
					} else if (preferences[preference.getIndex ()].startsWith ("M:")) {
						preference.setPrimaryEmail (false);
						preference.setPhoneDevice (new PhoneDevice());
						com.liferay.portal.model.EmailAddress emailAddress =
									EmailAddressLocalServiceUtil.getEmailAddress(Long.parseLong (preferences[preference.getIndex ()].substring (2)));
						org.cdph.han.persistence.EmailAddress pEmailAddress = convertEmailAddress(emailAddress);
						preference.setEmailAddress (pEmailAddress);
					} else if (preferences[preference.getIndex ()].startsWith ("P:")) {
						preference.setPrimaryEmail (false);
						org.cdph.han.model.PhoneDevice phoneDevice = PhoneDeviceLocalServiceUtil.getPhoneDevice(
								Long.parseLong (preferences[preference.getIndex ()].substring (2)));
						log.debug("Save phone " + preference.getIndex () + " - "+ preferences[preference.getIndex ()] + "> " + phoneDevice);
						preference.setPhoneDevice (convertPhoneDevice(phoneDevice));
						preference.setEmailAddress (new EmailAddress());
					}
					updateContactPreference(preference);
				} else {
					toRemove.add (preference);
				}
			}

			// Add existing preferences.
			ContactPreference preference = null;
			log.debug("Preferences " + preferences.length + ".");
			for (int i = dbPreferences.size (); i < preferences.length; i++) {
				preference = new ContactPreference ();
				preference.setUserData (userData);
				preference.setIndex (i);
				log.debug("Add preference " + i + ", preference " + preferences[preference.getIndex ()]);
				long prefId = 0;
				if (preferences[preference.getIndex ()] != null) {
					prefId = Integer.parseInt(preferences[preference.getIndex ()].substring (2));
				}
				if (preferences[i] == null || preferences[i].equals ("")) {
					continue;
				} else if (preferences[i].equals ("PRIMARY")) {
					preference.setPrimaryEmail (true);
				} else if (preferences[i].startsWith ("M:")) {
					log.debug("Adding Email Preference: " + prefId);
					if (prefId > 1) {
						com.liferay.portal.model.EmailAddress emailAddress = createEmailAddress(prefId);
						org.cdph.han.persistence.EmailAddress pEmailAddress = convertEmailAddress(emailAddress);
						preference.setEmailAddress (pEmailAddress);
					}
					preference.setPrimaryEmail (false);
				} else if (preferences[i].startsWith ("P:")) {
					org.cdph.han.model.PhoneDevice phoneDevice;
					log.debug("Adding Phone preference " + prefId);
					if (prefId < 1) {
						phoneDevice = PhoneDeviceLocalServiceUtil.getPhoneDevice(new Long(prefId));
					} else {
						phoneDevice = createPhoneDevice(prefId);
					}
					preference.setPhoneDevice (convertPhoneDevice(phoneDevice));
					preference.setPrimaryEmail (false);
				}
				userData.getContactPreferences ().add (preference);
				createContactPreference(preference);
			}
			if (toRemove.size () > 0) {		// Remove unused preferences.
				for (ContactPreference pref : toRemove) {
					ContactPreferenceLocalServiceUtil.deleteContactPreference(pref.getId());
					log.debug("Remove " + pref);
					userData.getContactPreferences ().remove (pref);
				}
			}
		} else {		// Add new preferences.
			userData.setContactPreferences (new ArrayList<ContactPreference> ());
			ContactPreference preference = null;
			for (int i = 0; i < preferences.length; i++) {
				log.debug("Preference " + i);
				preference = new ContactPreference ();
				preference.setUserData (userData);
				preference.setIndex (i);
				if (preferences[i] == null) {
					continue;
				} else if (preferences[i].equals ("PRIMARY")) {
					preference.setPrimaryEmail (true);
				} else if (preferences[i].startsWith ("M:")) {
					com.liferay.portal.model.EmailAddress emailAddress =
							EmailAddressLocalServiceUtil.getEmailAddress(Long.parseLong (preferences[preference.getIndex ()].substring (2)));
					org.cdph.han.persistence.EmailAddress pEmailAddress = convertEmailAddress(emailAddress);
					preference.setEmailAddress (pEmailAddress);
					log.debug("Email Preference " + pEmailAddress);
					preference.setPrimaryEmail (false);
				} else if (preferences[i].startsWith ("P:")) {
					org.cdph.han.model.PhoneDevice phoneDevice = PhoneDeviceLocalServiceUtil.getPhoneDevice(
							Long.parseLong (preferences[preference.getIndex ()].substring (2)));
					preference.setPhoneDevice (convertPhoneDevice(phoneDevice));
					log.debug("Phone Preference " + phoneDevice);
					preference.setPrimaryEmail (false);
				}
				userData.getContactPreferences ().add (preference);
				createContactPreference(preference);
			}
		}

		// Process alternate contacts 
		final List<AlternateContact> toDelete = new ArrayList<AlternateContact> ();
		List<org.cdph.han.model.AlternateContact> alternateContactsFromDB = AlternateContactLocalServiceUtil.findByUserId(userId);
		if (alternateContacts != null && !alternateContacts.isEmpty ()) {
			int index = 0;
			log.debug("Alternate Contacts: userData " + (userData.getAlternateContacts() == null ? "empty": userData.getAlternateContacts().size ()) + ".");
			log.debug("Alternate Contacts: Screen " + alternateContacts.size());
			// getAlternateContactsFromDBList(alternateContactsFromDB, long altContactId)
			if (alternateContacts.size () > userData.getAlternateContacts ().size()) {
				AlternateContact[] alternateContactsDB = userData.getAlternateContacts ().toArray (
						new AlternateContact[0]);	// userData.getAlternateContacts ().size ()
				org.cdph.han.model.AlternateContact alternateContactDB;
				log.debug("In DB: " + alternateContactsDB.length);
				for (UserDisplayTO alternateContact : alternateContacts) {
					alternateContactDB = new org.cdph.han.model.impl.AlternateContactImpl ();
					log.debug("Index " + index + ", contact " + alternateContact);
					if (index < alternateContactsDB.length) {
						alternateContactDB.setId(alternateContactsDB[index].getId());
					} else {
						long newId = CounterLocalServiceUtil.increment();
//						alternateContactDB.setUserData (userData);
						alternateContactDB.setId(newId);
					}
//					org.cdph.han.model.UserData userDataDB = UserDataLocalServiceUtil.getUserData(alternateContact.getId ());
//					log.debug("User Data: " + userDataDB);
//					.setAlternateContact (convertUserData(userDataDB));
					alternateContactDB.setAlternateUserId(alternateContact.getId());
					alternateContactDB.setUserId(userId);
					alternateContactDB.setPreference(index);
//					org.cdph.han.model.AlternateContact ac;
//					if (index < alternateContactsDB.length) {
//						ac = AlternateContactLocalServiceUtil.fetchAlternateContact(alternateContactDB.getId());
//					} else {
//						ac = AlternateContactLocalServiceUtil.createAlternateContact(AlternateContactLocalServiceUtil.getAlternateContactsCount() + 1);
//						userData.getAlternateContacts ().add (alternateContactDB);
//					}
//					ac.setUserId(userData.getUserId());
//					ac.setAlternateUserId(alternateContactDB.getAlternateContact().getUserId());
//					updateAlternateContact(ac, alternateContactDB);
					AlternateContactLocalServiceUtil.updateAlternateContact(alternateContactDB);
					++index;
				}
			} else {
				UserDisplayTO alternateContact;
				List<AlternateContact> addedContacts = new ArrayList<AlternateContact>();
				for (AlternateContact alternateContactPersist : userData.getAlternateContacts ()) {
					log.debug("From user data: " + userData.getAlternateContacts().size() + ", index: " + index);
					if (index < alternateContacts.size ()) {
						alternateContact = alternateContacts.get (index);
						log.debug("Index " + index + ", contact " + alternateContact.getId() + " " + alternateContact.getLastName());
//						org.cdph.han.model.UserData userDataDB = UserDataLocalServiceUtil.getUserData(alternateContact.getId ());
						org.cdph.han.model.AlternateContact alternateContactDB = getAlternateContactsFromDBList(alternateContactsFromDB, alternateContact.getId ());
						if (alternateContactDB == null) {
							toDelete.add (alternateContactPersist);
							long newId = CounterLocalServiceUtil.increment();
							alternateContactDB = new org.cdph.han.model.impl.AlternateContactImpl ();
							alternateContactDB.setId(newId);
							log.debug("Add " + alternateContact.getId () + ", new Id " + newId);
							AlternateContactLocalServiceUtil.addAlternateContact(alternateContactDB);
							alternateContactDB.setAlternateUserId(alternateContact.getId ());
							AlternateContact addedContact = new AlternateContact();
							addedContact.setIndex(index);
							UserData userDataDB = new UserData();
							Contact userContact = new Contact();
							userContact.setFirstName(alternateContact.getFirstName());
							userContact.setLastName(alternateContact.getLastName());
							userDataDB.setUserId(alternateContact.getId ());
							userDataDB.setContact(userContact);
							addedContact.setAlternateContact(userDataDB);
							addedContacts.add(addedContact);
						}
						alternateContactDB.setPreference(index);
						AlternateContactLocalServiceUtil.updateAlternateContact(alternateContactDB);
//						alternateContactPersist.setAlternateContact (convertUserData(userDataDB));
//						alternateContactPersist.setIndex (index);
					} else {
						log.debug("Index: " + index + alternateContacts.size () + " automatic delete.");
						toDelete.add (alternateContactPersist);
					}
					++index;
				}
				for (AlternateContact addedContact : addedContacts) {
					userData.getAlternateContacts().add(addedContact);
				}
			}
		} else {		// Delete all previous alternate contacts by adding them to toDelete.
			if (userData.getAlternateContacts () != null && !userData.getAlternateContacts ().isEmpty ()) {
				for (AlternateContact alternateContact : userData.getAlternateContacts ()) {
					toDelete.add (alternateContact);
				}
			}
		}
		for (AlternateContact alternateContact : toDelete) {
			userData.getAlternateContacts ().remove (alternateContact);
			AlternateContactLocalServiceUtil.deleteAlternateContact(alternateContact.getId());
		}

		// Update user telephony preferences
		boolean updateId = telephonyId != null && telephonyId.length () > 0;
		boolean updatePass = telephonyPass != null && telephonyPass.length () > 0;
		boolean updatePIN = telephonyPIN != null && telephonyPIN.length () > 0;
		if (updateId || updatePass || updatePIN) {
			if (updateId) {
				userData.setTelephonyId (telephonyId);
			}
			if (updatePass) {
				userData.setTelephonyPassword (telephonyPass);
			}
			if (updatePIN) {
				userData.setTelephonyPIN (telephonyPIN);
			}
		}
		log.debug("Update HAN User Data " + userData.getUserId());
		org.cdph.han.model.UserData ud = UserDataLocalServiceUtil.getUserData(userData.getUserId());
		ud.setDepartment(userData.getDepartment());
		ud.setEmailPassword(userData.getEmailPassword());
		ud.setTelephonyId(userData.getTelephonyId());
		ud.setTelephonyPassword(userData.getTelephonyPassword());
		ud.setTelephonyPIN(userData.getTelephonyPIN());
		log.trace("Update UD " + ud);
		UserDataLocalServiceUtil.updateUserData(ud);

		/*			TODO - Needs to prepare for Sync
		final List<Mir3UserTO> recipients = new ArrayList<Mir3UserTO> ();
		recipients.add (prepareUserForSynch (userData));
		//abey - commenting for compilation
		userSynchService.updateRecipients (recipients,
s				hanPropertiesService.getValue ("MIR3_ADMIN_USER"),
				hanPropertiesService.getValue ("MIR3_ADMIN_PASS"),
				hanPropertiesService.getValue ("MIR3_API_VERSION"),
				true);
		*/
	}

	private org.cdph.han.model.AlternateContact getAlternateContactsFromDBList(List<org.cdph.han.model.AlternateContact> altContacts, long altContactId) {
		for (org.cdph.han.model.AlternateContact altContact : altContacts) {
			if (altContact.getAlternateUserId() == altContactId) {
				return altContact;
			}
		}
		return null;
	}

	private void updateContactPreference(ContactPreference preference) {
		try {
			org.cdph.han.model.ContactPreference cp = ContactPreferenceLocalServiceUtil.getContactPreference(preference.getId());
			cp.setId(preference.getId());
			updateContactPreference(cp, preference);
		} catch (SystemException se) {
			log.error("System Exception: " + se);
		} catch (PortalException pe) {
			log.error("Portal Exception: " + pe);
		}
	}

	private void createContactPreference(ContactPreference preference) {
		try {
			int id = ContactPreferenceLocalServiceUtil.findMaxId() + 1;
			org.cdph.han.model.ContactPreference cp = ContactPreferenceLocalServiceUtil.createContactPreference(id);
			updateContactPreference(cp, preference);
		} catch (SystemException se) {
			log.error("System Exception: " + se);
		}
	}

	private void updateContactPreference(org.cdph.han.model.ContactPreference cp, ContactPreference preference)
		throws SystemException {
		if (preference.getEmailAddress() != null) cp.setEmailAddressId(preference.getEmailAddress().getEmailId());
		if (preference.getPhoneDevice() != null) cp.setPhoneId(preference.getPhoneDevice().getPhoneId());
		cp.setIndex(preference.getIndex());
		cp.setUserId(preference.getUserData().getUserId());
		cp.setPrimaryEmail(preference.getPrimaryEmail());
		log.info("Persisting contactPreference " + cp);
		ContactPreferenceLocalServiceUtil.updateContactPreference(cp);
	}

	public static org.cdph.han.persistence.PhoneDevice convertPhoneDevice(org.cdph.han.model.PhoneDevice phoneDevice) {
		org.cdph.han.persistence.PhoneDevice pPhoneDevice = new org.cdph.han.persistence.PhoneDevice();
		pPhoneDevice.setPhoneNumber(phoneDevice.getPhoneNumber());
		pPhoneDevice.setPhoneId(phoneDevice.getPhoneId());
		pPhoneDevice.setTypeId(phoneDevice.getTypeId());
		pPhoneDevice.setExtension(phoneDevice.getExtension());
		return pPhoneDevice;
	}

	public static org.cdph.han.persistence.EmailAddress convertEmailAddress(com.liferay.portal.model.EmailAddress emailAddress) {
		org.cdph.han.persistence.EmailAddress pEmailAddress = new org.cdph.han.persistence.EmailAddress();
		pEmailAddress.setAddress(emailAddress.getAddress());
		pEmailAddress.setEmailId(emailAddress.getEmailAddressId());
		pEmailAddress.setTypeId(new Long(emailAddress.getTypeId()));
//		pEmailAddress.setContact(contact);
		return pEmailAddress;
	}

	private void updateAlternateContact(org.cdph.han.model.AlternateContact alternateContact, org.cdph.han.persistence.AlternateContact pAc)
		throws SystemException {
		for (AlternateContact ac : pAc.getAlternateContact().getAlternateContacts()) {
			alternateContact.setAlternateUserId(ac.getAlternateContact().getUserId());
		}
		alternateContact.setUserId(pAc.getUserData().getUserId());
		alternateContact.setUserId(pAc.getAlternateContact().getUserId());
		alternateContact.setNew(false);
		alternateContact.setPreference(0);
		AlternateContactLocalServiceUtil.updateAlternateContact(alternateContact);
	}

	private org.cdph.han.persistence.UserData convertUserData(org.cdph.han.model.UserData userDataModel) {
		UserData userDataPersist = new UserData();
		userDataPersist.setDepartment(userDataModel.getDepartment());
		userDataPersist.setEmailPassword(userDataModel.getEmailPassword());
		userDataPersist.setMir3Id(userDataModel.getMir3Id());
		userDataPersist.setQuotaWarned(userDataModel.getQuotaWarned());
		userDataPersist.setTelephonyId(userDataModel.getTelephonyId());
		userDataPersist.setTelephonyPassword(userDataModel.getTelephonyPassword());
		userDataPersist.setTelephonyPIN(userDataModel.getTelephonyPIN());
		userDataPersist.setUserId(userDataModel.getUserId());
//		userDataPersist.set.setQuotaWarned(userDataModel.get);
		return userDataPersist;
	}

	/**
	 * Prepares the user information to be sent for update at mir3
	 * @param user with the user information
	 * @return the object to be used for updating the mir3 entry
	 */
	@SuppressWarnings("unchecked")
	private Mir3UserTO prepareUserForSynch (final UserData user) {
		final Mir3UserTO userTO = new Mir3UserTO ();
		userTO.setUsername (user.getEmail ());
		userTO.setFirstName (user.getContact ().getFirstName ());
		userTO.setLastName (user.getContact ().getLastName ());
		userTO.setLanguage ("ENGLISH");
		userTO.setTimezone ("CENTRAL_USA");
		userTO.setTitle (user.getContact ().getJobTitle ());
		userTO.setJobTitle (user.getContact ().getJobTitle ());
		userTO.setDepartment (user.getDepartment ());
		userTO.setMir3Role (securityService.getMir3UserRole (user.getUserId ()));
		if (user.getMir3Id () != null) {
			//abey - commenting for compilation
			//userTO.setMir3Id (user.getMir3Id ());
		}
		final Query q = entityManager.createNamedQuery ("Address.findByClassPK");
		q.setParameter ("classNameId", 14005L);
		q.setParameter ("classPK", user.getContact ().getContactId ());
		final List<Address> addresses = q.getResultList ();
		// Set the user address
		if (addresses != null && !addresses.isEmpty ()) {
			Address address = addresses.get (0);
			userTO.setAddress1 (address.getStreet1 ());
			userTO.setAddress2 (address.getStreet2 ());
			userTO.setCity (address.getCity ());
			userTO.setCountry ("USA");
			userTO.setFloor (address.getFloor ());
			if (address.getRegion () != null) {
				userTO.setState (address.getRegion ().getName ());
			}
			userTO.setZip (address.getZip ());
		}
		// Set the user alternates
		userTO.setAlternates (new ArrayList<String> ());
		for (AlternateContact contact : user.getAlternateContacts ()) {
			userTO.getAlternates ().add (contact.getAlternateContact ().getEmail ());
		}

		// Set the organizational data
		final StringBuilder organization = new StringBuilder ();
		final StringBuilder areaField = new StringBuilder ();
		final StringBuilder role = new StringBuilder ();
		final StringBuilder orgType = new StringBuilder ();
		for (UserOrganizationalData data : user.getUserOrganizationalData ()) {
			organization.append (data.getOrganization ().getName ()).append (',');
			areaField.append (data.getAreaField ().getDescription ()).append (',');
			role.append (data.getRole ().getName ()).append (',');
			orgType.append (entityManager.find (Organization.class,
					data.getOrganization ().getParentOrganizationId ()).getName ()).append (',');
		}
		// Check that none of the values exceeds the 100 length limit
		userTO.setRole (role.toString ().length () > MAX_LENGTH ?
				role.toString ().substring (0, MAX_LENGTH) : role.toString ().substring (0, role.length () - 1));
		userTO.setOrganization (organization.toString ().length () > MAX_LENGTH ?
				organization.toString ().substring (0, MAX_LENGTH) :
					organization.toString ().substring (0, organization.length () - 1));
		userTO.setOrganizationType (orgType.toString ().length () > MAX_LENGTH ?
				orgType.toString ().substring (0, MAX_LENGTH) :
					orgType.toString ().substring (0, orgType.length () - 1));
		userTO.setAreaField (areaField.toString ().length () > MAX_LENGTH ?
				areaField.toString ().substring (0, MAX_LENGTH) :
					areaField.toString ().substring (0, areaField.length () - 1));
		userTO.setDivision ("/");
		userTO.setDepartment (user.getDepartment ());
		userTO.setTelephonyId (user.getTelephonyId ());
		userTO.setPassword (user.getTelephonyPassword ());
		userTO.setPin (user.getTelephonyPIN ());
		userTO.setDevices (new ArrayList<Mir3DeviceTO> ());
		//userTO.setPreferences (new ArrayList<Mir3DevicePriorityTO> ());
		addDevices (userTO.getDevices (), user.getEmail (), user.getContact ().getEmailAddresses (),
				user.getContact ().getPhoneDevices (), user.getContactPreferences ());
		//userTO.setPreferences (new ArrayList<Mir3DevicePriorityTO> ());
		/*Mir3DevicePriorityTO preference;
		if (user.getContactPreferences () == null || user.getContactPreferences ().isEmpty ()) {
			preference = new Mir3DevicePriorityTO ();
			preference.setIndex (1);
			preference.setPriority (1);
		} else {
			int i = 1;
			for (ContactPreference pref : user.getContactPreferences ()) {
				preference = new Mir3DevicePriorityTO ();
				preference.setPriority (i++);
				if (pref.getPrimaryEmail ()) {
					preference.setIndex (1);
				} else if (pref.getEmailAddress () != null) {
					preference.setIndex (findEmail (pref.getEmailAddress (), userTO.getDevices ()));
				} else if (pref.getPhoneDevice () != null) {
					preference.setIndex (findPhone (pref.getPhoneDevice (), userTO.getDevices ()));
				}
			}
		}*/
		return userTO;
	}

	/**
	 * Returns the index of the phone device in the devices list
	 * @param phoneDevice to search
	 * @param devices list to search
	 * @return the index in the list of the phone device
	 */
	private int findPhone (final PhoneDevice phoneDevice, final Collection<ContactPreference> preferences) {
		int index = -1;
		for (ContactPreference preference : preferences) {
			if (preference.getPhoneDevice () != null && preference.getPhoneDevice ().equals (phoneDevice)) {
				index = preference.getIndex () + 1;
				break;
			}
		}
		return index;
	}

	/**
	 * Search for the index in the given list of the email address
	 * @param emailAddress to search
	 * @param devices list to search into
	 * @return index in the list
	 */
	private int findEmail (final EmailAddress emailAddress, final Collection<ContactPreference> preferences) {
		int index = -1;
		for (ContactPreference preference : preferences) {
			if (preference.getEmailAddress () != null && preference.getEmailAddress ().equals (emailAddress)) {
				index = preference.getIndex () + 1;
				break;
			}
		}
		return index;
	}

	/**
	 * Adds all the available devices into the user object
	 * @param devices the list to add the devices
	 * @param emailAddresses the email addresses the user has configured
	 * @param phoneDevices the phone devices the user has configured
	 */
	private void addDevices (final List<Mir3DeviceTO> devices, final String emailAddress,
			final Collection<EmailAddress> emailAddresses, final Collection<PhoneDevice> phoneDevices,
			final Collection<ContactPreference> preferences) {
		Mir3DeviceTO device = new Mir3DeviceTO ();
		device.setAddress (emailAddress);
		device.setDescription ("Primary Work Email");
		device.setType ("Work Email");
		int priority = getPrimaryEmailPriority (preferences);
		if (priority > 0) {
			device.setPriority (String.valueOf (priority));
		} else {
			device.setPriority ("OFF");
		}
		devices.add (device);
		for (PhoneDevice phone : phoneDevices) {
			device = new Mir3DeviceTO ();
			priority = findPhone (phone, preferences);
			if (priority > 0) {
				device.setPriority (String.valueOf (priority));
			} else {
				device.setPriority ("OFF");
			}
			if (phone.getExtension () != null && phone.getExtension ().length () > 0) {
				device.setAddress (phone.getPhoneNumber () + 'x' + phone.getExtension ());
			} else {
				device.setAddress (phone.getPhoneNumber ());
			}
			device.setDescription (phone.getFriendlyName ());
			if (phone.getPagerCarrier () != null) {
				//abey - commenting for compilation
				//device.setPagerCarrier (phone.getPagerCarrier ().getMir3Id ());
			}
			device.setType (entityManager.find (ListType.class, Long.valueOf (phone.getTypeId ())).getName ());
			devices.add (device);
		}
		for (EmailAddress email : emailAddresses) {
			device = new Mir3DeviceTO ();
			priority = findEmail (email, preferences);
			if (priority > 0) {
				device.setPriority (String.valueOf (priority));
			} else {
				device.setPriority ("OFF");
			}
			device.setAddress (email.getAddress ());
			device.setDescription (email.getFriendlyName ());
			device.setType (entityManager.find (ListType.class, Long.valueOf (email.getTypeId ())).getName ());
			devices.add (device);
		}
	}

	/**
	 * Checks if the primary email is in the device preferences
	 * @param preferences the user contact prefernces
	 * @return the rank in the preferences or -1 if not found
	 */
	private int getPrimaryEmailPriority (final Collection<ContactPreference> preferences) {
		int index = -1;
		for (ContactPreference preference : preferences) {
			if (preference.getPrimaryEmail ()) {
				index = preference.getIndex () + 1;
			}
		}
		return index;
	}

	private org.cdph.han.model.PhoneDevice createPhoneDevice(long id) {
		log.info("Create phone " + id);
		return PhoneDeviceLocalServiceUtil.createPhoneDevice(id);
	}

	private com.liferay.portal.model.EmailAddress createEmailAddress(long id) {
		try {
			log.info("Create email address " + id);
			return EmailAddressLocalServiceUtil.getEmailAddress(id);
		} catch (PortalException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return null;
	}
}
