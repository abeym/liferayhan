package org.cdph.han.facade.impl.custgroups;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.custgroups.dto.FilterCriteriaTO;
import org.cdph.han.custgroups.dto.RecipientTO;
import org.cdph.han.custgroups.dto.RecipientType;
import org.cdph.han.custgroups.services.*;
import org.cdph.han.model.impl.CustomGroupCriteriaImpl;
import org.cdph.han.model.impl.CustomGroupMemberImpl;
//import org.cdph.han.custgroups.services.GroupManagementServiceImpl;
import org.cdph.han.persistence.AreaField;
import org.cdph.han.persistence.CustomGroupCriteria;
import org.cdph.han.persistence.CustomGroupMember;
import org.cdph.han.persistence.NonHanUser;
import org.cdph.han.persistence.NonHanUserGroup;
import org.cdph.han.persistence.Organization;
import org.cdph.han.persistence.Role;
import org.cdph.han.persistence.UserCustomGroup;
import org.cdph.han.persistence.UserData;
import org.cdph.han.service.AreaFieldLocalServiceUtil;
import org.cdph.han.service.CustomGroupCriteriaLocalServiceUtil;
import org.cdph.han.service.CustomGroupMemberLocalServiceUtil;
import org.cdph.han.service.HanUserDataLocalServiceUtil;
import org.cdph.han.service.UserCustomGroupLocalServiceUtil;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.service.OrganizationLocalServiceUtil;

/**
 * Implementation of the GroupManagementService that uses JPA
 * @author Abey Mani
 * @version 1.0
 */
@Repository
@Service ("groupManagementService")
public class GroupManagementServiceImpl implements GroupManagementService {
	private static final Log log = LogFactory.getLog (GroupManagementServiceImpl.class);
	/** Persistence context */
	private EntityManager entityManager;
	
	public UserData lrToHanUserData(org.cdph.han.model.UserData lud){
		UserData ud = new UserData();
		//TODO: need to get this active flag
		ud.setActive(true);
		ud.setDepartment(lud.getDepartment());
		ud.setUserId(lud.getUserId());
		return ud;
	}
	
	
	/* (non-Javadoc)
	 * @see org.cdph.han.custgroups.services.GroupManagementService#saveGroup(java.lang.String, java.util.List)
	 */
	@Override
	//abey ...@Transactional (propagation = Propagation.REQUIRED)
	public long saveGroup (final long userId, final String name, final List<RecipientTO> recipients) {
		//final UserData user = entityManager.find (UserData.class, userId);
		log.debug("Save Han User Data for " + userId + " " + name);
		org.cdph.han.model.UserData lud = null;
		try {
			lud = org.cdph.han.service.UserDataLocalServiceUtil.getUserData(userId);
			log.debug("Get user data: " + lud.getUserId());
		} catch (PortalException e) {
			// 
			log.error(e);
		} catch (SystemException e) {
			log.error(e);
		}
		
	/*	UserCustomGroup group = new UserCustomGroup ();
		group.setCreatedDate (new Date ());
		group.setName (name);
		group.setUserData (user);
		*/
		UserCustomGroup group = new UserCustomGroup ();
		group.setCreatedDate (new Date ());
		group.setName (name);
		UserData ud = new UserData();
		ud = lrToHanUserData(lud);
		group.setUserData (ud);
		group.setModifiedBy(ud);
		OrganizationSearchLayerService osls = new OrganizationSearchLayerService();
		long groupId = osls.addGroup(group);
		log.debug("Add group Id: " + group.getGroupId() + ", id: " + group.getGroupId());
		CustomGroupMember member;
		org.cdph.han.model.CustomGroupMember modelMember;
		for (RecipientTO recipient : recipients) {
			member = new CustomGroupMember ();
			modelMember = CustomGroupMemberLocalServiceUtil.add();
			modelMember.setUserCustomGroup(groupId);
			log.debug("Add " + recipient.getType () +" member: " + modelMember);
			if (RecipientType.AREA_FIELD.equals (recipient.getType ())) {
				modelMember.setAreaFieldId((int) recipient.getId ());
			} else if (RecipientType.GROUP.equals (recipient.getType ())) {
				modelMember.setChildGroupId((int) recipient.getId ());
			} else if (RecipientType.NON_HAN_GROUP.equals (recipient.getType ())) {
				modelMember.setNonHanUserGroupId((int) recipient.getId ());
			} else if (RecipientType.NON_HAN_USER.equals (recipient.getType ())) {
				modelMember.setNonHanUserId((int) recipient.getId ());
			} else if (RecipientType.ORGANIZATION.equals (recipient.getType ())
					|| RecipientType.ORGANIZATION_TYPE.equals (recipient.getType ())) {
				modelMember.setOrganizationId((int) recipient.getId ());
			} else if (RecipientType.ROLE.equals (recipient.getType ())) {
				modelMember.setRoleId((int) recipient.getId ());
			} else if (RecipientType.USER.equals (recipient.getType ())) {
				modelMember.setUserId((int) recipient.getId ());
			} else if (RecipientType.CRITERIA.equals (recipient.getType ())) {
				modelMember.setChicagoOnly(recipient.isChicagoOnly ());
				for (FilterCriteriaTO criteria : recipient.getCriteria ()) {
					org.cdph.han.model.CustomGroupCriteria modelCriteria = CustomGroupCriteriaLocalServiceUtil.add();
//					dbCriteria = new CustomGroupCriteria ();
//					dbCriteria.setOrganizationType (entityManager.find (Organization.class,
//							criteria.getOrganizationTypeId ()));
//					dbCriteria.setAreaField (criteria.getAreaFieldId () == null ? null : 
//						entityManager.find (AreaField.class, criteria.getAreaFieldId ().intValue ()));
//					dbCriteria.setOrganization (criteria.getOrganizationId () == null ? null :
//						entityManager.find (Organization.class, criteria.getOrganizationId ()));
//					dbCriteria.setRole (criteria.getRoleId () == null ? null :
//						entityManager.find (Role.class, criteria.getRoleId ()));
//					dbCriteria.setMember (member);
//					member.getCriterias ().add (dbCriteria);
					modelCriteria.setAreaFieldId(criteria.getAreaFieldId());
					modelCriteria.setOrganizationId(criteria.getOrganizationId());
					modelCriteria.setOrganizationTypeId(criteria.getOrganizationTypeId ());
					modelCriteria.setRoleId(criteria.getRoleId());
					modelCriteria.setMemberId(member.getMemberId());
					log.debug("Add member criteria: " + modelCriteria.getMemberId() + "-> " + modelCriteria);
					try {
						CustomGroupCriteriaLocalServiceUtil.updateCustomGroupCriteria(modelCriteria);
					} catch (SystemException e) {e.printStackTrace();}
				}
			}
			member.setUserCustomGroup (group);
			try {
				CustomGroupMemberLocalServiceUtil.updateCustomGroupMember(modelMember);
			} catch (SystemException e) {e.printStackTrace();}
		}
		return group.getGroupId ();
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.custgroups.services.GroupManagementService#updateGroup(long, long, java.util.List)
	 */
	@Override
	//abey ...@Transactional (propagation = Propagation.REQUIRED)
	public void updateGroup (final long userId, final long groupId, final List<RecipientTO> recipients) {
		log.debug("Update Han User Data for " + userId + " , groupId " + groupId);
//		final UserData user = entityManager.find (UserData.class, userId);
//		final UserCustomGroup group = entityManager.find (UserCustomGroup.class, groupId);
		UserData user = null;
		UserCustomGroup group = null;
		try {
			user = getPersistUser(userId);
			group = getPersistGroup(groupId);
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (PortalException e) {
			e.printStackTrace();
		}

		CustomGroupMember[] membersInDB = group.getMembers ().toArray (
				new CustomGroupMember[group.getMembers ().size ()]);
		RecipientTO[] updatedRecipients = recipients.toArray (
				new RecipientTO[recipients.size ()]);
		RecipientTO recipient;
		CustomGroupMember member;
		org.cdph.han.model.CustomGroupCriteria dbCriteria;
		if (membersInDB.length > updatedRecipients.length) {
			for (int i = 0; i < membersInDB.length; i++) {
				member = membersInDB[i];
				org.cdph.han.model.CustomGroupMember mMember = new CustomGroupMemberImpl();
				try {
					mMember = CustomGroupMemberLocalServiceUtil.getCustomGroupMember(member.getMemberId());
				} catch (PortalException e) {
					e.printStackTrace();
				} catch (SystemException e) {
					e.printStackTrace();
				}
				if (i < updatedRecipients.length) {
					recipient = updatedRecipients[i];
					if (RecipientType.AREA_FIELD.equals (recipient.getType ())) {
						AreaField areaField = getPersistAreaField(recipient.getId ());
						mMember.setAreaFieldId(areaField.getAreaFieldId());
					} else if (RecipientType.GROUP.equals (recipient.getType ())) {
						mMember.setChildGroupId(recipient.getId());
					} else if (RecipientType.NON_HAN_GROUP.equals (recipient.getType ())) {
						mMember.setNonHanUserGroupId(recipient.getId());
					} else if (RecipientType.NON_HAN_USER.equals (recipient.getType ())) {
						mMember.setNonHanUserId(recipient.getId());
					} else if (RecipientType.ORGANIZATION.equals (recipient.getType ())
							|| RecipientType.ORGANIZATION_TYPE.equals (recipient.getType ())) {
						mMember.setOrganizationId(recipient.getId());
					} else if (RecipientType.ROLE.equals (recipient.getType ())) {
						mMember.setRoleId(recipient.getId());
					} else if (RecipientType.USER.equals (recipient.getType ())) {
						mMember.setUserId(recipient.getId());
					} else if (RecipientType.CRITERIA.equals (recipient.getType ())) {
						mMember.setChicagoOnly(recipient.isChicagoOnly ());
						member.setChicagoOnly (recipient.isChicagoOnly ());
						for (CustomGroupCriteria criteria : member.getCriterias ()) {
							removeCustomGroupCriteria(criteria);
						}
						member.setCriterias (new ArrayList<CustomGroupCriteria> ());
						addCriterias(member.getMemberId(), recipient.getCriteria ());
					}
					try {
						CustomGroupMemberLocalServiceUtil.updateCustomGroupMember(mMember);
					} catch (SystemException e) {
						e.printStackTrace();
					}
				} else {
					try {
						CustomGroupMemberLocalServiceUtil.deleteCustomGroupMember(mMember);
					} catch (SystemException e) {
						e.printStackTrace();
					}
				}
			}
		} else {
			for (int i = 0; i < updatedRecipients.length; i++) {
				recipient = updatedRecipients[i];
				if (i < membersInDB.length) {
					member = membersInDB[i];
					org.cdph.han.model.CustomGroupMember mMember = new CustomGroupMemberImpl();
					try {
						mMember = CustomGroupMemberLocalServiceUtil.getCustomGroupMember(member.getMemberId());
					} catch (PortalException e) {
						e.printStackTrace();
					} catch (SystemException e) {
						e.printStackTrace();
					}
					if (RecipientType.AREA_FIELD.equals (recipient.getType ())) {
						AreaField areaField = getPersistAreaField(recipient.getId ());
						mMember.setAreaFieldId(areaField.getAreaFieldId());
					} else if (RecipientType.GROUP.equals (recipient.getType ())) {
						mMember.setChildGroupId(recipient.getId());
					} else if (RecipientType.NON_HAN_GROUP.equals (recipient.getType ())) {
						mMember.setNonHanUserGroupId(recipient.getId());
					} else if (RecipientType.NON_HAN_USER.equals (recipient.getType ())) {
						mMember.setNonHanUserId(recipient.getId());
					} else if (RecipientType.ORGANIZATION.equals (recipient.getType ())
							|| RecipientType.ORGANIZATION_TYPE.equals (recipient.getType ())) {
						mMember.setOrganizationId(recipient.getId());
					} else if (RecipientType.ROLE.equals (recipient.getType ())) {
						mMember.setRoleId(recipient.getId());
					} else if (RecipientType.USER.equals (recipient.getType ())) {
						mMember.setUserId(recipient.getId());
					} else if (RecipientType.CRITERIA.equals (recipient.getType ())) {
						mMember.setChicagoOnly(recipient.isChicagoOnly ());
						for (CustomGroupCriteria criteria : member.getCriterias ()) {
							removeCustomGroupCriteria(criteria);
						}
						member.setCriterias (new ArrayList<CustomGroupCriteria> ());
						addCriterias(member.getMemberId(), recipient.getCriteria ());
					}
					try {
						CustomGroupMemberLocalServiceUtil.updateCustomGroupMember(mMember);
					} catch (SystemException e) {
						e.printStackTrace();
					}
				} else {
					member = new CustomGroupMember ();
					member.setUserCustomGroup (group);
					org.cdph.han.model.CustomGroupMember mMember = new CustomGroupMemberImpl();
					try {
						mMember = CustomGroupMemberLocalServiceUtil.getCustomGroupMember(member.getMemberId());
					} catch (PortalException e) {
						e.printStackTrace();
					} catch (SystemException e) {
						e.printStackTrace();
					}
					if (RecipientType.AREA_FIELD.equals (recipient.getType ())) {
						AreaField areaField = getPersistAreaField(recipient.getId ());
						mMember.setAreaFieldId(areaField.getAreaFieldId());
					} else if (RecipientType.GROUP.equals (recipient.getType ())) {
						mMember.setChildGroupId(recipient.getId());
					} else if (RecipientType.NON_HAN_GROUP.equals (recipient.getType ())) {
						mMember.setNonHanUserGroupId(recipient.getId());
					} else if (RecipientType.NON_HAN_USER.equals (recipient.getType ())) {
						mMember.setNonHanUserId(recipient.getId());
					} else if (RecipientType.ORGANIZATION.equals (recipient.getType ())
							|| RecipientType.ORGANIZATION_TYPE.equals (recipient.getType ())) {
						mMember.setOrganizationId(recipient.getId());
					} else if (RecipientType.ROLE.equals (recipient.getType ())) {
						mMember.setRoleId(recipient.getId());
					} else if (RecipientType.USER.equals (recipient.getType ())) {
						mMember.setUserId(recipient.getId());
					} else if (RecipientType.CRITERIA.equals (recipient.getType ())) {
						mMember.setChicagoOnly(recipient.isChicagoOnly ());
						member.setCriterias (new ArrayList<CustomGroupCriteria> ());
						addCriterias(member.getMemberId(), recipient.getCriteria ());
					}
					try {
						CustomGroupMemberLocalServiceUtil.updateCustomGroupMember(mMember);
					} catch (SystemException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	UserData getPersistUser(long userId) throws SystemException, PortalException {
		org.cdph.han.model.HanUserData modelUser = HanUserDataLocalServiceUtil.getHanUserData(userId);
		UserData user = new UserData();
		user.setUserId(modelUser.getUserId());
		user.setMir3Id(modelUser.getMir3_id());
		user.setDepartment(modelUser.getDepartment());
		user.setEmailPassword(modelUser.getMailPass());
		user.setTelephonyId(modelUser.getTelephony_id());
		user.setTelephonyPassword(modelUser.getTelephony_pass());
		user.setTelephonyPIN(modelUser.getTelephony_pin());
		return user;
	}

	private void addCriterias(long memberId, List<FilterCriteriaTO> criterias) {
		for (FilterCriteriaTO criteria : criterias) {
			org.cdph.han.model.CustomGroupCriteria dbCriteria = CustomGroupCriteriaLocalServiceUtil.add();
			dbCriteria.setOrganizationTypeId(criteria.getOrganizationTypeId());
			dbCriteria.setAreaFieldId(criteria.getAreaFieldId ().intValue ());
			dbCriteria.setOrganizationId(criteria.getOrganizationId ());
			dbCriteria.setRoleId (criteria.getRoleId ());
			dbCriteria.setMemberId(memberId);
			try {
				CustomGroupCriteriaLocalServiceUtil.updateCustomGroupCriteria(dbCriteria);
			} catch (SystemException se) {}
		}
	}

	UserCustomGroup getPersistGroup(long groupId) throws SystemException, PortalException {
		UserCustomGroup persistGroup = new UserCustomGroup();	// groupId);
		org.cdph.han.model.UserCustomGroup modelGroup = UserCustomGroupLocalServiceUtil.getUserCustomGroup(groupId);
		persistGroup.setGroupId(groupId);
		persistGroup.setDeleted(modelGroup.getDeleted());
		persistGroup.setName(modelGroup.getName());
		persistGroup.setDateModified (new Date ());
//		persistGroup.setUserData(modelGroup.?);
//		persistGroup.setModifiedBy(modelGroup.getModifiedBy());
//		persistGroup.setCreatedDate(modelGroup.getCreatedDate());
		return persistGroup;
	}

	AreaField getPersistAreaField(long id) {
		AreaField persistAreaField = new AreaField();
		try {
			org.cdph.han.model.AreaField areaField = AreaFieldLocalServiceUtil.getAreaField(id);
			persistAreaField.setAreaFieldId(new Long(areaField.getAreaFieldId()).intValue());
			persistAreaField.setDescription(areaField.getDescription());
			Organization persistOrg = getPersistOrganization(areaField.getOrganization());
			persistAreaField.setOrganization(persistOrg);
		} catch (PortalException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return persistAreaField;
	}

	Organization getPersistOrganization(long id) {
		Organization persistOrg = new Organization();
		try {
			com.liferay.portal.model.Organization org = OrganizationLocalServiceUtil.getOrganization(id);
			persistOrg.setOrganizationId(org.getOrganizationId());
			persistOrg.setName(org.getName());
//			persistOrg.setUserDatas(userDatas);
		} catch (PortalException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return persistOrg;
	}

	void removeCustomGroupCriteria(CustomGroupCriteria custGrp) {
		try {
			log.debug("Remove customer group: " + custGrp);
			CustomGroupCriteriaLocalServiceUtil.deleteCustomGroupCriteria(custGrp.getCriteriaId());
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (PortalException e) {
			e.printStackTrace();
		}
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.custgroups.services.GroupManagementService#deleteGroup(long, long)
	 */
	@Override
	//abey ...@Transactional (propagation = Propagation.REQUIRED)
	public DeleteResult deleteGroup (long userId, long groupId) {
		DeleteResult result = DeleteResult.NOT_OWNER;
		org.cdph.han.model.UserCustomGroup group = null;
		try {
			group = UserCustomGroupLocalServiceUtil.getUserCustomGroup(groupId);
			if (group.getUserId() == 0l || group.getUserId() == userId) {
				List<org.cdph.han.model.CustomGroupMember> ocurrences = CustomGroupMemberLocalServiceUtil.findByChildGroupId(groupId);
				if (ocurrences.isEmpty ()) {
					group.setDeleted (true);
					UserCustomGroupLocalServiceUtil.updateUserCustomGroup(group);
					result = DeleteResult.SUCCESS;
				} else {
					result = DeleteResult.GROUP_IN_USE;
				}
			} else {
				result = DeleteResult.NOT_OWNER;
			}
		} catch (SystemException e) {
			log.error(e);
		} catch (PortalException e) {
			log.error(e);
		}
		return result;
	}

	/**
	 * @param entityManager the entityManager to set
	 */
	//abey...
	//@PersistenceContext
	public void setEntityManager (EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
