package org.cdph.han.custgroups.services;

import org.cdph.han.custgroups.dto.GroupDisplayTO;
import org.cdph.han.custgroups.dto.GroupTO;

/**
 * Definition of the search service for custom groups
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public interface GroupSearchService {

	/**
	 * The implementation of this method must search all the groups associated to the current user
	 * @param userId ID of the user requesting the search
	 * @return List of GroupDisplayTO objects with the found groups
	 */
	GroupDisplayTO[] searchMyGroups (final long userId);

	/**
	 * The implementation of this method must check that the given user does not have a group with that name,
	 * the check must be case insensitive
	 * @param userId ID of the user for checking the name
	 * @param name desired name to check
	 * @return if the name is available or already exists
	 */
	boolean isNameAvailable (final long userId, final String name);

	/**
	 * The implementation of this method must search for custom groups matching the name filter, if the method does
	 * not receive a filter, the method must return all available groups.
	 * @param name to use as filter
	 * @return the groups found matching the filter, empty array if none
	 */
	GroupDisplayTO[] searchUserGroups (final String name);

	/**
	 * The implementation of this method must load the entire group
	 * @param id of the group to load
	 * @return GroupTO with the group information
	 */
	GroupTO loadGroup (final long id);

}
