package org.cdph.han.facade.impl.email;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.mail.Folder;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.alerts.dto.FileTO;
import org.cdph.han.alerts.services.UserDataService;
import org.cdph.han.email.dto.FolderDetailTO;
import org.cdph.han.email.services.EmailAuthenticator;
import org.cdph.han.email.services.EmailFolderService;
import org.cdph.han.email.services.EmailOperationOutcome;
import org.cdph.han.facade.impl.util.HanPropertiesServiceImpl;
import org.cdph.han.persistence.UserData;
import org.cdph.han.synch.dto.Mir3UserTO;
import org.cdph.han.synch.services.UserSynchService;
import org.cdph.han.synch.services.UserSynchServiceImpl;
import org.cdph.han.util.services.HanPropertiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implementation of the EmailFolderService
 * @author Abey Mani
 * @version 1.0
 */
@Service ("emailFolderService")
public class EmailFolderServiceImpl implements EmailFolderService {
	/** Class log */
	private static final Log log = LogFactory.getLog (EmailFolderServiceImpl.class);
	/** Domain */
	private static final String HAN_EMAIL_DOMAIN_KEY = "HAN_EMAIL_DOMAIN";
	/** Email properties */
	private final Properties props = new Properties ();
	/** Service used to retrieve the email password */
	@Autowired
	private UserDataService userDataService;
	/** Service used to retrieve han properties */
	@Autowired
	private HanPropertiesService hanPropertiesService;

	/**
	 * Default constructor
	 */
	public EmailFolderServiceImpl () {
		try {
			props.load (this.getClass ().getResourceAsStream ("/email.properties"));
		} catch (Exception ex) {
			log.fatal ("Error loading email properties.", ex);
		}
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.email.services.EmailFolderService#createFolder(java.lang.String, java.lang.String)
	 */
	@Override
	public EmailOperationOutcome createFolder (final long userId, final String folderName) {
		EmailOperationOutcome outcome;
		Store store = null;
		try {
			final Properties props = new Properties (this.props);
			final UserData user = userDataService.getUser (userId);
			props.put ("mail.user", user.getScreenName ());
			props.put ("mail.from", user.getScreenName () + '@'
					+ hanPropertiesService.getValue (HAN_EMAIL_DOMAIN_KEY));
			final Session session = Session.getInstance (props, 
					new EmailAuthenticator (user.getScreenName (), user.getEmailPassword ()));
			store = session.getStore ("imap");
			store.connect ();
			final Folder folder = store.getDefaultFolder ().getFolder ("INBOX").getFolder (folderName);
			if (folder.exists ()) {
				outcome = EmailOperationOutcome.FOLDER_ALREADY_EXISTS;
			} else {
				if (folder.create (Folder.HOLDS_MESSAGES)) {
					outcome = EmailOperationOutcome.FOLDER_CREATED;
				} else {
					outcome = EmailOperationOutcome.ERROR_CREATING_FOLDER;
				}
			}
		} catch (Exception ex) {
			log.error ("Error creating user folder.", ex);
			outcome = EmailOperationOutcome.ERROR_CREATING_FOLDER;
		} finally {
			if (store != null) {
				try {
					store.close ();
				} catch (MessagingException ex) {
					log.error ("Error while closing store.", ex);
				}
			}
		}
		return outcome;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.email.services.EmailFolderService#deleteFolder(java.lang.String, java.lang.String)
	 */
	@Override
	public EmailOperationOutcome deleteFolder (final long userId, final String folderName) {
		EmailOperationOutcome outcome;
		Store store = null;
		try {
			final Properties props = new Properties (this.props);
			final UserData user = userDataService.getUser (userId);
			props.put ("mail.user", user.getScreenName ());
			props.put ("mail.from", user.getScreenName () + '@'
					+ hanPropertiesService.getValue (HAN_EMAIL_DOMAIN_KEY));
			final Session session = Session.getInstance (props, 
					new EmailAuthenticator (user.getScreenName (), user.getEmailPassword ()));
			store = session.getStore ("imap");
			store.connect ();
			final Folder folder = store.getFolder (folderName);
			if (folder.exists ()) {
				folder.delete (false);
				outcome = EmailOperationOutcome.FOLDER_DELETED;
			} else {
				outcome = EmailOperationOutcome.FOLDER_DOES_NOT_EXISTS;
			}
		} catch (Exception ex) {
			log.error ("Error deleting user folder.", ex);
			outcome = EmailOperationOutcome.ERROR_DELETING_FOLDER;
		} finally {
			if (store != null) {
				try {
					store.close ();
				} catch (MessagingException ex) {
					log.error ("Error while closing store.", ex);
				}
			}
		}
		return outcome;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.email.services.EmailFolderService#renameFolder(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public EmailOperationOutcome renameFolder (final long userId, final String folderName,
			final String newFolderName) {
		EmailOperationOutcome outcome = null;
		Store store = null;
		try {
			final Properties props = new Properties (this.props);
			final UserData user = userDataService.getUser (userId);
			props.put ("mail.user", user.getScreenName ());
			props.put ("mail.from", user.getScreenName () + '@'
					+ hanPropertiesService.getValue (HAN_EMAIL_DOMAIN_KEY));
			final Session session = Session.getInstance (props, 
					new EmailAuthenticator (user.getScreenName (), user.getEmailPassword ()));
			store = session.getStore ("imap");
			store.connect ();
			Folder original = store.getFolder (folderName);
			Folder newFolder = store.getDefaultFolder ().getFolder ("INBOX").getFolder (newFolderName);
			if (original.exists ()) {
				if (!newFolder.exists ()) {
					original.renameTo (newFolder);
					outcome = EmailOperationOutcome.FOLDER_RENAMED;
				} else {
					outcome = EmailOperationOutcome.FOLDER_ALREADY_EXISTS;
				}
			} else {
				outcome = EmailOperationOutcome.FOLDER_DOES_NOT_EXISTS;
			}
		} catch (Exception ex) {
			log.error ("Error renaming folder.", ex);
			outcome = EmailOperationOutcome.ERROR_RENAMING_FOLDER;
		} finally {
			if (store != null) {
				try {
					store.close ();
				} catch (MessagingException ex) {
					log.error ("Error closing connection", ex);
				}
			}
		}
		return outcome;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.email.services.EmailFolderService#listFolders(long)
	 */
	@Override
	public FolderDetailTO[] listFolders (long userId) {
		Store store = null;
		FolderDetailTO[] names = null;
		try {
			log.debug("listFolders userId: "+ userId);
			//hanadmin / %34dfa63
			//han.initiator.production / St@P3!x5Qj8
			//hanhelpdesk / $^iq29BH
			final Properties props = new Properties (this.props);
			final UserData user = userDataService.getUser (userId);
			props.put ("mail.user", user.getScreenName ());
			props.put ("mail.from", user.getScreenName () + '@'
					+ hanPropertiesService.getValue (HAN_EMAIL_DOMAIN_KEY));
			// hanadmin %34dfa63 //user.getScreenName () user.getEmailPassword ()
			Session session = Session.getInstance (props, 
					new EmailAuthenticator ("hanadmin", "%34dfa63"));
			log.debug("listFolders session: "+ session);
			try{
	
			store = session.getStore ("imap");
			store.connect ();
			log.debug("connecting to imap store.getURLName(): "+ store.getURLName());
			log.debug("connecting to imap session.getPasswordAuthentication(store.getURLName(): "+ session.getPasswordAuthentication(store.getURLName()));

			
			Properties it = session.getProperties();
			Enumeration en = it.elements();
			
			while( en.hasMoreElements()){
				log.debug("it.elements().nextElement(): "+en.nextElement());
			}
			
			}catch(Exception e){
				log.error(e);
				Properties it = session.getProperties();
				Enumeration en = it.elements();
				
				while( en.hasMoreElements()){
					log.debug("it.elements().nextElement(): "+en.nextElement());
				}
			}
			
			try {
				log.debug("listFolders userId: "+ userId);
				//hanadmin / %34dfa63
				//han.initiator.production / St@P3!x5Qj8
				//hanhelpdesk / $^iq29BH
				//final Properties props = new Properties (this.props);
				//final UserData user = userDataService.getUser (userId);
				props.put ("mail.user", user.getScreenName ());
				props.put ("mail.from", user.getScreenName () + '@'
						+ hanPropertiesService.getValue (HAN_EMAIL_DOMAIN_KEY));
				// hanadmin %34dfa63 //user.getScreenName () user.getEmailPassword ()
				session = Session.getInstance (props, 
						new EmailAuthenticator ("han.initiator.production", "St@P3!x5Qj8"));
				log.debug("listFolders session: "+ session);
			
		
				store = session.getStore ("imap");
				store.connect ();
				log.debug("connecting to imap store.getURLName(): "+ store.getURLName());
				log.debug("connecting to imap session.getPasswordAuthentication(store.getURLName(): "+ session.getPasswordAuthentication(store.getURLName()));

				
				Properties it = session.getProperties();
				Enumeration en = it.elements();
				
				while( en.hasMoreElements()){
					log.debug("it.elements().nextElement(): "+en.nextElement());
				}
				
				}catch(Exception e){
					log.error(e);
					Properties it = session.getProperties();
					Enumeration en = it.elements();
					
					while( en.hasMoreElements()){
						log.debug("it.elements().nextElement(): "+en.nextElement());
					}
				}
			
			try {
				log.debug("listFolders userId: "+ userId);
				//hanadmin / %34dfa63
				//han.initiator.production / St@P3!x5Qj8
				//hanhelpdesk / $^iq29BH
				//final Properties props = new Properties (this.props);
				//final UserData user = userDataService.getUser (userId);
				props.put ("mail.user", user.getScreenName ());
				props.put ("mail.from", user.getScreenName () + '@'
						+ hanPropertiesService.getValue (HAN_EMAIL_DOMAIN_KEY));
				// hanadmin %34dfa63 //user.getScreenName () user.getEmailPassword ()
				session = Session.getInstance (props, 
						new EmailAuthenticator ("hanhelpdesk", "$^iq29BH"));
				log.debug("listFolders session: "+ session);
			
		
				store = session.getStore ("imap");
				store.connect ();
				log.debug("connecting to imap store.getURLName(): "+ store.getURLName());
				log.debug("connecting to imap session.getPasswordAuthentication(store.getURLName(): "+ session.getPasswordAuthentication(store.getURLName()));

				
				Properties it = session.getProperties();
				Enumeration en = it.elements();
				
				while( en.hasMoreElements()){
					log.debug("it.elements().nextElement(): "+en.nextElement());
				}
				
				}catch(Exception e){
					log.error(e);
					Properties it = session.getProperties();
					Enumeration en = it.elements();
					
					while( en.hasMoreElements()){
						log.debug("it.elements().nextElement(): "+en.nextElement());
					}
				}
			
			final Folder root = store.getDefaultFolder ();
			final Folder[] folders = root.list ();
			names = new FolderDetailTO[folders.length];
			log.debug ("Folders: " + folders.length);
			for (int i = 0; i < folders.length; i++) {
				names[i] = new FolderDetailTO (folders[i].getName (), folders[i].getFullName (),
						folders[i].getSeparator (), folders[i].getUnreadMessageCount () > 0,
						folders[i].getUnreadMessageCount (), folders[i].getMessageCount (),
						getChildren (folders[i]));
				log.debug ("Name: " + folders[i].getFullName () + " Messages: " + folders[i].getMessageCount ()
						+ " New Messages: " + folders[i].getNewMessageCount () + " Unread: "
						+ folders[i].getUnreadMessageCount ());
			}
		} catch (Exception ex) {
			log.error ("Error retrieving folder names", ex);
		} finally {
			if (store != null) {
				try {
					store.close ();
				} catch (MessagingException ex) {
					log.error ("Error closing connection", ex);
				}
			}
		}
		
		//TODO: remove this test for mail services
		long[] uid=new long[]{14143l};
		long[] dunno=new long[]{14143l};
		String content="";
		String subject="";
		try{
		List<FileTO> attachments = null;//han_admin@chicagohan.org
		EmailSenderServiceImpl emailSenderService = new EmailSenderServiceImpl();
		log.debug(" emailsender test uid[0]: "+uid[0]);
		emailSenderService.sendAdminEmail(uid, dunno, content, subject, attachments);
		log.debug(" completed emailSenderService.sendAdminEmail: uid[0]: "+uid[0]);
		}catch(Exception e){
			log.error("email send test error");
		}
		
		try{
			UserSynchService mir3Service = new UserSynchServiceImpl();
			/*mir3Service.addRecipients(recipients, 
					hanPropertiesService.getValue(MIR3_ADMIN_USER), 
					hanPropertiesService.getValue(MIR3_ADMIN_PASS), 
					hanPropertiesService.getValue(API_VERSION_KEY));*/
			String MIR3_ADMIN_USER = "MIR3_ADMIN_USER";
			/** MIR3 Admin User Password */
			String MIR3_ADMIN_PASS = "MIR3_ADMIN_PASS";
			/** MIR3 API Version key */
			String API_VERSION_KEY = "MIR3_API_VERSION";
			HanPropertiesService hanPropertiesService = new HanPropertiesServiceImpl();
			String miradmin =  hanPropertiesService.getValue(MIR3_ADMIN_USER);
			String mirpass =  hanPropertiesService.getValue(MIR3_ADMIN_PASS);
			String api =  hanPropertiesService.getValue(API_VERSION_KEY);
			
		Mir3UserTO mto = mir3Service.searchRecipient("han30tst4", miradmin, mirpass, api);
		log.debug("mto1: "+mto);
		mto = mir3Service.searchRecipient("han30tst3", miradmin, mirpass, api);
		log.debug("mto2: "+mto);
		mto = mir3Service.searchRecipient("han.admin", miradmin, mirpass, api);
		log.debug("mto3: "+mto);
		
		log.debug("mto: "+mto);
		}catch(Exception e){
			log.error("error looked up mir3 han user: "+e);
		}
		
		
		return names;
	}

	/**
	 * This method retrieves all the folders in the containing folder
	 * @param parent folder
	 * @return sub folders
	 * @throws MessagingException in case of errors while retrieving the folders
	 */
	private FolderDetailTO[] getChildren (final Folder parent) throws MessagingException {
		final List<FolderDetailTO> children = new ArrayList<FolderDetailTO> ();
		final Folder[] subFolders = parent.list ();
		if (subFolders != null) {
			for (Folder folder : subFolders) {
				children.add (new FolderDetailTO (folder.getName (), folder.getFullName (),
						folder.getSeparator (), folder.getUnreadMessageCount () > 0, folder.getUnreadMessageCount (),
						folder.getMessageCount (), getChildren (folder)));
				log.debug ("Name: " + folder.getFullName () + " Messages: " + folder.getMessageCount ()
						+ " New Messages: " + folder.getNewMessageCount () + " Unread: "
						+ folder.getUnreadMessageCount ());
			}
		}
		return children.toArray (new FolderDetailTO[children.size ()]);
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.email.services.EmailFolderService#emptyTrash(long)
	 */
	@Override
	public void emptyTrash (final long userId) throws MessagingException {
		Store store = null;
		final Properties props = new Properties (this.props);
		final UserData user = userDataService.getUser (userId);
		props.put ("mail.user", user.getScreenName ());
		props.put ("mail.from", user.getScreenName () + '@'
				+ hanPropertiesService.getValue (HAN_EMAIL_DOMAIN_KEY));
		final Session session = Session.getInstance (props, 
				new EmailAuthenticator (user.getScreenName (), user.getEmailPassword ()));
		store = session.getStore ("imap");
		store.connect ();
		final Folder trash = store.getDefaultFolder ().getFolder ("INBOX").getFolder ("Trash");
		trash.open (Folder.READ_WRITE);
		trash.close (true);
	}

}
