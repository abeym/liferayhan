package org.cdph.han.util.jobs;

import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.util.services.DailyUserSynchService;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * Quartz job to synchronize all users modified between job executions
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class UserSynchJob extends QuartzJobBean {
	/** Class log */
	private static final Log log = LogFactory.getLog (UserSynchJob.class);

	/* (non-Javadoc)
	 * @see org.springframework.scheduling.quartz.QuartzJobBean#executeInternal(org.quartz.JobExecutionContext)
	 */
	@Override
	protected void executeInternal (final JobExecutionContext jobContext)
			throws JobExecutionException {
		log.info ("Beginning user synchronization with mir3");
		try {
			final ApplicationContext appCtx =
				(ApplicationContext) jobContext.getScheduler ().getContext ().get ("applicationContext");
			DailyUserSynchService dailyService = (DailyUserSynchService) appCtx.getBean ("dailyUserSynchService");
			Date fromDate = jobContext.getPreviousFireTime ();
			if (fromDate == null) {
				final GregorianCalendar calendar = (GregorianCalendar) GregorianCalendar.getInstance ();
				calendar.add (GregorianCalendar.DAY_OF_YEAR, -1);
				fromDate = calendar.getTime ();
			}
			dailyService.synchronizeUsers (fromDate);
		} catch (Exception ex) {
			log.error ("Error updating users at mir3", ex);
		}
	}

}
