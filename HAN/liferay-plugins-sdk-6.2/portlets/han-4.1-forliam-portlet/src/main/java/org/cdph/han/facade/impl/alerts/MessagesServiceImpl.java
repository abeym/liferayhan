package org.cdph.han.facade.impl.alerts;

import java.util.Locale;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.cdph.han.alerts.services.*;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

/**
 * Implementation of the MessagesService
 * @author Abey Mani
 * @version 1.0
 */
@Repository
@Service ("messagesService")
@Scope ("singleton")
public class MessagesServiceImpl implements MessagesService {
	/** Name of the bundle to load */
	private static final String BASE_NAME = "/messages";

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.MessagesService#getMessage(java.lang.String, java.lang.String)
	 */
	public FacesMessage getMessage (final String messageKey, final String defaultMessage, final FacesMessage.Severity severity) {
		final ResourceBundle bundle = ResourceBundle.getBundle (BASE_NAME,
				FacesContext.getCurrentInstance ().getExternalContext ().getRequestLocale ());
		final String message = bundle.getString (messageKey);
		return new FacesMessage(severity, message == null ? defaultMessage : message,
				message == null ? defaultMessage : message);
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.MessagesService#getMessage(java.lang.String)
	 */
	public String getMessage (final String messageKey) {
		final Locale locale = FacesContext.getCurrentInstance () != null ? 
				FacesContext.getCurrentInstance ().getExternalContext ().getRequestLocale () :
					Locale.US;
		final ResourceBundle bundle = ResourceBundle.getBundle (BASE_NAME, locale);
		return bundle.getString (messageKey);
	}

}
