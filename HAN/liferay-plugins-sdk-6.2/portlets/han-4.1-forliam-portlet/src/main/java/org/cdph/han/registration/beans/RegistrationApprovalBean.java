package org.cdph.han.registration.beans;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.beans.BaseBean;
import org.cdph.han.facade.impl.registration.RegistrationServiceImpl;
import org.cdph.han.model.Preferences;
import org.cdph.han.model.UserAreaFieldAsoc;
import org.cdph.han.persistence.AreaField;
import org.cdph.han.persistence.Organization;
import org.cdph.han.persistence.RequestForm;
import org.cdph.han.persistence.Role;
import org.cdph.han.registration.dto.RequestFormDTO;
import org.cdph.han.registration.service.RegistrationService;
import org.cdph.han.service.AlternateContactLocalServiceUtil;
import org.cdph.han.service.PreferencesLocalServiceUtil;
import org.cdph.han.service.UserAreaFieldAsocLocalServiceUtil;
import org.cdph.han.util.services.SecurityServiceImpl;
//import org.cdph.han.service.Address1LocalServiceUtil;
import org.cdph.han.service.AddressLocalServiceUtil;
import org.icefaces.ace.model.table.RowStateMap;
import org.springframework.beans.factory.annotation.Autowired;

import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.DuplicateUserEmailAddressException;
import com.liferay.portal.DuplicateUserScreenNameException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.util.PortalUtil;
//import com.liferay.util.bridges.jsf.common.JSFPortletUtil;



public class RegistrationApprovalBean extends BaseBean {

	/** Class logger */
	private static Log log = LogFactory.getLog(RegistrationApprovalBean.class);
	
	//abey...
	//@Autowired
	private RegistrationService service;

	private RequestForm registration;
	private RequestFormDTO registrations;
	private Long selectedOrgType;
	private Long selectedOrg;
	private Integer selectedAreaField;
	private Long selectedRole;
	private SelectItem[] organizationTypes = new SelectItem[]{};
	private SelectItem[] organizations = new SelectItem[]{};
	private SelectItem[] areaFields = new SelectItem[]{};
	private SelectItem[] roles = new SelectItem[]{};
	private RowStateMap stateMap = new RowStateMap();
		
	/**
	 * Action listener for the row that sets the selected registration
	 */
	public String rowSelectionAction(org.icefaces.ace.event.SelectEvent se){
		log.debug("In rowSelectionAction()...");
		try{
			RequestForm registration = (RequestForm)se.getObject();
			FacesContext facesContext = FacesContext.getCurrentInstance();
			ExternalContext externalContext = facesContext.getExternalContext();
			PortletRequest portletRequest = (PortletRequest) externalContext.getRequest();
			long companyId = PortalUtil.getCompanyId(portletRequest);
			PortletSession session = portletRequest.getPortletSession();			
		    session.setAttribute("reqform", registration ,PortletSession.APPLICATION_SCOPE);
			//for (RequestForm registration : registrations.getWrappedData()) {//RequestForm
			//	if(registration.isSelected()){
					this.registration = registration;
					selectedOrgType = this.registration.getOrganizationType () == null ? -1
							: this.registration.getOrganizationType ().getOrganizationId ();
					selectedOrg = this.registration.getOrganization () == null ? null
							: this.registration.getOrganization ().getOrganizationId ();
					selectedAreaField = this.registration.getAreaField () == null ? -1
							: this.registration.getAreaField ().getAreaFieldId ();
					selectedRole = this.registration.getRole () == null ? -1
							: this.registration.getRole ().getRoleId ();
					//TODO: fill out han model objects
					//OrganizationLocalServiceUtil
					ensureService();
					
					final List<Organization> orgTypes = service.getOrganizationTypes ();
					organizationTypes = new SelectItem[orgTypes.size ()];
					int i = 0;
					for (Organization orgType : orgTypes) {
						organizationTypes[i++] = new SelectItem (orgType.getOrganizationId (), orgType.getName ());
					}
					if (selectedOrgType > 0) {
						retrieveOrganizationalData ();
					} else {
						organizations = new SelectItem[]{};
						areaFields = new SelectItem[]{};
						roles = new SelectItem[]{};
					}
					return "reviewRequest";
				//}
			//}
		}catch(Exception e){
			log.error(e);
			return "";
		}
		
		
	}

	private void retrieveOrganizationalData () {
		log.debug ("Retrieving organizations for: " + selectedOrgType);
		ensureService();
		final List<Organization> organizations = service.getSuborganizations (selectedOrgType);
		this.organizations = new SelectItem[organizations.size () + 1];
		int i = 0;
		this.organizations[i++] = new SelectItem (-1, "");
		for (Organization org : organizations) {
			this.organizations[i++] = new SelectItem (org.getOrganizationId (), org.getName ());
		}
		final List<AreaField> areaFields = service.getAreaFields (selectedOrgType);
		this.areaFields = new SelectItem[areaFields.size ()];
		i = 0;
		for (AreaField areaField : areaFields) {
			this.areaFields[i++] = new SelectItem (areaField.getAreaFieldId (), areaField.getDescription ());
		}
		final List<Role> roles = service.getOrganizationalRoles (selectedOrgType);
		this.roles = new SelectItem[roles.size ()];
		i = 0;
		for (Role role : roles) {
			this.roles[i++] = new SelectItem (role.getRoleId (), role.getName ());
		}
	}

	/**
	 * Value change listener used to retrieve the new organizations when changing the organization type
	 * @param event fired by the value change
	 */        
	public void organizationTypeChanged (final ValueChangeEvent event) {
		final Long newValue = (Long) event.getNewValue ();
		selectedOrgType = newValue;
		if (selectedOrgType > 0) {
			retrieveOrganizationalData ();
		} else {
			organizations = new SelectItem []{};
			areaFields = new SelectItem[]{};
			roles = new SelectItem[]{};
		}
	}
	
	/**
	 * Action listener for back button. It clears
	 * the request form list of previously selected 
	 * requests forms.
	 */
	public String reviewBackButtonAction(){
		log.debug("In reviewBackButtonAction()...");
		
		for (RequestForm registration : registrations.getWrappedData()) {
			if(registration.isSelected()){
				registration.setSelected(false);
			}
		}
		registrations.clearCache();
		
		return "back";
		
	}
	
	/**
	 * Action listener for reject button. 
	 */
	public String rejectButtonAction(){
		log.debug("In rejectButtonAction()...");
		ensureService();
		service.rejectRequest(registration);
		service.enqueueNotificationEmail(registration.getId());
		
		registrations.clearCache();

		return "reject";
		
	}
	public void ensureService(){
		service= service==null?new RegistrationServiceImpl():service;
	}
	
	/**
	 * Action listener for approve button. 
	 */
	public String approveButtonAction(){
		log.debug("In approveButtonAction()...");
		ensureService();
		String nextAction = "approve";			
		
		/*long companyId = JSFPortletUtil.getCompanyId(
				FacesContext.getCurrentInstance());*/
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ExternalContext externalContext = facesContext.getExternalContext();
		PortletRequest portletRequest = (PortletRequest) externalContext.getRequest();
		long companyId = PortalUtil.getCompanyId(portletRequest);

		PortletSession session = portletRequest.getPortletSession();
	    //session.setAttribute("reqform", registration ,PortletSession.APPLICATION_SCOPE);
		registration= (RequestForm) session.getAttribute("reqform",PortletSession.APPLICATION_SCOPE); 
		
		try{
			registration.setCompanyId(companyId);
			Organization orgType =registration.getOrganizationType();
			Organization org = registration.getOrganization();
			AreaField af = registration.getAreaField();
			Role r = registration.getRole();
			if (orgType == null || org == null || orgType.getOrganizationId() == -1 || org.getOrganizationId() == -1) {
				addFacesMessage("registration.approval.review.organization.other.selected");
				nextAction = "failure";
			} else {
				registration.setOrganization (org);//service.getOrganization (org.getOrganizationId())
				registration.setOrganizationType (orgType);//service.getOrganization (orgType.getOrganizationId())
				registration.setAreaField (af);//service.getAreaField (selectedAreaField)
				registration.setRole (r);//service.getRole (selectedRole)
				service.approveRequest(registration);
			}

			
		}catch(DuplicateUserEmailAddressException duex){log.error(duex);	
			addFacesMessage("registration.approval.review.duplicate.user.email");
			nextAction = "failure";
		}catch(DuplicateUserScreenNameException dusnex){log.error(dusnex);
			addFacesMessage("registration.approval.review.duplicate.user.screename");
			nextAction = "failure";
		}catch(Exception ex){
			log.error(ex);
			addFacesMessageNoBundle(ex.getMessage());
			//nextAction = "failure";TODO: put back
		}
		
	//	registrations.clearCache();
		
		return nextAction;
		
	}
	
	
	
	public void setService(RegistrationService service) {
		this.service = service;
	}	
	
	public List<RequestForm> byPassForTest(){
		List<RequestFormDTO> rfdtos = new ArrayList<RequestFormDTO>();
		//final  List<RequestForm> rfs = registrations.findRows(0,100);
		List<RequestForm> rfs = new ArrayList<RequestForm>();
		//AddressLocalServiceImpl
		//AddressLocalServiceUtil
		
		try {
			log.error("byPassForTest");
			//AddressLocalServiceUtil.createAddress1(1);
			Preferences preference = PreferencesLocalServiceUtil.createPreferences (
					CounterLocalServiceUtil.increment ());
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			log.error(e);
		}
		return rfs;
	}
	
	public List<RequestForm> getPendingRegistrations() {
		 List<RequestFormDTO> rfdtos = new ArrayList<RequestFormDTO>();
		// if(true)return byPassForTest();
		 final List<RequestForm> rfsempty=new ArrayList<RequestForm>();  
		 try{
		 service=service==null?new RegistrationServiceImpl():service;
		if(registrations == null){
			 registrations= new RequestFormDTO(service);
		}//else if(registrations){
	/*	try{
			//Object o=AlternateContactLocalServiceUtil.findByUserId(1l);
			//log.debug(o);
			UserAreaFieldAsoc userAreaFieldAsoc = UserAreaFieldAsocLocalServiceUtil.createUserAreaFieldAsoc(null);
					//.create(userAreaFieldpk);
			log.debug("UserAreaFieldAsoc ID: "
					+ userAreaFieldAsoc.getPrimaryKey());
			}catch(Exception e){
				e.printStackTrace();
				log.error(e);
			}
		//}*/
		
		final List<RequestForm> rfs = registrations.findRows(0,100);
			/* if(rfs!=null)for(RequestForm rf:rfs){
				RequestFormDTO rfdto = new RequestFormDTO(service);
				rfdto.setFirstName(rf.getFirstName());
				rfdto.setId(rf.getId());
				rfdto.setLastName(rf.getLastName());
				 rfdtos.add(rfdto);
				 rf.getApprovedRejectedDate();
				// rf.getS
			 }*/
			 
			 
			 registrations.setWrappedData(rfs);
		//}/*else{
			//registrations.clearCache();
		//}*/
	//	registrations.setRowCount(100);
	//	registrations.setRowIndex(0);
	//	registrations.setRowsPerPage(100);
			 return rfs;
		 }catch(Exception e){
			 log.error(e);
		 }
		return null;// registrations
	}

	public void setPendingRegistrations(RequestFormDTO registrations) {		
		this.registrations = registrations;
	}


	public RequestForm getRegistration() {
		return registration;
	}


	public void setRegistration(RequestForm registration) {
		this.registration = registration;
	}

	/**
	 * @return the selectedOrgType
	 */
	public Long getSelectedOrgType () {
		return selectedOrgType;
	}

	/**
	 * @param selectedOrgType the selectedOrgType to set
	 */
	public void setSelectedOrgType (Long selectedOrgType) {
		this.selectedOrgType = selectedOrgType;
	}

	/**
	 * @return the selectedOrg
	 */
	public Long getSelectedOrg () {
		return selectedOrg;
	}

	/**
	 * @param selectedOrg the selectedOrg to set
	 */
	public void setSelectedOrg (Long selectedOrg) {
		this.selectedOrg = selectedOrg;
	}

	/**
	 * @return the organizationTypes
	 */
	public SelectItem[] getOrganizationTypes () {
		return organizationTypes;
	}

	/**
	 * @param organizationTypes the organizationTypes to set
	 */
	public void setOrganizationTypes (SelectItem[] organizationTypes) {
		this.organizationTypes = organizationTypes;
	}

	/**
	 * @return the organizations
	 */
	public SelectItem[] getOrganizations () {
		for (SelectItem item : organizations) {
			/*if (item == null) {
				log.debug ("Organization null");
			} else {
				log.debug ("ID: " + item.getValue () + " NAME: " + item.getLabel ());
			}*/
		}
		return organizations;
	}

	/**
	 * @param organizations the organizations to set
	 */
	public void setOrganizations (SelectItem[] organizations) {
		this.organizations = organizations;
	}

	/**
	 * @return the selectedAreaField
	 */
	public Integer getSelectedAreaField () {
		return selectedAreaField;
	}

	/**
	 * @param selectedAreaField the selectedAreaField to set
	 */
	public void setSelectedAreaField (Integer selectedAreaField) {
		this.selectedAreaField = selectedAreaField;
	}

	/**
	 * @return the selectedRole
	 */
	public Long getSelectedRole () {
		return selectedRole;
	}

	/**
	 * @param selectedRole the selectedRole to set
	 */
	public void setSelectedRole (Long selectedRole) {
		this.selectedRole = selectedRole;
	}

	/**
	 * @return the areaFields
	 */
	public SelectItem[] getAreaFields () {
		return areaFields;
	}

	/**
	 * @param areaFields the areaFields to set
	 */
	public void setAreaFields (SelectItem[] areaFields) {
		this.areaFields = areaFields;
	}

	/**
	 * @return the roles
	 */
	public SelectItem[] getRoles () {
		return roles;
	}

	/**
	 * @param roles the roles to set
	 */
	public void setRoles (SelectItem[] roles) {
		this.roles = roles;
	}

	public RowStateMap getStateMap() {
		return stateMap;
	}

	public void setStateMap(RowStateMap stateMap) {
		this.stateMap = stateMap;
	}
}
