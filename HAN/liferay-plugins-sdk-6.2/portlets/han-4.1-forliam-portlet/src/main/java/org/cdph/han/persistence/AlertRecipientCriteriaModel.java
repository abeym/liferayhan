package org.cdph.han.persistence;

public interface AlertRecipientCriteriaModel {

	/**
	 * @return the criteriaId
	 */
	long getCriteriaId ();

	/**
	 * @return the recipient
	 */
	AlertRecipientModel getRecipient ();

	/**
	 * @return the organizationType
	 */
	Organization getOrganizationType ();

	/**
	 * @return the organization
	 */
	Organization getOrganization ();

	/**
	 * @return the areaField
	 */
	AreaField getAreaField ();

	/**
	 * @return the role
	 */
	Role getRole ();

}