package org.cdph.han.audittrail.service;

import java.util.List;

import org.cdph.han.audittrail.dto.AuditTrailTO;
import org.cdph.han.dto.HanLazyDataModel;

public interface AuditTrailService {

	public Integer getAuditTrailCount(HanLazyDataModel criteria);

	public List<AuditTrailTO> getAuditTrailRecords(HanLazyDataModel criteria,
			int startIndex, int maxResults);

}
