package org.cdph.han.devpref.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.cdph.han.alerts.services.UserDataService;
import org.cdph.han.custgroups.dto.UserDisplayTO;
import org.cdph.han.persistence.Address;
import org.cdph.han.persistence.AlternateContact;
import org.cdph.han.persistence.ContactPreference;
import org.cdph.han.persistence.EmailAddress;
import org.cdph.han.persistence.ListType;
import org.cdph.han.persistence.Organization;
import org.cdph.han.persistence.PhoneDevice;
import org.cdph.han.persistence.UserData;
import org.cdph.han.persistence.UserOrganizationalData;
import org.cdph.han.synch.dto.Mir3DeviceTO;
import org.cdph.han.synch.dto.Mir3UserTO;
import org.cdph.han.synch.services.UserSynchService;
import org.cdph.han.util.services.HanPropertiesService;
import org.cdph.han.util.services.SecurityService;
import org.cdph.han.util.services.SecurityServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of the ContactDevicesService interface using the UserDataService to get the current user information
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
//@Repository
//@Service ("contactDevicesService")
public class ContactDevicesServiceImpl implements ContactDevicesService {
	public ContactDevicesServiceImpl()
	{
		System.out.println("\n\n >>>>>>>>>>>>ContactDevicesServiceImpl:"+new Throwable("For testing !!!"));
	}
	/** Max length for custom fields */
	private static final int MAX_LENGTH = 100;
	/** UserDataService used to get access to the current user data */
	//abey...
	//@Autowired
	private UserDataService userDataService;
	/** Persistence context */
	private EntityManager entityManager;
	/** Service to synchronize users */
	//abey...
	//@Autowired
	private UserSynchService userSynchService;
	/** Service to retrieve the mir3 user credentials */
	//abey...
	//@Autowired
	private HanPropertiesService hanPropertiesService;
	/** Service to check the user role */
	//abey...
	//@Autowired
	private SecurityService securityService;

	/* (non-Javadoc)
	 * @see org.cdph.han.devpref.services.ContactDevicesService#loadContactDevices(org.cdph.han.alerts.services.UserDataService)
	 */
	//abey ...@Transactional (readOnly = true)
	public List<SelectItem> loadContactDevices () {
		final List<SelectItem> devices = new ArrayList<SelectItem> ();
		final UserData userData = userDataService.getCurrentUserData ();
		SelectItem device = new SelectItem ("PRIMARY", userData.getEmail ());
		devices.add (device);
		for (PhoneDevice phoneDevice : userData.getContact ().getPhoneDevices ()) {
			device = new SelectItem ();
			if (phoneDevice.getFriendlyName () != null && phoneDevice.getFriendlyName ().length () > 0) {
				device.setLabel (phoneDevice.getFriendlyName () + " - " + phoneDevice.getPhoneNumber ());
			} else {
				device.setLabel (phoneDevice.getPhoneNumber ());
			}
			device.setValue ("P:" + phoneDevice.getPhoneId ());
			devices.add (device);
		}
		for (EmailAddress email : userData.getContact ().getEmailAddresses ()) {
			device = new SelectItem ();
			if (email.getFriendlyName () != null && email.getFriendlyName ().length () > 0) {
				device.setLabel (email.getFriendlyName () + " - " + email.getAddress ());
			} else {
				device.setLabel (email.getAddress ());
			}
			device.setValue ("M:" + email.getEmailId ());
			devices.add (device);
		}
		return devices;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.devpref.services.ContactDevicesService#loadAlternateContacts()
	 */
	//abey ...@Transactional (readOnly = true)
	public List<UserDisplayTO> loadAlternateContacts () {
		final List<UserDisplayTO> alternateContacts = new ArrayList<UserDisplayTO> ();
		final UserData userData = userDataService.getCurrentUserData ();
		for (AlternateContact alternateContact : userData.getAlternateContacts ()) {
			alternateContacts.add (new UserDisplayTO (alternateContact.getAlternateContact ().getUserId (),
					alternateContact.getAlternateContact ().getContact ().getFirstName (),
					alternateContact.getAlternateContact ().getContact ().getLastName (),
					"", ""));
		}
		return alternateContacts;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.devpref.services.ContactDevicesService#loadPreferences()
	 */
	//abey ...@Transactional (readOnly = true)
	public String[] loadPreferences () {
		final String[] preferences = new String[6];
		final UserData userData = userDataService.getCurrentUserData ();
		for (ContactPreference contactPreference : userData.getContactPreferences ()) {
			if (contactPreference.getPrimaryEmail ()) {
				preferences[contactPreference.getIndex ()] = "PRIMARY";
			} else if (contactPreference.getEmailAddress () != null) {
				preferences[contactPreference.getIndex ()] = "M:" + contactPreference.getEmailAddress ().getEmailId ();
			} else if (contactPreference.getPhoneDevice () != null) {
				preferences[contactPreference.getIndex ()] = "P:" + contactPreference.getPhoneDevice ().getPhoneId ();
			}
		}
		return preferences;
	}

	//abey ...@Transactional (propagation = Propagation.REQUIRED)
	public void savePreferences (final String[] preferences, final List<UserDisplayTO> alternateContacts,
			final Long userId) {
		savePreferences (preferences, alternateContacts, null, null, null, userId);
	}
	
	/* (non-Javadoc)
	 * @see org.cdph.han.devpref.services.ContactDevicesService#savePreferences(java.lang.String[], java.util.List)
	 */
	//abey ...@Transactional (propagation = Propagation.REQUIRED)
	public void savePreferences (final String[] preferences, final List<UserDisplayTO> alternateContacts,
			final String telephonyId, final String telephonyPIN, final String telephonyPass) {
		final UserData userData = userDataService.getCurrentUserData ();	
		savePreferences (preferences, alternateContacts, telephonyId,
				telephonyPIN, telephonyPass, userData.getUserId());
	}

	/**
	 * @param entityManager the entityManager to set
	 */
	//abey...
	//@PersistenceContext
	public void setEntityManager (EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	/**
	 * Saves the devices preferences and updates the telephony data
	 * @param preferences list
	 * @param alternateContacts list
	 * @param telephonyId new id
	 * @param telephonyPIN new pin
	 * @param telephonyPass new pass
	 * @param userId
	 */
	private void savePreferences (final String[] preferences, final List<UserDisplayTO> alternateContacts,
			final String telephonyId, final String telephonyPIN, final String telephonyPass, final Long userId) {
		final UserData userData = userDataService.getUser(userId);
		// Retrieve the current contact preferences
		Collection<ContactPreference> dbPreferences = userData.getContactPreferences ();
		List<ContactPreference> toRemove = new ArrayList<ContactPreference> ();
		if (dbPreferences != null && !dbPreferences.isEmpty ()) {
			for (ContactPreference preference : dbPreferences) {
				if (preferences[preference.getIndex ()] != null && !preferences[preference.getIndex ()].equals ("")) {
					if (preferences[preference.getIndex ()].equals ("PRIMARY")) {
						preference.setPrimaryEmail (true);
						preference.setPhoneDevice (null);
						preference.setEmailAddress (null);
					} else if (preferences[preference.getIndex ()].startsWith ("M:")) {
						preference.setPrimaryEmail (false);
						preference.setPhoneDevice (null);
						preference.setEmailAddress (entityManager.find (EmailAddress.class, Long.parseLong (
								preferences[preference.getIndex ()].substring (2))));
					} else if (preferences[preference.getIndex ()].startsWith ("P:")) {
						preference.setPrimaryEmail (false);
						preference.setPhoneDevice (entityManager.find (PhoneDevice.class, Long.parseLong (
								preferences[preference.getIndex ()].substring (2))));
						preference.setEmailAddress (null);
					}
					entityManager.merge (preference);
				} else {
					toRemove.add (preference);
				}
			}
			ContactPreference preference = null;
			for (int i = dbPreferences.size (); i < preferences.length; i++) {
				preference = new ContactPreference ();
				preference.setUserData (userData);
				preference.setIndex (i);
				if (preferences[i] == null || preferences[i].equals ("")) {
					continue;
				} else if (preferences[i].equals ("PRIMARY")) {
					preference.setPrimaryEmail (true);
				} else if (preferences[i].startsWith ("M:")) {
					preference.setEmailAddress (entityManager.find (EmailAddress.class, Long.parseLong (
							preferences[preference.getIndex ()].substring (2))));
					preference.setPrimaryEmail (false);
				} else if (preferences[i].startsWith ("P:")) {
					preference.setPhoneDevice (entityManager.find (PhoneDevice.class, Long.parseLong (
							preferences[preference.getIndex ()].substring (2))));
					preference.setPrimaryEmail (false);
				}
				userData.getContactPreferences ().add (preference);
				entityManager.persist (preference);
			}
			if (toRemove.size () > 0) {
				for (ContactPreference pref : toRemove) {
					entityManager.remove (pref);
					userData.getContactPreferences ().remove (pref);
				}
			}
		} else {
			userData.setContactPreferences (new ArrayList<ContactPreference> ());
			ContactPreference preference = null;
			for (int i = 0; i < preferences.length; i++) {
				preference = new ContactPreference ();
				preference.setUserData (userData);
				preference.setIndex (i);
				if (preferences[i] == null) {
					continue;
				} else if (preferences[i].equals ("PRIMARY")) {
					preference.setPrimaryEmail (true);
				} else if (preferences[i].startsWith ("M:")) {
					preference.setEmailAddress (entityManager.find (EmailAddress.class, Long.parseLong (
							preferences[preference.getIndex ()].substring (2))));
					preference.setPrimaryEmail (false);
				} else if (preferences[i].startsWith ("P:")) {
					preference.setPhoneDevice (entityManager.find (PhoneDevice.class, Long.parseLong (
							preferences[preference.getIndex ()].substring (2))));
					preference.setPrimaryEmail (false);
				}
				userData.getContactPreferences ().add (preference);
				entityManager.persist (preference);
			}
		}
		final List<AlternateContact> toDelete = new ArrayList<AlternateContact> ();
		if (alternateContacts != null && !alternateContacts.isEmpty ()) {
			int index = 0;
			if (alternateContacts.size () > userData.getAlternateContacts ().size ()) {
				final AlternateContact[] alternateContactsDB = userData.getAlternateContacts ().toArray (
						new AlternateContact[userData.getAlternateContacts ().size ()]);
				AlternateContact alternateContactDB;
				for (UserDisplayTO alternateContact : alternateContacts) {
					if (index < alternateContactsDB.length) {
						alternateContactDB = alternateContactsDB[index];
					} else {
						alternateContactDB = new AlternateContact ();
						alternateContactDB.setUserData (userData);
					}
					alternateContactDB.setAlternateContact (
							entityManager.find (UserData.class, alternateContact.getId ()));
					alternateContactDB.setIndex (index);
					if (index < alternateContactsDB.length) {
						entityManager.merge (alternateContactDB);
					} else {
						entityManager.persist (alternateContactDB);
						userData.getAlternateContacts ().add (alternateContactDB);
					}
					++index;
				}
			} else {
				UserDisplayTO alternateContact;
				for (AlternateContact alternateContactDB : userData.getAlternateContacts ()) {
					if (index < alternateContacts.size ()) {
						alternateContact = alternateContacts.get (index);
						alternateContactDB.setAlternateContact (
								entityManager.find (UserData.class, alternateContact.getId ()));
						alternateContactDB.setIndex (index);
					} else {
						toDelete.add (alternateContactDB);
					}
					++index;
				}
			}
		} else {
			if (userData.getAlternateContacts () != null && !userData.getAlternateContacts ().isEmpty ()) {
				for (AlternateContact alternateContact : userData.getAlternateContacts ()) {
					toDelete.add (alternateContact);
				}
			}
		}
		for (AlternateContact alternateContact : toDelete) {
			userData.getAlternateContacts ().remove (alternateContact);
			entityManager.remove (alternateContact);
		}
		// Update user telephony preferences
		boolean updateId = telephonyId != null && telephonyId.length () > 0;
		boolean updatePass = telephonyPass != null && telephonyPass.length () > 0;
		boolean updatePIN = telephonyPIN != null && telephonyPIN.length () > 0;
		if (updateId || updatePass || updatePIN) {
			if (updateId) {
				userData.setTelephonyId (telephonyId);
			}
			if (updatePass) {
				userData.setTelephonyPassword (telephonyPass);
			}
			if (updatePIN) {
				userData.setTelephonyPIN (telephonyPIN);
			}
		}
		entityManager.merge (userData);
		final List<Mir3UserTO> recipients = new ArrayList<Mir3UserTO> ();
		recipients.add (prepareUserForSynch (userData));
		//abey - commenting for compilation
		/*
		userSynchService.updateRecipients (recipients,
				hanPropertiesService.getValue ("MIR3_ADMIN_USER"),
				hanPropertiesService.getValue ("MIR3_ADMIN_PASS"),
				hanPropertiesService.getValue ("MIR3_API_VERSION"),
				true);
		*/
	}

	public void ensureService(){
		securityService= securityService==null?new SecurityServiceImpl():securityService;
	}
	/**
	 * Prepares the user information to be sent for update at mir3
	 * @param user with the user information
	 * @return the object to be used for updating the mir3 entry
	 */
	@SuppressWarnings("unchecked")
	private Mir3UserTO prepareUserForSynch (final UserData user) {
		final Mir3UserTO userTO = new Mir3UserTO ();
		userTO.setUsername (user.getEmail ());
		userTO.setFirstName (user.getContact ().getFirstName ());
		userTO.setLastName (user.getContact ().getLastName ());
		userTO.setLanguage ("ENGLISH");
		userTO.setTimezone ("CENTRAL_USA");
		userTO.setTitle (user.getContact ().getJobTitle ());
		userTO.setJobTitle (user.getContact ().getJobTitle ());
		userTO.setDepartment (user.getDepartment ());
		ensureService();
		userTO.setMir3Role (securityService.getMir3UserRole (user.getUserId ()));
		if (user.getMir3Id () != null) {
			//abey - commenting for compilation
			//userTO.setMir3Id (user.getMir3Id ());
		}
		final Query q = entityManager.createNamedQuery ("Address.findByClassPK");
		q.setParameter ("classNameId", 14005L);
		q.setParameter ("classPK", user.getContact ().getContactId ());
		final List<Address> addresses = q.getResultList ();
		// Set the user address
		if (addresses != null && !addresses.isEmpty ()) {
			Address address = addresses.get (0);
			userTO.setAddress1 (address.getStreet1 ());
			userTO.setAddress2 (address.getStreet2 ());
			userTO.setCity (address.getCity ());
			userTO.setCountry ("USA");
			userTO.setFloor (address.getFloor ());
			if (address.getRegion () != null) {
				userTO.setState (address.getRegion ().getName ());
			}
			userTO.setZip (address.getZip ());
		}
		// Set the user alternates
		userTO.setAlternates (new ArrayList<String> ());
		for (AlternateContact contact : user.getAlternateContacts ()) {
			userTO.getAlternates ().add (contact.getAlternateContact ().getEmail ());
		}

		// Set the organizational data
		final StringBuilder organization = new StringBuilder ();
		final StringBuilder areaField = new StringBuilder ();
		final StringBuilder role = new StringBuilder ();
		final StringBuilder orgType = new StringBuilder ();
		for (UserOrganizationalData data : user.getUserOrganizationalData ()) {
			organization.append (data.getOrganization ().getName ()).append (',');
			areaField.append (data.getAreaField ().getDescription ()).append (',');
			role.append (data.getRole ().getName ()).append (',');
			orgType.append (entityManager.find (Organization.class,
					data.getOrganization ().getParentOrganizationId ()).getName ()).append (',');
		}
		// Check that none of the values exceeds the 100 length limit
		userTO.setRole (role.toString ().length () > MAX_LENGTH ?
				role.toString ().substring (0, MAX_LENGTH) : role.toString ().substring (0, role.length () - 1));
		userTO.setOrganization (organization.toString ().length () > MAX_LENGTH ?
				organization.toString ().substring (0, MAX_LENGTH) :
					organization.toString ().substring (0, organization.length () - 1));
		userTO.setOrganizationType (orgType.toString ().length () > MAX_LENGTH ?
				orgType.toString ().substring (0, MAX_LENGTH) :
					orgType.toString ().substring (0, orgType.length () - 1));
		userTO.setAreaField (areaField.toString ().length () > MAX_LENGTH ?
				areaField.toString ().substring (0, MAX_LENGTH) :
					areaField.toString ().substring (0, areaField.length () - 1));
		userTO.setDivision ("/");
		userTO.setDepartment (user.getDepartment ());
		userTO.setTelephonyId (user.getTelephonyId ());
		userTO.setPassword (user.getTelephonyPassword ());
		userTO.setPin (user.getTelephonyPIN ());
		userTO.setDevices (new ArrayList<Mir3DeviceTO> ());
		//userTO.setPreferences (new ArrayList<Mir3DevicePriorityTO> ());
		addDevices (userTO.getDevices (), user.getEmail (), user.getContact ().getEmailAddresses (),
				user.getContact ().getPhoneDevices (), user.getContactPreferences ());
		//userTO.setPreferences (new ArrayList<Mir3DevicePriorityTO> ());
		/*Mir3DevicePriorityTO preference;
		if (user.getContactPreferences () == null || user.getContactPreferences ().isEmpty ()) {
			preference = new Mir3DevicePriorityTO ();
			preference.setIndex (1);
			preference.setPriority (1);
		} else {
			int i = 1;
			for (ContactPreference pref : user.getContactPreferences ()) {
				preference = new Mir3DevicePriorityTO ();
				preference.setPriority (i++);
				if (pref.getPrimaryEmail ()) {
					preference.setIndex (1);
				} else if (pref.getEmailAddress () != null) {
					preference.setIndex (findEmail (pref.getEmailAddress (), userTO.getDevices ()));
				} else if (pref.getPhoneDevice () != null) {
					preference.setIndex (findPhone (pref.getPhoneDevice (), userTO.getDevices ()));
				}
			}
		}*/
		return userTO;
	}

	/**
	 * Returns the index of the phone device in the devices list
	 * @param phoneDevice to search
	 * @param devices list to search
	 * @return the index in the list of the phone device
	 */
	private int findPhone (final PhoneDevice phoneDevice, final Collection<ContactPreference> preferences) {
		int index = -1;
		for (ContactPreference preference : preferences) {
			if (preference.getPhoneDevice () != null && preference.getPhoneDevice ().equals (phoneDevice)) {
				index = preference.getIndex () + 1;
				break;
			}
		}
		return index;
	}

	/**
	 * Search for the index in the given list of the email address
	 * @param emailAddress to search
	 * @param devices list to search into
	 * @return index in the list
	 */
	private int findEmail (final EmailAddress emailAddress, final Collection<ContactPreference> preferences) {
		int index = -1;
		for (ContactPreference preference : preferences) {
			if (preference.getEmailAddress () != null && preference.getEmailAddress ().equals (emailAddress)) {
				index = preference.getIndex () + 1;
				break;
			}
		}
		return index;
	}

	/**
	 * Adds all the available devices into the user object
	 * @param devices the list to add the devices
	 * @param emailAddresses the email addresses the user has configured
	 * @param phoneDevices the phone devices the user has configured
	 */
	private void addDevices (final List<Mir3DeviceTO> devices, final String emailAddress,
			final Collection<EmailAddress> emailAddresses, final Collection<PhoneDevice> phoneDevices,
			final Collection<ContactPreference> preferences) {
		Mir3DeviceTO device = new Mir3DeviceTO ();
		device.setAddress (emailAddress);
		device.setDescription ("Primary Work Email");
		device.setType ("Work Email");
		int priority = getPrimaryEmailPriority (preferences);
		if (priority > 0) {
			device.setPriority (String.valueOf (priority));
		} else {
			device.setPriority ("OFF");
		}
		devices.add (device);
		for (PhoneDevice phone : phoneDevices) {
			device = new Mir3DeviceTO ();
			priority = findPhone (phone, preferences);
			if (priority > 0) {
				device.setPriority (String.valueOf (priority));
			} else {
				device.setPriority ("OFF");
			}
			if (phone.getExtension () != null && phone.getExtension ().length () > 0) {
				device.setAddress (phone.getPhoneNumber () + 'x' + phone.getExtension ());
			} else {
				device.setAddress (phone.getPhoneNumber ());
			}
			device.setDescription (phone.getFriendlyName ());
			if (phone.getPagerCarrier () != null) {
				//abey - commenting for compilation
				//device.setPagerCarrier (phone.getPagerCarrier ().getMir3Id ());
			}
			device.setType (entityManager.find (ListType.class, Long.valueOf (phone.getTypeId ())).getName ());
			devices.add (device);
		}
		for (EmailAddress email : emailAddresses) {
			device = new Mir3DeviceTO ();
			priority = findEmail (email, preferences);
			if (priority > 0) {
				device.setPriority (String.valueOf (priority));
			} else {
				device.setPriority ("OFF");
			}
			device.setAddress (email.getAddress ());
			device.setDescription (email.getFriendlyName ());
			device.setType (entityManager.find (ListType.class, Long.valueOf (email.getTypeId ())).getName ());
			devices.add (device);
		}
	}

	/**
	 * Checks if the primary email is in the device preferences
	 * @param preferences the user contact prefernces
	 * @return the rank in the preferences or -1 if not found
	 */
	private int getPrimaryEmailPriority (final Collection<ContactPreference> preferences) {
		int index = -1;
		for (ContactPreference preference : preferences) {
			if (preference.getPrimaryEmail ()) {
				index = preference.getIndex () + 1;
			}
		}
		return index;
	}

}
