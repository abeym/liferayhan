package org.cdph.han.facade.impl.alerts;

import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.alerts.beans.TopicsBean;
import org.cdph.han.alerts.services.*;
import org.cdph.han.persistence.AlertLocale;
import org.cdph.han.persistence.AlertTopic;
import org.cdph.han.persistence.Organization;
import org.cdph.han.util.dto.ListTypeTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.service.OrganizationLocalServiceUtil;

import org.cdph.han.service.AlertTopicLocalServiceUtil;
/**
 * Implementation of the topic service, the implementation uses JPA to manipulate the data in the HAN DB.
 * @author Abey Mani
 * @version 1.0
 */
@Repository
@Service ("topicsService")
public class TopicsServiceImpl implements TopicsService {
	/** Entity manager */
	private EntityManager entityManager;
	private static final Log log = LogFactory.getLog (TopicsServiceImpl.class);
	@Autowired
	private ModelMapper modelMapper;

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.TopicsService#insertNewTopic(long, java.lang.String)
	 */
	//abey ...@Transactional
	public long insertNewTopic (final int localeId, final String topic) {
		final AlertTopic alertTopic = new AlertTopic ();
		alertTopic.setTitle (topic);
		alertTopic.setLocale (entityManager.find (AlertLocale.class, localeId));
		entityManager.persist (alertTopic);
		return alertTopic.getId ();
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.TopicsService#listAvailableTopics(long)
	 */
	//abey ...@Transactional (readOnly = true)
	@SuppressWarnings ("unchecked")
	public List<AlertTopic> listAvailableTopics (final int localeId) {
		
		/*
		 * 		List<com.liferay.portal.model.Organization> organizations;
		List<com.liferay.portal.model.Organization> orgFilt = new ArrayList<com.liferay.portal.model.Organization>();
		List<Organization> organizationTypes = new ArrayList<Organization>();
		try {
			organizations = OrganizationLocalServiceUtil.getOrganizations(QueryUtil.ALL_POS, QueryUtil.ALL_POS); 
			for(com.liferay.portal.model.Organization org:organizations){
				if (org.getParentOrganizationId() == 0) {
					orgFilt.add(org);
				}
			}
			modelMapper.map(orgFilt, organizationTypes);
			if(organizationTypes.isEmpty())organizationTypes=loadOrgTypes(orgFilt);
		} catch (SystemException e) {
			log.error(e);
		}
		return organizationTypes;
		 */
		List<AlertTopic> hats = new ArrayList<AlertTopic>();
		try {
			List<org.cdph.han.model.AlertTopic> lats = AlertTopicLocalServiceUtil.getAlertTopics(QueryUtil.ALL_POS, QueryUtil.ALL_POS);
			modelMapper = modelMapper==null?new ModelMapper():modelMapper;
			for(org.cdph.han.model.AlertTopic lat:lats){
				AlertTopic at = new AlertTopic();
				modelMapper.map(lat, at);
				hats.add(at);
			}
		} catch (SystemException e) {
			log.error(e);
		}
		/*final Query q = entityManager.createNamedQuery ("AlertTopic.findByLocaleId");
		q.setParameter ("id", localeId);*/
		//return (List<AlertTopic>) q.getResultList ();
		return hats;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.TopicsService#checkAvailableTopic(long, java.lang.String)
	 */
	public boolean checkAvailableTopic (final int localeId, final String topic) {
		boolean available = false;
		final Query q = entityManager.createNamedQuery ("AlertTopic.findByTL");
		q.setParameter ("id", localeId);
		q.setParameter ("title", topic.toUpperCase ());
		final List<?> result = q.getResultList ();
		available = result == null || result.isEmpty ();
		return available;
	}

	/**
	 * @param entityManager the entityManager to set
	 */
	//abey...
	//@PersistenceContext
	public void setEntityManager (final EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
