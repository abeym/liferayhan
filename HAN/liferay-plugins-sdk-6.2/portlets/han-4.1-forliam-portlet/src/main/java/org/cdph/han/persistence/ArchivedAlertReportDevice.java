package org.cdph.han.persistence;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity class represneting a device in the alert report
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Entity
@Table (name = "alert_reports_devices_archive", schema = "lportal")
public class ArchivedAlertReportDevice implements Serializable, AlertReportDeviceModel {
	/** Class serial version ID */
	private static final long serialVersionUID = 8177083444507819657L;
	/** ID of the device record */
	@Id
	@Column (name = "id")
	@GeneratedValue (strategy = GenerationType.TABLE)
	private long id;
	/** The recipient to wich the devices belong */
	@ManyToOne
	@JoinColumn (name = "alert_report_recipient_id", nullable = false)
	private ArchivedAlertReportRecipient recipient;
	/** Email address or phone number */
	@Column (name = "address", length = 100)
	private String address;
	/** Result of the contact */
	@Column (name = "result", length = 20)
	private String result;
	/** Time the notification was sent */
	@Column (name = "time_sent")
	private Date timeSent;
	/** Time of the response, if any */
	@Column (name = "time_responded")
	private Date timeResponded;
	/** Duration of the cycle */
	@Column (name = "duration")
	private int duration;
	/** Description of the device */
	@Column (name = "description", length = 100)
	private String description;
	/** Contact Id */
	@Column (name = "contact_id")
	private int contactId;
	/** ID of the response if any */
	@Column (name = "response")
	private Integer response;

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode ());
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode ());
		result = prime * result + duration;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result
				+ ((recipient == null) ? 0 : recipient.hashCode ());
		result = prime * result
				+ ((this.result == null) ? 0 : this.result.hashCode ());
		result = prime * result
				+ ((timeResponded == null) ? 0 : timeResponded.hashCode ());
		result = prime * result
				+ ((timeSent == null) ? 0 : timeSent.hashCode ());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean equal = false;
		if (obj != null && obj instanceof ArchivedAlertReportDevice) {
			final ArchivedAlertReportDevice other = (ArchivedAlertReportDevice) obj;
			equal = other.getId () == id;
		}
		return equal;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertReportDeviceModel#getId()
	 */
	public long getId () {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId (long id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertReportDeviceModel#getRecipient()
	 */
	public ArchivedAlertReportRecipient getRecipient () {
		return recipient;
	}

	/**
	 * @param recipient the recipient to set
	 */
	public void setRecipient (ArchivedAlertReportRecipient recipient) {
		this.recipient = recipient;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertReportDeviceModel#getAddress()
	 */
	public String getAddress () {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress (String address) {
		this.address = address;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertReportDeviceModel#getResult()
	 */
	public String getResult () {
		return result;
	}

	/**
	 * @param result the result to set
	 */
	public void setResult (String result) {
		this.result = result;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertReportDeviceModel#getTimeSent()
	 */
	public Date getTimeSent () {
		return timeSent;
	}

	/**
	 * @param timeSent the timeSent to set
	 */
	public void setTimeSent (Date timeSent) {
		this.timeSent = timeSent;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertReportDeviceModel#getTimeResponded()
	 */
	public Date getTimeResponded () {
		return timeResponded;
	}

	/**
	 * @param timeResponded the timeResponded to set
	 */
	public void setTimeResponded (Date timeResponded) {
		this.timeResponded = timeResponded;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertReportDeviceModel#getDuration()
	 */
	public int getDuration () {
		return duration;
	}

	/**
	 * @param duration the duration to set
	 */
	public void setDuration (int duration) {
		this.duration = duration;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertReportDeviceModel#getDescription()
	 */
	public String getDescription () {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription (String description) {
		this.description = description;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertReportDeviceModel#getContactId()
	 */
	public int getContactId () {
		return contactId;
	}

	/**
	 * @param contactId the contactId to set
	 */
	public void setContactId (int contactId) {
		this.contactId = contactId;
	}

	/**
	 * @return the response
	 */
	public Integer getResponse () {
		return response;
	}

	/**
	 * @param response the response to set
	 */
	public void setResponse (Integer response) {
		this.response = response;
	}

}
