package org.cdph.han.util.services;

import com.liferay.portal.kernel.bean.PortalBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;

import java.util.ArrayList;
import java.util.List;

import org.cdph.han.persistence.ListType;
import org.cdph.han.util.dto.ListTypeTO;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

/**
 * Implementation of the ListTypeService, this implementation uses JPA to retrieve the data
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Repository
@Service ("listTypeService")
public class ListTypeServiceImpl implements ListTypeService {

	/* (non-Javadoc)
	 * @see org.cdph.han.util.services.ListTypeService#loadListForType(java.lang.String)
	 */
	@Override
	public ListTypeTO[] loadListForType (final String type) {
		final List<ListTypeTO> foundValues = new ArrayList<ListTypeTO> ();
		SessionFactory factory =  (SessionFactory)PortalBeanLocatorUtil.locate("liferaySessionFactory");
		Session session = null;
		try {
			session = factory.openSession();
			String sql = "select * from ListType l where l.type_ like ?";
			System.out.println("Run>  " + sql);
			SQLQuery q = session.createSQLQuery(sql);
			q.setCacheable(false);
			q.addEntity("ListType", ListType.class);

			QueryPos qPos = QueryPos.getInstance(q);
			qPos.add(type);
			System.out.println("Query Pos>  " + qPos.getPos());
			List<ListType> list = (List<ListType>) QueryUtil.list(q, factory.getDialect(), 1, QueryUtil.ALL_POS);
			for (ListType listType : list) {
				foundValues.add (new ListTypeTO (listType.getId (), listType.getName (), listType.getType ()));
			}
		} catch (Exception e) {
			System.err.println("ERROR Class not found: " + e);
		} finally {
			if (session != null) factory.closeSession(session);
		}
		return foundValues.toArray (new ListTypeTO[foundValues.size ()]);
	}

}
