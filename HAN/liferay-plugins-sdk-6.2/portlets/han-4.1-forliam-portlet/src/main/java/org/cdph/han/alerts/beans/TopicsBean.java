package org.cdph.han.alerts.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.alerts.dto.FileTO;
import org.cdph.han.alerts.services.MessagesService;
import org.cdph.han.alerts.services.TopicsService;
import org.cdph.han.facade.impl.alerts.MessagesServiceImpl;
import org.cdph.han.facade.impl.alerts.TopicsServiceImpl;
import org.cdph.han.facade.impl.alerts.UserDataServiceImpl;
import org.cdph.han.facade.impl.email.EmailSenderServiceImpl;
//import org.cdph.han.alerts.services.UserDataService;
import org.cdph.han.persistence.AlertTopic;
import org.cdph.han.persistence.UserData;
import org.cdph.han.util.dto.ListTypeTO;

import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.mir3.ws.*;

/**
 * Baked bean used to retrieve all topic information located in mir3
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class TopicsBean implements Serializable {
	/** Class serial version UID */
	private static final long serialVersionUID = 7986776892774251957L;
	/** Class logger */
	private static final Log log = LogFactory.getLog (TopicsBean.class);
	/** Web service end point reference to mir3 */
	public Mir3 service;
	/** Service for retrieving user data */
	private UserDataServiceImpl userDataService;
	/** The categories configured at Mir3 */
	private SelectItem[] categories;
	/** The priorities configured at Mir3 */
	private SelectItem[] priorities;
	/** The severities configured at Mir3 */
	private SelectItem[] severities;
	/** Service to retrieve all topics from the HAN DB */
	private TopicsService topicsService;
	/** Topic title */
	private String topicTitle;
	/** Flag for showing or hiding the add topic window */
	private boolean addTopic;
	/** Messages service */
	private MessagesService messagesService;
	/** Available topics */
	private SelectItem[] topics;

	/**
	 * Creates all the necessary objects after querying the API.
	 */
	private void queryWebService () {
		log.info ("Querying webservices.(not an error)");
		try {
			log.info ("QueryWebService call emailSenderService");
			long[] uid=new long[]{14143l};
			long[] dunno=new long[]{14143l};
			String content="";
			String subject="";
			List<FileTO> attachments = null;
			EmailSenderServiceImpl emailSenderService = new EmailSenderServiceImpl();
			log.debug("QueryWebService emailSenderService.sendAdminEmail: uid[0]: "+uid[0]);
			emailSenderService.sendAdminEmail(uid, dunno, content, subject, attachments);
			log.debug("QueryWebService completed emailSenderService.sendAdminEmail: uid[0]: "+uid[0]);
			
			
			
			if (service == null) {
				service = new Mir3Service ().getMir3 ();
			}
			GetAvailableTopicsType getAvailableTopics = new GetAvailableTopicsType ();
			getAvailableTopics.setApiVersion ("2.14");
			AuthorizationType authorization = new AuthorizationType ();
			userDataService = userDataService == null?new UserDataServiceImpl():userDataService;
			//UserData userData = userDataService.getCurrentUserData ();
			UserData userData = userDataService.getUser(19659);
			User u = UserLocalServiceUtil.getUser(19659);
			
			//	han-_ad-min haTp@ssw0rd
			// mir3.api.account@domianurl.youknow
			
			//Liam_Zanoni@exchange.compaid.com
			//17;@ffXW
			authorization.setUsername ("Liam_Zanoni@exchange.compaid.com");//userData.getEmail ()
			authorization.setPassword ("17;@ffXW");//userData.getTelephonyPassword ()
			
			
			
			
			getAvailableTopics.setAuthorization (authorization);
			log.info ("service.getAvailableTopicsOp getAvailableTopicsType: "+getAvailableTopics);
			ResponseType response = service.getAvailableTopicsOp (getAvailableTopics);
			if (response.getError ().isEmpty ()) {
				GetAvailableTopicsResponseType availableResponse = response.getGetAvailableTopicsResponse ();
				CategoriesType categories = availableResponse.getCategories ();
				PrioritiesType priorities = availableResponse.getPriorities ();
				SeveritiesType severities = availableResponse.getSeverities ();
				List<SelectItem> catList = new ArrayList<SelectItem> ();
				List<SelectItem> prioList = new ArrayList<SelectItem> ();
				List<SelectItem> sevList = new ArrayList<SelectItem> ();
				String parentCategory;
				for (CategoryType category : categories.getCategory ()) {
					parentCategory = category.getDivision () + category.getDescription ();
					addSubCategories (catList, parentCategory, category.getSubcategories ());
				}
				for (TopicType priority : priorities.getPriority ()) {
					prioList.add (new SelectItem (priority.getDivision () + priority.getDescription (),
							priority.getDescription ()));
				}
				for (TopicType severity : severities.getSeverity ()) {
					sevList.add (new SelectItem (severity.getDivision () + severity.getDescription (),
							severity.getDivision () + severity.getDescription ()));
				}
				this.categories = catList.toArray (new SelectItem[catList.size ()]);
				this.priorities = prioList.toArray (new SelectItem[prioList.size ()]);
				this.severities = sevList.toArray (new SelectItem[sevList.size ()]);
			} else {
				FacesContext.getCurrentInstance ().addMessage (null,
						messagesService.getMessage ("alerts.mir3.categories.error",
								"Error retrieving MIR3 categories, try again later.", FacesMessage.SEVERITY_ERROR));
				for (ErrorType error : response.getError ()) {
					log.error ("Got error: " + error.getErrorCode () + " Message: " + error.getErrorMessage ());
				}
			}
		} catch (Exception ex) {
			messagesService = messagesService==null?new MessagesServiceImpl():messagesService;
			FacesContext.getCurrentInstance ().addMessage (null,
					messagesService.getMessage ("alerts.mir3.categories.error",
							"Error retrieving MIR3 categories, try again later.", FacesMessage.SEVERITY_ERROR));
			//TODO: hard coding untill we conect to MIR3
			log.error ("Error retrieving mir3 preferences.", ex);
			
			this.categories = new ArrayList<ListTypeTO> ().toArray (new SelectItem[0]);
			List<SelectItem> eprioList = new ArrayList<SelectItem> ();
			eprioList.add (new SelectItem ("mir3" + "is not available yet",
					"is not available yet"));
			this.priorities = eprioList.toArray (new SelectItem[eprioList.size ()]);
			//this.priorities = new ArrayList<ListTypeTO> ().toArray (new SelectItem[0]);
			
			List<SelectItem> esevList = new ArrayList<SelectItem> ();
			esevList.add (new SelectItem ("mir3" + "is not available yet",
					"mir3" + "is not available yet"));
			this.severities = esevList.toArray (new SelectItem[esevList.size ()]);
			//this.severities = new ArrayList<ListTypeTO> ().toArray (new SelectItem[0]);
		}
	}

	/**
	 * Creates all the select items for the subcategories
	 * @param catList List of SelectItem
	 * @param parentCategory name of the parent category
	 * @param subCategories list of subcategories
	 */
	private void addSubCategories (final List<SelectItem> catList, final String parentCategory,
			final SubcategoriesType subCategories) {
		for (String subcategory : subCategories.getSubcategory ()) {
			catList.add (new SelectItem (parentCategory + ":" + subcategory,
					parentCategory + ":" + subcategory));
		}
	}

	/**
	 * Action listener to show the add topic pop up
	 * @param event action event
	 */
	public void toggleAddTopic (final ActionEvent event) {
		topicTitle = "";
		addTopic = !addTopic;
	}

	/**
	 * Action listener to add a new topic and hide the new topic window.
	 * @param event
	 */
	@SuppressWarnings ("unchecked")
	public void addNewTopic (final ActionEvent event) {
		addTopic = false;
		try {
			final boolean available = topicsService.checkAvailableTopic (1, topicTitle);
			if (available) {
				final long topicId = topicsService.insertNewTopic (1, topicTitle);
				topics = null;
				FacesContext.getCurrentInstance ().getExternalContext ().getSessionMap ().put ("topicId", topicId);
				FacesContext.getCurrentInstance ().addMessage (null,
						messagesService.getMessage ("alerts.compose.notification.topic.insert.success",
								"The topic was added successfully.",
								FacesMessage.SEVERITY_INFO));
			} else {
				FacesContext.getCurrentInstance ().addMessage (null,
						messagesService.getMessage ("alerts.compose.notification.topic.not.available",
								"The topic you tried to add is already registered in the database.",
								FacesMessage.SEVERITY_ERROR));
			}
		} catch (Exception ex) {
			log.error ("Error adding new topic.", ex);
			FacesContext.getCurrentInstance ().addMessage ("topic",
					messagesService.getMessage ("alerts.compose.notification.topic.error",
							"Error adding new topic, try again later.", FacesMessage.SEVERITY_ERROR));
		}
	}

	/**
	 * @param userDataService the userDataService to set
	 */
	public void setUserDataService (UserDataServiceImpl userDataService) {
		this.userDataService = userDataService;
	}

	/**
	 * @return the categories
	 */
	public SelectItem[] getCategories () {
		if (categories == null) {
			queryWebService ();
		}
		return categories;
	}

	/**
	 * @return the priorities
	 */
	public SelectItem[] getPriorities () {
		if (priorities == null) {
			queryWebService ();
		}
		return priorities;
	}

	/**
	 * @return the severities
	 */
	public SelectItem[] getSeverities () {
		if (severities == null) {
			queryWebService ();
		}
		return severities;
	}

	/**
	 * @return the topics
	 */
	public SelectItem[] getTopics () {
		if (topics == null) {
			final List<SelectItem> topics = new ArrayList<SelectItem> ();
			topicsService = topicsService==null?new TopicsServiceImpl():topicsService;
			final List<AlertTopic> dbTopics = topicsService.listAvailableTopics (1);
			for (AlertTopic topic : dbTopics) {
				topics.add (new SelectItem (topic.getId (), topic.getTitle ()));
			}
			this.topics = topics.toArray (new SelectItem[topics.size ()]);
		}
		return topics;
	}

	/**
	 * @param topicsService the topicsService to set
	 */
	public void setTopicsService (TopicsService topicsService) {
		this.topicsService = topicsService;
	}

	/**
	 * @return the topicTitle
	 */
	public String getTopicTitle () {
		return topicTitle;
	}

	/**
	 * @param topicTitle the topicTitle to set
	 */
	public void setTopicTitle (String topicTitle) {
		this.topicTitle = topicTitle;
	}

	/**
	 * @return the addTopic
	 */
	public boolean isAddTopic () {
		return addTopic;
	}

	/**
	 * @param addTopic the addTopic to set
	 */
	public void setAddTopic (boolean addTopic) {
		this.addTopic = addTopic;
	}

	/**
	 * @param messagesService the messagesService to set
	 */
	public void setMessagesService (MessagesService messagesService) {
		this.messagesService = messagesService;
	}

}