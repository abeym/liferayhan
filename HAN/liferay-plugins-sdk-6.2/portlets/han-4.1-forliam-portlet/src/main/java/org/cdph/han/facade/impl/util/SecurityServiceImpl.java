package org.cdph.han.facade.impl.util;

import com.liferay.faces.portal.context.LiferayFacesContext;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.service.RoleLocalServiceUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.util.services.SecurityService;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of the SecurityService that uses JSF to retrieve the user
 * profiles
 * 
 * @author Abey Mani
 * @version 1.0
 */
@Repository
@Service("securityService")
public class SecurityServiceImpl implements SecurityService {
	/** Name of the administrator role */
	private static final String ADMINISTRATOR_ROLE = "Administrator";
	/** Name of the super user role */
	private static final String SUPER_USER_ROLE = "Super User";
	/** Class logger */
	private static Log log = LogFactory.getLog(SecurityServiceImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cdph.han.util.services.SecurityService#isUserAdmin()
	 */
	//abey...@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public boolean isUserAdmin() {
		boolean admin = false;
		try {
			// final String userIdString = FacesContext.getCurrentInstance
			// ().getExternalContext ().getUserPrincipal ().getName ();
			User currentUser = LiferayFacesContext.getInstance().getUser();
			if (currentUser != null) {
				for (Role role : RoleLocalServiceUtil.getUserRoles(currentUser
						.getUserId())) {
					if (role.getName().equals(ADMINISTRATOR_ROLE)
							|| role.getName().equals(SUPER_USER_ROLE)
							|| role.getName().equals("HAN Alert Initiator")) {
						admin = true;
						break;
					}
				}
			}
		} catch (SystemException e) {
			log.error(e);
		}
		return admin;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cdph.han.util.services.SecurityService#getMir3UserRole(long)
	 */
	@Override
	public String getMir3UserRole(long userId) {
		String mir3Role;
		if (isUserAdmin()) {
			mir3Role = "Initiator";
		} else {
			mir3Role = "Recipient";
		}
		return mir3Role;
	}

}
