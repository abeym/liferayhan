/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.cdph.han.service.impl;

import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.portlet.PortletClassLoaderUtil;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.model.CustomGroupMember;
import org.cdph.han.model.impl.CustomGroupMemberImpl;
import org.cdph.han.service.CustomGroupMemberLocalServiceUtil;
import org.cdph.han.service.base.CustomGroupMemberLocalServiceBaseImpl;

/**
 * The implementation of the custom group member local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link org.cdph.han.service.CustomGroupMemberLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author zanonil
 * @see org.cdph.han.service.base.CustomGroupMemberLocalServiceBaseImpl
 * @see org.cdph.han.service.CustomGroupMemberLocalServiceUtil
 */
public class CustomGroupMemberLocalServiceImpl
	extends CustomGroupMemberLocalServiceBaseImpl {
	Log log = LogFactory.getLog(CustomGroupMemberLocalServiceImpl.class);

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link org.cdph.han.service.CustomGroupMemberLocalServiceUtil} to access the custom group member local service.
	 */
	public CustomGroupMember add(){
		try {
			long groupId = CounterLocalServiceUtil.increment();
			CustomGroupMember custGroupMember = new CustomGroupMemberImpl();
			custGroupMember.setPrimaryKey(groupId);
			this.addCustomGroupMember(custGroupMember);
			return custGroupMember;
		} catch (SystemException se) {
			return null;
		}
	}

	public List<CustomGroupMember> findByGroupId(long groupId){
		try {
		    DynamicQuery query = DynamicQueryFactoryUtil.forClass(
		    		CustomGroupMember.class, PortletClassLoaderUtil.getClassLoader())
					.add(PropertyFactoryUtil.forName("userCustomGroup").eq(groupId));
		    List<CustomGroupMember> custGroupMemberList = CustomGroupMemberLocalServiceUtil.dynamicQuery(query);
		    log.debug("Customer Group Members " + custGroupMemberList);
		    return custGroupMemberList;
		} catch (SystemException se) {
			return null;
		}
	}

	public List<CustomGroupMember> findByChildGroupId(long groupId){
		try {
		    DynamicQuery query = DynamicQueryFactoryUtil.forClass(
		    		CustomGroupMember.class, PortletClassLoaderUtil.getClassLoader())
					.add(PropertyFactoryUtil.forName("childGroupId").eq(groupId));
		    List<CustomGroupMember> custGroupMemberList = CustomGroupMemberLocalServiceUtil.dynamicQuery(query);
		    log.debug("Child Group Members " + custGroupMemberList);
		    return custGroupMemberList;
		} catch (SystemException se) {
			return null;
		}
	}
}