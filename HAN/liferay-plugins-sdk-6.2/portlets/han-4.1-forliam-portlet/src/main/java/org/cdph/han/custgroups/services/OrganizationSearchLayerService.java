package org.cdph.han.custgroups.services;

import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.bean.PortalBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.Order;
import com.liferay.portal.kernel.dao.orm.OrderFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.portlet.PortletClassLoaderUtil;
import com.liferay.portal.kernel.util.InfrastructureUtil;
import com.liferay.portal.model.Address;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.custgroups.dto.GroupDisplayTO;
import org.cdph.han.custgroups.dto.KeyDescriptionTO;
import org.cdph.han.custgroups.dto.UserDisplayTO;
import org.cdph.han.facade.impl.custgroups.OrganizationalSearchServiceImpl;
import org.cdph.han.model.AreaField;
import org.cdph.han.model.CustomGroupCriteria;
import org.cdph.han.model.CustomGroupMember;
import org.cdph.han.model.NonHanUser;
import org.cdph.han.model.TypeOrganizationRoles;
import org.cdph.han.persistence.BatchStatus;
import org.cdph.han.persistence.UserCustomGroup;
import org.cdph.han.service.AreaFieldLocalServiceUtil;
import org.cdph.han.service.CustomGroupCriteriaLocalServiceUtil;
import org.cdph.han.service.CustomGroupMemberLocalServiceUtil;
import org.cdph.han.service.NonHanUserLocalServiceUtil;
import org.cdph.han.service.TypeOrganizationRolesLocalServiceUtil;
import org.cdph.han.service.UserCustomGroupLocalServiceUtil;

public class OrganizationSearchLayerService implements OrganizationalSearchService {
	private static final Log log = LogFactory.getLog (OrganizationSearchLayerService.class);

	public KeyDescriptionTO[] getOrganizationTypes () {
		KeyDescriptionTO keyDescription;
		KeyDescriptionTO[] keyDescriptions = null;
		List<Organization> orgList = getParentOrganizations ();
		keyDescriptions = new KeyDescriptionTO[orgList.size()];
		log.info("Load " + orgList.size() + " Organization Types");
		int i = 0;
		for (Organization org : orgList) {
			keyDescription = new KeyDescriptionTO ();
			keyDescription.setDescription (org.getName ());
			keyDescription.setKey (org.getOrganizationId ());
			//log.info("Load Organization "+ org.getName() + " (" + org.getOrganizationId() + ")");
			keyDescriptions[i++] = keyDescription;
		}
		return keyDescriptions;
	}

	public KeyDescriptionTO[] getOrganizations (final long orgTypeId, final boolean onlyChicagoEntities) {
		final List<KeyDescriptionTO> organizations = new ArrayList<KeyDescriptionTO> ();
		Organization org = null;
		try {
			org = OrganizationLocalServiceUtil.getOrganization(orgTypeId);
		} catch (SystemException se) {
		} catch (PortalException se) {}
		KeyDescriptionTO keyDescription;
		if (onlyChicagoEntities) {
			List<Address> addresses = getAddresses(14017L, org.getOrganizationId(), "CHICAGO");
			if (addresses != null) {
				keyDescription = new KeyDescriptionTO ();
				keyDescription.setDescription (org.getName ());
				keyDescription.setKey (org.getOrganizationId ());
				organizations.add (keyDescription);
			}
		} else {
			keyDescription = new KeyDescriptionTO ();
			keyDescription.setDescription (org.getName ());
			keyDescription.setKey (org.getOrganizationId ());
			organizations.add (keyDescription);
		}
		return organizations.toArray (new KeyDescriptionTO[organizations.size ()]);
	}

	private List<Organization> getParentOrganizations () {
		List<Organization> orgList = null;
		try {
			//ClassLoader classLoader = 
				//	(ClassLoader)PortletBeanLocatorUtil.locate(ClpSerializer.SERVLET_CONTEXT_NAME,"portletClassLoader");
			
		    DynamicQuery organizationQuery = DynamicQueryFactoryUtil.forClass(
		    		Organization.class, PortletClassLoaderUtil.getClassLoader())
		    		.add(PropertyFactoryUtil.forName("parentOrganizationId").eq(0l));
			Order order = OrderFactoryUtil.desc("name");
			organizationQuery.addOrder(order);
			orgList = OrganizationLocalServiceUtil.dynamicQuery(organizationQuery);
			System.out.println("Retrieved " + orgList.size () + " organizations.");
		} catch (SystemException e) {
			log.error(e);
		} catch (Exception e) {
			log.error(e);
		}
		return orgList;
	}

	private List<Address> getAddresses (long classNameId, long classPk, String likeCity) {
		List<Address> retval = null;
		SessionFactory factory =  (SessionFactory)PortalBeanLocatorUtil.locate("liferaySessionFactory");
		Session session = null;
		try {
			System.out.println("classNameId "+ classNameId + ", classPk " + classPk + ", like " + likeCity);
			session = factory.openSession();
			String sql = "select * from address a where a.classNameId = ? and a.classPK = ? and upper (a.city) like ?";
			System.out.println("Run>  " + sql);
			SQLQuery q = session.createSQLQuery(sql);
			q.setCacheable(false);
			q.addEntity("address", Address.class);

			System.out.println("Query>  " + q);
			QueryPos qPos = QueryPos.getInstance(q);
			qPos.add(classNameId);
			qPos.add(classPk);
			qPos.add(likeCity);
			System.out.println("Query Pos>  " + qPos.getPos());
			retval = (List<Address>) QueryUtil.list(q, factory.getDialect(), 1, QueryUtil.ALL_POS);
		} catch (Exception e) {
			System.err.println("ERROR Class not found: " + e);
		} finally {
			if (session != null) factory.closeSession(session);
		}
		return retval;
	}

	public KeyDescriptionTO[] getAreaFields (final long orgTypeId) {
		KeyDescriptionTO[] keyDescriptions = null;
		try {
			Organization org = OrganizationLocalServiceUtil.getOrganization(orgTypeId);
//		    DynamicQuery areaFieldQuery = DynamicQueryFactoryUtil.forClass(
//		    		AreaField.class, PortletClassLoaderUtil.getClassLoader())
//		    		.add(PropertyFactoryUtil.forName("organization").eq(org.getOrganizationId()));
			List<AreaField> areaFieldList = AreaFieldLocalServiceUtil.getAreaFieldsForOrgType(orgTypeId);
			keyDescriptions = new KeyDescriptionTO[areaFieldList.size()];
			int i = 0;
			for (AreaField areaField : areaFieldList) {
				keyDescriptions[i] = new KeyDescriptionTO ();
				keyDescriptions[i].setDescription (areaField.getDescription());
				keyDescriptions[i++].setKey (areaField.getAreaFieldId());
			}
		} catch (SystemException se) {
		} catch (PortalException se) {}
		return keyDescriptions;
	}

	public KeyDescriptionTO[] getRoles (final long orgTypeId) {
		log.debug("Roles orgTypeId " + orgTypeId);
		Organization org = null;
		KeyDescriptionTO[] keyDescriptions = null;
		try {
			org = OrganizationLocalServiceUtil.getOrganization(orgTypeId);
			log.debug("Organization " + org.getName());
			List<TypeOrganizationRoles> orgRoleList = TypeOrganizationRolesLocalServiceUtil.findByOrganizationId(orgTypeId);
			if (orgRoleList == null) return new KeyDescriptionTO[0];
			keyDescriptions = new KeyDescriptionTO[orgRoleList.size()];
			int i = 0;
			for (TypeOrganizationRoles typeOrgRole : orgRoleList) {
				Role role = RoleLocalServiceUtil.getRole(typeOrgRole.getRoleId());
				log.debug("Add Role " + typeOrgRole.getRoleId() + " / " + role.getName());
				keyDescriptions[i] = new KeyDescriptionTO ();
				keyDescriptions[i].setDescription (role.getName ());
				keyDescriptions[i++].setKey (role.getRoleId ());
			}
		} catch (SystemException se) {
		} catch (PortalException se) {}
		return keyDescriptions;
	}

	public List<UserDisplayTO> getUsersInOrganization (long organizationId) {
		List<UserDisplayTO> users = new ArrayList<UserDisplayTO>();
		log.debug("Org Type Id: " + organizationId);
		try {
			Organization org = OrganizationLocalServiceUtil.getOrganization(organizationId);
			log.debug("Org : " + org.getOrganizationId() + ", " + org.getName());
			//TODO: Could not get passed the method wouldn't generate
	/*		List<UserOrganizationalData> userOrganizationalDataList = 
					UserOrganizationalDataLocalServiceUtil.custFindByOrganizationId(organizationId);
			log.debug("userOrganizationalDataList : " + userOrganizationalDataList);
			if (userOrganizationalDataList != null && userOrganizationalDataList.size() > 0) {
				for (UserOrganizationalData userOrganizationalData : userOrganizationalDataList) {
				}
			}
//	Presently, there is no data in this table 
//			for (UserOrganizationalData orgData : userOrganizationalDataList) {
//				users.add(null);
//			}*/

		} catch (SystemException se) {
		} catch (PortalException se) {
		} catch (Exception e) {log.error(e);}
		return users;
	}

	public UserDisplayTO[] searchUsersInBulkUpload (final List<Long> listIds) {
		StringBuilder ids = new StringBuilder ();
		for (long id : listIds) {
			ids.append (id).append (',');
		}
		List<UserDisplayTO> foundContacts = new ArrayList<UserDisplayTO> ();
		log.debug("List Ids: " + listIds);
		try {
			String[] gIds = new String[listIds.size()];
			for (int i = 0; i < listIds.size(); i++) gIds[i] = String.valueOf(listIds.get(i));
			List<NonHanUser> nonHanUsers = NonHanUserLocalServiceUtil.findByGroupIds(gIds);
			if (nonHanUsers != null) {
				for (NonHanUser nonHanUser : nonHanUsers) {
					foundContacts.add (new UserDisplayTO (nonHanUser.getId_(),
							nonHanUser.getFirstName (), nonHanUser.getLastName (), "", "", true));
				}
			}
		} catch (Exception e) {
			System.err.println("ERROR Class not found: " + e);
		}
		return foundContacts.toArray(new UserDisplayTO[foundContacts.size()]);
	}

	public UserDisplayTO[] searchUsersInGroup (final List<Long> groupIds) {
		log.debug("groupIds: " + groupIds);
		if (groupIds.size() < 1)
			return new UserDisplayTO[0];
		UserCustomGroup group;
		List<Long> userIdList = new ArrayList<Long>();
		StringBuilder userIds = new StringBuilder ();
		StringBuilder organizationIds = new StringBuilder ();
		StringBuilder areaFieldIds = new StringBuilder ();
		StringBuilder roleIds = new StringBuilder ();
		StringBuilder nonHanUserIds = new StringBuilder ();
		StringBuilder nonHanGroupIds = new StringBuilder ();
		Set<UserDisplayTO> foundUsers = new TreeSet<UserDisplayTO> ();
		long[] groupIdArray = new long[groupIds.size()];
		for (int i = 0; i<groupIds.size(); i++) {
			groupIdArray[i] = groupIds.get(i);
		}
		List<org.cdph.han.model.UserCustomGroup> foundGroups = UserCustomGroupLocalServiceUtil.findByGroupIdList(groupIdArray);
		log.trace("groups: "+ foundGroups);
		if (foundGroups == null) return new UserDisplayTO[0];

		for (org.cdph.han.model.UserCustomGroup foundGroup : foundGroups) {
			log.debug("group: " + foundGroup.getDeleted() + ": "+ foundGroup.getName());
			if (!foundGroup.isDeleted ()) {
				List<CustomGroupMember> cgms = CustomGroupMemberLocalServiceUtil.findByGroupId(foundGroup.getGroupId());
				log.debug("custom group Members: " + (cgms == null ? "is null": "has " + cgms.size() + " members."));
				for (CustomGroupMember member : cgms) {
					log.debug("Custom group Member: " + member);
					if (member.getAreaFieldId() > 0) {
						if (areaFieldIds.length()> 0) areaFieldIds.append (',');
						areaFieldIds.append (member.getAreaFieldId ());
					} else if (member.getNonHanUserId() > 0) {
						if (nonHanUserIds.length()> 0) nonHanUserIds.append (',');
						nonHanUserIds.append (member.getNonHanUserId());
					} else if (member.getNonHanUserGroupId() > 0) {
						if (nonHanGroupIds.length()> 0) nonHanGroupIds.append (',');
						nonHanGroupIds.append (member.getNonHanUserGroupId ());
					} else if (member.getOrganizationId() > 0) {
						if (organizationIds.length() > 0) organizationIds.append (',');
						organizationIds.append (member.getOrganizationId());
					} else if (member.getRoleId() > 0) {
						if (roleIds.length() > 0) roleIds.append (',');
						roleIds.append (member.getRoleId());
					} else if (member.getUserId() > 0) {
						if (userIds.length() > 0) userIds.append (',');
						userIds.append (member.getUserId ());
						userIdList.add(member.getUserId ());
					} else if (member.getMemberId() > 0) {						// get Criteria
						List<CustomGroupCriteria> customGroupCriterias = CustomGroupCriteriaLocalServiceUtil.findByMemberId(member.getMemberId());
						List<Long> orgIds = new ArrayList<Long> ();
						List<Long> aFIds = new ArrayList<Long> ();
						List<Long> rIds = new ArrayList<Long> ();
						long orgTypeId = 0;
						for (CustomGroupCriteria criteria : customGroupCriterias) {
							orgTypeId = criteria.getOrganizationId ();
							if (criteria.getAreaFieldId() > 0) {
								aFIds.add(criteria.getAreaFieldId());
							}
							if (criteria.getRoleId() > 0) {
								rIds.add(criteria.getRoleId());
							}
							if (criteria.getOrganizationId() > 0) {
								orgIds.add(criteria.getOrganizationId());
							}
						}
						if (orgIds.isEmpty () && aFIds.isEmpty () && rIds.isEmpty ()) {
							foundUsers.addAll (getUsersByOrganizationType (orgTypeId));
						} else {
							foundUsers.addAll (filterUsers (convertToArray (orgIds),
									convertToArray (aFIds), convertToArray (rIds)));
						}
					}
				}

				DataSource liferayDataSource = InfrastructureUtil.getDataSource();
				try {
					Statement hanStat = liferayDataSource.getConnection().createStatement();
					String sql = buildHanAreFieldQuery(areaFieldIds, organizationIds, roleIds);
					log.debug("Run>  " + sql);
					ResultSet hanResult = hanStat.executeQuery(sql);

					while (hanResult.next()) {
						User user = UserLocalServiceUtil.getUser(hanResult.getLong("userId"));
						if (user.isActive()) {
							log.debug("Adding HAN user " + user.getUserId() + ": " + user.getLastName());
							List<Organization> orgs = user.getOrganizations();
							String orgName = (orgs != null ?  orgs.get(0).getName() : "");
							List<Role> roles = user.getRoles();
							String roleName = (roles != null ? roles.get(0).getName() : "");
							foundUsers.add (new UserDisplayTO (user.getUserId(),
									user.getFirstName (), user.getLastName (),
									orgName, roleName));
						}
					}
					hanResult.close();
					hanStat.close();

					Statement nonHanStat = liferayDataSource.getConnection().createStatement();
					sql = buildNonHanAreFieldQuery(nonHanUserIds, nonHanGroupIds);
					log.debug("Run>  " + sql);
					ResultSet nonHanResult = nonHanStat.executeQuery(sql);

					while (nonHanResult.next()) {
						String status = nonHanResult.getString("status");
						log.debug("Adding Non-HAN user (" + status + ") " + nonHanResult.getString("lastName"));
						if (status.equals (BatchStatus.FINISHED)) {
							foundUsers.add (new UserDisplayTO (nonHanResult.getLong("nhuId"), nonHanResult.getString("firstName"),
									nonHanResult.getString("lastName"), "", "", true));
						}
					}
					nonHanResult.close();
					nonHanStat.close();
				} catch (Exception e) {
					System.err.println("ERROR Class not found: " + e);
				}
			}
		}
		for (Long userId : userIdList) {
			try {
				User user = UserLocalServiceUtil.getUser(userId);
				log.debug("Add User " + userId + ": " + user.getFullName() + ", foundUsers: " + foundUsers);
				List<Organization> orgs = user.getOrganizations();
				String orgName = (orgs != null ?  orgs.get(0).getName() : "");
				List<Role> roles = user.getRoles();
				String roleName = (roles != null ? roles.get(0).getName() : "");
				foundUsers.add (new UserDisplayTO (userId, user.getFirstName(),
						user.getLastName(), orgName, roleName, true));
			} catch (PortalException e) {
				e.printStackTrace();
			} catch (SystemException e) {
				e.printStackTrace();
			}
		}

//		for (int i = 0; i < groupIds.size(); i++) {
//			List<CustomGroupMember> cgms = CustomGroupMemberLocalServiceUtil.findByGroupId(groupIds.get(i));
//			if (cgms == null || cgms.size() < 1) {
//				UserDisplayTO thisUserDisplay =  new UserDisplayTO();
//				thisUserDisplay.setFirstName(groupIds.get(i).toString());
//				thisUserDisplay.setLastName(groupIds.get(i).toString());
//				thisUserDisplay.setId(groupIds.get(i));
//				userDisplayTOs.add(thisUserDisplay);
//			} else {
//				for (CustomGroupMember cgm : cgms) {
//					log.debug("CustomGroupMember: " + cgm);
//					UserDisplayTO thisUserDisplay =  new UserDisplayTO();
//					User user = null;
//					try {
//						if (cgm.getUserId() > 0) {
//							user = UserLocalServiceUtil.getUser(cgm.getUserId());
//						}
//						if (user != null) {
//							thisUserDisplay.setFirstName(user.getFirstName());
//							thisUserDisplay.setLastName(user.getLastName());
//							thisUserDisplay.setNotHanUser(false);
//							thisUserDisplay.setId(user.getUserId());
//						} else {
//							thisUserDisplay.setFirstName(groupIds.get(i).toString());
//							thisUserDisplay.setLastName(groupIds.get(i).toString());
//							thisUserDisplay.setId(groupIds.get(i));
//							thisUserDisplay.setNotHanUser(true);
//						}
//						userDisplayTOs.add(thisUserDisplay);
//					} catch (PortalException e) {
//						e.printStackTrace();
//					} catch (SystemException e) {
//						e.printStackTrace();
//					}
//				}
//			}
//		}
		return foundUsers.toArray(new UserDisplayTO[0]);
	}

	private String buildHanAreFieldQuery(StringBuilder areaFieldIds, StringBuilder organizationIds, StringBuilder roleIds) {
		String sql = "select userId, roleId organizationId, area_field_id "
				+ "  from users_areafields where 1=1";
		if (areaFieldIds.toString().length() > 0) {
			sql += " and area_field_id in (" + areaFieldIds.toString() + ")";
			log.trace("Query add areaFieldIds>  " + areaFieldIds.toString() + ".");
		}
		if (organizationIds.toString().length() > 0) {
			sql += " and organizationId in (" + organizationIds.toString() + ")";
			log.trace("Query add organizationIdss>  " + organizationIds.toString() + ".");
		}
		if (roleIds.toString().length() > 0) {
			sql += " and roleId in (" + roleIds.toString() + ")";
			log.trace("Query add roleIds>  " + roleIds.toString() + ".");
		}
		if (areaFieldIds.toString().length() < 1 && organizationIds.toString().length() < 1
				&& roleIds.toString().length() < 1) {
			sql += " and 1=2";
		}
		return sql;
	}

	private String buildNonHanAreFieldQuery(StringBuilder nonHanUserIds, StringBuilder nonHanGroupIds) {
		String sql = "select nhu.id_ nhuId, nhug.status, nhug.name, nhu.firstName, nhu.lastName, nhu.non_han_group_id "
				+ "  from non_han_user nhu, non_han_user_group nhug where nhu.non_han_group_id = nhug.id_";
		if (nonHanUserIds.toString().length() > 0) {
			sql += " and nhu.id_ in (" + nonHanUserIds.toString() + ")";
			log.trace("Query add nonHanUserIds>  " + nonHanUserIds.toString() + ".");
		}
		if (nonHanGroupIds.toString().length() > 0) {
			sql += " and nhug.id_ in (" + nonHanGroupIds.toString() + ")";
			log.trace("Query add nonHanGroupIds>  " + nonHanGroupIds.toString() + ".");
		}
		if (nonHanUserIds.toString().length() < 1 && nonHanGroupIds.toString().length() < 1) {
			sql += " and 1=2";
		}
		return sql;
	}

	private long[] convertToArray (final List<Long> toConvert) {
		final long[] converted = new long[toConvert.size ()];
		int i = 0;
		for (Long current : toConvert) {
			converted[i++] = current;
		}
		return converted;
	}

	public GroupDisplayTO[] getBulkUploadNames (final String nameFilter) {
		final List<GroupDisplayTO> bulkUploadLists = new ArrayList<GroupDisplayTO> ();
		DataSource liferayDataSource = InfrastructureUtil.getDataSource();
		try {
			Statement stat = liferayDataSource.getConnection().createStatement();
			log.debug("filter "+ nameFilter + ", " + BatchStatus.FINISHED + ".");
			String sql = "select id_, name, status, createdDate from non_han_user_group ";
			if (nameFilter != null && nameFilter.length () > 0)
				sql += " where upper (name) like \"" + nameFilter.toUpperCase() + "%\"";
			sql += " order by name";
			log.debug("Run>  " + sql);
			ResultSet result = stat.executeQuery(sql);
			while (result.next()) {
				String status = result.getString("status");
				long id = result.getLong("id_");
				log.debug(">  " + id  + "/" + status + ": " + result.getString("name"));
				if (status.equals(BatchStatus.FINISHED.toString())) {
					log.debug("adding " + id);
					bulkUploadLists.add (new GroupDisplayTO (id, result.getString("name"),
							"", result.getDate("createdDate")));
				}
			}
			result.close();
			stat.close();
		} catch (Exception e) {
			System.err.println("ERROR Class not found: " + e);
		}
		return bulkUploadLists.toArray(new GroupDisplayTO[0]);
	}

	public long addGroup(UserCustomGroup group) {
		
		long groupId = 0;
		try {
			groupId = CounterLocalServiceUtil.increment();
			log.debug("counter groupId: " + groupId);
			
			org.cdph.han.model.UserCustomGroup ucg =UserCustomGroupLocalServiceUtil.createUserCustomGroup(groupId);
			//TODO: convert date 
			ucg.setCreatedDate(group.getCreatedDate().toGMTString());
			ucg.setGroupId(groupId);
			try{
				if (group.getModifiedBy() != null) {
					ucg.setModifiedBy(group.getModifiedBy().getUserId());
				}
			}catch(Exception e){
				log.error("Exception: " + e);
			}
			ucg.setName(group.getName());
			//TODO: hunt down this value
			ucg.setNew(true);
			ucg.setUserId(group.getUserData().getUserId());
			log.debug("new groupId: " + ucg.getGroupId());
			UserCustomGroupLocalServiceUtil.updateUserCustomGroup(ucg);
		} catch (Exception e) {
			log.error(e);
		} 
		return groupId;
	}

	@Override
	public List<UserDisplayTO> filterUsers(long[] organizationIds,
			long[] areaFieldIds, long[] roleIds) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<UserDisplayTO> getUsersByOrganizationType(long orgTypeId) {
		final List<UserDisplayTO> users = new ArrayList<UserDisplayTO> ();
		try {
			Organization org = OrganizationLocalServiceUtil.getOrganization(orgTypeId);
//			UserOrganizationalData orgData = UserOrganizationalDataLocalServiceUtil.getUserOrganizationalData(orgTypeId);
		} catch (PortalException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return users;
	}

	@Override
	public UserDisplayTO[] searchIndividuals(String firstName, String lastName,
			boolean chicagoOnly) {
		OrganizationalSearchServiceImpl  organizationSearchServiceImpl = new OrganizationalSearchServiceImpl();     
		UserDisplayTO[] foundUsers_arr = organizationSearchServiceImpl.searchIndividuals(firstName, lastName, chicagoOnly);
//		List<UserDisplayTO> foundUsers = new ArrayList<UserDisplayTO> (Arrays.asList(foundUsers_arr));
//		log.debug("search: " + foundUsers);
//		if (foundUsers != null) {
//			for (UserDisplayTO foundUser : foundUsers) {
//				 foundUsers.add (foundUser);
//			}
//		}
		return foundUsers_arr;
	}
}
