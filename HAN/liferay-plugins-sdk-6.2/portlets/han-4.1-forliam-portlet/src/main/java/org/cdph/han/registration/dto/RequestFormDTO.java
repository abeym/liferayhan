package org.cdph.han.registration.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.cdph.han.dto.HanLazyDataModel;
import org.cdph.han.dto.LazyDataModel;
import org.cdph.han.persistence.Organization;
import org.cdph.han.persistence.RequestForm;
import org.cdph.han.registration.service.RegistrationService;

// implements Serializable{

public class RequestFormDTO extends LazyDataModel<RequestForm>{
	
	private RegistrationService service;
	private long id;
	private String firstName;
	private String middleName;
	private String lastName;
	private boolean selected;
	private Organization organization;
	private String organizationName;
	private Date requestSubmissionDate;
	
	public RequestFormDTO(RegistrationService service) {
		super();
		this.service = service;
		findRows(0,100);
	}

	@Override
	public int countRows() {
		//TODO resolve later
//		return service.getPendingRegistrationRequestsCount();
		return 0;
	}

	@Override
	public List<RequestForm> findRows(int startRow, int finishRow) {
		List<RequestForm> l1 =service.getPendingRegistrationRequests(startRow, (finishRow - startRow)+1);
		//List<RequestForm> l2 = service.getPendingRegistrationRequests();
		 
		return l1; 
	}

	public void setService(RegistrationService service) {
		this.service = service;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public Date getRequestSubmissionDate() {
		return requestSubmissionDate;
	}

	public void setRequestSubmissionDate(Date requestSubmissionDate) {
		this.requestSubmissionDate = requestSubmissionDate;
	}

}
