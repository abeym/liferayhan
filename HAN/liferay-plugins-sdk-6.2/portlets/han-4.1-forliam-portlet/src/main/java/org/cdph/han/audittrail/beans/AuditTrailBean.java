package org.cdph.han.audittrail.beans;

import com.liferay.faces.bridge.model.UploadedFile;
import com.liferay.faces.portal.context.LiferayFacesContext;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.model.User;
import com.liferay.portal.theme.ThemeDisplay;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.Application;
import javax.faces.application.ViewHandler;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIInput;
import javax.faces.component.UISelectOne;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.DataModel;
import javax.faces.model.SelectItem;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.portlet.PortletRequest;
import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.audittrail.dto.AuditTrailDTO;
import org.cdph.han.audittrail.dto.AuditTrailDataModel;
import org.cdph.han.audittrail.service.AuditTrailService;
import org.cdph.han.beans.BaseBean;
import org.cdph.han.dto.HanLazyDataModel;
import org.cdph.han.persistence.AuditTrailAction;
import org.icefaces.ace.component.datatable.DataTable;
import org.icefaces.ace.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;

public class AuditTrailBean extends BaseBean {

	/** Class logger */
	private static final Log log = LogFactory.getLog(AuditTrailBean.class);

	private Date startDate;
	private Date endDate;
	private String performedBy;
	private AuditTrailAction actionPerformed;
	private String result;

	private UIInput startDateComp;
	private UIInput endDateComp;
	private UIInput performedByComp;
	private UISelectOne actionPerformedComp;
	private DataTable dataTable;
	private SelectItem[] actions = {
			new SelectItem(null, ""),
			new SelectItem(AuditTrailAction.SUCCESSFUL_LOGIN,
					AuditTrailAction.SUCCESSFUL_LOGIN.getDisplayFormat()),
			new SelectItem(AuditTrailAction.FAILED_LOGIN,
					AuditTrailAction.FAILED_LOGIN.getDisplayFormat()),
			new SelectItem(AuditTrailAction.USER_LOCKOUT,
					AuditTrailAction.USER_LOCKOUT.getDisplayFormat()),
			new SelectItem(AuditTrailAction.USER_LOGOUT,
					AuditTrailAction.USER_LOGOUT.getDisplayFormat()),
			new SelectItem(AuditTrailAction.SESSION_DESTROYED,
					AuditTrailAction.SESSION_DESTROYED.getDisplayFormat()),
			new SelectItem(AuditTrailAction.ALERT_ARCHIVED,
					AuditTrailAction.ALERT_ARCHIVED.getDisplayFormat()),
			new SelectItem(AuditTrailAction.ALERT_DELETED,
					AuditTrailAction.ALERT_DELETED.getDisplayFormat()),
			new SelectItem(AuditTrailAction.ALERT_MODIFIED,
					AuditTrailAction.ALERT_MODIFIED.getDisplayFormat()),
			new SelectItem(AuditTrailAction.ALERT_PUBLISHED,
					AuditTrailAction.ALERT_PUBLISHED.getDisplayFormat()),
			new SelectItem(AuditTrailAction.ALERT_SCHEDULED,
					AuditTrailAction.ALERT_SCHEDULED.getDisplayFormat())

	};

	private AuditTrailDTO searchResults;

	private AuditTrailService service;

	/**
	 * Listener for the form search button click action.
	 * 
	 * @param e
	 *            click action event.
	 */
	public void searchButtonListener(ActionEvent e) {
		log.debug("In searchButtonListener...");

		if(auditTrailDataModel != null)
		{
			setStartDate(auditTrailDataModel.getStartDate());
			setEndDate(auditTrailDataModel.getEndDate());
		}
		if (startDate != null && endDate != null) {
			if (startDate.after(endDate)) {
				addFacesMessage(startDateComp.getClientId(FacesContext
						.getCurrentInstance()),
						"audittrail.search.dates.error1");
			}
			if (endDate.before(startDate)) {
				addFacesMessage(endDateComp.getClientId(FacesContext
						.getCurrentInstance()),
						"audittrail.search.dates.error2");
				return;
			}
		}

		/*searchResults.setWrappedData(null);//clearCache();

		searchResults.setStartDate(startDate);
		searchResults.setEndDate(endDate);
		searchResults.setPerformedBy(performedBy);
		searchResults.setActionPerformed(actionPerformed);
		searchResults.setResult(result);*/
		//auditTrailDataModel = new AuditTrailDataModel();
		/*auditTrailDataModel.load(0, 20, null, getFilters());
		dataTable.processDecodes(FacesContext.getCurrentInstance());*/
		/*DataTable dataTable = (DataTable)FacesContext.getCurrentInstance().getViewRoot().findComponent("queryForm:auditTable");
        dataTable.setFirst(0);*/
		
		/*auditTrailDataModel.setStartDate(startDate);
		auditTrailDataModel.setEndDate(endDate);
		auditTrailDataModel.setPerformedBy(performedBy);
		auditTrailDataModel.setActionPerformed(actionPerformed);
		auditTrailDataModel.setResult(result);*/
//		auditTrailDataModel.reloadFilters();
//		FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add(dataTable.getClientId());
	}

	/**
	 * Listener for the form clear button click action.
	 * 
	 * @param e
	 *            click action event.
	 */
	public void clearButtonListener(ActionEvent e) {
		log.debug("In clearButtonListener...");

		startDate = null;
		endDate = null;
		performedBy = null;
		actionPerformed = null;
		result = null;

		startDateComp.resetValue();
		endDateComp.resetValue();
		performedByComp.resetValue();
		actionPerformedComp.resetValue();

		/*searchResults.setWrappedData(null);//clearCache();
		searchResults.setStartDate(startDate);
		searchResults.setEndDate(endDate);
		searchResults.setPerformedBy(performedBy);
		searchResults.setActionPerformed(actionPerformed);
		searchResults.setResult(result);*/
		auditTrailDataModel.resetFilters();
		/*auditTrailDataModel.setStartDate(startDate);
		auditTrailDataModel.setEndDate(endDate);
		auditTrailDataModel.setPerformedBy(performedBy);
		auditTrailDataModel.setActionPerformed(actionPerformed);
		auditTrailDataModel.setResult(result);*/
//		FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add(dataTable.getClientId());
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getPerformedBy() {
		return performedBy;
	}

	public void setPerformedBy(String performedBy) {
		this.performedBy = performedBy;
	}

	public AuditTrailAction getActionPerformed() {
		return actionPerformed;
	}

	public void setActionPerformed(AuditTrailAction actionPerformed) {
		this.actionPerformed = actionPerformed;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public HanLazyDataModel getSearchResults() {
		if (searchResults == null) {
			searchResults = new AuditTrailDTO(service);
		}
		return searchResults;
	}

	public void setSearchResults(AuditTrailDTO searchResults) {
		this.searchResults = searchResults;
	}

	public void setService(AuditTrailService service) {
		this.service = service;
	}

	public SelectItem[] getActions() {
		return actions;
	}

	public void setStartDateComp(UIInput startDateComp) {
		this.startDateComp = startDateComp;
	}

	public void setEndDateComp(UIInput endDateComp) {
		this.endDateComp = endDateComp;
	}

	public UIInput getStartDateComp() {
		return startDateComp;
	}

	public UIInput getEndDateComp() {
		return endDateComp;
	}

	public UIInput getPerformedByComp() {
		return performedByComp;
	}

	public void setPerformedByComp(UIInput performedByComp) {
		this.performedByComp = performedByComp;
	}

	public UISelectOne getActionPerformedComp() {
		return actionPerformedComp;
	}

	public void setActionPerformedComp(UISelectOne actionPerformedComp) {
		this.actionPerformedComp = actionPerformedComp;
	}
	
	
	///////////////////**************************8/////////////////////////////////////

	// Private Data Members
	@Autowired
	@ManagedProperty("auditTrailDataModel")
	private transient AuditTrailDataModel auditTrailDataModel;

	private transient User selectedUser;
	private transient List<SelectItem> statusSelectItems;
	private transient UploadedFile uploadedFile;
	private transient String selectedUserPortraitURL;

	public void forceListReload() {
		selectedUser = null;
		selectedUserPortraitURL = null;
		auditTrailDataModel = null;
		uploadedFile = null;
	}

	public void setDataModel(DataModel dataModel) {
		this.auditTrailDataModel = (AuditTrailDataModel) dataModel;
	}
	public DataModel getDataModel() {

		if (auditTrailDataModel == null) {
			auditTrailDataModel = new AuditTrailDataModel();
		}

		return auditTrailDataModel;
	}

	public User getSelectedUser() {
		return selectedUser;
	}

	public void setSelectedUser(User selectedUser) {
		this.selectedUser = selectedUser;
		this.selectedUserPortraitURL = null;
	}

	/**
	 * This method returns a fully encoded URL that can be used in an HTML img tag to display the selected user's
	 * portrait. In order to determine the value of the URL, this method delegates much of that responsibility to the
	 * {@link UserPortraitResource} class.
	 */
	public String getSelectedUserPortraitURL() {

		if (selectedUserPortraitURL == null) {

			String uploadedFileId = null;

			if (uploadedFile != null) {
				uploadedFileId = uploadedFile.getId();
			}

			FacesContext facesContext = FacesContext.getCurrentInstance();
			ExternalContext externalContext = facesContext.getExternalContext();
			PortletRequest portletRequest = (PortletRequest) externalContext.getRequest();
			ThemeDisplay themeDisplay = (ThemeDisplay) portletRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String portalURL = themeDisplay.getPortalURL();
			String imagePath = portalURL + "/image";
			/*UserPortraitResource userPortraitResource = new UserPortraitResource(imagePath, selectedUser,
					uploadedFileId);
			String requestPath = userPortraitResource.getRequestPath();*/
			Application application = facesContext.getApplication();
			ViewHandler viewHandler = application.getViewHandler();
			//String resourceURL = viewHandler.getResourceURL(facesContext, requestPath);
			//selectedUserPortraitURL = externalContext.encodeResourceURL(resourceURL);
		}

		return selectedUserPortraitURL;
	}

	public List<SelectItem> getStatusSelectItems() {

		if (statusSelectItems == null) {
			LiferayFacesContext liferayFacesContext = LiferayFacesContext.getInstance();
			statusSelectItems = new ArrayList<SelectItem>();
			statusSelectItems.add(new SelectItem(WorkflowConstants.STATUS_ANY,
					liferayFacesContext.getMessage("any-status")));
			statusSelectItems.add(new SelectItem(WorkflowConstants.STATUS_APPROVED,
					liferayFacesContext.getMessage("active")));
			statusSelectItems.add(new SelectItem(WorkflowConstants.STATUS_INACTIVE,
					liferayFacesContext.getMessage("inactive")));
		}

		return statusSelectItems;
	}

	public UploadedFile getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(UploadedFile uploadedFile) {
		this.uploadedFile = uploadedFile;

		// Force ICEfaces to re-render the portrait URL.
		selectedUserPortraitURL = null;
	}

	public void selectUser(SelectEvent selectEvent) {

		try {
			User selectedUser = (User) selectEvent.getObject();
			setSelectedUser(selectedUser);
			setFormRendered(true);
		}
		catch (Exception e) {
			log.error(e.getMessage(), e);
			LiferayFacesContext liferayFacesContext = LiferayFacesContext.getInstance();
			liferayFacesContext.addGlobalUnexpectedErrorMessage();
		}
	}

	private boolean formRendered = false;

	public boolean isFormRendered() {
		return formRendered;
	}

	public void setFormRendered(boolean formRendered) {
		this.formRendered = formRendered;
	}

	

	public DataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(DataTable dataTable) {
		this.dataTable = dataTable;
	}
}
