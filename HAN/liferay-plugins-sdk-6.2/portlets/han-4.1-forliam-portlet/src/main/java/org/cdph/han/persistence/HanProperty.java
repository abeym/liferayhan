package org.cdph.han.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Entity class representing properties of the HAN system
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Entity
@Table (name = "han_properties", schema = "lportal")
@NamedQueries (
	value = {
		@NamedQuery (name = "HanProperty.FindByName", query = "select p from HanProperty p where p.name = :name")
	}
)
public class HanProperty implements Serializable {
	/** Class serial version UID */
	private static final long serialVersionUID = 4342708767648840146L;
	/** ID of the property */
	@Id
	@Column (name = "id", nullable = false)
	@GeneratedValue (strategy = GenerationType.TABLE)
	private long id;
	/** Name of the property */
	@Column (name = "name", length = 50, nullable = false, unique = true)
	private String name;
	/** Value of the property */
	@Column (name = "value", length = 50, nullable = false)
	private String value;

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean equal = false;
		if (obj != null && obj instanceof HanProperty) {
			final HanProperty other = (HanProperty) obj;
			equal = other.getId () == id;
			equal = equal && (name != null ? name.equals (other.getName ()) : other.getName () == null);
			equal = equal && (value != null ? value.equals (other.getValue ()) : other.getValue () == null);
		}
		return equal;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		return Long.valueOf (id).hashCode ();
	}

	/**
	 * @return the id
	 */
	public long getId () {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId (long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName () {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName (String name) {
		this.name = name;
	}

	/**
	 * @return the value
	 */
	public String getValue () {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue (String value) {
		this.value = value;
	}

}
