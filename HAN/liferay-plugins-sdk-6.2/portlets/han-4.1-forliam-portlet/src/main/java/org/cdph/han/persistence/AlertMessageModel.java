package org.cdph.han.persistence;

public interface AlertMessageModel {

	/**
	 * @return the id
	 */
	long getId ();

	/**
	 * @return the locale
	 */
	AlertLocale getLocale ();

	/**
	 * @return the attachment
	 */
	AlertAttachmentModel getAttachment ();

	/**
	 * @return the telephonyMessage
	 */
	String getTelephonyMessage ();

	/**
	 * @return the portalMessage
	 */
	String getPortalMessage ();

	/**
	 * @return the abstractText
	 */
	String getAbstractText ();

}