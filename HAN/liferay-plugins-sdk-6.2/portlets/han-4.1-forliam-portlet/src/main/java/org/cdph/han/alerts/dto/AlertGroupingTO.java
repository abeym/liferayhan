package org.cdph.han.alerts.dto;

/**
 * Class used for grouping alert results
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class AlertGroupingTO extends AlertSearchTO {
	/** Class serial version UID */
	private static final long serialVersionUID = 6770314356383528933L;
	/** Flag to expanded or collapse the group */
	private boolean expanded;
	/** Returns the key used to group the alerts */
	private Object key;

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.dto.AlertSearchTO#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean equal = false;
		if (obj != null && obj instanceof AlertGroupingTO) {
			final AlertGroupingTO other = (AlertGroupingTO) obj;
			equal = (getTitle () == null) ? other.getTitle () == null : getTitle ().equals (other.getTitle ());
			equal = equal && (key == null ? other.getKey () == null : key.equals (other.getKey ()));
		}
		return equal;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.dto.AlertSearchTO#hashCode()
	 */
	@Override
	public int hashCode () {
		return getTitle () != null ? getTitle ().hashCode () : -1;
	}

	/**
	 * @return the expanded
	 */
	public boolean isExpanded () {
		return expanded;
	}

	/**
	 * @param expanded the expanded to set
	 */
	public void setExpanded (boolean expanded) {
		this.expanded = expanded;
	}

	/**
	 * @return the image
	 */
	public String getImage () {
		String image;
		if (expanded) {
			image = "tree_nav_top_open_no_siblings.gif";
		} else {
			image = "tree_nav_top_close_no_siblings.gif";
		}
		return image;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.dto.AlertSearchTO#isSpecialized()
	 */
	@Override
	public boolean isSpecialized () {
		return true;
	}

	/**
	 * @return the key
	 */
	public Object getKey () {
		return key;
	}

	/**
	 * @param key the key to set
	 */
	public void setKey (Object key) {
		this.key = key;
	}

}
