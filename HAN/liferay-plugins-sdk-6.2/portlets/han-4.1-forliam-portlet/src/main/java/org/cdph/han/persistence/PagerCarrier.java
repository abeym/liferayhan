package org.cdph.han.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity class that represents a pager carrier
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Entity
@Table (name = "pager_carrier_catalog", schema = "lportal")
public class PagerCarrier implements Serializable {
	/** Class serial version UID */
	private static final long serialVersionUID = -6384088584375997005L;
	/** ID of the carrier */
	@Id
	@Column (name = "id")
	private int carrierId;
	/** Name of the carrier */
	@Column (name = "carrier_name")
	private String name;
	/** ID of the carrier at mir3 */
	@Column (name = "mir3_id")
	private int mir3Id;

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean equal = false;
		if (obj != null && obj instanceof PagerCarrier) {
			final PagerCarrier other = (PagerCarrier) obj;
			equal = carrierId == other.getCarrierId ();
			equal = equal && mir3Id == other.getMir3Id ();
		}
		return equal;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		return carrierId;
	}

	/**
	 * @return the carrierId
	 */
	public int getCarrierId () {
		return carrierId;
	}

	/**
	 * @param carrierId the carrierId to set
	 */
	public void setCarrierId (int carrierId) {
		this.carrierId = carrierId;
	}

	/**
	 * @return the name
	 */
	public String getName () {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName (String name) {
		this.name = name;
	}

	/**
	 * @return the mir3Id
	 */
	public int getMir3Id () {
		return mir3Id;
	}

	/**
	 * @param mir3Id the mir3Id to set
	 */
	public void setMir3Id (int mir3Id) {
		this.mir3Id = mir3Id;
	}

}
