package org.cdph.han.registration.service;

import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.DuplicateUserEmailAddressException;
import com.liferay.portal.DuplicateUserScreenNameException;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.Contact;
import com.liferay.portal.model.Phone;
import com.liferay.portal.model.User;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.service.PhoneLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.email.services.EmailFolderService;
import org.cdph.han.model.HanUserData;
import org.cdph.han.model.PhoneHanData;
import org.cdph.han.model.Preferences;
import org.cdph.han.model.UserAreaFieldAsoc;
import org.cdph.han.persistence.AreaField;
import org.cdph.han.persistence.HANPhoneType;
import org.cdph.han.persistence.Organization;
import org.cdph.han.persistence.RegistrationRequestStatus;
import org.cdph.han.persistence.RequestForm;
import org.cdph.han.persistence.Role;
import org.cdph.han.registration.exception.EmailAlreadyExistsException;
import org.cdph.han.service.AreaFieldLocalServiceUtil;
import org.cdph.han.service.HanUserDataLocalServiceUtil;
import org.cdph.han.service.PhoneHanDataLocalServiceUtil;
import org.cdph.han.service.PreferencesLocalServiceUtil;
import org.cdph.han.service.persistence.UserAreaFieldAsocPK;
import org.cdph.han.service.persistence.UserAreaFieldAsocUtil;
import org.cdph.han.synch.dto.Mir3DeviceTO;
import org.cdph.han.synch.dto.Mir3RecipientResponse;
import org.cdph.han.synch.dto.Mir3UserTO;
import org.cdph.han.synch.services.UserSynchService;
import org.cdph.han.util.services.HanPropertiesService;
import org.cdph.liferay.util.PwdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

//@Repository
//@Service("registrationService")
public class RegistrationServiceImpl implements RegistrationService {

	/** Class logger */
	private static Log log = LogFactory.getLog(RegistrationServiceImpl.class);

	/** Regular Organization */
	private static final int ORGANIZATION_TYPE = 1;
	/** No Region */
	private static final int ORGANIZATION_REGION_ID = 0;
	/** United States */
	private static final int ORGANIZATION_COUNTRY_ID = 19;
	/** Full Member on table ListType */
	private static final int ORGANIZATION_STATUS_ID = 12017;
	/*
	 * Key of the registered email used as primary dev pref during the
	 * registration process.
	 */
	private final String DEV_PREFERENCE_EMAIL_KEY = "PRIMARY";
	/**
	 * Key of the registered phone used as secondary dev pref during the
	 * registration process.
	 *
	private final String DEV_PREFERENCE_PHONE_KEY = "P:";
	*/
	/** MIR3 Admin User */
	private static final String MIR3_ADMIN_USER = "MIR3_ADMIN_USER";
	/** MIR3 Admin User Password */
	private static final String MIR3_ADMIN_PASS = "MIR3_ADMIN_PASS";
	/** MIR3 API Version key */
	private static final String API_VERSION_KEY = "MIR3_API_VERSION";
	/** MIR3 User Timezone */
	private static final String MIR3_USER_TIMEZONE = "CENTRAL_USA"; 
	/** MIR3 Default Role */
	private static final String MIR3_DEFAULT_ROLE = "Recipient";
	/** MIR3 Default Language */
	private static final String MIR3_DEFAULT_LANGUAGE = "ENGLISH";
	/** MIR3 Default Division */
	private static final String MIR3_DEFAULT_DIVISION = "/";
	/** MIR3 Default Device Description */
	private static final String MIR3_DEFAULT_DEVICE_DESC = "Office Email";
	/** MIR3 Default Device Type */
	private static final String MIR3_DEFAULT_DEVICE_TYPE = "Work Email";
	/** Key to retrieve the email domain */
	private static final String EMAIL_DOMAIN_KEY = "HAN_EMAIL_DOMAIN";
	

	/** Default Timezone for users */
	private static final String USER_DEFAULT_TIMEZONE_ID = "America/Chicago";

	/*@Autowired
	private ContactDevicesService contactDevPrefsService;*/

	//abey...
	//@Autowired
	private UserSynchService mir3Service;

	//abey...
	//@Autowired
	private HanPropertiesService hanPropertiesService;

	//abey...
	//@Autowired
	private EmailFolderService emailFolderService;

	private EntityManager em;

	//abey...
	//@Autowired
	//@Qualifier("mail")
	private JmsTemplate jmsTemplate;

	//abey ...
	//@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	//abey ...
	/*@Transactional(rollbackFor = { EmailAlreadyExistsException.class,
			RuntimeException.class })*/
	public Long registerRequest(RequestForm form)
			throws EmailAlreadyExistsException {

		if (emailExist(form.getEmail())) {
			throw new EmailAlreadyExistsException(
					"The email associated with this request already exists");
		}

		form.setRequestSubmissionDate(new Date());
		form.setStatus(RegistrationRequestStatus.SUBMITTED);
		em.persist(form);

		log.debug("Request ID: " + form.getId());

		return form.getId();
	}

	@SuppressWarnings("unchecked")
	//abey ...@Transactional(readOnly = true)
	public List<RequestForm> getPendingRegistrationRequests() {

		List<RequestForm> requests = em
				.createQuery(
						"Select r from RequestForm r where r.status=:status order by r.requestSubmissionDate")
				.setParameter("status", RegistrationRequestStatus.SUBMITTED)
				.getResultList();
		return requests;
	}

	@SuppressWarnings("unchecked")
	//abey ...@Transactional(readOnly = true)
	public List<RequestForm> getPendingRegistrationRequests(int startIndex,
			int maxResults) {

		List<RequestForm> requests = em
				.createQuery(
						"Select r from RequestForm r where r.status=:status order by r.requestSubmissionDate")
				.setParameter("status", RegistrationRequestStatus.SUBMITTED)
				.setFirstResult(startIndex).setMaxResults(maxResults)
				.getResultList();
		return requests;
	}

	//abey ...@Transactional(readOnly = true)
	public int getPendingRegistrationRequestsCount() {

		return ((Number) em
				.createQuery(
						"Select count(r) from RequestForm r where r.status=:status order by r.requestSubmissionDate")
				.setParameter("status", RegistrationRequestStatus.SUBMITTED)
				.getSingleResult()).intValue();
	}

	//abey ...@Transactional(readOnly = true)
	public Organization getOrganization(Long id) {
		return (Organization) em.find(Organization.class, id);
	}

	//abey ...@Transactional(readOnly = true)
	public AreaField getAreaField(Integer id) {
		return (AreaField) em.find(AreaField.class, id);
	}

	//abey ...@Transactional(readOnly = true)
	public Role getRole(Long id) {
		return (Role) em.find(Role.class, id);
	}

	//abey ...@Transactional(readOnly = true)
	public RequestForm getRegistrationRequest(Long id) {
		RequestForm reqForm = em.find(RequestForm.class, id);

		return reqForm;
	}

	/*
	 * public boolean reEnroll(ReEnrollmentForm form) { // Create a thread safe
	 * "copy" of the template message and customize it SimpleMailMessage msg =
	 * new SimpleMailMessage(templateMessage);
	 * msg.setTo("rmartinezayala@gmail.com"); msg.setText(
	 * "Dear Rodrigo, thank you for placing order. Your order number is XXYYY");
	 * 
	 * mailSender.send(msg);
	 * 
	 * return randomizer.nextBoolean(); }
	 */

	@SuppressWarnings("unchecked")
	//abey ...@Transactional(readOnly = true)
	public List<Organization> getOrganizationTypes() {
		List<Organization> organizationTypes = new ArrayList<Organization>();

		List<Organization> tmpOrgs = em.createQuery(
				"Select a from Organization a WHERE a.parentOrganizationId=0 ORDER BY a.name")
				.getResultList();

		for (Organization organization : tmpOrgs) {
			if (organization.getParentOrganizationId() == 0) {
				organizationTypes.add(organization);
			}
		}

		return organizationTypes;
	}

	@SuppressWarnings("unchecked")
	//abey ...@Transactional(readOnly = true)
	public List<Organization> getSuborganizations(Long parentOrgId) {
		log.debug("in RegistrationService.getSuborganizations(" + parentOrgId
				+ ")");
		List<Organization> subOrganiztions = null;
		if (parentOrgId == null) {
			return null;
		}

		subOrganiztions = em
				.createQuery(
						"Select o from Organization o where o.parentOrganizationId=:parentOrgID" +
						" ORDER BY o.name")
				.setParameter("parentOrgID", parentOrgId).getResultList();

		for (Organization organization : subOrganiztions) {
			log.debug(organization.getName());
		}

		return subOrganiztions;
	}

	//abey ...@Transactional(readOnly = true)
	public List<Role> getOrganizationalRoles(Long orgTypeId) {
		log.debug("In getOrganizationalRoles(" + orgTypeId + ")");
		final Organization orgType = em.find (Organization.class, orgTypeId);
		final List<Role> roles = new ArrayList<Role> ();
		for (Role role : orgType.getRoles ()) {
			roles.add (role);
		}
		return roles;
	}

	@SuppressWarnings("unchecked")
	//abey ...@Transactional(readOnly = true)
	public List<AreaField> getAreaFields(Long organizationType) {
	/*	log.debug("In getAreaFields(" + organizationType + ")");
		return em
				.createQuery(
						"Select a from AreaField a where a.organization.organizationId=:orgType" +
						" ORDER BY a.description")
				.setParameter("orgType", organizationType).getResultList();*/
		List<AreaField> ha = new ArrayList<AreaField>();
		try {
			List<org.cdph.han.model.AreaField> las = AreaFieldLocalServiceUtil.getAreaFields(QueryUtil.ALL_POS, QueryUtil.ALL_POS);
			for(org.cdph.han.model.AreaField la:las){
		
			}
		} catch (SystemException e) {
			log.error(e);
		}
		
		
		return ha;
	}

	/**
	 * Checks Liferay's User table and Registration Request table for an
	 * existing screename.
	 * 
	 * @return flag indicating whether the user exists or not
	 */
	//abey ...@Transactional(readOnly = true)
	public boolean usernameExist(String username) {
		log.debug("In usernameExist(" + username + ")");

		/*
		 * User user = UserLocalServiceUtil.getUserByScreenName(companyId,
		 * username);
		 */
		int userCount = em.createQuery(
				"Select u from UserData u where u.screenName=:username")
				.setParameter("username", username).getResultList().size();

		log.debug("Found " + userCount + " with screenName= " + username);
		if (userCount > 0)
			return true;

		int registrationReqCount = em
				.createQuery(
						"Select r from RequestForm r where r.userName=:username AND r.status <> :status")
				.setParameter("username", username).setParameter("status",
						RegistrationRequestStatus.REJECTED).getResultList()
				.size();

		log.debug("Found " + registrationReqCount + " with screenName= "
				+ username);

		return registrationReqCount > 0;
	}

	/**
	 * Checks Liferay's User table and Registration Request table for an
	 * existing email.
	 * 
	 * @return flag indicating whether the user exists or not
	 */
	//abey ...@Transactional(readOnly = true)
	public boolean emailExist(String email) {
		log.debug("In emailExist(" + email + ")");

		int userCount = em.createQuery(
				"Select u from UserData u where u.email=:email").setParameter(
				"email", email).getResultList().size();

		log.debug("Found " + userCount + " with email= " + email);
		if (userCount > 0)
			return true;

		int registrationReqCount = em.createQuery(
				"Select r from RequestForm r where r.email=:email "
						+ "AND r.status <> :status AND r.status <> :status2 ")
				.setParameter("email", email).setParameter("status",
						RegistrationRequestStatus.REJECTED).setParameter(
						"status2", RegistrationRequestStatus.APPROVED)
				.getResultList().size();

		log.debug("Found " + registrationReqCount + " RequestForm with email= "
				+ email);

		return registrationReqCount > 0;
	}

	/**
	 * Method that approves a registration request. It Uses liferay API
	 * specifically: UserLocalServiceUtil, HanUserDataLocalServiceUtil,
	 * UserAreaFieldAsocUtil and PhoneHanDataLocalServiceUtil. It also uses JPA
	 * to update the Registration Status and approved Date.
	 * 
	 * In order for this to work, Liferay's transaction manager was changed from
	 * HibernateTransactionManager to JtaTransactionManager. Its spring managed
	 * datasource (liferayDataSource) was configured with defaultAutoCommit to
	 * false. That way the transaction that is started by this portlet which is
	 * JTA can propagate to Liferay API's *ServiceUtil spring beans and use the
	 * tx created here.
	 * 
	 * Liferay Jboss Datasource was changed from <b>local-tx-datasource</b> to
	 * <b>xa-datasource</b>.
	 * 
	 * In portal.properties the following hibernate properties were
	 * added/changed:
	 * <ul>
	 * <li>hibernate.connection.release_mode=after_statement</li>
	 * <li>hibernate.transaction.factory_class=org.hibernate.transaction.
	 * JTATransactionFactory</li>
	 * <li>hibernate.transaction.manager_lookup_class=org.hibernate.transaction.
	 * JBossTransactionManagerLookup</li>
	 * <li>hibernate.connection.autocommit=false</li>
	 * <ul>
	 * 
	 */
	//abey ...
	/*@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {
			DuplicateUserEmailAddressException.class,
			DuplicateUserScreenNameException.class, RuntimeException.class })
			*/
	public void approveRequest(RequestForm request)
			throws DuplicateUserEmailAddressException,
			DuplicateUserScreenNameException {

		log.debug("In approveRequest()...");
		boolean mir3SyncFlag = false;
		
		try {
			// log.debug("Request companyID: " + request.getCompanyId());
			// log.debug("User Organization: " +
			// (request.getOrganization() == null ? "New Organization" :
			// request.getOrganization().getOrganizationId()));

			User defaultUser = UserLocalServiceUtil.getDefaultUser(request
					.getCompanyId());

			long creatorUserId = defaultUser.getUserId();
			long companyId = request.getCompanyId();
			boolean autoPassword = true;
			String password = null;
			String password2 = null;
			boolean autoScreenName = true;
			// String screenName = request.getUserName();
			String screenName = "";
			String emailAddress = request.getEmail();
			Locale locale = defaultUser.getLocale();
			String firstName = request.getFirstName();
			String middleName = request.getMiddleName();
			String lastName = request.getLastName();
			int prefixId = 0;
			int suffixId = 0;
			boolean male = true;
			int birthdayMonth = Calendar.JANUARY;
			int birthdayDay = 1;
			int birthdayYear = 1970;
			String jobTitle = request.getTitle();
			long[] organizationIds = null;
			boolean sendEmail = true;

			if (log.isDebugEnabled()) {
				log.debug("creatorUserId: " + creatorUserId + " \n"
						+ "companyId: " + companyId + " \n" + "autoPassword: "
						+ autoPassword + " \n" + "password: " + password
						+ " \n" + "password2: " + password2 + " \n"
						+ "autoScreenName: " + autoScreenName + " \n"
						+ "screenName: " + screenName + " \n"
						+ "emailAddress: " + emailAddress + " \n" + "locale: "
						+ locale + " \n" + "firstName: " + firstName + " \n"
						+ "middleName: " + middleName + " \n" + "lastName: "
						+ lastName + " \n" + "prefixId: " + prefixId + " \n"
						+ "suffixId: " + suffixId + " \n" + "male: " + male
						+ " \n" + "birthdayMonth: " + birthdayMonth + " \n"
						+ "birthdayDay: " + birthdayDay + " \n"
						+ "birthdayYear: " + birthdayYear + " \n"
						+ "jobTitle: " + jobTitle + " \n" + "sendEmail: "
						+ sendEmail);
			}

			if (request.getOrganization() == null
					|| request.getOrganization().getOrganizationId() == -1) {
				log.debug("Organization does not exist. Creating it...");

				com.liferay.portal.model.Organization organization = OrganizationLocalServiceUtil
						.addOrganization(creatorUserId, 
								request.getOrganizationType().getOrganizationId(),
								request.getOrganizationName(),
								ORGANIZATION_TYPE+"",// type
								ORGANIZATION_REGION_ID,// regionID,
								ORGANIZATION_COUNTRY_ID,// countryId
								ORGANIZATION_STATUS_ID,// statusId,
								"",// comments
								true,// recursable
								null);
				organizationIds = new long[] { organization.getOrganizationId() };
			} else {
				organizationIds = new long[] { request.getOrganization()
						.getOrganizationId() };
			}

			if (log.isDebugEnabled()) {
				String orgIds = "OrganizationIds: [ ";
				for (int i = 0; i < organizationIds.length; i++) {
					orgIds += organizationIds[i];
				}
				orgIds += " ]";
				log.debug(orgIds);
			}

			log.debug("Creating user...");
			// Create user
			User user = UserLocalServiceUtil.addUser(creatorUserId, companyId, autoPassword, password, 
					password2, autoScreenName, screenName, emailAddress, 
					0, //facebookId, 
					"", //openId, 
					locale, firstName, middleName, lastName, prefixId, suffixId, male, 
					birthdayMonth, birthdayDay, birthdayYear, 
					jobTitle, 
					null, //groupIds, 
					organizationIds, 
					null, //roleIds, 
					null, //userGroupIds, 
					sendEmail, 
					null //serviceContext
					);
					
			// Set the timezone to CST
			user.setTimeZoneId(USER_DEFAULT_TIMEZONE_ID);
			UserLocalServiceUtil.updateUser(user);

			// Area fields
			log
					.debug("Associating organization, area field and role to user...");
			UserAreaFieldAsocPK userAreaFieldpk = new UserAreaFieldAsocPK(user
					.getUserId(), organizationIds[0]);
			log.debug("UserAreaFieldAsocPK: " + userAreaFieldpk.getUserId()
					+ " | " + userAreaFieldpk.getOrganizationId());

			UserAreaFieldAsoc userAreaFieldAsoc = UserAreaFieldAsocUtil
					.create(userAreaFieldpk);
			log.debug("UserAreaFieldAsoc ID: "
					+ userAreaFieldAsoc.getPrimaryKey());

			// AreaField
			userAreaFieldAsoc.setAreaFieldId(request.getAreaField()
					.getAreaFieldId());
			// Roles
			userAreaFieldAsoc.setRoleId(request.getRole().getRoleId());
			//UserAreaFieldAsocUtil.update(userAreaFieldAsoc, true);
			UserAreaFieldAsocUtil.update(userAreaFieldAsoc, null);

			// PhoneLocalServiceUtil.addPhone(userId, className, classPK,
			// number, extension,typeId, primary))
			log.debug("Adding phone to user...");
			Phone phone = PhoneLocalServiceUtil.addPhone(user.getUserId(),
					Contact.class.getName(), user.getContactId(), request
							.getWorkTelephone(), request.getWorkTelephoneExt(),
					HANPhoneType.WORK.listTypeId(), true);

			// Extra phone attributes
			log.debug("Adding Extra phone to user...");
			PhoneHanData phoneData = PhoneHanDataLocalServiceUtil
					.createPhoneHanData(phone.getPhoneId());
			// phoneData.setPhoneId(phone.getPhoneId());
			phoneData.setNew(true);
			phoneData.setPhoneName("Default Phone");
			//phoneData.setPreference(null);
			//phoneData.setPagerCarrierId(null);
			phoneData.setPreference(0);
			phoneData.setPagerCarrierId(0);
			PhoneHanDataLocalServiceUtil.updatePhoneHanData(phoneData);

			PhoneLocalServiceUtil.updatePhone(phone);

			// Make the email the 1st contact preference
			log.debug("Setting the registration email as the primary device and the " +
					"phone as the secondary device preferences...");
			/*
			String phonePref = DEV_PREFERENCE_PHONE_KEY + phone.getPhoneId();
			String[] preferences = { DEV_PREFERENCE_EMAIL_KEY, phonePref };
			List<UserDisplayTO> altContacts = new ArrayList<UserDisplayTO>();
			-- This method is only used to save preferences of users already created.
			contactDevPrefsService.savePreferences(preferences, altContacts,
					user.getUserId());
			*/
			Preferences preference = PreferencesLocalServiceUtil.createPreferences (
					CounterLocalServiceUtil.increment (Preferences.class.getName ()));
			preference.setDeviceIndex (0);
			preference.setPrimaryEmail (true);
			preference.setUserId (user.getUserId ());
			PreferencesLocalServiceUtil.updatePreferences (preference);
			preference = PreferencesLocalServiceUtil.createPreferences (
					CounterLocalServiceUtil.increment (Preferences.class.getName ()));
			preference.setDeviceIndex (1);
			preference.setPrimaryEmail (false);
			preference.setPhoneId (phone.getPhoneId ());
			preference.setUserId (user.getUserId ());
			PreferencesLocalServiceUtil.updatePreferences (preference);

			// Sync with Mir3 (create user)
			log.debug("Synching (creating) user on Mir3...");
			String userGenPassword = PwdGenerator.getPassword();
			List<Mir3UserTO> recipients = createMir3TO(request, userGenPassword);
			long time1 = System.currentTimeMillis();
			List<Mir3RecipientResponse> responses = mir3Service.addRecipients(recipients, 
					hanPropertiesService.getValue(MIR3_ADMIN_USER), 
					hanPropertiesService.getValue(MIR3_ADMIN_PASS), 
					hanPropertiesService.getValue(API_VERSION_KEY));
			Mir3RecipientResponse response = responses.get (0);
			long time2 = System.currentTimeMillis();
			if (log.isDebugEnabled()) {
				log.debug("Finished synching. Time elapsed: " + (time2 - time1)
						+ " ms");
			}
			mir3SyncFlag = response.getUserId () == -1;
			
			//Query the mir3 recently created user			
			Mir3UserTO mir3User = 
				mir3Service.searchRecipient(request.getEmail(), 
						hanPropertiesService.getValue(MIR3_ADMIN_USER), 
						hanPropertiesService.getValue(MIR3_ADMIN_PASS),
						hanPropertiesService.getValue(API_VERSION_KEY));
			
			// Han UserData
			log.debug("Adding HAN UserData...");
			HanUserData hanData = HanUserDataLocalServiceUtil
					.createHanUserData(user.getUserId());
			hanData.setDepartment(request.getDepartment());
			hanData.setNew(true);
			hanData.setPublic_profile(true);
			hanData.setTelephony_pin(mir3User.getPin());			
			hanData.setTelephony_id(mir3User.getTelephonyId());
			hanData.setTelephony_pass(userGenPassword);
			hanData.setMir3_id (response.getUserId ());
			hanData.setMailPass(userGenPassword);
			HanUserDataLocalServiceUtil.updateHanUserData(hanData);

			// Changing the status and date of approval
			log
					.debug("Updating the Registration Request's status and approved date");
			request.setStatus(RegistrationRequestStatus.APPROVED);
			request.setApprovedRejectedDate(new Date());

			em.merge(request);

			/* Remove the comment delimiters when deploying inbox capabilities */
			final String command = //"/bin/echo -n '" + hanData.getMailPass ()
					"/usr/bin/sudo -u cyrus /usr/sbin/saslpasswd2 -c " +
						user.getScreenName () + '@' + hanPropertiesService.getValue (EMAIL_DOMAIN_KEY) + " -p";

			log.debug ("Command: " + command);

			Process process = Runtime.getRuntime ().exec (command);
			process.getOutputStream ().write (hanData.getMailPass ().getBytes ());
			process.getOutputStream ().write ((byte) '\n');
			process.getOutputStream ().flush ();
	
			if (process.waitFor () == 0) {
				log.debug ("Exit value: " + process.exitValue ());
				process = Runtime.getRuntime ().exec (
						"sudo -u cyrus /etc/msync/sync_users.sh");
				process.waitFor ();
				emailFolderService.listFolders (user.getUserId ());
			} else {
				log.error ("Error creating user inbox " + user.getScreenName () + " " + process.exitValue ());
				BufferedReader stdInput = new BufferedReader (
						new InputStreamReader (process.getInputStream ()));

				BufferedReader stdError = new BufferedReader (
						new InputStreamReader (process.getErrorStream ()));
				String s = null;

				// read the output from the command

				System.out
						.println ("Here is the standard output of the command:\n");
				while ((s = stdInput.readLine ()) != null) {
					System.out.println (s);
				}

				// read any errors from the attempted command

				System.out
						.println ("Here is the standard error of the command (if any):\n");
				while ((s = stdError.readLine ()) != null) {
					System.out.println (s);
				}

			}

		} catch (DuplicateUserEmailAddressException dunex) {
			log.error(dunex.getMessage(), dunex);
			mir3SyncFlag = true; // Set the flag to delete the user at mir3
			throw dunex;
		} catch (DuplicateUserScreenNameException dunsnex) {
			log.error(dunsnex.getMessage(), dunsnex);
			mir3SyncFlag = true; // Set the flag to delete the user at mir3
			throw dunsnex;
		} catch (Exception ex) {
			log.error(ex.getMessage(), ex);
			mir3SyncFlag = true; // Set the flag to delete the user at mir3
			throw new RuntimeException(ex);
		}finally{
			if(mir3SyncFlag){
				log.error("Rolling back mir3 sync. Deleting created user...");
				try{
					List<String> delRecipients = new ArrayList<String>();
					delRecipients.add(request.getEmail());
					mir3Service.deleteRecipients(delRecipients, 
							hanPropertiesService.getValue(MIR3_ADMIN_USER), 
							hanPropertiesService.getValue(MIR3_ADMIN_PASS),
							hanPropertiesService.getValue(API_VERSION_KEY));
				}catch(Exception mir3Ex){
					log.warn("Error deleting recipient in mir3", mir3Ex);
				}
			}		
		}
	}

	private List<Mir3UserTO> createMir3TO(RequestForm request, String generatedPassword){
		
		Mir3UserTO mir3User = new Mir3UserTO();
		
		mir3User.setAreaField(request.getAreaField().getDescription());
		mir3User.setDepartment(request.getDepartment());
		mir3User.setFirstName(request.getFirstName());
		mir3User.setLastName(request.getLastName());
		mir3User.setOrganization(request.getOrganization().getName());
		mir3User.setOrganizationType(request.getOrganizationType()
				.getName());
		mir3User.setPassword(generatedPassword);
		mir3User.setRole(request.getRole().getName());
		
		mir3User.setDivision(MIR3_DEFAULT_DIVISION);
		mir3User.setLanguage(MIR3_DEFAULT_LANGUAGE);
		mir3User.setMir3Role(MIR3_DEFAULT_ROLE);
		mir3User.setTimezone(MIR3_USER_TIMEZONE);
		mir3User.setTitle(request.getTitle());
		mir3User.setUsername(request.getEmail());

		mir3User.setDevices(new ArrayList<Mir3DeviceTO>());

		Mir3DeviceTO device = new Mir3DeviceTO();
		device.setAddress(request.getEmail());
		device.setDescription(MIR3_DEFAULT_DEVICE_DESC);
		device.setType(MIR3_DEFAULT_DEVICE_TYPE);
		device.setPriority ("1"); // Moved default preference to the device
		mir3User.getDevices().add(device);
		device = new Mir3DeviceTO ();
		if (request.getWorkTelephoneExt () != null && request.getWorkTelephoneExt ().length () > 0) {
			device.setAddress (request.getWorkTelephone () + 'x' + request.getWorkTelephoneExt ());
		} else {
			device.setAddress (request.getWorkTelephone ());
		}
		device.setDescription ("Work Phone");
		device.setPriority ("2");
		device.setType ("Work Phone");
		mir3User.getDevices ().add (device);

		/*
		mir3User.setPreferences(new ArrayList<Mir3DevicePriorityTO>());
		Mir3DevicePriorityTO priority = new Mir3DevicePriorityTO();
		priority.setIndex(1);
		priority.setPriority(1);
		mir3User.getPreferences().add(priority);
		*/			 

		List<Mir3UserTO> recipients = new ArrayList<Mir3UserTO>();
		recipients.add(mir3User);
		
		return recipients;
	}
	
	/**
	 * Denies the Registration request.
	 */
	//abey ...@Transactional
	public void rejectRequest(RequestForm request) {
		log.debug("In rejectRequest...");

		log
				.debug("Updating the Registration Request's status and rejected date");
		request.setStatus(RegistrationRequestStatus.REJECTED);
		request.setApprovedRejectedDate(new Date());

		em.merge(request);

	}

	/**
	 * Publishes a message to the mail registration queue
	 * 
	 * @param registrationReqId
	 *            the registration request number
	 */
	public void enqueueNotificationEmail(Long registrationReqId) {
		log.debug("In sendNotificationEmail(Long registrationReqId)...");
		jmsTemplate.convertAndSend(registrationReqId);
	}
}
