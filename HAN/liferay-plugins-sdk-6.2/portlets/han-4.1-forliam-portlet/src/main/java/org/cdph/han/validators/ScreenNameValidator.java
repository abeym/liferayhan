package org.cdph.han.validators;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ScreenNameValidator extends HanValidator {
	
	private static Log _log = LogFactory.getLog(ScreenNameValidator.class);
	
	public static String SPECIAL_CHARS = "`|~|!|@|#|\\$|%|\\^|&|\\*|\\(|\\)|_|\\+|-|=|\\{|\\}|\\||\\\\|:|\\\"|;|'|<|>|\\?|,|\\.|/";

	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {

		/* retrieve the string value of the field */
		String screenName = (String) value;

		if (!screenName.matches(".{3,19}")) {
			throw new ValidatorException(
					createFacesMessage("screenname.validator.invalid.format.length"));
		}
		
		if (screenName.matches("[0-9]+")) {
			throw new ValidatorException(
					createFacesMessage("screenname.validator.invalid.format.numbers"));
		}
		
		if (screenName.matches(EmailValidator.EMAIL_REGEX)) {
			throw new ValidatorException(
					createFacesMessage("screenname.validator.invalid.format.email"));
		}
		
		
		//if(!screenName.matches(".*[a-z|A-Z|0-9|\\s|" + SPECIAL_CHARS + "]+.*")){
		if(!screenName.matches("[a-z|A-Z|0-9]+")){
			throw new ValidatorException(
					createFacesMessage("screenname.validator.invalid.format"));
		}
	}

}
