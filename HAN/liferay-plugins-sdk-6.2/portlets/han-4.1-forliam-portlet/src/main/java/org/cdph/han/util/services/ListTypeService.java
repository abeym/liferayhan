package org.cdph.han.util.services;

import java.io.Serializable;

import org.cdph.han.util.dto.ListTypeTO;

/**
 * The implementation of this service interface must retrieve the date of the list types of liferay
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public interface ListTypeService extends Serializable{

	/**
	 * Loads all the available values for the given type
	 * @param type to retrieve the list values
	 * @return found values
	 */
	ListTypeTO[] loadListForType (final String type);

}
