package org.cdph.han.util.converters;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.facade.impl.alerts.UserDataServiceImpl;
import org.cdph.han.model.HanUserData;
import org.cdph.han.persistence.UserData;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.Contact;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;

public class UserDataConv {
	private static final Log log = LogFactory.getLog (UserDataConv.class);
	
	public  UserData buildUpUser(HanUserData hud){
		UserData ud = new UserData();
		try {
			
			User luser = UserLocalServiceUtil.getUser(hud.getUserId());
			Contact lc = luser.getContact();
			org.cdph.han.persistence.Contact hc = new org.cdph.han.persistence.Contact();
			ud.setDepartment(hud.getDepartment());
			ud.setEmail(hud.getMailPass());
			ud.setEmailPassword(hud.getMailPass());
			ud.setMir3Id(hud.getMir3_id());
			ud.setUserId(hud.getUserId());
			ud.setScreenName(luser.getScreenName());
			//TODO: not sure of this 
			ud.setActive(luser.getStatus()==0);
			//ud.set(u.getUserUuid());
			hc.setContactId(lc.getContactId());
			//TODO: work through this
			//hc.setEmailAddresses(lc.getEmailAddress());
			hc.setFirstName(lc.getFirstName());
			hc.setJobTitle(lc.getJobTitle());
			hc.setLastName(lc.getLastName());
			hc.setMiddleName(lc.getMiddleName());
			//TODO:need a join (or something) to a HAN table
			//hc.setPhoneDevices(lc.getP);
			ud.setContact(hc);
			
		} catch (PortalException e) {
			log.error(e);
		} catch (SystemException e) {
			log.error(e);
		}catch (Exception e) {
			log.error(e);
		}
		return ud;
	}
}
