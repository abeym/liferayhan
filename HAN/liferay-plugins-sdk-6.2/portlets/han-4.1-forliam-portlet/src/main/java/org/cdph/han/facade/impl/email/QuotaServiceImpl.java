package org.cdph.han.facade.impl.email;

import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.Quota;
import javax.mail.Quota.Resource;
import javax.mail.QuotaAwareStore;
import javax.mail.Session;
import javax.mail.Store;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.alerts.services.UserDataService;
import org.cdph.han.email.dto.QuotaTO;
import org.cdph.han.email.services.EmailAuthenticator;
import org.cdph.han.email.services.QuotaService;
import org.cdph.han.persistence.UserData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//import com.icesoft.util.Properties;

/**
 * Implementation of the service to retrieve the quota of a given user
 * @author Abey Mani
 * @version 1.0
 */
@Service ("quotaService")
public class QuotaServiceImpl implements QuotaService {
	/** Class log */
	private static final Log log = LogFactory.getLog (QuotaServiceImpl.class);
	/** Email properties */
	private final Properties props = new Properties ();
	/** Service to retrieve user information */
	@Autowired
	private UserDataService userDataService;

	/**
	 * Default constructor
	 */
	public QuotaServiceImpl () {
		try {
			props.load (this.getClass ().getResourceAsStream ("/email.properties"));
		} catch (Exception ex) {
			log.fatal ("Error loading email properties.", ex);
		}
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.email.services.QuotaService#getUserQuota(long)
	 */
	@Override
	public QuotaTO getUserQuota (final long userId) throws MessagingException {
		QuotaTO quota = new QuotaTO ();
		final UserData user = userDataService.getUser (userId);
		final Session session = Session.getInstance (props, new EmailAuthenticator (
				user.getScreenName (), user.getEmailPassword ()));
		final Store store = session.getStore ("imap");
		store.connect ();
		if (store instanceof QuotaAwareStore) {
			QuotaAwareStore awareStore = (QuotaAwareStore) store;
			Quota[] quotas = awareStore.getQuota ("INBOX");
			for (Quota q : quotas) {
				for (Resource r : q.resources) {
					if (r.name.equals ("STORAGE")) {
						quota = new QuotaTO (r.limit, r.usage);
						break;
					}
				}
			}
		}
		return quota;
	}

}
