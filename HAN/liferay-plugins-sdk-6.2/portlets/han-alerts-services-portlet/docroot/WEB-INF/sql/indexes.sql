create index IX_3957B56F on Address (classNameId, classPK);

create index IX_2932DD37 on ListType (type_);

create index IX_E89E4F1A on alert_reports (alert_id, notification_report_id);

create index IX_AF9FE820 on alert_topics (alert_locale_id);
create index IX_D67336E8 on alert_topics (alert_locale_id, title);

create index IX_42A22B2F on area_field_catalog (organization_id);

create index IX_41B9103A on hanAddress (classNameId, classPK);

create index IX_2DFDCB22 on han_properties (name);

create index IX_57A066E6 on non_han_user_group (name, description, status);
create index IX_6554AC93 on non_han_user_group (status);

create index IX_C69EDE75 on registration_request (status);
create index IX_B0ECC29F on registration_request (status, userName);

create index IX_D4B38CBD on user_cust_groups (userId, deleted);
create index IX_EC89BC8B on user_cust_groups (userId, name);