package org.cdph.harness;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.User;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.mir3.ws.ErrorType;
import com.mir3.ws.Mir3;
import com.mir3.ws.Mir3Service;
import com.mir3.ws.ResponseType;
import com.mir3.ws.SetEmailAttachmentType;

import org.apache.log4j.Logger;
//import org.cdph.han.alerts.dto.AlertTO;
//import org.cdph.han.alerts.exceptions.PublishAlertException;
import org.cdph.harness.delegate.SendAlertTest;
import org.cdph.harness.delegate.TestAlertsLayer;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.apache.log4j.LogManager;
/**
 * Portlet implementation class MirTests
 */
public class MirTests extends MVCPortlet {
 
	private static Logger _log=LogManager.getLogger(MirTests.class);

	public void sendAlert(ActionRequest req, ActionResponse resp){
		SendAlertTest tst = new SendAlertTest();
		tst.sendAlert();
		_log.info("enter sendAlert");
	}
	
	public void getAlerts(ActionRequest req, ActionResponse resp){
		try {
			User user = PortalUtil.getUser(req);
			TestAlertsLayer tst = new TestAlertsLayer();
			tst.testGetAllAlerts(user);
		} catch (SystemException | PortalException e) {
			System.err.println(e);
		}
		_log.info("enter getAlert");
	}

}
