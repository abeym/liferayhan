/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.cdph.han.service.impl;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.Order;
import com.liferay.portal.kernel.dao.orm.OrderFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.portlet.PortletClassLoaderUtil;

import java.util.ArrayList;
import java.util.List;

import org.cdph.han.model.AreaField;
import org.cdph.han.model.impl.AreaFieldImpl;
import org.cdph.han.service.AreaFieldLocalServiceUtil;
import org.cdph.han.service.base.AreaFieldLocalServiceBaseImpl;

/**
 * The implementation of the area field local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link org.cdph.han.service.AreaFieldLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Choicegen
 * @see org.cdph.han.service.base.AreaFieldLocalServiceBaseImpl
 * @see org.cdph.han.service.AreaFieldLocalServiceUtil
 */
public class AreaFieldLocalServiceImpl extends AreaFieldLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link org.cdph.han.service.AreaFieldLocalServiceUtil} to access the area field local service.
	 */
	/*	public List<AreaField> getAreaFields(Long organizationType) {
		List<org.cdph.han.model.AreaField> ha = new ArrayList<AreaField>();
		try {
		    DynamicQuery areaFieldQuery = DynamicQueryFactoryUtil.forClass(
		    		org.cdph.han.model.AreaField.class, PortletClassLoaderUtil.getClassLoader())
				.add(PropertyFactoryUtil.forName("organization").eq(organizationType));
		    Order order = OrderFactoryUtil.asc("description");
		    areaFieldQuery.addOrder(order);
			ha = AreaFieldLocalServiceUtil.dynamicQuery(areaFieldQuery);
		} catch (SystemException e) {
			System.err.println(e);
		}
		List<AreaField> ha2 = new ArrayList<AreaField>();
		for (org.cdph.han.model.AreaField af : ha) {
			AreaField af2 = new AreaFieldImpl();
			af2.setDescription(af.getDescription());
			ha2.add(af2);
		}
		return ha2;
		return null;
	}*/
	
	public List<AreaField> getAreaFieldsForOrgType(Long organizationType) {
	List<org.cdph.han.model.AreaField> ha = new ArrayList<AreaField>();
	try {
	    DynamicQuery areaFieldQuery = DynamicQueryFactoryUtil.forClass(
	    		org.cdph.han.model.AreaField.class, PortletClassLoaderUtil.getClassLoader())
			.add(PropertyFactoryUtil.forName("organization").eq(organizationType));
	    Order order = OrderFactoryUtil.asc("description");
	    areaFieldQuery.addOrder(order);
		ha = AreaFieldLocalServiceUtil.dynamicQuery(areaFieldQuery);
	} catch (SystemException e) {
		System.err.println(e);
	}
	List<AreaField> ha2 = new ArrayList<AreaField>();
	for (org.cdph.han.model.AreaField af : ha) {
		AreaField af2 = new AreaFieldImpl();
		af2.setAreaFieldId(af.getAreaFieldId());
		af2.setDescription(af.getDescription());
		ha2.add(af2);
	}
	return ha2;
}
	
	public List<Long> getAreaFields1(Long organizationType) {
		List<Long> ha = new ArrayList<Long>();
/*		List<org.cdph.han.model.AreaField> ha = new ArrayList<AreaField>();
		try {
		    DynamicQuery areaFieldQuery = DynamicQueryFactoryUtil.forClass(
		    		org.cdph.han.model.AreaField.class, PortletClassLoaderUtil.getClassLoader())
				.add(PropertyFactoryUtil.forName("organization").eq(organizationType));
		    Order order = OrderFactoryUtil.asc("description");
		    areaFieldQuery.addOrder(order);
			ha = AreaFieldLocalServiceUtil.dynamicQuery(areaFieldQuery);
		} catch (SystemException e) {
			System.err.println(e);
		}
		List<AreaField> ha2 = new ArrayList<AreaField>();
		for (org.cdph.han.model.AreaField af : ha) {
			AreaField af2 = new AreaFieldImpl();
			af2.setDescription(af.getDescription());
			ha2.add(af2);
		}
		return ha2;
		*/return ha;
	}
}
