/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.cdph.han.service.impl;

import com.liferay.portal.kernel.bean.PortalBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.portlet.PortletClassLoaderUtil;
import com.liferay.util.dao.orm.CustomSQLUtil;

import java.util.List;

import org.cdph.han.model.ContactPreference;
import org.cdph.han.service.ContactPreferenceLocalServiceUtil;
import org.cdph.han.service.base.ContactPreferenceLocalServiceBaseImpl;

/**
 * The implementation of the contact preference local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link org.cdph.han.service.ContactPreferenceLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Choicegen
 * @see org.cdph.han.service.base.ContactPreferenceLocalServiceBaseImpl
 * @see org.cdph.han.service.ContactPreferenceLocalServiceUtil
 */
public class ContactPreferenceLocalServiceImpl
	extends ContactPreferenceLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link org.cdph.han.service.ContactPreferenceLocalServiceUtil} to access the contact preference local service.
	 */

	public List<ContactPreference> findByUserId(long userId) {
		List<ContactPreference> contactPrefList = null;
		try {
		    DynamicQuery query = DynamicQueryFactoryUtil.forClass(
		    	ContactPreference.class, PortletClassLoaderUtil.getClassLoader())
				.add(PropertyFactoryUtil.forName("userId").eq(userId));
			contactPrefList = ContactPreferenceLocalServiceUtil.dynamicQuery(query);
		} catch (SystemException e) {
			e.getStackTrace();
		} catch (Exception e) {
			e.getStackTrace();
		}
		return contactPrefList;
	}

	public int findMaxId() {
		List<ContactPreference> contactPrefList = null;
		try {
		    DynamicQuery query = DynamicQueryFactoryUtil.forClass(
			    	ContactPreference.class, "maxQuery", PortletClassLoaderUtil.getClassLoader())
					.setProjection(ProjectionFactoryUtil.max("id"));
			contactPrefList = ContactPreferenceLocalServiceUtil.dynamicQuery(query);
			System.out.println("Max output: " + contactPrefList.get(0));
			String str = String.format("%s", contactPrefList.get(0));
			Long l = Long.parseLong(str);
			return l.intValue();
		} catch (Exception e) {
			System.err.println("ERROR Class not found: " + e);
		}
		return 0;
	}
}