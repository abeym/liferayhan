/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.cdph.han.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.model.AlternateContact;
import org.cdph.han.service.AlternateContactLocalServiceUtil;
import org.cdph.han.service.base.AlternateContactServiceBaseImpl;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.portlet.PortletClassLoaderUtil;

/**
 * The implementation of the alternate contact remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link org.cdph.han.service.AlternateContactService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Choicegen
 * @see org.cdph.han.service.base.AlternateContactServiceBaseImpl
 * @see org.cdph.han.service.AlternateContactServiceUtil
 */
public class AlternateContactServiceImpl extends AlternateContactServiceBaseImpl {
	private static final Log log = LogFactory.getLog (AlternateContactServiceImpl.class);
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link org.cdph.han.service.AlternateContactServiceUtil} to access the alternate contact remote service.
	 */
	public List<AlternateContact> findByUserId(long userId) {
		//log.info("Run Dynamic query of " + AlternateContact.class + " with order of date_published.");

		List<AlternateContact> alternateContactList = new ArrayList<AlternateContact>();
		try {
		    DynamicQuery altContactQuery = DynamicQueryFactoryUtil.forClass(
				AlternateContact.class, PortletClassLoaderUtil.getClassLoader())
				.add(PropertyFactoryUtil.forName("userId").eq(userId));
		    alternateContactList = AlternateContactLocalServiceUtil.dynamicQuery(altContactQuery);
		} catch (SystemException e) {
			e.getStackTrace();
		} catch (Exception e) {
			e.getStackTrace();
		}
		return alternateContactList;
	}
}