/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.cdph.han.service.impl;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.util.Collections;
import java.util.List;

import org.cdph.han.model.NonHanUserGroup;
import org.cdph.han.service.base.NonHanUserGroupLocalServiceBaseImpl;

/**
 * The implementation of the non han user group local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link org.cdph.han.service.NonHanUserGroupLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Choicegen
 * @see org.cdph.han.service.base.NonHanUserGroupLocalServiceBaseImpl
 * @see org.cdph.han.service.NonHanUserGroupLocalServiceUtil
 */
public class NonHanUserGroupLocalServiceImpl
	extends NonHanUserGroupLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link org.cdph.han.service.NonHanUserGroupLocalServiceUtil} to access the non han user group local service.
	 */
	private static final Log LOGGER = LogFactoryUtil.getLog(NonHanUserGroupLocalServiceImpl.class);
	// Select a from NonHanUserGroup a where a.name LIKE :groupName and a.description LIKE :groupDesc and a.status <> :status ORDER BY a.createdDate DESC
	public List<NonHanUserGroup> findByNameDescAndStatus(String name, String desc, String status) {
		List<NonHanUserGroup> nonHanUserGroups = Collections.EMPTY_LIST;
		try {
			nonHanUserGroups = nonHanUserGroupPersistence.findByNameDescStatus(name, desc, status);
		} catch (SystemException e) {
			LOGGER.error(e.getMessage());
		}
		return nonHanUserGroups;
	}
	
	public int findByNameDescAndStatusCount(String name, String desc, String status) {
		int count = 0;
		try {
			count = nonHanUserGroupPersistence.countByNameDescStatus(name, desc, status);
		} catch (SystemException e) {
			LOGGER.error(e.getMessage());
		}
		return count;
	}
	
	public List<NonHanUserGroup> findAllExceptSoftDeleted() {
		List<NonHanUserGroup> nonHanUserGroups = Collections.EMPTY_LIST;
		try {
			nonHanUserGroups = nonHanUserGroupPersistence.findBystatus("4");
		} catch (SystemException e) {
			LOGGER.error(e.getMessage());
		}
		return nonHanUserGroups;
	}
	
	public int getCount() {
		int count = 0;
		try {
			count = nonHanUserGroupPersistence.countBystatus("4");
		} catch (SystemException e) {
			LOGGER.error(e.getMessage());
		}
		return count;
	}
	
}