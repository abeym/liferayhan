/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.cdph.han.service.impl;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.portlet.PortletClassLoaderUtil;

import java.util.List;

import org.cdph.han.model.CustomGroupMember;
import org.cdph.han.model.impl.CustomGroupMemberImpl;
import org.cdph.han.service.CustomGroupMemberLocalServiceUtil;
import org.cdph.han.service.base.CustomGroupMemberLocalServiceBaseImpl;

/**
 * The implementation of the custom group member local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link org.cdph.han.service.CustomGroupMemberLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Choicegen
 * @see org.cdph.han.service.base.CustomGroupMemberLocalServiceBaseImpl
 * @see org.cdph.han.service.CustomGroupMemberLocalServiceUtil
 */
public class CustomGroupMemberLocalServiceImpl
	extends CustomGroupMemberLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link org.cdph.han.service.CustomGroupMemberLocalServiceUtil} to access the custom group member local service.
	 */
	

	public CustomGroupMember add() {
		try {
		    DynamicQuery query = DynamicQueryFactoryUtil.forClass(
		    		CustomGroupMember.class, "maxQuery", PortletClassLoaderUtil.getClassLoader())
					.setProjection(ProjectionFactoryUtil.max("memberId"));
			List<CustomGroupMember> customGroupMemberList = CustomGroupMemberLocalServiceUtil.dynamicQuery(query);
			String str = String.format("%s", customGroupMemberList.get(0));
			Long l = Long.parseLong(str);
			CustomGroupMember cgm = new CustomGroupMemberImpl();
			cgm.setMemberId(l+1);
			System.out.println("Adding Custom Group Member: " + l);
			return CustomGroupMemberLocalServiceUtil.addCustomGroupMember(cgm);
		} catch (Exception e) {
			System.err.println("ERROR Class not found: " + e);
			return null;
		}

	}
	
}