/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.cdph.han.service.impl;

import org.cdph.han.model.UserAreaFieldAsoc;
import org.cdph.han.service.UserAreaFieldAsocLocalServiceUtil;
import org.cdph.han.service.base.UserAreaFieldAsocLocalServiceBaseImpl;

import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the user area field asoc local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link org.cdph.han.service.UserAreaFieldAsocLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Choicegen
 * @see org.cdph.han.service.base.UserAreaFieldAsocLocalServiceBaseImpl
 * @see org.cdph.han.service.UserAreaFieldAsocLocalServiceUtil
 */
public class UserAreaFieldAsocLocalServiceImpl
	extends UserAreaFieldAsocLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link org.cdph.han.service.UserAreaFieldAsocLocalServiceUtil} to access the user area field asoc local service.
	 */
	public org.cdph.han.model.UserAreaFieldAsoc createUserAreaFieldAsoc(long userId,long organizationId){
		//try {
			//long uafaId = counterLocalService.increment(UserAreaFieldAsoc.class.getName());
		//} catch (SystemException e) {
			// TODO Auto-generated catch block
		//	e.printStackTrace();
		//}
		org.cdph.han.service.persistence.UserAreaFieldAsocPK userAreaFieldpk = 
				new org.cdph.han.service.persistence.UserAreaFieldAsocPK(userId, organizationId);
		UserAreaFieldAsoc uafa = userAreaFieldAsocPersistence.create(userAreaFieldpk);
		//org.cdph.han.model.UserAreaFieldAsoc lruserAreaFieldAsoc
		return uafa;
	}
}