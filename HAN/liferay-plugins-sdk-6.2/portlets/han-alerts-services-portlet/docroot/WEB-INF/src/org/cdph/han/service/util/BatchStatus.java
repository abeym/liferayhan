package org.cdph.han.service.util;

public enum BatchStatus {
	QUEUED, PROCESSING, FINISHED, ERROR, SOFT_DELETED;

	public String toString() {
		String value = null;
		switch (this) {
		case QUEUED:
			value = "QUEUED";
			break;
		case PROCESSING:
			value = "PROCESSING";
			break;
		case FINISHED:
			value = "FINISHED";
			break;
		case ERROR:
			value = "ERROR";
			break;
		case SOFT_DELETED:
			value = "SOFT_DELETED";
			break;
		default:
			value = "Undefined";
			break;
		}
		return value;
	}

	public String getStatusImagePath() {
		switch (this) {
		case QUEUED:
			return "batchSearch.row.status.queued";
		case PROCESSING:
			return "batchSearch.row.status.processing";
		case ERROR:
			return "batchSearch.row.status.error";
		case FINISHED:
			return "batchSearch.row.status.finished";
		default:
				return "";
		}
	}
}
