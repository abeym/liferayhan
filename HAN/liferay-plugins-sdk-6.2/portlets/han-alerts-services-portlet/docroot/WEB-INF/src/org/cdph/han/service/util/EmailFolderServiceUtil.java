package org.cdph.han.service.util;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.Folder;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.model.UserData;
import org.cdph.han.service.HanPropertyLocalServiceUtil;
import org.cdph.han.service.UserDataLocalServiceUtil;
import org.cdph.han.service.dto.FolderDetailTO;

/**
 * Implementation of the EmailFolderService
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class EmailFolderServiceUtil {
	/** Class log */
	private static final Log log = LogFactory.getLog (EmailFolderServiceUtil.class);
	/** Domain */
	private static final String HAN_EMAIL_DOMAIN_KEY = "HAN_EMAIL_DOMAIN";
	/** Email properties */
	private final Properties props = new Properties ();
	/** Service used to retrieve the email password */
	//private UserDataService userDataService;
	/** Service used to retrieve han properties */
	//private HanPropertiesService hanPropertiesService;

	/**
	 * Default constructor
	 */
	public EmailFolderServiceUtil () {
		try {
			props.load (this.getClass ().getResourceAsStream ("email.properties"));
		} catch (Exception ex) {
			log.fatal ("Error loading email properties.", ex);
		}
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.email.services.EmailFolderService#createFolder(java.lang.String, java.lang.String)
	 */
	public EmailOperationOutcome createFolder (final long userId, final String folderName) {
		EmailOperationOutcome outcome;
		Store store = null;
		try {
			final Properties props = new Properties (this.props);
			final User user = UserLocalServiceUtil.getUser(userId);
			final UserData hanUserData = UserDataLocalServiceUtil.getUserData(userId);
			props.put ("mail.user", user.getScreenName ());
			props.put ("mail.from", user.getScreenName () + '@'
					+ HanPropertyLocalServiceUtil.getValue (HAN_EMAIL_DOMAIN_KEY));
			final Session session = Session.getInstance (props, 
					new EmailAuthenticator (user.getScreenName (), hanUserData.getEmailPassword ()));
			store = session.getStore ("imap");
			store.connect ();
			final Folder folder = store.getDefaultFolder ().getFolder ("INBOX").getFolder (folderName);
			if (folder.exists ()) {
				outcome = EmailOperationOutcome.FOLDER_ALREADY_EXISTS;
			} else {
				if (folder.create (Folder.HOLDS_MESSAGES)) {
					outcome = EmailOperationOutcome.FOLDER_CREATED;
				} else {
					outcome = EmailOperationOutcome.ERROR_CREATING_FOLDER;
				}
			}
		} catch (Exception ex) {
			log.error ("Error creating user folder.", ex);
			outcome = EmailOperationOutcome.ERROR_CREATING_FOLDER;
		} finally {
			if (store != null) {
				try {
					store.close ();
				} catch (MessagingException ex) {
					log.error ("Error while closing store.", ex);
				}
			}
		}
		return outcome;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.email.services.EmailFolderService#deleteFolder(java.lang.String, java.lang.String)
	 */
	public EmailOperationOutcome deleteFolder (final long userId, final String folderName) {
		EmailOperationOutcome outcome;
		Store store = null;
		try {
			final Properties props = new Properties (this.props);
			final UserData userData = UserDataLocalServiceUtil.getUserData(userId);
			final User user = UserLocalServiceUtil.getUser(userId);
			props.put ("mail.user", user.getScreenName ());
			props.put ("mail.from", user.getScreenName () + '@'
					+ HanPropertyLocalServiceUtil.getValue (HAN_EMAIL_DOMAIN_KEY));
			final Session session = Session.getInstance (props, 
					new EmailAuthenticator (user.getScreenName (), userData.getEmailPassword ()));
			store = session.getStore ("imap");
			store.connect ();
			final Folder folder = store.getFolder (folderName);
			if (folder.exists ()) {
				folder.delete (false);
				outcome = EmailOperationOutcome.FOLDER_DELETED;
			} else {
				outcome = EmailOperationOutcome.FOLDER_DOES_NOT_EXISTS;
			}
		} catch (Exception ex) {
			log.error ("Error deleting user folder.", ex);
			outcome = EmailOperationOutcome.ERROR_DELETING_FOLDER;
		} finally {
			if (store != null) {
				try {
					store.close ();
				} catch (MessagingException ex) {
					log.error ("Error while closing store.", ex);
				}
			}
		}
		return outcome;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.email.services.EmailFolderService#renameFolder(java.lang.String, java.lang.String, java.lang.String)
	 */
	public EmailOperationOutcome renameFolder (final long userId, final String folderName,
			final String newFolderName) {
		EmailOperationOutcome outcome = null;
		Store store = null;
		try {
			final Properties props = new Properties (this.props);
			final UserData userData = UserDataLocalServiceUtil.getUserData(userId);
			final User user = UserLocalServiceUtil.getUser(userId);
			props.put ("mail.user", user.getScreenName ());
			props.put ("mail.from", user.getScreenName () + '@'
					+ HanPropertyLocalServiceUtil.getValue (HAN_EMAIL_DOMAIN_KEY));
			final Session session = Session.getInstance (props, 
					new EmailAuthenticator (user.getScreenName (), userData.getEmailPassword ()));
			store = session.getStore ("imap");
			store.connect ();
			Folder original = store.getFolder (folderName);
			Folder newFolder = store.getDefaultFolder ().getFolder ("INBOX").getFolder (newFolderName);
			if (original.exists ()) {
				if (!newFolder.exists ()) {
					original.renameTo (newFolder);
					outcome = EmailOperationOutcome.FOLDER_RENAMED;
				} else {
					outcome = EmailOperationOutcome.FOLDER_ALREADY_EXISTS;
				}
			} else {
				outcome = EmailOperationOutcome.FOLDER_DOES_NOT_EXISTS;
			}
		} catch (Exception ex) {
			log.error ("Error renaming folder.", ex);
			outcome = EmailOperationOutcome.ERROR_RENAMING_FOLDER;
		} finally {
			if (store != null) {
				try {
					store.close ();
				} catch (MessagingException ex) {
					log.error ("Error closing connection", ex);
				}
			}
		}
		return outcome;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.email.services.EmailFolderService#listFolders(long)
	 */
	public FolderDetailTO[] listFolders (long userId) {
		Store store = null;
		FolderDetailTO[] names = null;
		try {
			final Properties props = new Properties (this.props);
			final UserData userData = UserDataLocalServiceUtil.getUserData(userId);
			final User user = UserLocalServiceUtil.getUser(userId);
			props.put ("mail.user", user.getScreenName ());
			props.put ("mail.from", user.getScreenName () + '@'
					+ HanPropertyLocalServiceUtil.getValue (HAN_EMAIL_DOMAIN_KEY));
			final Session session = Session.getInstance (props, 
					new EmailAuthenticator (user.getScreenName (), userData.getEmailPassword ()));
			store = session.getStore ("imap");
			store.connect ();
			final Folder root = store.getDefaultFolder ();
			final Folder[] folders = root.list ();
			names = new FolderDetailTO[folders.length];
			log.debug ("Folders: " + folders.length);
			for (int i = 0; i < folders.length; i++) {
				names[i] = new FolderDetailTO (folders[i].getName (), folders[i].getFullName (),
						folders[i].getSeparator (), folders[i].getUnreadMessageCount () > 0,
						folders[i].getUnreadMessageCount (), folders[i].getMessageCount (),
						getChildren (folders[i]));
				log.debug ("Name: " + folders[i].getFullName () + " Messages: " + folders[i].getMessageCount ()
						+ " New Messages: " + folders[i].getNewMessageCount () + " Unread: "
						+ folders[i].getUnreadMessageCount ());
			}
		} catch (Exception ex) {
			log.error ("Error retrieving folder names", ex);
		} finally {
			if (store != null) {
				try {
					store.close ();
				} catch (MessagingException ex) {
					log.error ("Error closing connection", ex);
				}
			}
		}
		return names;
	}

	/**
	 * This method retrieves all the folders in the containing folder
	 * @param parent folder
	 * @return sub folders
	 * @throws MessagingException in case of errors while retrieving the folders
	 */
	private FolderDetailTO[] getChildren (final Folder parent) throws MessagingException {
		final List<FolderDetailTO> children = new ArrayList<FolderDetailTO> ();
		final Folder[] subFolders = parent.list ();
		if (subFolders != null) {
			for (Folder folder : subFolders) {
				children.add (new FolderDetailTO (folder.getName (), folder.getFullName (),
						folder.getSeparator (), folder.getUnreadMessageCount () > 0, folder.getUnreadMessageCount (),
						folder.getMessageCount (), getChildren (folder)));
				log.debug ("Name: " + folder.getFullName () + " Messages: " + folder.getMessageCount ()
						+ " New Messages: " + folder.getNewMessageCount () + " Unread: "
						+ folder.getUnreadMessageCount ());
			}
		}
		return children.toArray (new FolderDetailTO[children.size ()]);
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.email.services.EmailFolderService#emptyTrash(long)
	 */
	public void emptyTrash (final long userId) throws MessagingException {
		Store store = null;
		final Properties props = new Properties (this.props);
		UserData userData = null;
		User user = null;
		try {
			userData = UserDataLocalServiceUtil.getUserData(userId);
			user = UserLocalServiceUtil.getUser(userId);
		} catch (PortalException e) {
		   log.error(e.getMessage());
		} catch (SystemException e) {
			log.error(e.getMessage());
		}
		
		props.put ("mail.user", user.getScreenName ());
		props.put ("mail.from", user.getScreenName () + '@'
				+ HanPropertyLocalServiceUtil.getValue (HAN_EMAIL_DOMAIN_KEY));
		final Session session = Session.getInstance (props, 
				new EmailAuthenticator (user.getScreenName (), userData.getEmailPassword ()));
		store = session.getStore ("imap");
		store.connect ();
		final Folder trash = store.getDefaultFolder ().getFolder ("INBOX").getFolder ("Trash");
		trash.open (Folder.READ_WRITE);
		trash.close (true);
	}

}
