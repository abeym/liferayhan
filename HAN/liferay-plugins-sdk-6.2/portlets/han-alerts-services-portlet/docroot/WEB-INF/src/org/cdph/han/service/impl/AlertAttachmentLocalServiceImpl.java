/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.cdph.han.service.impl;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.Order;
import com.liferay.portal.kernel.dao.orm.OrderFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.portlet.PortletClassLoaderUtil;

import java.util.List;

import org.cdph.han.model.Alert;
import org.cdph.han.model.AlertAttachment;
import org.cdph.han.service.AlertAttachmentLocalServiceUtil;
import org.cdph.han.service.AlertLocalServiceUtil;
import org.cdph.han.service.base.AlertAttachmentLocalServiceBaseImpl;

/**
 * The implementation of the alert attachment local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link org.cdph.han.service.AlertAttachmentLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Choicegen
 * @see org.cdph.han.service.base.AlertAttachmentLocalServiceBaseImpl
 * @see org.cdph.han.service.AlertAttachmentLocalServiceUtil
 */
public class AlertAttachmentLocalServiceImpl
	extends AlertAttachmentLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link org.cdph.han.service.AlertAttachmentLocalServiceUtil} to access the alert attachment local service.
	 */

	public List<AlertAttachment> getByAlertId(long alertId)
			throws SystemException {
		List<AlertAttachment> alertList = null;
		try {
		    DynamicQuery publishQuery = DynamicQueryFactoryUtil.forClass(
				AlertAttachment.class, PortletClassLoaderUtil.getClassLoader())
				.add(PropertyFactoryUtil.forName("alertId").eq(alertId));
			alertList = AlertAttachmentLocalServiceUtil.dynamicQuery(publishQuery);
		} catch (SystemException e) {
			e.getStackTrace();
		} catch (Exception e) {
			e.getStackTrace();
		}
		return alertList;
    }
}