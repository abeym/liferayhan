/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.cdph.han.service.impl;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.portlet.PortletClassLoaderUtil;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.model.UserOrganizationalData;
import org.cdph.han.service.UserOrganizationalDataLocalServiceUtil;
import org.cdph.han.service.base.UserOrganizationalDataLocalServiceBaseImpl;

/**
 * The implementation of the user organizational data local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link org.cdph.han.service.UserOrganizationalDataLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Choicegen
 * @see org.cdph.han.service.base.UserOrganizationalDataLocalServiceBaseImpl
 * @see org.cdph.han.service.UserOrganizationalDataLocalServiceUtil
 */
public class UserOrganizationalDataLocalServiceImpl
	extends UserOrganizationalDataLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link org.cdph.han.service.UserOrganizationalDataLocalServiceUtil} to access the user organizational data local service.
	 */
	Log log = LogFactory.getLog(UserOrganizationalDataLocalServiceImpl.class);

	public List<UserOrganizationalData> findByOrganizationId(long orgTypeId) {
		List<UserOrganizationalData> orgList = null;
		try {
		    DynamicQuery orgQuery = DynamicQueryFactoryUtil.forClass(
		    		UserOrganizationalData.class, PortletClassLoaderUtil.getClassLoader())
		    		.add(PropertyFactoryUtil.forName("organizationId").eq(orgTypeId));
		    		;
			orgList = UserOrganizationalDataLocalServiceUtil.dynamicQuery(orgQuery);
		} catch (SystemException se) {log.error("System Error: " + se);
		}
		return orgList;
	}
}