package org.cdph.han.service.dto;

/**
 * Data transfer object containin the information of a folder
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class FolderDetailTO {
	/** Folders name */
	private String folderName;
	/** Folder full path */
	private String folderFullPath;
	/** Path separator */
	private char separator;
	/** Contains new messages */
	private boolean newMessages;
	/** New message quantity */
	private int newMessagesCount;
	/** Total messages */
	private int totalMessages;
	/** The children folders */
	private FolderDetailTO[] children;

	/**
	 * @param folderName folder short name
	 * @param folderFullPath folder full path
	 * @param separator path separator
	 * @param newMessages if the folder contains new messages
	 * @param newMessagesCount the number of new messages
	 * @param totalMessages the total messages in the folder
	 */
	public FolderDetailTO (final String folderName, final String folderFullPath, final char separator,
			final boolean newMessages, final int newMessagesCount, final int totalMessages,
			final FolderDetailTO[] children) {
		this.folderName = folderName;
		this.folderFullPath = folderFullPath;
		this.separator = separator;
		this.newMessages = newMessages;
		this.newMessagesCount = newMessagesCount;
		this.totalMessages = totalMessages;
		this.children = children;
	}

	/**
	 * @return the folderName
	 */
	public String getFolderName () {
		return folderName;
	}

	/**
	 * @return the folderFullPath
	 */
	public String getFolderFullPath () {
		return folderFullPath;
	}

	/**
	 * @return the separator
	 */
	public char getSeparator () {
		return separator;
	}

	/**
	 * @return the newMessages
	 */
	public boolean isNewMessages () {
		return newMessages;
	}

	/**
	 * @return the newMessagesCount
	 */
	public int getNewMessagesCount () {
		return newMessagesCount;
	}

	/**
	 * @return the totalMessages
	 */
	public int getTotalMessages () {
		return totalMessages;
	}

	/**
	 * @return the children
	 */
	public FolderDetailTO[] getChildren () {
		return children;
	}

	/**
	 * @param children the children to set
	 */
	public void setChildren (FolderDetailTO[] children) {
		this.children = children;
	}

}
