package org.cdph.harness.delegate;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.User;

import java.util.List;

import org.cdph.han.model.Alert;
import org.cdph.han.model.AlertAttachment;
import org.cdph.han.model.AlertRecipient;
import org.cdph.han.model.AlertTopic;
import org.cdph.han.model.AreaField;
import org.cdph.han.model.ContactPreference;
import org.cdph.han.model.TypeOrganizationRoles;
import org.cdph.han.service.AlertAttachmentLocalServiceUtil;
import org.cdph.han.service.AlertLocalServiceUtil;
import org.cdph.han.service.AlertRecipientLocalServiceUtil;
import org.cdph.han.service.AlertTopicLocalServiceUtil;
import org.cdph.han.service.AreaFieldLocalServiceUtil;
import org.cdph.han.service.ContactPreferenceLocalServiceUtil;
import org.cdph.han.service.OrganizationalRoleLocalServiceUtil;
import org.cdph.han.service.TypeOrganizationRolesLocalServiceUtil;

public class TestAlertsLayer {
	public void testGetAllAlerts(User user) {
		if (user != null) {
			System.out.println("Test Alerts> User: " + user.getUserId());
			List<Alert> alerts;
			alerts = AlertLocalServiceUtil.findPublishedByDate();
			System.out.println("Alerts PublishByDate> ");
			printAlerts(alerts);
			alerts = AlertLocalServiceUtil.findByRecDate(user.getUserId());	//.getAlerts(1, 100);
			System.out.println("Alerts ByDate> ");
			printAlerts(alerts);

			System.out.println("Alerts ByRecDate> ");
			alerts = AlertLocalServiceUtil.findByRecAction(user.getUserId());
			System.out.println("Alerts ByRecAction> ");
			printAlerts(alerts);

			alerts = AlertLocalServiceUtil.findByAlertRecipients(user.getUserId(), 1);
			System.out.println("Alerts ByRecAction> ");
			printAlerts(alerts);
			alerts = AlertLocalServiceUtil.findByRecIdAction(user.getUserId(), 1);
			System.out.println("Alerts ByRecIdAction> ");
			printAlerts(alerts);
			try {
				alerts = AlertLocalServiceUtil.getAlerts(1, 100);
				System.out.println("Alerts getAlerts> ");
				printAlerts(alerts);
			} catch (SystemException e) {
				e.printStackTrace();
			}
			try {
				List<AlertAttachment> alertAttachments = AlertAttachmentLocalServiceUtil.getByAlertId(32);
				if (alertAttachments != null) {
					for (AlertAttachment alertAttachment : alertAttachments) {
						System.out.println("Attachment> " + alertAttachment.getName() + " / " + alertAttachment.getMimeType());
					}
				} else System.out.println("Alert No Attachments");

				List<AlertRecipient> alertRecipients = AlertRecipientLocalServiceUtil.getByAlertId(1);
				if (alertRecipients != null) {
					for (AlertRecipient alertRecipient : alertRecipients) {
						System.out.println("Recipient> " + alertRecipient);
					}
				} else System.out.println("Alert No Recipients");

				AlertTopic alertTopic = AlertTopicLocalServiceUtil.getAlertTopic(1);
				System.out.println("AlertTest> alertTopic " + alertTopic);
				List<TypeOrganizationRoles> roles = TypeOrganizationRolesLocalServiceUtil.findByOrganizationId(12100l);
				System.out.println("AlertTest> roles " + roles);
			/*	AreaFieldLocalServiceUtil.getAreaFieldsForOrgType(organizationType)
				List<AreaField> areaFields = AreaFieldLocalServiceUtil.getAreaFields(12100L);
				if (areaFields != null) {
					System.out.println("AlertTest> area Fields:" + areaFields.size());
				} else System.out.println("No Area Fields.");
				List<ContactPreference> contactPreferences = ContactPreferenceLocalServiceUtil.findByUserId(14143L);
				if (contactPreferences != null) {
					System.out.println("AlertTest> contact Preferences: " + contactPreferences.size());
				} else System.out.println("No Contact Preferences.");
				System.out.println("Max Contact Preferences: " + ContactPreferenceLocalServiceUtil.findMaxId());
				} else System.out.println("No Area Fields.");*/
			} catch (SystemException | PortalException e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("AlertTest> no user.");
		}
	}

	private void printAlerts(List<Alert> alerts) {
		if (alerts == null) {
			System.out.println("Alert> No alerts returned.");
			return;
		}
		System.out.println("Alert> " + alerts.size() + " alerts returned.");
//		for (Alert alert : alerts) {
//			System.out.println("Alert> " + alert.getName() + " / " + alert.getCategory());
//		}
	}
}
