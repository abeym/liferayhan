package org.cdph.harness.delegate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
//import org.cdph.han.alerts.dto.AlertTO;
//import org.cdph.han.alerts.dto.AlertTO;
//import org.cdph.han.custgroups.dto.RecipientTO;
//import org.cdph.han.custgroups.dto.RecipientType;
//import org.cdph.han.model.UserData;
//import org.cdph.han.alerts.dto.AlertTO;
//import org.cdph.han.persistence.UserData;
import org.cdph.han.service.AlertLocaleServiceUtil;
//import org.cdph.harness.AlertTO;
import org.cdph.harness.MirTests;


import com.mir3.ws.AuthorizationType;
import com.mir3.ws.BroadcastInfoType;
import com.mir3.ws.ErrorType;
import com.mir3.ws.LeaveMessageType;
import com.mir3.ws.Mir3;
import com.mir3.ws.Mir3Service;
import com.mir3.ws.NotificationDetailType;
import com.mir3.ws.NotificationRecipientsDetailsType;
import com.mir3.ws.OneStepNotificationResponseType;
import com.mir3.ws.OneStepNotificationType;
import com.mir3.ws.ResponseType;
import com.mir3.ws.SetEmailAttachmentType;
import com.mir3.ws.StringWithLocale;
import com.mir3.ws.StringWithMessageTypeAndLocale;
import com.mir3.ws.VerbiagePerMessageType;
import com.mir3.ws.VerbiageType;
public class SendAlertTest {
	private static Logger _log=LogManager.getLogger(SendAlertTest.class);
	private static final String API_VERSION_KEY = "MIR3_API_VERSION";
	private Mir3 service;
	public void initService() {
		Mir3Service port = new Mir3Service ();
		service = port.getMir3 ();
		//service.initiateNotificationsOp(arg0)
	}
	public void sendAlert(){
		initService();
		
		sendAlert(stubAlert());
		//service.oneStepNotificationOp(arg0)
		//AlertLocalServiceUtil.
		//NotificationServiceImpl ns = 
	}
	
	public int sendAlert (final AlertTO alert)  {//throws PublishAlertException
		//final Mir3Service mir3Service = new Mir3Service ();
		//final Mir3 service = mir3Service.getMir3 ();
		final ResponseType response = service.oneStepNotificationOp (createOneStepNotification (alert));
		int notificationReportId = 0;
		String notificationReportIds="0";
		if (response.getError () != null && response.getError ().isEmpty ()) {
			try{
			OneStepNotificationResponseType  rsp = response.getOneStepNotificationResponse ();
			notificationReportIds = rsp.getNotificationReportUUID();
			}catch(Exception e){
				_log.error(e);
			}
			//notificationReportId = response.getOneStepNotificationResponse ().getNotificationReportId ();
		} else {
			for (ErrorType error : response.getError ()) {
				_log.error ("Error: " + error.getErrorMessage () + " Error code: " + error.getErrorCode ());
			}
			//throw new PublishAlertException ("Error publishing alert at mir3.");
		}
		//if (alert.getNotificationAttachment () != -1) {// && alert.getAttachments () != null && !alert.getAttachments ().isEmpty ()
			
			//final SetEmailAttachmentType setEmailAttachment = createEmailAttachment (alert);
		//	final ResponseType attachmentResponse = service.setEmailAttachmentOp (setEmailAttachment);
		//	if (!attachmentResponse.getError ().isEmpty ()) {
				//throw new PublishAlertException (attachmentResponse.getError ().get (0).getErrorMessage ()
				//		+ " Error Code: " + attachmentResponse.getError ().get (0).getErrorCode ());
			//}
		//}
		notificationReportId = Integer.parseInt(notificationReportIds);
		return notificationReportId;
	}



/**
 * Creates an object OneStepNotificationType using the data provided by the AlertTO object
 * @param alert AlertTO object with the notification type
 * @return OneStepNotificationType object ready to send a new notification
 */
private OneStepNotificationType createOneStepNotification (final AlertTO alert) {
	final OneStepNotificationType oneStepNotification = new OneStepNotificationType ();
	oneStepNotification.setAuthorization (retrieveAuthorization (alert.getPublishedByUserId ()));//retrieveAuthorization (alert.getPublishedByUserId ())
	oneStepNotification.setApiVersion ("3.6");//hanPropertiesService.getValue (API_VERSION_KEY)
	//oneStepNotification.setDynamicRecipients (createDynamicRecipients (alert));
	oneStepNotification.setNotificationDetail (createNotificationDetail (alert));//createNotificationDetail (alert)
	return oneStepNotification;
}

private AuthorizationType retrieveAuthorization (final long userId) {
	final AuthorizationType authorization = new AuthorizationType ();
	//final UserData userData = userDataService.getUser (userId);
	//UserData userData = new UserData();
	String telephonyPassword = "";
	
	authorization.setPassword ("69Mets41##");//userData.getTelephonyPassword ()
	authorization.setUsername ("Liam.Zanoni");//userData.getEmail ()
	return authorization;
}

private AlertTO stubAlert(){
	AlertTO alert = new AlertTO();
	alert.setTelephonyMessage("han alert 325-1");
	alert.setPublishedByUserId(12l);
	alert.setAlertName("firstAlert1");
	return alert;
}

private NotificationDetailType createNotificationDetail (final AlertTO alert) {
	
	final NotificationDetailType notificationDetail = new NotificationDetailType ();
	//if (alert.getNotificationAttachment () != -1) {
		//notificationDetail.setAttachmentSummaries (createAttachments (alert));
	//}
	//if (alert.getNotificationMethod () == 1) {
		notificationDetail.setBroadcastInfo (createBroadcastInfo (alert));
	/*} else if (alert.getNotificationMethod () == 2) {
		notificationDetail.setFirstResponseInfo (createFirstResponseInfo (alert));
	} else if (alert.getNotificationMethod () == 3) {
		notificationDetail.setCalloutInfo (createCalloutInfo (alert));
	}*/
	notificationDetail.setCallAnalysis (true);
	notificationDetail.setContactAttemptCycles (alert.getContactCycles ());
	notificationDetail.setContactCycleDelay (5);//convertToMinutes (alert.getContactCycleDelay ()) * 60
	//notificationDetail.setLeaveMessage (createLeaveMessage (alert));//
	notificationDetail.setMessage (alert.getTelephonyMessage ());
//	notificationDetail.setOneStepInfo (createOneStepInfo (alert));
	notificationDetail.setPlayGreeting (alert.isPlayGreeting ());
	notificationDetail.setRecordedMessageTitle (alert.getAlertName ());
	notificationDetail.setReplayMessage (alert.isReplayMessages ());
	notificationDetail.setRequiresPin (alert.isRequiresPIN ());
	//notificationDetail.setResponseOptions (createResponseOptions (alert));
	notificationDetail.setSelectLanguage (alert.isSelectLanguage ());
	notificationDetail.setSendToSubscribers (alert.isSendToSubscribers ());
	notificationDetail.setStopIfFullHuman (alert.isStopAfterEntireMessage ());
	notificationDetail.setStopIfFullMachine (alert.isEntireMachine ());
	notificationDetail.setStopIfPartialHuman (alert.isStopAnswerPhone ());
	notificationDetail.setStopIfPartialMachine (alert.isPartialMachine ());
	notificationDetail.setStrictDeviceDelay (true);
	notificationDetail.setTextDeviceDelay (1000);//convertToMinutes (alert.getTextDelay ()) * 60
	notificationDetail.setTitle (alert.getAlertName ());
	notificationDetail.setUseAlternates (alert.isUseAlternates ());
	notificationDetail.setUseTopics (alert.isUseTopics ());
	notificationDetail.setValidateRecipient (alert.isValidateRecipients ());
	//abey - commenting to copmile
	notificationDetail.setVerbiage (createVerbiage (alert));
	//notificationDetail.setLocationOverride (createLocationOverride (alert));
	notificationDetail.setUseAlias (true);
	notificationDetail.setInitiatorAlias ("Chicago Health Alert Network");
	return notificationDetail;
}

private VerbiagePerMessageType  createVerbiage (final AlertTO alert) {
	final VerbiagePerMessageType verbiage = new VerbiagePerMessageType();
	//final VerbiageType verbiage = new VerbiageType ();
	//StringWithLocale string = null;
	StringWithMessageTypeAndLocale string = null;
	if (alert.getVoiceDelivery () == 2) {
		string = new StringWithMessageTypeAndLocale();
		string.setLocale ("en_US");
		string.setValue (alert.getVoiceFile ());
		verbiage.getRecordingTitle().add(string);
	}
	string = new StringWithMessageTypeAndLocale();
	string.setLocale ("en_US");
	string.setValue (alert.getTelephonyMessage ());
	verbiage.getText ().add (string);
	return verbiage;
}


private BroadcastInfoType createBroadcastInfo (AlertTO alert) {
	final BroadcastInfoType broadcastInfoType = new BroadcastInfoType ();
	broadcastInfoType.setBroadcastDuration (3000);//convertToMinutes (alert.getDuration ()) * 60
	broadcastInfoType.setRecipients (createDynamicRecipients (alert));
	return broadcastInfoType;
}

private LeaveMessageType createLeaveMessage (final AlertTO alert) {
	LeaveMessageType leaveMessageType = new LeaveMessageType ();
	leaveMessageType.setCallbackInfo (alert.getLeaveMessage () == 2 || alert.getLeaveMessage () == 4);
	leaveMessageType.setMessage (alert.getLeaveMessage () == 2 || alert.getLeaveMessage () == 3);
	return leaveMessageType;
}

private NotificationRecipientsDetailsType createDynamicRecipients (final AlertTO alert) {
	final NotificationRecipientsDetailsType dynamicRecipients = new NotificationRecipientsDetailsType ();
/*	final Map<RecipientType, List<Long>> ids = new HashMap<RecipientType, List<Long>> ();
	final List<String> emailsUsed = new ArrayList<String> ();
	ids.put (RecipientType.AREA_FIELD, new ArrayList<Long> ());
	ids.put (RecipientType.GROUP, new ArrayList<Long> ());
	ids.put (RecipientType.NON_HAN_GROUP, new ArrayList<Long> ());
	ids.put (RecipientType.NON_HAN_USER, new ArrayList<Long> ());
	ids.put (RecipientType.ORGANIZATION, new ArrayList<Long> ());
	ids.put (RecipientType.ORGANIZATION_TYPE, new ArrayList<Long> ());
	ids.put (RecipientType.ROLE, new ArrayList<Long> ());
	ids.put (RecipientType.USER, new ArrayList<Long> ());
	for (RecipientTO recipient : alert.getRecipients ()) {
		addRecipientToRecipientsDetails (recipient, dynamicRecipients, ids, alert, emailsUsed);
	}*/
	//validateRecipients (dynamicRecipients);
	return dynamicRecipients;
}


}


