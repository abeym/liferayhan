package org.cdph.harness.delegate;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

//import org.cdph.han.custgroups.dto.RecipientTO;

/**
 * Transfer object used to send data between the persistence layer and the presentation layer.
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class AlertTO implements Serializable {
	/** Class serial version UID */
	private static final long serialVersionUID = 963275035068727186L;
	/** ID of the alert if already saved */
	private Long alertId;
	/** Name of the alert */
	private String alertName;
	/** Duration of the alert */
	private String duration;
	/** Priority of the alert */
	private String priority;
	/** Date when the alert was created */
	private Date createdDate;
	/** User ID of the author of the alert */
	private long authorUserId;
	/** User ID of the user that published the alert */
	private Long publishedByUserId;
	/** Date when the alert was published */
	private Date publishedDate;
	/** Last modification date */
	private Date lastModifiedDate;
	/** Date when the notification was sent */
	private Date notificationDate;
	/** ID of the user to contact */
	private long contactUserId;
	/** Date of the incident */
	private Date incidentDate;
	/** Abstract of the alert */
	private String abstractText;
	/** Alert full details */
	private String fullDetailText;
	/** List of attachments */
	//private List<FileTO> attachments;
	/** Flag for scheduling an alert or posting it immediately */
	private boolean scheduled;
	/** Date the alert is scheduled */
	private Date scheduledDate;
	/** Date the alert is going to expire */
	private Date expirationDate;
	/** Id of the topic selected */
	private Long topicId;
	/** List of recipients */
	//private List<RecipientTO> recipients;
	/** Validate that the recipient received the message */
	private boolean validateRecipients;
	/** Override Location */
	private boolean locationOverride;
	/** Replay the message */
	private boolean replayMessages;
	/** One text contact */
	private boolean oneTextContact;
	/** Requires PIN to listen the message */
	private boolean requiresPIN;
	/** Confirm the recipient response */
	private boolean confirmResponse;
	/** Send report to recipients */
	private boolean reportRecipients;
	/** Play the greeting */
	private boolean playGreeting;
	/** Use alternates */
	private boolean useAlternates;
	/** Ability to select the language */
	private boolean selectLanguage;
	/** Use mir3 categories, severities and priorities */
	private boolean useTopics;
	/** Send the message to the subscribed users */
	private boolean sendToSubscribers;
	/** Stop contacting recipient if listened to entire message */
	private boolean stopAfterEntireMessage;
	/** Stop contacting recipient if answered the phone */
	private boolean stopAnswerPhone;
	/** Stop contacting recipient if all message was recorded in the answering machine */
	private boolean entireMachine;
	/** Stop contacting recipient if part of the message was recorded in the answering machine */
	private boolean partialMachine;
	/** Time between contact attempts */
	private String contactCycleDelay;
	/** Time to wait for a text response */
	private String textDelay;
	/** Number of contact attempts */
	private Integer contactCycles;
	/** Expedited delivery */
	private boolean expeditedDelivery;
	/** Option to leave a message, message and call back info, call back info or none */
	private Integer leaveMessage;
	/** Voice delivery method */
	private int voiceDelivery;
	/** List of options */
	//private List<OptionTO> responseOptions;
	/** Method of notification */
	private int notificationMethod;
	/** Attachment to send with the notification */
	private Integer notificationAttachment;
	/** Telephony message */
	private String telephonyMessage;
	/** The name of the author of the alert */
	private String authorName;
	/** Title of the topic selected */
	private String topic;
	/** Name of the recorded file */
	private String voiceFile;
	/** Flag for archived alerts */
	private boolean archived;
	/** List containing the device override */
	private List<Long> locationOverridePriorities;
	/** Override default status only */
	private boolean overrideDefaultStatus;
	/** Marks the alert as ready to publish */
	private boolean readyToPublish;

	/**
	 * @return the alertId
	 */
	public Long getAlertId () {
		return alertId;
	}

	/**
	 * @param alertId the alertId to set
	 */
	public void setAlertId (Long alertId) {
		this.alertId = alertId;
	}

	/**
	 * @return the alertName
	 */
	public String getAlertName () {
		return alertName;
	}

	/**
	 * @param alertName the alertName to set
	 */
	public void setAlertName (String alertName) {
		this.alertName = alertName;
	}

	/**
	 * @return the duration
	 */
	public String getDuration () {
		return duration;
	}

	/**
	 * @param duration the duration to set
	 */
	public void setDuration (String duration) {
		this.duration = duration;
	}

	/**
	 * @return the priority
	 */
	public String getPriority () {
		return priority;
	}

	/**
	 * @param priority the priority to set
	 */
	public void setPriority (String priority) {
		this.priority = priority;
	}

	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate () {
		return createdDate;
	}

	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate (Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the authorUserId
	 */
	public long getAuthorUserId () {
		return authorUserId;
	}

	/**
	 * @param authorUserId the authorUserId to set
	 */
	public void setAuthorUserId (long authorUserId) {
		this.authorUserId = authorUserId;
	}

	/**
	 * @return the publishedByUserId
	 */
	public Long getPublishedByUserId () {
		return publishedByUserId;
	}

	/**
	 * @param publishedByUserId the publishedByUserId to set
	 */
	public void setPublishedByUserId (Long publishedByUserId) {
		this.publishedByUserId = publishedByUserId;
	}

	/**
	 * @return the publishedDate
	 */
	public Date getPublishedDate () {
		return publishedDate;
	}

	/**
	 * @param publishedDate the publishedDate to set
	 */
	public void setPublishedDate (Date publishedDate) {
		this.publishedDate = publishedDate;
	}

	/**
	 * @return the lastModifiedDate
	 */
	public Date getLastModifiedDate () {
		return lastModifiedDate;
	}

	/**
	 * @param lastModifiedDate the lastModifiedDate to set
	 */
	public void setLastModifiedDate (Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	/**
	 * @return the notificationDate
	 */
	public Date getNotificationDate () {
		return notificationDate;
	}

	/**
	 * @param notificationDate the notificationDate to set
	 */
	public void setNotificationDate (Date notificationDate) {
		this.notificationDate = notificationDate;
	}

	/**
	 * @return the contactUserId
	 */
	public long getContactUserId () {
		return contactUserId;
	}

	/**
	 * @param contactUserId the contactUserId to set
	 */
	public void setContactUserId (long contactUserId) {
		this.contactUserId = contactUserId;
	}

	/**
	 * @return the incidentDate
	 */
	public Date getIncidentDate () {
		return incidentDate;
	}

	/**
	 * @param incidentDate the incidentDate to set
	 */
	public void setIncidentDate (Date incidentDate) {
		this.incidentDate = incidentDate;
	}

	/**
	 * @return the abstractText
	 */
	public String getAbstractText () {
		return abstractText;
	}

	/**
	 * @param abstractText the abstractText to set
	 */
	public void setAbstractText (String abstractText) {
		this.abstractText = abstractText;
	}

	/**
	 * @return the fullDetailText
	 */
	public String getFullDetailText () {
		return fullDetailText;
	}

	/**
	 * @param fullDetailText the fullDetailText to set
	 */
	public void setFullDetailText (String fullDetailText) {
		this.fullDetailText = fullDetailText;
	}

	/**
	 * @return the attachments
	 */
/*	public List<FileTO> getAttachments () {
		return attachments;
	}*/

	/**
	 * @param attachments the attachments to set
	 */
/*	public void setAttachments (List<FileTO> attachments) {
		this.attachments = attachments;
	}*/

	/**
	 * @return the scheduled
	 */
	public boolean isScheduled () {
		return scheduled;
	}

	/**
	 * @param scheduled the scheduled to set
	 */
	public void setScheduled (boolean scheduled) {
		this.scheduled = scheduled;
	}

	/**
	 * @return the scheduledDate
	 */
	public Date getScheduledDate () {
		return scheduledDate;
	}

	/**
	 * @param scheduledDate the scheduledDate to set
	 */
	public void setScheduledDate (Date scheduledDate) {
		this.scheduledDate = scheduledDate;
	}

	/**
	 * @return the expirationDate
	 */
	public Date getExpirationDate () {
		return expirationDate;
	}

	/**
	 * @param expirationDate the expirationDate to set
	 */
	public void setExpirationDate (Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	/**
	 * @return the topicId
	 */
	public Long getTopicId () {
		return topicId;
	}

	/**
	 * @param topicId the topicId to set
	 */
	public void setTopicId (Long topicId) {
		this.topicId = topicId;
	}

	/**
	 * @return the recipients
	 */
/*	public List<RecipientTO> getRecipients () {
		return recipients;
	}*/

	/**
	 * @param recipients the recipients to set
	 */
/*	public void setRecipients (List<RecipientTO> recipients) {
		this.recipients = recipients;
	}*/

	/**
	 * @return the validateRecipients
	 */
	public boolean isValidateRecipients () {
		return validateRecipients;
	}

	/**
	 * @param validateRecipients the validateRecipients to set
	 */
	public void setValidateRecipients (boolean validateRecipients) {
		this.validateRecipients = validateRecipients;
	}

	/**
	 * @return the locationOverride
	 */
	public boolean isLocationOverride () {
		return locationOverride;
	}

	/**
	 * @param locationOverride the locationOverride to set
	 */
	public void setLocationOverride (boolean locationOverride) {
		this.locationOverride = locationOverride;
	}

	/**
	 * @return the replayMessages
	 */
	public boolean isReplayMessages () {
		return replayMessages;
	}

	/**
	 * @param replayMessages the replayMessages to set
	 */
	public void setReplayMessages (boolean replayMessages) {
		this.replayMessages = replayMessages;
	}

	/**
	 * @return the oneTextContact
	 */
	public boolean isOneTextContact () {
		return oneTextContact;
	}

	/**
	 * @param oneTextContact the oneTextContact to set
	 */
	public void setOneTextContact (boolean oneTextContact) {
		this.oneTextContact = oneTextContact;
	}

	/**
	 * @return the requiresPIN
	 */
	public boolean isRequiresPIN () {
		return requiresPIN;
	}

	/**
	 * @param requiresPIN the requiresPIN to set
	 */
	public void setRequiresPIN (boolean requiresPIN) {
		this.requiresPIN = requiresPIN;
	}

	/**
	 * @return the confirmResponse
	 */
	public boolean isConfirmResponse () {
		return confirmResponse;
	}

	/**
	 * @param confirmResponse the confirmResponse to set
	 */
	public void setConfirmResponse (boolean confirmResponse) {
		this.confirmResponse = confirmResponse;
	}

	/**
	 * @return the reportRecipients
	 */
	public boolean isReportRecipients () {
		return reportRecipients;
	}

	/**
	 * @param reportRecipients the reportRecipients to set
	 */
	public void setReportRecipients (boolean reportRecipients) {
		this.reportRecipients = reportRecipients;
	}

	/**
	 * @return the playGreeting
	 */
	public boolean isPlayGreeting () {
		return playGreeting;
	}

	/**
	 * @param playGreeting the playGreeting to set
	 */
	public void setPlayGreeting (boolean playGreeting) {
		this.playGreeting = playGreeting;
	}

	/**
	 * @return the useAlternates
	 */
	public boolean isUseAlternates () {
		return useAlternates;
	}

	/**
	 * @param useAlternates the useAlternates to set
	 */
	public void setUseAlternates (boolean useAlternates) {
		this.useAlternates = useAlternates;
	}

	/**
	 * @return the selectLanguage
	 */
	public boolean isSelectLanguage () {
		return selectLanguage;
	}

	/**
	 * @param selectLanguage the selectLanguage to set
	 */
	public void setSelectLanguage (boolean selectLanguage) {
		this.selectLanguage = selectLanguage;
	}

	/**
	 * @return the useTopics
	 */
	public boolean isUseTopics () {
		return useTopics;
	}

	/**
	 * @param useTopics the useTopics to set
	 */
	public void setUseTopics (boolean useTopics) {
		this.useTopics = useTopics;
	}

	/**
	 * @return the sendToSubscribers
	 */
	public boolean isSendToSubscribers () {
		return sendToSubscribers;
	}

	/**
	 * @param sendToSubscribers the sendToSubscribers to set
	 */
	public void setSendToSubscribers (boolean sendToSubscribers) {
		this.sendToSubscribers = sendToSubscribers;
	}

	/**
	 * @return the stopAfterEntireMessage
	 */
	public boolean isStopAfterEntireMessage () {
		return stopAfterEntireMessage;
	}

	/**
	 * @param stopAfterEntireMessage the stopAfterEntireMessage to set
	 */
	public void setStopAfterEntireMessage (boolean stopAfterEntireMessage) {
		this.stopAfterEntireMessage = stopAfterEntireMessage;
	}

	/**
	 * @return the stopAnswerPhone
	 */
	public boolean isStopAnswerPhone () {
		return stopAnswerPhone;
	}

	/**
	 * @param stopAnswerPhone the stopAnswerPhone to set
	 */
	public void setStopAnswerPhone (boolean stopAnswerPhone) {
		this.stopAnswerPhone = stopAnswerPhone;
	}

	/**
	 * @return the entireMachine
	 */
	public boolean isEntireMachine () {
		return entireMachine;
	}

	/**
	 * @param entireMachine the entireMachine to set
	 */
	public void setEntireMachine (boolean entireMachine) {
		this.entireMachine = entireMachine;
	}

	/**
	 * @return the partialMachine
	 */
	public boolean isPartialMachine () {
		return partialMachine;
	}

	/**
	 * @param partialMachine the partialMachine to set
	 */
	public void setPartialMachine (boolean partialMachine) {
		this.partialMachine = partialMachine;
	}

	/**
	 * @return the contactCycleDelay
	 */
	public String getContactCycleDelay () {
		return contactCycleDelay;
	}

	/**
	 * @param contactCycleDelay the contactCycleDelay to set
	 */
	public void setContactCycleDelay (String contactCycleDelay) {
		this.contactCycleDelay = contactCycleDelay;
	}

	/**
	 * @return the textDelay
	 */
	public String getTextDelay () {
		return textDelay;
	}

	/**
	 * @param textDelay the textDelay to set
	 */
	public void setTextDelay (String textDelay) {
		this.textDelay = textDelay;
	}

	/**
	 * @return the contactCycles
	 */
	public Integer getContactCycles () {
		return contactCycles;
	}

	/**
	 * @param contactCycles the contactCycles to set
	 */
	public void setContactCycles (Integer contactCycles) {
		this.contactCycles = contactCycles;
	}

	/**
	 * @return the expeditedDelivery
	 */
	public boolean isExpeditedDelivery () {
		return expeditedDelivery;
	}

	/**
	 * @param expeditedDelivery the expeditedDelivery to set
	 */
	public void setExpeditedDelivery (boolean expeditedDelivery) {
		this.expeditedDelivery = expeditedDelivery;
	}

	/**
	 * @return the leaveMessage
	 */
	public Integer getLeaveMessage () {
		return leaveMessage;
	}

	/**
	 * @param leaveMessage the leaveMessage to set
	 */
	public void setLeaveMessage (Integer leaveMessage) {
		this.leaveMessage = leaveMessage;
	}

	/**
	 * @return the voiceDelivery
	 */
	public int getVoiceDelivery () {
		return voiceDelivery;
	}

	/**
	 * @param voiceDelivery the voiceDelivery to set
	 */
	public void setVoiceDelivery (int voiceDelivery) {
		this.voiceDelivery = voiceDelivery;
	}

	/**
	 * @return the responseOptions
	 */
/*	public List<OptionTO> getResponseOptions () {
		return responseOptions;
	}*/

	/**
	 * @param responseOptions the responseOptions to set
	 */
	/*public void setResponseOptions (List<OptionTO> responseOptions) {
		this.responseOptions = responseOptions;
	}*/

	/**
	 * @return the notificationMethod
	 */
	public int getNotificationMethod () {
		return notificationMethod;
	}

	/**
	 * @param notificationMethod the notificationMethod to set
	 */
	public void setNotificationMethod (int notificationMethod) {
		this.notificationMethod = notificationMethod;
	}

	/**
	 * @return the notificationAttachment
	 */
	public Integer getNotificationAttachment () {
		return notificationAttachment;
	}

	/**
	 * @param notificationAttachment the notificationAttachment to set
	 */
	public void setNotificationAttachment (Integer notificationAttachment) {
		this.notificationAttachment = notificationAttachment;
	}

	/**
	 * @return the telephonyMessage
	 */
	public String getTelephonyMessage () {
		return telephonyMessage;
	}

	/**
	 * @param telephonyMessage the telephonyMessage to set
	 */
	public void setTelephonyMessage (String telephonyMessage) {
		this.telephonyMessage = telephonyMessage;
	}

	/**
	 * @return the authorName
	 */
	public String getAuthorName () {
		return authorName;
	}

	/**
	 * @param authorName the authorName to set
	 */
	public void setAuthorName (String authorName) {
		this.authorName = authorName;
	}

	/**
	 * @return the topic
	 */
	public String getTopic () {
		return topic;
	}

	/**
	 * @param topic the topic to set
	 */
	public void setTopic (String topic) {
		this.topic = topic;
	}

	/**
	 * @return the voiceFile
	 */
	public String getVoiceFile () {
		return voiceFile;
	}

	/**
	 * @param voiceFile the voiceFile to set
	 */
	public void setVoiceFile (String voiceFile) {
		this.voiceFile = voiceFile;
	}

	/**
	 * @return the archived
	 */
	public boolean isArchived () {
		return archived;
	}

	/**
	 * @param archived the archived to set
	 */
	public void setArchived (boolean archived) {
		this.archived = archived;
	}

	/**
	 * @return the locationOverridePriorities
	 */
	public List<Long> getLocationOverridePriorities () {
		return locationOverridePriorities;
	}

	/**
	 * @param locationOverridePriorities the locationOverridePriorities to set
	 */
	public void setLocationOverridePriorities (List<Long> locationOverridePriorities) {
		this.locationOverridePriorities = locationOverridePriorities;
	}

	/**
	 * @return the overrideDefaultStatus
	 */
	public boolean isOverrideDefaultStatus () {
		return overrideDefaultStatus;
	}

	/**
	 * @param overrideDefaultStatus the overrideDefaultStatus to set
	 */
	public void setOverrideDefaultStatus (boolean overrideDefaultStatus) {
		this.overrideDefaultStatus = overrideDefaultStatus;
	}

	/**
	 * @return the readyToPublish
	 */
	public boolean isReadyToPublish () {
		return readyToPublish;
	}

	/**
	 * @param readyToPublish the readyToPublish to set
	 */
	public void setReadyToPublish (boolean readyToPublish) {
		this.readyToPublish = readyToPublish;
	}

}
