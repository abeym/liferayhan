/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.cdph.han.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;

/**
 * @author Choicegen
 */
public class AlertFinderUtil {
	public static java.util.List<org.cdph.han.model.Alert> findDrafts() {
		return getFinder().findDrafts();
	}

	public static java.util.List<org.cdph.han.model.Alert> findPublishedByDate() {
		return getFinder().findPublishedByDate();
	}

	public static java.util.List<org.cdph.han.model.Alert> findPublishedByTopic() {
		return getFinder().findPublishedByTopic();
	}

	public static java.util.List<org.cdph.han.model.Alert> findPublishedByLevel() {
		return getFinder().findPublishedByLevel();
	}

	public static java.util.List<org.cdph.han.model.Alert> findScheduled() {
		return getFinder().findScheduled();
	}

	public static java.util.List<org.cdph.han.model.Alert> findByAlertRecipient(
		long alertId, long userId) {
		return getFinder().findByAlertRecipient(alertId, userId);
	}

	public static java.util.List<org.cdph.han.model.Alert> findByReqAction(
		long userId) {
		return getFinder().findByReqAction(userId);
	}

	public static java.util.List<org.cdph.han.model.Alert> findByRecDate(
		long userId) {
		return getFinder().findByRecDate(userId);
	}

	public static java.util.List<org.cdph.han.model.Alert> findByRecTopic(
		long userId) {
		return getFinder().findByRecTopic(userId);
	}

	public static java.util.List<org.cdph.han.model.Alert> findByRecLevel(
		long userId) {
		return getFinder().findByRecLevel(userId);
	}

	public static java.util.List<org.cdph.han.model.Alert> findByIDReqAction(
		long userId, long alertId) {
		return getFinder().findByIDReqAction(userId, alertId);
	}

	public static AlertFinder getFinder() {
		if (_finder == null) {
			_finder = (AlertFinder)PortletBeanLocatorUtil.locate(org.cdph.han.service.ClpSerializer.getServletContextName(),
					AlertFinder.class.getName());

			ReferenceRegistry.registerReference(AlertFinderUtil.class, "_finder");
		}

		return _finder;
	}

	public void setFinder(AlertFinder finder) {
		_finder = finder;

		ReferenceRegistry.registerReference(AlertFinderUtil.class, "_finder");
	}

	private static AlertFinder _finder;
}