/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.cdph.han.service.persistence;

/**
 * @author Choicegen
 */
public interface AlertFinder {
	public java.util.List<org.cdph.han.model.Alert> findDrafts();

	public java.util.List<org.cdph.han.model.Alert> findPublishedByDate();

	public java.util.List<org.cdph.han.model.Alert> findPublishedByTopic();

	public java.util.List<org.cdph.han.model.Alert> findPublishedByLevel();

	public java.util.List<org.cdph.han.model.Alert> findScheduled();

	public java.util.List<org.cdph.han.model.Alert> findByAlertRecipient(
		long alertId, long userId);

	public java.util.List<org.cdph.han.model.Alert> findByReqAction(long userId);

	public java.util.List<org.cdph.han.model.Alert> findByRecDate(long userId);

	public java.util.List<org.cdph.han.model.Alert> findByRecTopic(long userId);

	public java.util.List<org.cdph.han.model.Alert> findByRecLevel(long userId);

	public java.util.List<org.cdph.han.model.Alert> findByIDReqAction(
		long userId, long alertId);
}