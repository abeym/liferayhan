package org.cdph.han.alerts.dto;

import java.io.Serializable;

/**
 * This data transfer object combines the Recipient with each of the Devices used for contact, this object is
 * used for grouping using ICE Faces capabilities
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class ReportRecipientDeviceTO implements Serializable {
	/** Class serial version UID */
	private static final long serialVersionUID = 7722929259991127537L;
	/** The recipient */
	private ReportRecipientTO recipient;
	/** The device */
	private ReportDeviceTO device;
	/** The outcome of the contact */
	private String contactResult;

	/**
	 * @return the recipient
	 */
	public ReportRecipientTO getRecipient () {
		return recipient;
	}

	/**
	 * @return the device
	 */
	public ReportDeviceTO getDevice () {
		return device;
	}

	/**
	 * The constructor for the object
	 */
	public ReportRecipientDeviceTO (final ReportRecipientTO recipient, final ReportDeviceTO device) {
		this.recipient = recipient;
		this.device = device;
	}

	/**
	 * @return the contactResult
	 */
	public String getContactResult () {
		return contactResult;
	}

	/**
	 * @param contactResult the contactResult to set
	 */
	public void setContactResult (String contactResult) {
		this.contactResult = contactResult;
	}

}
