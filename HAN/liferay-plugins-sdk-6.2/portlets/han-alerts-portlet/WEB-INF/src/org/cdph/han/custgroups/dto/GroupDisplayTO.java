package org.cdph.han.custgroups.dto;

import java.util.Date;

/**
 * Class representing a bulk upload list
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class GroupDisplayTO {
	/** Id of the bulk upload list */
	private long id;
	/** Name of the bulk upload list */
	private String name;
	/** Name of the creator of the bulk upload list */
	private String createdBy;
	/** Date of creation */
	private Date dateCreated;
	/** Determines if the list have been selected */
	private boolean selected;
	/** Name of the user that modified the group if any */
	private String modifiedBy;
	/** Modification date if any */
	private Date dateModified;

	/**
	 * Default constructor
	 */
	public GroupDisplayTO () {}

	/**
	 * Constructor that sets all fields
	 * @param bulkUploadId id of the bulk upload
	 * @param name of the bulk upload
	 * @param createdBy name of the creator of the bulk upload
	 * @param dateCreated date of the creation of the bulk upload
	 */
	public GroupDisplayTO (final long id, final String name, final String createdBy,
			final Date dateCreated) {
		this.id = id;
		this.name = name;
		this.createdBy = createdBy;
		this.dateCreated = dateCreated;
	}

	/**
	 * Constructor for user groups
	 * @param id of the group
	 * @param name of the group
	 * @param createdBy name of the user that created the group
	 * @param dateCreated date of the group creation
	 * @param modifiedBy name of the user that modified the group
	 * @param dateModified date when the group was modified
	 */
	public GroupDisplayTO (final long id, final String name, final String createdBy,
			final Date dateCreated, final String modifiedBy, final Date dateModified) {
		this.id = id;
		this.name = name;
		this.createdBy = createdBy;
		this.dateCreated = dateCreated;
		this.modifiedBy = modifiedBy;
		this.dateModified = dateModified;
	}

	/**
	 * @return the bulkUploadId
	 */
	public long getId () {
		return id;
	}

	/**
	 * @return the name
	 */
	public String getName () {
		return name;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy () {
		return createdBy;
	}

	/**
	 * @return the dateCreated
	 */
	public Date getDateCreated () {
		return dateCreated;
	}

	/**
	 * @param bulkUploadId the bulkUploadId to set
	 */
	public void setId (long bulkUploadId) {
		this.id = bulkUploadId;
	}

	/**
	 * @param name the name to set
	 */
	public void setName (String name) {
		this.name = name;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy (String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @param dateCreated the dateCreated to set
	 */
	public void setDateCreated (Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	/**
	 * @return the selected
	 */
	public boolean isSelected () {
		return selected;
	}

	/**
	 * @param selected the selected to set
	 */
	public void setSelected (boolean selected) {
		this.selected = selected;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy () {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy (String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the dateModified
	 */
	public Date getDateModified () {
		return dateModified;
	}

	/**
	 * @param dateModified the dateModified to set
	 */
	public void setDateModified (Date dateModified) {
		this.dateModified = dateModified;
	}

}
