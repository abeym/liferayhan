package org.cdph.han.util.asynch.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.cdph.han.util.asynch.services.AsynchRequestType;

/**
 * This transfer object contains the information needed to perform an asynchronous query
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class AsynchRequestTO implements Serializable {
	/** Class serial verision UID */
	private static final long serialVersionUID = 3039264933825763493L;
	/** Group ID to send this request response */
	private String groupId;
	/** Map with the parameters of the request */
	private Map<String, Serializable> request;
	/** The type of request */
	private AsynchRequestType requestType;
	/** ID to use as correlation ID */
	private String correlationID;

	/**
	 * Constructor that only sets an external groupId and creates a generic map
	 * @param groupId to send the response
	 */
	public AsynchRequestTO (final String groupId, final AsynchRequestType requestType, final String correlationID) {
		this.groupId = groupId;
		this.requestType = requestType;
		this.correlationID = correlationID;
		request = new HashMap<String, Serializable> ();
	}

	/**
	 * Constructor to set the group id and the request map
	 * @param groupId to send the response
	 * @param request map containing the request parameters
	 */
	public AsynchRequestTO (final String groupId, final AsynchRequestType requestType,
			final Map<String, Serializable> request, final String correlationID) {
		this.groupId = groupId;
		this.requestType = requestType;
		this.request = request;
		this.correlationID = correlationID;
	}

	/**
	 * @return the groupId
	 */
	public String getGroupId () {
		return groupId;
	}

	/**
	 * @return the request
	 */
	public Map<String, Serializable> getRequest () {
		return request;
	}

	/**
	 * @return the requestType
	 */
	public AsynchRequestType getRequestType () {
		return requestType;
	}

	/**
	 * @return the correlationID
	 */
	public String getCorrelationID () {
		return correlationID;
	}

}
