package org.cdph.han.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Entity representing a message in the HAN system
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Entity
@Table (name = "alert_messages", schema = "lportal")
public class AlertMessage implements Serializable, AlertMessageModel {
	/** Class serial version UID */
	private static final long serialVersionUID = -8728304098679857439L;
	/** ID of the message */
	@Id
	@Column (name = "id")
	@GeneratedValue (strategy = GenerationType.TABLE)
	private long id;
	/** Alert to which this message belongs */
	@ManyToOne
	@JoinColumn (name = "alert_id", nullable = false)
	private Alert alert;
	/** Alert locale */
	@ManyToOne
	@JoinColumn (name = "alert_locale_id", nullable = false)
	private AlertLocale locale;
	/** Attachment for the telephony message */
	@OneToOne
	@JoinColumn (name = "alert_attachment_id", nullable = true)
	private AlertAttachment attachment;
	/** Telephony message */
	@Column (name = "telephony_message", nullable = true, length = 3950)
	private String telephonyMessage;
	/** Message to be displayed in the portal */
	@Lob
	@Column (name = "portal_message", nullable = true, length = 65535)
	private String portalMessage;
	/** Abstract of the alert */
	@Lob
	@Column (name = "abstract", nullable = true, length = 65535)
	private String abstractText;

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean equal = false;
		if (obj != null && obj instanceof AlertMessage) {
			final AlertMessageModel other = (AlertMessageModel) obj;
			equal = other.getId () == id;
		}
		return equal;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		return Long.valueOf (id).hashCode ();
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertMessageModel#getId()
	 */
	public long getId () {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId (long id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertMessageModel#getAlert()
	 */
	public Alert getAlert () {
		return alert;
	}

	/**
	 * @param alert the alert to set
	 */
	public void setAlert (Alert alert) {
		this.alert = alert;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertMessageModel#getLocale()
	 */
	public AlertLocale getLocale () {
		return locale;
	}

	/**
	 * @param locale the locale to set
	 */
	public void setLocale (AlertLocale locale) {
		this.locale = locale;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertMessageModel#getAttachment()
	 */
	public AlertAttachment getAttachment () {
		return attachment;
	}

	/**
	 * @param attachment the attachment to set
	 */
	public void setAttachment (AlertAttachment attachment) {
		this.attachment = attachment;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertMessageModel#getTelephonyMessage()
	 */
	public String getTelephonyMessage () {
		return telephonyMessage;
	}

	/**
	 * @param telephonyMessage the telephonyMessage to set
	 */
	public void setTelephonyMessage (String telephonyMessage) {
		this.telephonyMessage = telephonyMessage;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertMessageModel#getPortalMessage()
	 */
	public String getPortalMessage () {
		return portalMessage;
	}

	/**
	 * @param portalMessage the portalMessage to set
	 */
	public void setPortalMessage (String portalMessage) {
		this.portalMessage = portalMessage;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertMessageModel#getAbstractText()
	 */
	public String getAbstractText () {
		return abstractText;
	}

	/**
	 * @param abstractText the abstractText to set
	 */
	public void setAbstractText (String abstractText) {
		this.abstractText = abstractText;
	}

}
