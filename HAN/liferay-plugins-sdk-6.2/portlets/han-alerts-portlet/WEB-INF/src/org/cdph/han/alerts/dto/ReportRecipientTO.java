package org.cdph.han.alerts.dto;

import java.io.Serializable;
import java.util.List;

/**
 * This data transfer object class represents a recipient in the reports provided by mir3 API
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class ReportRecipientTO implements Serializable {
	/** Class serial version UID */
	private static final long serialVersionUID = -242493382524510319L;
	/** Recipient first name */
	private String firstName;
	/** Recipient last name */
	private String lastName;
	/** Recipient Report ID */
	private int recipientReportId;
	/** List of devices used to contact the recipient */
	private List<ReportDeviceTO> devices;
	/** ID of the user at mir3 */
	private Integer mir3Id;

	/**
	 * @return the firstName
	 */
	public String getFirstName () {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName (String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName () {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName (String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the recipientReportId
	 */
	public int getRecipientReportId () {
		return recipientReportId;
	}

	/**
	 * @param recipientReportId the recipientReportId to set
	 */
	public void setRecipientReportId (int recipientReportId) {
		this.recipientReportId = recipientReportId;
	}

	/**
	 * @return the devices
	 */
	public List<ReportDeviceTO> getDevices () {
		return devices;
	}

	/**
	 * @param devices the devices to set
	 */
	public void setDevices (List<ReportDeviceTO> devices) {
		this.devices = devices;
	}

	/**
	 * @return the complete name
	 */
	public String getCompleteName () {
		return firstName + " " + lastName;
	}

	/**
	 * @return the mir3Id
	 */
	public Integer getMir3Id () {
		return mir3Id;
	}

	/**
	 * @param mir3Id the mir3Id to set
	 */
	public void setMir3Id (Integer mir3Id) {
		this.mir3Id = mir3Id;
	}

}
