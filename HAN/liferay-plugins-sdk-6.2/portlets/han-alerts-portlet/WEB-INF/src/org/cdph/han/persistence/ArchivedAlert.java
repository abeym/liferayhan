package org.cdph.han.persistence;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

/**
 * Entity class representing an archived alert
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Entity
@Table (name = "alerts_archive", schema = "lportal")
@NamedQueries (
	value = {
		@NamedQuery (name = "ArchivedAlert.FindArchivedByDate", query = "select a from ArchivedAlert a where a.deleted = false order by a.datePublished desc"),
		@NamedQuery (name = "ArchivedAlert.FindArchivedByTopic", query = "select a from ArchivedAlert a where a.deleted = false order by a.topic.title, a.datePublished desc"),
		@NamedQuery (name = "ArchivedAlert.FindArchivedByLevel", query = "select a from ArchivedAlert a where a.deleted = false order by a.severity, a.datePublished desc")
	}
)
@NamedNativeQueries (
	value = {
		@NamedNativeQuery (name = "ArchivedAlert.FindByRecDate",
			query = "select " +
					"    distinct (a.id), a.author_userId, a.contact_userId, a.publisher_userId, a.modifier_userId, a.alert_topic_id, a.date_created, a.date_expired, " +
					"	 a.date_published, a.date_modified, a.date_scheduled, a.incident_date, a.name, a.validate_recipients, a.replay_messages, a.one_text_contact, " +
					"	 a.requires_pin, a.confirm_response, a.report_recipients, a.play_greeting, a.use_alternates, a.select_language, a.use_topics, a.send_to_subscribers, " +
					"	 a.entire_machine, a.partial_machine, a.answer_phone, a.entire_message, a.category, a.priority, a.severity, a.contact_cycle_delay, a.text_device_delay, " +
					"	 a.contact_attempt_cycles, a.expedited_delivery, a.leave_message, a.duration, a.notification_method, a.voice_delivery, a.voice_file, " +
					"    a.deleted, a.date_archived " +
					"from " +
					"    alerts_archive a, " +
					"    alert_recipients_archive r, " +
					"    users_areafields ua " +
					"where " +
					"    a.deleted = false " +
					"    and a.id = r.archived_alert_id " +
					"    and ua.userId = :userId " +
					"    and ( " +
					"        r.userId = ua.userId " +
					"        or r.organizationId = ua.organizationId " +
					"        or r.area_field_id = ua.area_field_id " +
					"        or r.roleId = ua.area_field_id " +
					"        or r.groupId in ((select g.groupId from user_cust_groups g, cust_group_members m, users_areafields od where m.groupId = g.groupId and od.userId = :userId and (m.organizationId = od.organizationId or m.areaFieldId = od.area_field_id or m.roleId = od.roleId))) " +
					"    ) " +
					"order by a.date_published desc;",
			resultClass = ArchivedAlert.class
		),
		@NamedNativeQuery (name = "ArchivedAlert.FindByRecTopic",
			query = "select " +
					"    distinct (a.id), a.author_userId, a.contact_userId, a.publisher_userId, a.modifier_userId, a.alert_topic_id, a.date_created, a.date_expired, " +
					"	 a.date_published, a.date_modified, a.date_scheduled, a.incident_date, a.name, a.validate_recipients, a.replay_messages, a.one_text_contact, " +
					"	 a.requires_pin, a.confirm_response, a.report_recipients, a.play_greeting, a.use_alternates, a.select_language, a.use_topics, a.send_to_subscribers, " +
					"	 a.entire_machine, a.partial_machine, a.answer_phone, a.entire_message, a.category, a.priority, a.severity, a.contact_cycle_delay, a.text_device_delay, " +
					"	 a.contact_attempt_cycles, a.expedited_delivery, a.leave_message, a.duration, a.notification_method, a.voice_delivery, a.voice_file, " +
					"    a.deleted, a.date_archived " +
					"from " +
					"    alerts_archive a, " +
					"    alert_recipients_archive r, " +
					"    users_areafields ua " +
					"where " +
					"    a.deleted = false " +
					"    and a.id = r.archived_alert_id " +
					"    and ua.userId = :userId " +
					"    and ( " +
					"        r.userId = ua.userId " +
					"        or r.organizationId = ua.organizationId " +
					"        or r.area_field_id = ua.area_field_id " +
					"        or r.roleId = ua.area_field_id " +
					"        or r.groupId in ((select g.groupId from user_cust_groups g, cust_group_members m, users_areafields od where m.groupId = g.groupId and od.userId = :userId and (m.organizationId = od.organizationId or m.areaFieldId = od.area_field_id or m.roleId = od.roleId))) " +
					"    ) " +
					"order by a.alert_topic_id, a.date_published desc;",
			resultClass = ArchivedAlert.class
		),
		@NamedNativeQuery (name = "ArchivedAlert.FindByRecLevel",
			query = "select " +
					"    distinct (a.id), a.author_userId, a.contact_userId, a.publisher_userId, a.modifier_userId, a.alert_topic_id, a.date_created, a.date_expired, " +
					"	 a.date_published, a.date_modified, a.date_scheduled, a.incident_date, a.name, a.validate_recipients, a.replay_messages, a.one_text_contact, " +
					"	 a.requires_pin, a.confirm_response, a.report_recipients, a.play_greeting, a.use_alternates, a.select_language, a.use_topics, a.send_to_subscribers, " +
					"	 a.entire_machine, a.partial_machine, a.answer_phone, a.entire_message, a.category, a.priority, a.severity, a.contact_cycle_delay, a.text_device_delay, " +
					"	 a.contact_attempt_cycles, a.expedited_delivery, a.leave_message, a.duration, a.notification_method, a.voice_delivery, a.voice_file, " +
					"    a.deleted, a.date_archived " +
					"from " +
					"    alerts_archive a, " +
					"    alert_recipients_archive r, " +
					"    users_areafields ua " +
					"where " +
					"    a.deleted = false " +
					"    and a.id = r.archived_alert_id " +
					"    and ua.userId = :userId " +
					"    and ( " +
					"        r.userId = ua.userId " +
					"        or r.organizationId = ua.organizationId " +
					"        or r.area_field_id = ua.area_field_id " +
					"        or r.roleId = ua.area_field_id " +
					"        or r.groupId in ((select g.groupId from user_cust_groups g, cust_group_members m, users_areafields od where m.groupId = g.groupId and od.userId = :userId and (m.organizationId = od.organizationId or m.areaFieldId = od.area_field_id or m.roleId = od.roleId))) " +
					"    ) " +
					"order by a.priority, a.date_published desc;",
			resultClass = ArchivedAlert.class
		)
	}
)
public class ArchivedAlert implements Serializable, AlertModel {
	/** Class serial version UID */
	private static final long serialVersionUID = 8452816079132031848L;
	/** ID of the alert */
	@Id
	@Column
	private long id;
	/** Author of the alert */
	@ManyToOne
	@JoinColumn (name = "author_userId", nullable = false)
	private UserData author;
	/** Contact of the alert */
	@ManyToOne
	@JoinColumn (name = "contact_userId", nullable = false)
	private UserData contact;
	/** User that made the last modification */
	@ManyToOne
	@JoinColumn (name = "modifier_userId", nullable = true)
	private UserData modifier;
	/** Date of the alert creation */
	@Column (name = "date_created", nullable = false)
	private Date dateCreated;
	/** Date of the alert publication */
	@Column (name = "date_published", nullable = true)
	private Date datePublished;
	/** User that published the alert */
	@ManyToOne
	@JoinColumn (name = "publisher_userId", nullable = true)
	private UserData publisher;
	/** Date of the last alert modification */
	@Column (name = "date_modified", nullable = true)
	private Date dateModified;
	/** Date when the alert will be published */
	@Column (name = "date_scheduled", nullable = true)
	private Date dateScheduled;
	/** Name of the alert */
	@Column (name = "name", nullable = false, length = 100)
	private String name;
	/** Date of expiration for the alert */
	@Column (name = "date_expired", nullable = true)
	private Date dateExpired;
	/** Used as a flag to validate recipients */
	@Column (name = "validate_recipients", nullable = false)
	private boolean validateRecipients;
	/** Flag for replaying the messages */
	@Column (name = "replay_messages", nullable = false)
	private boolean replayMessages;
	/** Flag for one text contact */
	@Column (name = "one_text_contact", nullable = false)
	private boolean oneTextContact;
	/** Flag for requiring PIN */
	@Column (name = "requires_pin", nullable = false)
	private boolean requiresPIN;
	/** Flag for confirming responses */
	@Column (name = "confirm_response", nullable = false)
	private boolean confirmResponse;
	/** Flag for reporting recipients with report capabilities */
	@Column (name = "report_recipients", nullable = false)
	private boolean reportRecipients;
	/** Flag for playing the greeting message */
	@Column (name = "play_greeting", nullable = false)
	private boolean playGreeting;
	/** Flag for using alternates */
	@Column (name = "use_alternates", nullable = false)
	private boolean useAlternates;
	/** Flag for allowing selection of language */
	@Column (name = "select_language", nullable = false)
	private boolean selectLanguage;
	/** Flag for using topics */
	@Column (name = "use_topics", nullable = false)
	private boolean useTopics;
	/** Flag for sending the alert to the subscribers */
	@Column (name = "send_to_subscribers", nullable = false)
	private boolean sendToSubscribers;
	/** Id of the category */
	@Column (name = "category")
	private String category;
	/** Id of the priority */
	@Column (name = "priority")
	private String priority;
	/** Id of the severity */
	@Column (name = "severity")
	private String severity;
	/** Delay between contact attempts */
	@Column (name = "contact_cycle_delay", nullable = true)
	private Integer contactCycleDelay;
	/** Time to wait for a response in a text device */
	@Column (name = "text_device_delay", nullable = true)
	private Integer textDeviceDelay;
	/** Number of cycles for trying to contact the recipients */
	@Column (name = "contact_attempt_cycles", nullable = true)
	private Integer contactAttempCycles;
	/** Flag to set expedited delivery */
	@Column (name = "expedited_delivery", nullable = false)
	private boolean expeditedDelivery;
	/** Type of message to leave */
	@Column (name = "leave_message")
	private Integer leaveMessage;
	/** Attachments of the alert */
	@OneToMany (mappedBy = "archivedAlert", cascade = CascadeType.ALL)
	private Collection<ArchivedAlertAttachment> attachments;
	/** Alert messages */
	@OneToMany (mappedBy = "archivedAlert", cascade = CascadeType.ALL)
	private Collection<ArchivedAlertMessage> messages;
	/** Alert options */
	@OneToMany (mappedBy = "archivedAlert", cascade = CascadeType.ALL)
	private Collection<ArchivedAlertOption> options;
	/** Alert recipients */
	@OneToMany (mappedBy = "archivedAlert", cascade = CascadeType.ALL)
	private Collection<ArchivedAlertRecipient> recipients;
	/** Alert topic */
	@ManyToOne
	@JoinColumn (name = "alert_topic_id", nullable = true)
	private AlertTopic topic;
	/** Date of the incident */
	@Column (name = "incident_date")
	private Date incidentDate;
	/** Time the alert will remain active in the telephony provider */
	@Column (name = "duration")
	private Integer duration;
	/** Stop contacting recipient if the message was left in the answering machine */
	@Column (name = "entire_machine")
	private boolean entireMachine;
	/** Stop contacting recipient if part of the message was left in the answering machine */
	@Column (name = "partial_machine")
	private boolean partialMachine;
	/** Stop contacting recipient if all the message was listened */
	@Column (name = "entire_message")
	private boolean entireMessage;
	/** Stop contacting recipient if the phone was answered */
	@Column (name = "answer_phone")
	private boolean answerPhone;
	/** Method to be used when sending the notification */
	@Column (name = "notification_method")
	private Integer notificationMethod;
	/** Method used to delivery voice messages */
	@Column (name = "voice_delivery")
	private Integer voiceDelivery;
	/** Name of the recorded file to use */
	@Column (name = "voice_file", length = 100)
	private String voiceFile;
	/** Reports associated to the alert */
	@OneToMany (mappedBy = "alert", cascade = {CascadeType.ALL})
	private Collection<ArchivedAlertReport> alertReports;
	/** Flag for alerts deleted in the system */
	@Column (name = "deleted")
	private boolean deleted;
	/** Date and time when the alert was archived */
	@Column (name = "date_archived")
	private Date dateArchived;
	/** List of overrided locations */
	@OneToMany (mappedBy = "alert", cascade = {CascadeType.ALL})
	@OrderBy ("priority")
	private Collection<ArchivedAlertLocation> locations;
	/** Override defaults only */
	@Column (name = "override_default")
	private boolean overrideDefaultsOnly;
	/** Flag determining if the location is overriden */
	@Column (name = "location_override")
	private boolean locationOverride;

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean equal = false;
		if (obj != null && obj instanceof ArchivedAlert) {
			final ArchivedAlert other = (ArchivedAlert) obj;
			equal = other.getId () == id;
		}
		return equal;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		return Long.valueOf (id).hashCode ();
	}

	/**
	 * @return the id
	 */
	public long getId () {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId (long id) {
		this.id = id;
	}

	/**
	 * @return the author
	 */
	public UserData getAuthor () {
		return author;
	}

	/**
	 * @param author the author to set
	 */
	public void setAuthor (UserData author) {
		this.author = author;
	}

	/**
	 * @return the contact
	 */
	public UserData getContact () {
		return contact;
	}

	/**
	 * @param contact the contact to set
	 */
	public void setContact (UserData contact) {
		this.contact = contact;
	}

	/**
	 * @return the modifier
	 */
	public UserData getModifier () {
		return modifier;
	}

	/**
	 * @param modifier the modifier to set
	 */
	public void setModifier (UserData modifier) {
		this.modifier = modifier;
	}

	/**
	 * @return the dateCreated
	 */
	public Date getDateCreated () {
		return dateCreated;
	}

	/**
	 * @param dateCreated the dateCreated to set
	 */
	public void setDateCreated (Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	/**
	 * @return the datePublished
	 */
	public Date getDatePublished () {
		return datePublished;
	}

	/**
	 * @param datePublished the datePublished to set
	 */
	public void setDatePublished (Date datePublished) {
		this.datePublished = datePublished;
	}

	/**
	 * @return the publisher
	 */
	public UserData getPublisher () {
		return publisher;
	}

	/**
	 * @param publisher the publisher to set
	 */
	public void setPublisher (UserData publisher) {
		this.publisher = publisher;
	}

	/**
	 * @return the dateModified
	 */
	public Date getDateModified () {
		return dateModified;
	}

	/**
	 * @param dateModified the dateModified to set
	 */
	public void setDateModified (Date dateModified) {
		this.dateModified = dateModified;
	}

	/**
	 * @return the dateScheduled
	 */
	public Date getDateScheduled () {
		return dateScheduled;
	}

	/**
	 * @param dateScheduled the dateScheduled to set
	 */
	public void setDateScheduled (Date dateScheduled) {
		this.dateScheduled = dateScheduled;
	}

	/**
	 * @return the name
	 */
	public String getName () {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName (String name) {
		this.name = name;
	}

	/**
	 * @return the dateExpired
	 */
	public Date getDateExpired () {
		return dateExpired;
	}

	/**
	 * @param dateExpired the dateExpired to set
	 */
	public void setDateExpired (Date dateExpired) {
		this.dateExpired = dateExpired;
	}

	/**
	 * @return the validateRecipients
	 */
	public boolean isValidateRecipients () {
		return validateRecipients;
	}

	/**
	 * @param validateRecipients the validateRecipients to set
	 */
	public void setValidateRecipients (boolean validateRecipients) {
		this.validateRecipients = validateRecipients;
	}

	/**
	 * @return the replayMessages
	 */
	public boolean isReplayMessages () {
		return replayMessages;
	}

	/**
	 * @param replayMessages the replayMessages to set
	 */
	public void setReplayMessages (boolean replayMessages) {
		this.replayMessages = replayMessages;
	}

	/**
	 * @return the oneTextContact
	 */
	public boolean isOneTextContact () {
		return oneTextContact;
	}

	/**
	 * @param oneTextContact the oneTextContact to set
	 */
	public void setOneTextContact (boolean oneTextContact) {
		this.oneTextContact = oneTextContact;
	}

	/**
	 * @return the requiresPIN
	 */
	public boolean isRequiresPIN () {
		return requiresPIN;
	}

	/**
	 * @param requiresPIN the requiresPIN to set
	 */
	public void setRequiresPIN (boolean requiresPIN) {
		this.requiresPIN = requiresPIN;
	}

	/**
	 * @return the confirmResponse
	 */
	public boolean isConfirmResponse () {
		return confirmResponse;
	}

	/**
	 * @param confirmResponse the confirmResponse to set
	 */
	public void setConfirmResponse (boolean confirmResponse) {
		this.confirmResponse = confirmResponse;
	}

	/**
	 * @return the reportRecipients
	 */
	public boolean isReportRecipients () {
		return reportRecipients;
	}

	/**
	 * @param reportRecipients the reportRecipients to set
	 */
	public void setReportRecipients (boolean reportRecipients) {
		this.reportRecipients = reportRecipients;
	}

	/**
	 * @return the playGreeting
	 */
	public boolean isPlayGreeting () {
		return playGreeting;
	}

	/**
	 * @param playGreeting the playGreeting to set
	 */
	public void setPlayGreeting (boolean playGreeting) {
		this.playGreeting = playGreeting;
	}

	/**
	 * @return the useAlternates
	 */
	public boolean isUseAlternates () {
		return useAlternates;
	}

	/**
	 * @param useAlternates the useAlternates to set
	 */
	public void setUseAlternates (boolean useAlternates) {
		this.useAlternates = useAlternates;
	}

	/**
	 * @return the selectLanguage
	 */
	public boolean isSelectLanguage () {
		return selectLanguage;
	}

	/**
	 * @param selectLanguage the selectLanguage to set
	 */
	public void setSelectLanguage (boolean selectLanguage) {
		this.selectLanguage = selectLanguage;
	}

	/**
	 * @return the useTopics
	 */
	public boolean isUseTopics () {
		return useTopics;
	}

	/**
	 * @param useTopics the useTopics to set
	 */
	public void setUseTopics (boolean useTopics) {
		this.useTopics = useTopics;
	}

	/**
	 * @return the sendToSubscribers
	 */
	public boolean isSendToSubscribers () {
		return sendToSubscribers;
	}

	/**
	 * @param sendToSubscribers the sendToSubscribers to set
	 */
	public void setSendToSubscribers (boolean sendToSubscribers) {
		this.sendToSubscribers = sendToSubscribers;
	}

	/**
	 * @return the category
	 */
	public String getCategory () {
		return category;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory (String category) {
		this.category = category;
	}

	/**
	 * @return the priority
	 */
	public String getPriority () {
		return priority;
	}

	/**
	 * @param priority the priority to set
	 */
	public void setPriority (String priority) {
		this.priority = priority;
	}

	/**
	 * @return the severity
	 */
	public String getSeverity () {
		return severity;
	}

	/**
	 * @param severity the severity to set
	 */
	public void setSeverity (String severity) {
		this.severity = severity;
	}

	/**
	 * @return the contactCycleDelay
	 */
	public Integer getContactCycleDelay () {
		return contactCycleDelay;
	}

	/**
	 * @param contactCycleDelay the contactCycleDelay to set
	 */
	public void setContactCycleDelay (Integer contactCycleDelay) {
		this.contactCycleDelay = contactCycleDelay;
	}

	/**
	 * @return the textDeviceDelay
	 */
	public Integer getTextDeviceDelay () {
		return textDeviceDelay;
	}

	/**
	 * @param textDeviceDelay the textDeviceDelay to set
	 */
	public void setTextDeviceDelay (Integer textDeviceDelay) {
		this.textDeviceDelay = textDeviceDelay;
	}

	/**
	 * @return the contactAttempCycles
	 */
	public Integer getContactAttempCycles () {
		return contactAttempCycles;
	}

	/**
	 * @param contactAttempCycles the contactAttempCycles to set
	 */
	public void setContactAttempCycles (Integer contactAttempCycles) {
		this.contactAttempCycles = contactAttempCycles;
	}

	/**
	 * @return the expeditedDelivery
	 */
	public boolean isExpeditedDelivery () {
		return expeditedDelivery;
	}

	/**
	 * @param expeditedDelivery the expeditedDelivery to set
	 */
	public void setExpeditedDelivery (boolean expeditedDelivery) {
		this.expeditedDelivery = expeditedDelivery;
	}

	/**
	 * @return the leaveMessage
	 */
	public Integer getLeaveMessage () {
		return leaveMessage;
	}

	/**
	 * @param leaveMessage the leaveMessage to set
	 */
	public void setLeaveMessage (Integer leaveMessage) {
		this.leaveMessage = leaveMessage;
	}

	/**
	 * @return the attachments
	 */
	public Collection<ArchivedAlertAttachment> getAttachments () {
		return attachments;
	}

	/**
	 * @param attachments the attachments to set
	 */
	public void setAttachments (Collection<ArchivedAlertAttachment> attachments) {
		this.attachments = attachments;
	}

	/**
	 * @return the messages
	 */
	public Collection<ArchivedAlertMessage> getMessages () {
		return messages;
	}

	/**
	 * @param messages the messages to set
	 */
	public void setMessages (Collection<ArchivedAlertMessage> messages) {
		this.messages = messages;
	}

	/**
	 * @return the options
	 */
	public Collection<ArchivedAlertOption> getOptions () {
		return options;
	}

	/**
	 * @param options the options to set
	 */
	public void setOptions (Collection<ArchivedAlertOption> options) {
		this.options = options;
	}

	/**
	 * @return the recipients
	 */
	public Collection<ArchivedAlertRecipient> getRecipients () {
		return recipients;
	}

	/**
	 * @param recipients the recipients to set
	 */
	public void setRecipients (Collection<ArchivedAlertRecipient> recipients) {
		this.recipients = recipients;
	}

	/**
	 * @return the topic
	 */
	public AlertTopic getTopic () {
		return topic;
	}

	/**
	 * @param topic the topic to set
	 */
	public void setTopic (AlertTopic topic) {
		this.topic = topic;
	}

	/**
	 * @return the incidentDate
	 */
	public Date getIncidentDate () {
		return incidentDate;
	}

	/**
	 * @param incidentDate the incidentDate to set
	 */
	public void setIncidentDate (Date incidentDate) {
		this.incidentDate = incidentDate;
	}

	/**
	 * @return the duration
	 */
	public Integer getDuration () {
		return duration;
	}

	/**
	 * @param duration the duration to set
	 */
	public void setDuration (Integer duration) {
		this.duration = duration;
	}

	/**
	 * @return the entireMachine
	 */
	public boolean isEntireMachine () {
		return entireMachine;
	}

	/**
	 * @param entireMachine the entireMachine to set
	 */
	public void setEntireMachine (boolean entireMachine) {
		this.entireMachine = entireMachine;
	}

	/**
	 * @return the partialMachine
	 */
	public boolean isPartialMachine () {
		return partialMachine;
	}

	/**
	 * @param partialMachine the partialMachine to set
	 */
	public void setPartialMachine (boolean partialMachine) {
		this.partialMachine = partialMachine;
	}

	/**
	 * @return the entireMessage
	 */
	public boolean isEntireMessage () {
		return entireMessage;
	}

	/**
	 * @param entireMessage the entireMessage to set
	 */
	public void setEntireMessage (boolean entireMessage) {
		this.entireMessage = entireMessage;
	}

	/**
	 * @return the answerPhone
	 */
	public boolean isAnswerPhone () {
		return answerPhone;
	}

	/**
	 * @param answerPhone the answerPhone to set
	 */
	public void setAnswerPhone (boolean answerPhone) {
		this.answerPhone = answerPhone;
	}

	/**
	 * @return the notificationMethod
	 */
	public Integer getNotificationMethod () {
		return notificationMethod;
	}

	/**
	 * @param notificationMethod the notificationMethod to set
	 */
	public void setNotificationMethod (Integer notificationMethod) {
		this.notificationMethod = notificationMethod;
	}

	/**
	 * @return the voiceDelivery
	 */
	public Integer getVoiceDelivery () {
		return voiceDelivery;
	}

	/**
	 * @param voiceDelivery the voiceDelivery to set
	 */
	public void setVoiceDelivery (Integer voiceDelivery) {
		this.voiceDelivery = voiceDelivery;
	}

	/**
	 * @return the voiceFile
	 */
	public String getVoiceFile () {
		return voiceFile;
	}

	/**
	 * @param voiceFile the voiceFile to set
	 */
	public void setVoiceFile (String voiceFile) {
		this.voiceFile = voiceFile;
	}

	/**
	 * @return the deleted
	 */
	public boolean isDeleted () {
		return deleted;
	}

	/**
	 * @param deleted the deleted to set
	 */
	public void setDeleted (boolean deleted) {
		this.deleted = deleted;
	}

	/**
	 * @param alertReports the alertReports to set
	 */
	public void setAlertReports (Collection<ArchivedAlertReport> alertReports) {
		this.alertReports = alertReports;
	}

	/**
	 * @return the alertReports
	 */
	public Collection<ArchivedAlertReport> getAlertReports () {
		return alertReports;
	}

	/**
	 * @return the dateArchived
	 */
	public Date getDateArchived () {
		return dateArchived;
	}

	/**
	 * @param dateArchived the dateArchived to set
	 */
	public void setDateArchived (Date dateArchived) {
		this.dateArchived = dateArchived;
	}

	/**
	 * @return the locations
	 */
	public Collection<ArchivedAlertLocation> getLocations () {
		return locations;
	}

	/**
	 * @param locations the locations to set
	 */
	public void setLocations (Collection<ArchivedAlertLocation> locations) {
		this.locations = locations;
	}

	/**
	 * @return the overrideDefaultsOnly
	 */
	public boolean isOverrideDefaultsOnly () {
		return overrideDefaultsOnly;
	}

	/**
	 * @param overrideDefaultsOnly the overrideDefaultsOnly to set
	 */
	public void setOverrideDefaultsOnly (boolean overrideDefaultsOnly) {
		this.overrideDefaultsOnly = overrideDefaultsOnly;
	}

	/**
	 * @return the locationOverride
	 */
	public boolean isLocationOverride () {
		return locationOverride;
	}

	/**
	 * @param locationOverride the locationOverride to set
	 */
	public void setLocationOverride (boolean locationOverride) {
		this.locationOverride = locationOverride;
	}

}
