package org.cdph.han.util.listeners;

import java.io.File;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Listener class used to remove any unsaved temporary file from the upload directory
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class FileCleanerListener implements HttpSessionListener {
	/** Class log */
	private static final Log log = LogFactory.getLog (FileCleanerListener.class);
	/** Upload directory */
	private static final String FILE_UPLOAD_DIRECTORY = "upload";

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpSessionListener#sessionCreated(javax.servlet.http.HttpSessionEvent)
	 */
	public void sessionCreated (final HttpSessionEvent se) {
		// Nothing to do in the creation of a session
	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpSessionListener#sessionDestroyed(javax.servlet.http.HttpSessionEvent)
	 */
	public void sessionDestroyed (final HttpSessionEvent se) {
		final String sessionId = se.getSession ().getId ();
		final String applicationPath = se.getSession ().getServletContext ().getRealPath (
				se.getSession ().getServletContext ().getServletContextName ());
		final String directorySeparator = System.getProperty ("file.separator");
		final String fileUploadPath = applicationPath.substring (0, applicationPath.lastIndexOf (directorySeparator))
				+ directorySeparator + FILE_UPLOAD_DIRECTORY + directorySeparator + sessionId;
		final File fileUploadDirectory = new File (fileUploadPath);
		if (fileUploadDirectory.exists () && fileUploadDirectory.isDirectory ()) {
			try {
				File[] files = fileUploadDirectory.listFiles ();
				for (File file : files) {
					if (file.isFile ()) {
						file.delete ();
					}
				}
				fileUploadDirectory.delete ();
			} catch (Exception ex) {
				log.error ("Error removing temporal uploaded files.", ex);
			}
		}
	}

}
