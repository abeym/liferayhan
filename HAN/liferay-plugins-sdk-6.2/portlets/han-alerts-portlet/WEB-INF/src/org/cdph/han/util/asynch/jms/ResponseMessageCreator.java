package org.cdph.han.util.asynch.jms;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.cdph.han.util.asynch.dto.AsynchResponseTO;
import org.springframework.jms.core.MessageCreator;

/**
 * This implementation of the message creator expects a fully set AsynchResponseTO object to create the message
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class ResponseMessageCreator implements MessageCreator {
	/** Response object to send in the message */
	private AsynchResponseTO response;
	/** The correlation id to use in the message */
	private String correlationId;

	/**
	 * Constructor to set the response object and the correlation Id
	 * @param response with the query results
	 * @param correlationId of the request message
	 */
	public ResponseMessageCreator (final AsynchResponseTO response, final String correlationId) {
		this.response = response;
		this.correlationId = correlationId;
	}

	/* (non-Javadoc)
	 * @see org.springframework.jms.core.MessageCreator#createMessage(javax.jms.Session)
	 */
	@Override
	public Message createMessage (final Session session) throws JMSException {
		final ObjectMessage objectMessage = session.createObjectMessage (response);
		// Set the correlation ID if any
		if (correlationId != null) {
			objectMessage.setJMSCorrelationID (correlationId);
		}
		return objectMessage;
	}

}
