package org.cdph.han.alerts.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.EventObject;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.portlet.PortletSession;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.alerts.dto.AlertTO;
import org.cdph.han.alerts.dto.FileTO;
import org.cdph.han.alerts.dto.OptionTO;
import org.cdph.han.alerts.jobs.AlertPublicationJob;
import org.cdph.han.alerts.jobs.ArchiveAlertJob;
import org.cdph.han.alerts.jobs.ReportQueryJob;
import org.cdph.han.alerts.services.AlertJPAService;
import org.cdph.han.alerts.services.MessagesService;
import org.cdph.han.alerts.services.UserDataService;
import org.cdph.han.alerts.services.VoiceMessageService;
import org.cdph.han.custgroups.dto.RecipientTO;
import org.cdph.han.persistence.UserData;
import org.cdph.han.util.dto.ListTypeTO;
import org.cdph.han.util.services.HanPropertiesService;
import org.cdph.han.util.services.ListTypeService;
import org.cdph.han.util.services.SecurityService;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;

import com.icesoft.faces.async.render.RenderManager;
import com.icesoft.faces.async.render.Renderable;
import com.icesoft.faces.component.ext.HtmlDataTable;
import com.icesoft.faces.component.inputfile.InputFile;
import com.icesoft.faces.context.DisposableBean;
import com.icesoft.faces.webapp.xmlhttp.FatalRenderingException;
import com.icesoft.faces.webapp.xmlhttp.PersistentFacesState;
import com.icesoft.faces.webapp.xmlhttp.RenderingException;
import com.icesoft.faces.webapp.xmlhttp.TransientRenderingException;

/**
 * Class for the baked bean to process the request to compose a new alert
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class ComposeAlertBean implements Renderable, DisposableBean, Serializable {
	/** Class serial version UID */
	private static final long serialVersionUID = 4894244379352711161L;
	/** Class logger */
	private static final Log log = LogFactory.getLog (ComposeAlertBean.class);
	/** Property for the maximum attachments per alert */
	private static final String HAN_MAX_ATTACHMENTS = "HAN_MAX_ATTACHMENTS";
	/** Render manager */
    private RenderManager renderManager;
    /** Persistent faces state */
    private PersistentFacesState persistentFacesState;
    /** Users session ID */
    private String sessionId;
	/** UserDataService reference */
	private UserDataService userDataService;
	/** Name of the alert */
	private String alertName;
	/** Duration of the alert */
	private String duration;
	/** Priority of the alert */
	private String priority;
	/** Date when the alert was created */
	private Date createdDate;
	/** Name of the author of the alert */
	private String createdBy;
	/** User ID of the author of the alert */
	private long authorUserId;
	/** Name of the user that published the alert */
	private String publishedBy;
	/** User ID of the user that published the alert */
	private Long publishedByUserId;
	/** Date when the alert was published */
	private Date publishedDate;
	/** Last modification date */
	private Date lastModifiedDate;
	/** Date when the notification was sent */
	private Date notificationDate;
	/** Name of the user to contact */
	private String contactName;
	/** ID of the user to contact */
	private Long contactUserId;
	/** Date of the incident */
	private Date incidentDate;
	/** Abstract of the alert */
	private String abstractText;
	/** Alert full details */
	private String fullDetailText;
	/** List of attachments */
	private List<FileTO> attachments;
	/** Latest file uploaded by the user */
	private FileTO currentFile;
	/** Current upload progress */
	private int uploadProgress;
	/** Messages service */
	private MessagesService messagesService;
	/** Flag for scheduling an alert or posting it immediately */
	private boolean scheduled;
	/** Date the alert is scheduled */
	private Date scheduledDate;
	/** Date the alert is going to expire */
	private Date expirationDate;
	/** Id of the topic selected */
	private Long topicId;
	/** List of recipients */
	private List<RecipientTO> recipients;
	/** Validate that the recipient received the message */
	private boolean validateRecipients;
	/** Override Location */
	private boolean locationOverride;
	/** Replay the message */
	private boolean replayMessages;
	/** One text contact */
	private boolean oneTextContact;
	/** Requires PIN to listen the message */
	private boolean requiresPIN;
	/** Confirm the recipient response */
	private boolean confirmResponse;
	/** Send report to recipients */
	private boolean reportRecipients;
	/** Play the greeting */
	private boolean playGreeting;
	/** Use alternates */
	private boolean useAlternates;
	/** Ability to select the language */
	private boolean selectLanguage;
	/** Use mir3 categories, severities and priorities */
	private boolean useTopics;
	/** Send the message to the subscribed users */
	private boolean sendToSubscribers;
	/** Stop contacting recipient if listened to entire message */
	private boolean stopAfterEntireMessage;
	/** Stop contacting recipient if answered the phone */
	private boolean stopAnswerPhone;
	/** Stop contacting recipient if all message was recorded in the answering machine */
	private boolean entireMachine;
	/** Stop contacting recipient if part of the message was recorded in the answering machine */
	private boolean partialMachine;
	/** Time between contact attempts */
	private String contactCycleDelay;
	/** Time to wait for a text response */
	private String textDelay;
	/** Number of contact attempts */
	private int contactCycles;
	/** Expedited delivery */
	private boolean expeditedDelivery;
	/** Option to leave a message, message and call back info, call back info or none */
	private int leaveMessage;
	/** Voice delivery method */
	private int voiceDelivery;
	/** List of options */
	private List<OptionTO> responseOptions;
	/** Binding to the options data table */
	private HtmlDataTable optionsDataTable;
	/** Method of notification */
	private int notificationMethod = 1;
	/** Attachment to send with the notification */
	private int notificationAttachment = -1;
	/** Current alert id */
	private Long alertId;
	/** Service to access the alert information in the HAN DB */
	private AlertJPAService alertJPAService;
	/** The telephony provider message */
	private String telephonyMessage;
	/** Scheduler */
	private Scheduler scheduler;
	/** Serivice to retrieve all the pre recorded voice messages */
	private VoiceMessageService voiceMessageService;
	/** Selected voice message */
	private String selectedVoiceFile;
	/** Service for looking up the users role */
	private SecurityService securityService;
	/** Available locations for override */
	private SelectItem[] locations;
	/** Selected locations for override */
	private SelectItem[] selectedLocations;
	/** Service for retrieving the list values for a type */
	private ListTypeService listTypeService;
	/** Currently selected location */
	private long[] selectedLocation;
	/** Location to move */
	private long[] toMoveLocation;
	/** Override defaults only */
	private boolean overrideDefaultStatus;
	/** Flag to determine if the user is administrator */
	private Boolean admin;
	/** Voice message files */
	private SelectItem[] voiceMessageFiles;
	/** If the user selected to require PIN then the leave message option must be read only */
	private boolean leaveMessageReadOnly;
	/** Service to retrieve properties from the DB */
	private HanPropertiesService hanPropertiesService;
	/** Message for uploaded files */
	private String uploadMessage;
	/** The maximum number of attachments per alert */
	private int maxAttachments;
	/** What image needs to be rendered */
	private String uploadMsgStyle;

	/**
	 * Default constructor
	 */
	public ComposeAlertBean () {
		createdDate = new Date ();
		scheduledDate = new Date ();
		responseOptions = new ArrayList<OptionTO> ();
		persistentFacesState = PersistentFacesState.getInstance();
		if (FacesContext.getCurrentInstance ().getExternalContext ().getSession (false) != null) {
			final Object sesObj = FacesContext.getCurrentInstance ().getExternalContext ().getSession (false);
			if (sesObj instanceof HttpSession) {
				sessionId = ((HttpSession) sesObj).getId ();
			} else if (sesObj instanceof PortletSession) {
				sessionId = ((PortletSession) sesObj).getId ();
			} else {
				sessionId = sesObj.toString ();
			}
		}
		duration = "004:00";
		contactCycleDelay = "00:10";
		contactCycles = 3;
		textDelay = "00:10";
	}

	/**
	 * Action for starting a new alert, prepares all fields for a new alert
	 * @return outcome
	 */
	public String startNewAlert () {
		String outcome = "newAlert";
		abstractText = null;
		attachments = Collections.synchronizedList (new ArrayList<FileTO> ());
		alertId = null;
		alertName = null;
		confirmResponse = true;
		contactName = createdBy;
		contactUserId = authorUserId;
		currentFile = null;
		entireMachine = false;
		expeditedDelivery = false;
		expirationDate = null;
		fullDetailText = null;
		incidentDate = null;
		lastModifiedDate = null;
		leaveMessage = 2;
		locationOverride = true;
		notificationAttachment = -1;
		notificationDate = null;
		notificationMethod = 1;
		telephonyMessage = null;
		oneTextContact = false;
		partialMachine = false;
		playGreeting = true;
		priority = "/Low";
		publishedBy = null;
		publishedByUserId = null;
		publishedDate = null;
		recipients = new ArrayList<RecipientTO> ();
		replayMessages = true;
		reportRecipients = true;
		requiresPIN = false;
		responseOptions = new ArrayList<OptionTO> ();
		voiceMessageFiles = null;
		voiceDelivery = 1;
		/*responseOptions.add (new OptionTO ());
		responseOptions.add (new OptionTO ());
		responseOptions.add (new OptionTO ());*/
		leaveMessageReadOnly = false;
		scheduled = false;
		scheduledDate = null;
		selectLanguage = false;
		sendToSubscribers = false;
		stopAfterEntireMessage = true;
		stopAnswerPhone = false;
		topicId = null;
		uploadProgress = 0;
		useAlternates = false;
		useTopics = false;
		createdDate = new Date ();
		attachments = Collections.synchronizedList (new ArrayList<FileTO> ());
		UserData userData = userDataService.getCurrentUserData ();
		createdBy = userData.getContact ().getFirstName () + " " + userData.getContact ().getLastName ();
		authorUserId = userData.getUserId ();
		contactName = createdBy;
		contactUserId = authorUserId;
		duration = "006:00";
		contactCycleDelay = "000:10";
		contactCycles = 1;
		textDelay = "000:03";
		selectedLocations = new SelectItem[]{};
		maxAttachments = Integer.parseInt (hanPropertiesService.getValue (HAN_MAX_ATTACHMENTS));
		recipients = new ArrayList<RecipientTO> ();
		uploadMessage = "";
		validateRecipients = false;
		FacesContext.getCurrentInstance ().getExternalContext ().getSessionMap ().put (
				"recipients", recipients);
		// Load locations
		// Retrieve the phone types
		final ListTypeTO[] phoneLocations =
			listTypeService.loadListForType ("com.liferay.portal.model.Contact.phone");
		// Retrieve the email types
		final ListTypeTO[] emailLocations =
			listTypeService.loadListForType ("com.liferay.portal.model.Contact.emailAddress");
		locations = new SelectItem[phoneLocations.length];
		selectedLocations = new SelectItem[emailLocations.length];
		int index = 0;
		for (ListTypeTO current : emailLocations) {
			selectedLocations[index++] = new SelectItem (current.getId (), current.getName ());
		}
		index = 0;
		for (ListTypeTO current : phoneLocations) {
			locations[index++] = new SelectItem (current.getId (), current.getName ());
		}
		return outcome;
	}

	/**
	 * Action for cleaning up the ComposeAlertBean
	 * @return outcome
	 */
	public String cancel () {
		String outcome = "success";
		abstractText = null;
		attachments = Collections.synchronizedList (new ArrayList<FileTO> ());
		alertId = null;
		alertName = null;
		confirmResponse = false;
		contactName = createdBy;
		contactUserId = authorUserId;
		currentFile = null;
		duration = null;
		entireMachine = false;
		expeditedDelivery = false;
		expeditedDelivery = false;
		expirationDate = null;
		fullDetailText = null;
		incidentDate = null;
		lastModifiedDate = null;
		leaveMessage = 0;
		locationOverride = false;
		notificationAttachment = -1;
		notificationDate = null;
		notificationMethod = 0;
		oneTextContact = false;
		partialMachine = false;
		playGreeting = false;
		priority = null;
		publishedBy = null;
		publishedByUserId = null;
		publishedDate = null;
		recipients = new ArrayList<RecipientTO> ();
		replayMessages = false;
		reportRecipients = true;
		requiresPIN = false;
		responseOptions = new ArrayList<OptionTO> ();
		scheduled = false;
		scheduledDate = null;
		selectLanguage = false;
		sendToSubscribers = false;
		stopAfterEntireMessage = false;
		stopAnswerPhone = false;
		topicId = null;
		uploadProgress = 0;
		useAlternates = false;
		useTopics = false;
		duration = "004:00";
		contactCycleDelay = "000:10";
		contactCycles = 3;
		textDelay = "000:10";
		return outcome;
	}

	/**
	 * Action listener invoked for uploading a new file
	 * @param event ActionEvent generated by the user
	 */
	public void uploadFile (final ActionEvent event) {
		InputFile inputFile = (InputFile) event.getSource ();
		if (inputFile.getFileInfo ().getStatus () == InputFile.SAVED && attachments.size () < maxAttachments) {
			currentFile = new FileTO ();
			currentFile.setFileInfo (inputFile.getFileInfo ());
			currentFile.setFile (inputFile.getFileInfo ().getFile ());
			synchronized (attachments) {
				attachments.add (currentFile);
			}
			uploadMessage = messagesService.getMessage (
					"alerts.compose.notification.uploaded.attachments.added");
			uploadMsgStyle = ".iceMsgsInfo portlet-msg-info";
		} else if (inputFile.getFileInfo ().getStatus () == InputFile.SAVED) {
			uploadMessage = messagesService.getMessage (
					"alerts.compose.notification.uploaded.attachments.limit.exceeded");
			uploadMsgStyle = ".iceMsgsError portlet-msg-error";
		} else if (inputFile.getFileInfo ().getStatus () == InputFile.SIZE_LIMIT_EXCEEDED) {
			uploadMessage = messagesService.getMessage (
					"alerts.compose.notification.uploaded.attachments.size.exceeded");
			uploadMsgStyle = ".iceMsgsError portlet-msg-error";
		} else if (inputFile.getFileInfo ().getStatus () == InputFile.UNKNOWN_SIZE) {
			uploadMessage = messagesService.getMessage (
					"alerts.compose.notification.uploaded.attachments.limit.no.size");
			uploadMsgStyle = ".iceMsgsError portlet-msg-error";
		}
	}

	/**
	 * Updates the progress of the current upload
	 * @param event
	 */
	public void fileUploadProgress (final EventObject event) {
		InputFile inputFile = (InputFile) event.getSource ();
		uploadProgress = inputFile.getFileInfo ().getPercent ();
		renderManager.getOnDemandRenderer (sessionId).requestRender ();
	}

	/**
	 * Removes a previously uploaded file
	 * @param event
	 */
	public void removeFile (final ActionEvent event) {
		final String fileName = (String) FacesContext.getCurrentInstance ().getExternalContext (
				).getRequestParameterMap ().get ("fileName");
		synchronized (attachments) {
			FileTO uploadedFile;
			for (int i = 0; i < attachments.size (); i++) {
				uploadedFile = attachments.get (i);
				if (uploadedFile.getFileInfo ().getFileName ().equals (fileName)) {
				    attachments.remove (i);
				    break;
				}
			}
			uploadMessage = "";
		}
    }

	/**
	 * Value change listener invoked when the delivery schedule is changed
	 * @param event triggered by the change
	 */
	public void scheduledChanged (final ValueChangeEvent event) {
		scheduledDate = new Date ();
		scheduled = (Boolean) event.getNewValue ();
	}

	/**
	 * Action used to update the topic selection
	 * @return outcome
	 */
	public String topicAdded () {
		String outcome = "newAlert";
		topicId = (Long) FacesContext.getCurrentInstance ().getExternalContext ().getSessionMap ().get ("topicId");
		FacesContext.getCurrentInstance ().getExternalContext ().getSessionMap ().remove ("topicId");
		return outcome;
	}

	/**
	 * Validate that all required fields are present to let the user continue to select the alert recipients
	 * @return outcome
	 */
	public String selectRecipients () {
		String outcome = "selectRecipients";
		if (topicId == null || topicId == -1) {
			outcome = "failure";
			FacesContext.getCurrentInstance ().addMessage ("topic",
					messagesService.getMessage ("alerts.compose.notification.no.topic",
							"Topic Field is required for Alert Content Section",
							FacesMessage.SEVERITY_ERROR));
		}
		if (alertName == null || alertName.length () == 0) {
			outcome = "failure";
			FacesContext.getCurrentInstance ().addMessage ("title",
					messagesService.getMessage ("alerts.compose.notification.no.title",
							"Title Field is required for Alert Content Section",
							FacesMessage.SEVERITY_ERROR));
		} else if (alertName.length () < 3 || alertName.length () > 100) {
			outcome = "failure";
			FacesContext.getCurrentInstance ().addMessage ("title",
					messagesService.getMessage ("alerts.compose.notification.title.wrong.size",
							"Title Field requires a length between 3 and 100 characters.",
							FacesMessage.SEVERITY_ERROR));
		}
		if (abstractText == null || abstractText.length () == 0) {
			outcome = "failure";
			FacesContext.getCurrentInstance ().addMessage ("abstract",
					messagesService.getMessage ("alerts.compose.notification.no.abstract",
							"Abstract Field is required for Alert Content Section",
							FacesMessage.SEVERITY_ERROR));
		} else if (abstractText.length () > 65535) {
			outcome = "failure";
			FacesContext.getCurrentInstance ().addMessage ("abstract",
					messagesService.getMessage ("alerts.compose.notification.abstract.too.long",
							"Abstract Field must be no longer than 65535 characters.",
							FacesMessage.SEVERITY_ERROR));
		}
		if (telephonyMessage == null || telephonyMessage.length () == 0) {
			outcome = "failure";
			FacesContext.getCurrentInstance ().addMessage ("telephonyMessage",
					messagesService.getMessage ("alerts.compose.notification.no.notification.message",
							"Notification Message Field is required for Alert Content Section",
							FacesMessage.SEVERITY_ERROR));
		} else if (telephonyMessage.length () > 3950) {
			outcome = "failure";
			FacesContext.getCurrentInstance ().addMessage ("telephonyMessage",
					messagesService.getMessage ("alerts.compose.notification.notification.message.too.long",
							"Notification Message Field must be no longer than 3950 characters.",
							FacesMessage.SEVERITY_ERROR));
		}
		if (fullDetailText == null || fullDetailText.length () == 0) {
			outcome = "failure";
			FacesContext.getCurrentInstance ().addMessage ("fullDetails",
					messagesService.getMessage ("alerts.compose.notification.no.details",
							"Details Field is required for Alert Content Section",
							FacesMessage.SEVERITY_ERROR));
		} else if (fullDetailText.length () > 65535) {
			outcome = "failure";
			FacesContext.getCurrentInstance ().addMessage ("fullDetails",
					messagesService.getMessage ("alerts.compose.notification.details.too.long",
							"Details Field must be no longer than 65535 characters including formating tags.",
							FacesMessage.SEVERITY_ERROR));
		}
		if (incidentDate != null) {
			GregorianCalendar today = (GregorianCalendar) GregorianCalendar.getInstance (
					TimeZone.getTimeZone ("America/Chicago"));
			GregorianCalendar toCompare = (GregorianCalendar) GregorianCalendar.getInstance (
					TimeZone.getTimeZone ("America/Chicago"));
			toCompare.setTime (incidentDate);
			toCompare.set (Calendar.HOUR_OF_DAY, 0);
			toCompare.set (Calendar.MINUTE, 0);
			toCompare.set (Calendar.SECOND, 0);
			toCompare.set (Calendar.MILLISECOND, 0);
			if (toCompare.after (today)) {
				FacesContext.getCurrentInstance ().addMessage ("incidentDate",
						messagesService.getMessage ("alerts.compose.notification.incident.date.after",
								"The incident day must be prior or equal to the current date.",
								FacesMessage.SEVERITY_ERROR));
				outcome = "failure";
			}
		}
		if (scheduled) {
			if (scheduledDate == null) {
				FacesContext.getCurrentInstance ().addMessage ("scheduledDate",
						messagesService.getMessage ("alerts.compose.notification.scheduled.no.date",
								"When scheduling an alert you must set a date.",
								FacesMessage.SEVERITY_ERROR));
				outcome = "failure";
			} else {
				GregorianCalendar today = (GregorianCalendar) GregorianCalendar.getInstance (
						TimeZone.getTimeZone ("America/Chicago"));
				GregorianCalendar toCompare = (GregorianCalendar) GregorianCalendar.getInstance (
						TimeZone.getTimeZone ("America/Chicago"));
				toCompare.setTime (scheduledDate);
				toCompare.set (Calendar.HOUR_OF_DAY, 8);
				toCompare.set (Calendar.MINUTE, 0);
				toCompare.set (Calendar.SECOND, 0);
				scheduledDate = toCompare.getTime ();
				if (toCompare.before (today)) {
					FacesContext.getCurrentInstance ().addMessage ("scheduledDate",
							messagesService.getMessage ("alerts.compose.notification.schedule.date.before",
									"The date to publish an alert must be after today.",
									FacesMessage.SEVERITY_ERROR));
					outcome = "failure";
				}
			}
		}
		if (expirationDate != null) {
			GregorianCalendar today = (GregorianCalendar) GregorianCalendar.getInstance (
					TimeZone.getTimeZone ("America/Chicago"));
			GregorianCalendar toCompare = (GregorianCalendar) GregorianCalendar.getInstance (
					TimeZone.getTimeZone ("America/Chicago"));
			// Check if the alert is scheduled to be published
			if (scheduledDate != null) {
				today.setTime (scheduledDate);
			}
			toCompare.setTime (expirationDate);
			toCompare.set (Calendar.HOUR_OF_DAY, 23);
			toCompare.set (Calendar.MINUTE, 59);
			toCompare.set (Calendar.SECOND, 59);
			today.add (Calendar.HOUR, 24);
			if (toCompare.before (today)) {
				FacesContext.getCurrentInstance ().addMessage ("expirationDate",
						messagesService.getMessage ("alerts.compose.notification.expiration.date.before",
								"The alert date to remain current must be at least 2 days after the publication date.",
								FacesMessage.SEVERITY_ERROR));
				outcome = "failure";
			}
		}
		FacesContext.getCurrentInstance ().getExternalContext ().getSessionMap ().put (
				"recipients", recipients);
		return outcome;
	}

	/**
	 * Validate that the recipients are selected
	 * @return outcome
	 */
	@SuppressWarnings ("unchecked")
	public String advancedOptions () {
		String outcome = "options";
		recipients = (List<RecipientTO>) FacesContext.getCurrentInstance (
				).getExternalContext ().getSessionMap ().get ("recipients");
		FacesContext.getCurrentInstance ().getExternalContext ().getSessionMap ().remove ("recipients");
		if (recipients == null || recipients.isEmpty ()) {
			outcome = "selectRecipients";
			FacesContext.getCurrentInstance ().addMessage (null,
					messagesService.getMessage ("alerts.recipients.not.selected",
							"You most select at least one recipient.",
							FacesMessage.SEVERITY_ERROR));
		}
		return outcome;
	}

	/**
	 * Action listener to remove options from the response options list
	 * @param event ActionEvent triggered by the user
	 */
	public void removeOption (final ActionEvent event) {
		responseOptions.remove (optionsDataTable.getRowIndex ());
	}

	/**
	 * Action listener to add a new response option
	 * @param event ActionEvent triggered by the user
	 */
	public void addNewOption (final ActionEvent event) {
		responseOptions.add (new OptionTO ());
	}

	/**
	 * This action listener validates that the alert title is present, then saves all the alert info
	 * in the database as a draft.
	 * @param event ActionEvent triggered by the user
	 */
	public void saveAlert (final ActionEvent event) {
		if (alertName == null || alertName.length () == 0) {
			FacesContext.getCurrentInstance ().addMessage ("title",
					messagesService.getMessage ("alerts.compose.notification.no.title",
							"Title Field is required for Alert Content Section",
							FacesMessage.SEVERITY_ERROR));
		} else if (alertName.length () < 3 || alertName.length () > 100) {
			FacesContext.getCurrentInstance ().addMessage ("title",
					messagesService.getMessage ("alerts.compose.notification.title.wrong.size",
							"Title Field requires a length between 3 and 100 characters.",
							FacesMessage.SEVERITY_ERROR));
		} else {
			final AlertTO alertTO = populateAlertTO ();
			alertTO.setReadyToPublish (event == null);
			try {
				alertId = alertJPAService.saveAlert (alertTO);
				if (event != null) {
					FacesContext.getCurrentInstance ().addMessage (null,
							messagesService.getMessage ("alerts.save.success",
									"The alert was saved successfully as a draft.",
									FacesMessage.SEVERITY_INFO));
				}
			} catch (Exception ex) {
				FacesContext.getCurrentInstance ().addMessage (null,
						messagesService.getMessage ("alerts.save.error",
								"The alert was not save as a Draft, please try again later.",
								FacesMessage.SEVERITY_ERROR));
				log.error ("Error saving alert.", ex);
			}
		}
	}

	/**
	 * Action to retrieve a selected alert
	 * @return outcome
	 */
	public String loadAlert () {
		String outcome = "success";
		startNewAlert ();
		final String alertIdText = (String) FacesContext.getCurrentInstance (
				).getExternalContext ().getRequestParameterMap ().get ("alertId");
		final long alertId = Long.parseLong (alertIdText);
		try {
			final AlertTO alert = alertJPAService.retrieveAlert (alertId, false);
			populateBean (alert);
			final UserData userData = userDataService.getUser (alert.getAuthorUserId ());
			createdBy = userData.getContact ().getFirstName () + " " + userData.getContact ().getLastName ();
			FacesContext.getCurrentInstance ().getExternalContext ().getSessionMap ().put (
					"recipients", recipients);
		} catch (Exception ex) {
			log.error ("Error loading alert with id: " + alertIdText, ex);
			FacesContext.getCurrentInstance ().addMessage (null,
					messagesService.getMessage ("alerts.load.error",
							"Error loading the requested Alert, please try again later.",
							FacesMessage.SEVERITY_ERROR));
			outcome = "loadError";
		}
		return outcome;
	}

	/**
	 * Make a copy of another alert to compose a new one
	 * @return outcome of the operation
	 */
	public String copyAlert () {
		String outcome = "newAlert";
		startNewAlert ();
		final String alertIdText = (String) FacesContext.getCurrentInstance (
				).getExternalContext ().getRequestParameterMap ().get ("alertId");
		final String archivedString = (String) FacesContext.getCurrentInstance (
				).getExternalContext ().getRequestParameterMap ().get ("archived");
		final long alertId = Long.parseLong (alertIdText);
		final boolean archived = Boolean.parseBoolean (archivedString);
		try {
			final AlertTO alert = alertJPAService.retrieveAlert (alertId, archived);
			populateBean (alert);
			this.alertId = null;
			final UserData userData = userDataService.getUser (alert.getAuthorUserId ());
			createdBy = userData.getContact ().getFirstName () + " " + userData.getContact ().getLastName ();
			authorUserId = userData.getUserId ();
			publishedDate = null;
			createdDate = new Date ();
			lastModifiedDate = createdDate;
			FacesContext.getCurrentInstance ().getExternalContext ().getSessionMap ().put (
					"recipients", recipients);
		} catch (Exception ex) {
			log.error ("Error loading alert with id: " + alertIdText, ex);
			FacesContext.getCurrentInstance ().addMessage (null,
					messagesService.getMessage ("alerts.load.error",
							"Error loading the requested Alert, please try again later.",
							FacesMessage.SEVERITY_ERROR));
			outcome = "failure";
		}
		return outcome;
	}

	/**
	 * Sets the bean properties according to the retrieved alert
	 * @param alert AlertTO with the saved alert values
	 */
	private void populateBean (final AlertTO alert) {
		abstractText = alert.getAbstractText ();
		alertId = alert.getAlertId ();
		alertName = alert.getAlertName ();
		attachments = alert.getAttachments ();
		authorUserId = alert.getAuthorUserId ();
		confirmResponse = alert.isConfirmResponse ();
		contactCycleDelay = alert.getContactCycleDelay ();
		contactCycles = alert.getContactCycles () == null ? 0 : alert.getContactCycles ();
		contactUserId = alert.getContactUserId ();
		createdDate = alert.getCreatedDate ();
		duration = alert.getDuration ();
		entireMachine = alert.isEntireMachine ();
		expeditedDelivery = alert.isExpeditedDelivery ();
		expirationDate = alert.getExpirationDate ();
		fullDetailText = alert.getFullDetailText ();
		incidentDate = alert.getIncidentDate ();
		lastModifiedDate = alert.getLastModifiedDate ();
		leaveMessage = alert.getLeaveMessage ();
		locationOverride = alert.isLocationOverride ();
		notificationAttachment = alert.getNotificationAttachment ();
		notificationDate = alert.getNotificationDate ();
		notificationMethod = alert.getNotificationMethod ();
		oneTextContact = alert.isOneTextContact ();
		overrideDefaultStatus = alert.isOverrideDefaultStatus ();
		partialMachine = alert.isPartialMachine ();
		playGreeting = alert.isPlayGreeting ();
		priority = alert.getPriority ();
		publishedByUserId = alert.getPublishedByUserId ();
		publishedDate = alert.getPublishedDate ();
		recipients = alert.getRecipients ();
		replayMessages = alert.isReplayMessages ();
		reportRecipients = alert.isReportRecipients ();
		requiresPIN = alert.isRequiresPIN ();
		responseOptions = alert.getResponseOptions ();
		scheduled = alert.isScheduled ();
		scheduledDate = alert.getScheduledDate ();
		selectLanguage = alert.isSelectLanguage ();
		stopAfterEntireMessage = alert.isStopAfterEntireMessage ();
		stopAnswerPhone = alert.isStopAnswerPhone ();
		textDelay = alert.getTextDelay ();
		topicId = alert.getTopicId () != null ? alert.getTopicId () : -1;
		useAlternates = alert.isUseAlternates ();
		useTopics = alert.isUseTopics ();
		validateRecipients = alert.isValidateRecipients ();
		voiceDelivery = alert.getVoiceDelivery ();
		telephonyMessage = alert.getTelephonyMessage ();
		selectedVoiceFile = alert.getVoiceFile ();
		leaveMessageReadOnly = alert.isRequiresPIN ();
		if (locationOverride) {
			// Retrieve the phone types
			final ListTypeTO[] phoneLocations =
				listTypeService.loadListForType ("com.liferay.portal.model.Contact.phone");
			// Retrieve the email types
			final ListTypeTO[] emailLocations =
				listTypeService.loadListForType ("com.liferay.portal.model.Contact.emailAddress");
			final List<SelectItem> foundLocations = new ArrayList<SelectItem> ();
			final List<SelectItem> selected = new ArrayList<SelectItem> ();
			for (ListTypeTO current : emailLocations) {
				foundLocations.add (new SelectItem (current.getId (), current.getName ()));
			}
			for (ListTypeTO current : phoneLocations) {
				foundLocations.add (new SelectItem (current.getId (), current.getName ()));
			}
			if (alert.getLocationOverridePriorities () != null
					&& alert.getLocationOverridePriorities ().size () > 0) {
				SelectItem temp = null;
				for (long id : alert.getLocationOverridePriorities ()) {
					for (SelectItem loc : foundLocations) {
						if (loc.getValue ().equals (id)) {
							temp = loc;
							break;
						}
					}
					selected.add (temp);
					foundLocations.remove (temp);
				}
			}
			locations = foundLocations.toArray (new SelectItem[foundLocations.size ()]);
			selectedLocations = selected.toArray (new SelectItem[selected.size ()]);
		}
	}

	/**
	 * Creates a new AlertTO object with the information collected from the user
	 * @return AlertTO object with the current information
	 */
	private AlertTO populateAlertTO () {
		final AlertTO alertTO = new AlertTO ();
		alertTO.setAbstractText (abstractText == null || abstractText.length () == 0 ? null : abstractText);
		alertTO.setAlertId (alertId);
		alertTO.setAlertName (alertName);
		alertTO.setAttachments (attachments);
		alertTO.setAuthorUserId (authorUserId);
		alertTO.setConfirmResponse (confirmResponse);
		alertTO.setContactCycleDelay (contactCycleDelay == null || contactCycleDelay.length () == 0
				? null : contactCycleDelay);
		alertTO.setContactCycles (contactCycles == 0 ? null : contactCycles);
		alertTO.setContactUserId (contactUserId);
		alertTO.setCreatedDate (createdDate);
		alertTO.setDuration (duration == null || duration.length () == 0 ? null : duration);
		alertTO.setEntireMachine (entireMachine);
		alertTO.setExpeditedDelivery (expeditedDelivery);
		alertTO.setExpirationDate (expirationDate);
		alertTO.setFullDetailText (fullDetailText);
		alertTO.setIncidentDate (incidentDate);
		alertTO.setLastModifiedDate (lastModifiedDate);
		alertTO.setLeaveMessage (requiresPIN ? 4 : leaveMessage);
		alertTO.setLocationOverride (locationOverride);
		alertTO.setNotificationAttachment (notificationAttachment >= 0 ? notificationAttachment : -1);
		alertTO.setNotificationDate (notificationDate);
		alertTO.setNotificationMethod (notificationMethod);
		alertTO.setOneTextContact (oneTextContact);
		alertTO.setPartialMachine (partialMachine);
		alertTO.setPlayGreeting (playGreeting);
		alertTO.setPriority (priority);
		alertTO.setPublishedByUserId (publishedByUserId);
		alertTO.setPublishedDate (publishedDate);
		alertTO.setRecipients (recipients);
		alertTO.setReplayMessages (replayMessages);
		alertTO.setReportRecipients (reportRecipients);
		alertTO.setRequiresPIN (requiresPIN);
		alertTO.setResponseOptions (responseOptions);
		alertTO.setScheduled (scheduled);
		alertTO.setScheduledDate (scheduledDate);
		alertTO.setSelectLanguage (selectLanguage);
		alertTO.setStopAfterEntireMessage (stopAfterEntireMessage);
		alertTO.setStopAnswerPhone (stopAnswerPhone);
		alertTO.setTextDelay (textDelay == null || textDelay.length () == 0 ? null : textDelay);
		alertTO.setTopicId (topicId == null || topicId == -1 ? null : topicId);
		alertTO.setUseAlternates (useAlternates);
		alertTO.setUseTopics (useTopics);
		alertTO.setValidateRecipients (validateRecipients);
		alertTO.setVoiceDelivery (voiceDelivery);
		alertTO.setTelephonyMessage (telephonyMessage);
		alertTO.setVoiceFile (selectedVoiceFile);
		alertTO.setOverrideDefaultStatus (overrideDefaultStatus);
		if (locationOverride && selectedLocations != null && selectedLocations.length > 0) {
			alertTO.setLocationOverridePriorities (new ArrayList<Long> (selectedLocations.length));
			for (SelectItem current : selectedLocations) {
				alertTO.getLocationOverridePriorities ().add ((Long) current.getValue ());
			}
		}
		return alertTO;
	}

	/**
	 * Action to publish the alert
	 * @return outcome
	 */
	public void publishAlert (final ActionEvent event) {
		String outcome = "success";
		boolean validated = true;
		if (locationOverride) {
			if (selectedLocations == null || selectedLocations.length == 0) {
				outcome = "failure";
				FacesContext.getCurrentInstance ().addMessage (null,
						messagesService.getMessage ("alerts.compose.options.location.override.no.selection",
								"The 'Contact Attempt Cycles' cannot exceed 3 for an alert with no response options.",
								FacesMessage.SEVERITY_ERROR));
				validated = false;
			}
		}
		if ((responseOptions == null || responseOptions.isEmpty ()) && contactCycles > 3) {
			outcome = "failure";
			FacesContext.getCurrentInstance ().addMessage (null,
					messagesService.getMessage ("alerts.publish.validation.error.cycle",
							"You can only set up to 3 contact cycle attemps with no response options.",
							FacesMessage.SEVERITY_ERROR));
			validated = false;
		}
		if (expirationDate != null) {
			GregorianCalendar timeLive = (GregorianCalendar) GregorianCalendar.getInstance (
					TimeZone.getTimeZone ("America/Chicago"));
			GregorianCalendar expiration = (GregorianCalendar) GregorianCalendar.getInstance (
					TimeZone.getTimeZone ("America/Chicago"));
			// Check if the alert is scheduled and set the scheduled time
			if (scheduled) {
				timeLive.setTime (scheduledDate);
			}
			// Add another hour
			timeLive.add (Calendar.HOUR, 1);
			// Add the time the alert will remain active
			timeLive.add (Calendar.MILLISECOND, (int) getMilliseconds (duration));
			// Set the requested expiration date
			expiration.setTime (expirationDate);
			// Check if the expiration will happen before the alert notification is finished
			if (expiration.before (timeLive)) {
				outcome = "failure";
				FacesContext.getCurrentInstance ().addMessage (null,
						messagesService.getMessage ("alerts.compose.notification.not.enough.time",
								"Given the date the alert will remain current and the notification duration " +
								"the alert won't finish before archiving. " +
								"Either set a new date or reduce the duration.",
								FacesMessage.SEVERITY_ERROR));
				validated = false;
			}
		}
		if (validated) {
			try {
				if (scheduled) {
					saveAlert (null);
					Trigger simpleTrigger = new SimpleTrigger (
							"PublishAlert-" + alertId, "ALERT_PUBLICATION_GROUP",
							scheduledDate);
					simpleTrigger.setPriority (5);
					JobDetail jobDetail = new JobDetail ();
					jobDetail.setGroup ("ALERT_PUBLICATION_GROUP");
					jobDetail.setJobClass (AlertPublicationJob.class);
					jobDetail.setName ("PUBLISH_ALERT-" + alertId);
					jobDetail.setJobDataMap (new JobDataMap ());
					jobDetail.getJobDataMap ().put ("alertId", alertId.longValue ());
					scheduler.scheduleJob (jobDetail, simpleTrigger);
					FacesContext.getCurrentInstance ().addMessage (null,
							messagesService.getMessage ("alerts.publish.scheduled.success",
									"The alert was scheduled for publishing successfully.",
									FacesMessage.SEVERITY_INFO));
					outcome = "scheduled";
				} else {
					final AlertTO toPublish = populateAlertTO ();
					final int notificationReportId = alertJPAService.publishAlert (toPublish);
					alertId = toPublish.getAlertId ();
					SimpleTrigger simpleTrigger = new SimpleTrigger (
							"QueryReport-" + alertId + ":" + notificationReportId,
							"ALERT_PUBLICATION_GROUP");
					simpleTrigger.setPriority (5);
					JobDetail jobDetail = new JobDetail ();
					jobDetail.setGroup ("ALERT_PUBLICATION_GROUP");
					jobDetail.setJobClass (ReportQueryJob.class);
					jobDetail.setName ("QUERY_REPORT-" + alertId + ":" + notificationReportId);
					jobDetail.setJobDataMap (new JobDataMap ());
					jobDetail.getJobDataMap ().put ("alertId", alertId.longValue ());
					jobDetail.getJobDataMap ().put ("notificationReportId", notificationReportId);
					simpleTrigger.setRepeatInterval (30000);
					final Date startTime = new Date ();
					startTime.setTime (startTime.getTime () + 30000);
					simpleTrigger.setStartTime (startTime);
					// Make sure the trigger is fired enough times to get the report
					simpleTrigger.setRepeatCount (((int) getMilliseconds (duration) / 30000) + 4);
					scheduler.scheduleJob (jobDetail, simpleTrigger);
					FacesContext.getCurrentInstance ().addMessage (null,
							messagesService.getMessage ("alerts.publish.success",
									"The alert was published successfully.",
									FacesMessage.SEVERITY_INFO));
				}
				if (expirationDate != null) {
					SimpleTrigger simpleTrigger = new SimpleTrigger (
							"ArchiveAlert-" + alertId, "ALERT_ARCHIVE_GROUP",
							expirationDate);
					simpleTrigger.setPriority (5);
					JobDetail jobDetail = new JobDetail ();
					jobDetail.setGroup ("ALERT_ARCHIVE_GROUP");
					jobDetail.setJobClass (ArchiveAlertJob.class);
					jobDetail.setName ("ARCHIVE_ALERT-" + alertId);
					jobDetail.setJobDataMap (new JobDataMap ());
					jobDetail.getJobDataMap ().put ("alertId", alertId.longValue ());
					scheduler.scheduleJob (jobDetail, simpleTrigger);
				}
			} catch (Exception ex) {
				FacesContext.getCurrentInstance ().addMessage (null,
						messagesService.getMessage ("alerts.publish.error",
								"The alert was not published, please try again later.",
								FacesMessage.SEVERITY_ERROR));
				log.error ("Error saving alert.", ex);
				outcome = "failure";
			}
		}
		FacesContext.getCurrentInstance ().getExternalContext ().getSessionMap ().put ("PUBLISH_OUTCOME", outcome);
		if (alertId != null) {
			FacesContext.getCurrentInstance ().getExternalContext ().getSessionMap ().put ("ALERT_ID", alertId);
		}
	}

	/**
	 * Transforms a string with the format hh:mm to milliseconds
	 * @param duration the string to get the milliseconds from
	 * @return the total milliseconds
	 */
	private long getMilliseconds (final String duration) {
		long milliseconds = 0;
		final int hours = Integer.parseInt (duration.substring (0, duration.indexOf (':')));
		final int minutes = Integer.parseInt (duration.substring (duration.indexOf (':') + 1));
		milliseconds = minutes * 60000;
		milliseconds += hours * 3600000;
		return milliseconds;
	}

	/**
	 * Loads or removes the locations
	 * @param event
	 */
	public void toggleLocation (final ValueChangeEvent event) {
		boolean load = (Boolean) event.getNewValue ();
		if (load) {
			// Retrieve the phone types
			final ListTypeTO[] phoneLocations =
				listTypeService.loadListForType ("com.liferay.portal.model.Contact.phone");
			// Retrieve the email types
			final ListTypeTO[] emailLocations =
				listTypeService.loadListForType ("com.liferay.portal.model.Contact.emailAddress");
			locations = new SelectItem[phoneLocations.length + emailLocations.length];
			int index = 0;
			for (ListTypeTO current : emailLocations) {
				locations[index++] = new SelectItem (current.getId (), current.getName ());
			}
			for (ListTypeTO current : phoneLocations) {
				locations[index++] = new SelectItem (current.getId (), current.getName ());
			}
		} else {
			locations = new SelectItem[0];
			selectedLocations = new SelectItem[0];
		}
	}

	/**
	 * Add a location to the selected locations for override
	 * @param event
	 */
	public void addLocation (final ValueChangeEvent event) {
		if (locations.length > 0) {
			final long[] selected = (long[]) event.getNewValue ();
			if (selected != null && selected.length > 0) {
				final SelectItem[] tempLocations = new SelectItem[locations.length - selected.length];
				final SelectItem[] tempSelectedLocations =
					new SelectItem[selectedLocations == null ? selected.length
							: selectedLocations.length + selected.length];
				SelectItem toMove = null;
				int idx = 0;
				for (long id : selected) {
					int index = 0;
					for (SelectItem current : locations) {
						if (!current.getValue ().equals (id)) {
							tempLocations[index++] = current;
						} else {
							toMove = current;
						}
					}
					index = 0;
					if (selectedLocations != null) {
						for (SelectItem current : selectedLocations) {
							tempSelectedLocations[index++] = current;
						}
					}
					tempSelectedLocations[index + idx++] = toMove;
				}
				locations = tempLocations;
				selectedLocations = tempSelectedLocations;
			}
		}
	}

	/**
	 * Remove a location from the selected locations to override
	 * @param event
	 */
	public void removeLocation (final ActionEvent event) {
		if (selectedLocations.length > 0) {
			if (selectedLocation != null && selectedLocation.length == 1) {
				final SelectItem[] tempLocations = new SelectItem[locations.length + selectedLocation.length];
				final SelectItem[] tempSelectedLocations =
					new SelectItem[selectedLocations.length - selectedLocation.length];
				SelectItem toMove = null;
				int idx = 0;
				for (long id : selectedLocation) {
					int index = 0;
					for (SelectItem current : selectedLocations) {
						if (!current.getValue ().equals (id)) {
							tempSelectedLocations[index++] = current;
						} else {
							toMove = current;
						}
					}
					index = 0;
					for (SelectItem current : locations) {
						tempLocations[index++] = current;
					}
					tempLocations[index + idx++] = toMove;
				}
				locations = tempLocations;
				selectedLocations = tempSelectedLocations;
			}
		}
	}

	/**
	 * Listener to move up a location override
	 * @param event
	 */
	public void moveUpLocation (final ActionEvent event) {
		// Only allow one selection
		if (selectedLocation != null && selectedLocation.length > 0) {
			if (selectedLocations != null && selectedLocations.length > 1) {
				int idx = 0;
				for (SelectItem location : selectedLocations) {
					if (location.getValue ().equals (selectedLocation[0])) {
						break;
					}
					++idx;
				}
				// Move the locations if the index is greater than 0
				if (idx > 0) {
					SelectItem temp = selectedLocations[idx - 1];
					selectedLocations[idx - 1] = selectedLocations[idx];
					selectedLocations[idx] = temp;
				}
			}
		}
	}

	/**
	 * Listener to move up a location override
	 * @param event
	 */
	public void moveDownLocation (final ActionEvent event) {
		// Only allow one selection
		if (selectedLocation != null && selectedLocation.length > 0) {
			if (selectedLocations != null && selectedLocations.length > 1) {
				int idx = 0;
				for (SelectItem location : selectedLocations) {
					if (location.getValue ().equals (selectedLocation[0])) {
						break;
					}
					++idx;
				}
				// Move the locations if the index is lesser than the selectedLocations size - 1
				if (idx < selectedLocations.length - 1) {
					SelectItem temp = selectedLocations[idx + 1];
					selectedLocations[idx + 1] = selectedLocations[idx];
					selectedLocations[idx] = temp;
				}
			}
		}
	}

	/**
	 * Value change listener for loading the voice message files
	 * @param event change event
	 */
	public void deliveryChanged (final ValueChangeEvent event) {
		final Object newValue = event.getNewValue ();
		if (newValue != null) {
			final int selectedOption = (Integer) newValue;
			if (selectedOption == 2) {
				String[] recordedFiles = voiceMessageService.recordedFiles ();
				voiceMessageFiles = new SelectItem[recordedFiles.length];
				for (int i = 0; i < recordedFiles.length; i++) {
					voiceMessageFiles[i] = new SelectItem (recordedFiles[i], recordedFiles[i]);
				}
			}
		}
	}

	/**
	 * Value change listener for assigning the appropriate leave message only option
	 * @param event
	 */
	public void requiresPINChanged (final ValueChangeEvent event) {
		final Object newValue = event.getNewValue ();
		log.info ("New Value: " + newValue);
		if (newValue != null) {
			final boolean requiresPIN = (Boolean) newValue;
			log.info ("Requires PIN: " + requiresPIN);
			if (requiresPIN) {
				leaveMessage = 4;
				leaveMessageReadOnly = true;
			} else {
				leaveMessageReadOnly = false;
			}
		}
	}

	/**
	 * @return the alertName
	 */
	public String getAlertName () {
		return alertName;
	}

	/**
	 * @param alertName the alertName to set
	 */
	public void setAlertName (String alertName) {
		this.alertName = alertName;
	}

	/**
	 * @return the duration
	 */
	public String getDuration () {
		return duration;
	}

	/**
	 * @param duration the duration to set
	 */
	public void setDuration (String duration) {
		this.duration = duration;
	}

	/**
	 * @return the priority
	 */
	public String getPriority () {
		return priority;
	}

	/**
	 * @param priority the priority to set
	 */
	public void setPriority (String priority) {
		this.priority = priority;
	}

	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate () {
		return createdDate;
	}

	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate (Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy () {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy (String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the authorUserId
	 */
	public long getAuthorUserId () {
		return authorUserId;
	}

	/**
	 * @param authorUserId the authorUserId to set
	 */
	public void setAuthorUserId (long authorUserId) {
		this.authorUserId = authorUserId;
	}

	/**
	 * @return the publishedBy
	 */
	public String getPublishedBy () {
		return publishedBy;
	}

	/**
	 * @param publishedBy the publishedBy to set
	 */
	public void setPublishedBy (String publishedBy) {
		this.publishedBy = publishedBy;
	}

	/**
	 * @return the publishedByUserId
	 */
	public Long getPublishedByUserId () {
		return publishedByUserId;
	}

	/**
	 * @param publishedByUserId the publishedByUserId to set
	 */
	public void setPublishedByUserId (Long publishedByUserId) {
		this.publishedByUserId = publishedByUserId;
	}

	/**
	 * @return the lastModifiedDate
	 */
	public Date getLastModifiedDate () {
		return lastModifiedDate;
	}

	/**
	 * @param lastModifiedDate the lastModifiedDate to set
	 */
	public void setLastModifiedDate (Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	/**
	 * @return the notificationDate
	 */
	public Date getNotificationDate () {
		return notificationDate;
	}

	/**
	 * @param notificationDate the notificationDate to set
	 */
	public void setNotificationDate (Date notificationDate) {
		this.notificationDate = notificationDate;
	}

	/**
	 * @return the publishedDate
	 */
	public Date getPublishedDate () {
		return publishedDate;
	}

	/**
	 * @param publishedDate the publishedDate to set
	 */
	public void setPublishedDate (Date publishedDate) {
		this.publishedDate = publishedDate;
	}

	/**
	 * @param userDataService the userDataService to set
	 */
	public void setUserDataService (UserDataService userDataService) {
		this.userDataService = userDataService;
	}

	/**
	 * @return the contactName
	 */
	public String getContactName () {
		return contactName;
	}

	/**
	 * @param contactName the contactName to set
	 */
	public void setContactName (String contactName) {
		this.contactName = contactName;
	}

	/**
	 * @return the contactUserId
	 */
	public Long getContactUserId () {
		return contactUserId;
	}

	/**
	 * @param contactUserId the contactUserId to set
	 */
	public void setContactUserId (Long contactUserId) {
		this.contactUserId = contactUserId;
	}

	/**
	 * @return the incidentDate
	 */
	public Date getIncidentDate () {
		return incidentDate;
	}

	/**
	 * @param incidentDate the incidentDate to set
	 */
	public void setIncidentDate (Date incidentDate) {
		this.incidentDate = incidentDate;
	}

	/**
	 * @return the abstractText
	 */
	public String getAbstractText () {
		return abstractText;
	}

	/**
	 * @param abstractText the abstractText to set
	 */
	public void setAbstractText (String abstractText) {
		this.abstractText = abstractText;
	}

	/**
	 * @return the fullDetailText
	 */
	public String getFullDetailText () {
		return fullDetailText;
	}

	/**
	 * @param fullDetailText the fullDetailText to set
	 */
	public void setFullDetailText (String fullDetailText) {
		this.fullDetailText = fullDetailText;
	}

	/* (non-Javadoc)
	 * @see com.icesoft.faces.async.render.Renderable#getState()
	 */
	public PersistentFacesState getState () {
		return persistentFacesState;
	}

	/* (non-Javadoc)
	 * @see com.icesoft.faces.async.render.Renderable#renderingException(com.icesoft.faces.webapp.xmlhttp.RenderingException)
	 */
	public void renderingException (final RenderingException ex) {
		if (log.isTraceEnabled() && ex instanceof TransientRenderingException) {
			log.trace ("InputFileController Transient Rendering excpetion:", ex);
		} else if (ex instanceof FatalRenderingException) {
			if (log.isTraceEnabled()) {
				log.trace ("InputFileController Fatal rendering exception: ", ex);
			}
			renderManager.getOnDemandRenderer (sessionId).remove (this);
			renderManager.getOnDemandRenderer (sessionId).dispose ();
		}
	}

	/**
	 * Sets the renderManager
	 * @param renderManager the render manager
	 */
	public void setRenderManager (final RenderManager renderManager) {
        this.renderManager = renderManager;
        renderManager.getOnDemandRenderer (sessionId).add (this);
    }

	/* (non-Javadoc)
	 * @see com.icesoft.faces.context.DisposableBean#dispose()
	 */
	public void dispose () throws Exception {
		if (log.isTraceEnabled ()) {
			log.trace ("OutputProgressController dispose OnDemandRenderer for session: " + sessionId);
		}
		renderManager.getOnDemandRenderer (sessionId).remove (this);
		renderManager.getOnDemandRenderer (sessionId).dispose ();
	}

	/**
	 * @return the uploadProgress
	 */
	public int getUploadProgress () {
		return uploadProgress;
	}

	/**
	 * @param uploadProgress the uploadProgress to set
	 */
	public void setUploadProgress (int uploadProgress) {
		this.uploadProgress = uploadProgress;
	}

	/**
	 * @return the attachments
	 */
	public List<FileTO> getAttachments () {
		return attachments;
	}

	/**
	 * @param messagesService the messagesService to set
	 */
	public void setMessagesService (MessagesService messagesService) {
		this.messagesService = messagesService;
	}

	/**
	 * @return the scheduled
	 */
	public boolean isScheduled () {
		return scheduled;
	}

	/**
	 * @param scheduled the scheduled to set
	 */
	public void setScheduled (boolean scheduled) {
		this.scheduled = scheduled;
	}

	public String getScheduledStr () {
		return String.valueOf (scheduled);
	}

	/**
	 * @param scheduled the scheduled to set
	 */
	public void setScheduledStr (String scheduled) {
		this.scheduled = scheduled == null ? false : Boolean.valueOf (scheduled);
	}

	/**
	 * @return the scheduledDate
	 */
	public Date getScheduledDate () {
		return scheduledDate;
	}

	/**
	 * @param scheduledDate the scheduledDate to set
	 */
	public void setScheduledDate (Date scheduledDate) {
		this.scheduledDate = scheduledDate;
	}

	/**
	 * @return the expirationDate
	 */
	public Date getExpirationDate () {
		return expirationDate;
	}

	/**
	 * @param expirationDate the expirationDate to set
	 */
	public void setExpirationDate (Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	/**
	 * @return the topicId
	 */
	public Long getTopicId () {
		return topicId;
	}

	/**
	 * @param topicId the topicId to set
	 */
	public void setTopicId (Long topicId) {
		this.topicId = topicId;
	}

	/**
	 * @return the validateRecipients
	 */
	public boolean isValidateRecipients () {
		return validateRecipients;
	}

	/**
	 * @param validateRecipients the validateRecipients to set
	 */
	public void setValidateRecipients (boolean validateRecipients) {
		this.validateRecipients = validateRecipients;
	}

	/**
	 * @return the replayMessages
	 */
	public boolean isReplayMessages () {
		return replayMessages;
	}

	/**
	 * @param replayMessages the replayMessages to set
	 */
	public void setReplayMessages (boolean replayMessages) {
		this.replayMessages = replayMessages;
	}

	/**
	 * @return the oneTextContact
	 */
	public boolean isOneTextContact () {
		return oneTextContact;
	}

	/**
	 * @param oneTextContact the oneTextContact to set
	 */
	public void setOneTextContact (boolean oneTextContact) {
		this.oneTextContact = oneTextContact;
	}

	/**
	 * @return the requiresPIN
	 */
	public boolean isRequiresPIN () {
		return requiresPIN;
	}

	/**
	 * @param requiresPIN the requiresPIN to set
	 */
	public void setRequiresPIN (boolean requiresPIN) {
		this.requiresPIN = requiresPIN;
	}

	/**
	 * @return the confirmResponse
	 */
	public boolean isConfirmResponse () {
		return confirmResponse;
	}

	/**
	 * @param confirmResponse the confirmResponse to set
	 */
	public void setConfirmResponse (boolean confirmResponse) {
		this.confirmResponse = confirmResponse;
	}

	/**
	 * @return the reportRecipients
	 */
	public boolean isReportRecipients () {
		return reportRecipients;
	}

	/**
	 * @param reportRecipients the reportRecipients to set
	 */
	public void setReportRecipients (boolean reportRecipients) {
		this.reportRecipients = reportRecipients;
	}

	/**
	 * @return the playGreeting
	 */
	public boolean isPlayGreeting () {
		return playGreeting;
	}

	/**
	 * @param playGreeting the playGreeting to set
	 */
	public void setPlayGreeting (boolean playGreeting) {
		this.playGreeting = playGreeting;
	}

	/**
	 * @return the useAlternates
	 */
	public boolean isUseAlternates () {
		return useAlternates;
	}

	/**
	 * @param useAlternates the useAlternates to set
	 */
	public void setUseAlternates (boolean useAlternates) {
		this.useAlternates = useAlternates;
	}

	/**
	 * @return the selectLanguage
	 */
	public boolean isSelectLanguage () {
		return selectLanguage;
	}

	/**
	 * @param selectLanguage the selectLanguage to set
	 */
	public void setSelectLanguage (boolean selectLanguage) {
		this.selectLanguage = selectLanguage;
	}

	/**
	 * @return the useTopics
	 */
	public boolean isUseTopics () {
		return useTopics;
	}

	/**
	 * @param useTopics the useTopics to set
	 */
	public void setUseTopics (boolean useTopics) {
		this.useTopics = useTopics;
	}

	/**
	 * @return the sendToSubscribers
	 */
	public boolean isSendToSubscribers () {
		return sendToSubscribers;
	}

	/**
	 * @param sendToSubscribers the sendToSubscribers to set
	 */
	public void setSendToSubscribers (boolean sendToSubscribers) {
		this.sendToSubscribers = sendToSubscribers;
	}

	/**
	 * @return the stopAfterEntireMessage
	 */
	public boolean isStopAfterEntireMessage () {
		return stopAfterEntireMessage;
	}

	/**
	 * @param stopAfterEntireMessage the stopAfterEntireMessage to set
	 */
	public void setStopAfterEntireMessage (boolean stopAfterEntireMessage) {
		this.stopAfterEntireMessage = stopAfterEntireMessage;
	}

	/**
	 * @return the stopAnswerPhone
	 */
	public boolean isStopAnswerPhone () {
		return stopAnswerPhone;
	}

	/**
	 * @param stopAnswerPhone the stopAnswerPhone to set
	 */
	public void setStopAnswerPhone (boolean stopAnswerPhone) {
		this.stopAnswerPhone = stopAnswerPhone;
	}

	/**
	 * @return the entireMachine
	 */
	public boolean isEntireMachine () {
		return entireMachine;
	}

	/**
	 * @param entireMachine the entireMachine to set
	 */
	public void setEntireMachine (boolean entireMachine) {
		this.entireMachine = entireMachine;
	}

	/**
	 * @return the partialMachine
	 */
	public boolean isPartialMachine () {
		return partialMachine;
	}

	/**
	 * @param partialMachine the partialMachine to set
	 */
	public void setPartialMachine (boolean partialMachine) {
		this.partialMachine = partialMachine;
	}

	/**
	 * @return the contactCycleDelay
	 */
	public String getContactCycleDelay () {
		return contactCycleDelay;
	}

	/**
	 * @param contactCycleDelay the contactCycleDelay to set
	 */
	public void setContactCycleDelay (String contactCycleDelay) {
		this.contactCycleDelay = contactCycleDelay;
	}

	/**
	 * @return the textDelay
	 */
	public String getTextDelay () {
		return textDelay;
	}

	/**
	 * @param textDelay the textDelay to set
	 */
	public void setTextDelay (String textDelay) {
		this.textDelay = textDelay;
	}

	/**
	 * @return the contactCycles
	 */
	public int getContactCycles () {
		return contactCycles;
	}

	/**
	 * @param contactCycles the contactCycles to set
	 */
	public void setContactCycles (int contactCycles) {
		this.contactCycles = contactCycles;
	}

	/**
	 * @return the expeditedDelivery
	 */
	public boolean isExpeditedDelivery () {
		return expeditedDelivery;
	}

	/**
	 * @param expeditedDelivery the expeditedDelivery to set
	 */
	public void setExpeditedDelivery (boolean expeditedDelivery) {
		this.expeditedDelivery = expeditedDelivery;
	}

	/**
	 * @return the leaveMessage
	 */
	public int getLeaveMessage () {
		return leaveMessage;
	}

	/**
	 * @param leaveMessage the leaveMessage to set
	 */
	public void setLeaveMessage (int leaveMessage) {
		this.leaveMessage = leaveMessage;
	}

	/**
	 * @return the voiceDelivery
	 */
	public int getVoiceDelivery () {
		return voiceDelivery;
	}

	/**
	 * @param voiceDelivery the voiceDelivery to set
	 */
	public void setVoiceDelivery (int voiceDelivery) {
		this.voiceDelivery = voiceDelivery;
	}

	/**
	 * @return the locationOverride
	 */
	public boolean isLocationOverride () {
		return locationOverride;
	}

	/**
	 * @param locationOverride the locationOverride to set
	 */
	public void setLocationOverride (boolean locationOverride) {
		this.locationOverride = locationOverride;
	}

	/**
	 * @return the responseOptions
	 */
	public List<OptionTO> getResponseOptions () {
		return responseOptions;
	}

	/**
	 * @param responseOptions the responseOptions to set
	 */
	public void setResponseOptions (List<OptionTO> responseOptions) {
		this.responseOptions = responseOptions;
	}

	/**
	 * @return the optionsDataTable
	 */
	public HtmlDataTable getOptionsDataTable () {
		return optionsDataTable;
	}

	/**
	 * @param optionsDataTable the optionsDataTable to set
	 */
	public void setOptionsDataTable (HtmlDataTable optionsDataTable) {
		this.optionsDataTable = optionsDataTable;
	}

	/**
	 * @return the notificationMethod
	 */
	public int getNotificationMethod () {
		return notificationMethod;
	}

	/**
	 * @param notificationMethod the notificationMethod to set
	 */
	public void setNotificationMethod (int notificationMethod) {
		this.notificationMethod = notificationMethod;
	}

	/**
	 * @return the notificationAttachment
	 */
	public int getNotificationAttachment () {
		return notificationAttachment;
	}

	/**
	 * @param notificationAttachment the notificationAttachment to set
	 */
	public void setNotificationAttachment (int notificationAttachment) {
		this.notificationAttachment = notificationAttachment;
	}

	/**
	 * @return the sortedAttachments
	 */
	public SelectItem[] getSortedAttachments () {
		final List<SelectItem> selectItems = new ArrayList<SelectItem> (attachments.size ());
		int i = 0;
		for (FileTO file : attachments) {
			selectItems.add (new SelectItem (i++, file.getFileInfo ().getFileName ()));
		}
		return selectItems.toArray (new SelectItem[selectItems.size ()]);
	}

	/**
	 * @param alertJPAService the alertJPAService to set
	 */
	public void setAlertJPAService (AlertJPAService alertJPAService) {
		this.alertJPAService = alertJPAService;
	}

	/**
	 * @return the telephonyMessage
	 */
	public String getTelephonyMessage () {
		return telephonyMessage;
	}

	/**
	 * @param telephonyMessage the telephonyMessage to set
	 */
	public void setTelephonyMessage (String telephonyMessage) {
		this.telephonyMessage = telephonyMessage;
	}

	/**
	 * @return the notEditable
	 */
	public boolean isNotEditable () {
		return publishedDate != null;
	}

	/**
	 * @param scheduler the scheduler to set
	 */
	public void setScheduler (Scheduler scheduler) {
		this.scheduler = scheduler;
	}

	/**
	 * @param voiceMessageService the voiceMessageService to set
	 */
	public void setVoiceMessageService (VoiceMessageService voiceMessageService) {
		this.voiceMessageService = voiceMessageService;
	}

	/**
	 * @return the voice message files
	 * @return
	 */
	public SelectItem[] getVoiceMessageFiles () {
		return voiceMessageFiles;
	}

	/**
	 * @return the selectedVoiceFile
	 */
	public String getSelectedVoiceFile () {
		return selectedVoiceFile;
	}

	/**
	 * @param selectedVoiceFile the selectedVoiceFile to set
	 */
	public void setSelectedVoiceFile (String selectedVoiceFile) {
		this.selectedVoiceFile = selectedVoiceFile;
	}

	/**
	 * @return if the alert is new
	 */
	public boolean isNew () {
		return alertId == null;
	}

	/**
	 * Check for the users roles that can create alerts
	 * @return if the user can create alerts
	 */
	public boolean isAdmin () {
		if (admin == null) {
			admin = securityService.isUserAdmin ();
		}
		return admin;
	}

	/**
	 * @param securityService the securityService to set
	 */
	public void setSecurityService (SecurityService securityService) {
		this.securityService = securityService;
	}

	/**
	 * @return the locations
	 */
	public SelectItem[] getLocations () {
		return locations;
	}

	/**
	 * @param locations the locations to set
	 */
	public void setLocations (SelectItem[] locations) {
		this.locations = locations;
	}

	/**
	 * @return the selectedLocations
	 */
	public SelectItem[] getSelectedLocations () {
		return selectedLocations;
	}

	/**
	 * @param selectedLocations the selectedLocations to set
	 */
	public void setSelectedLocations (SelectItem[] selectedLocations) {
		this.selectedLocations = selectedLocations;
	}

	/**
	 * @param listTypeService the listTypeService to set
	 */
	public void setListTypeService (ListTypeService listTypeService) {
		this.listTypeService = listTypeService;
	}

	/**
	 * @return the selectedLocation
	 */
	public long[] getSelectedLocation () {
		return selectedLocation;
	}

	/**
	 * @param selectedLocation the selectedLocation to set
	 */
	public void setSelectedLocation (long[] selectedLocation) {
		this.selectedLocation = selectedLocation;
	}

	/**
	 * @return the selectedLocationsEmpty
	 */
	public boolean isSelectedLocationsEmpty () {
		return selectedLocations == null || selectedLocations.length == 0;
	}

	/**
	 * @return the toMoveLocation
	 */
	public long[] getToMoveLocation () {
		return toMoveLocation;
	}

	/**
	 * @param toMoveLocation the toMoveLocation to set
	 */
	public void setToMoveLocation (long[] toMoveLocation) {
		this.toMoveLocation = toMoveLocation;
	}

	/**
	 * @return the overrideDefaultStatus
	 */
	public boolean isOverrideDefaultStatus () {
		return overrideDefaultStatus;
	}

	/**
	 * @param overrideDefaultStatus the overrideDefaultStatus to set
	 */
	public void setOverrideDefaultStatus (boolean overrideDefaultStatus) {
		this.overrideDefaultStatus = overrideDefaultStatus;
	}

	/**
	 * @return the leaveMessageReadOnly
	 */
	public boolean isLeaveMessageReadOnly () {
		return leaveMessageReadOnly;
	}

	/**
	 * @param leaveMessageReadOnly the leaveMessageReadOnly to set
	 */
	public void setLeaveMessageReadOnly (boolean leaveMessageReadOnly) {
		this.leaveMessageReadOnly = leaveMessageReadOnly;
	}

	/**
	 * @return the uploadMessage
	 */
	public String getUploadMessage () {
		return uploadMessage;
	}

	/**
	 * @param uploadMessage the uploadMessage to set
	 */
	public void setUploadMessage (String uploadMessage) {
		this.uploadMessage = uploadMessage;
	}

	/**
	 * @param hanPropertiesService the hanPropertiesService to set
	 */
	public void setHanPropertiesService (HanPropertiesService hanPropertiesService) {
		this.hanPropertiesService = hanPropertiesService;
	}

	/**
	 * @return the maxAttachments
	 */
	public int getMaxAttachments () {
		return maxAttachments;
	}

	/**
	 * @return the uploadMsgStyle
	 */
	public String getUploadMsgStyle () {
		return uploadMsgStyle;
	}

	/**
	 * @param uploadMsgStyle the uploadMsgStyle to set
	 */
	public void setUploadMsgStyle (String uploadMsgStyle) {
		this.uploadMsgStyle = uploadMsgStyle;
	}

}
