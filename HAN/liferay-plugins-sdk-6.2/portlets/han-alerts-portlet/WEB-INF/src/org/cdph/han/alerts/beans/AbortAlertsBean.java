package org.cdph.han.alerts.beans;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.alerts.services.AbortAlertService;

/**
 * Baked bean that aborts alerts
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class AbortAlertsBean {
	/** Class log */
	private static final Log log = LogFactory.getLog (AbortAlertsBean.class);
	/** Shows or hides the confirmation dialog */
	private boolean confirmationVisible;
	/** ID of the alert to abort */
	private long alertId;
	/** Service for aborting alerts*/
	private AbortAlertService abortAlertService;

	/**
	 * Displays and hides the confirmation dialog
	 * @param event ActionEvent
	 */
	public void toggleConfirmation (final ActionEvent event) {
		if (!confirmationVisible) {
			final String alertIdString = (String) FacesContext.getCurrentInstance ().getExternalContext (
					).getRequestParameterMap ().get ("alertId");
			alertId = Long.parseLong (alertIdString);
		}
		confirmationVisible = !confirmationVisible;
	}

	/**
	 * @return the confirmationVisible
	 */
	public boolean isConfirmationVisible () {
		return confirmationVisible;
	}

	/**
	 * @param confirmationVisible the confirmationVisible to set
	 */
	public void setConfirmationVisible (boolean confirmationVisible) {
		this.confirmationVisible = confirmationVisible;
	}

	/**
	 * @return the alertId
	 */
	public long getAlertId () {
		return alertId;
	}

	/**
	 * @param alertId the alertId to set
	 */
	public void setAlertId (long alertId) {
		this.alertId = alertId;
	}

	/**
	 * @return the abortAlertService
	 */
	public AbortAlertService getAbortAlertService () {
		return abortAlertService;
	}

	/**
	 * @param abortAlertService the abortAlertService to set
	 */
	public void setAbortAlertService (AbortAlertService abortAlertService) {
		this.abortAlertService = abortAlertService;
	}

}
