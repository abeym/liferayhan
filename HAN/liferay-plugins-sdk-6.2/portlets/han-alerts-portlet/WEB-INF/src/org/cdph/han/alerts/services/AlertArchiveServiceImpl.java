package org.cdph.han.alerts.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.persistence.Alert;
import org.cdph.han.persistence.AlertAttachment;
import org.cdph.han.persistence.AlertAttachmentModel;
import org.cdph.han.persistence.AlertMessage;
import org.cdph.han.persistence.AlertMessageModel;
import org.cdph.han.persistence.AlertOption;
import org.cdph.han.persistence.AlertOptionModel;
import org.cdph.han.persistence.AlertRecipient;
import org.cdph.han.persistence.AlertRecipientCriteria;
import org.cdph.han.persistence.AlertReport;
import org.cdph.han.persistence.AlertReportDevice;
import org.cdph.han.persistence.AlertReportRecipient;
import org.cdph.han.persistence.ArchivedAlert;
import org.cdph.han.persistence.ArchivedAlertAttachment;
import org.cdph.han.persistence.ArchivedAlertMessage;
import org.cdph.han.persistence.ArchivedAlertOption;
import org.cdph.han.persistence.ArchivedAlertRecipient;
import org.cdph.han.persistence.ArchivedAlertRecipientCriteria;
import org.cdph.han.persistence.ArchivedAlertReport;
import org.cdph.han.persistence.ArchivedAlertReportDevice;
import org.cdph.han.persistence.ArchivedAlertReportRecipient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of the AlertArchiveService
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Repository
@Service ("alertArchiveService")
public class AlertArchiveServiceImpl implements AlertArchiveService {
	/** Class log */
	private static final Log log = LogFactory.getLog (AlertArchiveServiceImpl.class);
	/** Persistence context */
	private EntityManager entityManager;
	/** Service for retrieving the user data */
	@Autowired
	private UserDataService userDataService;

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.AlertArchiveService#deleteAlert(long)
	 */
	@Transactional (propagation = Propagation.REQUIRED)
	public void deleteAlert (final long alertId, final boolean archived) {
		if (!archived) {
			archiveAlert (alertId);
		}
		final ArchivedAlert archivedAlert = entityManager.find (ArchivedAlert.class, alertId);
		archivedAlert.setDeleted (true);
		archivedAlert.setModifier (userDataService.getCurrentUserData ());
		entityManager.merge (archivedAlert);
		/*try {
			final long auditTrailId = CounterLocalServiceUtil.increment (HanAuditTrail.class.getName ());
			HanAuditTrail auditTrail = HanAuditTrailLocalServiceUtil.createHanAuditTrail (
					auditTrailId);
			auditTrail.setActionPerformed (AuditTrailAction.ALERT_DELETED.toString ());
			auditTrail.setAlertId (archivedAlert.getId ());
			auditTrail.setDate (archivedAlert.getDateModified ());
			auditTrail.setUserId (archivedAlert.getModifier ().getUserId ());
			auditTrail.setAuditTrailId (auditTrailId);
			HanAuditTrailLocalServiceUtil.addHanAuditTrail (auditTrail);
		} catch (SystemException ex) {
			log.error ("Error creating audit trail.", ex);
			throw new RuntimeException ("Error creating audit trail.", ex);
		}*/
		entityManager.flush ();
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.AlertArchiveService#archiveAlert(long)
	 */
	@Transactional (propagation = Propagation.REQUIRED)
	public long archiveAlert (final long alertId) {
		final Alert alert = entityManager.find (Alert.class, alertId);
		final ArchivedAlert archivedAlert = new ArchivedAlert ();
		archivedAlert.setId (alert.getId ());
		archivedAlert.setAnswerPhone (alert.isAnswerPhone ());
		archivedAlert.setAuthor (alert.getAuthor ());
		archivedAlert.setCategory (alert.getCategory ());
		archivedAlert.setConfirmResponse (alert.isConfirmResponse ());
		archivedAlert.setContact (alert.getContact ());
		archivedAlert.setContactAttempCycles (alert.getContactAttempCycles ());
		archivedAlert.setContactCycleDelay (alert.getContactCycleDelay ());
		archivedAlert.setDateArchived (new Date ());
		archivedAlert.setDateCreated (alert.getDateCreated ());
		archivedAlert.setDateExpired (alert.getDateExpired ());
		archivedAlert.setDateModified (alert.getDateModified ());
		archivedAlert.setDatePublished (alert.getDatePublished ());
		archivedAlert.setDateScheduled (alert.getDateScheduled ());
		archivedAlert.setDuration (alert.getDuration ());
		archivedAlert.setEntireMachine (alert.isEntireMachine ());
		archivedAlert.setEntireMessage (alert.isEntireMessage ());
		archivedAlert.setExpeditedDelivery (alert.isExpeditedDelivery ());
		archivedAlert.setIncidentDate (alert.getIncidentDate ());
		archivedAlert.setLeaveMessage (alert.getLeaveMessage ());
		archivedAlert.setModifier (userDataService.getCurrentUserData ());
		archivedAlert.setName (alert.getName ());
		archivedAlert.setNotificationMethod (alert.getNotificationMethod ());
		archivedAlert.setOneTextContact (alert.isOneTextContact ());
		archivedAlert.setPartialMachine (alert.isPartialMachine ());
		archivedAlert.setPlayGreeting (alert.isPlayGreeting ());
		archivedAlert.setPriority (alert.getPriority ());
		archivedAlert.setPublisher (alert.getPublisher ());
		archivedAlert.setReplayMessages (alert.isReplayMessages ());
		archivedAlert.setReportRecipients (alert.isReportRecipients ());
		archivedAlert.setRequiresPIN (alert.isRequiresPIN ());
		archivedAlert.setSelectLanguage (alert.isSelectLanguage ());
		archivedAlert.setSendToSubscribers (alert.isSendToSubscribers ());
		archivedAlert.setSeverity (alert.getSeverity ());
		archivedAlert.setTextDeviceDelay (alert.getTextDeviceDelay ());
		archivedAlert.setTopic (alert.getTopic ());
		archivedAlert.setUseAlternates (alert.isUseAlternates ());
		archivedAlert.setUseTopics (alert.isUseTopics ());
		archivedAlert.setValidateRecipients (alert.isValidateRecipients ());
		archivedAlert.setVoiceDelivery (alert.getVoiceDelivery ());
		archivedAlert.setAttachments (createAttachments (alert.getAttachments (), archivedAlert));
		archivedAlert.setMessages (createMessages (alert.getMessages (), archivedAlert));
		archivedAlert.setOptions (createOptions (alert.getOptions (), archivedAlert));
		archivedAlert.setRecipients (createRecipients (alert.getRecipients (), archivedAlert));
		archivedAlert.setAlertReports (createAlertReports (alert.getAlertReports (), archivedAlert));
		entityManager.persist (archivedAlert);
		entityManager.remove (alert);
		/*try {
			final long auditTrailId = CounterLocalServiceUtil.increment (HanAuditTrail.class.getName ());
			HanAuditTrail auditTrail = HanAuditTrailLocalServiceUtil.createHanAuditTrail (
					auditTrailId);
			auditTrail.setActionPerformed (AuditTrailAction.ALERT_ARCHIVED.toString ());
			auditTrail.setAlertId (archivedAlert.getId ());
			auditTrail.setDate (archivedAlert.getDateModified ());
			auditTrail.setUserId (archivedAlert.getModifier ().getUserId ());
			auditTrail.setAuditTrailId (auditTrailId);
			HanAuditTrailLocalServiceUtil.addHanAuditTrail (auditTrail);
		} catch (SystemException ex) {
			log.error ("Error creating audit trail.", ex);
			throw new RuntimeException ("Error creating audit trail.", ex);
		}*/
		return archivedAlert.getId ();
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.AlertArchiveService#archiveAlert(long)
	 */
	@Transactional (propagation = Propagation.REQUIRED)
	public void archiveAlertFromJob (final long alertId) {
		final Alert alert = entityManager.find (Alert.class, alertId);
		final ArchivedAlert archivedAlert = new ArchivedAlert ();
		archivedAlert.setId (alert.getId ());
		archivedAlert.setAnswerPhone (alert.isAnswerPhone ());
		archivedAlert.setAuthor (alert.getAuthor ());
		archivedAlert.setCategory (alert.getCategory ());
		archivedAlert.setConfirmResponse (alert.isConfirmResponse ());
		archivedAlert.setContact (alert.getContact ());
		archivedAlert.setContactAttempCycles (alert.getContactAttempCycles ());
		archivedAlert.setContactCycleDelay (alert.getContactCycleDelay ());
		archivedAlert.setDateArchived (new Date ());
		archivedAlert.setDateCreated (alert.getDateCreated ());
		archivedAlert.setDateExpired (alert.getDateExpired ());
		archivedAlert.setDateModified (alert.getDateModified ());
		archivedAlert.setDatePublished (alert.getDatePublished ());
		archivedAlert.setDateScheduled (alert.getDateScheduled ());
		archivedAlert.setDuration (alert.getDuration ());
		archivedAlert.setEntireMachine (alert.isEntireMachine ());
		archivedAlert.setEntireMessage (alert.isEntireMessage ());
		archivedAlert.setExpeditedDelivery (alert.isExpeditedDelivery ());
		archivedAlert.setIncidentDate (alert.getIncidentDate ());
		archivedAlert.setLeaveMessage (alert.getLeaveMessage ());
		archivedAlert.setModifier (alert.getModifier ());
		archivedAlert.setName (alert.getName ());
		archivedAlert.setNotificationMethod (alert.getNotificationMethod ());
		archivedAlert.setOneTextContact (alert.isOneTextContact ());
		archivedAlert.setPartialMachine (alert.isPartialMachine ());
		archivedAlert.setPlayGreeting (alert.isPlayGreeting ());
		archivedAlert.setPriority (alert.getPriority ());
		archivedAlert.setPublisher (alert.getPublisher ());
		archivedAlert.setReplayMessages (alert.isReplayMessages ());
		archivedAlert.setReportRecipients (alert.isReportRecipients ());
		archivedAlert.setRequiresPIN (alert.isRequiresPIN ());
		archivedAlert.setSelectLanguage (alert.isSelectLanguage ());
		archivedAlert.setSendToSubscribers (alert.isSendToSubscribers ());
		archivedAlert.setSeverity (alert.getSeverity ());
		archivedAlert.setTextDeviceDelay (alert.getTextDeviceDelay ());
		archivedAlert.setTopic (alert.getTopic ());
		archivedAlert.setUseAlternates (alert.isUseAlternates ());
		archivedAlert.setUseTopics (alert.isUseTopics ());
		archivedAlert.setValidateRecipients (alert.isValidateRecipients ());
		archivedAlert.setVoiceDelivery (alert.getVoiceDelivery ());
		archivedAlert.setAttachments (createAttachments (alert.getAttachments (), archivedAlert));
		archivedAlert.setMessages (createMessages (alert.getMessages (), archivedAlert));
		archivedAlert.setOptions (createOptions (alert.getOptions (), archivedAlert));
		archivedAlert.setRecipients (createRecipients (alert.getRecipients (), archivedAlert));
		archivedAlert.setAlertReports (createAlertReports (alert.getAlertReports (), archivedAlert));
		entityManager.persist (archivedAlert);
		entityManager.remove (alert);
		/*try {
			final long auditTrailId = CounterLocalServiceUtil.increment (HanAuditTrail.class.getName ());
			HanAuditTrail auditTrail = HanAuditTrailLocalServiceUtil.createHanAuditTrail (
					auditTrailId);
			auditTrail.setActionPerformed (AuditTrailAction.ALERT_MODIFIED.toString ());
			auditTrail.setAlertId (archivedAlert.getId ());
			auditTrail.setDate (archivedAlert.getDateModified ());
			auditTrail.setUserId (archivedAlert.getModifier ().getUserId ());
			auditTrail.setAuditTrailId (auditTrailId);
			HanAuditTrailLocalServiceUtil.addHanAuditTrail (auditTrail);
		} catch (SystemException ex) {
			log.error ("Error creating audit trail.", ex);
			throw new RuntimeException ("Error creating audit trail.", ex);
		}*/
	}

	/**
	 * Creates a list of alert attachments to move to the archive
	 * @param attachments Collection of AlertAttachment objects to move to the archive
	 * @param archivedAlert to which the attachments belong
	 * @return List of ArchivedAlertAttachment objects
	 */
	private Collection<ArchivedAlertAttachment> createAttachments (final Collection<AlertAttachment> attachments,
			final ArchivedAlert archivedAlert) {
		final List<ArchivedAlertAttachment> archivedAttachments = new ArrayList<ArchivedAlertAttachment> ();
		ArchivedAlertAttachment archivedAttachment;
		for (AlertAttachmentModel attachment : attachments) {
			archivedAttachment = new ArchivedAlertAttachment ();
			archivedAttachment.setArchivedAlert (archivedAlert);
			archivedAttachment.setAttachment (attachment.getAttachment ());
			archivedAttachment.setMimeType (attachment.getMimeType ());
			archivedAttachment.setName (attachment.getName ());
			archivedAttachments.add (archivedAttachment);
		}
		return archivedAttachments;
	}

	/**
	 * Creates a list of alert messages to move to the archive
	 * @param messages Collection of AlertMessage objects to be moved to the archive
	 * @param archivedAlert to which the messages belong
	 * @return List of ArchivedAlertMessage objects to persist
	 */
	private Collection<ArchivedAlertMessage> createMessages (final Collection<AlertMessage> messages,
			final ArchivedAlert archivedAlert) {
		final List<ArchivedAlertMessage> archivedMessages = new ArrayList<ArchivedAlertMessage> ();
		ArchivedAlertMessage archivedMessage;
		for (AlertMessageModel message : messages) {
			archivedMessage = new ArchivedAlertMessage ();
			archivedMessage.setAbstractText (message.getAbstractText ());
			archivedMessage.setArchivedAlert (archivedAlert);
			//archivedMessage.setAttachment (message.)
			archivedMessage.setLocale (message.getLocale ());
			archivedMessage.setPortalMessage (message.getPortalMessage ());
			archivedMessage.setTelephonyMessage (message.getTelephonyMessage ());
			archivedMessages.add (archivedMessage);
		}
		return archivedMessages;
	}

	/**
	 * Creates all the options objects to be stored in the archive
	 * @param options Collection of AlertOption to move to the archive
	 * @param archivedAlert to which the response options belong
	 * @return List with the options to archive
	 */
	private Collection<ArchivedAlertOption> createOptions (final Collection<AlertOption> options,
			final ArchivedAlert archivedAlert) {
		final List<ArchivedAlertOption> archivedOptions = new ArrayList<ArchivedAlertOption> ();
		ArchivedAlertOption archivedOption;
		for (AlertOptionModel option : options) {
			archivedOption = new ArchivedAlertOption ();
			archivedOption.setAction (option.getAction ());
			archivedOption.setArchivedAlert (archivedAlert);
			archivedOption.setCascade (option.getCascade ());
			archivedOption.setLocale (option.getLocale ());
			archivedOption.setMessage (option.getMessage ());
			archivedOption.setOptionId (option.getOptionId ());
			archivedOptions.add (archivedOption);
		}
		return archivedOptions;
	}

	/**
	 * Creates all the recipients to be moved to the archive tables
	 * @param recipients Collection with the recipients of the alert
	 * @param archivedAlert to which the recipients belong
	 * @return List with the attachments to move to the archive
	 */
	private Collection<ArchivedAlertRecipient> createRecipients (final Collection<AlertRecipient> recipients,
			final ArchivedAlert archivedAlert) {
		final List<ArchivedAlertRecipient> archivedRecipients = new ArrayList<ArchivedAlertRecipient> ();
		ArchivedAlertRecipient archivedRecipient = null;
		for (AlertRecipient recipient : recipients) {
			archivedRecipient = new ArchivedAlertRecipient ();
			archivedRecipient.setArchivedAlert (archivedAlert);
			archivedRecipient.setAreaField (recipient.getAreaField ());
			archivedRecipient.setNonHanUser (recipient.getNonHanUser ());
			archivedRecipient.setNonHanUserGroup (recipient.getNonHanUserGroup ());
			archivedRecipient.setOrganization (recipient.getOrganization ());
			archivedRecipient.setRole (recipient.getRole ());
			archivedRecipient.setUser (recipient.getUser ());
			archivedRecipient.setUserCustomGroup (recipient.getUserCustomGroup ());
			archivedRecipients.add (archivedRecipient);
			if (recipient.getCriterias () != null && !recipient.getCriterias ().isEmpty ()) {
				ArchivedAlertRecipientCriteria criteria;
				archivedRecipient.setCriterias (new ArrayList<ArchivedAlertRecipientCriteria> ());
				for (AlertRecipientCriteria criteriaToArchive : recipient.getCriterias ()) {
					criteria = new ArchivedAlertRecipientCriteria ();
					criteria.setRecipient (archivedRecipient);
					criteria.setAreaField (criteriaToArchive.getAreaField ());
					criteria.setOrganization (criteriaToArchive.getOrganization ());
					criteria.setOrganizationType (criteriaToArchive.getOrganizationType ());
					criteria.setRole (criteriaToArchive.getRole ());
					archivedRecipient.getCriterias ().add (criteria);
				}
			}
		}
		return archivedRecipients;
	}

	/**
	 * Creates a list of reports to archive
	 * @param reports source list of reports
	 * @param archivedAlert to which the reports belong
	 * @return the list of reports ready to archive
	 */
	private Collection<ArchivedAlertReport> createAlertReports (final Collection<AlertReport> reports,
			final ArchivedAlert archivedAlert) {
		final List<ArchivedAlertReport> archivedReports = new ArrayList<ArchivedAlertReport> ();
		ArchivedAlertReport archivedReport = null;
		for (AlertReport report : reports) {
			archivedReport = new ArchivedAlertReport ();
			archivedReport.setAlert (archivedAlert);
			archivedReport.setAnsweringMachine (report.getAnsweringMachine ());
			archivedReport.setBusy (report.getBusy ());
			archivedReport.setCompleted (report.getCompleted ());
			archivedReport.setHangUp (report.getHangUp ());
			archivedReport.setInvalidResponse (report.getInvalidResponse ());
			archivedReport.setLeftMessage (report.getLeftMessage ());
			archivedReport.setNoAnswer (report.getNoAnswer ());
			archivedReport.setNotAtThisLocation (report.getNotAtThisLocation ());
			archivedReport.setNotificationFinished (report.isNotificationFinished ());
			archivedReport.setNotificationReportId (report.getNotificationReportId ());
			archivedReport.setOther (report.getOther ());
			archivedReport.setPhoneTime (report.getPhoneTime ());
			archivedReport.setRecipients (createReportRecipients (report.getRecipients (), archivedReport));
			archivedReport.setTotalCalls (report.getTotalCalls ());
			archivedReport.setTotalContacted (report.getTotalContacted ());
			archivedReport.setTotalEmails (report.getTotalEmails ());
			archivedReport.setTotalFaxes (report.getTotalFaxes ());
			archivedReport.setTotalPages (report.getTotalPages ());
			archivedReport.setTotalRecipients (report.getTotalRecipients ());
			archivedReport.setTotalResponded (report.getTotalResponded ());
			archivedReport.setTotalSms (report.getTotalSms ());
			archivedReport.setWrongAddressTel (report.getWrongAddressTel ());
			archivedReport.setLastFetch (report.getLastFetch ());
			archivedReports.add (archivedReport);
		}
		return archivedReports;
	}

	/**
	 * Creates a list of report recipients to be archived
	 * @param recipients the source list of recipients
	 * @param archivedReport archived report to which the recipients belong
	 * @return the list to be archived
	 */
	private Collection<ArchivedAlertReportRecipient> createReportRecipients (
			final Collection<AlertReportRecipient> recipients,
			final ArchivedAlertReport archivedReport) {
		final List<ArchivedAlertReportRecipient> archivedRecipients =
			new ArrayList<ArchivedAlertReportRecipient> ();
		ArchivedAlertReportRecipient archivedRecipient = null;
		for (AlertReportRecipient recipient : recipients) {
			archivedRecipient = new ArchivedAlertReportRecipient ();
			archivedRecipient.setDevices (createDevices (recipient.getDevices (), archivedRecipient));
			archivedRecipient.setFirstName (recipient.getFirstName ());
			archivedRecipient.setLastName (recipient.getLastName ());
			archivedRecipient.setRecipientReportId (recipient.getRecipientReportId ());
			archivedRecipient.setReport (archivedReport);
			archivedRecipients.add (archivedRecipient);
		}
		return archivedRecipients;
	}

	/**
	 * Creates a list of devices to be archived
	 * @param devices the source list of devices to archive
	 * @param archivedRecipient archived recipient to which the devices belong
	 * @return list of devices ready to archive
	 */
	private Collection<ArchivedAlertReportDevice> createDevices (
			final Collection<AlertReportDevice> devices,
			final ArchivedAlertReportRecipient archivedRecipient) {
		final List<ArchivedAlertReportDevice> archivedDevices =
			new ArrayList<ArchivedAlertReportDevice> ();
		ArchivedAlertReportDevice archivedDevice = null;	
		for (AlertReportDevice device : devices) {
			archivedDevice = new ArchivedAlertReportDevice ();
			archivedDevice.setRecipient (archivedRecipient);
			archivedDevice.setAddress (device.getAddress ());
			archivedDevice.setContactId (device.getContactId ());
			archivedDevice.setDescription (device.getDescription ());
			archivedDevice.setDuration (device.getDuration ());
			archivedDevice.setResponse (device.getResponse ());
			archivedDevice.setResult (device.getResult ());
			archivedDevice.setTimeResponded (device.getTimeResponded ());
			archivedDevice.setTimeSent (device.getTimeSent ());
			archivedDevices.add (archivedDevice);
		}
		return archivedDevices;
	}

	/**
	 * @param entityManager the entityManager to set
	 */
	@PersistenceContext
	public void setEntityManager (EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
