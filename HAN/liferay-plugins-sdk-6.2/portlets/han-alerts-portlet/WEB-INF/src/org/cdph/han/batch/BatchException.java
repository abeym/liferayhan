package org.cdph.han.batch;

public class BatchException extends Exception {

	public BatchException() {
		super();
	}
	
	public BatchException(String message) {
		super(message);
	}
	
	public BatchException(Throwable t) {
		super(t);
	}
}
