package org.cdph.han.alerts.dto;

import java.io.Serializable;

/**
 * This class represents an attachment of an alert
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class AttachmentTO implements Serializable {
	/** Class serial version UID */
	private static final long serialVersionUID = 6502756948090934648L;
	/** Attachment */
	private byte[] attachment;
	/** Mime type of the attachment */
	private String mimeType;
	/** Name of the file */
	private String name;

	/**
	 * Constructor
	 * @param attachment byte array containing the attachment
	 * @param mimeType of the attachment
	 * @param name of the attachment
	 */
	public AttachmentTO (final byte[] attachment, final String mimeType, final String name) {
		this.attachment = attachment;
		this.mimeType = mimeType;
		this.name = name;
	}

	/**
	 * @return the attachment
	 */
	public byte[] getAttachment () {
		return attachment;
	}

	/**
	 * @return the mimeType
	 */
	public String getMimeType () {
		return mimeType;
	}

	/**
	 * @return the name
	 */
	public String getName () {
		return name;
	}

}
