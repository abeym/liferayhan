package org.cdph.han.persistence;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Entity class representing a member of the custom group.
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Entity (name = "CustomGroupMember")
@Table (name = "cust_group_members", schema = "lportal")
public class CustomGroupMember implements Serializable {
	/** Class serial version UID */
	private static final long serialVersionUID = 1288036391654941924L;
	/** Primary key */
	@Id
	@Column (name = "memberId", nullable = false)
	@GeneratedValue (strategy = GenerationType.TABLE)
	private long memberId;
	/** Group of the member */
	@ManyToOne
	@JoinColumn (name = "groupId", nullable = false)
	private UserCustomGroup userCustomGroup;
	/** Organization */
	@ManyToOne
	@JoinColumn (name = "organizationId", nullable = true)
    private Organization organization;
	/** Area/Field */
	@ManyToOne
	@JoinColumn (name = "areaFieldId", nullable = true)
    private AreaField areaField;
	/** Role */
	@ManyToOne
	@JoinColumn (name = "roleId", nullable = true)
    private Role role;
	/** User data of the member */
	@ManyToOne
	@JoinColumn (name = "userId", nullable = true)
    private UserData userData;
	/** Non Han Group */
	@ManyToOne
	@JoinColumn (name = "non_han_group_id", nullable = true)
	private NonHanUserGroup nonHanUserGroup;
	/** Non Han User */
	@ManyToOne
	@JoinColumn (name = "non_han_user_id", nullable = true)
	private NonHanUser nonHanUser;
	/** Child group */
	@OneToOne
	@JoinColumn (name = "child_groupId", nullable = true)
	private UserCustomGroup childGroup;
	/** Flag for Chicago only entities */
	@Column (name = "chicago_only")
	private boolean chicagoOnly;
	/** Criteria of the current member */
	@OneToMany (mappedBy = "member", cascade = {CascadeType.ALL})
	private Collection<CustomGroupCriteria> criterias;

	/**
	 * @return the nonHanUser
	 */
	public NonHanUser getNonHanUser () {
		return nonHanUser;
	}

	/**
	 * @param nonHanUser the nonHanUser to set
	 */
	public void setNonHanUser (NonHanUser nonHanUser) {
		this.nonHanUser = nonHanUser;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean equal = false;
		if (obj != null && obj instanceof CustomGroupMember) {
			final CustomGroupMember other = (CustomGroupMember) obj;
			equal = other.getMemberId () == memberId;
		}
		return equal;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		return Long.valueOf (memberId).hashCode ();
	}

	/**
	 * @return the memberId
	 */
	public long getMemberId () {
		return memberId;
	}

	/**
	 * @return the userCustomGroup
	 */
	public UserCustomGroup getUserCustomGroup () {
		return userCustomGroup;
	}

	/**
	 * @return the organization
	 */
	public Organization getOrganization () {
		return organization;
	}

	/**
	 * @return the areaField
	 */
	public AreaField getAreaField () {
		return areaField;
	}

	/**
	 * @return the role
	 */
	public Role getRole () {
		return role;
	}

	/**
	 * @return the userData
	 */
	public UserData getUserData () {
		return userData;
	}

	/**
	 * @param memberId the memberId to set
	 */
	public void setMemberId (long memberId) {
		this.memberId = memberId;
	}

	/**
	 * @param userCustomGroup the userCustomGroup to set
	 */
	public void setUserCustomGroup (UserCustomGroup userCustomGroup) {
		this.userCustomGroup = userCustomGroup;
	}

	/**
	 * @param organization the organization to set
	 */
	public void setOrganization (Organization organization) {
		this.organization = organization;
	}

	/**
	 * @param areaField the areaField to set
	 */
	public void setAreaField (AreaField areaField) {
		this.areaField = areaField;
	}

	/**
	 * @param role the role to set
	 */
	public void setRole (Role role) {
		this.role = role;
	}

	/**
	 * @param userData the userData to set
	 */
	public void setUserData (UserData userData) {
		this.userData = userData;
	}

	/**
	 * @return the nonHanUserGroup
	 */
	public NonHanUserGroup getNonHanUserGroup () {
		return nonHanUserGroup;
	}

	/**
	 * @param nonHanUserGroup the nonHanUserGroup to set
	 */
	public void setNonHanUserGroup (NonHanUserGroup nonHanUserGroup) {
		this.nonHanUserGroup = nonHanUserGroup;
	}

	/**
	 * @return the childGroup
	 */
	public UserCustomGroup getChildGroup () {
		return childGroup;
	}

	/**
	 * @param childGroup the childGroup to set
	 */
	public void setChildGroup (UserCustomGroup childGroup) {
		this.childGroup = childGroup;
	}

	/**
	 * @return the chicagoOnly
	 */
	public boolean isChicagoOnly () {
		return chicagoOnly;
	}

	/**
	 * @param chicagoOnly the chicagoOnly to set
	 */
	public void setChicagoOnly (boolean chicagoOnly) {
		this.chicagoOnly = chicagoOnly;
	}

	/**
	 * @return the criterias
	 */
	public Collection<CustomGroupCriteria> getCriterias () {
		return criterias;
	}

	/**
	 * @param criterias the criterias to set
	 */
	public void setCriterias (Collection<CustomGroupCriteria> criterias) {
		this.criterias = criterias;
	}

}
