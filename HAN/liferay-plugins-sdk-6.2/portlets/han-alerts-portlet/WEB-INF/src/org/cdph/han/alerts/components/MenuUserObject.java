package org.cdph.han.alerts.components;

import javax.swing.tree.DefaultMutableTreeNode;

import com.icesoft.faces.component.tree.IceUserObject;

/**
 * Class to add more state functionality to the IceUserObject, the added functionality will allow the distinction
 * of the search method to call
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class MenuUserObject extends IceUserObject {
	/** Type of search associated to the object */
	private SearchType type;

	/**
	 * Overwrite of the parent constructor
	 * @param wrapper DefaultMutableTreeNode wrapper object
	 */
	public MenuUserObject (final DefaultMutableTreeNode wrapper) {
		super (wrapper);

		setLeafIcon ("tree_document.gif");
		setBranchContractedIcon ("tree_folder_closed.gif");
		setBranchExpandedIcon ("tree_folder_open.gif");
		setExpanded (true);
	}

	/**
	 * @return the type
	 */
	public SearchType getType () {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType (SearchType type) {
		this.type = type;
	}

	/**
	 * Enum to list all the available searchs in the HAN alert portlet
	 * @author Horacio Oswaldo Ferro
	 * @version 1.0
	 */
	public static enum SearchType {
		ROOT_MENU,
		ALERTS_REQUIRING_ACTION,
		CURRENT_MENU,
		CURRENT_BY_PUBLICATION_DATE, CURRENT_BY_TOPIC, CURRENT_BY_LEVEL_OF_ALERT,
		ARCHIVED_MENU,
		ARCHIVED_BY_PUBLICATION_DATE, ARCHIVED_BY_TOPIC, ARCHIVED_BY_LEVEL_OF_ALERT,
		DRAFT_MENU,
		DRAFT_BY_LAST_MODIFIED,
		SCHEDULED_MENU,
		SCHEDULED_BY_SCHEDULED_DATE,
		ABORT_TRACK_ALERTS,
		SHOW_ALERT_DETAIL,
		ABORT_TRACK_ALERTS_DETAIL,
		ABORT_TRACK_ALERTS_EXTENDED_DETAIL
	}

}
