package org.cdph.han.batch.beans;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.EventObject;
import java.util.List;
import java.util.ResourceBundle;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.batch.BatchException;
import org.cdph.han.batch.service.BatchService;
import org.cdph.han.persistence.BatchStatus;
import org.cdph.han.persistence.ContactDeviceType;
import org.cdph.han.persistence.NonHanUser;
import org.cdph.han.persistence.NonHanUserGroup;
import org.cdph.han.persistence.UserData;

import com.icesoft.faces.component.ext.RowSelectorEvent;
import com.icesoft.faces.component.inputfile.InputFile;

public class BatchUploadBean implements Serializable {

	private static final long serialVersionUID = -1325348305815197202L;

	/** Class logger */
	private static Log log = LogFactory.getLog(BatchUploadBean.class);

	// File sizes used to generate formatted label
	public static final long MEGABYTE_LENGTH_BYTES = 1048000l;
	public static final long KILOBYTE_LENGTH_BYTES = 1024l;

	/** Spring injected bean for persistence and biz functionality */
	private BatchService service;

	// latest file uploaded by client
	private InputFileData currentFile;
	
	// file upload completed percent (Progress)
	private int fileProgress;

	private String groupName;
	private String groupDescription;
	private String eventDescription;
	private List<ContactDeviceType> contactPrefs;
	
	

	public BatchUploadBean() {
		log.debug("Constructor()...");

		contactPrefs = new ArrayList<ContactDeviceType>(5);
		contactPrefs.add(ContactDeviceType.PERSONAL_EMAIL);
		contactPrefs.add(ContactDeviceType.HOME_PHONE);
		contactPrefs.add(ContactDeviceType.WORK_EMAIL);
		contactPrefs.add(ContactDeviceType.WORK_PHONE);
		contactPrefs.add(ContactDeviceType.CELL_PHONE);

	}

	/**
	 * <p>
	 * Action method which is triggered when a user clicks on the submit 
	 * button. It validates that the file has been uploaded and that the 
	 * contact preferences have been set.
	 * </p>
	 *  @return next action for navigation
	 */
	public String submitButtonAction() {
		log.debug("In submitButtonAction...");
		String nextAction = "";

		ResourceBundle bundle = ResourceBundle.getBundle("messages",
				FacesContext.getCurrentInstance().getViewRoot().getLocale());

		if (contactPrefs == null || contactPrefs.isEmpty()) {
			String msg = bundle
					.getString("batchUpload.error.missing.preferences");
			FacesMessage message = new FacesMessage(
					FacesMessage.SEVERITY_ERROR, msg, msg);
			FacesContext.getCurrentInstance().addMessage("", message);
			nextAction = "failure";
		}

		//TODO Check for file upload status in case the user first uploads a valid file, then
		// a file that exceeds the limit and then presses submit. Validate that the status of the last
		//file uploaded is consistent/valid.
		if (currentFile == null || currentFile.getStatus() != InputFile.SAVED) {
			String msg = bundle.getString("batchUpload.error.missing.file");
			FacesMessage message = new FacesMessage(
					FacesMessage.SEVERITY_ERROR, msg, msg);
			FacesContext.getCurrentInstance().addMessage(null, message);
			nextAction = "failure";
		}
		Pattern pattern = Pattern.compile ("[^\\s0-9a-zA-Z]");
		if (pattern.matcher (groupName.subSequence (0, groupName.length ())).find ()) {
			String msg = bundle.getString("batchUpload.error.invalid.group.name");
			FacesMessage message = new FacesMessage(
					FacesMessage.SEVERITY_ERROR, msg, msg);
			FacesContext.getCurrentInstance().addMessage(null, message);
			nextAction = "failure";
		}

		if (nextAction != null && !nextAction.equals("failure")) {
			prepareAndSubmitBatchRequest();
			nextAction = "back2Search";
			clearForm();
		}

		return nextAction;

	}
	
	private void clearForm(){
		//contactPrefs = null;
		currentFile = null;
		fileProgress = 0;
		groupName = null;
		groupDescription = null;
		eventDescription = null;
	}

	/**
	 * Creates a NonHanUserGroup from the form information
	 * and registers the request using the spring service bean
	 */
	private void prepareAndSubmitBatchRequest() {
		log.debug("In submitButtonListener...");

		Principal principal = FacesContext.getCurrentInstance()
				.getExternalContext().getUserPrincipal();

		if (log.isDebugEnabled()) {
			log.debug("Group Name: " + groupName);
			log.debug("Group Desc: " + groupDescription);
			log.debug("Event Description: " + eventDescription);
			for (ContactDeviceType pref : contactPrefs) {
				log.debug(pref.name());
			}
			if (currentFile != null && currentFile.getFileInfo() != null) {
				log
						.debug("Filename:"
								+ currentFile.getFileInfo().getFileName());
			}
			log.debug("Principal name: " + principal.getName());
		}

		UserData userData = service.getUserDataById(Long.parseLong(principal
				.getName()));

		if (log.isDebugEnabled()) {
			log.debug("User Last Name: " + userData.getContact().getLastName());
		}

		NonHanUserGroup group = new NonHanUserGroup();

		group.setUser(userData);
		group.setName(groupName);
		group.setDescription(groupDescription);
		group.setEventDescription(eventDescription);
		group.setCreatedDate(new Date());
		group.setFile(getBytesFromFile(currentFile.getFile()));
		group.setContactPrefList(contactPrefs);
		group.setStatus(BatchStatus.QUEUED);

		try {
			service.registerUploadBatchRequest(group);
			service.enqueueBatchUploadRequest(group.getId());
		} catch (BatchException be) {
			log.error(be.getMessage(), be);
		}
		

	}
	
	
	/**
	 * <p>
	 * Action event method which is triggered when a user clicks on the upload
	 * file button. Uploaded files are added to a list so that user have the
	 * option to delete them programatically. Any errors that occurs during the
	 * file uploaded are added the messages output.
	 * </p>
	 * 
	 * @param event
	 *            jsf action event.
	 */
	public void uploadFile(ActionEvent event) {
		//TODO Enable CleanUp Listener on web.xml and change to relative path for upload file directory
		log.debug("In uploadFile...");
		InputFile inputFile = (InputFile) event.getSource();
		log.debug("InputFile Status: " + inputFile.getStatus());
		log.debug("InputFile Status: " + inputFile.getStatus());
		if (inputFile.getStatus() == InputFile.SAVED) {
			log.debug("File Name:" + inputFile.getFileInfo().getFileName());
			log.debug("File Size:" + inputFile.getFileInfo().getSize());
			log.debug("File ContentType:"
					+ inputFile.getFileInfo().getContentType());
			currentFile = new InputFileData(inputFile);
		}
	}

	/**
	 * <p>
	 * This method is bound to the inputFile component and is executed multiple
	 * times during the file upload process. Every call allows the user to finds
	 * out what percentage of the file has been uploaded. This progress
	 * information can then be used with a progressBar component for user
	 * feedback on the file upload progress.
	 * </p>
	 * 
	 * @param event
	 *            holds a InputFile object in its source which can be probed for
	 *            the file upload percentage complete.
	 */
	public void fileUploadProgress(EventObject event) {
		InputFile ifile = (InputFile) event.getSource();
		fileProgress = ifile.getFileInfo().getPercent();
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getGroupDescription() {
		return groupDescription;
	}

	public void setGroupDescription(String groupDescription) {
		this.groupDescription = groupDescription;
	}

	public String getEventDescription() {
		return eventDescription;
	}

	public void setEventDescription(String eventDescription) {
		this.eventDescription = eventDescription;
	}

	public int getFileProgress() {
		return fileProgress;
	}

	public List<ContactDeviceType> getContactPrefs() {
		return contactPrefs;
	}

	public void setContactPrefs(List<ContactDeviceType> contactPrefs) {
		this.contactPrefs = contactPrefs;
	}

	public void setService(BatchService service) {
		this.service = service;
	}

	private byte[] getBytesFromFile(File file){
		log.debug("In getBytesFromFile(file)...");
        // Create the byte array to hold the data
		byte[] bytes = null;
		
		try {
			InputStream is = new FileInputStream(file);
   
			long length = file.length();
   
			bytes = new byte[(int)length];
   
			// Read in the bytes
			int offset = 0;
			int numRead = 0;
			while (offset < bytes.length
			       && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
			    offset += numRead;
			}
   
			// Ensure all the bytes have been read in
			if (offset < bytes.length) {
			    throw new IOException("Could not completely read file "+file.getName());
			}
   
			// Close the input stream and return bytes
			is.close();
		} catch (Exception ex) {
			log.error(ex.getMessage(), ex);
		} 
        
        return bytes;
    }

}
