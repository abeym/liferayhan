package org.cdph.han.batch.beans;

import javax.faces.event.ActionEvent;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.batch.dto.NonHanUserDTO;
import org.cdph.han.batch.dto.NonHanUserGroupDTO;
import org.cdph.han.batch.service.BatchService;
import org.cdph.han.persistence.NonHanUserGroup;

import com.icesoft.faces.component.ext.RowSelectorEvent;

public class BatchUploadSearchBean {

	/** Class logger */
	private static final Log log = LogFactory
			.getLog(BatchUploadSearchBean.class);

	private String groupName;
	private String groupDesc;
	private NonHanUserGroupDTO groups;
	private NonHanUserDTO batchMembers;
	
	private boolean popUpRendered = false;
	private int currentRow=-1;

	/** Spring injected bean for persistence and biz functionality */
	private BatchService service;

	/**
	 * Listener for the form search button click action.
	 * 
	 * @param e
	 *            click action event.
	 */
	public void searchButtonListener(ActionEvent e) {
		log.debug("In searchButtonListener...");
		groups.clearCache();
		if(batchMembers != null){
			batchMembers.clearCache();
			batchMembers.setNonHanGroupId(null);
		}
		groups.setGroupName(groupName);
		groups.setGroupDesc(groupDesc);
		
	}

	/**
	 * SelectionListener bound to the ice:rowSelector component. Called when a
	 * row from the batch group table is selected in the UI.
	 * 
	 * @param event
	 *            from the ice:rowSelector component
	 */
	public void rowSelectionListener(RowSelectorEvent event) {
		log.debug("In rowSelectionListener...");
		// clear our list, so that we can build a new one
		if (batchMembers != null) {
			batchMembers.clearCache();
		}
		
		currentRow = event.getRow();

		for (NonHanUserGroup group : groups.getWrappedData()) {
			if (group.isSelected()) {
				log.debug("Requesting members for group ID: " + group.getId());
				if(batchMembers == null){
					batchMembers = new NonHanUserDTO(service);
				}
				batchMembers.setNonHanGroupId(group.getId());
			}
		}
	}
	
	/**
	 * Method triggered by the deletion confirmation popup.
	 * @param event
	 */
	public void deleteGroup(ActionEvent event){
		log.debug("In deleteGroup()...");

		if(currentRow > -1){		
			NonHanUserGroup group = groups.getWrappedData().get(currentRow);
			log.debug("Soft deleting: " + group.getId());
			service.softDeleteNonHanGroup(group);
		}
		
		popUpRendered = false;
		groups.clearCache();
		
		if(batchMembers != null){
			batchMembers.clearCache();
			batchMembers.setNonHanGroupId(null);
		}
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getGroupDesc() {
		return groupDesc;
	}

	public void setGroupDesc(String groupDesc) {
		this.groupDesc = groupDesc;
	}

	public NonHanUserGroupDTO getSearchResults() {
		if (groups == null) {
			groups = new NonHanUserGroupDTO(service);
		}
		return groups;
	}

	public void setSearchResults(NonHanUserGroupDTO searchResults) {
		this.groups = searchResults;
	}

	public void setService(BatchService service) {
		this.service = service;
	}

	public NonHanUserDTO getBatchMembers() {
		return batchMembers;
	}

	public void setBatchMembers(NonHanUserDTO batchMembers) {
		this.batchMembers = batchMembers;
	}
	
	public void togglePopUp(ActionEvent event) {
		popUpRendered = !popUpRendered;
    }
	
	public boolean getPopUpRendered() {
        return popUpRendered;
    }


}