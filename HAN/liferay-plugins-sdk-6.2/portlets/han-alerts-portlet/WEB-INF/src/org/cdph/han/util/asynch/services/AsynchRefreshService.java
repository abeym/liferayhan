package org.cdph.han.util.asynch.services;

import org.cdph.han.util.asynch.beans.UpdatableBean;
import org.cdph.han.util.asynch.dto.AsynchRequestTO;
import org.cdph.han.util.asynch.dto.AsynchResponseTO;

/**
 * This service keep track of all the managed beans that needs to be refreshed after a service call
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public interface AsynchRefreshService {

	/**
	 * Adds a bean that will be notified when the response to the query request is received, after that the bean is
	 * removed automatically
	 * @param updatableBean bean to be updated when receiving the given correlation ID
	 * @param correlationID ID to use for distinguishing the response message
	 */
	void addOneTimeUpdatableBean (final UpdatableBean updatableBean, final AsynchRequestTO request);

	/**
	 * Adds a bean as subscriber to a group, the refresh service will keep refreshing the bean until it is unregistrered
	 * @param updatableBean bean to be updated when a message belonging to the provided groupId is received
	 * @param groupId ID of the group to filter messages
	 */
	void addSubscriberUpdatableBean (final UpdatableBean updatableBean, final String groupId);

	/**
	 * Removes a bean from the subscriber group
	 * @param updatableBean bean to be removed
	 * @param groupId ID of the group to remove the bean
	 */
	void removeSubscriberUpdatableBean (final UpdatableBean updatableBean, final String groupId);

	/**
	 * Method to tell the service to propagate the update to the registered beans (if any)
	 * @param correlationID the message correlation ID
	 * @param groupId the group id
	 * @param updatedInfo the updated data
	 */
	void propagateUpdate (final String correlationID, final String groupId, final AsynchResponseTO updatedInfo);

	/**
	 * Removes the given updatable bean from all the subscriber groups
	 * @param toRemove bean to be removed
	 */
	void removeBean (final UpdatableBean toRemove);

}
