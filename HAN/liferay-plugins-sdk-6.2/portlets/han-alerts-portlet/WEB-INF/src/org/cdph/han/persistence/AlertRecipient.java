package org.cdph.han.persistence;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Entity representing a recipient for an alert in the HAN system.
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Entity
@Table (name = "alert_recipients", schema = "lportal")
public class AlertRecipient implements Serializable, AlertRecipientModel {
	/** Class serial version UID */
	private static final long serialVersionUID = 3937472988900147663L;
	/** Id of the alert recipient */
	@Id
	@Column (name = "id")
	@GeneratedValue (strategy = GenerationType.TABLE)
	private long id;
	/** Alert to which this recipient was assigned */
	@ManyToOne
	@JoinColumn (name = "alert_id", nullable = false)
	private Alert alert;
	/** If the recipient is a HAN user this will hold the value */
	@ManyToOne
	@JoinColumn (name = "userId", nullable = true)
	private UserData user;
	/** If the recipient is a non HAN user this will hold the value */
	@ManyToOne
	@JoinColumn (name = "non_han_user_id", nullable = true)
	private NonHanUser nonHanUser;
	/** User custom group */
	@ManyToOne
	@JoinColumn (name = "groupId", nullable = true)
	private UserCustomGroup userCustomGroup;
	/** Organization */
	@ManyToOne
	@JoinColumn (name = "organizationId", nullable = true)
    private Organization organization;
	/** Area/Field */
	@ManyToOne
	@JoinColumn (name = "area_field_id", nullable = true)
    private AreaField areaField;
	/** Role */
	@ManyToOne
	@JoinColumn (name = "roleId", nullable = true)
    private Role role;
	/** Non Han Group */
	@ManyToOne
	@JoinColumn (name = "non_han_user_group_id", nullable = true)
	private NonHanUserGroup nonHanUserGroup;
	/** Flag to determine if the notification was already sent to the recipient */
	@Column (name = "notified", nullable = false)
	private boolean notified;
	/** Criteria of the current member */
	@OneToMany (mappedBy = "recipient", cascade = {CascadeType.ALL})
	private Collection<AlertRecipientCriteria> criterias;
	/** Flag for Chicago only entities */
	@Column (name = "chicago_only")
	private boolean chicagoOnly;

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean equal = false;
		if (obj != null && obj instanceof AlertRecipient) {
			final AlertRecipient other = (AlertRecipient) obj;
			equal = other.getId () == id;
		}
		return equal;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		return Long.valueOf (id).hashCode ();
	}

	/**
	 * @return the id
	 */
	public long getId () {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId (long id) {
		this.id = id;
	}

	/**
	 * @return the alert
	 */
	public Alert getAlert () {
		return alert;
	}

	/**
	 * @param alert the alert to set
	 */
	public void setAlert (Alert alert) {
		this.alert = alert;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertRecipientModel#getUser()
	 */
	public UserData getUser () {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser (UserData user) {
		this.user = user;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertRecipientModel#getNonHanUser()
	 */
	public NonHanUser getNonHanUser () {
		return nonHanUser;
	}

	/**
	 * @param nonHanUser the nonHanUser to set
	 */
	public void setNonHanUser (NonHanUser nonHanUser) {
		this.nonHanUser = nonHanUser;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertRecipientModel#getUserCustomGroup()
	 */
	public UserCustomGroup getUserCustomGroup () {
		return userCustomGroup;
	}

	/**
	 * @param userCustomGroup the userCustomGroup to set
	 */
	public void setUserCustomGroup (UserCustomGroup userCustomGroup) {
		this.userCustomGroup = userCustomGroup;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertRecipientModel#getOrganization()
	 */
	public Organization getOrganization () {
		return organization;
	}

	/**
	 * @param organization the organization to set
	 */
	public void setOrganization (Organization organization) {
		this.organization = organization;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertRecipientModel#getAreaField()
	 */
	public AreaField getAreaField () {
		return areaField;
	}

	/**
	 * @param areaField the areaField to set
	 */
	public void setAreaField (AreaField areaField) {
		this.areaField = areaField;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertRecipientModel#getRole()
	 */
	public Role getRole () {
		return role;
	}

	/**
	 * @param role the role to set
	 */
	public void setRole (Role role) {
		this.role = role;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertRecipientModel#getNonHanUserGroup()
	 */
	public NonHanUserGroup getNonHanUserGroup () {
		return nonHanUserGroup;
	}

	/**
	 * @param nonHanUserGroup the nonHanUserGroup to set
	 */
	public void setNonHanUserGroup (NonHanUserGroup nonHanUserGroup) {
		this.nonHanUserGroup = nonHanUserGroup;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertRecipientModel#isNotified()
	 */
	public boolean isNotified () {
		return notified;
	}

	/**
	 * @param notified the notified to set
	 */
	public void setNotified (boolean notified) {
		this.notified = notified;
	}

	/**
	 * @return the criterias
	 */
	public Collection<AlertRecipientCriteria> getCriterias () {
		return criterias;
	}

	/**
	 * @param criterias the criterias to set
	 */
	public void setCriterias (Collection<AlertRecipientCriteria> criterias) {
		this.criterias = criterias;
	}

	/**
	 * @return the chicagoOnly
	 */
	public boolean isChicagoOnly () {
		return chicagoOnly;
	}

	/**
	 * @param chicagoOnly the chicagoOnly to set
	 */
	public void setChicagoOnly (boolean chicagoOnly) {
		this.chicagoOnly = chicagoOnly;
	}

}
