package org.cdph.han.persistence;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity class representing a registry in the deleted users table
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Entity
@Table (name = "Deleted_Users", schema = "lportal")
public class DeletedUser implements Serializable {
	/** Class serial version UID */
	private static final long serialVersionUID = -3220732003950001933L;
	/** The ID of the deleted user */
	@Id
	@Column (name = "userId")
	private long userId;
	/** The email that the user has registered when deleted */
	@Column (name = "emailAddress", length = 75, nullable = false)
	private String email;
	/** Date the user was deleted */
	@Column (name = "dateDeleted", nullable = false)
	private Date dateDeleted;

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean equal = false;
		if (obj != null && obj instanceof DeletedUser) {
			final DeletedUser other = (DeletedUser) obj;
			equal = userId == other.getUserId ();
		}
		return equal;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		return Long.valueOf (userId).hashCode ();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString () {
		// TODO Auto-generated method stub
		return super.toString ();
	}

	/**
	 * @return the userId
	 */
	public long getUserId () {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId (long userId) {
		this.userId = userId;
	}

	/**
	 * @return the email
	 */
	public String getEmail () {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail (String email) {
		this.email = email;
	}

	/**
	 * @return the dateDeleted
	 */
	public Date getDateDeleted () {
		return dateDeleted;
	}

	/**
	 * @param dateDeleted the dateDeleted to set
	 */
	public void setDateDeleted (Date dateDeleted) {
		this.dateDeleted = dateDeleted;
	}

}
