package org.cdph.han.custgroups.dto;

import java.util.List;

/**
 * Transfer object containing the information of a custom user group
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class GroupTO {
	/** The group Id */
	private long id;
	/** The group name */
	private String name;
	/** The recipients in the group */
	private List<RecipientTO> recipients;

	/**
	 * Defualt constructor
	 */
	public GroupTO () {
	}

	/**
	 * This constructor set all fields
	 * @param id of the group
	 * @param name of the group
	 * @param recipients in the group
	 */
	public GroupTO (final long id, final String name, final List<RecipientTO> recipients) {
		this.id = id;
		this.name = name;
		this.recipients = recipients;
	}

	/**
	 * @return the id
	 */
	public long getId () {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId (long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName () {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName (String name) {
		this.name = name;
	}

	/**
	 * @return the recipients
	 */
	public List<RecipientTO> getRecipients () {
		return recipients;
	}

	/**
	 * @param recipients the recipients to set
	 */
	public void setRecipients (List<RecipientTO> recipients) {
		this.recipients = recipients;
	}

}
