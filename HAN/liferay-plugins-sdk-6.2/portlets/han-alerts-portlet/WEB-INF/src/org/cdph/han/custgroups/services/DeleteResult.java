package org.cdph.han.custgroups.services;

/**
 * Enumeration with the possible result of a group delete
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public enum DeleteResult {
	SUCCESS, NOT_OWNER, GROUP_IN_USE

}
