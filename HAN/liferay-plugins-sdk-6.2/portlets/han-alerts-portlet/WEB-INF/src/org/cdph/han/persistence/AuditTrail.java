package org.cdph.han.persistence;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "audit_log", schema = "lportal")
@NamedQueries(value = { @NamedQuery(name = "AuditTrail.GetCount", query = "Select count(a) from AuditTrail a") })
public class AuditTrail implements Serializable {

	private static final long serialVersionUID = -2917531848834102417L;

	/** Primary Key */
	@Id
	@Column(name = "id", nullable = false)
	private Long id;

	/** Action performed */
	@Column(name = "action_performed", nullable = false, length = 100)
	@Enumerated(EnumType.STRING)
	private AuditTrailAction actionPerformed;

	/** Date */
	@Column(name = "date_performed", nullable = false)
	private Date datePerformed;

	/** User information */
	//@ManyToOne
	@Column(name = "userId", nullable = false)
	private Long user;

	/** Alert information */
	@Column(name = "alert_id", nullable = true)
	private Long alertId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AuditTrailAction getActionPerformed() {
		return actionPerformed;
	}

	public void setActionPerformed(AuditTrailAction actionPerformed) {
		this.actionPerformed = actionPerformed;
	}

	public Date getDatePerformed() {
		return datePerformed;
	}

	public void setDatePerformed(Date datePerformed) {
		this.datePerformed = datePerformed;
	}

	public Long getUser() {
		return user;
	}

	public void setUser(Long user) {
		this.user = user;
	}

	public Long getAlertId() {
		return alertId;
	}

	public void setAlertId(Long alertId) {
		this.alertId = alertId;
	}

}
