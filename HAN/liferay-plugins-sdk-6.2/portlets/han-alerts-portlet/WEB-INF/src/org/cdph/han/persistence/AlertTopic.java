package org.cdph.han.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import javax.persistence.Entity;

/**
 * Entity class representing an alert topic
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Entity
@Table (name = "alert_topics", schema = "lportal")
@NamedQueries (
	value = {
		@NamedQuery (name = "AlertTopic.findByLocaleId", query = "select t from AlertTopic t where t.locale.id = :id order by t.title"),
		@NamedQuery (name = "AlertTopic.findByTL", query = "select t from AlertTopic t where t.locale.id = :id and upper (t.title) like :title")
	}
)
public class AlertTopic implements Serializable {
	/** Class serial version UID */
	private static final long serialVersionUID = 2452394178470320190L;
	/** Id of the topic */
	@Id
	@Column (name = "id")
	@GeneratedValue (strategy = GenerationType.TABLE)
	private long id;
	/** Locale of the alert topic */
	@ManyToOne
	@JoinColumn (name = "alert_locale_id", nullable = false)
	private AlertLocale locale;
	/** Title of the topic */
	@Column (name = "title", nullable = false, length = 100)
	private String title;

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean equal = false;
		if (obj != null && obj instanceof AlertTopic) {
			final AlertTopic other = (AlertTopic) obj;
			equal = other.getId () == id;
		}
		return equal;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		// TODO Auto-generated method stub
		return super.hashCode ();
	}

	/**
	 * @return the id
	 */
	public long getId () {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId (long id) {
		this.id = id;
	}

	/**
	 * @return the locale
	 */
	public AlertLocale getLocale () {
		return locale;
	}

	/**
	 * @param locale the locale to set
	 */
	public void setLocale (AlertLocale locale) {
		this.locale = locale;
	}

	/**
	 * @return the title
	 */
	public String getTitle () {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle (String title) {
		this.title = title;
	}

}
