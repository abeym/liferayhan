package org.cdph.han.custgroups.dto;

/**
 * Enumeration of recipient types
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public enum RecipientType {
	ORGANIZATION_TYPE ("orgtype.message"), ORGANIZATION ("org.message"), AREA_FIELD ("areafield.message"),
	ROLE ("role.message"), USER ("user.message"), GROUP ("group.message"), NON_HAN_GROUP ("bulkuploadlist.message"),
	NON_HAN_USER ("bulkuploaduser.message"), CRITERIA ("criteria.message");
	public String TypeMsgId;

	private RecipientType (final String typeMsgId) {
		this.TypeMsgId = typeMsgId;
	}
}
