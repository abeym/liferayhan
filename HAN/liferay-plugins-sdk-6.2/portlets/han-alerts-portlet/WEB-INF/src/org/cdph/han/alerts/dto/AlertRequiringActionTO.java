package org.cdph.han.alerts.dto;

/**
 * Specialization of the AlertPublishedTO that reflects an alert that requires further action from the user
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class AlertRequiringActionTO extends AlertPublishedTO {
	/** Class serial version UID */
	private static final long serialVersionUID = 8103518293203424362L;
	/** Id of the notification report */
	private int notificationReportId;
	/** Id of the recipient report */
	private int recipientReportId;

	/**
	 * @return the recipientReportId
	 */
	public int getRecipientReportId () {
		return recipientReportId;
	}

	/**
	 * @param recipientReportId the recipientReportId to set
	 */
	public void setRecipientReportId (int recipientReportId) {
		this.recipientReportId = recipientReportId;
	}

	/**
	 * @return the notificationReportId
	 */
	public int getNotificationReportId () {
		return notificationReportId;
	}

	/**
	 * @param notificationReportId the notificationReportId to set
	 */
	public void setNotificationReportId (int notificationReportId) {
		this.notificationReportId = notificationReportId;
	}

}
