package org.cdph.han.util.asynch.services;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.util.asynch.dto.AsynchRequestTO;
import org.cdph.han.util.asynch.jms.RequestMessageCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

/**
 * This implementation of the AsynchRequestService uses Spring JMS Template to send the request
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Service ("asynchRequestService")
public class AsynchRequestServiceImpl implements AsynchRequestService {
	/** Class log */
	private static final Log log = LogFactory.getLog (AsynchRequestServiceImpl.class);
	/** JMS Template to use for sending the message */
	@Autowired
	@Qualifier ("hanAsynchReq")
	private JmsTemplate jmsTemplate;

	/* (non-Javadoc)
	 * @see org.cdph.han.util.asynch.services.AsynchRequestService#sendRequest(org.cdph.han.util.asynch.dto.AsynchRequestTO)
	 */
	@Override
	public String sendRequest (final AsynchRequestTO request) {
		final RequestMessageCreator messageCreator = new RequestMessageCreator (request);
		jmsTemplate.send (messageCreator);
		return messageCreator.getCorrelationID ();
	}

}
