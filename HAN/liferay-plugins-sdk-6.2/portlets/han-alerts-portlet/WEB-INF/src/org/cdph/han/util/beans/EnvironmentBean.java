package org.cdph.han.util.beans;

import org.cdph.han.alerts.services.MessagesService;

/**
 * Backed bean used to determine the environment that the application is running in.
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class EnvironmentBean {
	/** Service to retrieve the messages inside the messages.properties file */
	private MessagesService messagesService;

	/**
	 * @param messagesService the messagesService to set
	 */
	public void setMessagesService (MessagesService messagesService) {
		this.messagesService = messagesService;
	}

	/**
	 * @return the imageName
	 */
	public String getImageName () {
		String imageName = "";
		final String environtment = System.getProperty ("env.HOSTNAME");
		imageName = messagesService.getMessage ("environment.image." + environtment);
		return imageName;
	}

}
