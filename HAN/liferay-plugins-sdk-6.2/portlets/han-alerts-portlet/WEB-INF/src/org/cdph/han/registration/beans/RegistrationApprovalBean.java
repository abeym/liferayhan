package org.cdph.han.registration.beans;

import java.util.List;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.beans.BaseBean;
import org.cdph.han.persistence.AreaField;
import org.cdph.han.persistence.Organization;
import org.cdph.han.persistence.RequestForm;
import org.cdph.han.persistence.Role;
import org.cdph.han.registration.dto.RequestFormDTO;
import org.cdph.han.registration.service.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;

import com.liferay.portal.DuplicateUserEmailAddressException;
import com.liferay.portal.DuplicateUserScreenNameException;
import com.liferay.util.bridges.jsf.common.JSFPortletUtil;



public class RegistrationApprovalBean extends BaseBean {

	/** Class logger */
	private static Log log = LogFactory.getLog(RegistrationApprovalBean.class);
	
	@Autowired
	private RegistrationService service;

	private RequestForm registration;
	private RequestFormDTO registrations;
	private Long selectedOrgType;
	private Long selectedOrg;
	private Integer selectedAreaField;
	private Long selectedRole;
	private SelectItem[] organizationTypes = new SelectItem[]{};
	private SelectItem[] organizations = new SelectItem[]{};
	private SelectItem[] areaFields = new SelectItem[]{};
	private SelectItem[] roles = new SelectItem[]{};
	
		
	/**
	 * Action listener for the row that sets the selected registration
	 */
	public String rowSelectionAction(){
		log.debug("In rowSelectionAction()...");
		
		for (RequestForm registration : registrations.getWrappedData()) {
			if(registration.isSelected()){
				this.registration = registration;
				selectedOrgType = this.registration.getOrganizationType () == null ? -1
						: this.registration.getOrganizationType ().getOrganizationId ();
				selectedOrg = this.registration.getOrganization () == null ? null
						: this.registration.getOrganization ().getOrganizationId ();
				selectedAreaField = this.registration.getAreaField () == null ? -1
						: this.registration.getAreaField ().getAreaFieldId ();
				selectedRole = this.registration.getRole () == null ? -1
						: this.registration.getRole ().getRoleId ();
				final List<Organization> orgTypes = service.getOrganizationTypes ();
				organizationTypes = new SelectItem[orgTypes.size ()];
				int i = 0;
				for (Organization orgType : orgTypes) {
					organizationTypes[i++] = new SelectItem (orgType.getOrganizationId (), orgType.getName ());
				}
				if (selectedOrgType > 0) {
					retrieveOrganizationalData ();
				} else {
					organizations = new SelectItem[]{};
					areaFields = new SelectItem[]{};
					roles = new SelectItem[]{};
				}
				return "reviewRequest";
			}
		}
		
		return "";
	}

	private void retrieveOrganizationalData () {
		log.debug ("Retrieving organizations for: " + selectedOrgType);
		final List<Organization> organizations = service.getSuborganizations (selectedOrgType);
		this.organizations = new SelectItem[organizations.size () + 1];
		int i = 0;
		this.organizations[i++] = new SelectItem (-1, "");
		for (Organization org : organizations) {
			this.organizations[i++] = new SelectItem (org.getOrganizationId (), org.getName ());
		}
		final List<AreaField> areaFields = service.getAreaFields (selectedOrgType);
		this.areaFields = new SelectItem[areaFields.size ()];
		i = 0;
		for (AreaField areaField : areaFields) {
			this.areaFields[i++] = new SelectItem (areaField.getAreaFieldId (), areaField.getDescription ());
		}
		final List<Role> roles = service.getOrganizationalRoles (selectedOrgType);
		this.roles = new SelectItem[roles.size ()];
		i = 0;
		for (Role role : roles) {
			this.roles[i++] = new SelectItem (role.getRoleId (), role.getName ());
		}
	}

	/**
	 * Value change listener used to retrieve the new organizations when changing the organization type
	 * @param event fired by the value change
	 */
	public void organizationTypeChanged (final ValueChangeEvent event) {
		final Long newValue = (Long) event.getNewValue ();
		selectedOrgType = newValue;
		if (selectedOrgType > 0) {
			retrieveOrganizationalData ();
		} else {
			organizations = new SelectItem []{};
			areaFields = new SelectItem[]{};
			roles = new SelectItem[]{};
		}
	}
	
	/**
	 * Action listener for back button. It clears
	 * the request form list of previously selected 
	 * requests forms.
	 */
	public String reviewBackButtonAction(){
		log.debug("In reviewBackButtonAction()...");
		
		for (RequestForm registration : registrations.getWrappedData()) {
			if(registration.isSelected()){
				registration.setSelected(false);
			}
		}
		registrations.clearCache();
		
		return "back";
		
	}
	
	/**
	 * Action listener for reject button. 
	 */
	public String rejectButtonAction(){
		log.debug("In rejectButtonAction()...");
		
		service.rejectRequest(registration);
		service.enqueueNotificationEmail(registration.getId());
		
		registrations.clearCache();

		return "reject";
		
	}
	
	
	/**
	 * Action listener for approve button. 
	 */
	public String approveButtonAction(){
		log.debug("In approveButtonAction()...");
		
		String nextAction = "approve";			
		
		long companyId = JSFPortletUtil.getCompanyId(
				FacesContext.getCurrentInstance());
		registration.setCompanyId(companyId);
				
		try{
			if (selectedOrgType == null || selectedOrg == null || selectedOrgType == -1 || selectedOrg == -1) {
				addFacesMessage("registration.approval.review.organization.other.selected");
				nextAction = "failure";
			} else {
				registration.setOrganization (service.getOrganization (selectedOrg));
				registration.setOrganizationType (service.getOrganization (selectedOrgType));
				registration.setAreaField (service.getAreaField (selectedAreaField));
				registration.setRole (service.getRole (selectedRole));
				service.approveRequest(registration);
			}
			
		}catch(DuplicateUserEmailAddressException duex){	
			addFacesMessage("registration.approval.review.duplicate.user.email");
			nextAction = "failure";
		}catch(DuplicateUserScreenNameException dusnex){
			addFacesMessage("registration.approval.review.duplicate.user.screename");
			nextAction = "failure";
		}catch(Exception ex){
			addFacesMessageNoBundle(ex.getMessage());
			nextAction = "failure";
		}
		
		registrations.clearCache();
		
		return nextAction;
		
	}
	
	
	
	public void setService(RegistrationService service) {
		this.service = service;
	}	

	public RequestFormDTO getPendingRegistrations() {
		
		if(registrations == null){
			 registrations= new RequestFormDTO(service);
		}/*else{
			registrations.clearCache();
		}*/
		
		return registrations;
	}

	public void setPendingRegistrations(RequestFormDTO registrations) {		
		this.registrations = registrations;
	}


	public RequestForm getRegistration() {
		return registration;
	}


	public void setRegistration(RequestForm registration) {
		this.registration = registration;
	}

	/**
	 * @return the selectedOrgType
	 */
	public Long getSelectedOrgType () {
		return selectedOrgType;
	}

	/**
	 * @param selectedOrgType the selectedOrgType to set
	 */
	public void setSelectedOrgType (Long selectedOrgType) {
		this.selectedOrgType = selectedOrgType;
	}

	/**
	 * @return the selectedOrg
	 */
	public Long getSelectedOrg () {
		return selectedOrg;
	}

	/**
	 * @param selectedOrg the selectedOrg to set
	 */
	public void setSelectedOrg (Long selectedOrg) {
		this.selectedOrg = selectedOrg;
	}

	/**
	 * @return the organizationTypes
	 */
	public SelectItem[] getOrganizationTypes () {
		return organizationTypes;
	}

	/**
	 * @param organizationTypes the organizationTypes to set
	 */
	public void setOrganizationTypes (SelectItem[] organizationTypes) {
		this.organizationTypes = organizationTypes;
	}

	/**
	 * @return the organizations
	 */
	public SelectItem[] getOrganizations () {
		for (SelectItem item : organizations) {
			if (item == null) {
				log.debug ("Organization null");
			} else {
				log.debug ("ID: " + item.getValue () + " NAME: " + item.getLabel ());
			}
		}
		return organizations;
	}

	/**
	 * @param organizations the organizations to set
	 */
	public void setOrganizations (SelectItem[] organizations) {
		this.organizations = organizations;
	}

	/**
	 * @return the selectedAreaField
	 */
	public Integer getSelectedAreaField () {
		return selectedAreaField;
	}

	/**
	 * @param selectedAreaField the selectedAreaField to set
	 */
	public void setSelectedAreaField (Integer selectedAreaField) {
		this.selectedAreaField = selectedAreaField;
	}

	/**
	 * @return the selectedRole
	 */
	public Long getSelectedRole () {
		return selectedRole;
	}

	/**
	 * @param selectedRole the selectedRole to set
	 */
	public void setSelectedRole (Long selectedRole) {
		this.selectedRole = selectedRole;
	}

	/**
	 * @return the areaFields
	 */
	public SelectItem[] getAreaFields () {
		return areaFields;
	}

	/**
	 * @param areaFields the areaFields to set
	 */
	public void setAreaFields (SelectItem[] areaFields) {
		this.areaFields = areaFields;
	}

	/**
	 * @return the roles
	 */
	public SelectItem[] getRoles () {
		return roles;
	}

	/**
	 * @param roles the roles to set
	 */
	public void setRoles (SelectItem[] roles) {
		this.roles = roles;
	}
}
