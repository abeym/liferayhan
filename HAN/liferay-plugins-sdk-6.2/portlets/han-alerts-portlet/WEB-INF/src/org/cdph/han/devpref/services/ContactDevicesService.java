package org.cdph.han.devpref.services;

import java.util.List;

import javax.faces.model.SelectItem;

import org.cdph.han.custgroups.dto.UserDisplayTO;

/**
 * Contract for services storing, updating and retrieving contact preferences options and devices.
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public interface ContactDevicesService {

	/**
	 * Loads the contact devices associated to the current user
	 * @return List of SelectItem objects with the found user devices
	 */
	List<SelectItem> loadContactDevices ();

	/**
	 * Loads the contact devices preference order
	 * @return Array of String objects with the preference order
	 */
	String[] loadPreferences ();

	/**
	 * Loads the alternate contacts for the user
	 * @return List of UserDisplayTO objects
	 */
	List<UserDisplayTO> loadAlternateContacts ();

	/**
	 * Saves the user contact preferences to the DB. It saves it for the current logged in user.
	 * @param preferences Array of String objects containing the order of contact devices
	 * @param alternateContacts List of UserDisplayTO objects with the users to set as alternate contacts
	 */
	void savePreferences (final String[] preferences, final List<UserDisplayTO> alternateContacts,
			final String telephonyId, final String telephonyPIN, final String telephonyPass);
	
	/**
	 * Saves the user contact preferences to the DB
	 * @param preferences Array of String objects containing the order of contact devices
	 * @param alternateContacts List of UserDisplayTO objects with the users to set as alternate contacts
	 * @param userId Long the user whose preferences will be saved.
	 */
	void savePreferences (final String[] preferences, final List<UserDisplayTO> alternateContacts, Long userId);

}
