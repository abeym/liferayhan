package org.cdph.han.util.asynch.beans;

import org.cdph.han.util.asynch.dto.AsynchResponseTO;

/**
 * This interface provides a contract for baked beans that can be updated after an asynchronous call
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public interface UpdatableBean {

	/**
	 * The implementation of this method must update the implementing class state with the provided object
	 * @param updatedInfo data to use for update the object state
	 */
	void update (final AsynchResponseTO updatedInfo);

}
