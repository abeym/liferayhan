package org.cdph.han.persistence;

/**
 * Interface for retrieving the alert option information regarding of the type (archived or current)
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public interface AlertOptionModel {

	/**
	 * @return the id
	 */
	long getId ();

	/**
	 * @return the locale
	 */
	AlertLocale getLocale ();

	/**
	 * @return the message
	 */
	String getMessage ();

	/**
	 * @return the action
	 */
	int getAction ();

	/**
	 * @return the cascade
	 */
	long getCascade ();

	/**
	 * @return the optionId
	 */
	int getOptionId ();

	/**
	 * @return the callBridge
	 */
	String getCallBridge ();

}