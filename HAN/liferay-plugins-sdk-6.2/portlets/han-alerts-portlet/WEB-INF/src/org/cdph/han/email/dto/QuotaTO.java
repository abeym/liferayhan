package org.cdph.han.email.dto;

/**
 * Transfer object containing the available quota of the user
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class QuotaTO {
	/** If the store supports quotas */
	private boolean supported;
	/** Limit of the quota */
	private long limit;
	/** Current quota usage */
	private long usage;

	/**
	 * Constructs the quota object with the supported flag set to false
	 */
	public QuotaTO () {
		supported = false;
	}

	/**
	 * Constructor to set the limit and usage of the user quota
	 * @param limit of the quota
	 * @param usage of the quota
	 */
	public QuotaTO (final long limit, final long usage) {
		this.limit = limit;
		this.usage = usage;
		supported = true;
	}

	/**
	 * @return the supported
	 */
	public boolean isSupported () {
		return supported;
	}

	/**
	 * @return the limit
	 */
	public long getLimit () {
		return limit;
	}

	/**
	 * @return the usage
	 */
	public long getUsage () {
		return usage;
	}

	/**
	 * @return the used percentage
	 */
	public int getUsed () {
		float percent = (float) usage / (float) limit;
		percent *= 100;
		return Math.round (percent);
	}

	/**
	 * @return the warning flag
	 */
	public boolean isWarning () {
		return getUsed () >= 80;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean equal = false;
		if (obj instanceof QuotaTO) {
			QuotaTO other = (QuotaTO) obj;
			equal = limit == other.getLimit ();
			equal = equal && (usage == other.getUsage ());
		}
		return equal;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		return (int) (usage + limit);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString () {
		return "Quota: " + usage + " / " + limit;
	}

}
