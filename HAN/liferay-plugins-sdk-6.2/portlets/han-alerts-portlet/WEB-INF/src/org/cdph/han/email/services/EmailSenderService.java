package org.cdph.han.email.services;

import java.util.List;

import org.cdph.han.alerts.dto.FileTO;

/**
 * The implementation of this service must allow to send emails as a particular user or using the administrator
 * account
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public interface EmailSenderService {

	/**
	 * The implementation of this method must send an email to the provided recipients (to and cc) using the from
	 * address of the provided user (from) using the provided message and subject
	 * @param from userId of the sender
	 * @param to userId of the recipients
	 * @param cc userId of the recipients
	 * @param message of the email
	 * @param subject of the email
	 * @param attachments to send in the email
	 * @param saveSent to save message in sent folder
	 * @return the size of the sent message
	 */
	int sendEmail (final long from, final long[] to, final long[] cc, final String message, final String subject,
			final List<FileTO> attachments, final boolean saveSent);

	/**
	 * The implementation fo this method must send an email to the provided recipients (to and cc) using the
	 * han administrator email account
	 * @param to userId of the recipients
	 * @param cc userId of the recipients
	 * @param message of the email
	 * @param subject of the email
	 * @param attachments to send in the email
	 */
	void sendAdminEmail (final long[] to, final long[] cc, final String message, final String subject,
			final List<FileTO> attachments);

	/**
	 * The implementation of this method must store the message in the drafts folder
	 * @param from userId of the sender
	 * @param to userIds of the recipients
	 * @param cc userIds of the recipients
	 * @param message of the email
	 * @param subject of the email
	 * @param attachments to send in the email
	 * @param draftNumber of previous draft if any
	 */
	int saveDraft (final long from, final long[] to, final long[] cc, final String message, final String subject,
			final List<FileTO> attachments, final Integer draftNumber);

}
