package org.cdph.han.alerts.exceptions;

/**
 * Exception propagated in case of errors while inserting or updating an alert
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class SaveAlertException extends Exception {
	/** Class serial version UID */
	private static final long serialVersionUID = -617722042493174970L;

	/**
	 * Default constructor
	 */
	public SaveAlertException () {
		super ();
	}

	/**
	 * Constructor to set the message of the exception
	 * @param msg String with the message
	 */
	public SaveAlertException (final String msg) {
		super (msg);
	}

	/**
	 * Constructor to set the cause of the exception
	 * @param cause Throwable cause of the exception
	 */
	public SaveAlertException (final Throwable cause) {
		super (cause);
	}

	/**
	 * Constructor to set the message and cause of the exception
	 * @param msg String with the message
	 * @param cause Throwable cause of the exception
	 */
	public SaveAlertException (final String msg, final Throwable cause) {
		super (msg, cause);
	}

}
