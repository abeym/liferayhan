package org.cdph.han.email.services;

import java.util.List;

import javax.mail.MessagingException;

import org.cdph.han.alerts.dto.FileTO;
import org.cdph.han.email.dto.EmailAttachmentTO;
import org.cdph.han.email.dto.EmailSummaryTO;
import org.cdph.han.email.dto.EmailTO;

/**
 * The implementation of this service must allow the system to manipulate all messages
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public interface EmailMessageService {

	/**
	 * The implementation of this method must return all the messages available in the selected folder
	 * @param userId Id of the user
	 * @param folderName the name of the folder to "open"
	 * @return the list of messages in the given folder
	 * @throws MessagingException in case of errors while retrieving the email listing
	 */
	EmailSummaryTO[] listMessagesInFolder (final long userId, final String folderName)
	throws MessagingException;

	/**
	 * The implementation of this method must move the messages from the given folder to the destination folder,
	 * if any error is found must rise a MessagingException
	 * @param userId id of the user
	 * @param messageNumbers the id of the messages to move
	 * @param fromFolder folder where the message is stored
	 * @param toFolder folder where the message is going to be moved
	 * @throws MessagingException in case an error is found while moving the message
	 */
	EmailOperationOutcome moveMessageToFolder (final long userId, final int[] messageNumbers, final String fromFolder,
			final String toFolder) throws MessagingException;

	/**
	 * The implementation of this method must save a message as a draft
	 * @param from user id that will send the message
	 * @param to recipients user id
	 * @param cc recipients user id
	 * @param message
	 * @param subject of the message
	 * @param attachments of the email
	 * @return if the message was saved or not
	 */
	boolean saveAsDraft (final long from, final long[] to, final long[] cc, final String message, final String subject,
			final List<FileTO> attachments);

	/**
	 * The implementation of this method must return the message with the given message number from the given folder
	 * @param userId of the user owning the message
	 * @param folderName name of the folder where the message is stored
	 * @param messageNumber the number of the message possition in the folder
	 * @return the email message
	 * @throws MessagingException in case of errors while retrieving the message
	 */
	EmailTO retrieveMessage (final long userId, final String folderName, final int messageNumber)
	throws MessagingException;

	/**
	 * The implementation of this method must delete the given message from the folder
	 * @param userId of the user owning the message
	 * @param folderName the name of the folder in which the message is located
	 * @param messageNumbers to delete
	 * @throws MessagingException in case of errors while deleting the messages
	 */
	void deleteMessages (final long userId, final String folderName, final int[] messageNumbers,
			final boolean moveToTrash) throws MessagingException;

	/**
	 * The implementation of this method must create a temporal file to make the attachment editable
	 * @param attachments to make editable
	 * @return list of editable attachment objects <code>EmailTO</code>
	 * @throws Exception in case of errors
	 */
	List<FileTO> makeEditable (final EmailAttachmentTO[] attachments) throws Exception;

	/**
	 * The implementation of this method must mark the given messages as seen
	 * @param userId of the user owning the messages
	 * @param folderName where the messages are located
	 * @param messageNumbers to be marked as seen
	 * @throws MessagingException in case of errors
	 */
	void markAsSeen (final long userId, final String folderName, final int[] messageNumbers)
	throws MessagingException;

}
