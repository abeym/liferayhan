package org.cdph.han.registration.beans;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIInput;
import javax.faces.component.UISelectOne;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.beans.BaseBean;
import org.cdph.han.persistence.AreaField;
import org.cdph.han.persistence.Organization;
import org.cdph.han.persistence.RegistrationRequestStatus;
import org.cdph.han.persistence.RequestForm;
import org.cdph.han.persistence.Role;
import org.cdph.han.registration.exception.EmailAlreadyExistsException;
import org.cdph.han.registration.service.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;



public class RegistrationBean extends BaseBean{

	/** Class logger */
	private static Log log = LogFactory.getLog(RegistrationBean.class);

	private String firstName;
	private String lastName;
	private String middleName;
	private String userName;
	private Long organizationType;
	private Long organization;
	private String organizationName; 
	private String organizationDesc;
	private String title;
	private String department;
	private Long areaField;
	private Long role;
	private String telephone;
	private String telephoneExt;
	private String email;
	private String email2;
	private String purpose;
	private String referralName;
	private String referralTitle;
	private String referralTelephone;
	private String referralTelephoneExt;
	
	private Long requestNumber;
	
	private SelectItem[] organizations;
	private SelectItem[] areaFields;
	private SelectItem[] roles;
	
	private String userNameCheckMessage;
	private String userNameCheckStyle;
	
	private String emailCheckMessage;
	private String emailCheckStyle;
	
	private boolean otherOrganization;
	private String orgTypeDesc;
	private String orgDesc;
	private String areaFieldDesc;
	private String roleDesc;
	
	private boolean popUpRendered = false;

	private UISelectOne organizationTypeComp;
	private UIInput emailComp;
	private UIInput emailComp2;
		
	@Autowired
	private RegistrationService service;	
	
	
	public SelectItem[] getOrganizationTypes(){
		List<Organization> orgTypes = service.getOrganizationTypes();
		ArrayList<SelectItem> items = new ArrayList<SelectItem>();
		
		items.add(new SelectItem(-1,""));
		for (Organization organization : orgTypes) {
			items.add(new SelectItem(organization.getOrganizationId(),
					organization.getName()));
		}
		SelectItem[] itemsUI = new SelectItem[items.size()];
		return items.toArray(itemsUI);
		
	}
	
	public SelectItem[] getOrganizations(){
		return organizations;
		
	}
	
	/**
     * Value change listener for the organization type change event.
     *
     * @param event value change event
     */
    public void orgTypeChanged(ValueChangeEvent event) {
    	log.debug("In orgTypeChanged()...");
     
        Long newOrgTypeID = Long.parseLong(event.getNewValue().toString());
        log.debug("Getting info for Org ID: " + newOrgTypeID);
        
        if(newOrgTypeID == -1){
        	return;
        }
    	
        //Get Organizations
    	List<Organization> orgsTmp = service.getSuborganizations(newOrgTypeID);
		ArrayList<SelectItem> items = new ArrayList<SelectItem>();
		
		for (Organization organization : orgsTmp) {
			items.add(new SelectItem(organization.getOrganizationId(),
					organization.getName()));
		}
		items.add(new SelectItem(-1, "Other"));
		SelectItem[] itemsUI = new SelectItem[items.size()];
		organizations = items.toArray(itemsUI);
		
		//Get AreaFields
		List<AreaField> areaFieldsTmp = service.getAreaFields(newOrgTypeID);
		items = new ArrayList<SelectItem>();
		
		for (AreaField areaField : areaFieldsTmp) {
			items.add(new SelectItem(areaField.getAreaFieldId(),
					areaField.getDescription()));
		}
		itemsUI = new SelectItem[items.size()];
		areaFields = items.toArray(itemsUI);
		
		//Get Roles
		List<Role> rolesTmp = service.getOrganizationalRoles(newOrgTypeID);
		items = new ArrayList<SelectItem>();
		
		for (Role role : rolesTmp) {
			items.add(new SelectItem(role.getRoleId(),
					role.getName()));
		}
		itemsUI = new SelectItem[items.size()];
		roles = items.toArray(itemsUI);
    }
    
    /**
     * Value change listener for the organization change event.
     *
     * @param event value change event
     */
    public void organizationChanged(ValueChangeEvent event) {
    	log.debug("In organizationChanged()...");
        
        Long newOrgID = Long.parseLong(event.getNewValue().toString());
        
        if(newOrgID == -1){
        	//Enable Organization Name and Description
        	otherOrganization = true;
        	popUpRendered = true;
        }else{
        	otherOrganization = false;
        	organizationName = null;
        	organizationDesc = null;
        	popUpRendered = false;
        }
    }
    
    /**
     * Action for the form submit button click action.
     *
     * @param e click action event.
     */
    public String formSubmitButtonAction() {
    	log.debug("In formSubmitButtonAction...");
    	
    	if(organizationType == -1){
    		addFacesMessage(organizationTypeComp.getClientId(FacesContext.getCurrentInstance()),
    				"registration.request.validation.organization.type.missing");
    		return "";
    	}
    	
    	emailCheckMessage = "";
		emailCheckStyle = "";
    	
    	if(!email.equals(email2)){
    		addFacesMessage(emailComp2.getClientId(FacesContext.getCurrentInstance()),
				"registration.request.validation.email.confirmation");
    		return "";
    	}
    	
    	if(emailExist(email)){    		
    		addFacesMessage(emailComp.getClientId(FacesContext.getCurrentInstance()),
    				"registration.request.validation.email.exists");
    		return "";
    	}
    	
    	if(otherOrganization && 
    			(organizationName == null || organizationName.trim().equals("") ||
    			 organizationDesc == null || organizationDesc.trim().equals(""))){
    		popUpRendered = true;
    		return "";
    	}
    	
    	
    	orgTypeDesc = service.getOrganization(organizationType).getName();
    	if(organization == -1){
    		orgDesc = "Other";
    	}else{
    		orgDesc= service.getOrganization(organization).getName();
    	}
    	areaFieldDesc = service.getAreaField(areaField.intValue()).getDescription();
    	roleDesc = service.getRole(role).getName();
    	
    	
    	return "confirm";
    }
    
    /**
     * Action for the form cancel button click action.
     *
     * @param e click action event.
     */
    public String formCancelButtonAction() {

    	cleanUp();
    	
    	return "back";
    }
    
    /**
     * Action for the form submit button click action.
     *
     * @param e click action event.
     */
    public String confirmSubmitButtonAction() {
    	log.debug("In confirmSubmitButtonAction...");
    	String outcome = "success";
    	    	    	
    	try{
    		RequestForm requestForm = new RequestForm();
	    	
	    	requestForm.setFirstName(firstName);
	    	requestForm.setLastName(lastName);
	    	requestForm.setMiddleName(middleName);
	    	//requestForm.setUserName(userName);
	    	
	    	//Organization type
	    	Organization orgType = new Organization();
	    	orgType.setOrganizationId(organizationType);
	    	requestForm.setOrganizationType(orgType);
	    	
	    	//Organization
	    	if(organization != -1){
	    		Organization org = new Organization();
		    	org.setOrganizationId(organization);
		    	requestForm.setOrganization(org);
	    	}
	    		    	
	    	requestForm.setOrganizationName(organizationName);
	    	requestForm.setOrganizationDescription(organizationDesc);
	    	requestForm.setTitle(title);
	    	requestForm.setDepartment(department);	 
	    	
	    	//AreaField
	    	AreaField af = new AreaField();
	    	af.setAreaFieldId(areaField.intValue());
	    	requestForm.setAreaField(af);
	    	
	    	//Role
	    	Role rl = new Role();
	    	rl.setRoleId(role);
	    	requestForm.setRole(rl);
	    	
	    	requestForm.setWorkTelephone(telephone);
	    	requestForm.setWorkTelephoneExt(telephoneExt);
	    	requestForm.setEmail(email);

	    	requestForm.setReferralPurpose(purpose);
	    	requestForm.setReferralName(referralName);
	    	requestForm.setReferralTitle(referralTitle);
	    	requestForm.setReferralTelephone(referralTelephone);
	    	requestForm.setReferralTelephoneExt(referralTelephoneExt);
	    	
	    	requestForm.setStatus(RegistrationRequestStatus.SUBMITTED);
	    	
	    	requestNumber = service.registerRequest(requestForm);
	    	log.debug("Request registration #" + requestNumber);
	    	service.enqueueNotificationEmail(requestNumber);
	    	
	    	cleanUp();
	    	
    	}catch (EmailAlreadyExistsException eaex) {
    		log.error(eaex.getMessage(), eaex);
    		addFacesMessage(emailComp.getClientId(FacesContext.getCurrentInstance()),
				"registration.request.validation.email.exists");
    		outcome = "";
		}catch(Exception ex){
    		log.error(ex.getMessage(), ex);
    		addFacesMessageNoBundle(ex.getMessage());
    		outcome = "";
    	}
    	return outcome;
    }

    public void userNameValueChangeListener(ValueChangeEvent vce){
    	log.debug("In userNameValueChangeListener...");
    	
    	String newValue = vce.getNewValue().toString();
    	log.debug("New Value: " + newValue);
    	if(newValue == null || newValue.trim().equals("")){
    		userNameCheckMessage = "";
    		userNameCheckStyle = "";
    		return;
    	}
    	
    	usernameExist(newValue);   	
    	
    }
    
    public void emailValueChangeListener(ValueChangeEvent vce){
    	log.debug("In userNameValueChangeListener...");    	
    	
    	if(vce.getNewValue() == null ){
    		emailCheckMessage = "";
    		emailCheckStyle = "";
    		return;
    	}
    	
    	String newValue = vce.getNewValue().toString();
    	
    	if (newValue.trim().equals("")){
    		emailCheckMessage = "";
    		emailCheckStyle = "";
    		return;
    	}   	
    	
    	boolean exists = emailExist(newValue);
    	log.debug("Email exists? " + exists);
    	
    	if(exists){
    		addFacesMessage(vce.getComponent().getClientId(FacesContext.getCurrentInstance()),
				"registration.request.validation.email.exists");
    	}else{
    		emailCheckMessage = getBundleMessage("registration.request.validation.email.available");
    		emailCheckStyle = "color:blue; font-weight:bold;";
    	}
    	
    }
    
    public void emailConfirmationValueChangeListener(ValueChangeEvent vce){
    	log.debug("In emailConfirmationValueChangeListener...");
    	        	
    	if( vce.getNewValue() == null ){
    		return;
    	}
    	
    	String newValue = vce.getNewValue().toString();
    	
    	if (newValue.trim().equals("")){
    		return;
    	}  
    	
    	log.debug("Comparing: " + newValue + " == " +  email);
    	
    	if(!newValue.equals(email)){
    		addFacesMessage(vce.getComponent().getClientId(FacesContext.getCurrentInstance()),
    				"registration.request.validation.email.confirmation");
    	}
    	
    }
    
    private boolean usernameExist(String userId){
    	/*long companyId = JSFPortletUtil.getCompanyId(
    			FacesContext.getCurrentInstance());*/
    	
    	ResourceBundle bundle = ResourceBundle.getBundle("messages",
				FacesContext.getCurrentInstance().getViewRoot().getLocale());	
    	
    	boolean flag = true;
    	
    	try {
			flag = service.usernameExist(userId);
		} catch (Exception ex) {
			userNameCheckMessage = ex.getMessage();
			log.error(ex.getMessage(),ex);
		} 
		
		if(flag){
			userNameCheckMessage = bundle
				.getString("registration.request.validation.username.exists");
			userNameCheckStyle = "color:red; font-weight:bold;";
		}else{
			
			userNameCheckMessage = bundle
				.getString("registration.request.validation.username.available");
			userNameCheckStyle = "color:blue; font-weight:bold;";
		}			
		
    	log.debug("Username exists? " + flag);
    	return flag;
    }
    
    private boolean emailExist(String email){
    	
    	boolean flag = true;
    	    	
    	try {
			flag = service.emailExist(email);
		} catch (Exception ex) {
			addFacesMessageNoBundle(ex.getMessage());
		} 
    	return flag;
    }    

	public void setService(RegistrationService service) {
		this.service = service;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getOrganizationType() {
		return organizationType;
	}

	public void setOrganizationType(Long organizationType) {
		this.organizationType = organizationType;
	}

	public Long getOrganization() {
		return organization;
	}

	public void setOrganization(Long organization) {
		this.organization = organization;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public String getOrganizationDesc() {
		return organizationDesc;
	}

	public void setOrganizationDesc(String organizationDesc) {
		this.organizationDesc = organizationDesc;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public Long getAreaField() {
		return areaField;
	}

	public void setAreaField(Long areaField) {
		this.areaField = areaField;
	}

	public Long getRole() {
		return role;
	}

	public void setRole(Long role) {
		this.role = role;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public String getReferralName() {
		return referralName;
	}

	public void setReferralName(String referralName) {
		this.referralName = referralName;
	}

	public String getReferralTitle() {
		return referralTitle;
	}

	public void setReferralTitle(String referralTitle) {
		this.referralTitle = referralTitle;
	}

	public String getReferralTelephone() {
		return referralTelephone;
	}

	public void setReferralTelephone(String referralTelephone) {
		this.referralTelephone = referralTelephone;
	}

	public SelectItem[] getAreaFields() {
		return areaFields;
	}

	public SelectItem[] getRoles() {
		return roles;
	}

	public String getUserNameCheckMessage() {
		return userNameCheckMessage;
	}

	public void setUserCheckNameMessage(String userNameCheckMessage) {
		this.userNameCheckMessage = userNameCheckMessage;
	}

	public String getUserNameCheckStyle() {
		return userNameCheckStyle;
	}

	public void setUserCheckNameStyle(String userNameCheckStyle) {
		this.userNameCheckStyle = userNameCheckStyle;
	}

	public Long getRequestNumber() {
		return requestNumber;
	}

	public void setRequestNumber(Long requestNumber) {
		this.requestNumber = requestNumber;
	}

	public String getTelephoneExt() {
		return telephoneExt;
	}

	public void setTelephoneExt(String telephoneExt) {
		this.telephoneExt = telephoneExt;
	}

	public String getReferralTelephoneExt() {
		return referralTelephoneExt;
	}

	public void setReferralTelephoneExt(String referralTelephoneExt) {
		this.referralTelephoneExt = referralTelephoneExt;
	}

	public boolean isOtherOrganization() {
		return otherOrganization;
	}

	public void setOtherOrganization(boolean otherOrganization) {
		this.otherOrganization = otherOrganization;
	}

	public String getOrgTypeDesc() {
		return orgTypeDesc;
	}

	public void setOrgTypeDesc(String orgTypeDesc) {
		this.orgTypeDesc = orgTypeDesc;
	}

	public String getOrgDesc() {
		return orgDesc;
	}

	public void setOrgDesc(String orgDesc) {
		this.orgDesc = orgDesc;
	}

	public String getAreaFieldDesc() {
		return areaFieldDesc;
	}

	public void setAreaFieldDesc(String areaFieldDesc) {
		this.areaFieldDesc = areaFieldDesc;
	}

	public String getRoleDesc() {
		return roleDesc;
	}

	public void setRoleDesc(String roleDesc) {
		this.roleDesc = roleDesc;
	}
	
	/**
	 * Cleans the beans fields
	 */
	private void cleanUp(){
		firstName = null;
		lastName = null;
		middleName = null;
		userName = null;
		organizationType = null;
		organization = null;
		organizationName = null;
		organizationDesc = null;
		title = null;
		department = null;
		areaField = null;
		role = null;
		telephone = null;
		telephoneExt = null;
		email = null;
		email2 = null;
		purpose = null;
		referralName = null;
		referralTitle = null;
		referralTelephone = null;
		referralTelephoneExt = null;
		
		//requestNumber = null;
		
		organizations = null;
		areaFields = null;
		roles = null;
		
		userNameCheckMessage = null;
		userNameCheckStyle = null;
		
		emailCheckMessage = null;
		emailCheckStyle = null;
		
		otherOrganization = false;
		orgTypeDesc = null;
		orgDesc = null;
		areaFieldDesc = null;
		roleDesc = null;
	}

	public String getEmailCheckMessage() {
		return emailCheckMessage;
	}

	public void setEmailCheckMessage(String emailCheckMessage) {
		this.emailCheckMessage = emailCheckMessage;
	}

	public String getEmailCheckStyle() {
		return emailCheckStyle;
	}

	public void setEmailCheckStyle(String emailCheckStyle) {
		this.emailCheckStyle = emailCheckStyle;
	}

	public String getEmail2() {
		return email2;
	}

	public void setEmail2(String email2) {
		this.email2 = email2;
	}
	
	public void togglePopUp(ActionEvent event) {
		popUpRendered = !popUpRendered;
    }
	
	public boolean getPopUpRendered() {
        return popUpRendered;
    }

	public UISelectOne getOrganizationTypeComp() {
		return organizationTypeComp;
	}

	public void setOrganizationTypeComp(UISelectOne organizationTypeComp) {
		this.organizationTypeComp = organizationTypeComp;
	}

	public UIInput getEmailComp() {
		return emailComp;
	}

	public void setEmailComp(UIInput emailComp) {
		this.emailComp = emailComp;
	}
	
	public UIInput getEmailComp2() {
		return emailComp2;
	}

	public void setEmailComp2(UIInput emailComp) {
		this.emailComp2 = emailComp;
	}
}
