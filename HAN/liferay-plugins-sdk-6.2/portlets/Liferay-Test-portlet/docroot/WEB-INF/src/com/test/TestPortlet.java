package com.test;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.LocalizationUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Company;
import com.liferay.portal.model.Role;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.util.bridges.mvc.MVCPortlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;


/**
 * Portlet implementation class TestPortlet
 */
public class TestPortlet extends MVCPortlet {
 
	
	
	
	
	       @Override
	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		// TODO Auto-generated method stub
	    	
	    	   ThemeDisplay themeDisplay=	(ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
	    	   Company company=themeDisplay.getCompany();
	    	   
	    	   
	   try {
		/*OrganizationLocalServiceUtil.addOrganization(20199, OrganizationConstants.DEFAULT_PARENT_ORGANIZATION_ID, "TEST1",
				   OrganizationConstants.TYPE_REGULAR_ORGANIZATION, 1001, 1, ListTypeConstants.ORGANIZATION_STATUS_DEFAULT, null, false, new ServiceContext()) 	  ;
		*/
		
		   String desc="Detail Desc";
		   
		   Map<Locale, String> titleMap=LocalizationUtil.getLocalizationMap(StringPool.BLANK);
			 
			 Map<Locale, String> descriptionMap=LocalizationUtil.getLocalizationMap(desc); 
			 
			/* com.liferay.portal.model.Role LrRole= 
					 	RoleLocalServiceUtil.addRole(company.getDefaultUser().getUserId(), null, 0L,"RoleName1", titleMap, descriptionMap, 3, null,  null);*/
			 
			 Role role =RoleLocalServiceUtil.addRole(company.getDefaultUser().getUserId(), Role.class.getName(), 0, "RoleName4", titleMap, descriptionMap, 3, null, null);

				
		   
		
	} catch (PortalException | SystemException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} 
	    	   
		super.doView(renderRequest, renderResponse);
	}

		public void addEmployee(ActionRequest actionRequest,
	                     ActionResponse actionResponse) throws IOException, PortletException {
	String employeeName = ParamUtil.getString(actionRequest, "employeeName");
	              String employeeAddress = ParamUtil.getString(actionRequest,
	                           "employeeAddress");
	              Map<String, String> employeeMap = new HashMap<String, String>();
	              employeeMap.put("employeeName", employeeName);
	              employeeMap.put("employeeAddress", employeeAddress);
	              actionRequest.setAttribute("employeeMap", employeeMap);
	              actionResponse.setRenderParameter("mvcPath","/html/jsps/displayEmployee.jsp");
	       }


}
