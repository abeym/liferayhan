package org.cdph.han.custgroups.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.cdph.han.custgroups.dto.GroupDisplayTO;
import org.cdph.han.custgroups.dto.KeyDescriptionTO;
import org.cdph.han.custgroups.dto.UserDisplayTO;
import org.cdph.han.persistence.AreaField;
import org.cdph.han.persistence.BatchStatus;
import org.cdph.han.persistence.CustomGroupCriteria;
import org.cdph.han.persistence.CustomGroupMember;
import org.cdph.han.persistence.NonHanUser;
import org.cdph.han.persistence.NonHanUserGroup;
import org.cdph.han.persistence.Organization;
import org.cdph.han.persistence.Role;
import org.cdph.han.persistence.UserCustomGroup;
import org.cdph.han.persistence.UserData;
import org.cdph.han.persistence.UserOrganizationalData;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of the OrganizationalSearchService interface using JPA
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Repository
@Service ("organizationalSearchService")
public class OrganizationalSearchServiceImpl implements OrganizationalSearchService {
	/** Persistence context */
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.cdph.han.custgroups.services.OrganizationalSearchService#getOrganizationTypes()
	 */
	@Override
	//abey ...@Transactional (readOnly = true)
	public KeyDescriptionTO[] getOrganizationTypes () {
		final Query q = entityManager.createNamedQuery ("Organization.FindOrgTypes");
		final List<?> orgTypes = q.getResultList ();
		Organization orgType;
		KeyDescriptionTO keyDescription;
		final KeyDescriptionTO[] keyDescriptions = new KeyDescriptionTO[orgTypes.size ()];
		int i = 0;
		for (Object o : orgTypes) {
			orgType = (Organization) o;
			keyDescription = new KeyDescriptionTO ();
			keyDescription.setDescription (orgType.getName ());
			keyDescription.setKey (orgType.getOrganizationId ());
			keyDescriptions[i++] = keyDescription;
		}
		return keyDescriptions;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.custgroups.services.OrganizationalSearchService#getOrganizations(long, boolean)
	 */
	@Override
	//abey ...@Transactional (readOnly = true)
	public KeyDescriptionTO[] getOrganizations (final long orgTypeId, final boolean onlyChicagoEntities) {
		final List<KeyDescriptionTO> organizations = new ArrayList<KeyDescriptionTO> ();
		final Organization orgType = entityManager.find (Organization.class, orgTypeId);
		KeyDescriptionTO keyDescription;
		if (onlyChicagoEntities) {
			final Query q = entityManager.createNamedQuery ("Address.findByClassIdPK");
			for (Organization organization : orgType.getChildOrganizations ()) {
				q.setParameter ("classNameId", 14017L);
				q.setParameter ("classPK", organization.getOrganizationId ());
				q.setParameter ("city", "CHICAGO");
				if (q.getResultList ().size () > 0) {
					keyDescription = new KeyDescriptionTO ();
					keyDescription.setDescription (organization.getName ());
					keyDescription.setKey (organization.getOrganizationId ());
					organizations.add (keyDescription);
				}
			}
		} else {
			for (Organization organization : orgType.getChildOrganizations ()) {
				keyDescription = new KeyDescriptionTO ();
				keyDescription.setDescription (organization.getName ());
				keyDescription.setKey (organization.getOrganizationId ());
				organizations.add (keyDescription);
			}
		}
		return organizations.toArray (new KeyDescriptionTO[organizations.size ()]);
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.custgroups.services.OrganizationalSearchService#getAreaFields(long)
	 */
	@Override
	//abey ...@Transactional (readOnly = true)
	public KeyDescriptionTO[] getAreaFields (final long orgTypeId) {
		final Organization orgType = entityManager.find (Organization.class, orgTypeId);
		final KeyDescriptionTO[] keyDescriptions = new KeyDescriptionTO[orgType.getAreaFields ().size ()];
		int i = 0;
		for (AreaField areaField : orgType.getAreaFields ()) {
			keyDescriptions[i] = new KeyDescriptionTO ();
			keyDescriptions[i].setDescription (areaField.getDescription ());
			keyDescriptions[i++].setKey (areaField.getAreaFieldId ());
		}
		return keyDescriptions;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.custgroups.services.OrganizationalSearchService#getRoles(long)
	 */
	@Override
	//abey ...@Transactional (readOnly = true)
	public KeyDescriptionTO[] getRoles (final long orgTypeId) {
		final Organization orgType = entityManager.find (Organization.class, orgTypeId);
		final KeyDescriptionTO[] keyDescriptions = new KeyDescriptionTO[orgType.getRoles ().size ()];
		int i = 0;
		for (Role role : orgType.getRoles ()) {
			keyDescriptions[i] = new KeyDescriptionTO ();
			keyDescriptions[i].setDescription (role.getName ());
			keyDescriptions[i++].setKey (role.getRoleId ());
		}
		return keyDescriptions;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.custgroups.services.OrganizationalSearchService#getUsersInOrganization(long)
	 */
	@Override
	//abey ...@Transactional (readOnly = true)
	public List<UserDisplayTO> getUsersInOrganization (long organizationId) {
		final List<UserDisplayTO> users = new ArrayList<UserDisplayTO> ();
		final Organization organization = entityManager.find (Organization.class, organizationId);
		for (UserOrganizationalData orgData : organization.getUserDatas ()) {
			if (orgData.getUserData ().isActive ()) {
				users.add (new UserDisplayTO (orgData.getUserData ().getUserId (),
						orgData.getUserData ().getContact ().getFirstName (),
						orgData.getUserData ().getContact ().getLastName (),
						organization.getName (), orgData.getRole ().getName ()));
			}
		}
		return users;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.custgroups.services.OrganizationalSearchService#filterUsers(long[], long[], long[])
	 */
	@Override
	//abey ...@Transactional (readOnly = true)
	public List<UserDisplayTO> filterUsers (final long[] organizationIds,
			final long[] areaFieldIds, final long[] roleIds) {
		final List<UserDisplayTO> foundUsers = new ArrayList<UserDisplayTO> ();
		final String query = buildSearchQuery (organizationIds, areaFieldIds, roleIds);
		final Query q = entityManager.createQuery (query);
		final List<?> foundUserDatas = q.getResultList ();
		UserOrganizationalData orgData;
		for (Object o : foundUserDatas) {
			orgData = (UserOrganizationalData) o;
			if (orgData.getUserData ().isActive ()) {
				foundUsers.add (new UserDisplayTO (orgData.getUserData ().getUserId (),
						orgData.getUserData ().getContact ().getFirstName (),
						orgData.getUserData ().getContact ().getLastName (),
						orgData.getOrganization ().getName (), orgData.getRole ().getName ()));
			}
		}
		return foundUsers;
	}

	/**
	 * Creates a EQL query to load the users using the provided filter criteria
	 * @param organizationIds array with the selected organization ids
	 * @param areaFieldsIds array with the selected area/field ids
	 * @param roleIds array with the selected role ids
	 * @return String with the proper SQL
	 */
	private String buildSearchQuery (final long[] organizationIds,
			final long[] areaFieldIds, final long[] roleIds) {
		final StringBuilder sqlBuilder = new StringBuilder ("select d from UserOrganizationalData d");
		final StringBuilder where = new StringBuilder (" where ");
		boolean whereClause = false;
		boolean previous = false;
		if (organizationIds != null && organizationIds.length > 0) {
			where.append (" d.organization.organizationId in (");
			for (int i = 0; i < organizationIds.length; i++) {
				if (previous) {
					where.append (", ");
				}
				where.append (organizationIds[i]);
				previous = true;
			}
			where.append (')');
			whereClause = true;
		}
		if (areaFieldIds != null && areaFieldIds.length > 0) {
			if (whereClause) {
				where.append (" and");
			}
			where.append (" d.areaField.areaFieldId in (");
			previous = false;
			for (int i = 0; i < areaFieldIds.length; i++) {
				if (previous) {
					where.append (", ");
				}
				where.append (areaFieldIds[i]);
				previous = true;
			}
			where.append (')');
			whereClause = true;
		}
		if (roleIds != null && roleIds.length > 0) {
			if (whereClause) {
				where.append (" and");
			}
			where.append (" d.role.roleId in (");
			previous = false;
			for (int i = 0; i < roleIds.length; i++) {
				if (previous) {
					where.append (", ");
				}
				where.append (roleIds[i]);
				previous = true;
			}
			where.append (')');
		}
		return sqlBuilder.append (where).toString ();
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.custgroups.services.OrganizationalSearchService#getUsersByOrganizationType(long)
	 */
	@Override
	//abey ...@Transactional (readOnly = true)
	public List<UserDisplayTO> getUsersByOrganizationType (long orgTypeId) {
		final List<UserDisplayTO> foundUsers = new ArrayList<UserDisplayTO> ();
		final Organization orgType = entityManager.find (Organization.class, orgTypeId);
		for (Organization org : orgType.getChildOrganizations ()) {
			for (UserOrganizationalData userOrgData : org.getUserDatas ()) {
				if (userOrgData.getUserData ().isActive ()) {
					foundUsers.add (new UserDisplayTO (userOrgData.getUserData ().getUserId (),
							userOrgData.getUserData ().getContact ().getFirstName (),
							userOrgData.getUserData ().getContact ().getLastName (),
							org.getName (), userOrgData.getRole ().getName ()));
				}
			}
		}
		return foundUsers;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.custgroups.services.GroupSearchService#searchUsersInBulkUpload(long[])
	 */
	@Override
	//abey ...@Transactional (readOnly = true)
	public UserDisplayTO[] searchUsersInBulkUpload (final List<Long> listIds) {
		StringBuilder ids = new StringBuilder ();
		for (long id : listIds) {
			ids.append (id).append (',');
		}
		final Query q = entityManager.createQuery ("select c from NonHanUser c where c.nonHanGroup.id in ("
				+ ids.toString ().substring (0, ids.length () - 1) + ") order by c.lastName");
		final List<?> contacts = q.getResultList ();
		final List<UserDisplayTO> foundContacts = new ArrayList<UserDisplayTO> ();
		NonHanUser nonHanUser;
		for (Object o : contacts) {
			nonHanUser = (NonHanUser) o;
			if (nonHanUser.getNonHanGroup ().getStatus ().equals (BatchStatus.FINISHED)) {
				foundContacts.add (new UserDisplayTO (nonHanUser.getId (),
						nonHanUser.getFirstName (), nonHanUser.getLastName (), "", "", true));
			}
		}
		return foundContacts.toArray (new UserDisplayTO[foundContacts.size ()]);
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.custgroups.services.GroupSearchService#searchUsersInGroup(java.util.List)
	 */
	@Override
	//abey ...@Transactional (readOnly = true)
	public UserDisplayTO[] searchUsersInGroup (final List<Long> groupIds) {
		StringBuilder ids = new StringBuilder ();
		for (long id : groupIds) {
			ids.append (id).append (',');
		}
		Query q = entityManager.createQuery (
				"select g from UserCustomGroup g where g.groupId in (" +
				ids.substring (0, ids.length () - 1) + ")");
		final List<?> foundGroups = q.getResultList ();
		UserCustomGroup group;
		StringBuilder userIds = new StringBuilder ();
		StringBuilder organizationIds = new StringBuilder ();
		StringBuilder areaFieldIds = new StringBuilder ();
		StringBuilder roleIds = new StringBuilder ();
		StringBuilder nonHanUserIds = new StringBuilder ();
		StringBuilder nonHanGroupIds = new StringBuilder ();
		Set<UserDisplayTO> foundUsers = new TreeSet<UserDisplayTO> ();
		for (Object o : foundGroups) {
			group = (UserCustomGroup) o;
			if (!group.isDeleted ()) {
				for (CustomGroupMember member : group.getMembers ()) {
					if (member.getAreaField () != null) {
						areaFieldIds.append (member.getAreaField ().getAreaFieldId ()).append (',');
					} else if (member.getNonHanUser () != null) {
						nonHanUserIds.append (member.getNonHanUser ().getId ()).append (',');
					} else if (member.getNonHanUserGroup () != null) {
						nonHanGroupIds.append (member.getNonHanUserGroup ().getId ()).append (',');
					} else if (member.getOrganization () != null) {
						organizationIds.append (member.getOrganization ().getOrganizationId ()).append (',');
					} else if (member.getRole () != null) {
						roleIds.append (member.getRole ().getRoleId ()).append (',');
					} else if (member.getUserData () != null) {
						userIds.append (member.getUserData ().getUserId ()).append (',');;
					} else if (member.getCriterias () != null && !member.getCriterias ().isEmpty ()) {
						List<Long> orgIds = new ArrayList<Long> ();
						List<Long> aFIds = new ArrayList<Long> ();
						List<Long> rIds = new ArrayList<Long> ();
						long orgTypeId = 0;
						for (CustomGroupCriteria crit : member.getCriterias ()) {
							orgTypeId = crit.getOrganizationType ().getOrganizationId ();
							if (crit.getOrganization () != null) {
								orgIds.add (crit.getOrganization ().getOrganizationId ());
							}
							if (crit.getAreaField () != null) {
								aFIds.add ((long) crit.getAreaField ().getAreaFieldId ());
							}
							if (crit.getRole () != null) {
								rIds.add (crit.getRole ().getRoleId ());
							}
						}
						if (orgIds.isEmpty () && aFIds.isEmpty () && rIds.isEmpty ()) {
							foundUsers.addAll (getUsersByOrganizationType (orgTypeId));
						} else {
							foundUsers.addAll (filterUsers (convertToArray (orgIds),
									convertToArray (aFIds), convertToArray (rIds)));
						}
					}
				}
			}
		}
		if (userIds.length () > 0 || organizationIds.length () > 0 || areaFieldIds.length () > 0 
				|| roleIds.length () > 0) {
			q = entityManager.createQuery (buildUsersInGroupQuery (userIds.toString (), organizationIds.toString (),
					areaFieldIds.toString (), roleIds.toString ()));
			List<?> foundAssociations = q.getResultList ();
			UserOrganizationalData assoc;
			for (Object o : foundAssociations) {
				assoc = (UserOrganizationalData) o;
				if (assoc.getUserData ().isActive ()) {
					foundUsers.add (new UserDisplayTO (assoc.getUserData ().getUserId (),
							assoc.getUserData ().getContact ().getFirstName (),
							assoc.getUserData ().getContact ().getLastName (),
							assoc.getOrganization ().getName (), assoc.getRole ().getName ()));
				}
			}
		}
		if (nonHanGroupIds.length () > 0 || nonHanUserIds.length () > 0) {
			q = entityManager.createQuery (buildNonHanUsersQuery (nonHanGroupIds.toString (),
					nonHanUserIds.toString ()));
			List<?> foundNonHanUsers = q.getResultList ();
			NonHanUser nonHanUser;
			for (Object o : foundNonHanUsers) {
				nonHanUser = (NonHanUser) o;
				if (nonHanUser.getNonHanGroup ().getStatus ().equals (BatchStatus.FINISHED)) {
					foundUsers.add (new UserDisplayTO (nonHanUser.getId (), nonHanUser.getFirstName (),
							nonHanUser.getLastName (), "", "", true));
				}
			}
		}
		return foundUsers.toArray (new UserDisplayTO[foundUsers.size ()]);
	}

	private long[] convertToArray (final List<Long> toConvert) {
		final long[] converted = new long[toConvert.size ()];
		int i = 0;
		for (Long current : toConvert) {
			converted[i++] = current;
		}
		return converted;
	}

	/**
	 * Creates a query to retrieve all the users belonging to a group
	 * @param userIds ids of the users in the group
	 * @param organizationIds ids of the organizations in the group
	 * @param areaFieldIds ids of the area fields
	 * @param roleIds ids of the roles
	 * @return query to retrieve all users with the provided parameters
	 */
	private String buildUsersInGroupQuery (final String userIds, final String organizationIds,
			final String areaFieldIds, final String roleIds) {
		StringBuilder query = new StringBuilder ();
		boolean and = false;
		query.append ("select distinct a from UserOrganizationalData a where ");
		if (userIds.length () > 0) {
			and = true;
			query.append ("a.userData.userId in (").append (userIds.substring (0, userIds.length () - 1)).append (')');
		}
		if (organizationIds.length () > 0) {
			if (and) {
				query.append (" and ");
			}
			and = true;
			query.append ("a.organization.organizationId in (").append (
					organizationIds.substring (0, organizationIds.length () - 1)).append (')');
		}
		if (areaFieldIds.length () > 0) {
			if (and) {
				query.append (" and ");
			}
			and = true;
			query.append ("a.areaField.areaFieldId in (").append (areaFieldIds.substring (0, areaFieldIds.length () - 1)).append (')');
		}
		if (roleIds.length () > 0) {
			if (and) {
				query.append (" and ");
			}
			query.append ("a.role.roleId in (").append (roleIds.substring (0, roleIds.length () - 1)).append (')');
		}
		return query.toString ();
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.custgroups.services.GroupSearchService#searchIndividuals(java.lang.String, java.lang.String, boolean)
	 */
	@Override
	//abey ...@Transactional (readOnly = true)
	public UserDisplayTO[] searchIndividuals (final String firstName, final String lastName,
			final boolean chicagoOnly) {
		final Query q = entityManager.createQuery (buildIndividualSearchQuery (firstName, lastName));
		final List<?> searchResults = q.getResultList ();
		UserData userData;
		List<UserDisplayTO> userList = new ArrayList<UserDisplayTO> ();
		if (chicagoOnly) {
			Query addressQuery = entityManager.createNamedQuery ("Address.findByClassIdPK");
			List<?> addressList; 
			for (Object o : searchResults) {
				userData = (UserData) o;
				if (userData.isActive ()) {
					for (UserOrganizationalData orgData : userData.getUserOrganizationalData ()) {
						addressQuery.setParameter ("classNameId", 14017L);
						addressQuery.setParameter ("classPK", orgData.getOrganization ().getOrganizationId ());
						addressQuery.setParameter ("city", "CHICAGO");
						addressList = addressQuery.getResultList ();
						if (!addressList.isEmpty ()) {
							userList.add (new UserDisplayTO (userData.getUserId (),
									userData.getContact ().getFirstName (), userData.getContact ().getLastName (),
									orgData.getOrganization ().getName (), orgData.getRole ().getName ()));
						}
					}
				}
			}
		} else {
			for (Object o : searchResults) {
				userData = (UserData) o;
				if (userData.isActive ()) {
					for (UserOrganizationalData orgData : userData.getUserOrganizationalData ()) {
						userList.add (new UserDisplayTO (userData.getUserId (), userData.getContact ().getFirstName (),
								userData.getContact ().getLastName (),
								orgData.getOrganization ().getName (), orgData.getRole ().getName ()));
					}
				}
			}
		}
		return userList.toArray (new UserDisplayTO[userList.size ()]);
	}

	/**
	 * Creates the ejbql to search the users that matches the provided filter
	 * @return query
	 */
	private String buildIndividualSearchQuery (final String firstName, final String lastName) {
		StringBuilder query = new StringBuilder ();
		boolean and = false;
		query.append ("select u from UserData u where ");
		if (lastName != null && !lastName.equals ("")) {
			and = true;
			query.append ("upper (u.contact.lastName) like '" + lastName.toUpperCase () + "%'");
		}
		if (firstName != null && !firstName.equals ("")) {
			if (and) {
				query.append (" and ");
			}
			query.append ("upper (u.contact.firstName) like '" + firstName.toUpperCase () + "%'");
		}
		if (lastName != null && !lastName.equals ("")) {
			query.append (" order by u.contact.lastName");
		} else {
			query.append (" order by u.contact.firstName");
		}
		return query.toString ();
	}

	/**
	 * Builds a query to retrieve all non han users matching the provided filter criteria
	 * @param nonHanGroupIds ids of non han users groups
	 * @param nonHanUserIds ids of non han users
	 * @return query
	 */
	private String buildNonHanUsersQuery (final String nonHanGroupIds, final String nonHanUserIds) {
		StringBuilder query = new StringBuilder ();
		boolean and = false;
		query.append ("select distinct u from NonHanUser u where ");
		if (nonHanGroupIds.length () > 0) {
			and = true;
			query.append ("u.nonHanGroup.id in (").append (
					nonHanGroupIds.substring (0, nonHanGroupIds.length () - 1)).append (')');
		}
		if (nonHanUserIds.length () > 0) {
			if (and) {
				query.append (" and ");
			}
			query.append ("u.id in (").append (nonHanUserIds.substring (0,
					nonHanUserIds.length () - 1)).append (')');
		}
		return query.toString ();
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.custgroups.services.OrganizationalSearchService#getBulkUploadNames(java.lang.String)
	 */
	@Override
	//abey ...@Transactional (readOnly = true)
	public GroupDisplayTO[] getBulkUploadNames (final String nameFilter) {
		final Query q = entityManager.createQuery (
				(nameFilter != null && nameFilter.length () > 0) ?
						"select l from NonHanUserGroup l where UPPER (l.name) like '"
						+ nameFilter.toUpperCase () + "%' order by l.name" :
						"select l from NonHanUserGroup l order by l.name"
				);
		final List<?> nonHanUserGroups = q.getResultList ();
		final List<GroupDisplayTO> bulkUploadLists = new ArrayList<GroupDisplayTO> ();
		NonHanUserGroup nonHanUserGroup;
		for (Object o : nonHanUserGroups) {
			nonHanUserGroup = (NonHanUserGroup) o;
			if (nonHanUserGroup.getStatus ().equals (BatchStatus.FINISHED)) {
				bulkUploadLists.add (new GroupDisplayTO (nonHanUserGroup.getId (), nonHanUserGroup.getName (),
						nonHanUserGroup.getUser ().getContact ().getFirstName () + " " +
						nonHanUserGroup.getUser ().getContact ().getLastName (), nonHanUserGroup.getCreatedDate ()));
			}
		}
		return bulkUploadLists.toArray (new GroupDisplayTO[bulkUploadLists.size ()]);
	}

	/**
	 * @param entityManager the entityManager to set
	 */
	//abey...
	//@PersistenceContext
	public void setEntityManager (EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
