package org.cdph.han.persistence;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;

@Entity
@Table(name = "registration_request", schema="lportal")
public class RequestForm implements Serializable {

	private static final long serialVersionUID = 8184568650622080006L;
	
	@TableGenerator(name="registration_request_generator", initialValue=1, allocationSize=1)
	
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator="registration_request_generator")
	@Column(name = "id", nullable = false)
	private Long id;

	@Column(name = "firstName", nullable = false)
	private String firstName;
	@Column(name = "middleName", nullable = false)
	private String middleName;
	@Column(name = "lastName", nullable = true)
	private String lastName;
	@Column(name = "userName", nullable = true)
	private String userName;

	@ManyToOne
	@JoinColumn (name = "organizationType", nullable = false)
	private Organization organizationType;
	@ManyToOne
	@JoinColumn (name = "organization", nullable = true)
	private Organization organization;
	@Column(name = "organizationName", nullable = true)
	private String organizationName;
	@Column(name = "organizationDescription", nullable = true)
	private String organizationDescription;
	@Column(name = "title", nullable = false)
	private String title;
	@Column(name = "department", nullable = false)
	private String department;
	@ManyToOne
	@JoinColumn (name = "areaField", nullable = false)
	private AreaField areaField;
	@ManyToOne
	@JoinColumn (name = "role", nullable = false)
	private Role role;
	@Column(name = "email", nullable = false)
	private String email;
	
	@Column(name = "workTelephone", nullable = false)
	private String workTelephone;
	@Column(name = "workTelephoneExt", nullable = true)
	private String workTelephoneExt;

	@Column(name = "referralPurpose", nullable = false)
	private String referralPurpose;
	@Column(name = "referralName", nullable = false)
	private String referralName;
	@Column(name = "referralTitle", nullable = false)
	private String referralTitle;
	@Column(name = "referralTelephone", nullable = false)
	private String referralTelephone;
	@Column(name = "referralTelephoneExt", nullable = true)
	private String referralTelephoneExt;
		
	@Column(name = "requestSubmissionDate", nullable = false )
	private Date requestSubmissionDate;	
	
	@Column(name = "status", nullable = false )
	private RegistrationRequestStatus status;
	@Column(name = "approvedRejectedDate", nullable = true )
	private Date approvedRejectedDate;
	
	@Transient
	private Long companyId;
	
	/** Flag to indicated the icefaces datatable if this row is selected	 */
	@Transient 
	private boolean selected;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public String getOrganizationDescription() {
		return organizationDescription;
	}

	public void setOrganizationDescription(String organizationDescription) {
		this.organizationDescription = organizationDescription;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getWorkTelephoneExt() {
		return workTelephoneExt;
	}

	public void setWorkTelephoneExt(String workTelephoneExt) {
		this.workTelephoneExt = workTelephoneExt;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Organization getOrganizationType() {
		return organizationType;
	}

	public void setOrganizationType(Organization organizationType) {
		this.organizationType = organizationType;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public AreaField getAreaField() {
		return areaField;
	}

	public void setAreaField(AreaField areaField) {
		this.areaField = areaField;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getReferralPurpose() {
		return referralPurpose;
	}

	public void setReferralPurpose(String referralPurpose) {
		this.referralPurpose = referralPurpose;
	}

	public String getReferralName() {
		return referralName;
	}

	public void setReferralName(String referralName) {
		this.referralName = referralName;
	}

	public String getReferralTitle() {
		return referralTitle;
	}

	public void setReferralTitle(String referralTitle) {
		this.referralTitle = referralTitle;
	}

	public Date getRequestSubmissionDate() {
		return requestSubmissionDate;
	}

	public void setRequestSubmissionDate(Date requestSubmissionDate) {
		this.requestSubmissionDate = requestSubmissionDate;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public RegistrationRequestStatus getStatus() {
		return status;
	}

	public void setStatus(RegistrationRequestStatus status) {
		this.status = status;
	}

	public Date getApprovedRejectedDate() {
		return approvedRejectedDate;
	}

	public void setApprovedRejectedDate(Date approvedRejectedDate) {
		this.approvedRejectedDate = approvedRejectedDate;
	}

	public String getWorkTelephone() {
		return workTelephone;
	}

	public void setWorkTelephone(String workTelephone) {
		this.workTelephone = workTelephone;
	}

	public String getReferralTelephone() {
		return referralTelephone;
	}

	public void setReferralTelephone(String referralTelephone) {
		this.referralTelephone = referralTelephone;
	}

	public String getReferralTelephoneExt() {
		return referralTelephoneExt;
	}

	public void setReferralTelephoneExt(String referralTelephoneExt) {
		this.referralTelephoneExt = referralTelephoneExt;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
}
