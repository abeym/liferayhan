package org.cdph.han.util.services;

import java.util.Date;

/**
 * Service to synchronize users with mir3
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public interface DailyUserSynchService {

	/**
	 * Synchronizes all the users that have been modified after the date provided
	 * @param fromDate date of last synchronization
	 */
	void synchronizeUsers (final Date fromDate);

}
