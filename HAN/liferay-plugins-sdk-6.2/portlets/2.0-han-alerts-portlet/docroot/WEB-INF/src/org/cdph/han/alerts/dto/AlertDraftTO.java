package org.cdph.han.alerts.dto;

import java.util.Date;

/**
 * Specialized class for the result records of a Draft search
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class AlertDraftTO extends AlertSearchTO {
	/** Class serial version UID */
	private static final long serialVersionUID = 197488993997833266L;
	/** Date of creation */
	private Date creationDate;
	/** Date of modification */
	private Date lastModifiedDate;

	/**
	 * @return the creationDate
	 */
	public Date getCreationDate () {
		return creationDate;
	}

	/**
	 * @param creationDate the creationDate to set
	 */
	public void setCreationDate (Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * @return the lastModifiedDate
	 */
	public Date getLastModifiedDate () {
		return lastModifiedDate;
	}

	/**
	 * @param lastModifiedDate the lastModifiedDate to set
	 */
	public void setLastModifiedDate (Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

}
