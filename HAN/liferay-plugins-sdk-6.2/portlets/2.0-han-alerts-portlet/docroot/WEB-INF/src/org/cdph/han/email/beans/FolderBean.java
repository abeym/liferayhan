package org.cdph.han.email.beans;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ActionListener;
import javax.faces.model.SelectItem;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.alerts.services.MessagesService;
import org.cdph.han.email.component.FolderUserObject;
import org.cdph.han.email.dto.FolderDetailTO;
import org.cdph.han.email.dto.QuotaTO;
import org.cdph.han.email.services.EmailFolderService;
import org.cdph.han.email.services.EmailOperationOutcome;
import org.cdph.han.email.services.QuotaService;
import org.cdph.han.email.util.EmailUtil;

/**
 * Backed bean managing all operations applicable to an email folder
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class FolderBean implements ActionListener {
	/** Class log */
	private static final Log log = LogFactory.getLog (FolderBean.class);
	/** Model containing the folders available for the curren user */
	private DefaultTreeModel model;
	/** Service to retrieve, create, delete, rename email folders */
	private EmailFolderService emailFolderService;
	/** Service to retrieve messages */
	private MessagesService messagesService;
	/** The currently selected folder */
	private FolderUserObject currentSelection;
	/** Service to retrieve the quota */
	private QuotaService quotaService;
	/** Sets the visibility of the create folder popup */
	private boolean createVisible;
	/** Sets the visibility of the delete folder popup */
	private boolean deleteVisible;
	/** Sets the visibility of the rename folder popup */
	private boolean renameVisible;
	/** Name of the folder to create */
	private String folderName;
	/** Sets the visibility of the trash folder popup */
	private boolean trashVisible;
	/** Keeps a list of the email folders */
	private FolderDetailTO[] folders;
	/** Current users quota */
	private QuotaTO quota;

	/**
	 * Creates a new tree node and adds it to the provided parent tree node
	 * @param parent tree node
	 * @param title of the new tree node
	 * @param leaf if the new node is a leaf
	 * @param expanded if the new node should be expanded
	 * @return the new tree node
	 */
	private DefaultMutableTreeNode createNode (final DefaultMutableTreeNode parent, final String title,
			final boolean leaf, final boolean expanded, final String fullName) {
		final DefaultMutableTreeNode node = new DefaultMutableTreeNode ();
		FolderUserObject userObject = new FolderUserObject (node);
		userObject.setText (title);
		userObject.setLeaf (leaf);
		userObject.setExpanded (expanded);
		userObject.setFullName (fullName);
		userObject.setName (title);
		node.setUserObject (userObject);
		if (parent != null) {
			parent.add (node);
		}
		return node;
	}

	/**
	 * Initializes the bean with the available folders of the user
	 */
	private void init () {
		folders = emailFolderService.listFolders (EmailUtil.getInstance ().getUserId ());
		try {
			quota = quotaService.getUserQuota (EmailUtil.getInstance ().getUserId ());
		} catch (Exception ex) {
			log.error ("Error", ex);
		}
		DefaultMutableTreeNode root = createNode (null, "Folders", false, true, null);
		root.setAllowsChildren (true);
		if (folders != null && folders.length == 1) {
			String title;
			if (folders[0].isNewMessages ()) {
				title = folders[0].getFolderName () + " (" + folders[0].getNewMessagesCount () + ")";
			} else {
				title = folders[0].getFolderName ();
			}
			root = createNode (null, title, false, true, folders[0].getFolderFullPath ());
			root.setAllowsChildren (true);
			addChildren (root, folders[0].getChildren ());
		} else {
			addChildren (root, folders);
		}
		currentSelection = (FolderUserObject) root.getUserObject ();
		model = new DefaultTreeModel (root);
	}

	/**
	 * Creates another tree level with the given folders
	 * @param root node
	 * @param children of the current node
	 */
	private void addChildren (final DefaultMutableTreeNode root, final FolderDetailTO[] children) {
		DefaultMutableTreeNode node = null;
		String title;
		for (FolderDetailTO folder : children) {
			log.debug ("Name: " + folder.getFolderName () + " Path: " + folder.getFolderFullPath ()
					+ " Separator: '" + folder.getSeparator () + "' New Messages: "
					+ folder.getNewMessagesCount ());
			if (folder.isNewMessages ()) {
				title = folder.getFolderName () + " (" + folder.getNewMessagesCount () + ")";
			} else {
				title = folder.getFolderName ();
			}
			node = createNode (root, title, false, false, folder.getFolderFullPath ());
			node.setAllowsChildren (true);
			addChildren (node, folder.getChildren ());
		}
	}

	/**
	 * Reload all folder information
	 * @return
	 */
	public String reload () {
		final FolderUserObject temp = currentSelection;
		init ();
		currentSelection = updateCurrent (temp);
		return "success";
	}

	/**
	 * Action listener to reload the folders
	 * @param event
	 */
	public void reload (final ActionEvent event) {
		final FolderUserObject temp = currentSelection;
		init ();
		currentSelection = updateCurrent (temp);
	}

	/**
	 * Action listener to toggle the new folder popup
	 * @param event
	 */
	public void toggleNewFolder (final ActionEvent event) {
		if (!createVisible) {
			folderName = null;
		}
		createVisible = !createVisible;
	}

	/**
	 * Action listener to toggle the delete popup
	 * @param event
	 */
	public void toggleDeleteFolder (final ActionEvent event) {
		deleteVisible = !deleteVisible;
	}

	/**
	 * Action listener to toggle the rename folder popup
	 * @param event
	 */
	public void toggleRenameFolder (final ActionEvent event) {
		if (!renameVisible) {
			folderName = null;
		}
		renameVisible = !renameVisible;
	}

	/**
	 * Action listener to toggle the trash folder popup
	 * @param event
	 */
	public void toggleTrash (final ActionEvent event) {
		trashVisible = !trashVisible;
	}

	/**
	 * Creates a new folder with the provided name
	 * @param event
	 */
	public void createFolder (final ActionEvent event) {
		if (validateName ()) {
			EmailOperationOutcome outcome = emailFolderService.createFolder (
					EmailUtil.getInstance ().getUserId (), folderName);
			if (EmailOperationOutcome.FOLDER_CREATED.equals (outcome)) {
				FacesContext.getCurrentInstance ().addMessage (null,
						messagesService.getMessage ("email.inbox.folder.new.created",
								"Folder Created Successfully", FacesMessage.SEVERITY_INFO));
			} else if (EmailOperationOutcome.ERROR_CREATING_FOLDER.equals (outcome)) {
				FacesContext.getCurrentInstance ().addMessage (null,
						messagesService.getMessage ("email.inbox.folder.new.error",
								"Error creating folder, please try again later", FacesMessage.SEVERITY_ERROR));
			} else if (EmailOperationOutcome.FOLDER_ALREADY_EXISTS.equals (outcome)) {
				FacesContext.getCurrentInstance ().addMessage (null,
						messagesService.getMessage ("email.inbox.folder.new.exists",
								"The given folder name already exists", FacesMessage.SEVERITY_ERROR));
			}
		}
		createVisible = false;
		reload ();
	}

	/**
	 * Action listener that deletes the selected folder unless its a system folder
	 * @param event
	 */
	public void deleteFolder (final ActionEvent event) {
		if (currentSelection.getName ().equals ("Drafts") || currentSelection.getName ().equals ("Sent")
				|| currentSelection.getName ().equals ("Trash") || currentSelection.getName ().equals ("INBOX")) {
			FacesContext.getCurrentInstance ().addMessage (null,
					messagesService.getMessage ("email.inbox.folder.delete.system.folder",
							"You cannot delete system folders (Drafts, Sent, Trash, INBOX)",
							FacesMessage.SEVERITY_ERROR));
		} else {
			EmailOperationOutcome outcome = emailFolderService.deleteFolder (EmailUtil.getInstance ().getUserId (),
					currentSelection.getFullName ());
			if (EmailOperationOutcome.FOLDER_DELETED.equals (outcome)) {
				FacesContext.getCurrentInstance ().addMessage (null,
						messagesService.getMessage ("email.inbox.folder.delete.success",
								"Folder deleted successfully", FacesMessage.SEVERITY_INFO));
			} else {
				FacesContext.getCurrentInstance ().addMessage (null,
						messagesService.getMessage ("email.inbox.folder.delete.error",
								"Error deleting folder, please try again later", FacesMessage.SEVERITY_ERROR));
			}
		}
		deleteVisible = false;
		reload ();
	}

	/**
	 * Action listener that renames the folder with the new name
	 * @param event
	 */
	public void renameFolder (final ActionEvent event) {
		if (currentSelection.getName ().startsWith ("Drafts") || currentSelection.getName ().startsWith ("Sent")
				|| currentSelection.getName ().startsWith ("Trash")
				|| currentSelection.getName ().startsWith ("INBOX")) {
			FacesContext.getCurrentInstance ().addMessage (null,
					messagesService.getMessage ("email.inbox.folder.rename.system.folder",
							"You cannot rename a system folder (Drafts, Sent, Trash, INBOX)",
							FacesMessage.SEVERITY_ERROR));
		} else if (validateName ()) {
			EmailOperationOutcome outcome = emailFolderService.renameFolder (EmailUtil.getInstance ().getUserId (),
					currentSelection.getFullName (), folderName);
			if (EmailOperationOutcome.FOLDER_ALREADY_EXISTS.equals (outcome)) {
				FacesContext.getCurrentInstance ().addMessage (null,
						messagesService.getMessage ("email.inbox.folder.rename.already.exists",
								"The new name is already in use by another folder", FacesMessage.SEVERITY_ERROR));
			} else if (EmailOperationOutcome.ERROR_RENAMING_FOLDER.equals (outcome)) {
				FacesContext.getCurrentInstance ().addMessage (null,
						messagesService.getMessage ("email.inbox.folder.rename.error",
								"Error renaming folder, please try again later", FacesMessage.SEVERITY_ERROR));
			} else if (EmailOperationOutcome.FOLDER_RENAMED.equals (outcome)) {
				FacesContext.getCurrentInstance ().addMessage (null,
						messagesService.getMessage ("email.inbox.folder.rename.success",
								"Folder renamed successfully", FacesMessage.SEVERITY_INFO));
			}
		}
		renameVisible = false;
		reload ();
	}

	/**
	 * This action event empties the trash folder of the user
	 * @param event
	 */
	public void emptyTrash (final ActionEvent event) {
		try {
			trashVisible = false;
			emailFolderService.emptyTrash (EmailUtil.getInstance ().getUserId ());
			FacesContext.getCurrentInstance ().addMessage (null,
					messagesService.getMessage ("email.inbox.folder.trash.empty.success",
							"The Trash folder has been emptied successfully", FacesMessage.SEVERITY_INFO));
			reload ();
		} catch (Exception ex) {
			log.error ("Error emptying trash folder.");
			FacesContext.getCurrentInstance ().addMessage (null,
					messagesService.getMessage ("email.inbox.folder.trash.empty.error",
							"The Trash folder cannot be emptied, try again later", FacesMessage.SEVERITY_ERROR));
		}
	}

	/**
	 * Method for validating that the new folder name only contains numbers and letters
	 */
	private boolean validateName () {
		final Pattern p = Pattern.compile ("[^0-9a-zA-Z\\s]");
		final Matcher m = p.matcher (folderName);
		boolean valid = true;
		if (m.find ()) {
			valid = false;
			FacesContext.getCurrentInstance ().addMessage (null,
					messagesService.getMessage ("email.inbox.folder.name.invalid",
							"The folder name must contain only numbers and letters", FacesMessage.SEVERITY_ERROR));
		}
		return valid;
	}

	/* (non-Javadoc)
	 * @see javax.faces.event.ActionListener#processAction(javax.faces.event.ActionEvent)
	 */
	@Override
	public void processAction (final ActionEvent event) throws AbortProcessingException {
		final FolderUserObject temp = currentSelection;
		init ();
		currentSelection = updateCurrent (temp);
	}

	/**
	 * Search for the updated version of the currently selected folder
	 * @param current folder selected
	 * @return updated folder
	 */
	@SuppressWarnings ("unchecked")
	private FolderUserObject updateCurrent (final FolderUserObject current) {
		FolderUserObject updated = null;
		DefaultMutableTreeNode root = (DefaultMutableTreeNode) model.getRoot ();
		if (((FolderUserObject) root.getUserObject ()).getFullName ().equals (current.getFullName ())) {
			updated = (FolderUserObject) root.getUserObject ();
		} else {
			updated = findByFullPath (root.children (), current.getFullName ());
		}
		return updated;
	}

	/**
	 * Search for the folder with the same full name
	 * @param children to search
	 * @param fullName to match
	 * @return the found object or null if not found
	 */
	@SuppressWarnings ("unchecked")
	private FolderUserObject findByFullPath (final Enumeration<DefaultMutableTreeNode> children,
			final String fullName) {
		FolderUserObject match = null;
		FolderUserObject toCompare = null;
		DefaultMutableTreeNode current = null;
		while (children != null && children.hasMoreElements ()) {
			current = children.nextElement ();
			toCompare = (FolderUserObject) current.getUserObject ();
			if (toCompare.getFullName ().equals (fullName)) {
				match = toCompare;
				break;
			} else {
				match = findByFullPath (current.children (), fullName);
				if (match != null) {
					break;
				}
			}
		}
		return match;
	}

	/**
	 * @param emailFolderService the emailFolderService to set
	 */
	public void setEmailFolderService (EmailFolderService emailFolderService) {
		this.emailFolderService = emailFolderService;
		if (quotaService != null) {
			init ();
		}
	}

	/**
	 * @return the model
	 */
	public DefaultTreeModel getModel () {
		return model;
	}

	/**
	 * @param model the model to set
	 */
	public void setModel (DefaultTreeModel model) {
		this.model = model;
	}

	/**
	 * @return the currentSelection
	 */
	public FolderUserObject getCurrentSelection () {
		return currentSelection;
	}

	/**
	 * @param currentSelection the currentSelection to set
	 */
	public void setCurrentSelection (FolderUserObject currentSelection) {
		this.currentSelection = currentSelection;
	}

	/**
	 * @return the createVisible
	 */
	public boolean isCreateVisible () {
		return createVisible;
	}

	/**
	 * @param createVisible the createVisible to set
	 */
	public void setCreateVisible (boolean createVisible) {
		this.createVisible = createVisible;
	}

	/**
	 * @return the deleteVisible
	 */
	public boolean isDeleteVisible () {
		return deleteVisible;
	}

	/**
	 * @param deleteVisible the deleteVisible to set
	 */
	public void setDeleteVisible (boolean deleteVisible) {
		this.deleteVisible = deleteVisible;
	}

	/**
	 * @return the renameVisible
	 */
	public boolean isRenameVisible () {
		return renameVisible;
	}

	/**
	 * @param renameVisible the renameVisible to set
	 */
	public void setRenameVisible (boolean renameVisible) {
		this.renameVisible = renameVisible;
	}

	/**
	 * @return the folderName
	 */
	public String getFolderName () {
		return folderName;
	}

	/**
	 * @param folderName the folderName to set
	 */
	public void setFolderName (String folderName) {
		this.folderName = folderName;
	}

	/**
	 * @param messagesService the messagesService to set
	 */
	public void setMessagesService (MessagesService messagesService) {
		this.messagesService = messagesService;
	}

	/**
	 * @return the trashVisible
	 */
	public boolean isTrashVisible () {
		return trashVisible;
	}

	/**
	 * @param trashVisible the trashVisible to set
	 */
	public void setTrashVisible (boolean trashVisible) {
		this.trashVisible = trashVisible;
	}

	/**
	 * @return the plainFolders
	 */
	public SelectItem[] getPlainFolders () {
		final List<SelectItem> folders = new ArrayList<SelectItem> ();
		for (FolderDetailTO folder : this.folders) {
			folders.add (new SelectItem (folder.getFolderFullPath (), folder.getFolderName ()));
			folders.addAll (getChildren (folder.getChildren ()));
		}
		return folders.toArray (new SelectItem[folders.size ()]);
	}

	/**
	 * Return a list of select items for the given children
	 * @param children to create select items
	 * @return the list of select items
	 */
	private List<SelectItem> getChildren (final FolderDetailTO[] children) {
		final List<SelectItem> childrenList = new ArrayList<SelectItem> ();
		for (FolderDetailTO folder : children) {
			if (!folder.getFolderName ().startsWith ("Drafts") && !folder.getFolderName ().startsWith ("Trash")) {
				childrenList.add (new SelectItem (folder.getFolderFullPath (), folder.getFolderName ()));
				childrenList.addAll (getChildren (folder.getChildren ()));
			}
		}
		return childrenList;
	}

	/**
	 * @param quotaService the quotaService to set
	 */
	public void setQuotaService (final QuotaService quotaService) {
		this.quotaService = quotaService;
		if (emailFolderService != null) {
			init ();
		}
	}

	/**
	 * @return the quota
	 */
	public QuotaTO getQuota () {
		return quota;
	}

}
