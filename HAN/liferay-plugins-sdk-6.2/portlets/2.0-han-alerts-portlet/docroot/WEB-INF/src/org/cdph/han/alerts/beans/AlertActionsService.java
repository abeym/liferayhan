package org.cdph.han.alerts.beans;

import java.util.List;

import org.cdph.han.alerts.dto.AlertSearchTO;

public interface AlertActionsService {

	List<AlertSearchTO> alertsPossiblyRequiringAction();

}
