package org.cdph.han.persistence;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Class representing the users contact information
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Entity (name = "Contact")
@Table (name = "Contact_", schema = "lportal")
public class Contact implements Serializable {
	/** Class serial version UID */
	private static final long serialVersionUID = -7668815334410481114L;
	/** Contact id */
	@Id
	@Column (name = "contactId")
	private Long contactId;
	/** Users first name */
	@Column (name = "firstName")
	private String firstName;
	/** Users last name */
	@Column (name = "lastName")
	private String lastName;
	/** Users middle name */
	@Column (name = "middleName")
	private String middleName;
	/** The user emails */
	@OneToMany (mappedBy = "contact")
	private Collection<EmailAddress> emailAddresses;
	/** The user phone devices */
	@OneToMany (mappedBy = "contact")
	private Collection<PhoneDevice> phoneDevices;
	/** The users job title */
	@Column (name = "jobTitle")
	private String jobTitle;

	/**
	 * @return the firstName
	 */
	public String getFirstName () {
		return firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName () {
		return lastName;
	}

	/**
	 * @return the middleName
	 */
	public String getMiddleName () {
		return middleName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName (String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName (String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @param middleName the middleName to set
	 */
	public void setMiddleName (String middleName) {
		this.middleName = middleName;
	}

	/**
	 * @return the contactId
	 */
	public Long getContactId () {
		return contactId;
	}

	/**
	 * @param contactId the contactId to set
	 */
	public void setContactId (Long contactId) {
		this.contactId = contactId;
	}

	/**
	 * @return the emailAddresses
	 */
	public Collection<EmailAddress> getEmailAddresses () {
		return emailAddresses;
	}

	/**
	 * @param emailAddresses the emailAddresses to set
	 */
	public void setEmailAddresses (Collection<EmailAddress> emailAddresses) {
		this.emailAddresses = emailAddresses;
	}

	/**
	 * @return the phoneDevices
	 */
	public Collection<PhoneDevice> getPhoneDevices () {
		return phoneDevices;
	}

	/**
	 * @param phoneDevices the phoneDevices to set
	 */
	public void setPhoneDevices (Collection<PhoneDevice> phoneDevices) {
		this.phoneDevices = phoneDevices;
	}

	/**
	 * @return the jobTitle
	 */
	public String getJobTitle () {
		return jobTitle;
	}

	/**
	 * @param jobTitle the jobTitle to set
	 */
	public void setJobTitle (String jobTitle) {
		this.jobTitle = jobTitle;
	}

}
