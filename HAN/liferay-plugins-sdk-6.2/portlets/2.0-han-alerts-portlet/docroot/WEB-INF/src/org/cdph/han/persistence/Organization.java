package org.cdph.han.persistence;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

/**
 * Entity class representing a organization or organization type
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Entity (name = "Organization")
@Table (name = "Organization_", schema = "lportal")
@NamedQueries (value = {
	@NamedQuery (name = "Organization.FindOrgTypes", query = "select o from Organization o where o.parentOrganizationId = 0 order by o.name")
})
public class Organization implements Serializable {
	/** Class serial version UID */
	private static final long serialVersionUID = 783012138554246502L;
	/** Primary key for organization */
	@Id
	@Column (name = "organizationId", nullable = false)
	private long organizationId;
	/** Name of the organization */
	@Column (name = "name")
	private String name;
	/** Parent organization */
	@Column (name = "parentOrganizationId", nullable = false)
	private Long parentOrganizationId;
	/** Child organizations */
	@OneToMany
	@JoinColumn (name = "parentOrganizationId")
	@OrderBy (value = "name")
	private Collection<Organization> childOrganizations;
	/** Users registered in the organization */
	@ManyToMany
	@JoinTable (name = "Users_Orgs", schema = "lportal",
			joinColumns = @JoinColumn (name = "organizationId", referencedColumnName = "organizationId"),
			inverseJoinColumns = @JoinColumn (name = "userId", referencedColumnName = "userId"))
	private Collection<UserData> users;
	/** Organizational roles */
	@ManyToMany
	@JoinTable (name = "TypeOrganizationRoles", schema = "lportal",
			joinColumns = @JoinColumn (name = "organizationTypeId", referencedColumnName = "organizationId"),
			inverseJoinColumns = @JoinColumn (name = "roleId", referencedColumnName = "roleId"))
	@OrderBy (value = "name")
	private Collection<Role> roles;
	/** Organization Areas/Fields */
	@OneToMany (mappedBy = "organization")
	@OrderBy (value = "description")
	private Collection<AreaField> areaFields;
	/** Asigned users to the organization */
	@OneToMany (mappedBy = "organization")
	private Collection<UserOrganizationalData> userDatas;

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean equal = false;
		if (obj != null && obj instanceof Organization) {
			final Organization other = (Organization) obj;
			equal = other.getOrganizationId () == organizationId;
		}
		return equal;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		return Long.valueOf (organizationId).hashCode ();
	}

	/**
	 * @return the organizationId
	 */
	public long getOrganizationId () {
		return organizationId;
	}

	/**
	 * @return the name
	 */
	public String getName () {
		return name;
	}

	/**
	 * @return the parentOrganization
	 */
	public Long getParentOrganizationId () {
		return parentOrganizationId;
	}

	/**
	 * @return the childOrganizations
	 */
	public Collection<Organization> getChildOrganizations () {
		return childOrganizations;
	}

	/**
	 * @param organizationId the organizationId to set
	 */
	public void setOrganizationId (final long organizationId) {
		this.organizationId = organizationId;
	}

	/**
	 * @param name the name to set
	 */
	public void setName (final String name) {
		this.name = name;
	}

	/**
	 * @param parentOrganization the parentOrganization to set
	 */
	public void setParentOrganizationId (final Long parentOrganizationId) {
		this.parentOrganizationId = parentOrganizationId;
	}

	/**
	 * @param childOrganizations the childOrganizations to set
	 */
	public void setChildOrganizations (final Collection<Organization> childOrganizations) {
		this.childOrganizations = childOrganizations;
	}

	/**
	 * @return the users
	 */
	public Collection<UserData> getUsers () {
		return users;
	}

	/**
	 * @param users the users to set
	 */
	public void setUsers (Collection<UserData> users) {
		this.users = users;
	}

	/**
	 * @return the roles
	 */
	public Collection<Role> getRoles () {
		return roles;
	}

	/**
	 * @param roles the roles to set
	 */
	public void setRoles (Collection<Role> roles) {
		this.roles = roles;
	}

	/**
	 * @return the areaFields
	 */
	public Collection<AreaField> getAreaFields () {
		return areaFields;
	}

	/**
	 * @param areaFields the areaFields to set
	 */
	public void setAreaFields (Collection<AreaField> areaFields) {
		this.areaFields = areaFields;
	}

	/**
	 * @return the userDatas
	 */
	public Collection<UserOrganizationalData> getUserDatas () {
		return userDatas;
	}

	/**
	 * @param userDatas the userDatas to set
	 */
	public void setUserDatas (Collection<UserOrganizationalData> userDatas) {
		this.userDatas = userDatas;
	}

}
