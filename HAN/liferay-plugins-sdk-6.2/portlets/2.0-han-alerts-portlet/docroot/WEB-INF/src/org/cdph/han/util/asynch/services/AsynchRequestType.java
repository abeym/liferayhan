package org.cdph.han.util.asynch.services;

/**
 * Enumeration listing the suported request types
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public enum AsynchRequestType {
	ALERTS_REQUIRING_ACTION,
	ALERT_REQUIRES_ACTION

}
