package org.cdph.han.util.services;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.cdph.han.persistence.ListType;
import org.cdph.han.util.dto.ListTypeTO;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of the ListTypeService, this implementation uses JPA to retrieve the data
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
//abey cocmmenting to deploy
//@Repository
//@Service ("listTypeService")
public class ListTypeServiceImpl implements ListTypeService {
	/** Persistence context */
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.cdph.han.util.services.ListTypeService#loadListForType(java.lang.String)
	 */
	@Override
	//abey ...@Transactional (readOnly = true)
	public ListTypeTO[] loadListForType (final String type) {
		final List<ListTypeTO> foundValues = new ArrayList<ListTypeTO> ();
		final Query q = entityManager.createNamedQuery ("ListType.FindByType");
		q.setParameter ("type", type);
		final List<?> results = q.getResultList ();
		ListType listType;
		for (Object obj : results) {
			listType = (ListType) obj;
			foundValues.add (new ListTypeTO (listType.getId (), listType.getName (), listType.getType ()));
		}
		return foundValues.toArray (new ListTypeTO[foundValues.size ()]);
	}

	/**
	 * @param entityManager the entityManager to set
	 */
	//@PersistenceContext
	public void setEntityManager (EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
