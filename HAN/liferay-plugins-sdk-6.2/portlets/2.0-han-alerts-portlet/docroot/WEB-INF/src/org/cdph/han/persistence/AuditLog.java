package org.cdph.han.persistence;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Entity class representing an entry in the audit log table
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Entity
@Table (name = "audit_log", schema = "lportal")
@NamedQueries (
	value = {
		@NamedQuery (name = "AuditLog.findByDate", query = "select a from AuditLog a order by datePerformed")
	}
)
public class AuditLog implements Serializable {
	/** Class serial version UID */
	private static final long serialVersionUID = 5644316115458665994L;
	/** Record ID */
	@Id
	@Column (name = "id")
	private long id;
	/** Action performed */
	@Column (name = "action_performed", length = 100)
	private String actionPerformed;
	/** User performing the action */
	@ManyToOne
	@JoinColumn (name = "userId", nullable = false)
	private UserData user;
	/** Date and time when the action was performed */
	@Column (name = "date_performed", nullable = false)
	private Date datePerformed;
	/** Alert ID if the action was performed on an alert */
	@Column (name = "alert_id", nullable = true)
	private Long alertId;

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((actionPerformed == null) ? 0 : actionPerformed.hashCode ());
		result = prime * result + (int) (alertId ^ (alertId >>> 32));
		result = prime * result
				+ ((datePerformed == null) ? 0 : datePerformed.hashCode ());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((user == null) ? 0 : user.hashCode ());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass () != obj.getClass ())
			return false;
		final AuditLog other = (AuditLog) obj;
		if (actionPerformed == null) {
			if (other.actionPerformed != null)
				return false;
		} else if (!actionPerformed.equals (other.actionPerformed))
			return false;
		if (alertId != other.alertId)
			return false;
		if (datePerformed == null) {
			if (other.datePerformed != null)
				return false;
		} else if (!datePerformed.equals (other.datePerformed))
			return false;
		if (id != other.id)
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals (other.user))
			return false;
		return true;
	}

	/**
	 * @return the id
	 */
	public long getId () {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId (long id) {
		this.id = id;
	}

	/**
	 * @return the actionPerformed
	 */
	public String getActionPerformed () {
		return actionPerformed;
	}

	/**
	 * @param actionPerformed the actionPerformed to set
	 */
	public void setActionPerformed (String actionPerformed) {
		this.actionPerformed = actionPerformed;
	}

	/**
	 * @return the user
	 */
	public UserData getUser () {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser (UserData user) {
		this.user = user;
	}

	/**
	 * @return the datePerformed
	 */
	public Date getDatePerformed () {
		return datePerformed;
	}

	/**
	 * @param datePerformed the datePerformed to set
	 */
	public void setDatePerformed (Date datePerformed) {
		this.datePerformed = datePerformed;
	}

	/**
	 * @return the alertId
	 */
	public Long getAlertId () {
		return alertId;
	}

	/**
	 * @param alertId the alertId to set
	 */
	public void setAlertId (Long alertId) {
		this.alertId = alertId;
	}

}
