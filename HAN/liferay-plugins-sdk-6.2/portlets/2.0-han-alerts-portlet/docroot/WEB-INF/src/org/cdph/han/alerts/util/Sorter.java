package org.cdph.han.alerts.util;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Utility class to sort search results
 * @author Horacio Oswaldo Ferro
 *
 * @param <K> Type parameter to be used as key
 * @param <V> Type parameter to be used as value
 */
public class Sorter<K, V> {

	/**
	 * Groups the alerts into a LinkedHashMap preserving the order after querying the underlying DB,
	 * the provided getter method is used to retrieve the value to use as grouping value.
	 * @param alerts List of AlertSearchTO objects to group
	 * @param sortGetter Method to use as getter to group the alerts
	 * @return Map using the object obtained by the getter method as key and List with AlertSearchTO
	 * objects as values
	 */
	public Map<K, List<V>> groupBy (final List<V> values, final Method sortGetter, final Class<K> keyClass) {
		final Map<K, List<V>> grouped = new LinkedHashMap<K, List<V>> ();
		K key;
		Object o;
		try {
			for (V value : values) {
				o = sortGetter.invoke (value, new Object[]{});
				key = keyClass.cast (o);
				if (!grouped.containsKey (key)) {
					grouped.put (key, new ArrayList<V> ());
				}
				grouped.get (key).add (value);
			}
		} catch (Exception ex) {
			throw new RuntimeException ("Error getting the key value.", ex);
		}
		return grouped;
	}

}
