package org.cdph.han.alerts.services;

import java.util.List;

import org.cdph.han.persistence.AlertTopic;

/**
 * This interface defines the minimum methods required to manipulate the topics in the HAN System
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public interface TopicsService {

	/**
	 * Retrieve all available alert topics for the given locale
	 * @param localeId id of the locale
	 * @return List with AlertTopic objects, empty if there are no available topics
	 */
	List<AlertTopic> listAvailableTopics (final int localeId);

	/**
	 * Insert a new topic in the HAN database
	 * @param localeId Id of the locale
	 * @param topic String with the description of the topic
	 */
	long insertNewTopic (final int localeId, final String topic);

	/**
	 * Checks if the provided topic is available for insertion
	 * @param localeId id of the locale
	 * @param topic String to be checked
	 * @return if the topic is available
	 */
	boolean checkAvailableTopic (final int localeId, final String topic);

}
