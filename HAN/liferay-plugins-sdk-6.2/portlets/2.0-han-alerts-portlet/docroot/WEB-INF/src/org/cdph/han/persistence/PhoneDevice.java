package org.cdph.han.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * This class contains the data of the contact devices of the user.
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Entity (name = "PhoneNumber")
@Table (name = "phone_devices", schema = "lportal")
public class PhoneDevice implements Serializable {
	/** Class serial version UID */
	private static final long serialVersionUID = 7190823851160185391L;
	/** ID of the phone device */
	@Id
	@Column (name = "phoneId", nullable = false, insertable = false, updatable = false)
	private long phoneId;
	/** Number of the phone device */
	@Column (name = "number_")
	private String phoneNumber;
	/** Friendly name of the phone device */
	@Column (name = "phone_name")
	private String friendlyName;
	/** User data */
	@ManyToOne
	@JoinColumn (name = "classPK", nullable = false)
	private Contact contact;
	/** Id of the phone type */
	@Column (name = "typeId")
	private int typeId;
	/** Id of the pager carrier */
	@OneToOne (optional = true)
	@JoinColumn (name = "pager_carrier_id")
	private PagerCarrier pagerCarrier;
	/** Phone extension */
	@Column (name = "extension")
	private String extension;

	/**
	 * @return the phoneId
	 */
	public long getPhoneId () {
		return phoneId;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber () {
		return phoneNumber;
	}

	/**
	 * @return the friendlyName
	 */
	public String getFriendlyName () {
		return friendlyName;
	}

	/**
	 * @param phoneId the phoneId to set
	 */
	public void setPhoneId (final long phoneId) {
		this.phoneId = phoneId;
	}

	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber (final String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @param friendlyName the friendlyName to set
	 */
	public void setFriendlyName (final String friendlyName) {
		this.friendlyName = friendlyName;
	}

	/**
	 * @return the typeId
	 */
	public int getTypeId () {
		return typeId;
	}

	/**
	 * @param typeId the typeId to set
	 */
	public void setTypeId (int typeId) {
		this.typeId = typeId;
	}

	/**
	 * @return the pagerCarrierId
	 */
	public PagerCarrier getPagerCarrier () {
		return pagerCarrier;
	}

	/**
	 * @param pagerCarrierId the pagerCarrierId to set
	 */
	public void setPagerCarrier (PagerCarrier pagerCarrier) {
		this.pagerCarrier = pagerCarrier;
	}

	/**
	 * @return the extension
	 */
	public String getExtension () {
		return extension;
	}

	/**
	 * @param extension the extension to set
	 */
	public void setExtension (String extension) {
		this.extension = extension;
	}

	/**
	 * @return the contact
	 */
	public Contact getContact () {
		return contact;
	}

	/**
	 * @param contact the contact to set
	 */
	public void setContact (Contact contact) {
		this.contact = contact;
	}

}
