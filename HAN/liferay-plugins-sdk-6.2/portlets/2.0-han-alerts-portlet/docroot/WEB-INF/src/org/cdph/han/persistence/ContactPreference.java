package org.cdph.han.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

/**
 * This class represents a Contact Preference.
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Entity (name = "ContactPreference")
@Table (name = "contact_preferences", schema = "lportal")
public class ContactPreference implements Serializable {
	/** Serial version UID */
	private static final long serialVersionUID = 3907253085101203847L;
	/** The registry ID */
	@Id
	@Column (name = "id", nullable = false)
	@GeneratedValue (strategy = GenerationType.TABLE)
	@TableGenerator (schema = "lportal", name = "Dev_Pref", pkColumnName = "name", table = "Counter",
		pkColumnValue = "org.cdph.han.userext.model.Preferences", valueColumnName = "currentId")
	private long id;
	/** The index of the device */
	@Column (name = "device_idx", nullable = false)
	private int index;
	/** Flag to identify the primary email */
	@Column (name = "primary_email", nullable = false)
	private boolean primaryEmail;
	/** Email address if the contact device is email otherwise null */
	@OneToOne
	@JoinColumn (name = "emailAddressId", unique = true, nullable = true, updatable = true)
	private EmailAddress emailAddress;
	/** Phone device if the contact device is a phone otherwise null */
	@OneToOne
	@JoinColumn (name = "phoneId", unique = true, nullable = true, updatable = true)
	private PhoneDevice phoneDevice;
	/** User data */
	@ManyToOne
	@JoinColumn (name = "userId", nullable = false)
	private UserData userData;

	/**
	 * @return the userId
	 *
	public long getUserId () {
		return userId;
	}

	/**
	 * @return the index
	 */
	public int getIndex () {
		return index;
	}

	/**
	 * @param userId the userId to set
	 *
	public void setUserId (final long userId) {
		this.userId = userId;
	}

	/**
	 * @param index the index to set
	 */
	public void setIndex (int index) {
		this.index = index;
	}

	/**
	 * @return the primaryEmail
	 */
	public boolean getPrimaryEmail () {
		return primaryEmail;
	}

	/**
	 * @return the emailAddress
	 */
	public EmailAddress getEmailAddress () {
		return emailAddress;
	}

	/**
	 * @return the phoneDevice
	 */
	public PhoneDevice getPhoneDevice () {
		return phoneDevice;
	}

	/**
	 * @return the userData
	 */
	public UserData getUserData () {
		return userData;
	}

	/**
	 * @param primaryEmail the primaryEmail to set
	 */
	public void setPrimaryEmail (final boolean primaryEmail) {
		this.primaryEmail = primaryEmail;
	}

	/**
	 * @param emailAddress the emailAddress to set
	 */
	public void setEmailAddress (final EmailAddress emailAddress) {
		this.emailAddress = emailAddress;
	}

	/**
	 * @param phoneDevice the phoneDevice to set
	 */
	public void setPhoneDevice (final PhoneDevice phoneDevice) {
		this.phoneDevice = phoneDevice;
	}

	/**
	 * @param userData the userData to set
	 */
	public void setUserData (final UserData userData) {
		this.userData = userData;
	}

	/**
	 * @return the id
	 */
	public long getId () {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId (final long id) {
		this.id = id;
	}

}
