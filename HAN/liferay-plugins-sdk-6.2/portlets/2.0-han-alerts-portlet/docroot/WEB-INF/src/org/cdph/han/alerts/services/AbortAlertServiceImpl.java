package org.cdph.han.alerts.services;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.alerts.exceptions.AlertTerminatedException;
import org.cdph.han.persistence.Alert;
import org.cdph.han.persistence.AlertReport;
import org.cdph.han.persistence.UserData;
import org.cdph.han.util.services.HanPropertiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mir3.ws.AuthorizationType;
import com.mir3.ws.ErrorType;
import com.mir3.ws.Mir3;
import com.mir3.ws.Mir3Service;
import com.mir3.ws.ResponseType;
import com.mir3.ws.TerminateNotificationsType;

/**
 * This implementation of the AbortAlertService uses mir3 webservices to abort a notification
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
//abey commenting...
//@Service ("abortAlertService")
public class AbortAlertServiceImpl implements AbortAlertService {
	/** Class log */
	private static Log log = LogFactory.getLog (AbortAlertServiceImpl.class);
	/** MIR3 API Version key */
	private static final String API_VERSION_KEY = "MIR3_API_VERSION";
	/** Persistence context */
	private EntityManager entityManager;
	/** Mir3 services */
	private Mir3 service;
	/** Service to retrieve properties from the database */
	//abey commenting...
	//@Autowired
	private HanPropertiesService hanPropertiesService;
	/** Service used to retrieve user data */
	//abey commenting...
	//@Autowired
	private UserDataService userDataService;

	/**
	 * Constructor
	 */
	public AbortAlertServiceImpl () {
		service = new Mir3Service ().getMir3 ();
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.AbortAlertService#abortNotification(long)
	 */
	//abey...
	//@Transactional (propagation = Propagation.REQUIRED)
	public boolean abortNotification (final long alertId) throws AlertTerminatedException {
		final Alert alert = entityManager.find (Alert.class, alertId);
		final TerminateNotificationsType request = new TerminateNotificationsType ();
		request.setApiVersion (hanPropertiesService.getValue (API_VERSION_KEY));
		final UserData currentUser = userDataService.getCurrentUserData ();
		final AuthorizationType authorization = new AuthorizationType ();
		authorization.setPassword (currentUser.getTelephonyPassword ());
		authorization.setUsername (currentUser.getEmail ());
		request.setAuthorization (authorization);
		for (AlertReport report : alert.getAlertReports ()) {
			if (!report.isNotificationFinished ()) {
				request.getNotificationReportId ().add (report.getNotificationReportId ());
			}
		}
		if (request.getNotificationReportId ().isEmpty ()) {
			throw new AlertTerminatedException ("The alert is already terminated.");
		} else {
			final ResponseType response = service.terminateNotificationsOp (request);
			if (!response.getError ().isEmpty ()) {
				for (ErrorType error : response.getError ()) {
					log.error ("Error found: " + error.getErrorMessage () + " Error code: " + error.getErrorCode ());
				}
			}
			return response.isSuccess ();
		}
	}

	/**
	 * @param entityManager the entityManager to set
	 */
	//abey commenting...
	//@PersistenceContext
	public void setEntityManager (EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
