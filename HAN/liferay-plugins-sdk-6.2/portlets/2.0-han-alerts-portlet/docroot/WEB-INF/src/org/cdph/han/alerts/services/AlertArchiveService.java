package org.cdph.han.alerts.services;

/**
 * Interface that defines the services provided to archive an alert
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public interface AlertArchiveService {

	/**
	 * The implementation of this method must move all the alert data to the archive tables
	 * @param alertId long with the ID of the alert to archive
	 * @return the new id of the alert in the archives table
	 */
	long archiveAlert (final long alertId);

	/**
	 * The implementation of this method must move all the alert data to the archive tables
	 * @param alertId long with the ID of the alert to archive
	 * @return the new id of the alert in the archives table
	 */
	void archiveAlertFromJob (final long alertId);

	/**
	 * The implementation of this method must move the alert to the archive tables and flag it as deleted,
	 * the delete must be a soft delete
	 * @param alertId id of the alert to delete
	 */
	void deleteAlert (final long alertId, final boolean archived);

}
