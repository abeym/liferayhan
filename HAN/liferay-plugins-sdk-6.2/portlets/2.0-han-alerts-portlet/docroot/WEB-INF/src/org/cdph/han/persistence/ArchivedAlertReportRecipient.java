package org.cdph.han.persistence;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.*;

/**
 * Entity class representing a recipient of an alert report
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Entity
@Table (name = "alert_reports_recipients_archive", schema = "lportal")
public class ArchivedAlertReportRecipient implements Serializable, AlertReportRecipientModel {
	/** Class serial version ID */
	private static final long serialVersionUID = 6421287936104241367L;
	/** Id of the report recipient */
	@Id
	@Column (name = "id")
	@GeneratedValue (strategy = GenerationType.TABLE)
	private long id;
	/** Id of the alert report */
	@ManyToOne
	@JoinColumn (name = "alert_report_id", nullable = false)
	private ArchivedAlertReport report;
	/** ID provided by mir3 */
	@Column (name = "recipient_report_id")
	private int recipientReportId;
	/** Recipient first name */
	@Column (name = "first_name")
	private String firstName;
	/** Recipoent last name */
	@Column (name = "last_name")
	private String lastName;
	/** Devices used to contact the recipient */
	@OneToMany (mappedBy = "recipient", cascade = {CascadeType.ALL})
	private Collection<ArchivedAlertReportDevice> devices;

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean equal = false;
		if (obj != null && obj instanceof ArchivedAlertReportRecipient) {
			final ArchivedAlertReportRecipient other = (ArchivedAlertReportRecipient) obj;
			equal = other.getId () == id;
		}
		return equal;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		return Long.valueOf (id).hashCode ();
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertReportRecipientModel#getId()
	 */
	public long getId () {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId (long id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertReportRecipientModel#getReport()
	 */
	public ArchivedAlertReport getReport () {
		return report;
	}

	/**
	 * @param alertReportId the alertReportId to set
	 */
	public void setReport (ArchivedAlertReport report) {
		this.report = report;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertReportRecipientModel#getRecipientReportId()
	 */
	public int getRecipientReportId () {
		return recipientReportId;
	}

	/**
	 * @param recipientReportId the recipientReportId to set
	 */
	public void setRecipientReportId (int recipientReportId) {
		this.recipientReportId = recipientReportId;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertReportRecipientModel#getFirstName()
	 */
	public String getFirstName () {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName (String firstName) {
		this.firstName = firstName;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertReportRecipientModel#getLastName()
	 */
	public String getLastName () {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName (String lastName) {
		this.lastName = lastName;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertReportRecipientModel#getDevices()
	 */
	public Collection<ArchivedAlertReportDevice> getDevices () {
		return devices;
	}

	/**
	 * @param devices the devices to set
	 */
	public void setDevices (Collection<ArchivedAlertReportDevice> devices) {
		this.devices = devices;
	}

}
