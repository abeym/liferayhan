package org.cdph.han.batch.beans;

import java.io.File;

import com.icesoft.faces.component.inputfile.FileInfo;
import com.icesoft.faces.component.inputfile.InputFile;

public class InputFileData {
	
    private InputFile inputFile;
    
    public InputFileData(InputFile inputFile){
    	this.inputFile = inputFile;
    }

    public FileInfo getFileInfo() {
        return inputFile.getFileInfo();
    }


    public File getFile() {
        return inputFile.getFile();
    }
    
    public int getStatus(){
    	return inputFile.getStatus();
    }

    
    /**
     * Method to return the file size as a formatted string
     * For example, 4000 bytes would be returned as 4kb
     *
     *@return formatted file size
     */
    public String getSizeFormatted() {
        long ourLength = getFile().length();
        
        // Generate formatted label, such as 4kb, instead of just a plain number
        if (ourLength >= BatchUploadBean.MEGABYTE_LENGTH_BYTES) {
            return ourLength / BatchUploadBean.MEGABYTE_LENGTH_BYTES + "mb";
        }
        else if (ourLength >= BatchUploadBean.KILOBYTE_LENGTH_BYTES) {
            return ourLength / BatchUploadBean.KILOBYTE_LENGTH_BYTES + "kb";
        }
        else if (ourLength == 0) {
            return "0";
        }
        else if (ourLength < BatchUploadBean.KILOBYTE_LENGTH_BYTES) {
            return ourLength + "b";
        }
        
        return Long.toString(ourLength);
    }    

}
