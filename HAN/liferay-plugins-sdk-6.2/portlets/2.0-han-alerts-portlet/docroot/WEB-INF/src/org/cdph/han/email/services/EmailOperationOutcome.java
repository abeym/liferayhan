package org.cdph.han.email.services;

/**
 * This enumeration contains all the possible outcomes of an email operation
 * @author Oswaldo
 *
 */
public enum EmailOperationOutcome {
	FOLDER_CREATED, FOLDER_DELETED, FOLDER_RENAMED,
	FOLDER_ALREADY_EXISTS, FOLDER_NOT_EMPTY, FOLDER_DOES_NOT_EXISTS,
	ERROR_CREATING_FOLDER, ERROR_DELETING_FOLDER, ERROR_RENAMING_FOLDER,
	MESSAGE_MOVED

}
