package org.cdph.han.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

/**
 * Entity class representing an alternate contact
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Entity
@Table (name = "alternate_contacts", schema = "lportal")
public class AlternateContact implements Serializable {
	/** Class serial version UID */
	private static final long serialVersionUID = 6988765998408430731L;
	/** ID of the alternate contact record */
	@Id
	@Column (name = "id", nullable = false)
	@GeneratedValue (strategy = GenerationType.TABLE)
	@TableGenerator (schema = "lportal", name = "alternates", pkColumnName = "name", table = "Counter",
			pkColumnValue = "org.cdph.han.userext.model.AlternateContact", valueColumnName = "currentId")
	private long id;
	/** UserData of the user that the alternate contact belongs */
	@ManyToOne
	@JoinColumn (name = "userId", nullable = false)
	private UserData userData;
	/** UserData of the alternate contact */
	@ManyToOne
	@JoinColumn (name = "alternate_userId", nullable = false)
	private UserData alternateContact;
	/** Index of the alternate contact preference */
	@Column (name = "preference", nullable = false)
	private int index;

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean equal = false;
		if (obj != null && obj instanceof AlternateContact) {
			final AlternateContact other = (AlternateContact) obj;
			equal = other.getId () == id;
		}
		return equal;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		return Long.valueOf (id).hashCode ();
	}

	/**
	 * @return the id
	 */
	public long getId () {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId (long id) {
		this.id = id;
	}

	/**
	 * @return the userData
	 */
	public UserData getUserData () {
		return userData;
	}

	/**
	 * @param userData the userData to set
	 */
	public void setUserData (UserData userData) {
		this.userData = userData;
	}

	/**
	 * @return the alternateContact
	 */
	public UserData getAlternateContact () {
		return alternateContact;
	}

	/**
	 * @param alternateContact the alternateContact to set
	 */
	public void setAlternateContact (UserData alternateContact) {
		this.alternateContact = alternateContact;
	}

	/**
	 * @return the index
	 */
	public int getIndex () {
		return index;
	}

	/**
	 * @param index the index to set
	 */
	public void setIndex (int index) {
		this.index = index;
	}

}
