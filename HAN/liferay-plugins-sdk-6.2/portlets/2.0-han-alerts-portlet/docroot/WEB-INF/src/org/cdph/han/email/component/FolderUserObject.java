package org.cdph.han.email.component;

import javax.swing.tree.DefaultMutableTreeNode;

import com.icesoft.faces.component.tree.IceUserObject;

/**
 * Object used to render the folder structure of the email inbox
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class FolderUserObject extends IceUserObject {
	/** Folder full name */
	private String fullName;
	/** Folder name */
	private String name;

	/**
	 * Default constructor
	 * @param wrapper
	 */
	public FolderUserObject (final DefaultMutableTreeNode wrapper) {
		super (wrapper);

		setLeafIcon ("tree_document.gif");
		setBranchContractedIcon ("tree_folder_closed.gif");
		setBranchExpandedIcon ("tree_folder_open.gif");
		setExpanded (true);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((fullName == null) ? 0 : fullName.hashCode ());
		result = prime * result + ((name == null) ? 0 : name.hashCode ());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass () != obj.getClass ())
			return false;
		FolderUserObject other = (FolderUserObject) obj;
		if (fullName == null) {
			if (other.fullName != null)
				return false;
		} else if (!fullName.equals (other.fullName))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals (other.name))
			return false;
		return true;
	}

	/**
	 * @return the fullName
	 */
	public String getFullName () {
		return fullName;
	}

	/**
	 * @param fullName the fullName to set
	 */
	public void setFullName (String fullName) {
		this.fullName = fullName;
	}

	/**
	 * @return the name
	 */
	public String getName () {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName (String name) {
		this.name = name;
	}

}
