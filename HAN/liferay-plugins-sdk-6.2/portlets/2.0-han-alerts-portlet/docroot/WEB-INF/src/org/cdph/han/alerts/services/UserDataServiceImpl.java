package org.cdph.han.alerts.services;

import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;

import org.cdph.han.custgroups.dto.UserDisplayTO;
import org.cdph.han.persistence.UserData;
import org.cdph.han.persistence.UserOrganizationalData;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of the UserDataService contract, this implementation uses JSF
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Repository
@Service ("userService")
public class UserDataServiceImpl implements UserDataService {
	/** JPA entity manager */
	private EntityManager entityManager;

	/**
	 * Setter to inject the persistence context
	 * @param em JPA Entity Manager
	 */
	//abey...
	//@PersistenceContext
	public void setEntityManager (final EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.UserDataService#getCurrentUserData()
	 */
	//abey ...@Transactional (propagation = Propagation.SUPPORTS)
	public UserData getCurrentUserData () {
		final Object requestObj = FacesContext.getCurrentInstance ().getExternalContext ().getRequest ();
		String userId;
		UserData userData = null;
		if (requestObj instanceof HttpServletRequest) {
			userId = ((HttpServletRequest) requestObj).getRemoteUser ();
		} else if (requestObj instanceof PortletRequest) {
			userId = ((PortletRequest) requestObj).getRemoteUser ();
		} else {
			userId = null;
		}
		if (userId != null) {
			long primaryKey = Long.parseLong (userId);
			userData = getUser (primaryKey);
		}
		return userData;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.UserDataService#getAlertInitiators()
	 */
	//abey ...@Transactional (readOnly = true)
	public List<?> getAlertInitiators () {
		Query q = entityManager.createNamedQuery ("UserData.FindAlertInitiator");
		final List<?> foundUsers = q.getResultList ();
		return foundUsers;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.UserDataService#findUsersByFirstAndLastName(java.lang.String, java.lang.String)
	 */
	//abey ...@Transactional (readOnly = true)
	public List<UserDisplayTO> findUsersByFirstAndLastName (final String firstName, final String lastName) {
		final boolean filterByFirstName = firstName != null && firstName.length () > 0;
		final boolean filterByLastName = lastName != null && lastName.length () > 0;
		List<?> foundUsersDB = null;
		final List<UserDisplayTO> foundUsers = new ArrayList<UserDisplayTO> ();
		if (filterByFirstName && filterByLastName) {
			final Query q = entityManager.createNamedQuery ("UserData.FindByFNLN");
			q.setParameter ("firstName", firstName.toUpperCase () + "%");
			q.setParameter ("lastName", lastName.toUpperCase () + "%");
			foundUsersDB = q.getResultList ();
		} else if (filterByFirstName) {
			final Query q = entityManager.createNamedQuery ("UserData.FindByFN");
			q.setParameter ("firstName", firstName.toUpperCase () + "%");
			foundUsersDB = q.getResultList ();
		} else if (filterByLastName) {
			final Query q = entityManager.createNamedQuery ("UserData.FindByLN");
			q.setParameter ("lastName", lastName.toUpperCase () + "%");
			foundUsersDB = q.getResultList ();
		} else {
			foundUsersDB = new ArrayList<Object> ();
		}
		UserData foundUserDB;
		for (Object o : foundUsersDB) {
			foundUserDB = (UserData) o;
			for (UserOrganizationalData orgData : foundUserDB.getUserOrganizationalData ()) {
				if (orgData.getUserData ().isActive ()) {
					foundUsers.add (new UserDisplayTO (foundUserDB.getUserId (),
							foundUserDB.getContact ().getFirstName (),
							foundUserDB.getContact ().getLastName (),
							orgData.getOrganization ().getName (),
							orgData.getRole ().getName ()));
				}
			}
		}
		return foundUsers;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.UserDataService#getUser(long)
	 */
	//abey ...@Transactional (readOnly = true)
	public UserData getUser (final long userId) {
		return entityManager.find (UserData.class, userId);
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.UserDataService#getAllUsers()
	 */
	@Override
	//abey ...@Transactional (readOnly = true)
	@SuppressWarnings ("unchecked")
	public List<UserData> getAllUsers () {
		final Query q = entityManager.createQuery ("select u from UserData u");
		return (List<UserData>) q.getResultList ();
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.UserDataService#toggleWarnedUser(long, boolean)
	 */
	@Override
	//abey ...@Transactional (propagation = Propagation.REQUIRED)
	public void toggleWarnedUser (long userId, boolean warned) {
		final Query q = entityManager.createNativeQuery (
				"UPDATE han_user_data SET quota_warned = ? WHERE userid = ?");
		q.setParameter (1, warned);
		q.setParameter (2, userId);
		q.executeUpdate ();
	}

}
