package org.cdph.han.alerts.dto;

import java.util.Date;

/**
 * Specialized class for the result records of the Current and Archived alert searchs
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class AlertPublishedTO extends AlertSearchTO {
	/** Class serial version UID */
	private static final long serialVersionUID = -366524935701549535L;
	/** The alert publication date */
	private Date publicationDate;
	/** The level of the alert */
	private String levelOfAlert;
	/** Date of the alert creation */
	private Date creationDate;
	/** Last date the alert was modified */
	private Date lastModifiedDate;
	/** If the alert is archived */
	private boolean archived;
	/** If the alert notification is finished */
	private boolean finished;

	/**
	 * @return the publicationDate
	 */
	public Date getPublicationDate () {
		return publicationDate;
	}

	/**
	 * @param publicationDate the publicationDate to set
	 */
	public void setPublicationDate (Date publicationDate) {
		this.publicationDate = publicationDate;
	}

	/**
	 * @return the levelOfAlert
	 */
	public String getLevelOfAlert () {
		return levelOfAlert;
	}

	/**
	 * @param levelOfAlert the levelOfAlert to set
	 */
	public void setLevelOfAlert (String levelOfAlert) {
		this.levelOfAlert = levelOfAlert;
	}

	/**
	 * @return the creationDate
	 */
	public Date getCreationDate () {
		return creationDate;
	}

	/**
	 * @param creationDate the creationDate to set
	 */
	public void setCreationDate (Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * @return the lastModifiedDate
	 */
	public Date getLastModifiedDate () {
		return lastModifiedDate;
	}

	/**
	 * @param lastModifiedDate the lastModifiedDate to set
	 */
	public void setLastModifiedDate (Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	/**
	 * @return the archived
	 */
	public boolean isArchived () {
		return archived;
	}

	/**
	 * @param archived the archived to set
	 */
	public void setArchived (boolean archived) {
		this.archived = archived;
	}

	/**
	 * @return the finished
	 */
	public boolean isFinished () {
		return finished;
	}

	/**
	 * @param finished the finished to set
	 */
	public void setFinished (boolean finished) {
		this.finished = finished;
	}

}
