package org.cdph.han.util.asynch.services;

import org.cdph.han.util.asynch.dto.AsynchRequestTO;

/**
 * Interface that defines the minimum behaviour for the implementation of the service to request asynchronous queries
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public interface AsynchRequestService {

	/**
	 * Send the request to a JMS Queue for processing
	 * @param request with the query parameters
	 * @param the correlation ID associated to the sent message
	 */
	String sendRequest (final AsynchRequestTO request);

}
