package org.cdph.han.email.util;

import javax.faces.context.FacesContext;

/**
 * Utility class for the email client, implements the singleton pattern
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class EmailUtil {
	/** Unique instance */
	private static EmailUtil instance = null;

	/**
	 * Private constructor to prevent other classes from creating instances of this class
	 */
	private EmailUtil () {
	}

	/**
	 * Returns the unique instance of the utility class
	 * @return instance of the class
	 */
	public static EmailUtil getInstance () {
		if (instance == null) {
			instance = new EmailUtil ();
		}
		return instance;
	}

	/**
	 * This method must only be called inside a backed bean
	 * @return the user id
	 */
	public long getUserId () {
		final String userIdString = FacesContext.getCurrentInstance ().getExternalContext (
				).getUserPrincipal ().getName ();
		return Long.parseLong (userIdString);
	}

}
