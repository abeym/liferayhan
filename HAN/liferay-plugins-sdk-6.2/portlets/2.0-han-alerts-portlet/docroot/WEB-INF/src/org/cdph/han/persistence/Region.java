package org.cdph.han.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Class to reference a Region (State) in liferay
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Entity
@Table (name = "Region", schema = "lportal")
public class Region implements Serializable {
	/** Class serial version UID */
	private static final long serialVersionUID = 8979881027223391023L;
	/** The id of the region */
	@Id
	@Column (name = "regionId")
	private long regionId;
	/** Name of the region */
	@Column (name = "name")
	private String name;

	/**
	 * @return the regionId
	 */
	public long getRegionId () {
		return regionId;
	}

	/**
	 * @param regionId the regionId to set
	 */
	public void setRegionId (long regionId) {
		this.regionId = regionId;
	}

	/**
	 * @return the name
	 */
	public String getName () {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName (String name) {
		this.name = name;
	}

}
