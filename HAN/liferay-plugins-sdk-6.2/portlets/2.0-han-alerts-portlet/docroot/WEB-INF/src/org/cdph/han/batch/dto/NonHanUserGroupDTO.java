package org.cdph.han.batch.dto;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.batch.service.BatchService;
import org.cdph.han.dto.LazyDataModel;
import org.cdph.han.persistence.NonHanUserGroup;

public class NonHanUserGroupDTO extends LazyDataModel<NonHanUserGroup> {
	
	/** Class logger */
	private static final Log log = LogFactory
			.getLog(NonHanUserDTO.class);

	private BatchService service;

	private String groupName;
	private String groupDesc;
	
	public NonHanUserGroupDTO(BatchService service) {
		this.service = service;
	}

	@Override
	public int countRows() {

		if((groupName == null || groupName.trim().equals("")) &&
				(groupDesc == null || groupDesc.trim().equals(""))){
			return service.getNonHanUserGroupCount();
		}
		
		return service.getNonHanUserGroupCount(groupName, groupDesc );
	}

	@Override
	public List<NonHanUserGroup> findRows(int startRow, int finishRow) {
		log.debug("In findRows(" + startRow + "," + finishRow + ")...");
		if((groupName == null || groupName.trim().equals("")) &&
				(groupDesc == null || groupDesc.trim().equals(""))){
			return service.getNonHanUserGroups(startRow, (finishRow - startRow) + 1);
		}else{
			return service.searchNonHanUserGroup(groupName, groupDesc,
					startRow, (finishRow - startRow) + 1);
		}
		
	}
	

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getGroupDesc() {
		return groupDesc;
	}

	public void setGroupDesc(String groupDesc) {
		this.groupDesc = groupDesc;
	}

}
