package org.cdph.han.alerts.jobs;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.alerts.services.AlertJPAService;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SimpleTrigger;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

/**
 * Class representing a Job used to publish an alert in a future time.
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Component ("alertPublicationJob")
public class AlertPublicationJob extends QuartzJobBean {
	/** Class log */
	private static final Log log = LogFactory.getLog (AlertPublicationJob.class);

	/* (non-Javadoc)
	 * @see org.springframework.scheduling.quartz.QuartzJobBean#executeInternal(org.quartz.JobExecutionContext)
	 */
	@Override
	protected void executeInternal (final JobExecutionContext jobContext) throws JobExecutionException {
		try {
			final ApplicationContext appCtx =
				(ApplicationContext) jobContext.getScheduler ().getContext ().get ("applicationContext");
			final AlertJPAService alertJPAService = (AlertJPAService) appCtx.getBean ("alertJPAService");
			final long alertId = jobContext.getJobDetail ().getJobDataMap ().getLong ("alertId");
			final int notificationReportId = alertJPAService.publishAlert (alertId);
			SimpleTrigger simpleTrigger = new SimpleTrigger (
					"QueryReport-" + alertId + ":" + notificationReportId,
					"ALERT_PUBLICATION_GROUP");
			simpleTrigger.setPriority (5);
			JobDetail jobDetail = new JobDetail ();
			jobDetail.setGroup ("ALERT_PUBLICATION_GROUP");
			jobDetail.setJobClass (ReportQueryJob.class);
			jobDetail.setName ("QUERY_REPORT");
			jobDetail.setJobDataMap (new JobDataMap ());
			jobDetail.getJobDataMap ().put ("alertId", alertId);
			jobDetail.getJobDataMap ().put ("notificationReportId", notificationReportId);
			simpleTrigger.setRepeatInterval (30000);
			simpleTrigger.setRepeatCount (SimpleTrigger.REPEAT_INDEFINITELY);
			jobContext.getScheduler ().scheduleJob (jobDetail, simpleTrigger);
		} catch (Exception ex) {
			log.error ("Error publishing alert.", ex);
			throw new JobExecutionException ("Error publishing alert.", ex, false);
		}
	}

}
