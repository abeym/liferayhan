package org.cdph.han.alerts.beans;

import java.util.List;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.alerts.dto.AlertSearchTO;
import org.cdph.han.alerts.services.AlertActionsService;
import org.cdph.han.util.asynch.beans.UpdatableBean;
import org.cdph.han.util.asynch.dto.AsynchRequestTO;
import org.cdph.han.util.asynch.dto.AsynchResponseTO;
import org.cdph.han.util.asynch.services.AsynchRefreshService;
import org.cdph.han.util.asynch.services.AsynchRequestType;

import com.icesoft.faces.async.render.Disposable;
import com.icesoft.faces.async.render.OnDemandRenderer;
import com.icesoft.faces.async.render.RenderManager;
import com.icesoft.faces.async.render.Renderable;
import com.icesoft.faces.webapp.xmlhttp.PersistentFacesState;
import com.icesoft.faces.webapp.xmlhttp.RenderingException;

/**
 * This class controls the alerts requiring action, the first search is only on the local database while an asynchronous
 * service is invoked, the user won't be able to do any action on the alert before the response from the asynchronous
 * service is received.
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class AlertActionBean extends AbstractAlertSearchBean implements UpdatableBean, Renderable, Disposable {
	/** Class serial version UID */
	private static final long serialVersionUID = 3850454280706128036L;
	/** Class log */
	private static final Log log = LogFactory.getLog (AlertActionBean.class);
	/** If the mir3 service response has arrived */
	private boolean mir3Response;
    /** ICE Faces Persistent faces state */
    private PersistentFacesState persistentFacesState;
    /** Render manager */
    private RenderManager renderManager;
    /** Service to register for updates while tracking an alert */
    private AsynchRefreshService asynchRefreshService;
	/** Renderer for updating the user gui */
	private OnDemandRenderer renderer;
	/** Service for retrieving and performing actions on the given alerts */
	private AlertActionsService alertActionsService;

	/**
	 * Default constructor to set the persistent faces state
	 */
	public AlertActionBean () {
		persistentFacesState = PersistentFacesState.getInstance ();
	}

	/**
	 * Action event to retrieve all the alerts that may require action
	 * @param event
	 */
	public void loadAlerts (final ActionEvent event) {
		searchTitle = "alerts.requiring.action.title";
		foundAlerts = alertActionsService.alertsPossiblyRequiringAction ();
		mir3Response = false;
		renderer = renderManager.getOnDemandRenderer (getSessionId ());
		renderer.add (this);
		final AsynchRequestTO request = new AsynchRequestTO (null, AsynchRequestType.ALERTS_REQUIRING_ACTION,
				getSessionId ());
		request.getRequest ().put ("USER_ID",
				FacesContext.getCurrentInstance ().getExternalContext ().getUserPrincipal ().getName ());
		asynchRefreshService.addOneTimeUpdatableBean (this, request);
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.util.asynch.beans.UpdatableBean#update(org.cdph.han.util.asynch.dto.AsynchResponseTO)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void update (final AsynchResponseTO updatedInfo) {
		if (updatedInfo.getResult ().get ("FOUND_ALERTS") instanceof List) {
			foundAlerts = (List<AlertSearchTO>) updatedInfo.getResult ().get ("FOUND_ALERTS");
			mir3Response = true;
			renderer.requestRender ();
			renderer.remove (this);
			renderer.dispose ();
		}
	}

	/* (non-Javadoc)
	 * @see com.icesoft.faces.async.render.Disposable#dispose()
	 */
	@Override
	public void dispose () {
		if (renderer != null) {
			renderer.remove (this);
			renderer.dispose ();
		}
	}

	/* (non-Javadoc)
	 * @see com.icesoft.faces.async.render.Renderable#getState()
	 */
	@Override
	public PersistentFacesState getState () {
		return persistentFacesState;
	}

	/* (non-Javadoc)
	 * @see com.icesoft.faces.async.render.Renderable#renderingException(com.icesoft.faces.webapp.xmlhttp.RenderingException)
	 */
	@Override
	public void renderingException (RenderingException arg0) {
		log.info ("Removing bean after receiving a rendering exception.");
		asynchRefreshService.removeBean (this);
		if (renderer != null) {
			renderer.remove (this);
			renderer.dispose ();
		}
	}

	/**
	 * @return the mir3Response
	 */
	public boolean isMir3Response () {
		return mir3Response;
	}

	/**
	 * @param mir3Response the mir3Response to set
	 */
	public void setMir3Response (boolean mir3Response) {
		this.mir3Response = mir3Response;
	}

	/**
	 * @param renderManager the renderManager to set
	 */
	public void setRenderManager (RenderManager renderManager) {
		this.renderManager = renderManager;
	}

	/**
	 * @param asynchRefreshService the asynchRefreshService to set
	 */
	public void setAsynchRefreshService (AsynchRefreshService asynchRefreshService) {
		this.asynchRefreshService = asynchRefreshService;
	}

	/**
	 * @param alertActionsService the alertActionsService to set
	 */
	public void setAlertActionsService (AlertActionsService alertActionsService) {
		this.alertActionsService = alertActionsService;
	}

}
