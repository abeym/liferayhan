package org.cdph.han.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

public class EmailValidator extends HanValidator {

	public static final String EMAIL_REGEX = "([a-zA-Z0-9\\_\\-\\.]+)@" +
		"(([0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})$";
	private Pattern mask;

	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {
		/* create a mask */
		if (mask == null){
			mask = Pattern.compile(EMAIL_REGEX);
		}		

		/* retrieve the string value of the field */
		String phoneNumber = (String) value;

		/* check to ensure that the value is a phone number */
		Matcher matcher = mask.matcher(phoneNumber);

		if (!matcher.matches()) {

			FacesMessage msg = new FacesMessage();
			msg.setDetail(getErrorMessage("email.validator.invalid.format"));
			msg.setSummary(getErrorMessage("email.validator.invalid.format"));
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);
		}

	}

}
