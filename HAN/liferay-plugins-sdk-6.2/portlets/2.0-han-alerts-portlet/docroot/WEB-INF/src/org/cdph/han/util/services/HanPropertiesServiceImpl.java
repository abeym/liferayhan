package org.cdph.han.util.services;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.cdph.han.persistence.HanProperty;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of the HanPropertiesService using JPA
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */

//abey cocmmenting to deploy
//@Repository
//@Service ("hanPropertiesService")
public class HanPropertiesServiceImpl implements HanPropertiesService {
	/** Entity manager */
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.cdph.han.util.services.HanPropertiesService#deleteProperty(java.lang.String)
	 */
	//abey ...@Transactional
	public void deleteProperty (final String name) {
		final HanProperty property = searchProperty (name);
		entityManager.remove (property);
		entityManager.flush ();
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.util.services.HanPropertiesService#getValue(java.lang.String)
	 */
	//abey ...@Transactional (readOnly = true)
	public String getValue (final String name) {
		return searchProperty (name).getValue ();
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.util.services.HanPropertiesService#insertProperty(java.lang.String, java.lang.String)
	 */
	//abey ...@Transactional
	public void insertProperty (final String name, final String value) {
		final HanProperty property = new HanProperty ();
		property.setName (name);
		property.setValue (value);
		entityManager.persist (property);
		entityManager.flush ();
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.util.services.HanPropertiesService#updateProperty(java.lang.String, java.lang.String)
	 */
	//abey ...@Transactional
	public void updateProperty (final String name, final String value) {
		final HanProperty property = searchProperty (name);
		property.setValue (value);
		entityManager.merge (property);
		entityManager.flush ();
	}

	/**
	 * Searches a HAN property by name
	 * @param name String with the name of the property
	 * @return String with the value of the property
	 */
	private HanProperty searchProperty (final String name) {
		final Query q = entityManager.createNamedQuery ("HanProperty.FindByName");
		q.setParameter ("name", name);
		return (HanProperty) q.getSingleResult ();
	}

	/**
	 * @param entityManager the entityManager to set
	 */
	//abey cocmmenting to deploy
	//@PersistenceContext
	public void setEntityManager (final EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
