package org.cdph.han.alerts.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.alerts.dto.AlertDraftTO;
import org.cdph.han.alerts.dto.AlertPublishedTO;
import org.cdph.han.alerts.dto.AlertScheduledTO;
import org.cdph.han.alerts.dto.AlertSearchTO;
import org.cdph.han.alerts.dto.AlertTO;
import org.cdph.han.alerts.dto.AttachmentTO;
import org.cdph.han.alerts.dto.FileTO;
import org.cdph.han.alerts.dto.OptionTO;
import org.cdph.han.alerts.exceptions.PublishAlertException;
import org.cdph.han.alerts.exceptions.SaveAlertException;
import org.cdph.han.alerts.util.Sorter;
import org.cdph.han.custgroups.dto.FilterCriteriaTO;
import org.cdph.han.custgroups.dto.RecipientTO;
import org.cdph.han.custgroups.dto.RecipientType;
import org.cdph.han.persistence.Alert;
import org.cdph.han.persistence.AlertAttachment;
import org.cdph.han.persistence.AlertAttachmentModel;
import org.cdph.han.persistence.AlertLocale;
import org.cdph.han.persistence.AlertLocation;
import org.cdph.han.persistence.AlertLocationModel;
import org.cdph.han.persistence.AlertMessage;
import org.cdph.han.persistence.AlertMessageModel;
import org.cdph.han.persistence.AlertModel;
import org.cdph.han.persistence.AlertOption;
import org.cdph.han.persistence.AlertOptionModel;
import org.cdph.han.persistence.AlertRecipient;
import org.cdph.han.persistence.AlertRecipientCriteria;
import org.cdph.han.persistence.AlertRecipientCriteriaModel;
import org.cdph.han.persistence.AlertRecipientModel;
import org.cdph.han.persistence.AlertReport;
import org.cdph.han.persistence.AlertReportModel;
import org.cdph.han.persistence.AlertTopic;
import org.cdph.han.persistence.ArchivedAlert;
import org.cdph.han.persistence.ArchivedAlertAttachment;
import org.cdph.han.persistence.AreaField;
import org.cdph.han.persistence.NonHanUser;
import org.cdph.han.persistence.NonHanUserGroup;
import org.cdph.han.persistence.Organization;
import org.cdph.han.persistence.Role;
import org.cdph.han.persistence.UserCustomGroup;
import org.cdph.han.persistence.UserData;
import org.cdph.han.util.services.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.icesoft.faces.component.inputfile.FileInfo;

/**
 * Implementation of the AlertJPAService
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Repository
@Service ("alertJPAService")
public class AlertJPAServiceImpl implements AlertJPAService {
	/** Upload directory */
	private static final String FILE_UPLOAD_DIRECTORY = "upload";
	/** Log */
	private static final Log log = LogFactory.getLog (AlertJPAServiceImpl.class);
	/** JPA entity manager */
	private EntityManager entityManager;
	/** MIR3 Service */
	//abey...
	//@Autowired
	private NotificationService notificationService;
	/** Service for getting the current user data */
	//abey ...
	//@Autowired
	private UserDataService userDataService;
	/** Service for determining the role of the current user */
	//abey ...
	//@Autowired
	private SecurityService securityService;

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.AlertJPAService#retrieveAlert(long)
	 */
	//abey...
		//@Transactional (readOnly = true)
	public AlertTO retrieveAlert (final long alertId, boolean archived) throws SaveAlertException {
		AlertTO alertTO = null;
		AlertModel alert = null;
		if (archived) {
			alert = entityManager.find (ArchivedAlert.class, alertId);
		} else {
			alert = entityManager.find (Alert.class, alertId);
		}
		if (alert != null) {
			alertTO = new AlertTO ();
			AlertMessageModel message = null;
			if (alert.getMessages () != null && alert.getMessages ().size () > 0) {
				message = alert.getMessages ().iterator ().next ();
			}
			alertTO.setAbstractText (message != null ? message.getAbstractText () : null);
			alertTO.setAlertId (alertId);
			alertTO.setAlertName (alert.getName ());
			alertTO.setAuthorUserId (alert.getAuthor ().getUserId ());
			alertTO.setConfirmResponse (alert.isConfirmResponse ());
			alertTO.setContactCycleDelay (convertToHoursMinutes (alert.getContactCycleDelay ()));
			alertTO.setContactCycles (alert.getContactAttempCycles ());
			alertTO.setContactUserId (alert.getContact ().getUserId ());
			alertTO.setCreatedDate (alert.getDateCreated ());
			alertTO.setDuration (convertToHoursMinutes (alert.getDuration ()));
			alertTO.setEntireMachine (alert.isEntireMachine ());
			alertTO.setExpeditedDelivery (alert.isExpeditedDelivery ());
			alertTO.setExpirationDate (alert.getDateExpired ());
			alertTO.setFullDetailText (message != null ? message.getPortalMessage () : null);
			alertTO.setTelephonyMessage (message != null ? message.getTelephonyMessage () : null);
			alertTO.setIncidentDate (alert.getIncidentDate ());
			alertTO.setLastModifiedDate (alert.getDateModified ());
			alertTO.setLeaveMessage (alert.getLeaveMessage ());
			alertTO.setLocationOverride (alert.isLocationOverride ());
			alertTO.setLocationOverridePriorities (readLocations (alert.getLocations ()));
			alertTO.setNotificationAttachment (readAttachments (
					alertTO, alert.getAttachments (), message != null ? message.getAttachment () : null));
			alertTO.setNotificationDate (alert.getDateScheduled ());
			alertTO.setNotificationMethod (alert.getNotificationMethod ());
			alertTO.setOneTextContact (alert.isOneTextContact ());
			alertTO.setOverrideDefaultStatus (alert.isOverrideDefaultsOnly ());
			alertTO.setPartialMachine (alert.isPartialMachine ());
			alertTO.setPlayGreeting (alert.isPlayGreeting ());
			alertTO.setPriority (alert.getPriority ());
			alertTO.setPublishedByUserId (alert.getPublisher () != null
					? alert.getPublisher ().getUserId () : null);
			alertTO.setPublishedDate (alert.getDatePublished ());
			readRecipients (alertTO, alert.getRecipients ());
			alertTO.setReplayMessages (alert.isReplayMessages ());
			alertTO.setReportRecipients (alert.isReportRecipients ());
			alertTO.setRequiresPIN (alert.isRequiresPIN ());
			readResponseOptions (alertTO, alert.getOptions ());
			alertTO.setScheduled (alert.getDateScheduled () != null);
			alertTO.setScheduledDate (alert.getDateScheduled ());
			alertTO.setSelectLanguage (alert.isSelectLanguage ());
			alertTO.setStopAfterEntireMessage (alert.isEntireMessage ());
			alertTO.setStopAnswerPhone (alert.isAnswerPhone ());
			alertTO.setTextDelay (convertToHoursMinutes (alert.getTextDeviceDelay ()));
			alertTO.setTopicId (alert.getTopic () != null ? alert.getTopic ().getId () : -1);
			alertTO.setUseAlternates (alert.isUseAlternates ());
			alertTO.setUseTopics (alert.isUseTopics ());
			alertTO.setValidateRecipients (alert.isValidateRecipients ());
			alertTO.setVoiceDelivery (alert.getVoiceDelivery ());
			alertTO.setVoiceFile (alert.getVoiceFile ());
			alertTO.setArchived (archived);
		}
		return alertTO;
	}

	/**
	 * Reads all the locations associated to the alert
	 * @param locations locations in the db
	 * @return the list of devices in the selected priority order
	 */
	private List<Long> readLocations (final Collection<? extends AlertLocationModel> locations) {
		final List<Long> priorities = new ArrayList<Long> (locations.size ());
		for (AlertLocationModel location : locations) {
			priorities.add (Long.valueOf (location.getDeviceId ()));
		}
		return priorities;
	}

	/**
	 * Reads all the alert options into the given AlertTO object
	 * @param alertTO AlertTO object to add the options
	 * @param options Collection of AlertOption objects
	 */
	private void readResponseOptions (final AlertTO alertTO,
			final Collection<? extends AlertOptionModel> options) {
		alertTO.setResponseOptions (new ArrayList<OptionTO> ());
		if (options != null && options.size () > 0) {
			OptionTO optionTO;
			for (AlertOptionModel option : options) {
				optionTO = new OptionTO ();
				optionTO.setAction (option.getAction ());
				optionTO.setOptionText (option.getMessage ());
				optionTO.setCallBridge (option.getCallBridge ());
				alertTO.getResponseOptions ().add (optionTO);
			}
		}
	}

	/**
	 * Reads all the recipients in the database for the given alert
	 * @param alertTO AlertTO object to set the recipients
	 * @param recipients Collection of AlertRecipient objects
	 */
	private void readRecipients (final AlertTO alertTO,
			final Collection<? extends AlertRecipientModel> recipients) {
		alertTO.setRecipients (new ArrayList<RecipientTO> ());
		RecipientTO recipientTO;
		if (recipients != null && recipients.size () > 0) {
			for (AlertRecipientModel recipient : recipients) {
				recipientTO = new RecipientTO ();
				recipientTO.setNotified (recipient.isNotified ());
				if (recipient.getAreaField () != null) {
					recipientTO.setId (recipient.getAreaField ().getAreaFieldId ());
					recipientTO.setName (recipient.getAreaField ().getDescription ());
					recipientTO.setType (RecipientType.AREA_FIELD);
				} else if (recipient.getNonHanUser () != null) {
					recipientTO.setId (recipient.getNonHanUser ().getId ());
					recipientTO.setName (recipient.getNonHanUser ().getFirstName ()
							+ " " + recipient.getNonHanUser ().getLastName ());
					recipientTO.setType (RecipientType.NON_HAN_USER);
				} else if (recipient.getNonHanUserGroup () != null) {
					recipientTO.setId (recipient.getNonHanUserGroup ().getId ());
					recipientTO.setName (recipient.getNonHanUserGroup ().getName ());
					recipientTO.setType (RecipientType.NON_HAN_GROUP);
				} else if (recipient.getOrganization () != null) {
					recipientTO.setId (recipient.getOrganization ().getOrganizationId ());
					recipientTO.setName (recipient.getOrganization ().getName ());
					recipientTO.setType (recipient.getOrganization ().getParentOrganizationId () == 0
							? RecipientType.ORGANIZATION_TYPE : RecipientType.ORGANIZATION);
				} else if (recipient.getRole () != null) {
					recipientTO.setId (recipient.getRole ().getRoleId ());
					recipientTO.setName (recipient.getRole ().getName ());
					recipientTO.setType (RecipientType.ROLE);
				} else if (recipient.getUser () != null) {
					recipientTO.setId (recipient.getUser ().getUserId ());
					recipientTO.setName (recipient.getUser ().getContact ().getFirstName ()
							+ " " + recipient.getUser ().getContact ().getLastName ());
					recipientTO.setType (RecipientType.USER);
				} else if (recipient.getUserCustomGroup () != null) {
					recipientTO.setId (recipient.getUserCustomGroup ().getGroupId ());
					recipientTO.setName (recipient.getUserCustomGroup ().getName ());
					recipientTO.setType (RecipientType.GROUP);
				} else if (recipient.getCriterias () != null && !recipient.getCriterias ().isEmpty ()) {
					StringBuilder organization = new StringBuilder ();
					StringBuilder role = new StringBuilder ();
					StringBuilder areaField = new StringBuilder ();
					String organizationType = null;
					FilterCriteriaTO criteria = null;
					for (AlertRecipientCriteriaModel dbCriteria : recipient.getCriterias ()) {
						criteria = new FilterCriteriaTO ();
						criteria.setOrganizationTypeId (dbCriteria.getOrganizationType ().getOrganizationId ());
						if (dbCriteria.getAreaField () != null) {
							areaField.append (dbCriteria.getAreaField ().getDescription ()).append (", ");
							criteria.setAreaFieldId ((long) dbCriteria.getAreaField ().getAreaFieldId ());
						}
						if (dbCriteria.getOrganization () != null) {
							organization.append (dbCriteria.getOrganization ().getName ()).append (", ");
							criteria.setOrganizationId (dbCriteria.getOrganization ().getOrganizationId ());
						}
						if (dbCriteria.getRole () != null) {
							role.append (dbCriteria.getRole ().getName ()).append (", ");
							criteria.setRoleId (dbCriteria.getRole ().getRoleId ());
						}
						if (organizationType == null) {
							organizationType = dbCriteria.getOrganizationType ().getName ();
						}
						recipientTO.getCriteria ().add (criteria);
					}
					StringBuilder caption = new StringBuilder ();
					caption.append (organizationType).append (" / ");
					if (organization.length () > 0) {
						caption.append (organization.substring (0, organization.lastIndexOf (", ")));
					}
					caption.append (" / ");
					if (areaField.length () > 0) {
						caption.append (areaField.substring (0, areaField.lastIndexOf (", ")));
					}
					caption.append (" / ");
					if (role.length () > 0) {
						caption.append (role.substring (0, role.lastIndexOf (", ")));
					}
					recipientTO.setChicagoOnly (recipient.isChicagoOnly ());
					recipientTO.setType (RecipientType.CRITERIA);
					recipientTO.setName (caption.toString ());
				}
				alertTO.getRecipients ().add (recipientTO);
			}
		}
	}

	/**
	 * Reads all the attachments of the alert and puts them available
	 * @param alertTO AlertTO object to add the attachments
	 * @param attachments Collection of AlertAttachment objects
	 * @return integer with the index of the telephony attachment if any
	 */
	private int readAttachments (final AlertTO alertTO,
			final Collection<? extends AlertAttachmentModel> attachments,
			final AlertAttachmentModel telAttachment) throws SaveAlertException {
		try {
			final Object reqObj = FacesContext.getCurrentInstance ().getExternalContext ().getRequest ();
			String uploadDir = "/tmp/upload";
			if (reqObj instanceof HttpServletRequest) {
				String appDir = ((HttpServletRequest) reqObj).getSession ().getServletContext ().getRealPath (
						((HttpServletRequest) reqObj).getSession ().getServletContext ().getServletContextName ());
				uploadDir = appDir.substring (0, appDir.lastIndexOf ('/')) + '/' + FILE_UPLOAD_DIRECTORY + '/'
						+ ((HttpServletRequest) reqObj).getSession ().getId ();
			} else if (reqObj instanceof PortletRequest) {
				String appDir = ((PortletRequest) reqObj).getPortletSession ().getPortletContext ().getRealPath (
						((PortletRequest) reqObj).getPortletSession ().getPortletContext (
								).getPortletContextName ());
				uploadDir = appDir.substring (0, appDir.lastIndexOf ('/')) + '/' + FILE_UPLOAD_DIRECTORY + '/' +
						((PortletRequest) reqObj).getPortletSession ().getId ();
			}
			final File uploadPath = new File (uploadDir);
			if (!uploadPath.exists ()) {
				uploadPath.mkdirs ();
			}
			int attachmentIndex = -1;
			alertTO.setAttachments (new ArrayList<FileTO> ());
			if (attachments != null && attachments.size () > 0) {
				FileTO fileTO;
				FileInfo fileInfo;
				File currentFile;
				WritableByteChannel channel;
				ByteBuffer buffer = ByteBuffer.allocate (20971520);
				int index = 0;
				for (AlertAttachmentModel alertAttachment : attachments) {
					fileTO = new FileTO ();
					fileInfo = new FileInfo ();
					fileInfo.setContentType (alertAttachment.getMimeType ());
					fileInfo.setFileName (alertAttachment.getName ());
					fileInfo.setPercent (100);
					//fileInfo.setPhysicalPath (uploadDir + '/' + fileInfo.getFileName ());
					fileTO.setFileInfo (fileInfo);
					currentFile = new File (uploadDir + '/' + fileInfo.getFileName ());
					if (!currentFile.exists ()) {
						channel = Channels.newChannel (new FileOutputStream (currentFile));
						buffer.clear ();
						buffer.put (alertAttachment.getAttachment ());
						buffer.flip ();
						channel.write (buffer);
						channel.close ();
					}
					fileTO.setFile (currentFile);
					if (alertAttachment.equals (telAttachment)) {
						attachmentIndex = index;
					}
					++index;
					alertTO.getAttachments ().add (fileTO);
				}
			}
			return attachmentIndex;
		} catch (FileNotFoundException fnex) {
			throw new SaveAlertException ("Attachment not found.", fnex);
		} catch (IOException ioex) {
			throw new SaveAlertException ("Error reading attachment.", ioex);
		} catch (NullPointerException npex) {
			// This means that the FacesContext can't be retrieved, so the alert is being published by a job
			return -1;
		}
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.AlertJPAService#saveAlert(org.cdph.han.alerts.dto.AlertTO)
	 */
	//abey...
		//@Transactional (propagation = Propagation.REQUIRED)
	public long saveAlert (final AlertTO alertTO) throws SaveAlertException {
		Alert alert = null;
		boolean newAlert = false;
		if (alertTO.getAlertId () != null) {
			alert = entityManager.find (Alert.class, alertTO.getAlertId ());
		}
		if (alert == null) {
			alert = new Alert ();
			newAlert = true;
		}
		AlertLocale locale = entityManager.find (AlertLocale.class, 1);
		alert.setName (alertTO.getAlertName ());
		if (newAlert) {
			alert.setAuthor (userDataService.getCurrentUserData ());
			alert.setModifier (alert.getAuthor ());
			alert.setDateModified (alertTO.getCreatedDate ());
		} else {
			alert.setModifier (userDataService.getCurrentUserData ());
			alert.setDateModified (new Date ());
		}
		alert.setCategory (null);
		alert.setConfirmResponse (alertTO.isConfirmResponse ());
		alert.setContact (entityManager.find (UserData.class, alertTO.getContactUserId ()));
		alert.setContactAttempCycles (alertTO.getContactCycles ());
		alert.setContactCycleDelay (convertToMinutes (alertTO.getContactCycleDelay ()));
		alert.setDateCreated (alertTO.getCreatedDate ());
		alert.setDateExpired (alertTO.getExpirationDate ());
		alert.setDuration (convertToMinutes (alertTO.getDuration ()));
		alert.setEntireMachine (alertTO.isEntireMachine ());
		alert.setExpeditedDelivery (alertTO.isExpeditedDelivery ());
		alert.setIncidentDate (alertTO.getIncidentDate ());
		alert.setLeaveMessage (alertTO.getLeaveMessage ());
		alert.setLocationOverride (alertTO.isLocationOverride ());
		alert.setDateScheduled (alertTO.getNotificationDate ());
		alert.setNotificationMethod (alertTO.getNotificationMethod ());
		alert.setOneTextContact (alertTO.isOneTextContact ());
		alert.setOverrideDefaultsOnly (alertTO.isOverrideDefaultStatus ());
		alert.setPartialMachine (alertTO.isPartialMachine ());
		alert.setPlayGreeting (alertTO.isPlayGreeting ());
		alert.setPriority (alertTO.getPriority ());
		alert.setDatePublished (alertTO.getPublishedDate ());
		alert.setReadyToPublish (alertTO.isReadyToPublish ());
		alert.setReplayMessages (alertTO.isReplayMessages ());
		alert.setReportRecipients (alertTO.isReportRecipients ());
		alert.setRequiresPIN (alertTO.isRequiresPIN ());
		alert.setDateScheduled (alertTO.isScheduled () ? alertTO.getScheduledDate () : null);
		alert.setSelectLanguage (alertTO.isSelectLanguage ());
		alert.setEntireMessage (alertTO.isStopAfterEntireMessage ());
		alert.setAnswerPhone (alertTO.isStopAnswerPhone ());
		alert.setTextDeviceDelay (convertToMinutes (alertTO.getTextDelay ()));
		alert.setTopic (alertTO.getTopicId () == null ? null :
			entityManager.find (AlertTopic.class, alertTO.getTopicId ()));
		alert.setUseAlternates (alertTO.isUseAlternates ());
		alert.setUseTopics (alertTO.isUseTopics ());
		alert.setValidateRecipients (alertTO.isValidateRecipients ());
		alert.setVoiceDelivery (alertTO.getVoiceDelivery ());
		alert.setVoiceFile (alertTO.getVoiceFile ());
		if (newAlert) {
			entityManager.persist (alert);
		} else {
			entityManager.merge (alert);
		}
		if (alertTO.isLocationOverride ()) {
			setLocations (alert, alertTO.getLocationOverridePriorities ());
		}
		setRecipients (alert, alertTO.getRecipients ());
		setResponseOptions (alert, alertTO.getResponseOptions (), locale);
		AlertMessage message = null;
		if (alert.getMessages () != null && alert.getMessages ().size () > 0) {
			message = alert.getMessages ().iterator ().next ();
			message.setAbstractText (alertTO.getAbstractText ());
			message.setPortalMessage (alertTO.getFullDetailText ());
			message.setLocale (locale);
			message.setTelephonyMessage (alertTO.getTelephonyMessage ());
			message.setAttachment (setAttachments (alert, alertTO.getAttachments (),
					alertTO.getNotificationAttachment ()));
			entityManager.merge (message);
		} else {
			alert.setMessages (new ArrayList<AlertMessage> ());
			message = new AlertMessage ();
			alert.getMessages ().add (message);
			message.setAbstractText (alertTO.getAbstractText ());
			message.setPortalMessage (alertTO.getFullDetailText ());
			message.setLocale (locale);
			message.setAlert (alert);
			message.setTelephonyMessage (alertTO.getTelephonyMessage ());
			message.setAttachment (setAttachments (alert, alertTO.getAttachments (),
					alertTO.getNotificationAttachment ()));
			entityManager.persist (message);
		}
		/*try {
			final long auditTrailId = CounterLocalServiceUtil.increment (HanAuditTrail.class.getName ());
			HanAuditTrail auditTrail = HanAuditTrailLocalServiceUtil.createHanAuditTrail (
					auditTrailId);
			auditTrail.setActionPerformed (AuditTrailAction.ALERT_MODIFIED.toString ());
			auditTrail.setAlertId (alert.getId ());
			auditTrail.setDate (alert.getDateModified ());
			auditTrail.setUserId (alert.getModifier ().getUserId ());
			auditTrail.setAuditTrailId (auditTrailId);
			HanAuditTrailLocalServiceUtil.addHanAuditTrail (auditTrail);
		} catch (SystemException ex) {
			log.error ("Error creating audit trail.", ex);
			throw new RuntimeException ("Error creating audit trail.", ex);
		}*/
		entityManager.flush ();
		return alert.getId ();
	}

	/**
	 * Stores the priorities in the Database for the location override
	 * @param alert to set the locations
	 * @param locationOverridePriorities the priorities to save
	 */
	private void setLocations (final Alert alert, final List<Long> locationOverridePriorities) {
		if (alert.getLocations () == null) {
			alert.setLocations (new ArrayList<AlertLocation> ());
		}
		final AlertLocation[] dbLocations = alert.getLocations ().toArray (
				new AlertLocation[alert.getLocations ().size ()]);
		if (dbLocations.length > locationOverridePriorities.size ()) {
			for (int i = 0; i < dbLocations.length; i++) {
				if (i < locationOverridePriorities.size ()) {
					if (dbLocations[i].getDeviceId () != locationOverridePriorities.get (i)) {
						dbLocations[i].setDeviceId (locationOverridePriorities.get (i).intValue ());
					}
					entityManager.merge (dbLocations[i]);
				} else {
					alert.getLocations ().remove (dbLocations[i]);
					entityManager.remove (dbLocations[i]);
				}
			}
		} else {
			AlertLocation location;
			for (int i = 0; i < locationOverridePriorities.size (); i++) {
				if (i < dbLocations.length) {
					if (dbLocations[i].getDeviceId () != locationOverridePriorities.get (i)) {
						dbLocations[i].setDeviceId (locationOverridePriorities.get (i).intValue ());
					}
					entityManager.merge (dbLocations[i]);
				} else {
					location = new AlertLocation ();
					location.setAlert (alert);
					location.setDeviceId (locationOverridePriorities.get (i).intValue ());
					location.setPriority (i + 1);
					entityManager.persist (location);
				}
			}
		}
	}

	/**
	 * Sets the recipients of the alert
	 * @param alert Alert object with the information of the alert
	 * @param recipients List<RecipientTO> with the recipients.
	 */
	private void setRecipients (final Alert alert, final List<RecipientTO> recipients) {
		if (recipients != null && recipients.size () > 0) {
			int index = 0;
			if (alert.getRecipients () == null) {
				alert.setRecipients (new ArrayList<AlertRecipient> ());
			}
			if (alert.getRecipients ().size () < recipients.size ()) {
				AlertRecipient alertRecipient;
				AlertRecipientCriteria recipientCriteria;
				for (RecipientTO recipient : recipients) {
					if (alert.getRecipients ().size () > index) {
						alertRecipient = ((List<AlertRecipient>) alert.getRecipients ()).get (index);
					} else {
						alertRecipient = new AlertRecipient ();
						alertRecipient.setAlert (alert);
					}
					alertRecipient.setAreaField (null);
					alertRecipient.setNonHanUser (null);
					alertRecipient.setNonHanUserGroup (null);
					alertRecipient.setOrganization (null);
					alertRecipient.setRole (null);
					alertRecipient.setUser (null);
					alertRecipient.setUserCustomGroup (null);
					alertRecipient.setNotified (recipient.isNotified ());
					if (recipient.getType ().equals (RecipientType.AREA_FIELD)) {
						alertRecipient.setAreaField (
								entityManager.find (AreaField.class, (int) recipient.getId ()));
					} else if (recipient.getType ().equals (RecipientType.GROUP)) {
						alertRecipient.setUserCustomGroup (
								entityManager.find (UserCustomGroup.class, recipient.getId ()));
					} else if (recipient.getType ().equals (RecipientType.NON_HAN_GROUP)) {
						alertRecipient.setNonHanUserGroup (
								entityManager.find (NonHanUserGroup.class, recipient.getId ()));
					} else if (recipient.getType ().equals (RecipientType.NON_HAN_USER)) {
						alertRecipient.setNonHanUser (
								entityManager.find (NonHanUser.class, recipient.getId ()));
					} else if (recipient.getType ().equals (RecipientType.ORGANIZATION)
							|| recipient.getType ().equals (RecipientType.ORGANIZATION_TYPE)) {
						alertRecipient.setOrganization (
								entityManager.find (Organization.class, recipient.getId ()));
					} else if (recipient.getType ().equals (RecipientType.ROLE)) {
						alertRecipient.setRole (entityManager.find (Role.class, recipient.getId ()));
					} else if (recipient.getType ().equals (RecipientType.USER)) {
						alertRecipient.setUser (entityManager.find (UserData.class, recipient.getId ()));
					} else if (recipient.getType ().equals (RecipientType.CRITERIA)) {
						alertRecipient.setChicagoOnly (recipient.isChicagoOnly ());
						if (alertRecipient.getCriterias () != null && !alertRecipient.getCriterias ().isEmpty ()) {
							for (AlertRecipientCriteria toRemove : alertRecipient.getCriterias ()) {
								entityManager.remove (toRemove);
							}
							alertRecipient.getCriterias ().clear ();
						} else {
							alertRecipient.setCriterias (new ArrayList<AlertRecipientCriteria> ());
						}
						for (FilterCriteriaTO criteria : recipient.getCriteria ()) {
							recipientCriteria = new AlertRecipientCriteria ();
							recipientCriteria.setRecipient (alertRecipient);
							recipientCriteria.setOrganizationType (
									entityManager.find (Organization.class, criteria.getOrganizationTypeId ()));
							recipientCriteria.setAreaField (criteria.getAreaFieldId () == null ? null :
									entityManager.find (AreaField.class, criteria.getAreaFieldId ().intValue ()));
							recipientCriteria.setOrganization (criteria.getOrganizationId () == null ? null :
									entityManager.find (Organization.class, criteria.getOrganizationId ()));
							recipientCriteria.setRole (criteria.getRoleId () == null ? null :
									entityManager.find (Role.class, criteria.getRoleId ()));
							alertRecipient.getCriterias ().add (recipientCriteria);
						}
					}
					if (alert.getRecipients ().size () > index) {
						entityManager.merge (alertRecipient);
					} else {
						entityManager.persist (alertRecipient);
					}
					++index;
				}
			} else {
				RecipientTO recipient;
				AlertRecipientCriteria recipientCriteria;
				for (AlertRecipient alertRecipient : alert.getRecipients ()) {
					if (recipients.size () > index) {
						recipient = recipients.get (index++);
						alertRecipient.setAreaField (null);
						alertRecipient.setNonHanUser (null);
						alertRecipient.setNonHanUserGroup (null);
						alertRecipient.setOrganization (null);
						alertRecipient.setRole (null);
						alertRecipient.setUser (null);
						alertRecipient.setUserCustomGroup (null);
						alertRecipient.setNotified (recipient.isNotified ());
						if (recipient.getType ().equals (RecipientType.AREA_FIELD)) {
							alertRecipient.setAreaField (
									entityManager.find (AreaField.class, (int) recipient.getId ()));
						} else if (recipient.getType ().equals (RecipientType.GROUP)) {
							alertRecipient.setUserCustomGroup (
									entityManager.find (UserCustomGroup.class, recipient.getId ()));
						} else if (recipient.getType ().equals (RecipientType.NON_HAN_GROUP)) {
							alertRecipient.setNonHanUserGroup (
									entityManager.find (NonHanUserGroup.class, recipient.getId ()));
						} else if (recipient.getType ().equals (RecipientType.NON_HAN_USER)) {
							alertRecipient.setNonHanUser (
									entityManager.find (NonHanUser.class, recipient.getId ()));
						} else if (recipient.getType ().equals (RecipientType.ORGANIZATION)
								|| recipient.getType ().equals (RecipientType.ORGANIZATION_TYPE)) {
							alertRecipient.setOrganization (
									entityManager.find (Organization.class, recipient.getId ()));
						} else if (recipient.getType ().equals (RecipientType.ROLE)) {
							alertRecipient.setRole (entityManager.find (Role.class, recipient.getId ()));
						} else if (recipient.getType ().equals (RecipientType.USER)) {
							alertRecipient.setUser (entityManager.find (UserData.class, recipient.getId ()));
						} else if (recipient.getType ().equals (RecipientType.CRITERIA)) {
							alertRecipient.setChicagoOnly (recipient.isChicagoOnly ());
							if (alertRecipient.getCriterias () != null && !alertRecipient.getCriterias ().isEmpty ()) {
								for (AlertRecipientCriteria toRemove : alertRecipient.getCriterias ()) {
									entityManager.remove (toRemove);
								}
								alertRecipient.getCriterias ().clear ();
							} else {
								alertRecipient.setCriterias (new ArrayList<AlertRecipientCriteria> ());
							}
							for (FilterCriteriaTO criteria : recipient.getCriteria ()) {
								recipientCriteria = new AlertRecipientCriteria ();
								recipientCriteria.setRecipient (alertRecipient);
								recipientCriteria.setOrganizationType (
										entityManager.find (Organization.class, criteria.getOrganizationTypeId ()));
								recipientCriteria.setAreaField (criteria.getAreaFieldId () == null ? null :
										entityManager.find (AreaField.class, criteria.getAreaFieldId ().intValue ()));
								recipientCriteria.setOrganization (criteria.getOrganizationId () == null ? null :
										entityManager.find (Organization.class, criteria.getOrganizationId ()));
								recipientCriteria.setRole (criteria.getRoleId () == null ? null :
										entityManager.find (Role.class, criteria.getRoleId ()));
								alertRecipient.getCriterias ().add (recipientCriteria);
							}
						}
						entityManager.merge (alertRecipient);
					} else {
						entityManager.remove (alertRecipient);
					}
				}
			}
		} else {
			if (alert.getRecipients () != null && alert.getRecipients ().size () > 0) {
				for (AlertRecipientModel recipient : alert.getRecipients ()) {
					entityManager.remove (recipient);
				}
			}
		}
	}

	/**
	 * This method sets the response options into the alert
	 * @param alert Alert object to add the response options
	 * @param responseOptions List<OptionTO> with the options to set
	 * @param locale locale of the current message
	 */
	private void setResponseOptions (final Alert alert, final List<OptionTO> responseOptions,
			final AlertLocale locale) {
		if (responseOptions != null && responseOptions.size () > 0) {
			if (alert.getOptions () == null) {
				alert.setOptions (new ArrayList<AlertOption> ());
			}
			int index = 0;
			if (alert.getOptions ().size () < responseOptions.size ()) {
				AlertOption alertOption = null;
				for (OptionTO option : responseOptions) {
					if (alert.getOptions ().size () > index) {
						alertOption = ((List<AlertOption>) alert.getOptions ()).get (index);
					} else {
						alertOption = new AlertOption ();
					}
					alertOption.setAction (option.getAction ());
					alertOption.setAlert (alert);
					alertOption.setLocale (locale);
					alertOption.setMessage (option.getOptionText ());
					alertOption.setOptionId (index + 1);
					alertOption.setCallBridge (option.getCallBridge ());
					if (alert.getOptions ().size () > index) {
						entityManager.merge (alertOption);
					} else {
						entityManager.persist (alertOption);
					}
					alert.getOptions ().add (alertOption);
					++index;
				}
			} else {
				OptionTO option;
				for (AlertOption alertOption : alert.getOptions ()) {
					if (responseOptions.size () > index) {
						option = responseOptions.get (index);
						alertOption.setAction (option.getAction ());
						alertOption.setLocale (locale);
						alertOption.setMessage (option.getOptionText ());
						alertOption.setCallBridge (option.getCallBridge ());
						entityManager.merge (alertOption);
					} else {
						entityManager.remove (alertOption);
					}
					++index;
				}
			}
		} else {
			if (alert.getOptions () != null && alert.getOptions ().size () > 0) {
				for (AlertOptionModel option : alert.getOptions ()) {
					entityManager.remove (option);
				}
			}
		}
	}

	/**
	 * This method sets all the attachments in the alert, removes attachments and adds new
	 * @param alert Alert object to update or store in database
	 * @param attachments List of attachments to store in the database
	 * @return AlertAttachment with the telephony attachment if any
	 */
	private AlertAttachment setAttachments (final Alert alert, final List<FileTO> attachments,
			final Integer attachmentIndex) throws SaveAlertException {
		try {
			AlertAttachment currentAttachment;
			AlertAttachment telAttachment = null;
			FileTO currentFileTO;
			int index = 0;
			int bytesRead = 0;
			ReadableByteChannel channel;
			ByteBuffer buffer = ByteBuffer.allocate (20971520);
			if (attachments != null && attachments.size () > 0) {
				if (alert.getAttachments () == null || alert.getAttachments ().size () == 0) {
					alert.setAttachments (new ArrayList<AlertAttachment> ());
				}
				if (alert.getAttachments ().size () > attachments.size ()) {
					for (AlertAttachment attachment : alert.getAttachments ()) {
						if (index < attachments.size ()) {
							currentFileTO = attachments.get (index);
							attachment.setMimeType (currentFileTO.getFileInfo ().getContentType ());
							attachment.setName (currentFileTO.getFileInfo ().getFileName ());
							channel = Channels.newChannel (new FileInputStream (currentFileTO.getFile ()));
							buffer.clear ();
							bytesRead = channel.read (buffer);
							attachment.setAttachment (new byte[bytesRead]);
							buffer.rewind ();
							buffer.get (attachment.getAttachment ());
							channel.close ();
							entityManager.merge (attachment);
							if (index == (attachmentIndex != null ? attachmentIndex : -1)) {
								telAttachment = attachment;
							}
						} else {
							entityManager.remove (attachment);
						}
						++index;
					}
				} else {
					AlertAttachment[] tempArray = alert.getAttachments ().toArray (
							new AlertAttachment[alert.getAttachments ().size ()]);
					for (FileTO fileTO : attachments) {
						if (index < alert.getAttachments ().size ()) {
							currentAttachment = tempArray[index];
						} else {
							currentAttachment = new AlertAttachment ();
							currentAttachment.setAlert (alert);
						}
						currentAttachment.setMimeType (fileTO.getFileInfo ().getContentType ());
						currentAttachment.setName (fileTO.getFileInfo ().getFileName ());
						channel = Channels.newChannel (new FileInputStream (fileTO.getFile ()));
						buffer.clear ();
						bytesRead = channel.read (buffer);
						currentAttachment.setAttachment (new byte[bytesRead]);
						buffer.rewind ();
						buffer.get (currentAttachment.getAttachment ());
						channel.close ();
						if (alert.getAttachments ().contains (currentAttachment)) {
							entityManager.merge (currentAttachment);
						} else {
							entityManager.persist (currentAttachment);
						}
						if (index == (attachmentIndex != null ? attachmentIndex : -1)) {
							telAttachment = currentAttachment;
						}
						++index;
					}
				}
			} else if (alert.getAttachments () != null && alert.getAttachments ().size () > 0) {
				for (AlertAttachmentModel attachment : alert.getAttachments ()) {
					entityManager.remove (attachment);
				}
			}
			return telAttachment;
		} catch (FileNotFoundException fnex) {
			throw new SaveAlertException ("Attachment not found.", fnex);
		} catch (IOException ioex) {
			throw new SaveAlertException ("Error reading attachment.", ioex);
		}
	}

	/**
	 * This method converts from a string (HH:MM) to an integer of the total of minutes
	 * @param time String with time in format (HH:MM)
	 * @return int with the total of minutes
	 */
	private int convertToMinutes (final String time) {
		int index = time.indexOf (':');
		int hours = Integer.parseInt (time.substring (0, index));
		int minutes = Integer.parseInt (time.substring (index + 1));
		minutes += hours * 60;
		return minutes;
	}

	/**
	 * This method converts an integer to a String representing the time in the format hh:mm
	 * @param minutes int with the total minutes
	 * @return String with the time in the format hh:mm
	 */
	private String convertToHoursMinutes (final int minutes) {
		final int hours = minutes / 60;
		final int mins = minutes - hours * 60;
		final StringBuilder time = new StringBuilder ();
		if (hours < 10) {
			time.append ("00");
		} else if (hours < 100) {
			time.append ('0');
		}
		time.append (hours).append (':');
		if (mins < 10) {
			time.append ('0');
		}
		time.append (mins);
		return time.toString ();
	}

	/**
	 * @param entityManager the entityManager to set
	 */
	//abey...
	//@PersistenceContext
	public void setEntityManager (EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.AlertJPAService#searchDrafts()
	 */
	//abey...
		//@Transactional (readOnly = true)
	public List<AlertSearchTO> searchDrafts () {
		final List<AlertSearchTO> results = new ArrayList<AlertSearchTO> ();
		final Query q = entityManager.createNamedQuery ("Alert.FindDrafts");
		final List<?> alerts = q.getResultList ();
		AlertModel alert;
		AlertDraftTO draft;
		for (Object o : alerts) {
			alert = (AlertModel) o;
			draft = new AlertDraftTO ();
			draft.setAlertId (alert.getId ());
			draft.setCreationDate (alert.getDateCreated ());
			draft.setLastModifiedDate (alert.getDateModified ());
			draft.setTitle (alert.getName ());
			draft.setTopic (alert.getTopic () != null ? alert.getTopic ().getTitle () : "");
			results.add (draft);
		}
		return results;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.AlertJPAService#searchCurrentByDate()
	 */
	//abey...
		//@Transactional (readOnly = true)
	public List<AlertSearchTO> searchCurrentByDate () {
		return searchCurrent (securityService.isUserAdmin ()
				? "Alert.FindPublishedByDate" : "Alert.FindByRecDate");
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.AlertJPAService#searchArchivedByAlertLevel()
	 */
	//abey...
		//@Transactional (readOnly = true)
	public Map<String, List<AlertSearchTO>> searchArchivedByAlertLevel () {
		try {
			Sorter<String, AlertSearchTO> sorter = new Sorter<String, AlertSearchTO> ();
			return sorter.groupBy (searchArchived (securityService.isUserAdmin ()
					? "ArchivedAlert.FindArchivedByLevel" : "ArchivedAlert.FindByRecLevel"),
					AlertPublishedTO.class.getMethod ("getLevelOfAlert", new Class[]{}), String.class);
		} catch (Exception ex) {
			throw new RuntimeException ("Error searching archived alerts.", ex);
		}
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.AlertJPAService#searchArchivedByPublishDate()
	 */
	//abey ...@Transactional (readOnly = true)
	public List<AlertSearchTO> searchArchivedByPublishDate () {
		return searchArchived (securityService.isUserAdmin ()
				? "ArchivedAlert.FindArchivedByDate" : "ArchivedAlert.FindByRecDate");
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.AlertJPAService#searchArchivedByTopic()
	 */
	//abey ...@Transactional (readOnly = true)
	public Map<String, List<AlertSearchTO>> searchArchivedByTopic () {
		try {
			Sorter<String, AlertSearchTO> sorter = new Sorter<String, AlertSearchTO> ();
			return sorter.groupBy (searchArchived (securityService.isUserAdmin ()
					? "ArchivedAlert.FindArchivedByTopic" : "ArchivedAlert.FindByRecTopic"),
					AlertPublishedTO.class.getMethod ("getTopic", new Class[]{}), String.class);
		} catch (Exception ex) {
			throw new RuntimeException ("Error searching archived alerts.", ex);
		}
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.AlertJPAService#searchCurrentByAlertLevel()
	 */
	//abey ...@Transactional (readOnly = true)
	public Map<String, List<AlertSearchTO>> searchCurrentByAlertLevel () {
		try {
			Sorter<String, AlertSearchTO> sorter = new Sorter<String, AlertSearchTO> ();
			return sorter.groupBy (searchCurrent (securityService.isUserAdmin ()
					? "Alert.FindPublishedByLevel" : "Alert.FindByRecLevel"),
					AlertPublishedTO.class.getMethod ("getLevelOfAlert", new Class[]{}), String.class);
		} catch (Exception ex) {
			throw new RuntimeException ("Error searching archived alerts.", ex);
		}
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.AlertJPAService#searchCurrentByTopic()
	 */
	//abey ...@Transactional (readOnly = true)
	public Map<String, List<AlertSearchTO>> searchCurrentByTopic () {
		try {
			Sorter<String, AlertSearchTO> sorter = new Sorter<String, AlertSearchTO> ();
			return sorter.groupBy (searchCurrent (securityService.isUserAdmin ()
					? "Alert.FindPublishedByTopic" : "Alert.FindByRecTopic"),
					AlertPublishedTO.class.getMethod ("getTopic", new Class[]{}), String.class);
		} catch (Exception ex) {
			throw new RuntimeException ("Error searching archived alerts.", ex);
		}
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.AlertJPAService#searchScheduled()
	 */
	public Map<Date, List<AlertSearchTO>> searchScheduled () {
		final List<AlertSearchTO> scheduledAlerts = new ArrayList<AlertSearchTO> ();
		final Query q = entityManager.createNamedQuery ("Alert.FindScheduled");
		final List<?> foundAlerts = q.getResultList ();
		AlertModel foundAlert;
		AlertScheduledTO scheduled;
		for (Object o : foundAlerts) {
			foundAlert = (AlertModel) o;
			scheduled = new AlertScheduledTO ();
			scheduled.setAlertId (foundAlert.getId ());
			scheduled.setAuthor (foundAlert.getAuthor ().getContact ().getFirstName () + " "
					+ foundAlert.getAuthor ().getContact ().getLastName ());
			scheduled.setLevelOfAlert (foundAlert.getPriority ());
			scheduled.setTitle (foundAlert.getName ());
			scheduled.setTopic (foundAlert.getTopic ().getTitle ());
			scheduled.setScheduledDate (foundAlert.getDateScheduled ());
			scheduled.setCreationDate (foundAlert.getDateCreated ());
			scheduledAlerts.add (scheduled);
		}
		final Sorter<Date, AlertSearchTO> sorter = new Sorter<Date, AlertSearchTO> ();
		try {
			return sorter.groupBy (scheduledAlerts, AlertScheduledTO.class.getMethod (
					"getScheduledDate", new Class[]{}), Date.class);
		} catch (Exception ex) {
			throw new RuntimeException ("Error searching archived alerts.", ex);
		}
	}

	/**
	 * Search for published alerts using the provided query name
	 * @param namedQueryName String with the name of the named query
	 * @return List with AlertPublishTO with the found records
	 */
	private List<AlertSearchTO> searchCurrent (final String namedQueryName) {
		final List<AlertSearchTO> results = new ArrayList<AlertSearchTO> ();
		final Query q = entityManager.createNamedQuery (namedQueryName);
		if (!securityService.isUserAdmin ()) {
			q.setParameter ("userId", userDataService.getCurrentUserData ().getUserId ());
		}
		final List<?> alerts = q.getResultList ();
		AlertModel alert;
		AlertPublishedTO current;
		for (Object o : alerts) {
			alert = (AlertModel) o;
			current = new AlertPublishedTO ();
			current.setAlertId (alert.getId ());
			current.setLevelOfAlert (alert.getPriority ().substring (
					alert.getPriority ().lastIndexOf ('/') + 1));
			current.setPublicationDate (alert.getDatePublished ());
			current.setTitle (alert.getName ());
			current.setTopic (alert.getTopic ().getTitle ());
			current.setCreationDate (alert.getDateCreated ());
			current.setLastModifiedDate (alert.getDateModified ());
			current.setArchived (false);
			current.setFinished (true);
			for (AlertReportModel report : alert.getAlertReports ()) {
				current.setFinished (current.isFinished () && report.isNotificationFinished ());
			}
			results.add (current);
		}
		return results;
	}

	/**
	 * Search for archived alerts using the provided named query name
	 * @param namedQueryName String with the named query name
	 * @return List with AlertPublishTO objects for each alert found
	 */
	private List<AlertSearchTO> searchArchived (final String namedQueryName) {
		final List<AlertSearchTO> results = new ArrayList<AlertSearchTO> ();
		final Query q = entityManager.createNamedQuery (namedQueryName);
		if (!securityService.isUserAdmin ()) {
			q.setParameter ("userId", userDataService.getCurrentUserData ().getUserId ());
		}
		final List<?> alerts = q.getResultList ();
		ArchivedAlert archivedAlert;
		AlertPublishedTO archived;
		for (Object o : alerts) {
			archivedAlert = (ArchivedAlert) o;
			archived = new AlertPublishedTO ();
			archived.setAlertId (archivedAlert.getId ());
			archived.setLevelOfAlert (archivedAlert.getPriority ().substring (
					archivedAlert.getPriority ().lastIndexOf ('/') + 1));
			archived.setPublicationDate (archivedAlert.getDatePublished ());
			archived.setTitle (archivedAlert.getName ());
			archived.setTopic (archivedAlert.getTopic ().getTitle ());
			archived.setCreationDate (archivedAlert.getDateCreated ());
			archived.setLastModifiedDate (archivedAlert.getDateModified ());
			archived.setArchived (true);
			archived.setFinished (true);
			results.add (archived);
		}
		return results;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.AlertJPAService#publishAlert(org.cdph.han.alerts.dto.AlertTO)
	 */
	//abey ...@Transactional (propagation = Propagation.REQUIRED, rollbackFor = {PublishAlertException.class})
	public int publishAlert (final AlertTO alert) throws PublishAlertException {
		try {
			alert.setPublishedByUserId (Long.parseLong (FacesContext.getCurrentInstance ().getExternalContext (
					).getUserPrincipal ().getName ()));
			final int notificationReportId = notificationService.sendAlert (alert);
			alert.setAlertId (saveAlert (alert));
			Alert alertDB = entityManager.find (Alert.class, alert.getAlertId ());
			alertDB.setPublisher (alertDB.getModifier ());
			alertDB.setDatePublished (new Date ());
			AlertReport alertReport = new AlertReport ();
			alertReport.setAlert (alertDB);
			alertReport.setNotificationReportId (notificationReportId);
			alertReport.setTotalRecipients (alert.getRecipients ().size ());
			log.info ("Total Recipients: " + alert.getRecipients ().size ());
			entityManager.persist (alertReport);
			/*try {
				final long auditTrailId = CounterLocalServiceUtil.increment (HanAuditTrail.class.getName ());
				HanAuditTrail auditTrail = HanAuditTrailLocalServiceUtil.createHanAuditTrail (
						auditTrailId);
				auditTrail.setActionPerformed (AuditTrailAction.ALERT_PUBLISHED.toString ());
				auditTrail.setAlertId (alertDB.getId ());
				auditTrail.setDate (alertDB.getDateModified ());
				auditTrail.setUserId (alertDB.getModifier ().getUserId ());
				auditTrail.setAuditTrailId (auditTrailId);
				HanAuditTrailLocalServiceUtil.addHanAuditTrail (auditTrail);
			} catch (SystemException ex) {
				log.error ("Error creating audit trail.", ex);
				throw new RuntimeException ("Error creating audit trail.", ex);
			}*/
			entityManager.flush ();
			return notificationReportId;
		} catch (Exception ex) {
			throw new PublishAlertException ("Error publishing alert.", ex);
		}
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.AlertJPAService#publishAlert(long)
	 */
	//abey ...@Transactional (propagation = Propagation.REQUIRED, rollbackFor = {PublishAlertException.class})
	public int publishAlert (final long alertId) throws PublishAlertException {
		final Alert alert = entityManager.find (Alert.class, alertId);
		alert.setDatePublished (new Date ());
		alert.setPublisher (alert.getModifier ());
		try {
			final AlertTO alertTO = retrieveAlert (alertId, false);
			alertTO.setPublishedByUserId (alert.getPublisher ().getUserId ());
			final int notificationReportId = notificationService.sendAlert (alertTO);
			AlertReport report = new AlertReport ();
			report.setAlert (alert);
			report.setNotificationReportId (notificationReportId);
			report.setTotalRecipients (alertTO.getRecipients ().size ());
			entityManager.merge (alert);
			entityManager.persist (report);
			/*try {
				final long auditTrailId = CounterLocalServiceUtil.increment (HanAuditTrail.class.getName ());
				HanAuditTrail auditTrail = HanAuditTrailLocalServiceUtil.createHanAuditTrail (
						auditTrailId);
				auditTrail.setActionPerformed (AuditTrailAction.ALERT_PUBLISHED.toString ());
				auditTrail.setAlertId (alert.getId ());
				auditTrail.setDate (alert.getDateModified ());
				auditTrail.setUserId (alert.getModifier ().getUserId ());
				auditTrail.setAuditTrailId (auditTrailId);
				HanAuditTrailLocalServiceUtil.addHanAuditTrail (auditTrail);
			} catch (SystemException ex) {
				log.error ("Error creating audit trail.", ex);
				throw new RuntimeException ("Error creating audit trail.", ex);
			}*/
			entityManager.flush ();
			return notificationReportId;
		} catch (Exception ex) {
			throw new PublishAlertException ("Error publishing alert.", ex);
		}
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.AlertJPAService#loadAlert(long)
	 */
	//abey ...@Transactional (readOnly = true)
	public AlertTO loadAlert (final long alertId, boolean archived) {
		final AlertModel alert = entityManager.find (archived ? ArchivedAlert.class : Alert.class, alertId);
		AlertTO alertTO = null;
		if (alert != null) {
			alertTO = new AlertTO ();
			AlertMessageModel message = null;
			if (alert.getMessages () != null && alert.getMessages ().size () > 0) {
				message = alert.getMessages ().iterator ().next ();
			}
			alertTO.setAbstractText (message != null ? message.getAbstractText () : null);
			alertTO.setAlertId (alertId);
			alertTO.setAlertName (alert.getName ());
			alertTO.setAuthorUserId (alert.getAuthor ().getUserId ());
			alertTO.setConfirmResponse (alert.isConfirmResponse ());
			alertTO.setContactCycleDelay (convertToHoursMinutes (alert.getContactCycleDelay ()));
			alertTO.setContactCycles (alert.getContactAttempCycles ());
			alertTO.setContactUserId (alert.getContact ().getUserId ());
			alertTO.setCreatedDate (alert.getDateCreated ());
			alertTO.setDuration (convertToHoursMinutes (alert.getDuration ()));
			alertTO.setEntireMachine (alert.isEntireMachine ());
			alertTO.setExpeditedDelivery (alert.isExpeditedDelivery ());
			alertTO.setExpirationDate (alert.getDateExpired ());
			alertTO.setFullDetailText (message != null ? message.getPortalMessage () : null);
			alertTO.setTelephonyMessage (message != null ? message.getTelephonyMessage () : null);
			alertTO.setIncidentDate (alert.getIncidentDate ());
			alertTO.setLastModifiedDate (alert.getDateModified ());
			alertTO.setLeaveMessage (alert.getLeaveMessage ());
			alertTO.setLocationOverride (alert.isLocationOverride ());
			alertTO.setLocationOverridePriorities (readLocations (alert.getLocations ()));
			alertTO.setNotificationDate (alert.getDateScheduled ());
			alertTO.setNotificationMethod (alert.getNotificationMethod ());
			alertTO.setOneTextContact (alert.isOneTextContact ());
			alertTO.setOverrideDefaultStatus (alert.isOverrideDefaultsOnly ());
			alertTO.setPartialMachine (alert.isPartialMachine ());
			alertTO.setPlayGreeting (alert.isPlayGreeting ());
			alertTO.setPriority (alert.getPriority ().substring (alert.getPriority ().lastIndexOf ('/') + 1));
			alertTO.setPublishedByUserId (alert.getPublisher () != null
					? alert.getPublisher ().getUserId () : null);
			alertTO.setPublishedDate (alert.getDatePublished ());
			readRecipients (alertTO, alert.getRecipients ());
			alertTO.setReplayMessages (alert.isReplayMessages ());
			alertTO.setReportRecipients (alert.isReportRecipients ());
			alertTO.setRequiresPIN (alert.isRequiresPIN ());
			readResponseOptions (alertTO, alert.getOptions ());
			// Since is for details and not editing, check if the alert is scheduled or still a draft.
			if (!archived) {
				alertTO.setScheduled (((Alert) alert).isReadyToPublish ());
			}
			alertTO.setScheduledDate (alert.getDateScheduled ());
			alertTO.setSelectLanguage (alert.isSelectLanguage ());
			alertTO.setStopAfterEntireMessage (alert.isEntireMessage ());
			alertTO.setStopAnswerPhone (alert.isAnswerPhone ());
			alertTO.setTextDelay (convertToHoursMinutes (alert.getTextDeviceDelay ()));
			alertTO.setTopicId (alert.getTopic () != null ? alert.getTopic ().getId () : -1);
			alertTO.setTopic (alert.getTopic () != null ? alert.getTopic ().getTitle () : "");
			alertTO.setUseAlternates (alert.isUseAlternates ());
			alertTO.setUseTopics (alert.isUseTopics ());
			alertTO.setValidateRecipients (alert.isValidateRecipients ());
			alertTO.setVoiceDelivery (alert.getVoiceDelivery ());
			alertTO.setAuthorName (alert.getContact ().getContact ().getFirstName () + " "
					+ alert.getContact ().getContact ().getLastName ());
			alertTO.setNotificationDate (alert.getDatePublished ());
			readAttachmentTitles (alertTO, alert.getAttachments ());
			alertTO.setArchived (archived);
		}
		return alertTO;
	}

	/**
	 * Read the attachments of the alert putting only the filename and type into the transfer object
	 * @param alertTO AlertTO object to set the attachments
	 * @param attachments Collection of AlertAttachment objects
	 */
	private void readAttachmentTitles (final AlertTO alertTO,
			final Collection<? extends AlertAttachmentModel> attachments) {
		alertTO.setAttachments (new ArrayList<FileTO> ());
		if (attachments != null && !attachments.isEmpty ()) {
			FileTO currentAttachment;
			for (AlertAttachmentModel attachment : attachments) {
				currentAttachment = new FileTO ();
				currentAttachment.setFileInfo (new FileInfo ());
				currentAttachment.getFileInfo ().setContentType (attachment.getMimeType ());
				currentAttachment.getFileInfo ().setFileName (attachment.getName ());
				currentAttachment.getFileInfo ().setSize (attachment.getAttachment ().length);
				currentAttachment.setId (attachment.getId ());
				alertTO.getAttachments ().add (currentAttachment);
			}
		}
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.AlertJPAService#readAttachment(long, long)
	 */
	//abey ...@Transactional (readOnly = true)
	public AttachmentTO readAttachment (final long attachmentId, final long userId, final boolean archived) {
		AttachmentTO attachmentTO = null;
		boolean authorized = securityService.isUserAdmin ();
		final AlertAttachmentModel attachment = entityManager.find (
				archived ? ArchivedAlertAttachment.class : AlertAttachment.class, attachmentId);
		if (!authorized && !archived) {
			Query q = entityManager.createNamedQuery ("Alert.FindByAlertRecipient");
			q.setParameter ("alertId",((AlertAttachment) attachment).getAlert ().getId ());
			q.setParameter ("userId", userId);
			authorized = !q.getResultList ().isEmpty ();
		}
		if (authorized) {
			attachmentTO = new AttachmentTO (attachment.getAttachment (),
					attachment.getMimeType (), attachment.getName ());
		}
		return attachmentTO;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.AlertJPAService#searchAlertsForTracking()
	 */
	//abey ...@Transactional (readOnly = true)
	public Map<String, List<AlertSearchTO>> searchAlertsForTracking () {
		final Map<String, List<AlertSearchTO>> foundAlerts = new LinkedHashMap <String, List<AlertSearchTO>> ();
		foundAlerts.put ("Current", searchCurrentByDate ());
		foundAlerts.put ("Archived", searchArchivedByPublishDate ());
		return foundAlerts;
	}

}
