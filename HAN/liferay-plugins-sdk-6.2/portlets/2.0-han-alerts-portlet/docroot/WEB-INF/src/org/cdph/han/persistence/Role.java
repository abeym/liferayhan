package org.cdph.han.persistence;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 * Entity representing a role in the HAN
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Entity (name = "Role")
@Table (name = "Role_", schema = "lportal")
public class Role implements Serializable {
	/** Class serial version UID */
	private static final long serialVersionUID = -2475779329986045228L;
	/** Id of the role */
	@Id
	@Column (name = "roleId", nullable = false)
	private long roleId;
	/** Name of the role */
	@Column (name = "name")
	private String name;
	/** Organizations associated to the role */
	@ManyToMany
	@JoinTable (name = "TypeOrganizationRoles", schema = "lportal",
			joinColumns = {@JoinColumn (name = "roleId", referencedColumnName = "roleId")},
			inverseJoinColumns = {@JoinColumn (name = "organizationTypeId", referencedColumnName = "organizationId")})
	private Collection<Organization> organizations;
	/** Users in role */
	@ManyToMany
	@JoinTable (name = "Users_Roles", schema = "lportal",
			joinColumns = @JoinColumn (name = "roleId", referencedColumnName = "roleId"),
			inverseJoinColumns = @JoinColumn (name = "userId", referencedColumnName = "userId"))
	private Collection<UserData> users;
	/** Role type */
	@Column (name = "type_")
	private int type;

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean equal = false;
		if (obj != null && obj instanceof Role) {
			final Role other = (Role) obj;
			equal = other.getRoleId () == roleId;
		}
		return equal;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		return Long.valueOf (roleId).hashCode ();
	}

	/**
	 * @return the roleId
	 */
	public long getRoleId () {
		return roleId;
	}

	/**
	 * @return the name
	 */
	public String getName () {
		return name;
	}

	/**
	 * @return the organizations
	 */
	public Collection<Organization> getOrganizations () {
		return organizations;
	}

	/**
	 * @return the users
	 */
	public Collection<UserData> getUsers () {
		return users;
	}

	/**
	 * @param roleId the roleId to set
	 */
	public void setRoleId (long roleId) {
		this.roleId = roleId;
	}

	/**
	 * @param name the name to set
	 */
	public void setName (String name) {
		this.name = name;
	}

	/**
	 * @param organizations the organizations to set
	 */
	public void setOrganizations (Collection<Organization> organizations) {
		this.organizations = organizations;
	}

	/**
	 * @param users the users to set
	 */
	public void setUsers (Collection<UserData> users) {
		this.users = users;
	}

	/**
	 * @return the type
	 */
	public int getType () {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType (int type) {
		this.type = type;
	}

}
