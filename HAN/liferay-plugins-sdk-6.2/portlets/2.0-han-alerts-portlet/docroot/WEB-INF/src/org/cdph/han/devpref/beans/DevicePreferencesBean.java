package org.cdph.han.devpref.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.alerts.services.MessagesService;
import org.cdph.han.alerts.services.UserDataService;
import org.cdph.han.custgroups.dto.UserDisplayTO;
import org.cdph.han.devpref.services.ContactDevicesService;
import org.cdph.han.persistence.UserData;
import org.cdph.han.util.services.SecurityService;

/**
 * Backed bean used to manage the preferences of the device for contact in the HAN.
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class DevicePreferencesBean implements Serializable {
	/** Class serial version UID */
	private static final long serialVersionUID = -3050918157870495282L;
	/** Class logger */
	private static final Log log = LogFactory.getLog (DevicePreferencesBean.class);
	/** Array containing the list of preferences, ranking from first (position 0) to last */
	private String[] preferences = new String[6];
	/** List of alternate contacts */
	private List<UserDisplayTO> alternateContacts;
	/** Service to get access to the available contact devices */
	private ContactDevicesService contactDevicesService;
	/** Available contact devices */
	private List<SelectItem> contactDevices;
	/** Service for retrieving error messages */
	private MessagesService messagesService;
	/** Service to find user data */
	private UserDataService userDataService;
	/** Contact first name */
	private String firstName;
	/** Contact last name */
	private String lastName;
	/** List of recipients */
	private List<UserDisplayTO> foundUsers;
	/** The telephony ID */
	private String telephonyId;
	/** The telephony PIN */
	private String telephonyPIN;
	/** The telephony Password */
	private String telephonyPass;
	/** Security service to validate user roles */
	private SecurityService securityService;

	/**
	 * This method initializes the bean with the current user information stored in the DB
	 */
	public void init () {
		try {
			preferences = contactDevicesService.loadPreferences ();
			alternateContacts = contactDevicesService.loadAlternateContacts ();
			contactDevices = contactDevicesService.loadContactDevices ();
			foundUsers = new ArrayList<UserDisplayTO> ();
			UserData user = userDataService.getCurrentUserData ();
			telephonyId = user.getTelephonyId ();
			telephonyPass = user.getTelephonyPassword ();
			telephonyPIN = user.getTelephonyPIN ();
		} catch (Exception ex) {
			log.error ("Error retrieving preferences.", ex);
		}
	}

	/**
	 * Saves the contact preferences in the database
	 * @return String with the outcome of the operation
	 */
	public String savePreferences () {
		String outcome = "success";
		if (validate ()) {
			removeEmptySlots ();
			try {
				contactDevicesService.savePreferences (preferences, alternateContacts,
						telephonyId, telephonyPIN, telephonyPass);
				FacesMessage message = new FacesMessage (FacesMessage.SEVERITY_INFO,
						"Urgent/Emergency Contact Information updated successfully.",
						"Urgent/Emergency Contact Information updated successfully.");
				FacesContext.getCurrentInstance ().addMessage (null, message);
				log.info ("Second device: " + preferences[1]);
			} catch (Exception ex) {
				log.error ("Error updating user contact preferences.", ex);
				outcome = "failure";
				FacesMessage message = new FacesMessage (FacesMessage.SEVERITY_ERROR,
						"Urgent/Emergency Contact Information cannot be updated.",
						"Urgent/Emergency Contact Information cannot be updated.");
				FacesContext.getCurrentInstance ().addMessage (null, message);
			}
		} else {
			outcome = "failure";
		}
		return outcome;
	}

	/**
	 * Reloads the preferences stored in the DataBase erasing the user modifications on screen
	 * @return outcome of the process
	 */
	public String reloadPreferences () {
		String outcome = "success";
		try {
			preferences = contactDevicesService.loadPreferences ();
			alternateContacts = contactDevicesService.loadAlternateContacts ();
			contactDevices = contactDevicesService.loadContactDevices ();
			UserData user = userDataService.getCurrentUserData ();
			telephonyPass = user.getTelephonyPassword ();
			telephonyPIN = user.getTelephonyPIN ();
		} catch (Exception ex) {
			log.error ("Error retrieving preferences.", ex);
			outcome = "failure";
			FacesMessage message = new FacesMessage (FacesMessage.SEVERITY_ERROR,
					"Error reloading Urgent/Emergency Contact Information.",
					"Error reloading Urgent/Emergency Contact Information.");
			FacesContext.getCurrentInstance ().addMessage (null, message);
		}
		return outcome;
	}

	/**
	 * Adds a new alternate contact to the users alternate contacts list
	 * @return outcome
	 */
	public String addAlternateContacts () {
		String outcome = "success";
		if (alternateContacts.size () < 3) {
			for (UserDisplayTO contact : foundUsers) {
				if (contact.isSelected () && !alternateContacts.contains (contact)
						&& alternateContacts.size () < 3) {
					alternateContacts.add (contact);
				}
			}
		} else {
			FacesMessage message = new FacesMessage (FacesMessage.SEVERITY_ERROR,
					"You can only add a maximum of 3 alternate contacts.",
					"You can only add a maximum of 3 alternate contacts.");
			FacesContext.getCurrentInstance ().addMessage (null, message);
		}
		return outcome;
	}

	/**
	 * Removes an alternate contact from the list
	 * @param event ActionEvent triggered by the user
	 */
	public void removeAlternateContact (final ActionEvent event) {
		final String alternateContactIdString = (String) FacesContext.getCurrentInstance ().getExternalContext (
				).getRequestParameterMap ().get ("alternateContactId");
		long alternateContactId = Long.parseLong (alternateContactIdString);
		for (UserDisplayTO contact : alternateContacts) {
			if (contact.getId () == alternateContactId) {
				alternateContacts.remove (contact);
				break;
			}
		}
	}

	/**
	 * Remove the empty slots in the array.
	 */
	private void removeEmptySlots () {
		int index = 0;
		String[] temp = new String[6];
		for (int i = 0; i < preferences.length; i++) {
			if (preferences[i] != null && !preferences[i].equals ("")) {
				temp[index++] = preferences[i];
			}
		}
		preferences = temp;
	}

	/**
	 * Validates that the current preferences doesn't contain duplicated values
	 * @return false in case the validation fails
	 */
	private boolean validate () {
		boolean valid = true;
		Set<String> currentValues = new HashSet<String> ();
		for (int i = 0; i < preferences.length && valid; i++) {
			if (preferences[i] != null && !preferences[i].equals ("")) {
				if (currentValues.contains (preferences[i])) {
					valid = false;
				} else {
					currentValues.add (preferences[i]);
				}
			}
		}
		if (!valid) {
			FacesMessage message = new FacesMessage (FacesMessage.SEVERITY_ERROR,
					"You may select a device just once.", "You may select a device just once.");
			FacesContext.getCurrentInstance ().addMessage (null, message);
		}
		if (currentValues.isEmpty ()) {
			FacesMessage message = new FacesMessage (FacesMessage.SEVERITY_ERROR,
					"Please provide at least 1 Contact Preference.", "Please provide at least 1 Contact Preference.");
			FacesContext.getCurrentInstance ().addMessage (null, message);
			valid = false;
		}
		// Added to validate password
		if (securityService.isUserAdmin ()) {
			if (telephonyPass.length () < 8) {
				FacesMessage message = new FacesMessage (FacesMessage.SEVERITY_ERROR,
						"The password must be at least 8 characters in length.",
						"The password must be at least 8 characters in length.");
				FacesContext.getCurrentInstance ().addMessage (null, message);
				valid = false;
			}
			final Pattern lowerCase = Pattern.compile ("[a-z]");
			final Pattern upperCase = Pattern.compile ("[A-Z]");
			final Pattern numbers = Pattern.compile ("[0-9]");
			final Pattern specialChars = Pattern.compile ("[`!@#\\$%\\^&\\*\\(\\)_\\+\\-=\\{\\}\\|\\\\:\";'<>\\?,\\./]");
			if (!lowerCase.matcher (telephonyPass.subSequence (0, telephonyPass.length ())).find ()) {
				FacesMessage message = new FacesMessage (FacesMessage.SEVERITY_ERROR,
						"The password must contain at least one lower case character.",
						"The password must contain at least one lower case character.");
				FacesContext.getCurrentInstance ().addMessage (null, message);
				valid = false;
			}
			if (!upperCase.matcher (telephonyPass.subSequence (0, telephonyPass.length ())).find ()) {
				FacesMessage message = new FacesMessage (FacesMessage.SEVERITY_ERROR,
						"The password must contain at least one upper case character.",
						"The password must contain at least one upper case character.");
				FacesContext.getCurrentInstance ().addMessage (null, message);
				valid = false;
			}
			if (!numbers.matcher (telephonyPass.subSequence (0, telephonyPass.length ())).find ()) {
				FacesMessage message = new FacesMessage (FacesMessage.SEVERITY_ERROR,
						"The password must contain at least one number.",
						"The password must contain at least one number.");
				FacesContext.getCurrentInstance ().addMessage (null, message);
				valid = false;
			}
			if (!specialChars.matcher (telephonyPass.subSequence (0, telephonyPass.length ())).find ()) {
				FacesMessage message = new FacesMessage (FacesMessage.SEVERITY_ERROR,
						"The password must contain at least one of the following characters: " +
						"` ! @ # $ % ^ & * ( ) _ + - = { } | \\ : \" ; ' < > ? , . /",
						"The password must contain at least one of the following characters: " +
						"` ! @ # $ % ^ & * ( ) _ + - = { } | \\ : \" ; ' < > ? , . /");
				FacesContext.getCurrentInstance ().addMessage (null, message);
				valid = false;
			}
		}
		// PIN Validation
		if (telephonyPIN.length () < 4) {
			FacesMessage message = new FacesMessage (FacesMessage.SEVERITY_ERROR,
					"The PIN must be at least 4 characters in length.",
					"The PIN must be at least 4 characters in length.");
			FacesContext.getCurrentInstance ().addMessage (null, message);
			valid = false;
		}
		final Pattern pin = Pattern.compile ("[^0-9]");
		if (pin.matcher (telephonyPIN.subSequence (0, telephonyPIN.length ())).find ()) {
			FacesMessage message = new FacesMessage (FacesMessage.SEVERITY_ERROR,
					"The PIN must contain only numeric characters.",
					"The PIN must contain only numeric characters.");
			FacesContext.getCurrentInstance ().addMessage (null, message);
			valid = false;
		}
		return valid;
	}

	/**
	 * Listener used to validate that the user doesn't leave a device slot without selection
	 * @param event with the new value and source
	 */
	public void preferenceChanged (ValueChangeEvent event) {
		log.info ("TYPE!!!! : " + event.getNewValue ().getClass ().getName ());
		log.info (event.getComponent ().getId ());
		final String id = event.getComponent ().getId ();
		final int position = Integer.parseInt (id.substring (4));
		final String newValue = (String) event.getNewValue ();
		final String oldValue = preferences[position];
		boolean equal = false;
		equal = newValue == null && oldValue == null;
		if (newValue != null && !equal) {
			equal = newValue.equals (oldValue);
		}
		log.info ("OLD VALUE!!!!: " + oldValue);
		log.info ("NEW VALUE!!!!: " + newValue);
		log.info ("Equal: " + equal);
		if (!equal) {
			preferences[position] = newValue;
			removeEmptySlots ();
		}
	}

	/**
	 * Action listener used to search for users using either first name, last name and first and last name
	 * @param event ActionEvent generated by the user request
	 */
	public void searchUsers (final ActionEvent event) {
		try {
			foundUsers = userDataService.findUsersByFirstAndLastName (firstName, lastName);
		} catch (Exception ex) {
			log.error ("Error performing contact search.", ex);
			FacesContext.getCurrentInstance ().addMessage (null,
					messagesService.getMessage ("alerts.search.error",
							"Error performing search, try again later.",
							FacesMessage.SEVERITY_ERROR));
		}
	}

	/**
	 * @return the preferences
	 */
	public String[] getPreferences () {
		for (String preference : preferences) {
			log.info ("Preference: " + preference);
		}
		return preferences;
	}

	/**
	 * @param preferences the preferences to set
	 */
	public void setPreferences (final String[] preferences) {
		this.preferences = preferences;
	}

	/**
	 * @return the alternateContact
	 */
	public List<UserDisplayTO> getAlternateContacts () {
		return alternateContacts;
	}

	/**
	 * @param alternateContact the alternateContact to set
	 */
	public void setAlternateContacts (List<UserDisplayTO> alternateContacts) {
		this.alternateContacts = alternateContacts;
	}

	/**
	 * @param contactDevicesService the contactDevicesService to set
	 */
	public void setContactDevicesService (
			ContactDevicesService contactDevicesService) {
		this.contactDevicesService = contactDevicesService;
		if (userDataService != null) {
			init ();
		}
	}

	/**
	 * @return the contactDevices
	 */
	public List<SelectItem> getContactDevices () {
		return contactDevices;
	}

	/**
	 * @param contactDevices the contactDevices to set
	 */
	public void setContactDevices (List<SelectItem> contactDevices) {
		this.contactDevices = contactDevices;
	}

	/**
	 * @param messagesService the messagesService to set
	 */
	public void setMessagesService (MessagesService messagesService) {
		this.messagesService = messagesService;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName () {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName (String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName () {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName (String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @param userDataService the userDataService to set
	 */
	public void setUserDataService (UserDataService userDataService) {
		this.userDataService = userDataService;
		if (contactDevicesService != null) {
			init ();
		}
	}

	/**
	 * @return the foundUsers
	 */
	public List<UserDisplayTO> getFoundUsers () {
		return foundUsers;
	}

	/**
	 * @param foundUsers the foundUsers to set
	 */
	public void setFoundUsers (List<UserDisplayTO> foundUsers) {
		this.foundUsers = foundUsers;
	}

	/**
	 * @return the telephonyId
	 */
	public String getTelephonyId () {
		String formatted;
		if (telephonyId != null && telephonyId.length () > 7) {
			formatted = telephonyId.substring (0, 3) + '-' + telephonyId.substring (3, 6)
					+ '-' + telephonyId.substring (6);
		} else {
			formatted = telephonyId;
		}
		return formatted;
	}

	/**
	 * @param telephonyId the telephonyId to set
	 */
	public void setTelephonyId (String telephonyId) {
		this.telephonyId = telephonyId;
	}

	/**
	 * @return the telephonyPIN
	 */
	public String getTelephonyPIN () {
		return telephonyPIN;
	}

	/**
	 * @param telephonyPIN the telephonyPIN to set
	 */
	public void setTelephonyPIN (String telephonyPIN) {
		this.telephonyPIN = telephonyPIN;
	}

	/**
	 * @return the telephonyPass
	 */
	public String getTelephonyPass () {
		return telephonyPass;
	}

	/**
	 * @param telephonyPass the telephonyPass to set
	 */
	public void setTelephonyPass (String telephonyPass) {
		this.telephonyPass = telephonyPass;
	}

	/**
	 * @return the securityService
	 */
	public SecurityService getSecurityService () {
		return securityService;
	}

	/**
	 * @param securityService the securityService to set
	 */
	public void setSecurityService (SecurityService securityService) {
		this.securityService = securityService;
	}

}
