package org.cdph.han.alerts.services;

import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.alerts.dto.AlertTO;
import org.cdph.han.alerts.dto.OptionTO;
import org.cdph.han.alerts.exceptions.PublishAlertException;
import org.cdph.han.custgroups.dto.FilterCriteriaTO;
import org.cdph.han.custgroups.dto.KeyDescriptionTO;
import org.cdph.han.custgroups.dto.RecipientTO;
import org.cdph.han.custgroups.dto.RecipientType;
import org.cdph.han.custgroups.services.OrganizationalSearchService;
import org.cdph.han.persistence.AreaField;
import org.cdph.han.persistence.ContactDeviceType;
import org.cdph.han.persistence.CustomGroupCriteria;
import org.cdph.han.persistence.CustomGroupMember;
import org.cdph.han.persistence.NonHanUser;
import org.cdph.han.persistence.NonHanUserContactDevice;
import org.cdph.han.persistence.NonHanUserGroup;
import org.cdph.han.persistence.Organization;
import org.cdph.han.persistence.Role;
import org.cdph.han.persistence.UserCustomGroup;
import org.cdph.han.persistence.UserData;
import org.cdph.han.persistence.UserOrganizationalData;
import org.cdph.han.util.dto.ListTypeTO;
import org.cdph.han.util.services.HanPropertiesService;
import org.cdph.han.util.services.ListTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mir3.ws.AttachmentSummariesType;
import com.mir3.ws.AttachmentSummaryType;
import com.mir3.ws.AttachmentType;
import com.mir3.ws.AuthorizationType;
import com.mir3.ws.BroadcastInfoType;
import com.mir3.ws.CalloutInfoType;
import com.mir3.ws.Device;
import com.mir3.ws.DevicesType;
import com.mir3.ws.ErrorType;
import com.mir3.ws.FirstResponseInfoType;
import com.mir3.ws.LeaveMessageType;
import com.mir3.ws.LocationOverrideDevicesType;
import com.mir3.ws.LocationOverrideType;
import com.mir3.ws.Mir3;
import com.mir3.ws.Mir3Service;
import com.mir3.ws.NotificationDetailType;
import com.mir3.ws.NotificationRecipientsDetailsType;
import com.mir3.ws.NotificationResponseOptionsType;
import com.mir3.ws.OneStepInfoType;
import com.mir3.ws.OneStepNotificationType;
import com.mir3.ws.OverrideType;
import com.mir3.ws.RecipientDetailType;
import com.mir3.ws.ResponseOptionType;
import com.mir3.ws.ResponseType;
import com.mir3.ws.SetEmailAttachmentType;
import com.mir3.ws.StringWithLocale;
import com.mir3.ws.VerbiageType;

/**
 * Implementation of the notification service
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Service ("notificationService")
public class NotificationServiceImpl implements NotificationService {
	/** Service logger */
	private static final Log log = LogFactory.getLog (NotificationServiceImpl.class);
	/** MIR3 API Version key */
	private static final String API_VERSION_KEY = "MIR3_API_VERSION";
	/** Persistence context */
	private EntityManager entityManager;
	/** Service for retrieving current user data */
	//abey ...
	//@Autowired
	private UserDataService userDataService;
	/** Service to retrieve properties from the database */
	//abey...
	//@Autowired
	private HanPropertiesService hanPropertiesService;
	/** Service for retrieving the device types */
	//abey...
	//@Autowired
	private ListTypeService listTypeService;
	/** Service to perform searches based on filters */
	@Autowired
	private OrganizationalSearchService organizationalSearchService;

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.NotificationService#sendAlert(org.cdph.han.alerts.dto.AlertTO)
	 */
	public int sendAlert (final AlertTO alert) throws PublishAlertException {
		final Mir3Service mir3Service = new Mir3Service ();
		final Mir3 service = mir3Service.getMir3 ();
		final ResponseType response = service.oneStepNotificationOp (createOneStepNotification (alert));
		int notificationReportId;
		if (response.getError () != null && response.getError ().isEmpty ()) {
			notificationReportId = response.getOneStepNotificationResponse ().getNotificationReportId ();
		} else {
			for (ErrorType error : response.getError ()) {
				log.error ("Error: " + error.getErrorMessage () + " Error code: " + error.getErrorCode ());
			}
			throw new PublishAlertException ("Error publishing alert at mir3.");
		}
		if (alert.getNotificationAttachment () != -1 && alert.getAttachments () != null
				&& !alert.getAttachments ().isEmpty ()) {
			final SetEmailAttachmentType setEmailAttachment = createEmailAttachment (alert);
			final ResponseType attachmentResponse = service.setEmailAttachmentOp (setEmailAttachment);
			if (!attachmentResponse.getError ().isEmpty ()) {
				throw new PublishAlertException (attachmentResponse.getError ().get (0).getErrorMessage ()
						+ " Error Code: " + attachmentResponse.getError ().get (0).getErrorCode ());
			}
		}
		return notificationReportId;
	}

	/**
	 * Creates a SetEmailAttachmentType object with the attachment to add to the notification
	 * @param alert AlertTO with the alert data
	 * @return SetEmailAttachmentType ready to process the request
	 * @throws PublishAlertException if the attachment cannot be read
	 */
	private SetEmailAttachmentType createEmailAttachment (final AlertTO alert) throws PublishAlertException {
		final SetEmailAttachmentType setEmailAttachmentType = new SetEmailAttachmentType ();
		setEmailAttachmentType.setApiVersion (hanPropertiesService.getValue (API_VERSION_KEY));
		setEmailAttachmentType.setAttachment (createAttachmentType (alert));
		setEmailAttachmentType.setAuthorization (retrieveAuthorization (alert.getPublishedByUserId ()));
		setEmailAttachmentType.setLocale ("en_US");
		setEmailAttachmentType.setNotification (alert.getAlertName ());
		return setEmailAttachmentType;
	}

	/**
	 * Creates an object AttachmentType with the attachment data
	 * @param alert AlertTO with the alert data
	 * @return AttachmentType object with the notification attachment
	 * @throws PublishAlertException if the attachment cannot be read
	 */
	private AttachmentType createAttachmentType (final AlertTO alert) throws PublishAlertException {
		ReadableByteChannel channel = null;
		try {
			final AttachmentType attachment = new AttachmentType ();
			final ByteBuffer buffer = ByteBuffer.allocate ((int) alert.getAttachments ().get (
					alert.getNotificationAttachment ()).getFileInfo ().getSize ());
			channel = Channels.newChannel (new FileInputStream (alert.getAttachments ().get (
						alert.getNotificationAttachment ()).getFile ()));
			final int bytesRead = channel.read (buffer);
			final byte[] value = new byte[bytesRead];
			buffer.rewind ();
			buffer.get (value);
			attachment.setContent (value);
			attachment.setFilename (alert.getAttachments ().get (
					alert.getNotificationAttachment ()).getFileInfo ().getFileName ());
			return attachment;
		} catch (Exception ex) {
			log.error ("Error reading attachment.", ex);
			throw new PublishAlertException ("Error reading attachment.", ex);
		} finally {
			if (channel != null) {
				try {
					channel.close ();
				} catch (Exception ex) {
					log.error ("Error closing channel.", ex);
				}
			}
		}
	}

	/**
	 * Creates an object OneStepNotificationType using the data provided by the AlertTO object
	 * @param alert AlertTO object with the notification type
	 * @return OneStepNotificationType object ready to send a new notification
	 */
	private OneStepNotificationType createOneStepNotification (final AlertTO alert) {
		final OneStepNotificationType oneStepNotification = new OneStepNotificationType ();
		oneStepNotification.setAuthorization (retrieveAuthorization (alert.getPublishedByUserId ()));
		oneStepNotification.setApiVersion (hanPropertiesService.getValue (API_VERSION_KEY));
		//oneStepNotification.setDynamicRecipients (createDynamicRecipients (alert));
		oneStepNotification.setNotificationDetail (createNotificationDetail (alert));
		return oneStepNotification;
	}

	/**
	 * Creates the notification detail object
	 * @param alert AlertTO object with the notification information
	 * @return NotificationDetailType with the notification data
	 */
	private NotificationDetailType createNotificationDetail (final AlertTO alert) {
		final NotificationDetailType notificationDetail = new NotificationDetailType ();
		if (alert.getNotificationAttachment () != -1) {
			notificationDetail.setAttachmentSummaries (createAttachments (alert));
		}
		//if (alert.getNotificationMethod () == 1) {
			notificationDetail.setBroadcastInfo (createBroadcastInfo (alert));
		/*} else if (alert.getNotificationMethod () == 2) {
			notificationDetail.setFirstResponseInfo (createFirstResponseInfo (alert));
		} else if (alert.getNotificationMethod () == 3) {
			notificationDetail.setCalloutInfo (createCalloutInfo (alert));
		}*/
		notificationDetail.setCallAnalysis (true);
		notificationDetail.setContactAttemptCycles (alert.getContactCycles ());
		notificationDetail.setContactCycleDelay (convertToMinutes (alert.getContactCycleDelay ()) * 60);
		notificationDetail.setLeaveMessage (createLeaveMessage (alert));
		notificationDetail.setMessage (alert.getTelephonyMessage ());
		notificationDetail.setOneStepInfo (createOneStepInfo (alert));
		notificationDetail.setPlayGreeting (alert.isPlayGreeting ());
		notificationDetail.setRecordedMessageTitle (alert.getAlertName ());
		notificationDetail.setReplayMessage (alert.isReplayMessages ());
		notificationDetail.setRequiresPin (alert.isRequiresPIN ());
		notificationDetail.setResponseOptions (createResponseOptions (alert));
		notificationDetail.setSelectLanguage (alert.isSelectLanguage ());
		notificationDetail.setSendToSubscribers (alert.isSendToSubscribers ());
		notificationDetail.setStopIfFullHuman (alert.isStopAfterEntireMessage ());
		notificationDetail.setStopIfFullMachine (alert.isEntireMachine ());
		notificationDetail.setStopIfPartialHuman (alert.isStopAnswerPhone ());
		notificationDetail.setStopIfPartialMachine (alert.isPartialMachine ());
		notificationDetail.setStrictDeviceDelay (true);
		notificationDetail.setTextDeviceDelay (convertToMinutes (alert.getTextDelay ()) * 60);
		notificationDetail.setTitle (alert.getAlertName ());
		notificationDetail.setUseAlternates (alert.isUseAlternates ());
		notificationDetail.setUseTopics (alert.isUseTopics ());
		notificationDetail.setValidateRecipient (alert.isValidateRecipients ());
		//abey - commenting to copmile
		//notificationDetail.setVerbiage (createVerbiage (alert));
		notificationDetail.setLocationOverride (createLocationOverride (alert));
		notificationDetail.setUseAlias (true);
		notificationDetail.setInitiatorAlias ("Chicago Health Alert Network");
		return notificationDetail;
	}

	/**
	 * Creates the location override with the provided parameters
	 * @param alert information
	 * @return the location override
	 */
	private LocationOverrideType createLocationOverride (final AlertTO alert) {
		LocationOverrideType locationOverride = null;
		if (alert.isLocationOverride ()) {
			// Retrieve the phone types
			final ListTypeTO[] phoneLocations =
				listTypeService.loadListForType ("com.liferay.portal.model.Contact.phone");
			// Retrieve the email types
			final ListTypeTO[] emailLocations =
				listTypeService.loadListForType ("com.liferay.portal.model.Contact.emailAddress");
			final Map<Long, ListTypeTO> locations = new HashMap<Long, ListTypeTO> ();
			for (ListTypeTO current : phoneLocations) {
				locations.put (current.getId (), current);
			}
			for (ListTypeTO current : emailLocations) {
				locations.put (current.getId (), current);
			}
			locationOverride = new LocationOverrideType ();
			locationOverride.setOverrideDefaultStatusOnly (alert.isOverrideDefaultStatus ());
			locationOverride.setDevices (new LocationOverrideDevicesType ());
			OverrideType override;
			int index = 1;
			for (Long id : alert.getLocationOverridePriorities ()) {
				override = new OverrideType ();
				override.setDeviceType (locations.get (id).getName ());
				override.setPriority (String.valueOf (index++));
				locationOverride.getDevices ().getOverride ().add (override);
			}
			for (Long id : locations.keySet ()) {
				if (!alert.getLocationOverridePriorities ().contains (id)) {
					override = new OverrideType ();
					override.setDeviceType (locations.get (id).getName ());
					override.setPriority ("OFF");
					locationOverride.getDevices ().getOverride ().add (override);
				}
			}
		}
		return locationOverride;
	}

	/**
	 * Creates the notification verbiage
	 * @param alert information
	 * @return the verbiage
	 */
	private VerbiageType createVerbiage (final AlertTO alert) {
		final VerbiageType verbiage = new VerbiageType ();
		StringWithLocale string = null;
		if (alert.getVoiceDelivery () == 2) {
			string = new StringWithLocale ();
			string.setLocale ("en_US");
			string.setValue (alert.getVoiceFile ());
			verbiage.getRecordingTitle ().add (string);
		}
		string = new StringWithLocale ();
		string.setLocale ("en_US");
		string.setValue (alert.getTelephonyMessage ());
		verbiage.getText ().add (string);
		return verbiage;
	}

	/**
	 * Add the response options to the alert
	 * @param alert AlertTO object with the alert information
	 * @return NotificationResponseOptionsType with the available responses
	 */
	private NotificationResponseOptionsType createResponseOptions (final AlertTO alert) {
		final NotificationResponseOptionsType notificationResponseOptionsType =
			alert.getResponseOptions ().isEmpty () ? null :
			new NotificationResponseOptionsType ();
		ResponseOptionType responseOption;
		VerbiageType verbiage;
		StringWithLocale string;
		for (OptionTO option : alert.getResponseOptions ()) {
			responseOption = new ResponseOptionType ();
			responseOption.setResponseText (option.getOptionText ());
			if (option.getAction () == 1) {
				responseOption.setCallBridgePhoneNumber (option.getCallBridge ());
			}
			verbiage = new VerbiageType ();
			//string = new StringWithLocale ();
			//string.setLocale ("en_US");
			//string.setValue (option.getOptionText ());
			//verbiage.getRecordingTitle ().add (string);
			string = new StringWithLocale ();
			string.setLocale ("en_US");
			string.setValue (option.getOptionText ());
			verbiage.getText ().add (string);
			//abey - commeting to compile
			//responseOption.setVerbiage (verbiage);
			notificationResponseOptionsType.getResponseOption ().add (responseOption);
		}
		return notificationResponseOptionsType;
	}

	/**
	 * Sets the duration of the broadcast with one step notification
	 * @param alert AlertTO with the alert data
	 * @return OneStepInfoType with the broadcast duration
	 */
	private OneStepInfoType createOneStepInfo (final AlertTO alert) {
		final OneStepInfoType oneStepInfoType = new OneStepInfoType ();
		oneStepInfoType.setBroadcastDuration (convertToMinutes (alert.getDuration ()) * 60);
		return oneStepInfoType;
	}

	/**
	 * Creates a LeaveMessageType with the parameters set according to the option selected
	 * @param alert AlertTO with the notification data
	 * @return LeaveMessageType with the settings set as the option selected
	 */
	private LeaveMessageType createLeaveMessage (final AlertTO alert) {
		LeaveMessageType leaveMessageType = new LeaveMessageType ();
		leaveMessageType.setCallbackInfo (alert.getLeaveMessage () == 2 || alert.getLeaveMessage () == 4);
		leaveMessageType.setMessage (alert.getLeaveMessage () == 2 || alert.getLeaveMessage () == 3);
		return leaveMessageType;
	}

	/**
	 * Converts a String of format hh:mm into minutes
	 * @param time String in format hh:mm
	 * @return int with the total of minutes
	 */
	private int convertToMinutes (final String time) {
		int minutes = Integer.parseInt (time.substring (time.indexOf (':') + 1));
		int hours = Integer.parseInt (time.substring (0, time.indexOf (':')));
		minutes += hours * 60;
		return minutes;
	}

	private CalloutInfoType createCalloutInfo (AlertTO alert) {
		// TODO Check with John
		final CalloutInfoType calloutInfoType = new CalloutInfoType ();
		calloutInfoType.setNotificationClosedNotice (true);
		return calloutInfoType;
	}

	/**
	 * Creates a FirstResponseInfoType object for a First response notification
	 * @param alert AlertTO with the notification data
	 * @return FirstResponseInfoType with the first response data
	 */
	private FirstResponseInfoType createFirstResponseInfo (AlertTO alert) {
		// TODO Check with John
		final FirstResponseInfoType firstResponseInfoType = new FirstResponseInfoType ();
		firstResponseInfoType.setNotificationClosedNotice (true);
		return firstResponseInfoType;
	}

	/**
	 * Creates the BroadcastInfoType object for a Broadcast notification
	 * @param alert AlertTO with the notification data
	 * @return BroadcastInfoType with the broadcast data
	 */
	private BroadcastInfoType createBroadcastInfo (AlertTO alert) {
		final BroadcastInfoType broadcastInfoType = new BroadcastInfoType ();
		broadcastInfoType.setBroadcastDuration (convertToMinutes (alert.getDuration ()) * 60);
		broadcastInfoType.setRecipients (createDynamicRecipients (alert));
		return broadcastInfoType;
	}

	/**
	 * Creates the attachment summaries object
	 * @param alert AlertTO object with the alert data
	 * @return AttachmentSummariesType with the attachments (if any)
	 */
	private AttachmentSummariesType createAttachments (AlertTO alert) {
		final AttachmentSummariesType attachmentSummaries = new AttachmentSummariesType ();
		if (alert.getNotificationAttachment () >= 0) {
			AttachmentSummaryType attachment = new AttachmentSummaryType ();
			attachment.setFilename (alert.getAttachments ().get (
					alert.getNotificationAttachment ()).getFileInfo ().getFileName ());
			attachment.setFilesize ((int) alert.getAttachments ().get (
					alert.getNotificationAttachment ()).getFileInfo ().getSize ());
			attachmentSummaries.getAttachmentSummaryType ().add (attachment);
		}
		return attachmentSummaries;
	}

	/**
	 * Creates an object AuthorizationType using the screen name and telephony password of the provided userId
	 * @param userId long with the id of the user to set as the initiator of the notification
	 * @return AuthorizationType with the authorization information
	 */
	private AuthorizationType retrieveAuthorization (final long userId) {
		final AuthorizationType authorization = new AuthorizationType ();
		final UserData userData = userDataService.getUser (userId);
		authorization.setPassword (userData.getTelephonyPassword ());
		authorization.setUsername (userData.getEmail ());
		return authorization;
	}

	/**
	 * Creates an object DynamicRecipientsType with the recipients of the alert
	 * @param alert AlertTO with the alert information
	 * @return DynamicRecipientsType with all the alert recipients
	 */
	private NotificationRecipientsDetailsType createDynamicRecipients (final AlertTO alert) {
		final NotificationRecipientsDetailsType dynamicRecipients = new NotificationRecipientsDetailsType ();
		final Map<RecipientType, List<Long>> ids = new HashMap<RecipientType, List<Long>> ();
		final List<String> emailsUsed = new ArrayList<String> ();
		ids.put (RecipientType.AREA_FIELD, new ArrayList<Long> ());
		ids.put (RecipientType.GROUP, new ArrayList<Long> ());
		ids.put (RecipientType.NON_HAN_GROUP, new ArrayList<Long> ());
		ids.put (RecipientType.NON_HAN_USER, new ArrayList<Long> ());
		ids.put (RecipientType.ORGANIZATION, new ArrayList<Long> ());
		ids.put (RecipientType.ORGANIZATION_TYPE, new ArrayList<Long> ());
		ids.put (RecipientType.ROLE, new ArrayList<Long> ());
		ids.put (RecipientType.USER, new ArrayList<Long> ());
		for (RecipientTO recipient : alert.getRecipients ()) {
			addRecipientToRecipientsDetails (recipient, dynamicRecipients, ids, alert, emailsUsed);
		}
		validateRecipients (dynamicRecipients);
		return dynamicRecipients;
	}

	/**
	 * This method validates that there are no more than one email address (principal) as recipient
	 * @param dynamicRecipients to validate
	 */
	private void validateRecipients (final NotificationRecipientsDetailsType dynamicRecipients) {
		final List<String> emails = new ArrayList<String> ();
		final List<RecipientDetailType> toRemove = new ArrayList<RecipientDetailType> ();
		for (com.mir3.ws.RecipientType recipient : dynamicRecipients.getRecipient ()) {
			emails.add (recipient.getUsername ());
		}
		for (RecipientDetailType recipient : dynamicRecipients.getDynamicRecipient ()) {
			for (Device device : recipient.getDevices ().getDevice ()) {
				if (device.getDeviceType ().endsWith ("Email")) {
					if (emails.contains (device.getAddress ())) {
						toRemove.add (recipient);
					}
				} else if (device.getDeviceType ().endsWith ("Email")) {
					if (emails.contains (device.getAddress ())) {
						toRemove.add (recipient);
					}
				}
			}
		}
		if (!toRemove.isEmpty ()) {
			dynamicRecipients.getDynamicRecipient ().removeAll (toRemove);
		}
	}

	/**
	 * Adds the RecipientTO to the recipient details
	 * @param recipient RecipientTO with the recipients to extract the details and add to the list
	 * @param recipientsDetails List of RecipientDetailType to add the extracted details
	 */
	private void addRecipientToRecipientsDetails (final RecipientTO recipient,
			final NotificationRecipientsDetailsType recipientsDetails,
			final Map<RecipientType, List<Long>> ids, final AlertTO alert,
			final List<String> emailsUsed) {
		log.info ("Adding recipient of type: " + recipient.getType () + " with ID: " + recipient.getId ());
		if (!recipient.isNotified ()) {
			if (RecipientType.AREA_FIELD.equals (recipient.getType ())) {
				addAreaField (entityManager.find (AreaField.class, (int) recipient.getId ()),
						recipientsDetails, ids, alert);
			} else if (RecipientType.GROUP.equals (recipient.getType ())) {
				addGroup (entityManager.find (UserCustomGroup.class, recipient.getId ()),
						recipientsDetails, ids, alert, emailsUsed);
			} else if (RecipientType.NON_HAN_GROUP.equals (recipient.getType ())) {
				addNonHanUserGroup (entityManager.find (NonHanUserGroup.class, recipient.getId ()),
						recipientsDetails, ids, alert, emailsUsed);
			} else if (RecipientType.NON_HAN_USER.equals (recipient.getType ())) {
				addNonHanUser (entityManager.find (NonHanUser.class, recipient.getId ()),
						recipientsDetails, ids, alert, emailsUsed);
			} else if (RecipientType.ORGANIZATION.equals (recipient.getType ())) {
				addOrganization (entityManager.find (Organization.class, recipient.getId ()),
						recipientsDetails, ids, alert);
			} else if (RecipientType.ORGANIZATION_TYPE.equals (recipient.getType ())) {
				addOrganizationType (entityManager.find (Organization.class, recipient.getId ()),
						recipientsDetails, ids, alert);
			} else if (RecipientType.ROLE.equals (recipient.getType ())) {
				addRole (entityManager.find (Role.class, recipient.getId ()), recipientsDetails, ids, alert);
			} else if (RecipientType.USER.equals (recipient.getType ())) {
				addUser (entityManager.find (UserData.class, recipient.getId ()), recipientsDetails, ids, alert);
			} else if (RecipientType.CRITERIA.equals (recipient.getType ())) {
				addCriteria (recipient.getCriteria (), recipientsDetails, ids, alert, recipient.isChicagoOnly ());
			}
		}
	}

	@SuppressWarnings("unchecked")
	private void addCriteria (final List<FilterCriteriaTO> criteria,
			final NotificationRecipientsDetailsType recipientsDetails,
			final Map<RecipientType, List<Long>> ids, final AlertTO alert,
			final boolean chicagoOnly) {
		List<Long> orgIds = new ArrayList<Long> ();
		List<Long> aFIds = new ArrayList<Long> ();
		List<Long> rIds = new ArrayList<Long> ();
		List<UserOrganizationalData> foundUsers;
		long orgTypeId = 0;
		for (FilterCriteriaTO filter : criteria) {
			orgTypeId = filter.getOrganizationTypeId ();
			if (filter.getOrganizationId () != null) {
				orgIds.add (filter.getOrganizationId ());
			}
			if (filter.getAreaFieldId () != null) {
				aFIds.add (filter.getAreaFieldId ());
			}
			if (filter.getRoleId () != null) {
				rIds.add (filter.getRoleId ());
			}
		}
		if (orgIds.isEmpty () && aFIds.isEmpty () && rIds.isEmpty ()) {
			foundUsers = new ArrayList<UserOrganizationalData> ();
			final Organization orgType = entityManager.find (Organization.class, orgTypeId);
			if (chicagoOnly) {
				final Query q = entityManager.createNamedQuery ("Address.findByClassIdPK");
				for (Organization organization : orgType.getChildOrganizations ()) {
					q.setParameter ("classNameId", 14017L);
					q.setParameter ("classPK", organization.getOrganizationId ());
					q.setParameter ("city", "CHICAGO");
					if (q.getResultList ().size () > 0) {
						foundUsers.addAll (organization.getUserDatas ());
					}
				}
			} else {
				for (Organization organization : orgType.getChildOrganizations ()) {
					foundUsers.addAll (organization.getUserDatas ());
				}
			}
		} else {
			Query q = entityManager.createQuery (buildSearchQuery (orgIds, aFIds, rIds));
			foundUsers = (List<UserOrganizationalData>) q.getResultList ();
		}
		for (UserOrganizationalData userOrgData : foundUsers) {
			addUser (userOrgData.getUserData (), recipientsDetails, ids, alert);
		}
	}

	/**
	 * Creates a EQL query to load the users using the provided filter criteria
	 * @param organizationIds array with the selected organization ids
	 * @param areaFieldsIds array with the selected area/field ids
	 * @param roleIds array with the selected role ids
	 * @return String with the proper SQL
	 */
	private String buildSearchQuery (final List<Long> organizationIds,
			final List<Long> areaFieldIds, final List<Long> roleIds) {
		final StringBuilder sqlBuilder = new StringBuilder ("select d from UserOrganizationalData d");
		final StringBuilder where = new StringBuilder (" where ");
		boolean whereClause = false;
		boolean previous = false;
		if (organizationIds != null && !organizationIds.isEmpty ()) {
			where.append (" d.organization.organizationId in (");
			for (Long id : organizationIds) {
				if (previous) {
					where.append (", ");
				}
				where.append (id);
				previous = true;
			}
			where.append (')');
			whereClause = true;
		}
		if (areaFieldIds != null && areaFieldIds.size () > 0) {
			if (whereClause) {
				where.append (" and");
			}
			where.append (" d.areaField.areaFieldId in (");
			previous = false;
			for (Long id : areaFieldIds) {
				if (previous) {
					where.append (", ");
				}
				where.append (id);
				previous = true;
			}
			where.append (')');
			whereClause = true;
		}
		if (roleIds != null && roleIds.size () > 0) {
			if (whereClause) {
				where.append (" and");
			}
			where.append (" d.role.roleId in (");
			previous = false;
			for (Long id : roleIds) {
				if (previous) {
					where.append (", ");
				}
				where.append (id);
				previous = true;
			}
			where.append (')');
		}
		return sqlBuilder.append (where).toString ();
	}

	/**
	 * Add a group to the recipient detail list
	 * @param group to add
	 * @param recipientsDetails list of recipient details
	 * @param ids Map with the ids already added to the details
	 */
	private void addGroup (final UserCustomGroup group, final NotificationRecipientsDetailsType recipientsDetails,
			final Map<RecipientType, List<Long>> ids, final AlertTO alert, final List<String> emailsUsed) {
		if (!ids.get (RecipientType.GROUP).contains (group.getGroupId ())) {
			ids.get (RecipientType.GROUP).add (group.getGroupId ());
			for (CustomGroupMember member : group.getMembers ()) {
				if (member.getAreaField () != null) {
					addAreaField (member.getAreaField (), recipientsDetails, ids, alert);
				} else if (member.getChildGroup () != null) {
					addGroup (member.getChildGroup (), recipientsDetails, ids, alert, emailsUsed);
				} else if (member.getNonHanUser () != null) {
					addNonHanUser (member.getNonHanUser (), recipientsDetails, ids, alert, emailsUsed);
				} else if (member.getNonHanUserGroup () != null) {
					addNonHanUserGroup (member.getNonHanUserGroup (), recipientsDetails, ids, alert, emailsUsed);
				} else if (member.getOrganization () != null) {
					if (member.getOrganization ().getParentOrganizationId () == 0) {
						addOrganizationType (member.getOrganization (), recipientsDetails, ids, alert);
					} else {
						addOrganization (member.getOrganization (), recipientsDetails, ids, alert);
					}
				} else if (member.getRole () != null) {
					addRole (member.getRole (), recipientsDetails, ids, alert);
				} else if (member.getUserData () != null) {
					addUser (member.getUserData (), recipientsDetails, ids, alert);
				} else if (member.getCriterias () != null && !member.getCriterias ().isEmpty ()) {
					addGroupCriteria (member.getCriterias (), recipientsDetails, ids, alert,
							member.isChicagoOnly ());
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	private void addGroupCriteria (final Collection<CustomGroupCriteria> criteria,
			final NotificationRecipientsDetailsType recipientsDetails,
			final Map<RecipientType, List<Long>> ids, final AlertTO alert,
			final boolean chicagoOnly) {
		List<Long> orgIds = new ArrayList<Long> ();
		List<Long> aFIds = new ArrayList<Long> ();
		List<Long> rIds = new ArrayList<Long> ();
		List<UserOrganizationalData> foundUsers;
		long orgTypeId = 0;
		for (CustomGroupCriteria filter : criteria) {
			orgTypeId = filter.getOrganizationType ().getOrganizationId ();
			if (filter.getOrganization () != null) {
				orgIds.add (filter.getOrganization ().getOrganizationId ());
			}
			if (filter.getAreaField () != null) {
				aFIds.add ((long) filter.getAreaField ().getAreaFieldId ());
			}
			if (filter.getRole () != null) {
				rIds.add (filter.getRole ().getRoleId ());
			}
		}
		if (orgIds.isEmpty () && aFIds.isEmpty () && rIds.isEmpty ()) {
			foundUsers = new ArrayList<UserOrganizationalData> ();
			final Organization orgType = entityManager.find (Organization.class, orgTypeId);
			if (chicagoOnly) {
				final Query q = entityManager.createNamedQuery ("Address.findByClassIdPK");
				for (Organization organization : orgType.getChildOrganizations ()) {
					q.setParameter ("classNameId", 14017L);
					q.setParameter ("classPK", organization.getOrganizationId ());
					q.setParameter ("city", "CHICAGO");
					if (q.getResultList ().size () > 0) {
						foundUsers.addAll (organization.getUserDatas ());
					}
				}
			} else {
				for (Organization organization : orgType.getChildOrganizations ()) {
					foundUsers.addAll (organization.getUserDatas ());
				}
			}
		} else {
			Query q = entityManager.createQuery (buildSearchQuery (orgIds, aFIds, rIds));
			foundUsers = (List<UserOrganizationalData>) q.getResultList ();
		}
		for (UserOrganizationalData userOrgData : foundUsers) {
			addUser (userOrgData.getUserData (), recipientsDetails, ids, alert);
		}
	}

	/**
	 * Adds a role to the recipients list
	 * @param role Role to add
	 * @param recipientsDetails list of recipient details
	 * @param ids Map with the ids already added to the details
	 */
	private void addRole (final Role role, final NotificationRecipientsDetailsType recipientsDetails,
			final Map<RecipientType, List<Long>> ids, final AlertTO alert) {
		if (!ids.get (RecipientType.ROLE).contains (role.getRoleId ())) {
			ids.get (RecipientType.ROLE).add (role.getRoleId ());
			for (UserData userData : role.getUsers ()) {
				addUser (userData, recipientsDetails, ids, alert);
			}
		}
	}

	/**
	 * Adds a new organization to the recipients list
	 * @param organization Organization with the information of the organization
	 * @param recipientsDetails list of recipient details
	 * @param ids Map with the ids already added to the details
	 */
	private void addOrganization (final Organization organization,
			final NotificationRecipientsDetailsType recipientsDetails,
			final Map<RecipientType, List<Long>> ids, final AlertTO alert) {
		if (!ids.get (RecipientType.ORGANIZATION).contains (organization.getOrganizationId ())) {
			ids.get (RecipientType.ORGANIZATION).add (organization.getOrganizationId ());
			for (UserData userData : organization.getUsers ()) {
				addUser (userData, recipientsDetails, ids, alert);
			}
		}
	}

	/**
	 * Adds an organization type to the recipient list
	 * @param organization Organization with the organization type
	 * @param recipientsDetails list of recipient details
	 * @param ids Map with the ids already added to the details
	 */
	private void addOrganizationType (final Organization organization,
			final NotificationRecipientsDetailsType recipientsDetails,
			final Map<RecipientType, List<Long>> ids, final AlertTO alert) {
		if (!ids.get (RecipientType.ORGANIZATION_TYPE).contains (organization.getOrganizationId ())) {
			ids.get (RecipientType.ORGANIZATION_TYPE).add (organization.getOrganizationId ());
			for (Organization org : organization.getChildOrganizations ()) {
				addOrganization (org, recipientsDetails, ids, alert);
			}
		}
	}

	/**
	 * Adds a new non han user group to the recipients list
	 * @param nonHanUserGroup NonHanUserGroup to add
	 * @param recipientsDetails list of recipient details
	 * @param ids Map with the ids already added to the details
	 */
	private void addNonHanUserGroup (final NonHanUserGroup nonHanUserGroup,
			final NotificationRecipientsDetailsType recipientsDetails,
			final Map<RecipientType, List<Long>> ids,
			final AlertTO alert, final List<String> emailsUsed) {
		if (!ids.get (RecipientType.NON_HAN_GROUP).contains (nonHanUserGroup.getId ())) {
			ids.get (RecipientType.NON_HAN_GROUP).add (nonHanUserGroup.getId ());
			for (NonHanUser nonHanUser : nonHanUserGroup.getNonHanUsers ()) {
				addNonHanUser (nonHanUser, recipientsDetails, ids, alert, emailsUsed);
			}
		}
	}

	/**
	 * Adds a non han user to the recipients list
	 * @param nonHanUser NonHanUser object with the recipient information
	 * @param recipientsDetails list of recipient details
	 * @param ids Map with the ids already added to the details
	 */
	private void addNonHanUser (final NonHanUser nonHanUser,
			final NotificationRecipientsDetailsType recipientsDetails,
			final Map<RecipientType, List<Long>> ids, final AlertTO alert,
			final List<String> emailsUsed) {
		NonHanUserContactDevice workEmail = null;
		for (NonHanUserContactDevice device : nonHanUser.getNonHanContactDevices ()) {
			if (ContactDeviceType.WORK_EMAIL.equals (device.getContactType ())) {
				workEmail = device;
				break;
			}
		}
		if (!ids.get (RecipientType.NON_HAN_USER).contains (nonHanUser.getId ())
				&& (workEmail == null || !emailsUsed.contains (workEmail.getContactDevice ()))) {
			ids.get (RecipientType.NON_HAN_USER).add (nonHanUser.getId ());
			if (workEmail != null) {
				emailsUsed.add (workEmail.getContactDevice ());
			}
			RecipientDetailType recipient = new RecipientDetailType ();
			recipient.setDevices (new DevicesType ());
			recipient.setFirstName (nonHanUser.getFirstName ());
			recipient.setLastName (nonHanUser.getLastName ());
			Device deviceToAdd;
			String ext = null;
			final List<ContactDeviceType> priorities =
				getPriorities (nonHanUser.getNonHanGroup ().getContactPreferences ());
			for (NonHanUserContactDevice device : nonHanUser.getNonHanContactDevices ()) {
				if (ContactDeviceType.CELL_PHONE.equals (device.getContactType ())) {
					deviceToAdd = new Device ();
					deviceToAdd.setDeviceType ("Mobile Phone");
					deviceToAdd.setAddress (device.getContactDevice ());
					deviceToAdd.setDescription ("Cell Phone");
					deviceToAdd.setDefaultPriority (String.valueOf (
							priorities.indexOf (ContactDeviceType.CELL_PHONE) + 1));
					recipient.getDevices ().getDevice ().add (deviceToAdd);
				} else if (ContactDeviceType.HOME_PHONE.equals (device.getContactType ())) {
					deviceToAdd = new Device ();
					deviceToAdd.setDeviceType ("Home Phone");
					deviceToAdd.setAddress (device.getContactDevice ());
					deviceToAdd.setDescription ("Home Phone");
					deviceToAdd.setDefaultPriority (String.valueOf (
							priorities.indexOf (ContactDeviceType.HOME_PHONE) + 1));
					recipient.getDevices ().getDevice ().add (deviceToAdd);
				} else if (ContactDeviceType.PERSONAL_EMAIL.equals (device.getContactType ())) {
					deviceToAdd = new Device ();
					deviceToAdd.setDeviceType ("Home Email");
					deviceToAdd.setAddress (device.getContactDevice ());
					deviceToAdd.setDescription ("Personal Email");
					deviceToAdd.setDefaultPriority (String.valueOf (
							priorities.indexOf (ContactDeviceType.PERSONAL_EMAIL) + 1));
					recipient.getDevices ().getDevice ().add (deviceToAdd);
				} else if (ContactDeviceType.WORK_EMAIL.equals (device.getContactType ())) {
					deviceToAdd = new Device ();
					deviceToAdd.setDeviceType ("Work Email");
					deviceToAdd.setAddress (device.getContactDevice ());
					deviceToAdd.setDescription ("Work Email");
					deviceToAdd.setDefaultPriority (String.valueOf (
							priorities.indexOf (ContactDeviceType.WORK_EMAIL) + 1));
					recipient.getDevices ().getDevice ().add (deviceToAdd);
				} else if (ContactDeviceType.WORK_PHONE.equals (device.getContactType ())) {
					deviceToAdd = new Device ();
					deviceToAdd.setDeviceType ("Work Phone");
					deviceToAdd.setAddress ((ext != null && ext.length () > 0 ) ?
							device.getContactDevice () + 'x' + ext : device.getContactDevice ());
					deviceToAdd.setDescription ("Work Phone");
					deviceToAdd.setDefaultPriority (String.valueOf (
							priorities.indexOf (ContactDeviceType.WORK_PHONE) + 1));
					recipient.getDevices ().getDevice ().add (deviceToAdd);
				} else if (ContactDeviceType.WORK_EXT.equals (device.getContactType ())) {
					ext = device.getContactDevice ();
				}
			}
			recipientsDetails.getDynamicRecipient ().add (recipient);
		}
	}

	/**
	 * Returns the priorities set up for the BU List
	 * @param contactPreferences string to parse
	 * @return the priorities ordered
	 */
	private List<ContactDeviceType> getPriorities (final String contactPreferences) {
		final List<ContactDeviceType> priorities = new ArrayList<ContactDeviceType> ();
		final StringTokenizer tokenizer = new StringTokenizer (contactPreferences, ",");
		String token;
		while (tokenizer.hasMoreTokens ()) {
			token = tokenizer.nextToken ();
			if ("Cell Phone".equals (token)) {
				priorities.add (ContactDeviceType.CELL_PHONE);
			} else if ("Personal EMail".equals (token)) {
				priorities.add (ContactDeviceType.PERSONAL_EMAIL);
			} else if ("Home Phone".equals (token)) {
				priorities.add (ContactDeviceType.HOME_PHONE);
			} else if ("Work EMail".equals (token)) {
				priorities.add (ContactDeviceType.WORK_EMAIL);
			} else if ("Work Phone".equals (token)) {
				priorities.add (ContactDeviceType.WORK_PHONE);
			}
		}
		return priorities;
	}

	/**
	 * Add an area/field to the recipient list
	 * @param areaField entity with the areaField information 
	 * @param recipientsDetails list of recipient details
	 * @param ids Map with the ids already added to the details
	 */
	private void addAreaField (final AreaField areaField,
			final NotificationRecipientsDetailsType recipientsDetails,
			final Map<RecipientType, List<Long>> ids, final AlertTO alert) {
		if (!ids.get (RecipientType.AREA_FIELD).contains (areaField.getAreaFieldId ())) {
			ids.get (RecipientType.AREA_FIELD).add ((long) areaField.getAreaFieldId ());
			for (UserOrganizationalData userOrgData : areaField.getUserDatas ()) {
				addUser (userOrgData.getUserData (), recipientsDetails, ids, alert);
			}
		}
	}

	/**
	 * Creates a new recipient using the provided user information
	 * @param userData Data to add to the recipients
	 * @param recipientsDetails list of recipients details to append the data
	 * @param ids Map with the ids already added to the details
	 */
	private void addUser (final UserData userData, final NotificationRecipientsDetailsType recipientsDetails,
			final Map<RecipientType, List<Long>> ids, final AlertTO alert) {
		if (!ids.get (RecipientType.USER).contains (userData.getUserId ()) && userData.isActive ()) {
			ids.get (RecipientType.USER).add (userData.getUserId ());
			final com.mir3.ws.RecipientType recipient = new com.mir3.ws.RecipientType ();
			recipientsDetails.getRecipient ().add (recipient);
			recipient.setUsername (userData.getEmail ());
			recipient.setFirstName (userData.getContact ().getFirstName ());
			recipient.setLastName (userData.getContact ().getLastName ());
		}
	}

	/**
	 * @param entityManager the entityManager to set
	 */
	//abey...
	//@PersistenceContext
	public void setEntityManager (EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
