package org.cdph.han.beans;

import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public abstract class BaseBean {
	
	/** Class logger */
	private static Log log = LogFactory.getLog(BaseBean.class);
	
	protected final String RESOURCE_BUNDLE_NAME = "messages";
	
	protected ResourceBundle bundle;
	
	protected void addFacesMessage(String resourceBundlekey){    	
    	addFacesMessage("", resourceBundlekey);
	}
    
	protected void addFacesMessage(String clientId, String resourceBundlekey){
		log.debug("In addFacesMessage(" + clientId + ", " + resourceBundlekey + ")");
		String msg = getBundleMessage(resourceBundlekey);
		FacesMessage message = new FacesMessage(
				FacesMessage.SEVERITY_ERROR, msg, msg);
		FacesContext.getCurrentInstance().addMessage(clientId, message);
	}
	
	protected String getBundleMessage(String key){
		if(bundle == null){
			bundle = ResourceBundle.getBundle(RESOURCE_BUNDLE_NAME,
				FacesContext.getCurrentInstance().getViewRoot().getLocale());
		}
		return bundle.getString(key);
		
	}	
	
	protected void addFacesMessageNoBundle(String messageText){
		FacesMessage message = new FacesMessage(
				FacesMessage.SEVERITY_ERROR, messageText, messageText);
		FacesContext.getCurrentInstance().addMessage("", message);
	}
	
}
