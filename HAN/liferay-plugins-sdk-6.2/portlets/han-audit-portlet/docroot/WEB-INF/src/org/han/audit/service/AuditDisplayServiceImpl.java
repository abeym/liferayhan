package org.han.audit.service;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;

import java.util.List;

import org.han.service.model.AuditTrail;
import org.han.service.service.AuditTrailLocalServiceUtil;
import org.springframework.stereotype.Service;

@Service
public class AuditDisplayServiceImpl implements AuditDisplayService {

	@Override
	public JSONArray getRecordsAsJson(JSONObject queryParameters, int start, int size) {
		return	AuditTrailLocalServiceUtil.getAuditTrailEntriesAsJson(queryParameters, start, size);
	}
	
	@Override
	public List<AuditTrail> getRecords(JSONObject queryParameters, int start, int size) {
		return	AuditTrailLocalServiceUtil.getAuditTrailEntries(queryParameters, start, size);
	}

}
