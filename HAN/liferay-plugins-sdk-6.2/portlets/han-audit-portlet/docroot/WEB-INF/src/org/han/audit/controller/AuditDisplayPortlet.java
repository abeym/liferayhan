package org.han.audit.controller;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.PortletDisplay;
import com.liferay.portal.theme.ThemeDisplay;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.han.audit.service.AuditDisplayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;


@Controller("auditController")
@RequestMapping(value="VIEW")
public class AuditDisplayPortlet {
	
	@Autowired
	AuditDisplayService auditDisplayService;
	
 
    @RenderMapping
    public String showHomePage(RenderRequest request, RenderResponse response){
         
    	log.debug("In view mode");
    	
    	
    	
    	ThemeDisplay themeDisplay= (ThemeDisplay)request.getAttribute(WebKeys.THEME_DISPLAY);
    	PortletDisplay portletDisplay= themeDisplay.getPortletDisplay();
    	String portletId= portletDisplay.getId();
    	
    	
    	if(Validator.isNotNull(request.getParameter("search"))){
    		
    		JSONObject jsonAsParameters=JSONFactoryUtil.createJSONObject();
    		
    		
    		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy");
    		Date startDate=null;
    		Date endDate=null;
    		try {

    			if(Validator.isNotNull(request.getParameter("startDate"))){
    				startDate = formatter.parse(request.getParameter("startDate"));
    				jsonAsParameters.put("startDate", startDate.getTime());
    				request.setAttribute("startDate", request.getParameter("startDate"));
    			}
    			
    			if(Validator.isNotNull(request.getParameter("endDate"))){
    				endDate = formatter.parse(request.getParameter("endDate"));
    				jsonAsParameters.put("endDate", endDate.getTime());
    				request.setAttribute("endDate", request.getParameter("endDate"));
    			}
    			if(Validator.isNotNull(request.getParameter("operation"))){
    				jsonAsParameters.put("eventType", request.getParameter("operation"));
    				request.setAttribute("operation", request.getParameter("operation"));
    			}
    			
    			if(Validator.isNotNull(request.getParameter("performedBy"))){
    				jsonAsParameters.put("userEmail", request.getParameter("performedBy"));
    				request.setAttribute("performedBy", request.getParameter("performedBy"));
    			}
    			

    		} catch (ParseException e) {
    			e.printStackTrace();
    		}
    		
    		System.out.println("Start Date"+request.getParameter("startDate"));
    		System.out.println("End Date"+request.getParameter("endDate"));
    		request.setAttribute("data",auditDisplayService.getRecords(jsonAsParameters, -1, -1));
    	}else{
    		request.setAttribute("data",auditDisplayService.getRecords(null, -1, -1));
    	}
    	
    	
    	
    	
    	
    	
    	request.setAttribute("thisPortletId", portletId);
        return "view";
         
    }
    
    @ResourceMapping(value="migrateData") 
    public void findStateForCountry(ResourceRequest request, ResourceResponse response) throws IOException
    { 
    	
    	log.debug("In Ajax mode");
 	
    	
    }
    
    
    
    private static Log log = LogFactoryUtil.getLog(AuditDisplayPortlet.class);
}
