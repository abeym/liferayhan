package org.han.audit.service;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;

import java.util.List;

import org.han.service.model.AuditTrail;

public interface AuditDisplayService {
	
	
	public List<AuditTrail> getRecords(JSONObject queryParameters,int start,int size);

	public JSONArray getRecordsAsJson(JSONObject queryParameters, int start, int size);

}
