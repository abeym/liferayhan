<%@page import="com.liferay.portal.kernel.portlet.LiferayPortletMode"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>



<%--<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-2.1.4.min.js"></script> --%>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/datatables.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-ui.min.js"></script>


<link rel="stylesheet" href="<%=request.getContextPath()%>/css/jquery-ui.min.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/datatables.min.css"/>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/dataTables.jqueryui.min.css"/>

<%-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/bs-3.3.5/jq-2.1.4,dt-1.10.9,r-1.0.7/datatables.min.css"/> --%>
<%--  <script type="text/javascript" src="https://cdn.datatables.net/r/bs-3.3.5/jq-2.1.4,dt-1.10.9,r-1.0.7/datatables.min.js"></script> --%>



<portlet:defineObjects />