<%@include file="../header.jsp" %>

<h3>This is the <I>Data Migration</I> portlet in View mode.</h3>

<%-- <portlet:resourceURL id="filterAudit" var="filterAudit"></portlet:resourceURL> --%>

<liferay-portlet:renderURL var="auditRenderUrl" portletMode="<%=LiferayPortletMode.VIEW.toString() %>">
</liferay-portlet:renderURL>


<table id="filterOptions" class="display" cellspacing="0" width="100%">
	<tr>
		<td>
			Start Date: <input type="text" id="startDate" value="<c:out value='${startDate}'/>"/>
		</td>
		<td>End Date: <input type="text" id="endDate" value="<c:out value='${endDate}'/>"/>
		</td>
	</tr>

	<tr>
		<td>Performed By <input type="text" id="performedBy" value="<c:out value='${performedBy}'/>"></td>
		<td>
		 <fieldset>
        	<label for="operation">Operation :</label>
	        <select name="operation" id="operation">
	         	<option value="all" selected="selected">All</option>
	            <option value="alert">Alert</option>
	            <option value="login">Login</option>
	            <option value="logout">Logout</option>
	            <option value="sessionDestroyed">Session Destroy</option>
	            <option value="lockout">Lockout</option>
	        </select>
    	</fieldset>
	   </td>
	</tr>
<tr>
<td>
<input type="button" id="search" value="Search">
<input type="button" id="clear" value="Clear">
</table>
<br>
<br>

<table id="auditEntries" class="display" cellspacing="0" width="80%">
<thead>
<tr>
<th>
	Date
</th>
<th>
	Performed By
</th>
<th>
	Operation
</th>
</tr>
</thead>
<tbody>

<c:forEach var="anEntry" items="${data}">
  <tr>
	  	<td>
	    	<c:out value="${anEntry.createDate}"/>
	    </td>
	    
	    <td>
	    	<c:out value="${anEntry.userEmail}"/>
	    </td> 
	    <td>
	    	<c:out value="${anEntry.eventType}"/>
	    </td>
    </tr>
</c:forEach>

</tbody>
</table>


<script type="text/javascript">
jQuery(document).ready(function($) {

			  /* This is the function that will get executed after the DOM is fully loaded */
			    $("#startDate" ).datepicker({
			      changeMonth: true,//this option for allowing user to select month
			      changeYear: true //this option for allowing user to select from year range
			     
			    });

			    $("#endDate" ).datepicker({
				      changeMonth: true,//this option for allowing user to select month
				      changeYear: true //this option for allowing user to select from year range
				});

			   $('#auditEntries').DataTable(); 

			   $("#clear").click(function(){

				   $("#startDate").val('');
				   $("#endDate").val('');
				   $("#performedBy").val('');
				   $("#operation").val('all');
			        
			    }); 


			   $("#search").click(function(){

				var startDateForServer=$.datepicker.formatDate('mm/dd/yy',$("#startDate").datepicker('getDate'));
				var endDateForServer=$.datepicker.formatDate('mm/dd/yy',$("#endDate").datepicker('getDate'));
	
				var renderUrl='<%=auditRenderUrl.toString()%>';
				 var renderHref=renderUrl+"&search="+true;
				 var allowRedirect=false;
				if(startDateForServer){
					renderHref=renderHref+"&startDate="+ startDateForServer;
					allowRedirect=true;
				}

				if(endDateForServer){
					renderHref=renderHref+"&endDate="+ endDateForServer;
					allowRedirect=true;
				}

				if($("#performedBy").val()){
					renderHref=renderHref+"&performedBy="+ $("#performedBy").val();
					allowRedirect=true;
				}

				if($("#operation").val()){
					renderHref=renderHref+"&operation="+ $("#operation").val();
					allowRedirect=true;
				}
				alert(renderHref);
				 //var renderHref=renderUrl+"&startDate="+ startDateForServer+"&endDate="+endDateForServer+"&search="+true+"&performedBy="+ $("#performedBy").val()+"&operation="+$("#operation").val();
				if(allowRedirect){
				 	$(location).attr('href', renderHref);
				}   
			  });
			    
});//jquery ready



</script>
