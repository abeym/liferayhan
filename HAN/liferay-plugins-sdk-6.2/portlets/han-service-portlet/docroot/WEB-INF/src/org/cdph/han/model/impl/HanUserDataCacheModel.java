/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.cdph.han.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import org.cdph.han.model.HanUserData;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing HanUserData in entity cache.
 *
 * @author Choicegen
 * @see HanUserData
 * @generated
 */
public class HanUserDataCacheModel implements CacheModel<HanUserData>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(17);

		sb.append("{userId=");
		sb.append(userId);
		sb.append(", telephony_pass=");
		sb.append(telephony_pass);
		sb.append(", mailPass=");
		sb.append(mailPass);
		sb.append(", telephony_id=");
		sb.append(telephony_id);
		sb.append(", telephony_pin=");
		sb.append(telephony_pin);
		sb.append(", department=");
		sb.append(department);
		sb.append(", public_profile=");
		sb.append(public_profile);
		sb.append(", mir3_id=");
		sb.append(mir3_id);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public HanUserData toEntityModel() {
		HanUserDataImpl hanUserDataImpl = new HanUserDataImpl();

		hanUserDataImpl.setUserId(userId);

		if (telephony_pass == null) {
			hanUserDataImpl.setTelephony_pass(StringPool.BLANK);
		}
		else {
			hanUserDataImpl.setTelephony_pass(telephony_pass);
		}

		if (mailPass == null) {
			hanUserDataImpl.setMailPass(StringPool.BLANK);
		}
		else {
			hanUserDataImpl.setMailPass(mailPass);
		}

		if (telephony_id == null) {
			hanUserDataImpl.setTelephony_id(StringPool.BLANK);
		}
		else {
			hanUserDataImpl.setTelephony_id(telephony_id);
		}

		if (telephony_pin == null) {
			hanUserDataImpl.setTelephony_pin(StringPool.BLANK);
		}
		else {
			hanUserDataImpl.setTelephony_pin(telephony_pin);
		}

		if (department == null) {
			hanUserDataImpl.setDepartment(StringPool.BLANK);
		}
		else {
			hanUserDataImpl.setDepartment(department);
		}

		hanUserDataImpl.setPublic_profile(public_profile);
		hanUserDataImpl.setMir3_id(mir3_id);

		hanUserDataImpl.resetOriginalValues();

		return hanUserDataImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		userId = objectInput.readLong();
		telephony_pass = objectInput.readUTF();
		mailPass = objectInput.readUTF();
		telephony_id = objectInput.readUTF();
		telephony_pin = objectInput.readUTF();
		department = objectInput.readUTF();
		public_profile = objectInput.readBoolean();
		mir3_id = objectInput.readInt();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(userId);

		if (telephony_pass == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(telephony_pass);
		}

		if (mailPass == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(mailPass);
		}

		if (telephony_id == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(telephony_id);
		}

		if (telephony_pin == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(telephony_pin);
		}

		if (department == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(department);
		}

		objectOutput.writeBoolean(public_profile);
		objectOutput.writeInt(mir3_id);
	}

	public long userId;
	public String telephony_pass;
	public String mailPass;
	public String telephony_id;
	public String telephony_pin;
	public String department;
	public boolean public_profile;
	public int mir3_id;
}