/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.cdph.han.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import org.cdph.han.model.PagerCarrier;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing PagerCarrier in entity cache.
 *
 * @author Choicegen
 * @see PagerCarrier
 * @generated
 */
public class PagerCarrierCacheModel implements CacheModel<PagerCarrier>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{id=");
		sb.append(id);
		sb.append(", name=");
		sb.append(name);
		sb.append(", mir3Id=");
		sb.append(mir3Id);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public PagerCarrier toEntityModel() {
		PagerCarrierImpl pagerCarrierImpl = new PagerCarrierImpl();

		pagerCarrierImpl.setId(id);

		if (name == null) {
			pagerCarrierImpl.setName(StringPool.BLANK);
		}
		else {
			pagerCarrierImpl.setName(name);
		}

		pagerCarrierImpl.setMir3Id(mir3Id);

		pagerCarrierImpl.resetOriginalValues();

		return pagerCarrierImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		id = objectInput.readInt();
		name = objectInput.readUTF();
		mir3Id = objectInput.readInt();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeInt(id);

		if (name == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(name);
		}

		objectOutput.writeInt(mir3Id);
	}

	public int id;
	public String name;
	public int mir3Id;
}