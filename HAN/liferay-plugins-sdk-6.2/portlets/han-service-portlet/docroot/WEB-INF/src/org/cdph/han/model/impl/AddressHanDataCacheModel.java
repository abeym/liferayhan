/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.cdph.han.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import org.cdph.han.model.AddressHanData;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing AddressHanData in entity cache.
 *
 * @author Choicegen
 * @see AddressHanData
 * @generated
 */
public class AddressHanDataCacheModel implements CacheModel<AddressHanData>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{addressId=");
		sb.append(addressId);
		sb.append(", floor=");
		sb.append(floor);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public AddressHanData toEntityModel() {
		AddressHanDataImpl addressHanDataImpl = new AddressHanDataImpl();

		addressHanDataImpl.setAddressId(addressId);

		if (floor == null) {
			addressHanDataImpl.setFloor(StringPool.BLANK);
		}
		else {
			addressHanDataImpl.setFloor(floor);
		}

		addressHanDataImpl.resetOriginalValues();

		return addressHanDataImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		addressId = objectInput.readLong();
		floor = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(addressId);

		if (floor == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(floor);
		}
	}

	public long addressId;
	public String floor;
}