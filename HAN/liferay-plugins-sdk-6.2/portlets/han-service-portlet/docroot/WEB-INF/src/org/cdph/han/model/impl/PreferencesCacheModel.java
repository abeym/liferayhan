/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.cdph.han.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import org.cdph.han.model.Preferences;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Preferences in entity cache.
 *
 * @author Choicegen
 * @see Preferences
 * @generated
 */
public class PreferencesCacheModel implements CacheModel<Preferences>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{id=");
		sb.append(id);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", deviceIndex=");
		sb.append(deviceIndex);
		sb.append(", primaryEmail=");
		sb.append(primaryEmail);
		sb.append(", emailAddressId=");
		sb.append(emailAddressId);
		sb.append(", phoneId=");
		sb.append(phoneId);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Preferences toEntityModel() {
		PreferencesImpl preferencesImpl = new PreferencesImpl();

		preferencesImpl.setId(id);
		preferencesImpl.setUserId(userId);
		preferencesImpl.setDeviceIndex(deviceIndex);
		preferencesImpl.setPrimaryEmail(primaryEmail);
		preferencesImpl.setEmailAddressId(emailAddressId);
		preferencesImpl.setPhoneId(phoneId);

		preferencesImpl.resetOriginalValues();

		return preferencesImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		id = objectInput.readLong();
		userId = objectInput.readLong();
		deviceIndex = objectInput.readInt();
		primaryEmail = objectInput.readBoolean();
		emailAddressId = objectInput.readLong();
		phoneId = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(id);
		objectOutput.writeLong(userId);
		objectOutput.writeInt(deviceIndex);
		objectOutput.writeBoolean(primaryEmail);
		objectOutput.writeLong(emailAddressId);
		objectOutput.writeLong(phoneId);
	}

	public long id;
	public long userId;
	public int deviceIndex;
	public boolean primaryEmail;
	public long emailAddressId;
	public long phoneId;
}