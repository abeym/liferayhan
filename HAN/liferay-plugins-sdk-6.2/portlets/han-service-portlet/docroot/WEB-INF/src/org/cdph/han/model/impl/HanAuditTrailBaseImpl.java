/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.cdph.han.model.impl;

import com.liferay.portal.kernel.exception.SystemException;

import org.cdph.han.model.HanAuditTrail;
import org.cdph.han.service.HanAuditTrailLocalServiceUtil;

/**
 * The extended model base implementation for the HanAuditTrail service. Represents a row in the &quot;audit_log&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This class exists only as a container for the default extended model level methods generated by ServiceBuilder. Helper methods and all application logic should be put in {@link HanAuditTrailImpl}.
 * </p>
 *
 * @author Choicegen
 * @see HanAuditTrailImpl
 * @see org.cdph.han.model.HanAuditTrail
 * @generated
 */
public abstract class HanAuditTrailBaseImpl extends HanAuditTrailModelImpl
	implements HanAuditTrail {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a han audit trail model instance should use the {@link HanAuditTrail} interface instead.
	 */
	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			HanAuditTrailLocalServiceUtil.addHanAuditTrail(this);
		}
		else {
			HanAuditTrailLocalServiceUtil.updateHanAuditTrail(this);
		}
	}
}