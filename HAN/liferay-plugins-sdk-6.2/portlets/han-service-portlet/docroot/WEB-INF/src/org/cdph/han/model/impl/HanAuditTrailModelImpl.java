/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.cdph.han.model.impl;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSON;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.util.PortalUtil;

import com.liferay.portlet.expando.model.ExpandoBridge;
import com.liferay.portlet.expando.util.ExpandoBridgeFactoryUtil;

import org.cdph.han.model.HanAuditTrail;
import org.cdph.han.model.HanAuditTrailModel;
import org.cdph.han.model.HanAuditTrailSoap;

import java.io.Serializable;

import java.sql.Types;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The base model implementation for the HanAuditTrail service. Represents a row in the &quot;audit_log&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface {@link org.cdph.han.model.HanAuditTrailModel} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link HanAuditTrailImpl}.
 * </p>
 *
 * @author Choicegen
 * @see HanAuditTrailImpl
 * @see org.cdph.han.model.HanAuditTrail
 * @see org.cdph.han.model.HanAuditTrailModel
 * @generated
 */
@JSON(strict = true)
public class HanAuditTrailModelImpl extends BaseModelImpl<HanAuditTrail>
	implements HanAuditTrailModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a han audit trail model instance should use the {@link org.cdph.han.model.HanAuditTrail} interface instead.
	 */
	public static final String TABLE_NAME = "audit_log";
	public static final Object[][] TABLE_COLUMNS = {
			{ "id", Types.BIGINT },
			{ "action_performed", Types.VARCHAR },
			{ "userId", Types.BIGINT },
			{ "date_performed", Types.TIMESTAMP },
			{ "alert_id", Types.BIGINT }
		};
	public static final String TABLE_SQL_CREATE = "create table audit_log (id LONG not null primary key,action_performed VARCHAR(75) null,userId LONG,date_performed DATE null,alert_id LONG)";
	public static final String TABLE_SQL_DROP = "drop table audit_log";
	public static final String ORDER_BY_JPQL = " ORDER BY hanAuditTrail.auditTrailId ASC";
	public static final String ORDER_BY_SQL = " ORDER BY audit_log.id ASC";
	public static final String DATA_SOURCE = "liferayDataSource";
	public static final String SESSION_FACTORY = "liferaySessionFactory";
	public static final String TX_MANAGER = "liferayTransactionManager";
	public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.entity.cache.enabled.org.cdph.han.model.HanAuditTrail"),
			true);
	public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.finder.cache.enabled.org.cdph.han.model.HanAuditTrail"),
			true);
	public static final boolean COLUMN_BITMASK_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.column.bitmask.enabled.org.cdph.han.model.HanAuditTrail"),
			true);
	public static long ACTIONPERFORMED_COLUMN_BITMASK = 1L;
	public static long USERID_COLUMN_BITMASK = 2L;
	public static long AUDITTRAILID_COLUMN_BITMASK = 4L;

	/**
	 * Converts the soap model instance into a normal model instance.
	 *
	 * @param soapModel the soap model instance to convert
	 * @return the normal model instance
	 */
	public static HanAuditTrail toModel(HanAuditTrailSoap soapModel) {
		if (soapModel == null) {
			return null;
		}

		HanAuditTrail model = new HanAuditTrailImpl();

		model.setAuditTrailId(soapModel.getAuditTrailId());
		model.setActionPerformed(soapModel.getActionPerformed());
		model.setUserId(soapModel.getUserId());
		model.setDate(soapModel.getDate());
		model.setAlertId(soapModel.getAlertId());

		return model;
	}

	/**
	 * Converts the soap model instances into normal model instances.
	 *
	 * @param soapModels the soap model instances to convert
	 * @return the normal model instances
	 */
	public static List<HanAuditTrail> toModels(HanAuditTrailSoap[] soapModels) {
		if (soapModels == null) {
			return null;
		}

		List<HanAuditTrail> models = new ArrayList<HanAuditTrail>(soapModels.length);

		for (HanAuditTrailSoap soapModel : soapModels) {
			models.add(toModel(soapModel));
		}

		return models;
	}

	public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.util.service.ServiceProps.get(
				"lock.expiration.time.org.cdph.han.model.HanAuditTrail"));

	public HanAuditTrailModelImpl() {
	}

	@Override
	public long getPrimaryKey() {
		return _auditTrailId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setAuditTrailId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _auditTrailId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Class<?> getModelClass() {
		return HanAuditTrail.class;
	}

	@Override
	public String getModelClassName() {
		return HanAuditTrail.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("auditTrailId", getAuditTrailId());
		attributes.put("actionPerformed", getActionPerformed());
		attributes.put("userId", getUserId());
		attributes.put("date", getDate());
		attributes.put("alertId", getAlertId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long auditTrailId = (Long)attributes.get("auditTrailId");

		if (auditTrailId != null) {
			setAuditTrailId(auditTrailId);
		}

		String actionPerformed = (String)attributes.get("actionPerformed");

		if (actionPerformed != null) {
			setActionPerformed(actionPerformed);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Date date = (Date)attributes.get("date");

		if (date != null) {
			setDate(date);
		}

		Long alertId = (Long)attributes.get("alertId");

		if (alertId != null) {
			setAlertId(alertId);
		}
	}

	@JSON
	@Override
	public long getAuditTrailId() {
		return _auditTrailId;
	}

	@Override
	public void setAuditTrailId(long auditTrailId) {
		_auditTrailId = auditTrailId;
	}

	@JSON
	@Override
	public String getActionPerformed() {
		if (_actionPerformed == null) {
			return StringPool.BLANK;
		}
		else {
			return _actionPerformed;
		}
	}

	@Override
	public void setActionPerformed(String actionPerformed) {
		_columnBitmask |= ACTIONPERFORMED_COLUMN_BITMASK;

		if (_originalActionPerformed == null) {
			_originalActionPerformed = _actionPerformed;
		}

		_actionPerformed = actionPerformed;
	}

	public String getOriginalActionPerformed() {
		return GetterUtil.getString(_originalActionPerformed);
	}

	@JSON
	@Override
	public long getUserId() {
		return _userId;
	}

	@Override
	public void setUserId(long userId) {
		_columnBitmask |= USERID_COLUMN_BITMASK;

		if (!_setOriginalUserId) {
			_setOriginalUserId = true;

			_originalUserId = _userId;
		}

		_userId = userId;
	}

	@Override
	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	@Override
	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	public long getOriginalUserId() {
		return _originalUserId;
	}

	@JSON
	@Override
	public Date getDate() {
		return _date;
	}

	@Override
	public void setDate(Date date) {
		_date = date;
	}

	@JSON
	@Override
	public long getAlertId() {
		return _alertId;
	}

	@Override
	public void setAlertId(long alertId) {
		_alertId = alertId;
	}

	public long getColumnBitmask() {
		return _columnBitmask;
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return ExpandoBridgeFactoryUtil.getExpandoBridge(0,
			HanAuditTrail.class.getName(), getPrimaryKey());
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		ExpandoBridge expandoBridge = getExpandoBridge();

		expandoBridge.setAttributes(serviceContext);
	}

	@Override
	public HanAuditTrail toEscapedModel() {
		if (_escapedModel == null) {
			_escapedModel = (HanAuditTrail)ProxyUtil.newProxyInstance(_classLoader,
					_escapedModelInterfaces, new AutoEscapeBeanHandler(this));
		}

		return _escapedModel;
	}

	@Override
	public Object clone() {
		HanAuditTrailImpl hanAuditTrailImpl = new HanAuditTrailImpl();

		hanAuditTrailImpl.setAuditTrailId(getAuditTrailId());
		hanAuditTrailImpl.setActionPerformed(getActionPerformed());
		hanAuditTrailImpl.setUserId(getUserId());
		hanAuditTrailImpl.setDate(getDate());
		hanAuditTrailImpl.setAlertId(getAlertId());

		hanAuditTrailImpl.resetOriginalValues();

		return hanAuditTrailImpl;
	}

	@Override
	public int compareTo(HanAuditTrail hanAuditTrail) {
		long primaryKey = hanAuditTrail.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof HanAuditTrail)) {
			return false;
		}

		HanAuditTrail hanAuditTrail = (HanAuditTrail)obj;

		long primaryKey = hanAuditTrail.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public void resetOriginalValues() {
		HanAuditTrailModelImpl hanAuditTrailModelImpl = this;

		hanAuditTrailModelImpl._originalActionPerformed = hanAuditTrailModelImpl._actionPerformed;

		hanAuditTrailModelImpl._originalUserId = hanAuditTrailModelImpl._userId;

		hanAuditTrailModelImpl._setOriginalUserId = false;

		hanAuditTrailModelImpl._columnBitmask = 0;
	}

	@Override
	public CacheModel<HanAuditTrail> toCacheModel() {
		HanAuditTrailCacheModel hanAuditTrailCacheModel = new HanAuditTrailCacheModel();

		hanAuditTrailCacheModel.auditTrailId = getAuditTrailId();

		hanAuditTrailCacheModel.actionPerformed = getActionPerformed();

		String actionPerformed = hanAuditTrailCacheModel.actionPerformed;

		if ((actionPerformed != null) && (actionPerformed.length() == 0)) {
			hanAuditTrailCacheModel.actionPerformed = null;
		}

		hanAuditTrailCacheModel.userId = getUserId();

		Date date = getDate();

		if (date != null) {
			hanAuditTrailCacheModel.date = date.getTime();
		}
		else {
			hanAuditTrailCacheModel.date = Long.MIN_VALUE;
		}

		hanAuditTrailCacheModel.alertId = getAlertId();

		return hanAuditTrailCacheModel;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{auditTrailId=");
		sb.append(getAuditTrailId());
		sb.append(", actionPerformed=");
		sb.append(getActionPerformed());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", date=");
		sb.append(getDate());
		sb.append(", alertId=");
		sb.append(getAlertId());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(19);

		sb.append("<model><model-name>");
		sb.append("org.cdph.han.model.HanAuditTrail");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>auditTrailId</column-name><column-value><![CDATA[");
		sb.append(getAuditTrailId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>actionPerformed</column-name><column-value><![CDATA[");
		sb.append(getActionPerformed());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>date</column-name><column-value><![CDATA[");
		sb.append(getDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>alertId</column-name><column-value><![CDATA[");
		sb.append(getAlertId());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private static ClassLoader _classLoader = HanAuditTrail.class.getClassLoader();
	private static Class<?>[] _escapedModelInterfaces = new Class[] {
			HanAuditTrail.class
		};
	private long _auditTrailId;
	private String _actionPerformed;
	private String _originalActionPerformed;
	private long _userId;
	private String _userUuid;
	private long _originalUserId;
	private boolean _setOriginalUserId;
	private Date _date;
	private long _alertId;
	private long _columnBitmask;
	private HanAuditTrail _escapedModel;
}