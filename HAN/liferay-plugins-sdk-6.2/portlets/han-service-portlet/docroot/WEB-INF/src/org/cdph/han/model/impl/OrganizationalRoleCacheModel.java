/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.cdph.han.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import org.cdph.han.model.OrganizationalRole;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing OrganizationalRole in entity cache.
 *
 * @author Choicegen
 * @see OrganizationalRole
 * @generated
 */
public class OrganizationalRoleCacheModel implements CacheModel<OrganizationalRole>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{organizationId=");
		sb.append(organizationId);
		sb.append(", roleId=");
		sb.append(roleId);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public OrganizationalRole toEntityModel() {
		OrganizationalRoleImpl organizationalRoleImpl = new OrganizationalRoleImpl();

		organizationalRoleImpl.setOrganizationId(organizationId);
		organizationalRoleImpl.setRoleId(roleId);

		organizationalRoleImpl.resetOriginalValues();

		return organizationalRoleImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		organizationId = objectInput.readLong();
		roleId = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(organizationId);
		objectOutput.writeLong(roleId);
	}

	public long organizationId;
	public long roleId;
}