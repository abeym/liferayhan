/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.cdph.han.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import org.cdph.han.model.PhoneHanData;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing PhoneHanData in entity cache.
 *
 * @author Choicegen
 * @see PhoneHanData
 * @generated
 */
public class PhoneHanDataCacheModel implements CacheModel<PhoneHanData>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{phoneId=");
		sb.append(phoneId);
		sb.append(", phoneName=");
		sb.append(phoneName);
		sb.append(", preference=");
		sb.append(preference);
		sb.append(", pagerCarrierId=");
		sb.append(pagerCarrierId);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public PhoneHanData toEntityModel() {
		PhoneHanDataImpl phoneHanDataImpl = new PhoneHanDataImpl();

		phoneHanDataImpl.setPhoneId(phoneId);

		if (phoneName == null) {
			phoneHanDataImpl.setPhoneName(StringPool.BLANK);
		}
		else {
			phoneHanDataImpl.setPhoneName(phoneName);
		}

		phoneHanDataImpl.setPreference(preference);
		phoneHanDataImpl.setPagerCarrierId(pagerCarrierId);

		phoneHanDataImpl.resetOriginalValues();

		return phoneHanDataImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		phoneId = objectInput.readLong();
		phoneName = objectInput.readUTF();
		preference = objectInput.readInt();
		pagerCarrierId = objectInput.readInt();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(phoneId);

		if (phoneName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(phoneName);
		}

		objectOutput.writeInt(preference);
		objectOutput.writeInt(pagerCarrierId);
	}

	public long phoneId;
	public String phoneName;
	public int preference;
	public int pagerCarrierId;
}