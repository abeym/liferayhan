/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.cdph.han.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import org.cdph.han.model.HanAuditTrail;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing HanAuditTrail in entity cache.
 *
 * @author Choicegen
 * @see HanAuditTrail
 * @generated
 */
public class HanAuditTrailCacheModel implements CacheModel<HanAuditTrail>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{auditTrailId=");
		sb.append(auditTrailId);
		sb.append(", actionPerformed=");
		sb.append(actionPerformed);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", date=");
		sb.append(date);
		sb.append(", alertId=");
		sb.append(alertId);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public HanAuditTrail toEntityModel() {
		HanAuditTrailImpl hanAuditTrailImpl = new HanAuditTrailImpl();

		hanAuditTrailImpl.setAuditTrailId(auditTrailId);

		if (actionPerformed == null) {
			hanAuditTrailImpl.setActionPerformed(StringPool.BLANK);
		}
		else {
			hanAuditTrailImpl.setActionPerformed(actionPerformed);
		}

		hanAuditTrailImpl.setUserId(userId);

		if (date == Long.MIN_VALUE) {
			hanAuditTrailImpl.setDate(null);
		}
		else {
			hanAuditTrailImpl.setDate(new Date(date));
		}

		hanAuditTrailImpl.setAlertId(alertId);

		hanAuditTrailImpl.resetOriginalValues();

		return hanAuditTrailImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		auditTrailId = objectInput.readLong();
		actionPerformed = objectInput.readUTF();
		userId = objectInput.readLong();
		date = objectInput.readLong();
		alertId = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(auditTrailId);

		if (actionPerformed == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(actionPerformed);
		}

		objectOutput.writeLong(userId);
		objectOutput.writeLong(date);
		objectOutput.writeLong(alertId);
	}

	public long auditTrailId;
	public String actionPerformed;
	public long userId;
	public long date;
	public long alertId;
}