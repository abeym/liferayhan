/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.cdph.han.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import org.cdph.han.model.EmailHanData;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing EmailHanData in entity cache.
 *
 * @author Choicegen
 * @see EmailHanData
 * @generated
 */
public class EmailHanDataCacheModel implements CacheModel<EmailHanData>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{emailAddressId=");
		sb.append(emailAddressId);
		sb.append(", description=");
		sb.append(description);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public EmailHanData toEntityModel() {
		EmailHanDataImpl emailHanDataImpl = new EmailHanDataImpl();

		emailHanDataImpl.setEmailAddressId(emailAddressId);

		if (description == null) {
			emailHanDataImpl.setDescription(StringPool.BLANK);
		}
		else {
			emailHanDataImpl.setDescription(description);
		}

		emailHanDataImpl.resetOriginalValues();

		return emailHanDataImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		emailAddressId = objectInput.readLong();
		description = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(emailAddressId);

		if (description == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(description);
		}
	}

	public long emailAddressId;
	public String description;
}