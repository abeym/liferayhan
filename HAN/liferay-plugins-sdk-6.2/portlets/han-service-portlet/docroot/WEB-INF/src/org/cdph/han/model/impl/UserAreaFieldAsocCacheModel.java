/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.cdph.han.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import org.cdph.han.model.UserAreaFieldAsoc;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing UserAreaFieldAsoc in entity cache.
 *
 * @author Choicegen
 * @see UserAreaFieldAsoc
 * @generated
 */
public class UserAreaFieldAsocCacheModel implements CacheModel<UserAreaFieldAsoc>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{userId=");
		sb.append(userId);
		sb.append(", organizationId=");
		sb.append(organizationId);
		sb.append(", areaFieldId=");
		sb.append(areaFieldId);
		sb.append(", roleId=");
		sb.append(roleId);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public UserAreaFieldAsoc toEntityModel() {
		UserAreaFieldAsocImpl userAreaFieldAsocImpl = new UserAreaFieldAsocImpl();

		userAreaFieldAsocImpl.setUserId(userId);
		userAreaFieldAsocImpl.setOrganizationId(organizationId);
		userAreaFieldAsocImpl.setAreaFieldId(areaFieldId);
		userAreaFieldAsocImpl.setRoleId(roleId);

		userAreaFieldAsocImpl.resetOriginalValues();

		return userAreaFieldAsocImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		userId = objectInput.readLong();
		organizationId = objectInput.readLong();
		areaFieldId = objectInput.readLong();
		roleId = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(userId);
		objectOutput.writeLong(organizationId);
		objectOutput.writeLong(areaFieldId);
		objectOutput.writeLong(roleId);
	}

	public long userId;
	public long organizationId;
	public long areaFieldId;
	public long roleId;
}