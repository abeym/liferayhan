/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.cdph.han.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import org.cdph.han.model.AlternateContact;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing AlternateContact in entity cache.
 *
 * @author Choicegen
 * @see AlternateContact
 * @generated
 */
public class AlternateContactCacheModel implements CacheModel<AlternateContact>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{id=");
		sb.append(id);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", alternateUserId=");
		sb.append(alternateUserId);
		sb.append(", preference=");
		sb.append(preference);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public AlternateContact toEntityModel() {
		AlternateContactImpl alternateContactImpl = new AlternateContactImpl();

		alternateContactImpl.setId(id);
		alternateContactImpl.setUserId(userId);
		alternateContactImpl.setAlternateUserId(alternateUserId);
		alternateContactImpl.setPreference(preference);

		alternateContactImpl.resetOriginalValues();

		return alternateContactImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		id = objectInput.readLong();
		userId = objectInput.readLong();
		alternateUserId = objectInput.readLong();
		preference = objectInput.readInt();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(id);
		objectOutput.writeLong(userId);
		objectOutput.writeLong(alternateUserId);
		objectOutput.writeInt(preference);
	}

	public long id;
	public long userId;
	public long alternateUserId;
	public int preference;
}