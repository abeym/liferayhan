/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.cdph.han.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import org.cdph.han.model.AreaFieldCatalog;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing AreaFieldCatalog in entity cache.
 *
 * @author Choicegen
 * @see AreaFieldCatalog
 * @generated
 */
public class AreaFieldCatalogCacheModel implements CacheModel<AreaFieldCatalog>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{areaFieldId=");
		sb.append(areaFieldId);
		sb.append(", organizationId=");
		sb.append(organizationId);
		sb.append(", description=");
		sb.append(description);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public AreaFieldCatalog toEntityModel() {
		AreaFieldCatalogImpl areaFieldCatalogImpl = new AreaFieldCatalogImpl();

		areaFieldCatalogImpl.setAreaFieldId(areaFieldId);
		areaFieldCatalogImpl.setOrganizationId(organizationId);

		if (description == null) {
			areaFieldCatalogImpl.setDescription(StringPool.BLANK);
		}
		else {
			areaFieldCatalogImpl.setDescription(description);
		}

		areaFieldCatalogImpl.resetOriginalValues();

		return areaFieldCatalogImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		areaFieldId = objectInput.readLong();
		organizationId = objectInput.readLong();
		description = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(areaFieldId);
		objectOutput.writeLong(organizationId);

		if (description == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(description);
		}
	}

	public long areaFieldId;
	public long organizationId;
	public String description;
}