create index IX_53FE2E9F on HanData_users_areafields (area_field_id);
create index IX_B2D1365D on HanData_users_areafields (userId);

create index IX_E30B1552 on TypeOrganizationRoles (organizationTypeId);

create index IX_E6E5F49D on alternate_contacts (alternate_userId);
create index IX_5F12BA78 on alternate_contacts (userId);

create index IX_42A22B2F on area_field_catalog (organization_id);

create index IX_56C9D30B on audit_log (userId, action_performed);

create index IX_A37A4C19 on contact_preferences (userId);

create index IX_2DFDCB22 on han_properties (name);

create index IX_42A999DD on users_areafields (userId);