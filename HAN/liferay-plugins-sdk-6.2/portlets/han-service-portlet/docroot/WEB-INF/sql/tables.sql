create table HanData_users_areafields (
	area_field_id LONG not null,
	userId LONG not null,
	primary key (area_field_id, userId)
);

create table TypeOrganizationRoles (
	organizationTypeId LONG not null,
	roleId LONG not null,
	primary key (organizationTypeId, roleId)
);

create table address_han_data (
	address_id LONG not null primary key,
	floor VARCHAR(75) null
);

create table alternate_contacts (
	id LONG not null primary key,
	userId LONG,
	alternate_userId LONG,
	preference INTEGER
);

create table area_field_catalog (
	area_field_id LONG not null primary key,
	organization_id LONG,
	description VARCHAR(75) null
);

create table audit_log (
	id LONG not null primary key,
	action_performed VARCHAR(75) null,
	userId LONG,
	date_performed DATE null,
	alert_id LONG
);

create table contact_preferences (
	id LONG not null primary key,
	userId LONG,
	device_idx INTEGER,
	primary_email BOOLEAN,
	emailAddressId LONG,
	phoneId LONG
);

create table email_han_data (
	email_address_id LONG not null primary key,
	description VARCHAR(75) null
);

create table han_properties (
	id LONG not null primary key,
	name VARCHAR(75) null,
	value VARCHAR(75) null
);

create table han_user_data (
	userId LONG not null primary key,
	telephony_pass VARCHAR(75) null,
	mailPass VARCHAR(75) null,
	telephony_id VARCHAR(75) null,
	telephony_pin VARCHAR(75) null,
	department VARCHAR(75) null,
	public_profile BOOLEAN,
	mir3_id INTEGER
);

create table pager_carrier_catalog (
	id INTEGER not null primary key,
	carrier_name VARCHAR(75) null,
	mir3_id INTEGER
);

create table phone_han_data (
	phone_id LONG not null primary key,
	phone_name VARCHAR(75) null,
	preference INTEGER,
	pager_carrier_id INTEGER
);

create table users_areafields (
	userId LONG not null,
	organizationId LONG not null,
	area_field_id LONG,
	roleId LONG,
	primary key (userId, organizationId)
);