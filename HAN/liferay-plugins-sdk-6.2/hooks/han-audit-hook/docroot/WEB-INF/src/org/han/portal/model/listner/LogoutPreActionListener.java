package org.han.portal.model.listner;

import com.liferay.portal.kernel.audit.AuditRequestThreadLocal;
import com.liferay.portal.kernel.events.Action;
import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.cdph.han.service.AuditTrailLocalServiceUtil;
import org.han.portal.model.listner.util.Constants;

public class LogoutPreActionListener extends Action{

	@Override
	public void run(HttpServletRequest request, HttpServletResponse response)
			throws ActionException {
		// TODO Auto-generated method stub
		
		System.out.println("LOG OUT CALLED");
	User sessionUser=(User)	request.getAttribute("USER");
	
	System.out.println("USER====>> "+sessionUser);
	AuditRequestThreadLocal auditRequestThreadLocal =
			AuditRequestThreadLocal.getAuditThreadLocal();
	Long realUserId = auditRequestThreadLocal.getRealUserId();
	
	String realUserName=null;
	String realUserEmail=null;
	
		try {
			if(realUserId!=0){
				realUserName = PortalUtil.getUserName(realUserId, StringPool.BLANK);
				User realUser;
			realUser = UserLocalServiceUtil.getUser(realUserId);
			if(Validator.isNotNull(realUser)){
				realUserEmail=realUser.getEmailAddress();
			}
		}
		} catch (PortalException | SystemException e) {
			e.printStackTrace();
		}
		
	
	Long userId=sessionUser.getPrimaryKey();
	String userName=null;
	String userEmail=null;
	if(userId!=0){
		userName=PortalUtil.getUserName(userId, StringPool.BLANK);
		if(Validator.isNotNull(sessionUser)){
			userEmail=sessionUser.getEmailAddress();
		}
	}
	
	AuditTrailLocalServiceUtil.auditUserLogout(Constants.LOGOUT_EVENT, userId);
		
		/*Enumeration<?> e = request.getAttributeNames();
		while (e.hasMoreElements())
		{
		    String name = (String) e.nextElement();

		    // Get the value of the attribute
		    Object value = request.getAttribute(name);

		    if (value instanceof Map) {
		        for (Map.Entry<?, ?> entry : ((Map<?, ?>)value).entrySet()) {
		            System.out.println(entry.getKey() + "=" + entry.getValue());
		        }
		    } else if (value instanceof List) {
		        for (Object element : (List)value) {
		            System.out.println(element);
		        }
		    }
		}*/

		
	}

}
