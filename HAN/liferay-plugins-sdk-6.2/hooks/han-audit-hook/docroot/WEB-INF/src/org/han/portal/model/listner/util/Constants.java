package org.han.portal.model.listner.util;

public interface Constants {
	
	String LOGIN_EVENT="login";
	String LOGOUT_EVENT="USER_LOGOUT";
	String LOGIN_FAILED="FAILED_LOGIN";
	String LOGIN_SUCCESS="SUCCESSFUL_LOGIN";
	String SESSION_DESTROYED="session-destroyed";
	String LOCKOUT_EVENT="lockout";
	String LOCKOUT="USER_LOCKOUT";
	
	String LOGIN_FAILED_EVENT_FIELD="failedLoginAttempts";
	String LOCKOUT_EVENT_FIELD="lockout";
	String LOGIN_SUCCESS_EVENT_FIELD="lastLoginDate";

}

