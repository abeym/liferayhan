package org.han.portal.model.listner;

import com.liferay.portal.ModelListenerException;
import com.liferay.portal.kernel.audit.AuditRequestThreadLocal;
import com.liferay.portal.kernel.bean.BeanPropertiesUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.BaseModelListener;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;

import org.cdph.han.service.AuditTrailLocalServiceUtil;
import org.han.portal.model.listner.util.Constants;

public class UserListner extends BaseModelListener<User> {
	
	

	public void onBeforeUpdate(User newUser) throws ModelListenerException {
		try {
			User oldUser = UserLocalServiceUtil.getUser(newUser.getUserId());
			
			AuditRequestThreadLocal auditRequestThreadLocal =
					AuditRequestThreadLocal.getAuditThreadLocal();
			Long realUserId = auditRequestThreadLocal.getRealUserId();
			
			String realUserName=null;
			String realUserEmail=null;
			if(realUserId!=0){
				realUserName = PortalUtil.getUserName(realUserId, StringPool.BLANK);
				User realUser=UserLocalServiceUtil.getUser(realUserId);
				if(Validator.isNotNull(realUser)){
					realUserEmail=realUser.getEmailAddress();
				}
			}
			
			Long userId=oldUser.getPrimaryKey();
			String userName=null;
			String userEmail=null;
			if(userId!=0){
				userName=PortalUtil.getUserName(userId, StringPool.BLANK);
				User user=UserLocalServiceUtil.getUser(userId);
				if(Validator.isNotNull(user)){
					userEmail=user.getEmailAddress();
				}
			}
			
			if (checkForAuditEvent(newUser, oldUser, Constants.LOGIN_FAILED_EVENT_FIELD)){
				AuditTrailLocalServiceUtil.auditLoginFailure(Constants.LOGIN_FAILED, userId);
			} else if(checkForAuditEvent(newUser, oldUser, Constants.LOCKOUT_EVENT)){
				AuditTrailLocalServiceUtil.auditLoginFailure(Constants.LOCKOUT, userId);
			} else if(checkForAuditEvent(newUser, oldUser, Constants.LOGIN_SUCCESS_EVENT_FIELD)){
				AuditTrailLocalServiceUtil.auditLoginFailure(Constants.LOGIN_SUCCESS, userId);
			}

		}
		catch (Exception e) {
			throw new ModelListenerException(e);
		}
	}
	
	private boolean checkForAuditEvent(User newUser,User oldUser,String fieldName){
		String newValue = String.valueOf(
				BeanPropertiesUtil.getObject(newUser, fieldName));
			String oldValue = String.valueOf(
				BeanPropertiesUtil.getObject(oldUser, fieldName));

			if (!Validator.equals(newValue, oldValue)) {
				return true;
			}
			return false;
	}
}
