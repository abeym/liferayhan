/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.cdph.han.service.impl;

import com.liferay.portal.kernel.exception.SystemException;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.model.AuditTrail;
import org.cdph.han.service.AuditTrailLocalServiceUtil;
import org.cdph.han.service.base.AuditTrailLocalServiceBaseImpl;

/**
 * The implementation of the audit trail local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link org.cdph.han.service.AuditTrailLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author linzer
 * @see org.cdph.han.service.base.AuditTrailLocalServiceBaseImpl
 * @see org.cdph.han.service.AuditTrailLocalServiceUtil
 */
public class AuditTrailLocalServiceImpl extends AuditTrailLocalServiceBaseImpl {
	Log log = LogFactory.getLog(AuditTrailLocalServiceBaseImpl.class);
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link org.cdph.han.service.AuditTrailLocalServiceUtil} to access the audit trail local service.
	 */
	private static long COUNT_BUFFER = 20000l;

	public AuditTrail auditLoginFailure(String eventName, Long userId){
		return this.addAuditEntry(eventName, userId);
	}
	
	public AuditTrail auditLoginSuccess(String eventName, Long userId){
		return this.addAuditEntry(eventName, userId);
	}
	
	public AuditTrail auditUserLockout(String eventName, Long userId){
		return this.addAuditEntry(eventName, userId);
	}

	public AuditTrail auditUserLogout(String eventName, Long userId){
		return this.addAuditEntry(eventName, userId);
	}

	public AuditTrail auditSessionDestroyed(String eventName, Long userId){
		return this.addAuditEntry(eventName, userId);
	}

	private AuditTrail addAuditEntry(String eventType, Long userId){
		
		try {
			AuditTrail anAuditTrail= auditTrailPersistence.create(counterLocalService.increment(AuditTrail.class.getName()));
			anAuditTrail.setActionPerformed(eventType);
			anAuditTrail.setDatePerformed(new Date());
			anAuditTrail.setUserId(userId);

			log.debug("Insert log record: " + anAuditTrail.getId() + ", event " + eventType + ", user " + userId);
			anAuditTrail=AuditTrailLocalServiceUtil.addAuditTrail(anAuditTrail);
			return anAuditTrail;
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return null;
	}
}