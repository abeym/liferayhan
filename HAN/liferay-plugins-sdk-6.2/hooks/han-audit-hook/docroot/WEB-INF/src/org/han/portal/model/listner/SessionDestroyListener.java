package org.han.portal.model.listner;

import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.events.SessionAction;

import java.util.Enumeration;

import javax.servlet.http.HttpSession;

public class SessionDestroyListener extends SessionAction {

	@Override
	public void run(HttpSession session) throws ActionException {
		// TODO Auto-generated method stub
		
		 for (Enumeration e = session.getAttributeNames(); e.hasMoreElements(); ) {     
			    String attribName = (String) e.nextElement();
			    Object attribValue = session.getAttribute(attribName);
			    
			    System.out.println("NAME="+attribName+" VALUE="+attribValue+"");
		 }
		
	}

}
