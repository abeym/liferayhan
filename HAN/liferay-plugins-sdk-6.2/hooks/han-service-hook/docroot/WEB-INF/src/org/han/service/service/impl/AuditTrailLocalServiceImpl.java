/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.han.service.service.impl;

import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.portlet.PortletClassLoaderUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;

import java.util.Date;
import java.util.List;

import org.han.service.model.AuditTrail;
import org.han.service.service.AuditTrailLocalServiceUtil;
import org.han.service.service.base.AuditTrailLocalServiceBaseImpl;

/**
 * The implementation of the audit trail local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link org.han.service.service.AuditTrailLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Rohan
 * @see org.han.service.service.base.AuditTrailLocalServiceBaseImpl
 * @see org.han.service.service.AuditTrailLocalServiceUtil
 */
public class AuditTrailLocalServiceImpl extends AuditTrailLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link org.han.service.service.AuditTrailLocalServiceUtil} to access the audit trail local service.
	 */
	
	
	private AuditTrail addAuditEntry(String eventType, String realUserId,String realUserName,String realUserEmail,String userId,String userName,String userEmail,String className,
			String classPK, JSONObject additionalInfo){
		
		try {
			AuditTrail anAuditTrail= auditTrailPersistence.create(counterLocalService.increment(AuditTrail.class.getName()));
			anAuditTrail.setEventType(eventType);
			anAuditTrail.setRealUserId(realUserId);
			anAuditTrail.setRealUserName(realUserName);
			anAuditTrail.setRealUserEmail(realUserEmail);
			anAuditTrail.setClassName(className);
			anAuditTrail.setClassPK(classPK);
			anAuditTrail.setUserId(userId);
			anAuditTrail.setUserName(userName);
			anAuditTrail.setUserEmail(userEmail);
			
			anAuditTrail.setCreateDate(new Date());
			if(Validator.isNotNull(additionalInfo)){
				anAuditTrail.setMessage(additionalInfo.toString());
			}
			anAuditTrail=AuditTrailLocalServiceUtil.addAuditTrail(anAuditTrail);
		//	AuditTrailLocalServiceUtil.addAuditEntry(eventType, realUserId, realUserName, className, classPK, additionalInfo);
			return anAuditTrail;
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	
	public AuditTrail auditLoginFailure(String eventName, String realUserId,String realUserName,String realUserEmail,String userId,String userName,String userEmail,String className,
			String classPK, JSONObject additionalInfo){
		return this.addAuditEntry(eventName, realUserId, realUserName,realUserEmail,userId,userName,userEmail,className, classPK, additionalInfo);
	}
	
	public AuditTrail auditLoginSuccess(String eventName, String realUserId,String realUserName,String realUserEmail,String userId,String userName,String userEmail,String className,
			String classPK, JSONObject additionalInfo){
		return this.addAuditEntry(eventName, realUserId, realUserName,realUserEmail,userId,userName,userEmail,className, classPK, additionalInfo);
	}
	
	public AuditTrail auditUserLockout(String eventName, String realUserId,String realUserName,String realUserEmail,String userId,String userName,String userEmail,String className,
			String classPK, JSONObject additionalInfo){
		return this.addAuditEntry(eventName, realUserId, realUserName,realUserEmail,userId,userName,userEmail,className, classPK, additionalInfo);
	}
	
	
	public List<AuditTrail> getAuditTrailEntries(JSONObject jsonParameters,int start,int size){
		
		
			DynamicQuery auditQuery = DynamicQueryFactoryUtil.forClass(AuditTrail.class, PortletClassLoaderUtil.getClassLoader());
			Criterion criterion = null;

			
			if(Validator.isNotNull(jsonParameters)){
				
			
					if(jsonParameters.has("userEmail")){
						String email=jsonParameters.getString("userEmail");
						if(email.contains("*")){
							email=email.replace("*", StringPool.BLANK);
							email=StringPool.PERCENT+email+StringPool.PERCENT;
							criterion = RestrictionsFactoryUtil.like("userEmail",email);
						}else{
							criterion = RestrictionsFactoryUtil.eq("userEmail",email);
						}
					}
					
					
					if(jsonParameters.has("startDate") || jsonParameters.has("endDate")){
						
						Date startDate=null;
						Date endDate=null;
						
						
						if(jsonParameters.has("startDate") && jsonParameters.has("endDate") ){
							
							if(Validator.isNotNull(jsonParameters.getLong("startDate")) && Validator.isNotNull(jsonParameters.getLong("endDate")) ){
								startDate=new Date(jsonParameters.getLong("startDate"));
								endDate=new Date(jsonParameters.getLong("endDate"));
								if(Validator.isNotNull(criterion)){
									criterion=RestrictionsFactoryUtil.and(criterion,RestrictionsFactoryUtil.between("createDate", startDate, endDate));
								}else{
									criterion=RestrictionsFactoryUtil.between("createDate", startDate, endDate);
								}
							}
							
						}else if(jsonParameters.has("startDate") ){
							
							if(Validator.isNotNull(jsonParameters.getLong("startDate"))  ){
								startDate=new Date(jsonParameters.getLong("startDate"));
								
								if(Validator.isNotNull(criterion)){
									criterion=RestrictionsFactoryUtil.and(criterion,RestrictionsFactoryUtil.ge("createDate", startDate));
								}else{
									criterion=RestrictionsFactoryUtil.ge("createDate", startDate);
								}
								
							}
							
						}else if(jsonParameters.has("endDate") ){
							
							if( Validator.isNotNull(jsonParameters.getLong("endDate")) ){
								endDate=new Date(jsonParameters.getLong("endDate"));
								
								if(Validator.isNotNull(criterion)){
									criterion=RestrictionsFactoryUtil.and(criterion,RestrictionsFactoryUtil.le("createDate", endDate));
								}else{
									criterion=RestrictionsFactoryUtil.le("createDate", endDate);
								}
							}
							
						}
						
					}
					
					if(jsonParameters.has("eventType")){
						if(Validator.isNotNull(jsonParameters.getString("eventType")) &&  !jsonParameters.getString("eventType").equalsIgnoreCase("all")){
							
							if(Validator.isNotNull(criterion)){
								criterion=RestrictionsFactoryUtil.and(criterion,RestrictionsFactoryUtil.like("eventType", jsonParameters.getString("eventType")));
							}else{
								criterion=RestrictionsFactoryUtil.like("eventType", jsonParameters.getString("eventType"));
							}
						}
					}
					
					auditQuery.add(criterion);
			}
				
			
			
			try {
				int end=0;
				if(start <=0){
					start=-1;
					
				}
				
				if(size<=0){
					end=-1;
				}else{
					end=start+size;
				}
				
				List <AuditTrail> auditTrailList=AuditTrailLocalServiceUtil.dynamicQuery(auditQuery);
				return auditTrailList;
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return null;
		
	}
	
	
	public JSONArray getAuditTrailEntriesAsJson(JSONObject jsonParameters,int start,int size){
		List<AuditTrail> auditEntries=this.getAuditTrailEntries(jsonParameters, start, size);
		JSONArray aJsonArray=JSONFactoryUtil.createJSONArray();
		for(AuditTrail anAuditTrail:auditEntries){
			aJsonArray.put(convertToJson(anAuditTrail));
		}
		
		return aJsonArray;
	}
	
	public JSONObject convertToJson(AuditTrail anAuditTrail){
		
		JSONObject aJsonObject=JSONFactoryUtil.createJSONObject();
		aJsonObject.put("id", anAuditTrail.getId());
		aJsonObject.put("eventType", anAuditTrail.getEventType());
		aJsonObject.put("className", anAuditTrail.getClassName());
		aJsonObject.put("classPK", anAuditTrail.getClassPK());
		aJsonObject.put("realUserId", anAuditTrail.getRealUserId());
		aJsonObject.put("realUserName", anAuditTrail.getRealUserName());
		aJsonObject.put("realUserEmail", anAuditTrail.getRealUserEmail());
		aJsonObject.put("userId", anAuditTrail.getUserId());
		aJsonObject.put("userName", anAuditTrail.getUserName());
		aJsonObject.put("userEmail", anAuditTrail.getUserEmail());
		aJsonObject.put("createDate", anAuditTrail.getCreateDate());
		
		
		return aJsonObject;
	}
	
	
	
	
	public AuditTrail auditUserLogout(String eventName, String realUserId,String realUserName,String realUserEmail,String userId,String userName,String userEmail,String className,
			String classPK, JSONObject additionalInfo){
		return this.addAuditEntry(eventName, realUserId, realUserName,realUserEmail,userId,userName,userEmail,className, classPK, additionalInfo);
	}
	
	
	public AuditTrail auditSessionDestroyed(String eventName, String realUserId,String realUserName,String realUserEmail,String userId,String userName,String userEmail,String className,
			String classPK, JSONObject additionalInfo){
		return this.addAuditEntry(eventName, realUserId, realUserName,realUserEmail,userId,userName,userEmail,className, classPK, additionalInfo);
	}
	
	
}