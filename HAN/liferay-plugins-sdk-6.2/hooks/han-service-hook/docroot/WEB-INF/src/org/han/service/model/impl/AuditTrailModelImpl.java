/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.han.service.model.impl;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.service.ServiceContext;

import com.liferay.portlet.expando.model.ExpandoBridge;
import com.liferay.portlet.expando.util.ExpandoBridgeFactoryUtil;

import org.han.service.model.AuditTrail;
import org.han.service.model.AuditTrailModel;

import java.io.Serializable;

import java.sql.Types;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * The base model implementation for the AuditTrail service. Represents a row in the &quot;HAN_AuditTrail&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface {@link org.han.service.model.AuditTrailModel} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link AuditTrailImpl}.
 * </p>
 *
 * @author Rohan
 * @see AuditTrailImpl
 * @see org.han.service.model.AuditTrail
 * @see org.han.service.model.AuditTrailModel
 * @generated
 */
public class AuditTrailModelImpl extends BaseModelImpl<AuditTrail>
	implements AuditTrailModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a audit trail model instance should use the {@link org.han.service.model.AuditTrail} interface instead.
	 */
	public static final String TABLE_NAME = "HAN_AuditTrail";
	public static final Object[][] TABLE_COLUMNS = {
			{ "id_", Types.BIGINT },
			{ "eventType", Types.VARCHAR },
			{ "className", Types.VARCHAR },
			{ "classPK", Types.VARCHAR },
			{ "realUserId", Types.VARCHAR },
			{ "realUserName", Types.VARCHAR },
			{ "realUserEmail", Types.VARCHAR },
			{ "userId", Types.VARCHAR },
			{ "userName", Types.VARCHAR },
			{ "userEmail", Types.VARCHAR },
			{ "createDate", Types.TIMESTAMP },
			{ "message", Types.VARCHAR }
		};
	public static final String TABLE_SQL_CREATE = "create table HAN_AuditTrail (id_ LONG not null primary key,eventType VARCHAR(75) null,className VARCHAR(75) null,classPK VARCHAR(75) null,realUserId VARCHAR(75) null,realUserName VARCHAR(75) null,realUserEmail VARCHAR(75) null,userId VARCHAR(75) null,userName VARCHAR(75) null,userEmail VARCHAR(75) null,createDate DATE null,message STRING null)";
	public static final String TABLE_SQL_DROP = "drop table HAN_AuditTrail";
	public static final String ORDER_BY_JPQL = " ORDER BY auditTrail.id ASC";
	public static final String ORDER_BY_SQL = " ORDER BY HAN_AuditTrail.id_ ASC";
	public static final String DATA_SOURCE = "liferayDataSource";
	public static final String SESSION_FACTORY = "liferaySessionFactory";
	public static final String TX_MANAGER = "liferayTransactionManager";
	public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.entity.cache.enabled.org.han.service.model.AuditTrail"),
			true);
	public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.finder.cache.enabled.org.han.service.model.AuditTrail"),
			true);
	public static final boolean COLUMN_BITMASK_ENABLED = false;
	public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.util.service.ServiceProps.get(
				"lock.expiration.time.org.han.service.model.AuditTrail"));

	public AuditTrailModelImpl() {
	}

	@Override
	public long getPrimaryKey() {
		return _id;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _id;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Class<?> getModelClass() {
		return AuditTrail.class;
	}

	@Override
	public String getModelClassName() {
		return AuditTrail.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("id", getId());
		attributes.put("eventType", getEventType());
		attributes.put("className", getClassName());
		attributes.put("classPK", getClassPK());
		attributes.put("realUserId", getRealUserId());
		attributes.put("realUserName", getRealUserName());
		attributes.put("realUserEmail", getRealUserEmail());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("userEmail", getUserEmail());
		attributes.put("createDate", getCreateDate());
		attributes.put("message", getMessage());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long id = (Long)attributes.get("id");

		if (id != null) {
			setId(id);
		}

		String eventType = (String)attributes.get("eventType");

		if (eventType != null) {
			setEventType(eventType);
		}

		String className = (String)attributes.get("className");

		if (className != null) {
			setClassName(className);
		}

		String classPK = (String)attributes.get("classPK");

		if (classPK != null) {
			setClassPK(classPK);
		}

		String realUserId = (String)attributes.get("realUserId");

		if (realUserId != null) {
			setRealUserId(realUserId);
		}

		String realUserName = (String)attributes.get("realUserName");

		if (realUserName != null) {
			setRealUserName(realUserName);
		}

		String realUserEmail = (String)attributes.get("realUserEmail");

		if (realUserEmail != null) {
			setRealUserEmail(realUserEmail);
		}

		String userId = (String)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		String userEmail = (String)attributes.get("userEmail");

		if (userEmail != null) {
			setUserEmail(userEmail);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		String message = (String)attributes.get("message");

		if (message != null) {
			setMessage(message);
		}
	}

	@Override
	public long getId() {
		return _id;
	}

	@Override
	public void setId(long id) {
		_id = id;
	}

	@Override
	public String getEventType() {
		if (_eventType == null) {
			return StringPool.BLANK;
		}
		else {
			return _eventType;
		}
	}

	@Override
	public void setEventType(String eventType) {
		_eventType = eventType;
	}

	@Override
	public String getClassName() {
		if (_className == null) {
			return StringPool.BLANK;
		}
		else {
			return _className;
		}
	}

	@Override
	public void setClassName(String className) {
		_className = className;
	}

	@Override
	public String getClassPK() {
		if (_classPK == null) {
			return StringPool.BLANK;
		}
		else {
			return _classPK;
		}
	}

	@Override
	public void setClassPK(String classPK) {
		_classPK = classPK;
	}

	@Override
	public String getRealUserId() {
		if (_realUserId == null) {
			return StringPool.BLANK;
		}
		else {
			return _realUserId;
		}
	}

	@Override
	public void setRealUserId(String realUserId) {
		_realUserId = realUserId;
	}

	@Override
	public String getRealUserName() {
		if (_realUserName == null) {
			return StringPool.BLANK;
		}
		else {
			return _realUserName;
		}
	}

	@Override
	public void setRealUserName(String realUserName) {
		_realUserName = realUserName;
	}

	@Override
	public String getRealUserEmail() {
		if (_realUserEmail == null) {
			return StringPool.BLANK;
		}
		else {
			return _realUserEmail;
		}
	}

	@Override
	public void setRealUserEmail(String realUserEmail) {
		_realUserEmail = realUserEmail;
	}

	@Override
	public String getUserId() {
		if (_userId == null) {
			return StringPool.BLANK;
		}
		else {
			return _userId;
		}
	}

	@Override
	public void setUserId(String userId) {
		_userId = userId;
	}

	@Override
	public String getUserName() {
		if (_userName == null) {
			return StringPool.BLANK;
		}
		else {
			return _userName;
		}
	}

	@Override
	public void setUserName(String userName) {
		_userName = userName;
	}

	@Override
	public String getUserEmail() {
		if (_userEmail == null) {
			return StringPool.BLANK;
		}
		else {
			return _userEmail;
		}
	}

	@Override
	public void setUserEmail(String userEmail) {
		_userEmail = userEmail;
	}

	@Override
	public Date getCreateDate() {
		return _createDate;
	}

	@Override
	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	@Override
	public String getMessage() {
		if (_message == null) {
			return StringPool.BLANK;
		}
		else {
			return _message;
		}
	}

	@Override
	public void setMessage(String message) {
		_message = message;
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return ExpandoBridgeFactoryUtil.getExpandoBridge(0,
			AuditTrail.class.getName(), getPrimaryKey());
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		ExpandoBridge expandoBridge = getExpandoBridge();

		expandoBridge.setAttributes(serviceContext);
	}

	@Override
	public AuditTrail toEscapedModel() {
		if (_escapedModel == null) {
			_escapedModel = (AuditTrail)ProxyUtil.newProxyInstance(_classLoader,
					_escapedModelInterfaces, new AutoEscapeBeanHandler(this));
		}

		return _escapedModel;
	}

	@Override
	public Object clone() {
		AuditTrailImpl auditTrailImpl = new AuditTrailImpl();

		auditTrailImpl.setId(getId());
		auditTrailImpl.setEventType(getEventType());
		auditTrailImpl.setClassName(getClassName());
		auditTrailImpl.setClassPK(getClassPK());
		auditTrailImpl.setRealUserId(getRealUserId());
		auditTrailImpl.setRealUserName(getRealUserName());
		auditTrailImpl.setRealUserEmail(getRealUserEmail());
		auditTrailImpl.setUserId(getUserId());
		auditTrailImpl.setUserName(getUserName());
		auditTrailImpl.setUserEmail(getUserEmail());
		auditTrailImpl.setCreateDate(getCreateDate());
		auditTrailImpl.setMessage(getMessage());

		auditTrailImpl.resetOriginalValues();

		return auditTrailImpl;
	}

	@Override
	public int compareTo(AuditTrail auditTrail) {
		int value = 0;

		if (getId() < auditTrail.getId()) {
			value = -1;
		}
		else if (getId() > auditTrail.getId()) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof AuditTrail)) {
			return false;
		}

		AuditTrail auditTrail = (AuditTrail)obj;

		long primaryKey = auditTrail.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public void resetOriginalValues() {
	}

	@Override
	public CacheModel<AuditTrail> toCacheModel() {
		AuditTrailCacheModel auditTrailCacheModel = new AuditTrailCacheModel();

		auditTrailCacheModel.id = getId();

		auditTrailCacheModel.eventType = getEventType();

		String eventType = auditTrailCacheModel.eventType;

		if ((eventType != null) && (eventType.length() == 0)) {
			auditTrailCacheModel.eventType = null;
		}

		auditTrailCacheModel.className = getClassName();

		String className = auditTrailCacheModel.className;

		if ((className != null) && (className.length() == 0)) {
			auditTrailCacheModel.className = null;
		}

		auditTrailCacheModel.classPK = getClassPK();

		String classPK = auditTrailCacheModel.classPK;

		if ((classPK != null) && (classPK.length() == 0)) {
			auditTrailCacheModel.classPK = null;
		}

		auditTrailCacheModel.realUserId = getRealUserId();

		String realUserId = auditTrailCacheModel.realUserId;

		if ((realUserId != null) && (realUserId.length() == 0)) {
			auditTrailCacheModel.realUserId = null;
		}

		auditTrailCacheModel.realUserName = getRealUserName();

		String realUserName = auditTrailCacheModel.realUserName;

		if ((realUserName != null) && (realUserName.length() == 0)) {
			auditTrailCacheModel.realUserName = null;
		}

		auditTrailCacheModel.realUserEmail = getRealUserEmail();

		String realUserEmail = auditTrailCacheModel.realUserEmail;

		if ((realUserEmail != null) && (realUserEmail.length() == 0)) {
			auditTrailCacheModel.realUserEmail = null;
		}

		auditTrailCacheModel.userId = getUserId();

		String userId = auditTrailCacheModel.userId;

		if ((userId != null) && (userId.length() == 0)) {
			auditTrailCacheModel.userId = null;
		}

		auditTrailCacheModel.userName = getUserName();

		String userName = auditTrailCacheModel.userName;

		if ((userName != null) && (userName.length() == 0)) {
			auditTrailCacheModel.userName = null;
		}

		auditTrailCacheModel.userEmail = getUserEmail();

		String userEmail = auditTrailCacheModel.userEmail;

		if ((userEmail != null) && (userEmail.length() == 0)) {
			auditTrailCacheModel.userEmail = null;
		}

		Date createDate = getCreateDate();

		if (createDate != null) {
			auditTrailCacheModel.createDate = createDate.getTime();
		}
		else {
			auditTrailCacheModel.createDate = Long.MIN_VALUE;
		}

		auditTrailCacheModel.message = getMessage();

		String message = auditTrailCacheModel.message;

		if ((message != null) && (message.length() == 0)) {
			auditTrailCacheModel.message = null;
		}

		return auditTrailCacheModel;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(25);

		sb.append("{id=");
		sb.append(getId());
		sb.append(", eventType=");
		sb.append(getEventType());
		sb.append(", className=");
		sb.append(getClassName());
		sb.append(", classPK=");
		sb.append(getClassPK());
		sb.append(", realUserId=");
		sb.append(getRealUserId());
		sb.append(", realUserName=");
		sb.append(getRealUserName());
		sb.append(", realUserEmail=");
		sb.append(getRealUserEmail());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", userName=");
		sb.append(getUserName());
		sb.append(", userEmail=");
		sb.append(getUserEmail());
		sb.append(", createDate=");
		sb.append(getCreateDate());
		sb.append(", message=");
		sb.append(getMessage());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(40);

		sb.append("<model><model-name>");
		sb.append("org.han.service.model.AuditTrail");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>id</column-name><column-value><![CDATA[");
		sb.append(getId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>eventType</column-name><column-value><![CDATA[");
		sb.append(getEventType());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>className</column-name><column-value><![CDATA[");
		sb.append(getClassName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>classPK</column-name><column-value><![CDATA[");
		sb.append(getClassPK());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>realUserId</column-name><column-value><![CDATA[");
		sb.append(getRealUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>realUserName</column-name><column-value><![CDATA[");
		sb.append(getRealUserName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>realUserEmail</column-name><column-value><![CDATA[");
		sb.append(getRealUserEmail());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userName</column-name><column-value><![CDATA[");
		sb.append(getUserName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userEmail</column-name><column-value><![CDATA[");
		sb.append(getUserEmail());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>createDate</column-name><column-value><![CDATA[");
		sb.append(getCreateDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>message</column-name><column-value><![CDATA[");
		sb.append(getMessage());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private static ClassLoader _classLoader = AuditTrail.class.getClassLoader();
	private static Class<?>[] _escapedModelInterfaces = new Class[] {
			AuditTrail.class
		};
	private long _id;
	private String _eventType;
	private String _className;
	private String _classPK;
	private String _realUserId;
	private String _realUserName;
	private String _realUserEmail;
	private String _userId;
	private String _userName;
	private String _userEmail;
	private Date _createDate;
	private String _message;
	private AuditTrail _escapedModel;
}