/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.han.service.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import org.han.service.model.MigrationSummary;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing MigrationSummary in entity cache.
 *
 * @author Rohan
 * @see MigrationSummary
 * @generated
 */
public class MigrationSummaryCacheModel implements CacheModel<MigrationSummary>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append("{id=");
		sb.append(id);
		sb.append(", startTime=");
		sb.append(startTime);
		sb.append(", endTime=");
		sb.append(endTime);
		sb.append(", TableName=");
		sb.append(TableName);
		sb.append(", added=");
		sb.append(added);
		sb.append(", skipped=");
		sb.append(skipped);
		sb.append(", totalFound=");
		sb.append(totalFound);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public MigrationSummary toEntityModel() {
		MigrationSummaryImpl migrationSummaryImpl = new MigrationSummaryImpl();

		migrationSummaryImpl.setId(id);

		if (startTime == Long.MIN_VALUE) {
			migrationSummaryImpl.setStartTime(null);
		}
		else {
			migrationSummaryImpl.setStartTime(new Date(startTime));
		}

		if (endTime == Long.MIN_VALUE) {
			migrationSummaryImpl.setEndTime(null);
		}
		else {
			migrationSummaryImpl.setEndTime(new Date(endTime));
		}

		if (TableName == null) {
			migrationSummaryImpl.setTableName(StringPool.BLANK);
		}
		else {
			migrationSummaryImpl.setTableName(TableName);
		}

		migrationSummaryImpl.setAdded(added);
		migrationSummaryImpl.setSkipped(skipped);
		migrationSummaryImpl.setTotalFound(totalFound);

		migrationSummaryImpl.resetOriginalValues();

		return migrationSummaryImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		id = objectInput.readLong();
		startTime = objectInput.readLong();
		endTime = objectInput.readLong();
		TableName = objectInput.readUTF();
		added = objectInput.readLong();
		skipped = objectInput.readLong();
		totalFound = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(id);
		objectOutput.writeLong(startTime);
		objectOutput.writeLong(endTime);

		if (TableName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(TableName);
		}

		objectOutput.writeLong(added);
		objectOutput.writeLong(skipped);
		objectOutput.writeLong(totalFound);
	}

	public long id;
	public long startTime;
	public long endTime;
	public String TableName;
	public long added;
	public long skipped;
	public long totalFound;
}