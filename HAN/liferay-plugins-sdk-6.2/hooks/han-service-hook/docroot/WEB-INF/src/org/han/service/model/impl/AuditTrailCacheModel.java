/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.han.service.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import org.han.service.model.AuditTrail;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing AuditTrail in entity cache.
 *
 * @author Rohan
 * @see AuditTrail
 * @generated
 */
public class AuditTrailCacheModel implements CacheModel<AuditTrail>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(25);

		sb.append("{id=");
		sb.append(id);
		sb.append(", eventType=");
		sb.append(eventType);
		sb.append(", className=");
		sb.append(className);
		sb.append(", classPK=");
		sb.append(classPK);
		sb.append(", realUserId=");
		sb.append(realUserId);
		sb.append(", realUserName=");
		sb.append(realUserName);
		sb.append(", realUserEmail=");
		sb.append(realUserEmail);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", userEmail=");
		sb.append(userEmail);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", message=");
		sb.append(message);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public AuditTrail toEntityModel() {
		AuditTrailImpl auditTrailImpl = new AuditTrailImpl();

		auditTrailImpl.setId(id);

		if (eventType == null) {
			auditTrailImpl.setEventType(StringPool.BLANK);
		}
		else {
			auditTrailImpl.setEventType(eventType);
		}

		if (className == null) {
			auditTrailImpl.setClassName(StringPool.BLANK);
		}
		else {
			auditTrailImpl.setClassName(className);
		}

		if (classPK == null) {
			auditTrailImpl.setClassPK(StringPool.BLANK);
		}
		else {
			auditTrailImpl.setClassPK(classPK);
		}

		if (realUserId == null) {
			auditTrailImpl.setRealUserId(StringPool.BLANK);
		}
		else {
			auditTrailImpl.setRealUserId(realUserId);
		}

		if (realUserName == null) {
			auditTrailImpl.setRealUserName(StringPool.BLANK);
		}
		else {
			auditTrailImpl.setRealUserName(realUserName);
		}

		if (realUserEmail == null) {
			auditTrailImpl.setRealUserEmail(StringPool.BLANK);
		}
		else {
			auditTrailImpl.setRealUserEmail(realUserEmail);
		}

		if (userId == null) {
			auditTrailImpl.setUserId(StringPool.BLANK);
		}
		else {
			auditTrailImpl.setUserId(userId);
		}

		if (userName == null) {
			auditTrailImpl.setUserName(StringPool.BLANK);
		}
		else {
			auditTrailImpl.setUserName(userName);
		}

		if (userEmail == null) {
			auditTrailImpl.setUserEmail(StringPool.BLANK);
		}
		else {
			auditTrailImpl.setUserEmail(userEmail);
		}

		if (createDate == Long.MIN_VALUE) {
			auditTrailImpl.setCreateDate(null);
		}
		else {
			auditTrailImpl.setCreateDate(new Date(createDate));
		}

		if (message == null) {
			auditTrailImpl.setMessage(StringPool.BLANK);
		}
		else {
			auditTrailImpl.setMessage(message);
		}

		auditTrailImpl.resetOriginalValues();

		return auditTrailImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		id = objectInput.readLong();
		eventType = objectInput.readUTF();
		className = objectInput.readUTF();
		classPK = objectInput.readUTF();
		realUserId = objectInput.readUTF();
		realUserName = objectInput.readUTF();
		realUserEmail = objectInput.readUTF();
		userId = objectInput.readUTF();
		userName = objectInput.readUTF();
		userEmail = objectInput.readUTF();
		createDate = objectInput.readLong();
		message = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(id);

		if (eventType == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(eventType);
		}

		if (className == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(className);
		}

		if (classPK == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(classPK);
		}

		if (realUserId == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(realUserId);
		}

		if (realUserName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(realUserName);
		}

		if (realUserEmail == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(realUserEmail);
		}

		if (userId == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userId);
		}

		if (userName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userName);
		}

		if (userEmail == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userEmail);
		}

		objectOutput.writeLong(createDate);

		if (message == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(message);
		}
	}

	public long id;
	public String eventType;
	public String className;
	public String classPK;
	public String realUserId;
	public String realUserName;
	public String realUserEmail;
	public String userId;
	public String userName;
	public String userEmail;
	public long createDate;
	public String message;
}