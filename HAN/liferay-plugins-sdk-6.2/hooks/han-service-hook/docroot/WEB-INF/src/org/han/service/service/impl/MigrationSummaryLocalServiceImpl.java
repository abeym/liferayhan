/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.han.service.service.impl;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.Validator;

import java.util.Date;
import java.util.List;

import org.han.service.model.MigrationSummary;
import org.han.service.service.MigrationSummaryLocalServiceUtil;
import org.han.service.service.base.MigrationSummaryLocalServiceBaseImpl;

/**
 * The implementation of the migration summary local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link org.han.service.service.MigrationSummaryLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Rohan
 * @see org.han.service.service.base.MigrationSummaryLocalServiceBaseImpl
 * @see org.han.service.service.MigrationSummaryLocalServiceUtil
 */
public class MigrationSummaryLocalServiceImpl
	extends MigrationSummaryLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link org.han.service.service.MigrationSummaryLocalServiceUtil} to access the migration summary local service.
	 */
	
	
	public MigrationSummary FindByTableName(String tableName){
		
		try {
			List<MigrationSummary> tableEntries =migrationSummaryPersistence.findBytableName(tableName);
			
			if(Validator.isNull(tableEntries)|| tableEntries.size()==0){
				return null;
			}
			
			return tableEntries.get(0);
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
		
	}
	

	public MigrationSummary addEntry(String tableName,Date startDateTime,Date endDateTime,Integer skipped,Integer added,Integer totalFound){
		
		try {
			List<MigrationSummary> tableEntries =migrationSummaryPersistence.findBytableName(tableName);
			
			if(tableEntries.size()>0){
				for(MigrationSummary migrationSummary:tableEntries){
						MigrationSummaryLocalServiceUtil.deleteMigrationSummary(migrationSummary);
				}
			}

				MigrationSummary	migrationSummary=migrationSummaryLocalService.createMigrationSummary(counterLocalService.increment(MigrationSummary.class.getName()));
					
				migrationSummary.setTableName(tableName);
				if(Validator.isNotNull(startDateTime)){
					migrationSummary.setStartTime(startDateTime);
				}
				
				if(Validator.isNotNull(endDateTime)){
					migrationSummary.setEndTime(endDateTime);
				}
				if(Validator.isNotNull(skipped)){
					migrationSummary.setSkipped(skipped);
				}
				
				if(Validator.isNotNull(added)){
					migrationSummary.setAdded(added);
				}
				if(Validator.isNotNull(totalFound)){
					migrationSummary.setTotalFound(totalFound);
				}
				migrationSummary=migrationSummaryLocalService.addMigrationSummary(migrationSummary);
			
			
			
			return migrationSummary;
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	public MigrationSummary updateEntry(MigrationSummary	migrationSummary,Date endDateTime,Integer skipped,Integer added){
		
		try {

			
				
				if(Validator.isNotNull(endDateTime)){
					migrationSummary.setEndTime(endDateTime);
				}
				if(Validator.isNotNull(skipped)){
					migrationSummary.setSkipped(skipped);
				}
				
				if(Validator.isNotNull(added)){
					migrationSummary.setAdded(added);
				}
				
				migrationSummary=migrationSummaryLocalService.updateMigrationSummary(migrationSummary);
			
			
			
			return migrationSummary;
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
		
	} 
	
}