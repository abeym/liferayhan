/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.han.service.service.impl;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.Validator;

import java.util.List;

import org.han.service.model.OldNewMapping;
import org.han.service.model.SkippedEntries;
import org.han.service.service.OldNewMappingLocalServiceUtil;
import org.han.service.service.SkippedEntriesLocalServiceUtil;
import org.han.service.service.base.SkippedEntriesLocalServiceBaseImpl;

/**
 * The implementation of the skipped entries local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link org.han.service.service.SkippedEntriesLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Rohan
 * @see org.han.service.service.base.SkippedEntriesLocalServiceBaseImpl
 * @see org.han.service.service.SkippedEntriesLocalServiceUtil
 */
public class SkippedEntriesLocalServiceImpl
	extends SkippedEntriesLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link org.han.service.service.SkippedEntriesLocalServiceUtil} to access the skipped entries local service.
	 */
	
	

	public SkippedEntries create(long oldPK,long newPK,String table){
		
		try {
			long id = counterLocalService.increment(OldNewMapping.class.getName());
			SkippedEntries aRecord=skippedEntriesLocalService.createSkippedEntries(id);
			aRecord.setOldPK(oldPK);
			aRecord.setNewPK(newPK);
			
			aRecord.setTableName(table.toLowerCase());
			aRecord=	skippedEntriesLocalService.addSkippedEntries(aRecord);
			return aRecord;
			
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	public JSONObject getJSON(SkippedEntries aMapping){
		
		if(Validator.isNotNull(aMapping)){
			JSONObject jsonObject=JSONFactoryUtil.createJSONObject();
			jsonObject.put("id", aMapping.getId());
			jsonObject.put("oldPK", aMapping.getOldPK());
			jsonObject.put("newPK", aMapping.getNewPK());
			jsonObject.put("Table", aMapping.getTableName());
			return jsonObject;
		}
		
		return null;
		
	}
	
public SkippedEntries findByTableNameAndOldPK(String entityName,long oldPk){
		
		try {
			List<SkippedEntries>  mappingList=skippedEntriesPersistence.findByentityNameAndOldPK(entityName, oldPk);
			if(Validator.isNotNull(mappingList)&& mappingList.size()!=0){
				return mappingList.get(mappingList.size()-1);
			}
			
			
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}