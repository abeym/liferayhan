/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.han.service.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import org.han.service.NoSuchOldNewMappingException;
import org.han.service.model.OldNewMapping;
import org.han.service.model.impl.OldNewMappingImpl;
import org.han.service.model.impl.OldNewMappingModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the old new mapping service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Rohan
 * @see OldNewMappingPersistence
 * @see OldNewMappingUtil
 * @generated
 */
public class OldNewMappingPersistenceImpl extends BasePersistenceImpl<OldNewMapping>
	implements OldNewMappingPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link OldNewMappingUtil} to access the old new mapping persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = OldNewMappingImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(OldNewMappingModelImpl.ENTITY_CACHE_ENABLED,
			OldNewMappingModelImpl.FINDER_CACHE_ENABLED,
			OldNewMappingImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(OldNewMappingModelImpl.ENTITY_CACHE_ENABLED,
			OldNewMappingModelImpl.FINDER_CACHE_ENABLED,
			OldNewMappingImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(OldNewMappingModelImpl.ENTITY_CACHE_ENABLED,
			OldNewMappingModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ENTITYNAMEANDOLDPK =
		new FinderPath(OldNewMappingModelImpl.ENTITY_CACHE_ENABLED,
			OldNewMappingModelImpl.FINDER_CACHE_ENABLED,
			OldNewMappingImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByentityNameAndOldPK",
			new String[] {
				String.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ENTITYNAMEANDOLDPK =
		new FinderPath(OldNewMappingModelImpl.ENTITY_CACHE_ENABLED,
			OldNewMappingModelImpl.FINDER_CACHE_ENABLED,
			OldNewMappingImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByentityNameAndOldPK",
			new String[] { String.class.getName(), Long.class.getName() },
			OldNewMappingModelImpl.TABLENAME_COLUMN_BITMASK |
			OldNewMappingModelImpl.OLDPK_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ENTITYNAMEANDOLDPK = new FinderPath(OldNewMappingModelImpl.ENTITY_CACHE_ENABLED,
			OldNewMappingModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByentityNameAndOldPK",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns all the old new mappings where TableName = &#63; and oldPK = &#63;.
	 *
	 * @param TableName the table name
	 * @param oldPK the old p k
	 * @return the matching old new mappings
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<OldNewMapping> findByentityNameAndOldPK(String TableName,
		long oldPK) throws SystemException {
		return findByentityNameAndOldPK(TableName, oldPK, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the old new mappings where TableName = &#63; and oldPK = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.OldNewMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param TableName the table name
	 * @param oldPK the old p k
	 * @param start the lower bound of the range of old new mappings
	 * @param end the upper bound of the range of old new mappings (not inclusive)
	 * @return the range of matching old new mappings
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<OldNewMapping> findByentityNameAndOldPK(String TableName,
		long oldPK, int start, int end) throws SystemException {
		return findByentityNameAndOldPK(TableName, oldPK, start, end, null);
	}

	/**
	 * Returns an ordered range of all the old new mappings where TableName = &#63; and oldPK = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.OldNewMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param TableName the table name
	 * @param oldPK the old p k
	 * @param start the lower bound of the range of old new mappings
	 * @param end the upper bound of the range of old new mappings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching old new mappings
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<OldNewMapping> findByentityNameAndOldPK(String TableName,
		long oldPK, int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ENTITYNAMEANDOLDPK;
			finderArgs = new Object[] { TableName, oldPK };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ENTITYNAMEANDOLDPK;
			finderArgs = new Object[] {
					TableName, oldPK,
					
					start, end, orderByComparator
				};
		}

		List<OldNewMapping> list = (List<OldNewMapping>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (OldNewMapping oldNewMapping : list) {
				if (!Validator.equals(TableName, oldNewMapping.getTableName()) ||
						(oldPK != oldNewMapping.getOldPK())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_OLDNEWMAPPING_WHERE);

			boolean bindTableName = false;

			if (TableName == null) {
				query.append(_FINDER_COLUMN_ENTITYNAMEANDOLDPK_TABLENAME_1);
			}
			else if (TableName.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ENTITYNAMEANDOLDPK_TABLENAME_3);
			}
			else {
				bindTableName = true;

				query.append(_FINDER_COLUMN_ENTITYNAMEANDOLDPK_TABLENAME_2);
			}

			query.append(_FINDER_COLUMN_ENTITYNAMEANDOLDPK_OLDPK_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(OldNewMappingModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindTableName) {
					qPos.add(TableName);
				}

				qPos.add(oldPK);

				if (!pagination) {
					list = (List<OldNewMapping>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<OldNewMapping>(list);
				}
				else {
					list = (List<OldNewMapping>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first old new mapping in the ordered set where TableName = &#63; and oldPK = &#63;.
	 *
	 * @param TableName the table name
	 * @param oldPK the old p k
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching old new mapping
	 * @throws org.han.service.NoSuchOldNewMappingException if a matching old new mapping could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OldNewMapping findByentityNameAndOldPK_First(String TableName,
		long oldPK, OrderByComparator orderByComparator)
		throws NoSuchOldNewMappingException, SystemException {
		OldNewMapping oldNewMapping = fetchByentityNameAndOldPK_First(TableName,
				oldPK, orderByComparator);

		if (oldNewMapping != null) {
			return oldNewMapping;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("TableName=");
		msg.append(TableName);

		msg.append(", oldPK=");
		msg.append(oldPK);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOldNewMappingException(msg.toString());
	}

	/**
	 * Returns the first old new mapping in the ordered set where TableName = &#63; and oldPK = &#63;.
	 *
	 * @param TableName the table name
	 * @param oldPK the old p k
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching old new mapping, or <code>null</code> if a matching old new mapping could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OldNewMapping fetchByentityNameAndOldPK_First(String TableName,
		long oldPK, OrderByComparator orderByComparator)
		throws SystemException {
		List<OldNewMapping> list = findByentityNameAndOldPK(TableName, oldPK,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last old new mapping in the ordered set where TableName = &#63; and oldPK = &#63;.
	 *
	 * @param TableName the table name
	 * @param oldPK the old p k
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching old new mapping
	 * @throws org.han.service.NoSuchOldNewMappingException if a matching old new mapping could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OldNewMapping findByentityNameAndOldPK_Last(String TableName,
		long oldPK, OrderByComparator orderByComparator)
		throws NoSuchOldNewMappingException, SystemException {
		OldNewMapping oldNewMapping = fetchByentityNameAndOldPK_Last(TableName,
				oldPK, orderByComparator);

		if (oldNewMapping != null) {
			return oldNewMapping;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("TableName=");
		msg.append(TableName);

		msg.append(", oldPK=");
		msg.append(oldPK);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOldNewMappingException(msg.toString());
	}

	/**
	 * Returns the last old new mapping in the ordered set where TableName = &#63; and oldPK = &#63;.
	 *
	 * @param TableName the table name
	 * @param oldPK the old p k
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching old new mapping, or <code>null</code> if a matching old new mapping could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OldNewMapping fetchByentityNameAndOldPK_Last(String TableName,
		long oldPK, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByentityNameAndOldPK(TableName, oldPK);

		if (count == 0) {
			return null;
		}

		List<OldNewMapping> list = findByentityNameAndOldPK(TableName, oldPK,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the old new mappings before and after the current old new mapping in the ordered set where TableName = &#63; and oldPK = &#63;.
	 *
	 * @param id the primary key of the current old new mapping
	 * @param TableName the table name
	 * @param oldPK the old p k
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next old new mapping
	 * @throws org.han.service.NoSuchOldNewMappingException if a old new mapping with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OldNewMapping[] findByentityNameAndOldPK_PrevAndNext(long id,
		String TableName, long oldPK, OrderByComparator orderByComparator)
		throws NoSuchOldNewMappingException, SystemException {
		OldNewMapping oldNewMapping = findByPrimaryKey(id);

		Session session = null;

		try {
			session = openSession();

			OldNewMapping[] array = new OldNewMappingImpl[3];

			array[0] = getByentityNameAndOldPK_PrevAndNext(session,
					oldNewMapping, TableName, oldPK, orderByComparator, true);

			array[1] = oldNewMapping;

			array[2] = getByentityNameAndOldPK_PrevAndNext(session,
					oldNewMapping, TableName, oldPK, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected OldNewMapping getByentityNameAndOldPK_PrevAndNext(
		Session session, OldNewMapping oldNewMapping, String TableName,
		long oldPK, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_OLDNEWMAPPING_WHERE);

		boolean bindTableName = false;

		if (TableName == null) {
			query.append(_FINDER_COLUMN_ENTITYNAMEANDOLDPK_TABLENAME_1);
		}
		else if (TableName.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_ENTITYNAMEANDOLDPK_TABLENAME_3);
		}
		else {
			bindTableName = true;

			query.append(_FINDER_COLUMN_ENTITYNAMEANDOLDPK_TABLENAME_2);
		}

		query.append(_FINDER_COLUMN_ENTITYNAMEANDOLDPK_OLDPK_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(OldNewMappingModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindTableName) {
			qPos.add(TableName);
		}

		qPos.add(oldPK);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(oldNewMapping);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<OldNewMapping> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the old new mappings where TableName = &#63; and oldPK = &#63; from the database.
	 *
	 * @param TableName the table name
	 * @param oldPK the old p k
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByentityNameAndOldPK(String TableName, long oldPK)
		throws SystemException {
		for (OldNewMapping oldNewMapping : findByentityNameAndOldPK(TableName,
				oldPK, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(oldNewMapping);
		}
	}

	/**
	 * Returns the number of old new mappings where TableName = &#63; and oldPK = &#63;.
	 *
	 * @param TableName the table name
	 * @param oldPK the old p k
	 * @return the number of matching old new mappings
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByentityNameAndOldPK(String TableName, long oldPK)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_ENTITYNAMEANDOLDPK;

		Object[] finderArgs = new Object[] { TableName, oldPK };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_OLDNEWMAPPING_WHERE);

			boolean bindTableName = false;

			if (TableName == null) {
				query.append(_FINDER_COLUMN_ENTITYNAMEANDOLDPK_TABLENAME_1);
			}
			else if (TableName.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ENTITYNAMEANDOLDPK_TABLENAME_3);
			}
			else {
				bindTableName = true;

				query.append(_FINDER_COLUMN_ENTITYNAMEANDOLDPK_TABLENAME_2);
			}

			query.append(_FINDER_COLUMN_ENTITYNAMEANDOLDPK_OLDPK_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindTableName) {
					qPos.add(TableName);
				}

				qPos.add(oldPK);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ENTITYNAMEANDOLDPK_TABLENAME_1 = "oldNewMapping.TableName IS NULL AND ";
	private static final String _FINDER_COLUMN_ENTITYNAMEANDOLDPK_TABLENAME_2 = "oldNewMapping.TableName = ? AND ";
	private static final String _FINDER_COLUMN_ENTITYNAMEANDOLDPK_TABLENAME_3 = "(oldNewMapping.TableName IS NULL OR oldNewMapping.TableName = '') AND ";
	private static final String _FINDER_COLUMN_ENTITYNAMEANDOLDPK_OLDPK_2 = "oldNewMapping.oldPK = ?";

	public OldNewMappingPersistenceImpl() {
		setModelClass(OldNewMapping.class);
	}

	/**
	 * Caches the old new mapping in the entity cache if it is enabled.
	 *
	 * @param oldNewMapping the old new mapping
	 */
	@Override
	public void cacheResult(OldNewMapping oldNewMapping) {
		EntityCacheUtil.putResult(OldNewMappingModelImpl.ENTITY_CACHE_ENABLED,
			OldNewMappingImpl.class, oldNewMapping.getPrimaryKey(),
			oldNewMapping);

		oldNewMapping.resetOriginalValues();
	}

	/**
	 * Caches the old new mappings in the entity cache if it is enabled.
	 *
	 * @param oldNewMappings the old new mappings
	 */
	@Override
	public void cacheResult(List<OldNewMapping> oldNewMappings) {
		for (OldNewMapping oldNewMapping : oldNewMappings) {
			if (EntityCacheUtil.getResult(
						OldNewMappingModelImpl.ENTITY_CACHE_ENABLED,
						OldNewMappingImpl.class, oldNewMapping.getPrimaryKey()) == null) {
				cacheResult(oldNewMapping);
			}
			else {
				oldNewMapping.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all old new mappings.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(OldNewMappingImpl.class.getName());
		}

		EntityCacheUtil.clearCache(OldNewMappingImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the old new mapping.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(OldNewMapping oldNewMapping) {
		EntityCacheUtil.removeResult(OldNewMappingModelImpl.ENTITY_CACHE_ENABLED,
			OldNewMappingImpl.class, oldNewMapping.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<OldNewMapping> oldNewMappings) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (OldNewMapping oldNewMapping : oldNewMappings) {
			EntityCacheUtil.removeResult(OldNewMappingModelImpl.ENTITY_CACHE_ENABLED,
				OldNewMappingImpl.class, oldNewMapping.getPrimaryKey());
		}
	}

	/**
	 * Creates a new old new mapping with the primary key. Does not add the old new mapping to the database.
	 *
	 * @param id the primary key for the new old new mapping
	 * @return the new old new mapping
	 */
	@Override
	public OldNewMapping create(long id) {
		OldNewMapping oldNewMapping = new OldNewMappingImpl();

		oldNewMapping.setNew(true);
		oldNewMapping.setPrimaryKey(id);

		return oldNewMapping;
	}

	/**
	 * Removes the old new mapping with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param id the primary key of the old new mapping
	 * @return the old new mapping that was removed
	 * @throws org.han.service.NoSuchOldNewMappingException if a old new mapping with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OldNewMapping remove(long id)
		throws NoSuchOldNewMappingException, SystemException {
		return remove((Serializable)id);
	}

	/**
	 * Removes the old new mapping with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the old new mapping
	 * @return the old new mapping that was removed
	 * @throws org.han.service.NoSuchOldNewMappingException if a old new mapping with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OldNewMapping remove(Serializable primaryKey)
		throws NoSuchOldNewMappingException, SystemException {
		Session session = null;

		try {
			session = openSession();

			OldNewMapping oldNewMapping = (OldNewMapping)session.get(OldNewMappingImpl.class,
					primaryKey);

			if (oldNewMapping == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchOldNewMappingException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(oldNewMapping);
		}
		catch (NoSuchOldNewMappingException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected OldNewMapping removeImpl(OldNewMapping oldNewMapping)
		throws SystemException {
		oldNewMapping = toUnwrappedModel(oldNewMapping);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(oldNewMapping)) {
				oldNewMapping = (OldNewMapping)session.get(OldNewMappingImpl.class,
						oldNewMapping.getPrimaryKeyObj());
			}

			if (oldNewMapping != null) {
				session.delete(oldNewMapping);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (oldNewMapping != null) {
			clearCache(oldNewMapping);
		}

		return oldNewMapping;
	}

	@Override
	public OldNewMapping updateImpl(
		org.han.service.model.OldNewMapping oldNewMapping)
		throws SystemException {
		oldNewMapping = toUnwrappedModel(oldNewMapping);

		boolean isNew = oldNewMapping.isNew();

		OldNewMappingModelImpl oldNewMappingModelImpl = (OldNewMappingModelImpl)oldNewMapping;

		Session session = null;

		try {
			session = openSession();

			if (oldNewMapping.isNew()) {
				session.save(oldNewMapping);

				oldNewMapping.setNew(false);
			}
			else {
				session.merge(oldNewMapping);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !OldNewMappingModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((oldNewMappingModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ENTITYNAMEANDOLDPK.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						oldNewMappingModelImpl.getOriginalTableName(),
						oldNewMappingModelImpl.getOriginalOldPK()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ENTITYNAMEANDOLDPK,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ENTITYNAMEANDOLDPK,
					args);

				args = new Object[] {
						oldNewMappingModelImpl.getTableName(),
						oldNewMappingModelImpl.getOldPK()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ENTITYNAMEANDOLDPK,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ENTITYNAMEANDOLDPK,
					args);
			}
		}

		EntityCacheUtil.putResult(OldNewMappingModelImpl.ENTITY_CACHE_ENABLED,
			OldNewMappingImpl.class, oldNewMapping.getPrimaryKey(),
			oldNewMapping);

		return oldNewMapping;
	}

	protected OldNewMapping toUnwrappedModel(OldNewMapping oldNewMapping) {
		if (oldNewMapping instanceof OldNewMappingImpl) {
			return oldNewMapping;
		}

		OldNewMappingImpl oldNewMappingImpl = new OldNewMappingImpl();

		oldNewMappingImpl.setNew(oldNewMapping.isNew());
		oldNewMappingImpl.setPrimaryKey(oldNewMapping.getPrimaryKey());

		oldNewMappingImpl.setId(oldNewMapping.getId());
		oldNewMappingImpl.setOldPK(oldNewMapping.getOldPK());
		oldNewMappingImpl.setNewPK(oldNewMapping.getNewPK());
		oldNewMappingImpl.setTableName(oldNewMapping.getTableName());

		return oldNewMappingImpl;
	}

	/**
	 * Returns the old new mapping with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the old new mapping
	 * @return the old new mapping
	 * @throws org.han.service.NoSuchOldNewMappingException if a old new mapping with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OldNewMapping findByPrimaryKey(Serializable primaryKey)
		throws NoSuchOldNewMappingException, SystemException {
		OldNewMapping oldNewMapping = fetchByPrimaryKey(primaryKey);

		if (oldNewMapping == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchOldNewMappingException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return oldNewMapping;
	}

	/**
	 * Returns the old new mapping with the primary key or throws a {@link org.han.service.NoSuchOldNewMappingException} if it could not be found.
	 *
	 * @param id the primary key of the old new mapping
	 * @return the old new mapping
	 * @throws org.han.service.NoSuchOldNewMappingException if a old new mapping with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OldNewMapping findByPrimaryKey(long id)
		throws NoSuchOldNewMappingException, SystemException {
		return findByPrimaryKey((Serializable)id);
	}

	/**
	 * Returns the old new mapping with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the old new mapping
	 * @return the old new mapping, or <code>null</code> if a old new mapping with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OldNewMapping fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		OldNewMapping oldNewMapping = (OldNewMapping)EntityCacheUtil.getResult(OldNewMappingModelImpl.ENTITY_CACHE_ENABLED,
				OldNewMappingImpl.class, primaryKey);

		if (oldNewMapping == _nullOldNewMapping) {
			return null;
		}

		if (oldNewMapping == null) {
			Session session = null;

			try {
				session = openSession();

				oldNewMapping = (OldNewMapping)session.get(OldNewMappingImpl.class,
						primaryKey);

				if (oldNewMapping != null) {
					cacheResult(oldNewMapping);
				}
				else {
					EntityCacheUtil.putResult(OldNewMappingModelImpl.ENTITY_CACHE_ENABLED,
						OldNewMappingImpl.class, primaryKey, _nullOldNewMapping);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(OldNewMappingModelImpl.ENTITY_CACHE_ENABLED,
					OldNewMappingImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return oldNewMapping;
	}

	/**
	 * Returns the old new mapping with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param id the primary key of the old new mapping
	 * @return the old new mapping, or <code>null</code> if a old new mapping with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OldNewMapping fetchByPrimaryKey(long id) throws SystemException {
		return fetchByPrimaryKey((Serializable)id);
	}

	/**
	 * Returns all the old new mappings.
	 *
	 * @return the old new mappings
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<OldNewMapping> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the old new mappings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.OldNewMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of old new mappings
	 * @param end the upper bound of the range of old new mappings (not inclusive)
	 * @return the range of old new mappings
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<OldNewMapping> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the old new mappings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.OldNewMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of old new mappings
	 * @param end the upper bound of the range of old new mappings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of old new mappings
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<OldNewMapping> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<OldNewMapping> list = (List<OldNewMapping>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_OLDNEWMAPPING);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_OLDNEWMAPPING;

				if (pagination) {
					sql = sql.concat(OldNewMappingModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<OldNewMapping>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<OldNewMapping>(list);
				}
				else {
					list = (List<OldNewMapping>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the old new mappings from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (OldNewMapping oldNewMapping : findAll()) {
			remove(oldNewMapping);
		}
	}

	/**
	 * Returns the number of old new mappings.
	 *
	 * @return the number of old new mappings
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_OLDNEWMAPPING);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the old new mapping persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.org.han.service.model.OldNewMapping")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<OldNewMapping>> listenersList = new ArrayList<ModelListener<OldNewMapping>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<OldNewMapping>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(OldNewMappingImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_OLDNEWMAPPING = "SELECT oldNewMapping FROM OldNewMapping oldNewMapping";
	private static final String _SQL_SELECT_OLDNEWMAPPING_WHERE = "SELECT oldNewMapping FROM OldNewMapping oldNewMapping WHERE ";
	private static final String _SQL_COUNT_OLDNEWMAPPING = "SELECT COUNT(oldNewMapping) FROM OldNewMapping oldNewMapping";
	private static final String _SQL_COUNT_OLDNEWMAPPING_WHERE = "SELECT COUNT(oldNewMapping) FROM OldNewMapping oldNewMapping WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "oldNewMapping.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No OldNewMapping exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No OldNewMapping exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(OldNewMappingPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"id"
			});
	private static OldNewMapping _nullOldNewMapping = new OldNewMappingImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<OldNewMapping> toCacheModel() {
				return _nullOldNewMappingCacheModel;
			}
		};

	private static CacheModel<OldNewMapping> _nullOldNewMappingCacheModel = new CacheModel<OldNewMapping>() {
			@Override
			public OldNewMapping toEntityModel() {
				return _nullOldNewMapping;
			}
		};
}