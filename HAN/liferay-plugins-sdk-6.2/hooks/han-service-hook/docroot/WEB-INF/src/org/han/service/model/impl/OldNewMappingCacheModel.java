/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.han.service.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import org.han.service.model.OldNewMapping;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing OldNewMapping in entity cache.
 *
 * @author Rohan
 * @see OldNewMapping
 * @generated
 */
public class OldNewMappingCacheModel implements CacheModel<OldNewMapping>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{id=");
		sb.append(id);
		sb.append(", oldPK=");
		sb.append(oldPK);
		sb.append(", newPK=");
		sb.append(newPK);
		sb.append(", TableName=");
		sb.append(TableName);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public OldNewMapping toEntityModel() {
		OldNewMappingImpl oldNewMappingImpl = new OldNewMappingImpl();

		oldNewMappingImpl.setId(id);
		oldNewMappingImpl.setOldPK(oldPK);
		oldNewMappingImpl.setNewPK(newPK);

		if (TableName == null) {
			oldNewMappingImpl.setTableName(StringPool.BLANK);
		}
		else {
			oldNewMappingImpl.setTableName(TableName);
		}

		oldNewMappingImpl.resetOriginalValues();

		return oldNewMappingImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		id = objectInput.readLong();
		oldPK = objectInput.readLong();
		newPK = objectInput.readLong();
		TableName = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(id);
		objectOutput.writeLong(oldPK);
		objectOutput.writeLong(newPK);

		if (TableName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(TableName);
		}
	}

	public long id;
	public long oldPK;
	public long newPK;
	public String TableName;
}