/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.han.service.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import org.han.service.NoSuchMigrationSummaryException;
import org.han.service.model.MigrationSummary;
import org.han.service.model.impl.MigrationSummaryImpl;
import org.han.service.model.impl.MigrationSummaryModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the migration summary service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Rohan
 * @see MigrationSummaryPersistence
 * @see MigrationSummaryUtil
 * @generated
 */
public class MigrationSummaryPersistenceImpl extends BasePersistenceImpl<MigrationSummary>
	implements MigrationSummaryPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link MigrationSummaryUtil} to access the migration summary persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = MigrationSummaryImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(MigrationSummaryModelImpl.ENTITY_CACHE_ENABLED,
			MigrationSummaryModelImpl.FINDER_CACHE_ENABLED,
			MigrationSummaryImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(MigrationSummaryModelImpl.ENTITY_CACHE_ENABLED,
			MigrationSummaryModelImpl.FINDER_CACHE_ENABLED,
			MigrationSummaryImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(MigrationSummaryModelImpl.ENTITY_CACHE_ENABLED,
			MigrationSummaryModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_TABLENAME =
		new FinderPath(MigrationSummaryModelImpl.ENTITY_CACHE_ENABLED,
			MigrationSummaryModelImpl.FINDER_CACHE_ENABLED,
			MigrationSummaryImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findBytableName",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TABLENAME =
		new FinderPath(MigrationSummaryModelImpl.ENTITY_CACHE_ENABLED,
			MigrationSummaryModelImpl.FINDER_CACHE_ENABLED,
			MigrationSummaryImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBytableName",
			new String[] { String.class.getName() },
			MigrationSummaryModelImpl.TABLENAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_TABLENAME = new FinderPath(MigrationSummaryModelImpl.ENTITY_CACHE_ENABLED,
			MigrationSummaryModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBytableName",
			new String[] { String.class.getName() });

	/**
	 * Returns all the migration summaries where TableName = &#63;.
	 *
	 * @param TableName the table name
	 * @return the matching migration summaries
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<MigrationSummary> findBytableName(String TableName)
		throws SystemException {
		return findBytableName(TableName, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the migration summaries where TableName = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.MigrationSummaryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param TableName the table name
	 * @param start the lower bound of the range of migration summaries
	 * @param end the upper bound of the range of migration summaries (not inclusive)
	 * @return the range of matching migration summaries
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<MigrationSummary> findBytableName(String TableName, int start,
		int end) throws SystemException {
		return findBytableName(TableName, start, end, null);
	}

	/**
	 * Returns an ordered range of all the migration summaries where TableName = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.MigrationSummaryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param TableName the table name
	 * @param start the lower bound of the range of migration summaries
	 * @param end the upper bound of the range of migration summaries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching migration summaries
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<MigrationSummary> findBytableName(String TableName, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TABLENAME;
			finderArgs = new Object[] { TableName };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_TABLENAME;
			finderArgs = new Object[] { TableName, start, end, orderByComparator };
		}

		List<MigrationSummary> list = (List<MigrationSummary>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (MigrationSummary migrationSummary : list) {
				if (!Validator.equals(TableName, migrationSummary.getTableName())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_MIGRATIONSUMMARY_WHERE);

			boolean bindTableName = false;

			if (TableName == null) {
				query.append(_FINDER_COLUMN_TABLENAME_TABLENAME_1);
			}
			else if (TableName.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_TABLENAME_TABLENAME_3);
			}
			else {
				bindTableName = true;

				query.append(_FINDER_COLUMN_TABLENAME_TABLENAME_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(MigrationSummaryModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindTableName) {
					qPos.add(TableName);
				}

				if (!pagination) {
					list = (List<MigrationSummary>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<MigrationSummary>(list);
				}
				else {
					list = (List<MigrationSummary>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first migration summary in the ordered set where TableName = &#63;.
	 *
	 * @param TableName the table name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching migration summary
	 * @throws org.han.service.NoSuchMigrationSummaryException if a matching migration summary could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MigrationSummary findBytableName_First(String TableName,
		OrderByComparator orderByComparator)
		throws NoSuchMigrationSummaryException, SystemException {
		MigrationSummary migrationSummary = fetchBytableName_First(TableName,
				orderByComparator);

		if (migrationSummary != null) {
			return migrationSummary;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("TableName=");
		msg.append(TableName);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchMigrationSummaryException(msg.toString());
	}

	/**
	 * Returns the first migration summary in the ordered set where TableName = &#63;.
	 *
	 * @param TableName the table name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching migration summary, or <code>null</code> if a matching migration summary could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MigrationSummary fetchBytableName_First(String TableName,
		OrderByComparator orderByComparator) throws SystemException {
		List<MigrationSummary> list = findBytableName(TableName, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last migration summary in the ordered set where TableName = &#63;.
	 *
	 * @param TableName the table name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching migration summary
	 * @throws org.han.service.NoSuchMigrationSummaryException if a matching migration summary could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MigrationSummary findBytableName_Last(String TableName,
		OrderByComparator orderByComparator)
		throws NoSuchMigrationSummaryException, SystemException {
		MigrationSummary migrationSummary = fetchBytableName_Last(TableName,
				orderByComparator);

		if (migrationSummary != null) {
			return migrationSummary;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("TableName=");
		msg.append(TableName);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchMigrationSummaryException(msg.toString());
	}

	/**
	 * Returns the last migration summary in the ordered set where TableName = &#63;.
	 *
	 * @param TableName the table name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching migration summary, or <code>null</code> if a matching migration summary could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MigrationSummary fetchBytableName_Last(String TableName,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBytableName(TableName);

		if (count == 0) {
			return null;
		}

		List<MigrationSummary> list = findBytableName(TableName, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the migration summaries before and after the current migration summary in the ordered set where TableName = &#63;.
	 *
	 * @param id the primary key of the current migration summary
	 * @param TableName the table name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next migration summary
	 * @throws org.han.service.NoSuchMigrationSummaryException if a migration summary with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MigrationSummary[] findBytableName_PrevAndNext(long id,
		String TableName, OrderByComparator orderByComparator)
		throws NoSuchMigrationSummaryException, SystemException {
		MigrationSummary migrationSummary = findByPrimaryKey(id);

		Session session = null;

		try {
			session = openSession();

			MigrationSummary[] array = new MigrationSummaryImpl[3];

			array[0] = getBytableName_PrevAndNext(session, migrationSummary,
					TableName, orderByComparator, true);

			array[1] = migrationSummary;

			array[2] = getBytableName_PrevAndNext(session, migrationSummary,
					TableName, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected MigrationSummary getBytableName_PrevAndNext(Session session,
		MigrationSummary migrationSummary, String TableName,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_MIGRATIONSUMMARY_WHERE);

		boolean bindTableName = false;

		if (TableName == null) {
			query.append(_FINDER_COLUMN_TABLENAME_TABLENAME_1);
		}
		else if (TableName.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_TABLENAME_TABLENAME_3);
		}
		else {
			bindTableName = true;

			query.append(_FINDER_COLUMN_TABLENAME_TABLENAME_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(MigrationSummaryModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindTableName) {
			qPos.add(TableName);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(migrationSummary);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<MigrationSummary> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the migration summaries where TableName = &#63; from the database.
	 *
	 * @param TableName the table name
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeBytableName(String TableName) throws SystemException {
		for (MigrationSummary migrationSummary : findBytableName(TableName,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(migrationSummary);
		}
	}

	/**
	 * Returns the number of migration summaries where TableName = &#63;.
	 *
	 * @param TableName the table name
	 * @return the number of matching migration summaries
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countBytableName(String TableName) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_TABLENAME;

		Object[] finderArgs = new Object[] { TableName };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_MIGRATIONSUMMARY_WHERE);

			boolean bindTableName = false;

			if (TableName == null) {
				query.append(_FINDER_COLUMN_TABLENAME_TABLENAME_1);
			}
			else if (TableName.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_TABLENAME_TABLENAME_3);
			}
			else {
				bindTableName = true;

				query.append(_FINDER_COLUMN_TABLENAME_TABLENAME_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindTableName) {
					qPos.add(TableName);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_TABLENAME_TABLENAME_1 = "migrationSummary.TableName IS NULL";
	private static final String _FINDER_COLUMN_TABLENAME_TABLENAME_2 = "migrationSummary.TableName = ?";
	private static final String _FINDER_COLUMN_TABLENAME_TABLENAME_3 = "(migrationSummary.TableName IS NULL OR migrationSummary.TableName = '')";

	public MigrationSummaryPersistenceImpl() {
		setModelClass(MigrationSummary.class);
	}

	/**
	 * Caches the migration summary in the entity cache if it is enabled.
	 *
	 * @param migrationSummary the migration summary
	 */
	@Override
	public void cacheResult(MigrationSummary migrationSummary) {
		EntityCacheUtil.putResult(MigrationSummaryModelImpl.ENTITY_CACHE_ENABLED,
			MigrationSummaryImpl.class, migrationSummary.getPrimaryKey(),
			migrationSummary);

		migrationSummary.resetOriginalValues();
	}

	/**
	 * Caches the migration summaries in the entity cache if it is enabled.
	 *
	 * @param migrationSummaries the migration summaries
	 */
	@Override
	public void cacheResult(List<MigrationSummary> migrationSummaries) {
		for (MigrationSummary migrationSummary : migrationSummaries) {
			if (EntityCacheUtil.getResult(
						MigrationSummaryModelImpl.ENTITY_CACHE_ENABLED,
						MigrationSummaryImpl.class,
						migrationSummary.getPrimaryKey()) == null) {
				cacheResult(migrationSummary);
			}
			else {
				migrationSummary.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all migration summaries.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(MigrationSummaryImpl.class.getName());
		}

		EntityCacheUtil.clearCache(MigrationSummaryImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the migration summary.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(MigrationSummary migrationSummary) {
		EntityCacheUtil.removeResult(MigrationSummaryModelImpl.ENTITY_CACHE_ENABLED,
			MigrationSummaryImpl.class, migrationSummary.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<MigrationSummary> migrationSummaries) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (MigrationSummary migrationSummary : migrationSummaries) {
			EntityCacheUtil.removeResult(MigrationSummaryModelImpl.ENTITY_CACHE_ENABLED,
				MigrationSummaryImpl.class, migrationSummary.getPrimaryKey());
		}
	}

	/**
	 * Creates a new migration summary with the primary key. Does not add the migration summary to the database.
	 *
	 * @param id the primary key for the new migration summary
	 * @return the new migration summary
	 */
	@Override
	public MigrationSummary create(long id) {
		MigrationSummary migrationSummary = new MigrationSummaryImpl();

		migrationSummary.setNew(true);
		migrationSummary.setPrimaryKey(id);

		return migrationSummary;
	}

	/**
	 * Removes the migration summary with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param id the primary key of the migration summary
	 * @return the migration summary that was removed
	 * @throws org.han.service.NoSuchMigrationSummaryException if a migration summary with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MigrationSummary remove(long id)
		throws NoSuchMigrationSummaryException, SystemException {
		return remove((Serializable)id);
	}

	/**
	 * Removes the migration summary with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the migration summary
	 * @return the migration summary that was removed
	 * @throws org.han.service.NoSuchMigrationSummaryException if a migration summary with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MigrationSummary remove(Serializable primaryKey)
		throws NoSuchMigrationSummaryException, SystemException {
		Session session = null;

		try {
			session = openSession();

			MigrationSummary migrationSummary = (MigrationSummary)session.get(MigrationSummaryImpl.class,
					primaryKey);

			if (migrationSummary == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchMigrationSummaryException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(migrationSummary);
		}
		catch (NoSuchMigrationSummaryException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected MigrationSummary removeImpl(MigrationSummary migrationSummary)
		throws SystemException {
		migrationSummary = toUnwrappedModel(migrationSummary);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(migrationSummary)) {
				migrationSummary = (MigrationSummary)session.get(MigrationSummaryImpl.class,
						migrationSummary.getPrimaryKeyObj());
			}

			if (migrationSummary != null) {
				session.delete(migrationSummary);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (migrationSummary != null) {
			clearCache(migrationSummary);
		}

		return migrationSummary;
	}

	@Override
	public MigrationSummary updateImpl(
		org.han.service.model.MigrationSummary migrationSummary)
		throws SystemException {
		migrationSummary = toUnwrappedModel(migrationSummary);

		boolean isNew = migrationSummary.isNew();

		MigrationSummaryModelImpl migrationSummaryModelImpl = (MigrationSummaryModelImpl)migrationSummary;

		Session session = null;

		try {
			session = openSession();

			if (migrationSummary.isNew()) {
				session.save(migrationSummary);

				migrationSummary.setNew(false);
			}
			else {
				session.merge(migrationSummary);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !MigrationSummaryModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((migrationSummaryModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TABLENAME.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						migrationSummaryModelImpl.getOriginalTableName()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TABLENAME,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TABLENAME,
					args);

				args = new Object[] { migrationSummaryModelImpl.getTableName() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TABLENAME,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TABLENAME,
					args);
			}
		}

		EntityCacheUtil.putResult(MigrationSummaryModelImpl.ENTITY_CACHE_ENABLED,
			MigrationSummaryImpl.class, migrationSummary.getPrimaryKey(),
			migrationSummary);

		return migrationSummary;
	}

	protected MigrationSummary toUnwrappedModel(
		MigrationSummary migrationSummary) {
		if (migrationSummary instanceof MigrationSummaryImpl) {
			return migrationSummary;
		}

		MigrationSummaryImpl migrationSummaryImpl = new MigrationSummaryImpl();

		migrationSummaryImpl.setNew(migrationSummary.isNew());
		migrationSummaryImpl.setPrimaryKey(migrationSummary.getPrimaryKey());

		migrationSummaryImpl.setId(migrationSummary.getId());
		migrationSummaryImpl.setStartTime(migrationSummary.getStartTime());
		migrationSummaryImpl.setEndTime(migrationSummary.getEndTime());
		migrationSummaryImpl.setTableName(migrationSummary.getTableName());
		migrationSummaryImpl.setAdded(migrationSummary.getAdded());
		migrationSummaryImpl.setSkipped(migrationSummary.getSkipped());
		migrationSummaryImpl.setTotalFound(migrationSummary.getTotalFound());

		return migrationSummaryImpl;
	}

	/**
	 * Returns the migration summary with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the migration summary
	 * @return the migration summary
	 * @throws org.han.service.NoSuchMigrationSummaryException if a migration summary with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MigrationSummary findByPrimaryKey(Serializable primaryKey)
		throws NoSuchMigrationSummaryException, SystemException {
		MigrationSummary migrationSummary = fetchByPrimaryKey(primaryKey);

		if (migrationSummary == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchMigrationSummaryException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return migrationSummary;
	}

	/**
	 * Returns the migration summary with the primary key or throws a {@link org.han.service.NoSuchMigrationSummaryException} if it could not be found.
	 *
	 * @param id the primary key of the migration summary
	 * @return the migration summary
	 * @throws org.han.service.NoSuchMigrationSummaryException if a migration summary with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MigrationSummary findByPrimaryKey(long id)
		throws NoSuchMigrationSummaryException, SystemException {
		return findByPrimaryKey((Serializable)id);
	}

	/**
	 * Returns the migration summary with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the migration summary
	 * @return the migration summary, or <code>null</code> if a migration summary with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MigrationSummary fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		MigrationSummary migrationSummary = (MigrationSummary)EntityCacheUtil.getResult(MigrationSummaryModelImpl.ENTITY_CACHE_ENABLED,
				MigrationSummaryImpl.class, primaryKey);

		if (migrationSummary == _nullMigrationSummary) {
			return null;
		}

		if (migrationSummary == null) {
			Session session = null;

			try {
				session = openSession();

				migrationSummary = (MigrationSummary)session.get(MigrationSummaryImpl.class,
						primaryKey);

				if (migrationSummary != null) {
					cacheResult(migrationSummary);
				}
				else {
					EntityCacheUtil.putResult(MigrationSummaryModelImpl.ENTITY_CACHE_ENABLED,
						MigrationSummaryImpl.class, primaryKey,
						_nullMigrationSummary);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(MigrationSummaryModelImpl.ENTITY_CACHE_ENABLED,
					MigrationSummaryImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return migrationSummary;
	}

	/**
	 * Returns the migration summary with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param id the primary key of the migration summary
	 * @return the migration summary, or <code>null</code> if a migration summary with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MigrationSummary fetchByPrimaryKey(long id)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)id);
	}

	/**
	 * Returns all the migration summaries.
	 *
	 * @return the migration summaries
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<MigrationSummary> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the migration summaries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.MigrationSummaryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of migration summaries
	 * @param end the upper bound of the range of migration summaries (not inclusive)
	 * @return the range of migration summaries
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<MigrationSummary> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the migration summaries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.MigrationSummaryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of migration summaries
	 * @param end the upper bound of the range of migration summaries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of migration summaries
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<MigrationSummary> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<MigrationSummary> list = (List<MigrationSummary>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_MIGRATIONSUMMARY);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_MIGRATIONSUMMARY;

				if (pagination) {
					sql = sql.concat(MigrationSummaryModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<MigrationSummary>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<MigrationSummary>(list);
				}
				else {
					list = (List<MigrationSummary>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the migration summaries from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (MigrationSummary migrationSummary : findAll()) {
			remove(migrationSummary);
		}
	}

	/**
	 * Returns the number of migration summaries.
	 *
	 * @return the number of migration summaries
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_MIGRATIONSUMMARY);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the migration summary persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.org.han.service.model.MigrationSummary")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<MigrationSummary>> listenersList = new ArrayList<ModelListener<MigrationSummary>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<MigrationSummary>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(MigrationSummaryImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_MIGRATIONSUMMARY = "SELECT migrationSummary FROM MigrationSummary migrationSummary";
	private static final String _SQL_SELECT_MIGRATIONSUMMARY_WHERE = "SELECT migrationSummary FROM MigrationSummary migrationSummary WHERE ";
	private static final String _SQL_COUNT_MIGRATIONSUMMARY = "SELECT COUNT(migrationSummary) FROM MigrationSummary migrationSummary";
	private static final String _SQL_COUNT_MIGRATIONSUMMARY_WHERE = "SELECT COUNT(migrationSummary) FROM MigrationSummary migrationSummary WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "migrationSummary.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No MigrationSummary exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No MigrationSummary exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(MigrationSummaryPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"id"
			});
	private static MigrationSummary _nullMigrationSummary = new MigrationSummaryImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<MigrationSummary> toCacheModel() {
				return _nullMigrationSummaryCacheModel;
			}
		};

	private static CacheModel<MigrationSummary> _nullMigrationSummaryCacheModel = new CacheModel<MigrationSummary>() {
			@Override
			public MigrationSummary toEntityModel() {
				return _nullMigrationSummary;
			}
		};
}