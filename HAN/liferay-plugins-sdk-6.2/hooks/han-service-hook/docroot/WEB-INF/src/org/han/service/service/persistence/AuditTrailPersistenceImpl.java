/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.han.service.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import org.han.service.NoSuchAuditTrailException;
import org.han.service.model.AuditTrail;
import org.han.service.model.impl.AuditTrailImpl;
import org.han.service.model.impl.AuditTrailModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the audit trail service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Rohan
 * @see AuditTrailPersistence
 * @see AuditTrailUtil
 * @generated
 */
public class AuditTrailPersistenceImpl extends BasePersistenceImpl<AuditTrail>
	implements AuditTrailPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link AuditTrailUtil} to access the audit trail persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = AuditTrailImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(AuditTrailModelImpl.ENTITY_CACHE_ENABLED,
			AuditTrailModelImpl.FINDER_CACHE_ENABLED, AuditTrailImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(AuditTrailModelImpl.ENTITY_CACHE_ENABLED,
			AuditTrailModelImpl.FINDER_CACHE_ENABLED, AuditTrailImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(AuditTrailModelImpl.ENTITY_CACHE_ENABLED,
			AuditTrailModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public AuditTrailPersistenceImpl() {
		setModelClass(AuditTrail.class);
	}

	/**
	 * Caches the audit trail in the entity cache if it is enabled.
	 *
	 * @param auditTrail the audit trail
	 */
	@Override
	public void cacheResult(AuditTrail auditTrail) {
		EntityCacheUtil.putResult(AuditTrailModelImpl.ENTITY_CACHE_ENABLED,
			AuditTrailImpl.class, auditTrail.getPrimaryKey(), auditTrail);

		auditTrail.resetOriginalValues();
	}

	/**
	 * Caches the audit trails in the entity cache if it is enabled.
	 *
	 * @param auditTrails the audit trails
	 */
	@Override
	public void cacheResult(List<AuditTrail> auditTrails) {
		for (AuditTrail auditTrail : auditTrails) {
			if (EntityCacheUtil.getResult(
						AuditTrailModelImpl.ENTITY_CACHE_ENABLED,
						AuditTrailImpl.class, auditTrail.getPrimaryKey()) == null) {
				cacheResult(auditTrail);
			}
			else {
				auditTrail.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all audit trails.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(AuditTrailImpl.class.getName());
		}

		EntityCacheUtil.clearCache(AuditTrailImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the audit trail.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(AuditTrail auditTrail) {
		EntityCacheUtil.removeResult(AuditTrailModelImpl.ENTITY_CACHE_ENABLED,
			AuditTrailImpl.class, auditTrail.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<AuditTrail> auditTrails) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (AuditTrail auditTrail : auditTrails) {
			EntityCacheUtil.removeResult(AuditTrailModelImpl.ENTITY_CACHE_ENABLED,
				AuditTrailImpl.class, auditTrail.getPrimaryKey());
		}
	}

	/**
	 * Creates a new audit trail with the primary key. Does not add the audit trail to the database.
	 *
	 * @param id the primary key for the new audit trail
	 * @return the new audit trail
	 */
	@Override
	public AuditTrail create(long id) {
		AuditTrail auditTrail = new AuditTrailImpl();

		auditTrail.setNew(true);
		auditTrail.setPrimaryKey(id);

		return auditTrail;
	}

	/**
	 * Removes the audit trail with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param id the primary key of the audit trail
	 * @return the audit trail that was removed
	 * @throws org.han.service.NoSuchAuditTrailException if a audit trail with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AuditTrail remove(long id)
		throws NoSuchAuditTrailException, SystemException {
		return remove((Serializable)id);
	}

	/**
	 * Removes the audit trail with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the audit trail
	 * @return the audit trail that was removed
	 * @throws org.han.service.NoSuchAuditTrailException if a audit trail with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AuditTrail remove(Serializable primaryKey)
		throws NoSuchAuditTrailException, SystemException {
		Session session = null;

		try {
			session = openSession();

			AuditTrail auditTrail = (AuditTrail)session.get(AuditTrailImpl.class,
					primaryKey);

			if (auditTrail == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchAuditTrailException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(auditTrail);
		}
		catch (NoSuchAuditTrailException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected AuditTrail removeImpl(AuditTrail auditTrail)
		throws SystemException {
		auditTrail = toUnwrappedModel(auditTrail);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(auditTrail)) {
				auditTrail = (AuditTrail)session.get(AuditTrailImpl.class,
						auditTrail.getPrimaryKeyObj());
			}

			if (auditTrail != null) {
				session.delete(auditTrail);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (auditTrail != null) {
			clearCache(auditTrail);
		}

		return auditTrail;
	}

	@Override
	public AuditTrail updateImpl(org.han.service.model.AuditTrail auditTrail)
		throws SystemException {
		auditTrail = toUnwrappedModel(auditTrail);

		boolean isNew = auditTrail.isNew();

		Session session = null;

		try {
			session = openSession();

			if (auditTrail.isNew()) {
				session.save(auditTrail);

				auditTrail.setNew(false);
			}
			else {
				session.merge(auditTrail);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(AuditTrailModelImpl.ENTITY_CACHE_ENABLED,
			AuditTrailImpl.class, auditTrail.getPrimaryKey(), auditTrail);

		return auditTrail;
	}

	protected AuditTrail toUnwrappedModel(AuditTrail auditTrail) {
		if (auditTrail instanceof AuditTrailImpl) {
			return auditTrail;
		}

		AuditTrailImpl auditTrailImpl = new AuditTrailImpl();

		auditTrailImpl.setNew(auditTrail.isNew());
		auditTrailImpl.setPrimaryKey(auditTrail.getPrimaryKey());

		auditTrailImpl.setId(auditTrail.getId());
		auditTrailImpl.setEventType(auditTrail.getEventType());
		auditTrailImpl.setClassName(auditTrail.getClassName());
		auditTrailImpl.setClassPK(auditTrail.getClassPK());
		auditTrailImpl.setRealUserId(auditTrail.getRealUserId());
		auditTrailImpl.setRealUserName(auditTrail.getRealUserName());
		auditTrailImpl.setRealUserEmail(auditTrail.getRealUserEmail());
		auditTrailImpl.setUserId(auditTrail.getUserId());
		auditTrailImpl.setUserName(auditTrail.getUserName());
		auditTrailImpl.setUserEmail(auditTrail.getUserEmail());
		auditTrailImpl.setCreateDate(auditTrail.getCreateDate());
		auditTrailImpl.setMessage(auditTrail.getMessage());

		return auditTrailImpl;
	}

	/**
	 * Returns the audit trail with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the audit trail
	 * @return the audit trail
	 * @throws org.han.service.NoSuchAuditTrailException if a audit trail with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AuditTrail findByPrimaryKey(Serializable primaryKey)
		throws NoSuchAuditTrailException, SystemException {
		AuditTrail auditTrail = fetchByPrimaryKey(primaryKey);

		if (auditTrail == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchAuditTrailException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return auditTrail;
	}

	/**
	 * Returns the audit trail with the primary key or throws a {@link org.han.service.NoSuchAuditTrailException} if it could not be found.
	 *
	 * @param id the primary key of the audit trail
	 * @return the audit trail
	 * @throws org.han.service.NoSuchAuditTrailException if a audit trail with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AuditTrail findByPrimaryKey(long id)
		throws NoSuchAuditTrailException, SystemException {
		return findByPrimaryKey((Serializable)id);
	}

	/**
	 * Returns the audit trail with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the audit trail
	 * @return the audit trail, or <code>null</code> if a audit trail with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AuditTrail fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		AuditTrail auditTrail = (AuditTrail)EntityCacheUtil.getResult(AuditTrailModelImpl.ENTITY_CACHE_ENABLED,
				AuditTrailImpl.class, primaryKey);

		if (auditTrail == _nullAuditTrail) {
			return null;
		}

		if (auditTrail == null) {
			Session session = null;

			try {
				session = openSession();

				auditTrail = (AuditTrail)session.get(AuditTrailImpl.class,
						primaryKey);

				if (auditTrail != null) {
					cacheResult(auditTrail);
				}
				else {
					EntityCacheUtil.putResult(AuditTrailModelImpl.ENTITY_CACHE_ENABLED,
						AuditTrailImpl.class, primaryKey, _nullAuditTrail);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(AuditTrailModelImpl.ENTITY_CACHE_ENABLED,
					AuditTrailImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return auditTrail;
	}

	/**
	 * Returns the audit trail with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param id the primary key of the audit trail
	 * @return the audit trail, or <code>null</code> if a audit trail with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AuditTrail fetchByPrimaryKey(long id) throws SystemException {
		return fetchByPrimaryKey((Serializable)id);
	}

	/**
	 * Returns all the audit trails.
	 *
	 * @return the audit trails
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<AuditTrail> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the audit trails.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.AuditTrailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of audit trails
	 * @param end the upper bound of the range of audit trails (not inclusive)
	 * @return the range of audit trails
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<AuditTrail> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the audit trails.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.AuditTrailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of audit trails
	 * @param end the upper bound of the range of audit trails (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of audit trails
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<AuditTrail> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<AuditTrail> list = (List<AuditTrail>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_AUDITTRAIL);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_AUDITTRAIL;

				if (pagination) {
					sql = sql.concat(AuditTrailModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<AuditTrail>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<AuditTrail>(list);
				}
				else {
					list = (List<AuditTrail>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the audit trails from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (AuditTrail auditTrail : findAll()) {
			remove(auditTrail);
		}
	}

	/**
	 * Returns the number of audit trails.
	 *
	 * @return the number of audit trails
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_AUDITTRAIL);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the audit trail persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.org.han.service.model.AuditTrail")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<AuditTrail>> listenersList = new ArrayList<ModelListener<AuditTrail>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<AuditTrail>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(AuditTrailImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_AUDITTRAIL = "SELECT auditTrail FROM AuditTrail auditTrail";
	private static final String _SQL_COUNT_AUDITTRAIL = "SELECT COUNT(auditTrail) FROM AuditTrail auditTrail";
	private static final String _ORDER_BY_ENTITY_ALIAS = "auditTrail.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No AuditTrail exists with the primary key ";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(AuditTrailPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"id"
			});
	private static AuditTrail _nullAuditTrail = new AuditTrailImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<AuditTrail> toCacheModel() {
				return _nullAuditTrailCacheModel;
			}
		};

	private static CacheModel<AuditTrail> _nullAuditTrailCacheModel = new CacheModel<AuditTrail>() {
			@Override
			public AuditTrail toEntityModel() {
				return _nullAuditTrail;
			}
		};
}