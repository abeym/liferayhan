/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.han.service.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import org.han.service.NoSuchSkippedEntriesException;
import org.han.service.model.SkippedEntries;
import org.han.service.model.impl.SkippedEntriesImpl;
import org.han.service.model.impl.SkippedEntriesModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the skipped entries service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Rohan
 * @see SkippedEntriesPersistence
 * @see SkippedEntriesUtil
 * @generated
 */
public class SkippedEntriesPersistenceImpl extends BasePersistenceImpl<SkippedEntries>
	implements SkippedEntriesPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link SkippedEntriesUtil} to access the skipped entries persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = SkippedEntriesImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(SkippedEntriesModelImpl.ENTITY_CACHE_ENABLED,
			SkippedEntriesModelImpl.FINDER_CACHE_ENABLED,
			SkippedEntriesImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(SkippedEntriesModelImpl.ENTITY_CACHE_ENABLED,
			SkippedEntriesModelImpl.FINDER_CACHE_ENABLED,
			SkippedEntriesImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(SkippedEntriesModelImpl.ENTITY_CACHE_ENABLED,
			SkippedEntriesModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ENTITYNAMEANDOLDPK =
		new FinderPath(SkippedEntriesModelImpl.ENTITY_CACHE_ENABLED,
			SkippedEntriesModelImpl.FINDER_CACHE_ENABLED,
			SkippedEntriesImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByentityNameAndOldPK",
			new String[] {
				String.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ENTITYNAMEANDOLDPK =
		new FinderPath(SkippedEntriesModelImpl.ENTITY_CACHE_ENABLED,
			SkippedEntriesModelImpl.FINDER_CACHE_ENABLED,
			SkippedEntriesImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByentityNameAndOldPK",
			new String[] { String.class.getName(), Long.class.getName() },
			SkippedEntriesModelImpl.TABLENAME_COLUMN_BITMASK |
			SkippedEntriesModelImpl.OLDPK_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ENTITYNAMEANDOLDPK = new FinderPath(SkippedEntriesModelImpl.ENTITY_CACHE_ENABLED,
			SkippedEntriesModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByentityNameAndOldPK",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns all the skipped entrieses where TableName = &#63; and oldPK = &#63;.
	 *
	 * @param TableName the table name
	 * @param oldPK the old p k
	 * @return the matching skipped entrieses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<SkippedEntries> findByentityNameAndOldPK(String TableName,
		long oldPK) throws SystemException {
		return findByentityNameAndOldPK(TableName, oldPK, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the skipped entrieses where TableName = &#63; and oldPK = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.SkippedEntriesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param TableName the table name
	 * @param oldPK the old p k
	 * @param start the lower bound of the range of skipped entrieses
	 * @param end the upper bound of the range of skipped entrieses (not inclusive)
	 * @return the range of matching skipped entrieses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<SkippedEntries> findByentityNameAndOldPK(String TableName,
		long oldPK, int start, int end) throws SystemException {
		return findByentityNameAndOldPK(TableName, oldPK, start, end, null);
	}

	/**
	 * Returns an ordered range of all the skipped entrieses where TableName = &#63; and oldPK = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.SkippedEntriesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param TableName the table name
	 * @param oldPK the old p k
	 * @param start the lower bound of the range of skipped entrieses
	 * @param end the upper bound of the range of skipped entrieses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching skipped entrieses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<SkippedEntries> findByentityNameAndOldPK(String TableName,
		long oldPK, int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ENTITYNAMEANDOLDPK;
			finderArgs = new Object[] { TableName, oldPK };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ENTITYNAMEANDOLDPK;
			finderArgs = new Object[] {
					TableName, oldPK,
					
					start, end, orderByComparator
				};
		}

		List<SkippedEntries> list = (List<SkippedEntries>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (SkippedEntries skippedEntries : list) {
				if (!Validator.equals(TableName, skippedEntries.getTableName()) ||
						(oldPK != skippedEntries.getOldPK())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_SKIPPEDENTRIES_WHERE);

			boolean bindTableName = false;

			if (TableName == null) {
				query.append(_FINDER_COLUMN_ENTITYNAMEANDOLDPK_TABLENAME_1);
			}
			else if (TableName.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ENTITYNAMEANDOLDPK_TABLENAME_3);
			}
			else {
				bindTableName = true;

				query.append(_FINDER_COLUMN_ENTITYNAMEANDOLDPK_TABLENAME_2);
			}

			query.append(_FINDER_COLUMN_ENTITYNAMEANDOLDPK_OLDPK_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(SkippedEntriesModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindTableName) {
					qPos.add(TableName);
				}

				qPos.add(oldPK);

				if (!pagination) {
					list = (List<SkippedEntries>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<SkippedEntries>(list);
				}
				else {
					list = (List<SkippedEntries>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first skipped entries in the ordered set where TableName = &#63; and oldPK = &#63;.
	 *
	 * @param TableName the table name
	 * @param oldPK the old p k
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching skipped entries
	 * @throws org.han.service.NoSuchSkippedEntriesException if a matching skipped entries could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public SkippedEntries findByentityNameAndOldPK_First(String TableName,
		long oldPK, OrderByComparator orderByComparator)
		throws NoSuchSkippedEntriesException, SystemException {
		SkippedEntries skippedEntries = fetchByentityNameAndOldPK_First(TableName,
				oldPK, orderByComparator);

		if (skippedEntries != null) {
			return skippedEntries;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("TableName=");
		msg.append(TableName);

		msg.append(", oldPK=");
		msg.append(oldPK);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchSkippedEntriesException(msg.toString());
	}

	/**
	 * Returns the first skipped entries in the ordered set where TableName = &#63; and oldPK = &#63;.
	 *
	 * @param TableName the table name
	 * @param oldPK the old p k
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching skipped entries, or <code>null</code> if a matching skipped entries could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public SkippedEntries fetchByentityNameAndOldPK_First(String TableName,
		long oldPK, OrderByComparator orderByComparator)
		throws SystemException {
		List<SkippedEntries> list = findByentityNameAndOldPK(TableName, oldPK,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last skipped entries in the ordered set where TableName = &#63; and oldPK = &#63;.
	 *
	 * @param TableName the table name
	 * @param oldPK the old p k
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching skipped entries
	 * @throws org.han.service.NoSuchSkippedEntriesException if a matching skipped entries could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public SkippedEntries findByentityNameAndOldPK_Last(String TableName,
		long oldPK, OrderByComparator orderByComparator)
		throws NoSuchSkippedEntriesException, SystemException {
		SkippedEntries skippedEntries = fetchByentityNameAndOldPK_Last(TableName,
				oldPK, orderByComparator);

		if (skippedEntries != null) {
			return skippedEntries;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("TableName=");
		msg.append(TableName);

		msg.append(", oldPK=");
		msg.append(oldPK);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchSkippedEntriesException(msg.toString());
	}

	/**
	 * Returns the last skipped entries in the ordered set where TableName = &#63; and oldPK = &#63;.
	 *
	 * @param TableName the table name
	 * @param oldPK the old p k
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching skipped entries, or <code>null</code> if a matching skipped entries could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public SkippedEntries fetchByentityNameAndOldPK_Last(String TableName,
		long oldPK, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByentityNameAndOldPK(TableName, oldPK);

		if (count == 0) {
			return null;
		}

		List<SkippedEntries> list = findByentityNameAndOldPK(TableName, oldPK,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the skipped entrieses before and after the current skipped entries in the ordered set where TableName = &#63; and oldPK = &#63;.
	 *
	 * @param id the primary key of the current skipped entries
	 * @param TableName the table name
	 * @param oldPK the old p k
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next skipped entries
	 * @throws org.han.service.NoSuchSkippedEntriesException if a skipped entries with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public SkippedEntries[] findByentityNameAndOldPK_PrevAndNext(long id,
		String TableName, long oldPK, OrderByComparator orderByComparator)
		throws NoSuchSkippedEntriesException, SystemException {
		SkippedEntries skippedEntries = findByPrimaryKey(id);

		Session session = null;

		try {
			session = openSession();

			SkippedEntries[] array = new SkippedEntriesImpl[3];

			array[0] = getByentityNameAndOldPK_PrevAndNext(session,
					skippedEntries, TableName, oldPK, orderByComparator, true);

			array[1] = skippedEntries;

			array[2] = getByentityNameAndOldPK_PrevAndNext(session,
					skippedEntries, TableName, oldPK, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected SkippedEntries getByentityNameAndOldPK_PrevAndNext(
		Session session, SkippedEntries skippedEntries, String TableName,
		long oldPK, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_SKIPPEDENTRIES_WHERE);

		boolean bindTableName = false;

		if (TableName == null) {
			query.append(_FINDER_COLUMN_ENTITYNAMEANDOLDPK_TABLENAME_1);
		}
		else if (TableName.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_ENTITYNAMEANDOLDPK_TABLENAME_3);
		}
		else {
			bindTableName = true;

			query.append(_FINDER_COLUMN_ENTITYNAMEANDOLDPK_TABLENAME_2);
		}

		query.append(_FINDER_COLUMN_ENTITYNAMEANDOLDPK_OLDPK_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(SkippedEntriesModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindTableName) {
			qPos.add(TableName);
		}

		qPos.add(oldPK);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(skippedEntries);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<SkippedEntries> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the skipped entrieses where TableName = &#63; and oldPK = &#63; from the database.
	 *
	 * @param TableName the table name
	 * @param oldPK the old p k
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByentityNameAndOldPK(String TableName, long oldPK)
		throws SystemException {
		for (SkippedEntries skippedEntries : findByentityNameAndOldPK(
				TableName, oldPK, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(skippedEntries);
		}
	}

	/**
	 * Returns the number of skipped entrieses where TableName = &#63; and oldPK = &#63;.
	 *
	 * @param TableName the table name
	 * @param oldPK the old p k
	 * @return the number of matching skipped entrieses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByentityNameAndOldPK(String TableName, long oldPK)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_ENTITYNAMEANDOLDPK;

		Object[] finderArgs = new Object[] { TableName, oldPK };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_SKIPPEDENTRIES_WHERE);

			boolean bindTableName = false;

			if (TableName == null) {
				query.append(_FINDER_COLUMN_ENTITYNAMEANDOLDPK_TABLENAME_1);
			}
			else if (TableName.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ENTITYNAMEANDOLDPK_TABLENAME_3);
			}
			else {
				bindTableName = true;

				query.append(_FINDER_COLUMN_ENTITYNAMEANDOLDPK_TABLENAME_2);
			}

			query.append(_FINDER_COLUMN_ENTITYNAMEANDOLDPK_OLDPK_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindTableName) {
					qPos.add(TableName);
				}

				qPos.add(oldPK);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ENTITYNAMEANDOLDPK_TABLENAME_1 = "skippedEntries.TableName IS NULL AND ";
	private static final String _FINDER_COLUMN_ENTITYNAMEANDOLDPK_TABLENAME_2 = "skippedEntries.TableName = ? AND ";
	private static final String _FINDER_COLUMN_ENTITYNAMEANDOLDPK_TABLENAME_3 = "(skippedEntries.TableName IS NULL OR skippedEntries.TableName = '') AND ";
	private static final String _FINDER_COLUMN_ENTITYNAMEANDOLDPK_OLDPK_2 = "skippedEntries.oldPK = ?";

	public SkippedEntriesPersistenceImpl() {
		setModelClass(SkippedEntries.class);
	}

	/**
	 * Caches the skipped entries in the entity cache if it is enabled.
	 *
	 * @param skippedEntries the skipped entries
	 */
	@Override
	public void cacheResult(SkippedEntries skippedEntries) {
		EntityCacheUtil.putResult(SkippedEntriesModelImpl.ENTITY_CACHE_ENABLED,
			SkippedEntriesImpl.class, skippedEntries.getPrimaryKey(),
			skippedEntries);

		skippedEntries.resetOriginalValues();
	}

	/**
	 * Caches the skipped entrieses in the entity cache if it is enabled.
	 *
	 * @param skippedEntrieses the skipped entrieses
	 */
	@Override
	public void cacheResult(List<SkippedEntries> skippedEntrieses) {
		for (SkippedEntries skippedEntries : skippedEntrieses) {
			if (EntityCacheUtil.getResult(
						SkippedEntriesModelImpl.ENTITY_CACHE_ENABLED,
						SkippedEntriesImpl.class, skippedEntries.getPrimaryKey()) == null) {
				cacheResult(skippedEntries);
			}
			else {
				skippedEntries.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all skipped entrieses.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(SkippedEntriesImpl.class.getName());
		}

		EntityCacheUtil.clearCache(SkippedEntriesImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the skipped entries.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(SkippedEntries skippedEntries) {
		EntityCacheUtil.removeResult(SkippedEntriesModelImpl.ENTITY_CACHE_ENABLED,
			SkippedEntriesImpl.class, skippedEntries.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<SkippedEntries> skippedEntrieses) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (SkippedEntries skippedEntries : skippedEntrieses) {
			EntityCacheUtil.removeResult(SkippedEntriesModelImpl.ENTITY_CACHE_ENABLED,
				SkippedEntriesImpl.class, skippedEntries.getPrimaryKey());
		}
	}

	/**
	 * Creates a new skipped entries with the primary key. Does not add the skipped entries to the database.
	 *
	 * @param id the primary key for the new skipped entries
	 * @return the new skipped entries
	 */
	@Override
	public SkippedEntries create(long id) {
		SkippedEntries skippedEntries = new SkippedEntriesImpl();

		skippedEntries.setNew(true);
		skippedEntries.setPrimaryKey(id);

		return skippedEntries;
	}

	/**
	 * Removes the skipped entries with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param id the primary key of the skipped entries
	 * @return the skipped entries that was removed
	 * @throws org.han.service.NoSuchSkippedEntriesException if a skipped entries with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public SkippedEntries remove(long id)
		throws NoSuchSkippedEntriesException, SystemException {
		return remove((Serializable)id);
	}

	/**
	 * Removes the skipped entries with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the skipped entries
	 * @return the skipped entries that was removed
	 * @throws org.han.service.NoSuchSkippedEntriesException if a skipped entries with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public SkippedEntries remove(Serializable primaryKey)
		throws NoSuchSkippedEntriesException, SystemException {
		Session session = null;

		try {
			session = openSession();

			SkippedEntries skippedEntries = (SkippedEntries)session.get(SkippedEntriesImpl.class,
					primaryKey);

			if (skippedEntries == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchSkippedEntriesException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(skippedEntries);
		}
		catch (NoSuchSkippedEntriesException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected SkippedEntries removeImpl(SkippedEntries skippedEntries)
		throws SystemException {
		skippedEntries = toUnwrappedModel(skippedEntries);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(skippedEntries)) {
				skippedEntries = (SkippedEntries)session.get(SkippedEntriesImpl.class,
						skippedEntries.getPrimaryKeyObj());
			}

			if (skippedEntries != null) {
				session.delete(skippedEntries);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (skippedEntries != null) {
			clearCache(skippedEntries);
		}

		return skippedEntries;
	}

	@Override
	public SkippedEntries updateImpl(
		org.han.service.model.SkippedEntries skippedEntries)
		throws SystemException {
		skippedEntries = toUnwrappedModel(skippedEntries);

		boolean isNew = skippedEntries.isNew();

		SkippedEntriesModelImpl skippedEntriesModelImpl = (SkippedEntriesModelImpl)skippedEntries;

		Session session = null;

		try {
			session = openSession();

			if (skippedEntries.isNew()) {
				session.save(skippedEntries);

				skippedEntries.setNew(false);
			}
			else {
				session.merge(skippedEntries);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !SkippedEntriesModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((skippedEntriesModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ENTITYNAMEANDOLDPK.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						skippedEntriesModelImpl.getOriginalTableName(),
						skippedEntriesModelImpl.getOriginalOldPK()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ENTITYNAMEANDOLDPK,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ENTITYNAMEANDOLDPK,
					args);

				args = new Object[] {
						skippedEntriesModelImpl.getTableName(),
						skippedEntriesModelImpl.getOldPK()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ENTITYNAMEANDOLDPK,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ENTITYNAMEANDOLDPK,
					args);
			}
		}

		EntityCacheUtil.putResult(SkippedEntriesModelImpl.ENTITY_CACHE_ENABLED,
			SkippedEntriesImpl.class, skippedEntries.getPrimaryKey(),
			skippedEntries);

		return skippedEntries;
	}

	protected SkippedEntries toUnwrappedModel(SkippedEntries skippedEntries) {
		if (skippedEntries instanceof SkippedEntriesImpl) {
			return skippedEntries;
		}

		SkippedEntriesImpl skippedEntriesImpl = new SkippedEntriesImpl();

		skippedEntriesImpl.setNew(skippedEntries.isNew());
		skippedEntriesImpl.setPrimaryKey(skippedEntries.getPrimaryKey());

		skippedEntriesImpl.setId(skippedEntries.getId());
		skippedEntriesImpl.setOldPK(skippedEntries.getOldPK());
		skippedEntriesImpl.setNewPK(skippedEntries.getNewPK());
		skippedEntriesImpl.setTableName(skippedEntries.getTableName());

		return skippedEntriesImpl;
	}

	/**
	 * Returns the skipped entries with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the skipped entries
	 * @return the skipped entries
	 * @throws org.han.service.NoSuchSkippedEntriesException if a skipped entries with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public SkippedEntries findByPrimaryKey(Serializable primaryKey)
		throws NoSuchSkippedEntriesException, SystemException {
		SkippedEntries skippedEntries = fetchByPrimaryKey(primaryKey);

		if (skippedEntries == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchSkippedEntriesException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return skippedEntries;
	}

	/**
	 * Returns the skipped entries with the primary key or throws a {@link org.han.service.NoSuchSkippedEntriesException} if it could not be found.
	 *
	 * @param id the primary key of the skipped entries
	 * @return the skipped entries
	 * @throws org.han.service.NoSuchSkippedEntriesException if a skipped entries with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public SkippedEntries findByPrimaryKey(long id)
		throws NoSuchSkippedEntriesException, SystemException {
		return findByPrimaryKey((Serializable)id);
	}

	/**
	 * Returns the skipped entries with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the skipped entries
	 * @return the skipped entries, or <code>null</code> if a skipped entries with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public SkippedEntries fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		SkippedEntries skippedEntries = (SkippedEntries)EntityCacheUtil.getResult(SkippedEntriesModelImpl.ENTITY_CACHE_ENABLED,
				SkippedEntriesImpl.class, primaryKey);

		if (skippedEntries == _nullSkippedEntries) {
			return null;
		}

		if (skippedEntries == null) {
			Session session = null;

			try {
				session = openSession();

				skippedEntries = (SkippedEntries)session.get(SkippedEntriesImpl.class,
						primaryKey);

				if (skippedEntries != null) {
					cacheResult(skippedEntries);
				}
				else {
					EntityCacheUtil.putResult(SkippedEntriesModelImpl.ENTITY_CACHE_ENABLED,
						SkippedEntriesImpl.class, primaryKey,
						_nullSkippedEntries);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(SkippedEntriesModelImpl.ENTITY_CACHE_ENABLED,
					SkippedEntriesImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return skippedEntries;
	}

	/**
	 * Returns the skipped entries with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param id the primary key of the skipped entries
	 * @return the skipped entries, or <code>null</code> if a skipped entries with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public SkippedEntries fetchByPrimaryKey(long id) throws SystemException {
		return fetchByPrimaryKey((Serializable)id);
	}

	/**
	 * Returns all the skipped entrieses.
	 *
	 * @return the skipped entrieses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<SkippedEntries> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the skipped entrieses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.SkippedEntriesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of skipped entrieses
	 * @param end the upper bound of the range of skipped entrieses (not inclusive)
	 * @return the range of skipped entrieses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<SkippedEntries> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the skipped entrieses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.SkippedEntriesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of skipped entrieses
	 * @param end the upper bound of the range of skipped entrieses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of skipped entrieses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<SkippedEntries> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<SkippedEntries> list = (List<SkippedEntries>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_SKIPPEDENTRIES);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_SKIPPEDENTRIES;

				if (pagination) {
					sql = sql.concat(SkippedEntriesModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<SkippedEntries>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<SkippedEntries>(list);
				}
				else {
					list = (List<SkippedEntries>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the skipped entrieses from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (SkippedEntries skippedEntries : findAll()) {
			remove(skippedEntries);
		}
	}

	/**
	 * Returns the number of skipped entrieses.
	 *
	 * @return the number of skipped entrieses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_SKIPPEDENTRIES);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the skipped entries persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.org.han.service.model.SkippedEntries")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<SkippedEntries>> listenersList = new ArrayList<ModelListener<SkippedEntries>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<SkippedEntries>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(SkippedEntriesImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_SKIPPEDENTRIES = "SELECT skippedEntries FROM SkippedEntries skippedEntries";
	private static final String _SQL_SELECT_SKIPPEDENTRIES_WHERE = "SELECT skippedEntries FROM SkippedEntries skippedEntries WHERE ";
	private static final String _SQL_COUNT_SKIPPEDENTRIES = "SELECT COUNT(skippedEntries) FROM SkippedEntries skippedEntries";
	private static final String _SQL_COUNT_SKIPPEDENTRIES_WHERE = "SELECT COUNT(skippedEntries) FROM SkippedEntries skippedEntries WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "skippedEntries.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No SkippedEntries exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No SkippedEntries exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(SkippedEntriesPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"id"
			});
	private static SkippedEntries _nullSkippedEntries = new SkippedEntriesImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<SkippedEntries> toCacheModel() {
				return _nullSkippedEntriesCacheModel;
			}
		};

	private static CacheModel<SkippedEntries> _nullSkippedEntriesCacheModel = new CacheModel<SkippedEntries>() {
			@Override
			public SkippedEntries toEntityModel() {
				return _nullSkippedEntries;
			}
		};
}