/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.han.service.service.base;

import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.bean.IdentifiableBean;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdate;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdateFactoryUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.model.PersistedModel;
import com.liferay.portal.service.BaseLocalServiceImpl;
import com.liferay.portal.service.PersistedModelLocalServiceRegistryUtil;
import com.liferay.portal.service.persistence.UserPersistence;

import org.han.service.model.AuditTrail;
import org.han.service.service.AuditTrailLocalService;
import org.han.service.service.persistence.AuditTrailPersistence;
import org.han.service.service.persistence.MigrationSummaryPersistence;
import org.han.service.service.persistence.OldNewMappingPersistence;
import org.han.service.service.persistence.SkippedEntriesPersistence;

import java.io.Serializable;

import java.util.List;

import javax.sql.DataSource;

/**
 * Provides the base implementation for the audit trail local service.
 *
 * <p>
 * This implementation exists only as a container for the default service methods generated by ServiceBuilder. All custom service methods should be put in {@link org.han.service.service.impl.AuditTrailLocalServiceImpl}.
 * </p>
 *
 * @author Rohan
 * @see org.han.service.service.impl.AuditTrailLocalServiceImpl
 * @see org.han.service.service.AuditTrailLocalServiceUtil
 * @generated
 */
public abstract class AuditTrailLocalServiceBaseImpl
	extends BaseLocalServiceImpl implements AuditTrailLocalService,
		IdentifiableBean {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link org.han.service.service.AuditTrailLocalServiceUtil} to access the audit trail local service.
	 */

	/**
	 * Adds the audit trail to the database. Also notifies the appropriate model listeners.
	 *
	 * @param auditTrail the audit trail
	 * @return the audit trail that was added
	 * @throws SystemException if a system exception occurred
	 */
	@Indexable(type = IndexableType.REINDEX)
	@Override
	public AuditTrail addAuditTrail(AuditTrail auditTrail)
		throws SystemException {
		auditTrail.setNew(true);

		return auditTrailPersistence.update(auditTrail);
	}

	/**
	 * Creates a new audit trail with the primary key. Does not add the audit trail to the database.
	 *
	 * @param id the primary key for the new audit trail
	 * @return the new audit trail
	 */
	@Override
	public AuditTrail createAuditTrail(long id) {
		return auditTrailPersistence.create(id);
	}

	/**
	 * Deletes the audit trail with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param id the primary key of the audit trail
	 * @return the audit trail that was removed
	 * @throws PortalException if a audit trail with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Indexable(type = IndexableType.DELETE)
	@Override
	public AuditTrail deleteAuditTrail(long id)
		throws PortalException, SystemException {
		return auditTrailPersistence.remove(id);
	}

	/**
	 * Deletes the audit trail from the database. Also notifies the appropriate model listeners.
	 *
	 * @param auditTrail the audit trail
	 * @return the audit trail that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Indexable(type = IndexableType.DELETE)
	@Override
	public AuditTrail deleteAuditTrail(AuditTrail auditTrail)
		throws SystemException {
		return auditTrailPersistence.remove(auditTrail);
	}

	@Override
	public DynamicQuery dynamicQuery() {
		Class<?> clazz = getClass();

		return DynamicQueryFactoryUtil.forClass(AuditTrail.class,
			clazz.getClassLoader());
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	@SuppressWarnings("rawtypes")
	public List dynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return auditTrailPersistence.findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.AuditTrailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	@SuppressWarnings("rawtypes")
	public List dynamicQuery(DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return auditTrailPersistence.findWithDynamicQuery(dynamicQuery, start,
			end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.AuditTrailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	@SuppressWarnings("rawtypes")
	public List dynamicQuery(DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return auditTrailPersistence.findWithDynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	 * Returns the number of rows that match the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows that match the dynamic query
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public long dynamicQueryCount(DynamicQuery dynamicQuery)
		throws SystemException {
		return auditTrailPersistence.countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * Returns the number of rows that match the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows that match the dynamic query
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public long dynamicQueryCount(DynamicQuery dynamicQuery,
		Projection projection) throws SystemException {
		return auditTrailPersistence.countWithDynamicQuery(dynamicQuery,
			projection);
	}

	@Override
	public AuditTrail fetchAuditTrail(long id) throws SystemException {
		return auditTrailPersistence.fetchByPrimaryKey(id);
	}

	/**
	 * Returns the audit trail with the primary key.
	 *
	 * @param id the primary key of the audit trail
	 * @return the audit trail
	 * @throws PortalException if a audit trail with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AuditTrail getAuditTrail(long id)
		throws PortalException, SystemException {
		return auditTrailPersistence.findByPrimaryKey(id);
	}

	@Override
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException, SystemException {
		return auditTrailPersistence.findByPrimaryKey(primaryKeyObj);
	}

	/**
	 * Returns a range of all the audit trails.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.AuditTrailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of audit trails
	 * @param end the upper bound of the range of audit trails (not inclusive)
	 * @return the range of audit trails
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<AuditTrail> getAuditTrails(int start, int end)
		throws SystemException {
		return auditTrailPersistence.findAll(start, end);
	}

	/**
	 * Returns the number of audit trails.
	 *
	 * @return the number of audit trails
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int getAuditTrailsCount() throws SystemException {
		return auditTrailPersistence.countAll();
	}

	/**
	 * Updates the audit trail in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param auditTrail the audit trail
	 * @return the audit trail that was updated
	 * @throws SystemException if a system exception occurred
	 */
	@Indexable(type = IndexableType.REINDEX)
	@Override
	public AuditTrail updateAuditTrail(AuditTrail auditTrail)
		throws SystemException {
		return auditTrailPersistence.update(auditTrail);
	}

	/**
	 * Returns the audit trail local service.
	 *
	 * @return the audit trail local service
	 */
	public org.han.service.service.AuditTrailLocalService getAuditTrailLocalService() {
		return auditTrailLocalService;
	}

	/**
	 * Sets the audit trail local service.
	 *
	 * @param auditTrailLocalService the audit trail local service
	 */
	public void setAuditTrailLocalService(
		org.han.service.service.AuditTrailLocalService auditTrailLocalService) {
		this.auditTrailLocalService = auditTrailLocalService;
	}

	/**
	 * Returns the audit trail persistence.
	 *
	 * @return the audit trail persistence
	 */
	public AuditTrailPersistence getAuditTrailPersistence() {
		return auditTrailPersistence;
	}

	/**
	 * Sets the audit trail persistence.
	 *
	 * @param auditTrailPersistence the audit trail persistence
	 */
	public void setAuditTrailPersistence(
		AuditTrailPersistence auditTrailPersistence) {
		this.auditTrailPersistence = auditTrailPersistence;
	}

	/**
	 * Returns the migration summary local service.
	 *
	 * @return the migration summary local service
	 */
	public org.han.service.service.MigrationSummaryLocalService getMigrationSummaryLocalService() {
		return migrationSummaryLocalService;
	}

	/**
	 * Sets the migration summary local service.
	 *
	 * @param migrationSummaryLocalService the migration summary local service
	 */
	public void setMigrationSummaryLocalService(
		org.han.service.service.MigrationSummaryLocalService migrationSummaryLocalService) {
		this.migrationSummaryLocalService = migrationSummaryLocalService;
	}

	/**
	 * Returns the migration summary persistence.
	 *
	 * @return the migration summary persistence
	 */
	public MigrationSummaryPersistence getMigrationSummaryPersistence() {
		return migrationSummaryPersistence;
	}

	/**
	 * Sets the migration summary persistence.
	 *
	 * @param migrationSummaryPersistence the migration summary persistence
	 */
	public void setMigrationSummaryPersistence(
		MigrationSummaryPersistence migrationSummaryPersistence) {
		this.migrationSummaryPersistence = migrationSummaryPersistence;
	}

	/**
	 * Returns the old new mapping local service.
	 *
	 * @return the old new mapping local service
	 */
	public org.han.service.service.OldNewMappingLocalService getOldNewMappingLocalService() {
		return oldNewMappingLocalService;
	}

	/**
	 * Sets the old new mapping local service.
	 *
	 * @param oldNewMappingLocalService the old new mapping local service
	 */
	public void setOldNewMappingLocalService(
		org.han.service.service.OldNewMappingLocalService oldNewMappingLocalService) {
		this.oldNewMappingLocalService = oldNewMappingLocalService;
	}

	/**
	 * Returns the old new mapping persistence.
	 *
	 * @return the old new mapping persistence
	 */
	public OldNewMappingPersistence getOldNewMappingPersistence() {
		return oldNewMappingPersistence;
	}

	/**
	 * Sets the old new mapping persistence.
	 *
	 * @param oldNewMappingPersistence the old new mapping persistence
	 */
	public void setOldNewMappingPersistence(
		OldNewMappingPersistence oldNewMappingPersistence) {
		this.oldNewMappingPersistence = oldNewMappingPersistence;
	}

	/**
	 * Returns the skipped entries local service.
	 *
	 * @return the skipped entries local service
	 */
	public org.han.service.service.SkippedEntriesLocalService getSkippedEntriesLocalService() {
		return skippedEntriesLocalService;
	}

	/**
	 * Sets the skipped entries local service.
	 *
	 * @param skippedEntriesLocalService the skipped entries local service
	 */
	public void setSkippedEntriesLocalService(
		org.han.service.service.SkippedEntriesLocalService skippedEntriesLocalService) {
		this.skippedEntriesLocalService = skippedEntriesLocalService;
	}

	/**
	 * Returns the skipped entries persistence.
	 *
	 * @return the skipped entries persistence
	 */
	public SkippedEntriesPersistence getSkippedEntriesPersistence() {
		return skippedEntriesPersistence;
	}

	/**
	 * Sets the skipped entries persistence.
	 *
	 * @param skippedEntriesPersistence the skipped entries persistence
	 */
	public void setSkippedEntriesPersistence(
		SkippedEntriesPersistence skippedEntriesPersistence) {
		this.skippedEntriesPersistence = skippedEntriesPersistence;
	}

	/**
	 * Returns the counter local service.
	 *
	 * @return the counter local service
	 */
	public com.liferay.counter.service.CounterLocalService getCounterLocalService() {
		return counterLocalService;
	}

	/**
	 * Sets the counter local service.
	 *
	 * @param counterLocalService the counter local service
	 */
	public void setCounterLocalService(
		com.liferay.counter.service.CounterLocalService counterLocalService) {
		this.counterLocalService = counterLocalService;
	}

	/**
	 * Returns the resource local service.
	 *
	 * @return the resource local service
	 */
	public com.liferay.portal.service.ResourceLocalService getResourceLocalService() {
		return resourceLocalService;
	}

	/**
	 * Sets the resource local service.
	 *
	 * @param resourceLocalService the resource local service
	 */
	public void setResourceLocalService(
		com.liferay.portal.service.ResourceLocalService resourceLocalService) {
		this.resourceLocalService = resourceLocalService;
	}

	/**
	 * Returns the user local service.
	 *
	 * @return the user local service
	 */
	public com.liferay.portal.service.UserLocalService getUserLocalService() {
		return userLocalService;
	}

	/**
	 * Sets the user local service.
	 *
	 * @param userLocalService the user local service
	 */
	public void setUserLocalService(
		com.liferay.portal.service.UserLocalService userLocalService) {
		this.userLocalService = userLocalService;
	}

	/**
	 * Returns the user remote service.
	 *
	 * @return the user remote service
	 */
	public com.liferay.portal.service.UserService getUserService() {
		return userService;
	}

	/**
	 * Sets the user remote service.
	 *
	 * @param userService the user remote service
	 */
	public void setUserService(
		com.liferay.portal.service.UserService userService) {
		this.userService = userService;
	}

	/**
	 * Returns the user persistence.
	 *
	 * @return the user persistence
	 */
	public UserPersistence getUserPersistence() {
		return userPersistence;
	}

	/**
	 * Sets the user persistence.
	 *
	 * @param userPersistence the user persistence
	 */
	public void setUserPersistence(UserPersistence userPersistence) {
		this.userPersistence = userPersistence;
	}

	public void afterPropertiesSet() {
		Class<?> clazz = getClass();

		_classLoader = clazz.getClassLoader();

		PersistedModelLocalServiceRegistryUtil.register("org.han.service.model.AuditTrail",
			auditTrailLocalService);
	}

	public void destroy() {
		PersistedModelLocalServiceRegistryUtil.unregister(
			"org.han.service.model.AuditTrail");
	}

	/**
	 * Returns the Spring bean ID for this bean.
	 *
	 * @return the Spring bean ID for this bean
	 */
	@Override
	public String getBeanIdentifier() {
		return _beanIdentifier;
	}

	/**
	 * Sets the Spring bean ID for this bean.
	 *
	 * @param beanIdentifier the Spring bean ID for this bean
	 */
	@Override
	public void setBeanIdentifier(String beanIdentifier) {
		_beanIdentifier = beanIdentifier;
	}

	@Override
	public Object invokeMethod(String name, String[] parameterTypes,
		Object[] arguments) throws Throwable {
		Thread currentThread = Thread.currentThread();

		ClassLoader contextClassLoader = currentThread.getContextClassLoader();

		if (contextClassLoader != _classLoader) {
			currentThread.setContextClassLoader(_classLoader);
		}

		try {
			return _clpInvoker.invokeMethod(name, parameterTypes, arguments);
		}
		finally {
			if (contextClassLoader != _classLoader) {
				currentThread.setContextClassLoader(contextClassLoader);
			}
		}
	}

	protected Class<?> getModelClass() {
		return AuditTrail.class;
	}

	protected String getModelClassName() {
		return AuditTrail.class.getName();
	}

	/**
	 * Performs an SQL query.
	 *
	 * @param sql the sql query
	 */
	protected void runSQL(String sql) throws SystemException {
		try {
			DataSource dataSource = auditTrailPersistence.getDataSource();

			SqlUpdate sqlUpdate = SqlUpdateFactoryUtil.getSqlUpdate(dataSource,
					sql, new int[0]);

			sqlUpdate.update();
		}
		catch (Exception e) {
			throw new SystemException(e);
		}
	}

	@BeanReference(type = org.han.service.service.AuditTrailLocalService.class)
	protected org.han.service.service.AuditTrailLocalService auditTrailLocalService;
	@BeanReference(type = AuditTrailPersistence.class)
	protected AuditTrailPersistence auditTrailPersistence;
	@BeanReference(type = org.han.service.service.MigrationSummaryLocalService.class)
	protected org.han.service.service.MigrationSummaryLocalService migrationSummaryLocalService;
	@BeanReference(type = MigrationSummaryPersistence.class)
	protected MigrationSummaryPersistence migrationSummaryPersistence;
	@BeanReference(type = org.han.service.service.OldNewMappingLocalService.class)
	protected org.han.service.service.OldNewMappingLocalService oldNewMappingLocalService;
	@BeanReference(type = OldNewMappingPersistence.class)
	protected OldNewMappingPersistence oldNewMappingPersistence;
	@BeanReference(type = org.han.service.service.SkippedEntriesLocalService.class)
	protected org.han.service.service.SkippedEntriesLocalService skippedEntriesLocalService;
	@BeanReference(type = SkippedEntriesPersistence.class)
	protected SkippedEntriesPersistence skippedEntriesPersistence;
	@BeanReference(type = com.liferay.counter.service.CounterLocalService.class)
	protected com.liferay.counter.service.CounterLocalService counterLocalService;
	@BeanReference(type = com.liferay.portal.service.ResourceLocalService.class)
	protected com.liferay.portal.service.ResourceLocalService resourceLocalService;
	@BeanReference(type = com.liferay.portal.service.UserLocalService.class)
	protected com.liferay.portal.service.UserLocalService userLocalService;
	@BeanReference(type = com.liferay.portal.service.UserService.class)
	protected com.liferay.portal.service.UserService userService;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private String _beanIdentifier;
	private ClassLoader _classLoader;
	private AuditTrailLocalServiceClpInvoker _clpInvoker = new AuditTrailLocalServiceClpInvoker();
}