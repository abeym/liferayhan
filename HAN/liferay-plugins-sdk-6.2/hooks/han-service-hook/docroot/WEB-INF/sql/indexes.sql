create index IX_EE6813E1 on HAN_MigrationSummary (TableName);

create index IX_53621E9A on HAN_OldNewMapping (TableName, oldPK);

create index IX_973E3359 on HAN_SkippedEntries (TableName, oldPK);