create table HAN_AuditTrail (
	id_ LONG not null primary key,
	eventType VARCHAR(75) null,
	className VARCHAR(75) null,
	classPK VARCHAR(75) null,
	realUserId VARCHAR(75) null,
	realUserName VARCHAR(75) null,
	realUserEmail VARCHAR(75) null,
	userId VARCHAR(75) null,
	userName VARCHAR(75) null,
	userEmail VARCHAR(75) null,
	createDate DATE null,
	message STRING null
);

create table HAN_MigrationSummary (
	id_ LONG not null primary key,
	startTime DATE null,
	endTime DATE null,
	TableName VARCHAR(75) null,
	added LONG,
	skipped LONG,
	totalFound LONG
);

create table HAN_OldNewMapping (
	id_ LONG not null primary key,
	oldPK LONG,
	newPK LONG,
	TableName VARCHAR(75) null
);

create table HAN_SkippedEntries (
	id_ LONG not null primary key,
	oldPK LONG,
	newPK LONG,
	TableName VARCHAR(75) null
);