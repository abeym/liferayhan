/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.han.service.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import org.han.service.model.OldNewMapping;

import java.util.List;

/**
 * The persistence utility for the old new mapping service. This utility wraps {@link OldNewMappingPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Rohan
 * @see OldNewMappingPersistence
 * @see OldNewMappingPersistenceImpl
 * @generated
 */
public class OldNewMappingUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(OldNewMapping oldNewMapping) {
		getPersistence().clearCache(oldNewMapping);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<OldNewMapping> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<OldNewMapping> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<OldNewMapping> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static OldNewMapping update(OldNewMapping oldNewMapping)
		throws SystemException {
		return getPersistence().update(oldNewMapping);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static OldNewMapping update(OldNewMapping oldNewMapping,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(oldNewMapping, serviceContext);
	}

	/**
	* Returns all the old new mappings where TableName = &#63; and oldPK = &#63;.
	*
	* @param TableName the table name
	* @param oldPK the old p k
	* @return the matching old new mappings
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<org.han.service.model.OldNewMapping> findByentityNameAndOldPK(
		java.lang.String TableName, long oldPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByentityNameAndOldPK(TableName, oldPK);
	}

	/**
	* Returns a range of all the old new mappings where TableName = &#63; and oldPK = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.OldNewMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param TableName the table name
	* @param oldPK the old p k
	* @param start the lower bound of the range of old new mappings
	* @param end the upper bound of the range of old new mappings (not inclusive)
	* @return the range of matching old new mappings
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<org.han.service.model.OldNewMapping> findByentityNameAndOldPK(
		java.lang.String TableName, long oldPK, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByentityNameAndOldPK(TableName, oldPK, start, end);
	}

	/**
	* Returns an ordered range of all the old new mappings where TableName = &#63; and oldPK = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.OldNewMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param TableName the table name
	* @param oldPK the old p k
	* @param start the lower bound of the range of old new mappings
	* @param end the upper bound of the range of old new mappings (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching old new mappings
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<org.han.service.model.OldNewMapping> findByentityNameAndOldPK(
		java.lang.String TableName, long oldPK, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByentityNameAndOldPK(TableName, oldPK, start, end,
			orderByComparator);
	}

	/**
	* Returns the first old new mapping in the ordered set where TableName = &#63; and oldPK = &#63;.
	*
	* @param TableName the table name
	* @param oldPK the old p k
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching old new mapping
	* @throws org.han.service.NoSuchOldNewMappingException if a matching old new mapping could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static org.han.service.model.OldNewMapping findByentityNameAndOldPK_First(
		java.lang.String TableName, long oldPK,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.han.service.NoSuchOldNewMappingException {
		return getPersistence()
				   .findByentityNameAndOldPK_First(TableName, oldPK,
			orderByComparator);
	}

	/**
	* Returns the first old new mapping in the ordered set where TableName = &#63; and oldPK = &#63;.
	*
	* @param TableName the table name
	* @param oldPK the old p k
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching old new mapping, or <code>null</code> if a matching old new mapping could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static org.han.service.model.OldNewMapping fetchByentityNameAndOldPK_First(
		java.lang.String TableName, long oldPK,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByentityNameAndOldPK_First(TableName, oldPK,
			orderByComparator);
	}

	/**
	* Returns the last old new mapping in the ordered set where TableName = &#63; and oldPK = &#63;.
	*
	* @param TableName the table name
	* @param oldPK the old p k
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching old new mapping
	* @throws org.han.service.NoSuchOldNewMappingException if a matching old new mapping could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static org.han.service.model.OldNewMapping findByentityNameAndOldPK_Last(
		java.lang.String TableName, long oldPK,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.han.service.NoSuchOldNewMappingException {
		return getPersistence()
				   .findByentityNameAndOldPK_Last(TableName, oldPK,
			orderByComparator);
	}

	/**
	* Returns the last old new mapping in the ordered set where TableName = &#63; and oldPK = &#63;.
	*
	* @param TableName the table name
	* @param oldPK the old p k
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching old new mapping, or <code>null</code> if a matching old new mapping could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static org.han.service.model.OldNewMapping fetchByentityNameAndOldPK_Last(
		java.lang.String TableName, long oldPK,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByentityNameAndOldPK_Last(TableName, oldPK,
			orderByComparator);
	}

	/**
	* Returns the old new mappings before and after the current old new mapping in the ordered set where TableName = &#63; and oldPK = &#63;.
	*
	* @param id the primary key of the current old new mapping
	* @param TableName the table name
	* @param oldPK the old p k
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next old new mapping
	* @throws org.han.service.NoSuchOldNewMappingException if a old new mapping with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static org.han.service.model.OldNewMapping[] findByentityNameAndOldPK_PrevAndNext(
		long id, java.lang.String TableName, long oldPK,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.han.service.NoSuchOldNewMappingException {
		return getPersistence()
				   .findByentityNameAndOldPK_PrevAndNext(id, TableName, oldPK,
			orderByComparator);
	}

	/**
	* Removes all the old new mappings where TableName = &#63; and oldPK = &#63; from the database.
	*
	* @param TableName the table name
	* @param oldPK the old p k
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByentityNameAndOldPK(java.lang.String TableName,
		long oldPK) throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByentityNameAndOldPK(TableName, oldPK);
	}

	/**
	* Returns the number of old new mappings where TableName = &#63; and oldPK = &#63;.
	*
	* @param TableName the table name
	* @param oldPK the old p k
	* @return the number of matching old new mappings
	* @throws SystemException if a system exception occurred
	*/
	public static int countByentityNameAndOldPK(java.lang.String TableName,
		long oldPK) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByentityNameAndOldPK(TableName, oldPK);
	}

	/**
	* Caches the old new mapping in the entity cache if it is enabled.
	*
	* @param oldNewMapping the old new mapping
	*/
	public static void cacheResult(
		org.han.service.model.OldNewMapping oldNewMapping) {
		getPersistence().cacheResult(oldNewMapping);
	}

	/**
	* Caches the old new mappings in the entity cache if it is enabled.
	*
	* @param oldNewMappings the old new mappings
	*/
	public static void cacheResult(
		java.util.List<org.han.service.model.OldNewMapping> oldNewMappings) {
		getPersistence().cacheResult(oldNewMappings);
	}

	/**
	* Creates a new old new mapping with the primary key. Does not add the old new mapping to the database.
	*
	* @param id the primary key for the new old new mapping
	* @return the new old new mapping
	*/
	public static org.han.service.model.OldNewMapping create(long id) {
		return getPersistence().create(id);
	}

	/**
	* Removes the old new mapping with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param id the primary key of the old new mapping
	* @return the old new mapping that was removed
	* @throws org.han.service.NoSuchOldNewMappingException if a old new mapping with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static org.han.service.model.OldNewMapping remove(long id)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.han.service.NoSuchOldNewMappingException {
		return getPersistence().remove(id);
	}

	public static org.han.service.model.OldNewMapping updateImpl(
		org.han.service.model.OldNewMapping oldNewMapping)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(oldNewMapping);
	}

	/**
	* Returns the old new mapping with the primary key or throws a {@link org.han.service.NoSuchOldNewMappingException} if it could not be found.
	*
	* @param id the primary key of the old new mapping
	* @return the old new mapping
	* @throws org.han.service.NoSuchOldNewMappingException if a old new mapping with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static org.han.service.model.OldNewMapping findByPrimaryKey(long id)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.han.service.NoSuchOldNewMappingException {
		return getPersistence().findByPrimaryKey(id);
	}

	/**
	* Returns the old new mapping with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param id the primary key of the old new mapping
	* @return the old new mapping, or <code>null</code> if a old new mapping with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static org.han.service.model.OldNewMapping fetchByPrimaryKey(long id)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(id);
	}

	/**
	* Returns all the old new mappings.
	*
	* @return the old new mappings
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<org.han.service.model.OldNewMapping> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the old new mappings.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.OldNewMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of old new mappings
	* @param end the upper bound of the range of old new mappings (not inclusive)
	* @return the range of old new mappings
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<org.han.service.model.OldNewMapping> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the old new mappings.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.OldNewMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of old new mappings
	* @param end the upper bound of the range of old new mappings (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of old new mappings
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<org.han.service.model.OldNewMapping> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the old new mappings from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of old new mappings.
	*
	* @return the number of old new mappings
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static OldNewMappingPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (OldNewMappingPersistence)PortletBeanLocatorUtil.locate(org.han.service.service.ClpSerializer.getServletContextName(),
					OldNewMappingPersistence.class.getName());

			ReferenceRegistry.registerReference(OldNewMappingUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(OldNewMappingPersistence persistence) {
	}

	private static OldNewMappingPersistence _persistence;
}