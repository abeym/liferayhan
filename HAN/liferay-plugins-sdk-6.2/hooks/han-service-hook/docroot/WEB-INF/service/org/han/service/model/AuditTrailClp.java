/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.han.service.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import org.han.service.service.AuditTrailLocalServiceUtil;
import org.han.service.service.ClpSerializer;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Rohan
 */
public class AuditTrailClp extends BaseModelImpl<AuditTrail>
	implements AuditTrail {
	public AuditTrailClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return AuditTrail.class;
	}

	@Override
	public String getModelClassName() {
		return AuditTrail.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _id;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _id;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("id", getId());
		attributes.put("eventType", getEventType());
		attributes.put("className", getClassName());
		attributes.put("classPK", getClassPK());
		attributes.put("realUserId", getRealUserId());
		attributes.put("realUserName", getRealUserName());
		attributes.put("realUserEmail", getRealUserEmail());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("userEmail", getUserEmail());
		attributes.put("createDate", getCreateDate());
		attributes.put("message", getMessage());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long id = (Long)attributes.get("id");

		if (id != null) {
			setId(id);
		}

		String eventType = (String)attributes.get("eventType");

		if (eventType != null) {
			setEventType(eventType);
		}

		String className = (String)attributes.get("className");

		if (className != null) {
			setClassName(className);
		}

		String classPK = (String)attributes.get("classPK");

		if (classPK != null) {
			setClassPK(classPK);
		}

		String realUserId = (String)attributes.get("realUserId");

		if (realUserId != null) {
			setRealUserId(realUserId);
		}

		String realUserName = (String)attributes.get("realUserName");

		if (realUserName != null) {
			setRealUserName(realUserName);
		}

		String realUserEmail = (String)attributes.get("realUserEmail");

		if (realUserEmail != null) {
			setRealUserEmail(realUserEmail);
		}

		String userId = (String)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		String userEmail = (String)attributes.get("userEmail");

		if (userEmail != null) {
			setUserEmail(userEmail);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		String message = (String)attributes.get("message");

		if (message != null) {
			setMessage(message);
		}
	}

	@Override
	public long getId() {
		return _id;
	}

	@Override
	public void setId(long id) {
		_id = id;

		if (_auditTrailRemoteModel != null) {
			try {
				Class<?> clazz = _auditTrailRemoteModel.getClass();

				Method method = clazz.getMethod("setId", long.class);

				method.invoke(_auditTrailRemoteModel, id);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getEventType() {
		return _eventType;
	}

	@Override
	public void setEventType(String eventType) {
		_eventType = eventType;

		if (_auditTrailRemoteModel != null) {
			try {
				Class<?> clazz = _auditTrailRemoteModel.getClass();

				Method method = clazz.getMethod("setEventType", String.class);

				method.invoke(_auditTrailRemoteModel, eventType);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getClassName() {
		return _className;
	}

	@Override
	public void setClassName(String className) {
		_className = className;

		if (_auditTrailRemoteModel != null) {
			try {
				Class<?> clazz = _auditTrailRemoteModel.getClass();

				Method method = clazz.getMethod("setClassName", String.class);

				method.invoke(_auditTrailRemoteModel, className);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getClassPK() {
		return _classPK;
	}

	@Override
	public void setClassPK(String classPK) {
		_classPK = classPK;

		if (_auditTrailRemoteModel != null) {
			try {
				Class<?> clazz = _auditTrailRemoteModel.getClass();

				Method method = clazz.getMethod("setClassPK", String.class);

				method.invoke(_auditTrailRemoteModel, classPK);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getRealUserId() {
		return _realUserId;
	}

	@Override
	public void setRealUserId(String realUserId) {
		_realUserId = realUserId;

		if (_auditTrailRemoteModel != null) {
			try {
				Class<?> clazz = _auditTrailRemoteModel.getClass();

				Method method = clazz.getMethod("setRealUserId", String.class);

				method.invoke(_auditTrailRemoteModel, realUserId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getRealUserName() {
		return _realUserName;
	}

	@Override
	public void setRealUserName(String realUserName) {
		_realUserName = realUserName;

		if (_auditTrailRemoteModel != null) {
			try {
				Class<?> clazz = _auditTrailRemoteModel.getClass();

				Method method = clazz.getMethod("setRealUserName", String.class);

				method.invoke(_auditTrailRemoteModel, realUserName);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getRealUserEmail() {
		return _realUserEmail;
	}

	@Override
	public void setRealUserEmail(String realUserEmail) {
		_realUserEmail = realUserEmail;

		if (_auditTrailRemoteModel != null) {
			try {
				Class<?> clazz = _auditTrailRemoteModel.getClass();

				Method method = clazz.getMethod("setRealUserEmail", String.class);

				method.invoke(_auditTrailRemoteModel, realUserEmail);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUserId() {
		return _userId;
	}

	@Override
	public void setUserId(String userId) {
		_userId = userId;

		if (_auditTrailRemoteModel != null) {
			try {
				Class<?> clazz = _auditTrailRemoteModel.getClass();

				Method method = clazz.getMethod("setUserId", String.class);

				method.invoke(_auditTrailRemoteModel, userId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUserName() {
		return _userName;
	}

	@Override
	public void setUserName(String userName) {
		_userName = userName;

		if (_auditTrailRemoteModel != null) {
			try {
				Class<?> clazz = _auditTrailRemoteModel.getClass();

				Method method = clazz.getMethod("setUserName", String.class);

				method.invoke(_auditTrailRemoteModel, userName);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUserEmail() {
		return _userEmail;
	}

	@Override
	public void setUserEmail(String userEmail) {
		_userEmail = userEmail;

		if (_auditTrailRemoteModel != null) {
			try {
				Class<?> clazz = _auditTrailRemoteModel.getClass();

				Method method = clazz.getMethod("setUserEmail", String.class);

				method.invoke(_auditTrailRemoteModel, userEmail);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getCreateDate() {
		return _createDate;
	}

	@Override
	public void setCreateDate(Date createDate) {
		_createDate = createDate;

		if (_auditTrailRemoteModel != null) {
			try {
				Class<?> clazz = _auditTrailRemoteModel.getClass();

				Method method = clazz.getMethod("setCreateDate", Date.class);

				method.invoke(_auditTrailRemoteModel, createDate);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getMessage() {
		return _message;
	}

	@Override
	public void setMessage(String message) {
		_message = message;

		if (_auditTrailRemoteModel != null) {
			try {
				Class<?> clazz = _auditTrailRemoteModel.getClass();

				Method method = clazz.getMethod("setMessage", String.class);

				method.invoke(_auditTrailRemoteModel, message);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getAuditTrailRemoteModel() {
		return _auditTrailRemoteModel;
	}

	public void setAuditTrailRemoteModel(BaseModel<?> auditTrailRemoteModel) {
		_auditTrailRemoteModel = auditTrailRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _auditTrailRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_auditTrailRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			AuditTrailLocalServiceUtil.addAuditTrail(this);
		}
		else {
			AuditTrailLocalServiceUtil.updateAuditTrail(this);
		}
	}

	@Override
	public AuditTrail toEscapedModel() {
		return (AuditTrail)ProxyUtil.newProxyInstance(AuditTrail.class.getClassLoader(),
			new Class[] { AuditTrail.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		AuditTrailClp clone = new AuditTrailClp();

		clone.setId(getId());
		clone.setEventType(getEventType());
		clone.setClassName(getClassName());
		clone.setClassPK(getClassPK());
		clone.setRealUserId(getRealUserId());
		clone.setRealUserName(getRealUserName());
		clone.setRealUserEmail(getRealUserEmail());
		clone.setUserId(getUserId());
		clone.setUserName(getUserName());
		clone.setUserEmail(getUserEmail());
		clone.setCreateDate(getCreateDate());
		clone.setMessage(getMessage());

		return clone;
	}

	@Override
	public int compareTo(AuditTrail auditTrail) {
		int value = 0;

		if (getId() < auditTrail.getId()) {
			value = -1;
		}
		else if (getId() > auditTrail.getId()) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof AuditTrailClp)) {
			return false;
		}

		AuditTrailClp auditTrail = (AuditTrailClp)obj;

		long primaryKey = auditTrail.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(25);

		sb.append("{id=");
		sb.append(getId());
		sb.append(", eventType=");
		sb.append(getEventType());
		sb.append(", className=");
		sb.append(getClassName());
		sb.append(", classPK=");
		sb.append(getClassPK());
		sb.append(", realUserId=");
		sb.append(getRealUserId());
		sb.append(", realUserName=");
		sb.append(getRealUserName());
		sb.append(", realUserEmail=");
		sb.append(getRealUserEmail());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", userName=");
		sb.append(getUserName());
		sb.append(", userEmail=");
		sb.append(getUserEmail());
		sb.append(", createDate=");
		sb.append(getCreateDate());
		sb.append(", message=");
		sb.append(getMessage());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(40);

		sb.append("<model><model-name>");
		sb.append("org.han.service.model.AuditTrail");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>id</column-name><column-value><![CDATA[");
		sb.append(getId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>eventType</column-name><column-value><![CDATA[");
		sb.append(getEventType());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>className</column-name><column-value><![CDATA[");
		sb.append(getClassName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>classPK</column-name><column-value><![CDATA[");
		sb.append(getClassPK());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>realUserId</column-name><column-value><![CDATA[");
		sb.append(getRealUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>realUserName</column-name><column-value><![CDATA[");
		sb.append(getRealUserName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>realUserEmail</column-name><column-value><![CDATA[");
		sb.append(getRealUserEmail());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userName</column-name><column-value><![CDATA[");
		sb.append(getUserName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userEmail</column-name><column-value><![CDATA[");
		sb.append(getUserEmail());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>createDate</column-name><column-value><![CDATA[");
		sb.append(getCreateDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>message</column-name><column-value><![CDATA[");
		sb.append(getMessage());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _id;
	private String _eventType;
	private String _className;
	private String _classPK;
	private String _realUserId;
	private String _realUserName;
	private String _realUserEmail;
	private String _userId;
	private String _userName;
	private String _userEmail;
	private Date _createDate;
	private String _message;
	private BaseModel<?> _auditTrailRemoteModel;
	private Class<?> _clpSerializerClass = org.han.service.service.ClpSerializer.class;
}