/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.han.service.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import org.han.service.model.MigrationSummary;

import java.util.List;

/**
 * The persistence utility for the migration summary service. This utility wraps {@link MigrationSummaryPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Rohan
 * @see MigrationSummaryPersistence
 * @see MigrationSummaryPersistenceImpl
 * @generated
 */
public class MigrationSummaryUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(MigrationSummary migrationSummary) {
		getPersistence().clearCache(migrationSummary);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<MigrationSummary> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<MigrationSummary> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<MigrationSummary> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static MigrationSummary update(MigrationSummary migrationSummary)
		throws SystemException {
		return getPersistence().update(migrationSummary);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static MigrationSummary update(MigrationSummary migrationSummary,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(migrationSummary, serviceContext);
	}

	/**
	* Returns all the migration summaries where TableName = &#63;.
	*
	* @param TableName the table name
	* @return the matching migration summaries
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<org.han.service.model.MigrationSummary> findBytableName(
		java.lang.String TableName)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBytableName(TableName);
	}

	/**
	* Returns a range of all the migration summaries where TableName = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.MigrationSummaryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param TableName the table name
	* @param start the lower bound of the range of migration summaries
	* @param end the upper bound of the range of migration summaries (not inclusive)
	* @return the range of matching migration summaries
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<org.han.service.model.MigrationSummary> findBytableName(
		java.lang.String TableName, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBytableName(TableName, start, end);
	}

	/**
	* Returns an ordered range of all the migration summaries where TableName = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.MigrationSummaryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param TableName the table name
	* @param start the lower bound of the range of migration summaries
	* @param end the upper bound of the range of migration summaries (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching migration summaries
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<org.han.service.model.MigrationSummary> findBytableName(
		java.lang.String TableName, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBytableName(TableName, start, end, orderByComparator);
	}

	/**
	* Returns the first migration summary in the ordered set where TableName = &#63;.
	*
	* @param TableName the table name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching migration summary
	* @throws org.han.service.NoSuchMigrationSummaryException if a matching migration summary could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static org.han.service.model.MigrationSummary findBytableName_First(
		java.lang.String TableName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.han.service.NoSuchMigrationSummaryException {
		return getPersistence()
				   .findBytableName_First(TableName, orderByComparator);
	}

	/**
	* Returns the first migration summary in the ordered set where TableName = &#63;.
	*
	* @param TableName the table name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching migration summary, or <code>null</code> if a matching migration summary could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static org.han.service.model.MigrationSummary fetchBytableName_First(
		java.lang.String TableName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBytableName_First(TableName, orderByComparator);
	}

	/**
	* Returns the last migration summary in the ordered set where TableName = &#63;.
	*
	* @param TableName the table name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching migration summary
	* @throws org.han.service.NoSuchMigrationSummaryException if a matching migration summary could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static org.han.service.model.MigrationSummary findBytableName_Last(
		java.lang.String TableName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.han.service.NoSuchMigrationSummaryException {
		return getPersistence()
				   .findBytableName_Last(TableName, orderByComparator);
	}

	/**
	* Returns the last migration summary in the ordered set where TableName = &#63;.
	*
	* @param TableName the table name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching migration summary, or <code>null</code> if a matching migration summary could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static org.han.service.model.MigrationSummary fetchBytableName_Last(
		java.lang.String TableName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBytableName_Last(TableName, orderByComparator);
	}

	/**
	* Returns the migration summaries before and after the current migration summary in the ordered set where TableName = &#63;.
	*
	* @param id the primary key of the current migration summary
	* @param TableName the table name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next migration summary
	* @throws org.han.service.NoSuchMigrationSummaryException if a migration summary with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static org.han.service.model.MigrationSummary[] findBytableName_PrevAndNext(
		long id, java.lang.String TableName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.han.service.NoSuchMigrationSummaryException {
		return getPersistence()
				   .findBytableName_PrevAndNext(id, TableName, orderByComparator);
	}

	/**
	* Removes all the migration summaries where TableName = &#63; from the database.
	*
	* @param TableName the table name
	* @throws SystemException if a system exception occurred
	*/
	public static void removeBytableName(java.lang.String TableName)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeBytableName(TableName);
	}

	/**
	* Returns the number of migration summaries where TableName = &#63;.
	*
	* @param TableName the table name
	* @return the number of matching migration summaries
	* @throws SystemException if a system exception occurred
	*/
	public static int countBytableName(java.lang.String TableName)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBytableName(TableName);
	}

	/**
	* Caches the migration summary in the entity cache if it is enabled.
	*
	* @param migrationSummary the migration summary
	*/
	public static void cacheResult(
		org.han.service.model.MigrationSummary migrationSummary) {
		getPersistence().cacheResult(migrationSummary);
	}

	/**
	* Caches the migration summaries in the entity cache if it is enabled.
	*
	* @param migrationSummaries the migration summaries
	*/
	public static void cacheResult(
		java.util.List<org.han.service.model.MigrationSummary> migrationSummaries) {
		getPersistence().cacheResult(migrationSummaries);
	}

	/**
	* Creates a new migration summary with the primary key. Does not add the migration summary to the database.
	*
	* @param id the primary key for the new migration summary
	* @return the new migration summary
	*/
	public static org.han.service.model.MigrationSummary create(long id) {
		return getPersistence().create(id);
	}

	/**
	* Removes the migration summary with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param id the primary key of the migration summary
	* @return the migration summary that was removed
	* @throws org.han.service.NoSuchMigrationSummaryException if a migration summary with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static org.han.service.model.MigrationSummary remove(long id)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.han.service.NoSuchMigrationSummaryException {
		return getPersistence().remove(id);
	}

	public static org.han.service.model.MigrationSummary updateImpl(
		org.han.service.model.MigrationSummary migrationSummary)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(migrationSummary);
	}

	/**
	* Returns the migration summary with the primary key or throws a {@link org.han.service.NoSuchMigrationSummaryException} if it could not be found.
	*
	* @param id the primary key of the migration summary
	* @return the migration summary
	* @throws org.han.service.NoSuchMigrationSummaryException if a migration summary with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static org.han.service.model.MigrationSummary findByPrimaryKey(
		long id)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.han.service.NoSuchMigrationSummaryException {
		return getPersistence().findByPrimaryKey(id);
	}

	/**
	* Returns the migration summary with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param id the primary key of the migration summary
	* @return the migration summary, or <code>null</code> if a migration summary with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static org.han.service.model.MigrationSummary fetchByPrimaryKey(
		long id) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(id);
	}

	/**
	* Returns all the migration summaries.
	*
	* @return the migration summaries
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<org.han.service.model.MigrationSummary> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the migration summaries.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.MigrationSummaryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of migration summaries
	* @param end the upper bound of the range of migration summaries (not inclusive)
	* @return the range of migration summaries
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<org.han.service.model.MigrationSummary> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the migration summaries.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.MigrationSummaryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of migration summaries
	* @param end the upper bound of the range of migration summaries (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of migration summaries
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<org.han.service.model.MigrationSummary> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the migration summaries from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of migration summaries.
	*
	* @return the number of migration summaries
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static MigrationSummaryPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (MigrationSummaryPersistence)PortletBeanLocatorUtil.locate(org.han.service.service.ClpSerializer.getServletContextName(),
					MigrationSummaryPersistence.class.getName());

			ReferenceRegistry.registerReference(MigrationSummaryUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(MigrationSummaryPersistence persistence) {
	}

	private static MigrationSummaryPersistence _persistence;
}