/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.han.service.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import org.han.service.model.MigrationSummary;

/**
 * The persistence interface for the migration summary service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Rohan
 * @see MigrationSummaryPersistenceImpl
 * @see MigrationSummaryUtil
 * @generated
 */
public interface MigrationSummaryPersistence extends BasePersistence<MigrationSummary> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link MigrationSummaryUtil} to access the migration summary persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the migration summaries where TableName = &#63;.
	*
	* @param TableName the table name
	* @return the matching migration summaries
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.han.service.model.MigrationSummary> findBytableName(
		java.lang.String TableName)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the migration summaries where TableName = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.MigrationSummaryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param TableName the table name
	* @param start the lower bound of the range of migration summaries
	* @param end the upper bound of the range of migration summaries (not inclusive)
	* @return the range of matching migration summaries
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.han.service.model.MigrationSummary> findBytableName(
		java.lang.String TableName, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the migration summaries where TableName = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.MigrationSummaryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param TableName the table name
	* @param start the lower bound of the range of migration summaries
	* @param end the upper bound of the range of migration summaries (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching migration summaries
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.han.service.model.MigrationSummary> findBytableName(
		java.lang.String TableName, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first migration summary in the ordered set where TableName = &#63;.
	*
	* @param TableName the table name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching migration summary
	* @throws org.han.service.NoSuchMigrationSummaryException if a matching migration summary could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.han.service.model.MigrationSummary findBytableName_First(
		java.lang.String TableName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.han.service.NoSuchMigrationSummaryException;

	/**
	* Returns the first migration summary in the ordered set where TableName = &#63;.
	*
	* @param TableName the table name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching migration summary, or <code>null</code> if a matching migration summary could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.han.service.model.MigrationSummary fetchBytableName_First(
		java.lang.String TableName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last migration summary in the ordered set where TableName = &#63;.
	*
	* @param TableName the table name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching migration summary
	* @throws org.han.service.NoSuchMigrationSummaryException if a matching migration summary could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.han.service.model.MigrationSummary findBytableName_Last(
		java.lang.String TableName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.han.service.NoSuchMigrationSummaryException;

	/**
	* Returns the last migration summary in the ordered set where TableName = &#63;.
	*
	* @param TableName the table name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching migration summary, or <code>null</code> if a matching migration summary could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.han.service.model.MigrationSummary fetchBytableName_Last(
		java.lang.String TableName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the migration summaries before and after the current migration summary in the ordered set where TableName = &#63;.
	*
	* @param id the primary key of the current migration summary
	* @param TableName the table name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next migration summary
	* @throws org.han.service.NoSuchMigrationSummaryException if a migration summary with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.han.service.model.MigrationSummary[] findBytableName_PrevAndNext(
		long id, java.lang.String TableName,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.han.service.NoSuchMigrationSummaryException;

	/**
	* Removes all the migration summaries where TableName = &#63; from the database.
	*
	* @param TableName the table name
	* @throws SystemException if a system exception occurred
	*/
	public void removeBytableName(java.lang.String TableName)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of migration summaries where TableName = &#63;.
	*
	* @param TableName the table name
	* @return the number of matching migration summaries
	* @throws SystemException if a system exception occurred
	*/
	public int countBytableName(java.lang.String TableName)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the migration summary in the entity cache if it is enabled.
	*
	* @param migrationSummary the migration summary
	*/
	public void cacheResult(
		org.han.service.model.MigrationSummary migrationSummary);

	/**
	* Caches the migration summaries in the entity cache if it is enabled.
	*
	* @param migrationSummaries the migration summaries
	*/
	public void cacheResult(
		java.util.List<org.han.service.model.MigrationSummary> migrationSummaries);

	/**
	* Creates a new migration summary with the primary key. Does not add the migration summary to the database.
	*
	* @param id the primary key for the new migration summary
	* @return the new migration summary
	*/
	public org.han.service.model.MigrationSummary create(long id);

	/**
	* Removes the migration summary with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param id the primary key of the migration summary
	* @return the migration summary that was removed
	* @throws org.han.service.NoSuchMigrationSummaryException if a migration summary with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.han.service.model.MigrationSummary remove(long id)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.han.service.NoSuchMigrationSummaryException;

	public org.han.service.model.MigrationSummary updateImpl(
		org.han.service.model.MigrationSummary migrationSummary)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the migration summary with the primary key or throws a {@link org.han.service.NoSuchMigrationSummaryException} if it could not be found.
	*
	* @param id the primary key of the migration summary
	* @return the migration summary
	* @throws org.han.service.NoSuchMigrationSummaryException if a migration summary with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.han.service.model.MigrationSummary findByPrimaryKey(long id)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.han.service.NoSuchMigrationSummaryException;

	/**
	* Returns the migration summary with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param id the primary key of the migration summary
	* @return the migration summary, or <code>null</code> if a migration summary with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.han.service.model.MigrationSummary fetchByPrimaryKey(long id)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the migration summaries.
	*
	* @return the migration summaries
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.han.service.model.MigrationSummary> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the migration summaries.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.MigrationSummaryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of migration summaries
	* @param end the upper bound of the range of migration summaries (not inclusive)
	* @return the range of migration summaries
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.han.service.model.MigrationSummary> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the migration summaries.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.MigrationSummaryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of migration summaries
	* @param end the upper bound of the range of migration summaries (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of migration summaries
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.han.service.model.MigrationSummary> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the migration summaries from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of migration summaries.
	*
	* @return the number of migration summaries
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}