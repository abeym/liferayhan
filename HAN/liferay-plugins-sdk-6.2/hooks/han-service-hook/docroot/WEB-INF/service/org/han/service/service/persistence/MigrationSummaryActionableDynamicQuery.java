/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.han.service.service.persistence;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

import org.han.service.model.MigrationSummary;
import org.han.service.service.MigrationSummaryLocalServiceUtil;

/**
 * @author Rohan
 * @generated
 */
public abstract class MigrationSummaryActionableDynamicQuery
	extends BaseActionableDynamicQuery {
	public MigrationSummaryActionableDynamicQuery() throws SystemException {
		setBaseLocalService(MigrationSummaryLocalServiceUtil.getService());
		setClass(MigrationSummary.class);

		setClassLoader(org.han.service.service.ClpSerializer.class.getClassLoader());

		setPrimaryKeyPropertyName("id");
	}
}