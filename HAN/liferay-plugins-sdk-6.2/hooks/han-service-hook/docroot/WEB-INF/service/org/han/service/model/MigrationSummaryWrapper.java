/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.han.service.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link MigrationSummary}.
 * </p>
 *
 * @author Rohan
 * @see MigrationSummary
 * @generated
 */
public class MigrationSummaryWrapper implements MigrationSummary,
	ModelWrapper<MigrationSummary> {
	public MigrationSummaryWrapper(MigrationSummary migrationSummary) {
		_migrationSummary = migrationSummary;
	}

	@Override
	public Class<?> getModelClass() {
		return MigrationSummary.class;
	}

	@Override
	public String getModelClassName() {
		return MigrationSummary.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("id", getId());
		attributes.put("startTime", getStartTime());
		attributes.put("endTime", getEndTime());
		attributes.put("TableName", getTableName());
		attributes.put("added", getAdded());
		attributes.put("skipped", getSkipped());
		attributes.put("totalFound", getTotalFound());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long id = (Long)attributes.get("id");

		if (id != null) {
			setId(id);
		}

		Date startTime = (Date)attributes.get("startTime");

		if (startTime != null) {
			setStartTime(startTime);
		}

		Date endTime = (Date)attributes.get("endTime");

		if (endTime != null) {
			setEndTime(endTime);
		}

		String TableName = (String)attributes.get("TableName");

		if (TableName != null) {
			setTableName(TableName);
		}

		Long added = (Long)attributes.get("added");

		if (added != null) {
			setAdded(added);
		}

		Long skipped = (Long)attributes.get("skipped");

		if (skipped != null) {
			setSkipped(skipped);
		}

		Long totalFound = (Long)attributes.get("totalFound");

		if (totalFound != null) {
			setTotalFound(totalFound);
		}
	}

	/**
	* Returns the primary key of this migration summary.
	*
	* @return the primary key of this migration summary
	*/
	@Override
	public long getPrimaryKey() {
		return _migrationSummary.getPrimaryKey();
	}

	/**
	* Sets the primary key of this migration summary.
	*
	* @param primaryKey the primary key of this migration summary
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_migrationSummary.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the ID of this migration summary.
	*
	* @return the ID of this migration summary
	*/
	@Override
	public long getId() {
		return _migrationSummary.getId();
	}

	/**
	* Sets the ID of this migration summary.
	*
	* @param id the ID of this migration summary
	*/
	@Override
	public void setId(long id) {
		_migrationSummary.setId(id);
	}

	/**
	* Returns the start time of this migration summary.
	*
	* @return the start time of this migration summary
	*/
	@Override
	public java.util.Date getStartTime() {
		return _migrationSummary.getStartTime();
	}

	/**
	* Sets the start time of this migration summary.
	*
	* @param startTime the start time of this migration summary
	*/
	@Override
	public void setStartTime(java.util.Date startTime) {
		_migrationSummary.setStartTime(startTime);
	}

	/**
	* Returns the end time of this migration summary.
	*
	* @return the end time of this migration summary
	*/
	@Override
	public java.util.Date getEndTime() {
		return _migrationSummary.getEndTime();
	}

	/**
	* Sets the end time of this migration summary.
	*
	* @param endTime the end time of this migration summary
	*/
	@Override
	public void setEndTime(java.util.Date endTime) {
		_migrationSummary.setEndTime(endTime);
	}

	/**
	* Returns the table name of this migration summary.
	*
	* @return the table name of this migration summary
	*/
	@Override
	public java.lang.String getTableName() {
		return _migrationSummary.getTableName();
	}

	/**
	* Sets the table name of this migration summary.
	*
	* @param TableName the table name of this migration summary
	*/
	@Override
	public void setTableName(java.lang.String TableName) {
		_migrationSummary.setTableName(TableName);
	}

	/**
	* Returns the added of this migration summary.
	*
	* @return the added of this migration summary
	*/
	@Override
	public long getAdded() {
		return _migrationSummary.getAdded();
	}

	/**
	* Sets the added of this migration summary.
	*
	* @param added the added of this migration summary
	*/
	@Override
	public void setAdded(long added) {
		_migrationSummary.setAdded(added);
	}

	/**
	* Returns the skipped of this migration summary.
	*
	* @return the skipped of this migration summary
	*/
	@Override
	public long getSkipped() {
		return _migrationSummary.getSkipped();
	}

	/**
	* Sets the skipped of this migration summary.
	*
	* @param skipped the skipped of this migration summary
	*/
	@Override
	public void setSkipped(long skipped) {
		_migrationSummary.setSkipped(skipped);
	}

	/**
	* Returns the total found of this migration summary.
	*
	* @return the total found of this migration summary
	*/
	@Override
	public long getTotalFound() {
		return _migrationSummary.getTotalFound();
	}

	/**
	* Sets the total found of this migration summary.
	*
	* @param totalFound the total found of this migration summary
	*/
	@Override
	public void setTotalFound(long totalFound) {
		_migrationSummary.setTotalFound(totalFound);
	}

	@Override
	public boolean isNew() {
		return _migrationSummary.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_migrationSummary.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _migrationSummary.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_migrationSummary.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _migrationSummary.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _migrationSummary.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_migrationSummary.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _migrationSummary.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_migrationSummary.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_migrationSummary.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_migrationSummary.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new MigrationSummaryWrapper((MigrationSummary)_migrationSummary.clone());
	}

	@Override
	public int compareTo(
		org.han.service.model.MigrationSummary migrationSummary) {
		return _migrationSummary.compareTo(migrationSummary);
	}

	@Override
	public int hashCode() {
		return _migrationSummary.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<org.han.service.model.MigrationSummary> toCacheModel() {
		return _migrationSummary.toCacheModel();
	}

	@Override
	public org.han.service.model.MigrationSummary toEscapedModel() {
		return new MigrationSummaryWrapper(_migrationSummary.toEscapedModel());
	}

	@Override
	public org.han.service.model.MigrationSummary toUnescapedModel() {
		return new MigrationSummaryWrapper(_migrationSummary.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _migrationSummary.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _migrationSummary.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_migrationSummary.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof MigrationSummaryWrapper)) {
			return false;
		}

		MigrationSummaryWrapper migrationSummaryWrapper = (MigrationSummaryWrapper)obj;

		if (Validator.equals(_migrationSummary,
					migrationSummaryWrapper._migrationSummary)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public MigrationSummary getWrappedMigrationSummary() {
		return _migrationSummary;
	}

	@Override
	public MigrationSummary getWrappedModel() {
		return _migrationSummary;
	}

	@Override
	public void resetOriginalValues() {
		_migrationSummary.resetOriginalValues();
	}

	private MigrationSummary _migrationSummary;
}