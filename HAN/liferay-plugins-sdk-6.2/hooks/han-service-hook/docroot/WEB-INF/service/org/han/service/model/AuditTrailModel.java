/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.han.service.model;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.service.ServiceContext;

import com.liferay.portlet.expando.model.ExpandoBridge;

import java.io.Serializable;

import java.util.Date;

/**
 * The base model interface for the AuditTrail service. Represents a row in the &quot;HAN_AuditTrail&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link org.han.service.model.impl.AuditTrailModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link org.han.service.model.impl.AuditTrailImpl}.
 * </p>
 *
 * @author Rohan
 * @see AuditTrail
 * @see org.han.service.model.impl.AuditTrailImpl
 * @see org.han.service.model.impl.AuditTrailModelImpl
 * @generated
 */
public interface AuditTrailModel extends BaseModel<AuditTrail> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a audit trail model instance should use the {@link AuditTrail} interface instead.
	 */

	/**
	 * Returns the primary key of this audit trail.
	 *
	 * @return the primary key of this audit trail
	 */
	public long getPrimaryKey();

	/**
	 * Sets the primary key of this audit trail.
	 *
	 * @param primaryKey the primary key of this audit trail
	 */
	public void setPrimaryKey(long primaryKey);

	/**
	 * Returns the ID of this audit trail.
	 *
	 * @return the ID of this audit trail
	 */
	public long getId();

	/**
	 * Sets the ID of this audit trail.
	 *
	 * @param id the ID of this audit trail
	 */
	public void setId(long id);

	/**
	 * Returns the event type of this audit trail.
	 *
	 * @return the event type of this audit trail
	 */
	@AutoEscape
	public String getEventType();

	/**
	 * Sets the event type of this audit trail.
	 *
	 * @param eventType the event type of this audit trail
	 */
	public void setEventType(String eventType);

	/**
	 * Returns the class name of this audit trail.
	 *
	 * @return the class name of this audit trail
	 */
	@AutoEscape
	public String getClassName();

	/**
	 * Sets the class name of this audit trail.
	 *
	 * @param className the class name of this audit trail
	 */
	public void setClassName(String className);

	/**
	 * Returns the class p k of this audit trail.
	 *
	 * @return the class p k of this audit trail
	 */
	@AutoEscape
	public String getClassPK();

	/**
	 * Sets the class p k of this audit trail.
	 *
	 * @param classPK the class p k of this audit trail
	 */
	public void setClassPK(String classPK);

	/**
	 * Returns the real user ID of this audit trail.
	 *
	 * @return the real user ID of this audit trail
	 */
	@AutoEscape
	public String getRealUserId();

	/**
	 * Sets the real user ID of this audit trail.
	 *
	 * @param realUserId the real user ID of this audit trail
	 */
	public void setRealUserId(String realUserId);

	/**
	 * Returns the real user name of this audit trail.
	 *
	 * @return the real user name of this audit trail
	 */
	@AutoEscape
	public String getRealUserName();

	/**
	 * Sets the real user name of this audit trail.
	 *
	 * @param realUserName the real user name of this audit trail
	 */
	public void setRealUserName(String realUserName);

	/**
	 * Returns the real user email of this audit trail.
	 *
	 * @return the real user email of this audit trail
	 */
	@AutoEscape
	public String getRealUserEmail();

	/**
	 * Sets the real user email of this audit trail.
	 *
	 * @param realUserEmail the real user email of this audit trail
	 */
	public void setRealUserEmail(String realUserEmail);

	/**
	 * Returns the user ID of this audit trail.
	 *
	 * @return the user ID of this audit trail
	 */
	@AutoEscape
	public String getUserId();

	/**
	 * Sets the user ID of this audit trail.
	 *
	 * @param userId the user ID of this audit trail
	 */
	public void setUserId(String userId);

	/**
	 * Returns the user name of this audit trail.
	 *
	 * @return the user name of this audit trail
	 */
	@AutoEscape
	public String getUserName();

	/**
	 * Sets the user name of this audit trail.
	 *
	 * @param userName the user name of this audit trail
	 */
	public void setUserName(String userName);

	/**
	 * Returns the user email of this audit trail.
	 *
	 * @return the user email of this audit trail
	 */
	@AutoEscape
	public String getUserEmail();

	/**
	 * Sets the user email of this audit trail.
	 *
	 * @param userEmail the user email of this audit trail
	 */
	public void setUserEmail(String userEmail);

	/**
	 * Returns the create date of this audit trail.
	 *
	 * @return the create date of this audit trail
	 */
	public Date getCreateDate();

	/**
	 * Sets the create date of this audit trail.
	 *
	 * @param createDate the create date of this audit trail
	 */
	public void setCreateDate(Date createDate);

	/**
	 * Returns the message of this audit trail.
	 *
	 * @return the message of this audit trail
	 */
	@AutoEscape
	public String getMessage();

	/**
	 * Sets the message of this audit trail.
	 *
	 * @param message the message of this audit trail
	 */
	public void setMessage(String message);

	@Override
	public boolean isNew();

	@Override
	public void setNew(boolean n);

	@Override
	public boolean isCachedModel();

	@Override
	public void setCachedModel(boolean cachedModel);

	@Override
	public boolean isEscapedModel();

	@Override
	public Serializable getPrimaryKeyObj();

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj);

	@Override
	public ExpandoBridge getExpandoBridge();

	@Override
	public void setExpandoBridgeAttributes(BaseModel<?> baseModel);

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge);

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext);

	@Override
	public Object clone();

	@Override
	public int compareTo(org.han.service.model.AuditTrail auditTrail);

	@Override
	public int hashCode();

	@Override
	public CacheModel<org.han.service.model.AuditTrail> toCacheModel();

	@Override
	public org.han.service.model.AuditTrail toEscapedModel();

	@Override
	public org.han.service.model.AuditTrail toUnescapedModel();

	@Override
	public String toString();

	@Override
	public String toXmlString();
}