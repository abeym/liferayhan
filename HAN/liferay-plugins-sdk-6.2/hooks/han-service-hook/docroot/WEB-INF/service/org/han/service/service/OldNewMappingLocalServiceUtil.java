/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.han.service.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for OldNewMapping. This utility wraps
 * {@link org.han.service.service.impl.OldNewMappingLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Rohan
 * @see OldNewMappingLocalService
 * @see org.han.service.service.base.OldNewMappingLocalServiceBaseImpl
 * @see org.han.service.service.impl.OldNewMappingLocalServiceImpl
 * @generated
 */
public class OldNewMappingLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link org.han.service.service.impl.OldNewMappingLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the old new mapping to the database. Also notifies the appropriate model listeners.
	*
	* @param oldNewMapping the old new mapping
	* @return the old new mapping that was added
	* @throws SystemException if a system exception occurred
	*/
	public static org.han.service.model.OldNewMapping addOldNewMapping(
		org.han.service.model.OldNewMapping oldNewMapping)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addOldNewMapping(oldNewMapping);
	}

	/**
	* Creates a new old new mapping with the primary key. Does not add the old new mapping to the database.
	*
	* @param id the primary key for the new old new mapping
	* @return the new old new mapping
	*/
	public static org.han.service.model.OldNewMapping createOldNewMapping(
		long id) {
		return getService().createOldNewMapping(id);
	}

	/**
	* Deletes the old new mapping with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param id the primary key of the old new mapping
	* @return the old new mapping that was removed
	* @throws PortalException if a old new mapping with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static org.han.service.model.OldNewMapping deleteOldNewMapping(
		long id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteOldNewMapping(id);
	}

	/**
	* Deletes the old new mapping from the database. Also notifies the appropriate model listeners.
	*
	* @param oldNewMapping the old new mapping
	* @return the old new mapping that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static org.han.service.model.OldNewMapping deleteOldNewMapping(
		org.han.service.model.OldNewMapping oldNewMapping)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteOldNewMapping(oldNewMapping);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.OldNewMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.OldNewMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static org.han.service.model.OldNewMapping fetchOldNewMapping(
		long id) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchOldNewMapping(id);
	}

	/**
	* Returns the old new mapping with the primary key.
	*
	* @param id the primary key of the old new mapping
	* @return the old new mapping
	* @throws PortalException if a old new mapping with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static org.han.service.model.OldNewMapping getOldNewMapping(long id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getOldNewMapping(id);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the old new mappings.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.OldNewMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of old new mappings
	* @param end the upper bound of the range of old new mappings (not inclusive)
	* @return the range of old new mappings
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<org.han.service.model.OldNewMapping> getOldNewMappings(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getOldNewMappings(start, end);
	}

	/**
	* Returns the number of old new mappings.
	*
	* @return the number of old new mappings
	* @throws SystemException if a system exception occurred
	*/
	public static int getOldNewMappingsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getOldNewMappingsCount();
	}

	/**
	* Updates the old new mapping in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param oldNewMapping the old new mapping
	* @return the old new mapping that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static org.han.service.model.OldNewMapping updateOldNewMapping(
		org.han.service.model.OldNewMapping oldNewMapping)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateOldNewMapping(oldNewMapping);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static org.han.service.model.OldNewMapping create(long oldPK,
		long newPK, java.lang.String table) {
		return getService().create(oldPK, newPK, table);
	}

	public static com.liferay.portal.kernel.json.JSONObject getJSON(
		org.han.service.model.OldNewMapping aMapping) {
		return getService().getJSON(aMapping);
	}

	public static org.han.service.model.OldNewMapping findByTableNameAndOldPK(
		java.lang.String entityName, long oldPk) {
		return getService().findByTableNameAndOldPK(entityName, oldPk);
	}

	public static void clearService() {
		_service = null;
	}

	public static OldNewMappingLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					OldNewMappingLocalService.class.getName());

			if (invokableLocalService instanceof OldNewMappingLocalService) {
				_service = (OldNewMappingLocalService)invokableLocalService;
			}
			else {
				_service = new OldNewMappingLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(OldNewMappingLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setService(OldNewMappingLocalService service) {
	}

	private static OldNewMappingLocalService _service;
}