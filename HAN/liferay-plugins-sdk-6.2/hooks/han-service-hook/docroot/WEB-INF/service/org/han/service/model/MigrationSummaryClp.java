/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.han.service.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import org.han.service.service.ClpSerializer;
import org.han.service.service.MigrationSummaryLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Rohan
 */
public class MigrationSummaryClp extends BaseModelImpl<MigrationSummary>
	implements MigrationSummary {
	public MigrationSummaryClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return MigrationSummary.class;
	}

	@Override
	public String getModelClassName() {
		return MigrationSummary.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _id;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _id;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("id", getId());
		attributes.put("startTime", getStartTime());
		attributes.put("endTime", getEndTime());
		attributes.put("TableName", getTableName());
		attributes.put("added", getAdded());
		attributes.put("skipped", getSkipped());
		attributes.put("totalFound", getTotalFound());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long id = (Long)attributes.get("id");

		if (id != null) {
			setId(id);
		}

		Date startTime = (Date)attributes.get("startTime");

		if (startTime != null) {
			setStartTime(startTime);
		}

		Date endTime = (Date)attributes.get("endTime");

		if (endTime != null) {
			setEndTime(endTime);
		}

		String TableName = (String)attributes.get("TableName");

		if (TableName != null) {
			setTableName(TableName);
		}

		Long added = (Long)attributes.get("added");

		if (added != null) {
			setAdded(added);
		}

		Long skipped = (Long)attributes.get("skipped");

		if (skipped != null) {
			setSkipped(skipped);
		}

		Long totalFound = (Long)attributes.get("totalFound");

		if (totalFound != null) {
			setTotalFound(totalFound);
		}
	}

	@Override
	public long getId() {
		return _id;
	}

	@Override
	public void setId(long id) {
		_id = id;

		if (_migrationSummaryRemoteModel != null) {
			try {
				Class<?> clazz = _migrationSummaryRemoteModel.getClass();

				Method method = clazz.getMethod("setId", long.class);

				method.invoke(_migrationSummaryRemoteModel, id);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getStartTime() {
		return _startTime;
	}

	@Override
	public void setStartTime(Date startTime) {
		_startTime = startTime;

		if (_migrationSummaryRemoteModel != null) {
			try {
				Class<?> clazz = _migrationSummaryRemoteModel.getClass();

				Method method = clazz.getMethod("setStartTime", Date.class);

				method.invoke(_migrationSummaryRemoteModel, startTime);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getEndTime() {
		return _endTime;
	}

	@Override
	public void setEndTime(Date endTime) {
		_endTime = endTime;

		if (_migrationSummaryRemoteModel != null) {
			try {
				Class<?> clazz = _migrationSummaryRemoteModel.getClass();

				Method method = clazz.getMethod("setEndTime", Date.class);

				method.invoke(_migrationSummaryRemoteModel, endTime);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTableName() {
		return _TableName;
	}

	@Override
	public void setTableName(String TableName) {
		_TableName = TableName;

		if (_migrationSummaryRemoteModel != null) {
			try {
				Class<?> clazz = _migrationSummaryRemoteModel.getClass();

				Method method = clazz.getMethod("setTableName", String.class);

				method.invoke(_migrationSummaryRemoteModel, TableName);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getAdded() {
		return _added;
	}

	@Override
	public void setAdded(long added) {
		_added = added;

		if (_migrationSummaryRemoteModel != null) {
			try {
				Class<?> clazz = _migrationSummaryRemoteModel.getClass();

				Method method = clazz.getMethod("setAdded", long.class);

				method.invoke(_migrationSummaryRemoteModel, added);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getSkipped() {
		return _skipped;
	}

	@Override
	public void setSkipped(long skipped) {
		_skipped = skipped;

		if (_migrationSummaryRemoteModel != null) {
			try {
				Class<?> clazz = _migrationSummaryRemoteModel.getClass();

				Method method = clazz.getMethod("setSkipped", long.class);

				method.invoke(_migrationSummaryRemoteModel, skipped);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getTotalFound() {
		return _totalFound;
	}

	@Override
	public void setTotalFound(long totalFound) {
		_totalFound = totalFound;

		if (_migrationSummaryRemoteModel != null) {
			try {
				Class<?> clazz = _migrationSummaryRemoteModel.getClass();

				Method method = clazz.getMethod("setTotalFound", long.class);

				method.invoke(_migrationSummaryRemoteModel, totalFound);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getMigrationSummaryRemoteModel() {
		return _migrationSummaryRemoteModel;
	}

	public void setMigrationSummaryRemoteModel(
		BaseModel<?> migrationSummaryRemoteModel) {
		_migrationSummaryRemoteModel = migrationSummaryRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _migrationSummaryRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_migrationSummaryRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			MigrationSummaryLocalServiceUtil.addMigrationSummary(this);
		}
		else {
			MigrationSummaryLocalServiceUtil.updateMigrationSummary(this);
		}
	}

	@Override
	public MigrationSummary toEscapedModel() {
		return (MigrationSummary)ProxyUtil.newProxyInstance(MigrationSummary.class.getClassLoader(),
			new Class[] { MigrationSummary.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		MigrationSummaryClp clone = new MigrationSummaryClp();

		clone.setId(getId());
		clone.setStartTime(getStartTime());
		clone.setEndTime(getEndTime());
		clone.setTableName(getTableName());
		clone.setAdded(getAdded());
		clone.setSkipped(getSkipped());
		clone.setTotalFound(getTotalFound());

		return clone;
	}

	@Override
	public int compareTo(MigrationSummary migrationSummary) {
		int value = 0;

		if (getId() < migrationSummary.getId()) {
			value = -1;
		}
		else if (getId() > migrationSummary.getId()) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof MigrationSummaryClp)) {
			return false;
		}

		MigrationSummaryClp migrationSummary = (MigrationSummaryClp)obj;

		long primaryKey = migrationSummary.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append("{id=");
		sb.append(getId());
		sb.append(", startTime=");
		sb.append(getStartTime());
		sb.append(", endTime=");
		sb.append(getEndTime());
		sb.append(", TableName=");
		sb.append(getTableName());
		sb.append(", added=");
		sb.append(getAdded());
		sb.append(", skipped=");
		sb.append(getSkipped());
		sb.append(", totalFound=");
		sb.append(getTotalFound());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(25);

		sb.append("<model><model-name>");
		sb.append("org.han.service.model.MigrationSummary");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>id</column-name><column-value><![CDATA[");
		sb.append(getId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>startTime</column-name><column-value><![CDATA[");
		sb.append(getStartTime());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>endTime</column-name><column-value><![CDATA[");
		sb.append(getEndTime());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>TableName</column-name><column-value><![CDATA[");
		sb.append(getTableName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>added</column-name><column-value><![CDATA[");
		sb.append(getAdded());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>skipped</column-name><column-value><![CDATA[");
		sb.append(getSkipped());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>totalFound</column-name><column-value><![CDATA[");
		sb.append(getTotalFound());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _id;
	private Date _startTime;
	private Date _endTime;
	private String _TableName;
	private long _added;
	private long _skipped;
	private long _totalFound;
	private BaseModel<?> _migrationSummaryRemoteModel;
	private Class<?> _clpSerializerClass = org.han.service.service.ClpSerializer.class;
}