/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.han.service.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Rohan
 * @generated
 */
public class MigrationSummarySoap implements Serializable {
	public static MigrationSummarySoap toSoapModel(MigrationSummary model) {
		MigrationSummarySoap soapModel = new MigrationSummarySoap();

		soapModel.setId(model.getId());
		soapModel.setStartTime(model.getStartTime());
		soapModel.setEndTime(model.getEndTime());
		soapModel.setTableName(model.getTableName());
		soapModel.setAdded(model.getAdded());
		soapModel.setSkipped(model.getSkipped());
		soapModel.setTotalFound(model.getTotalFound());

		return soapModel;
	}

	public static MigrationSummarySoap[] toSoapModels(MigrationSummary[] models) {
		MigrationSummarySoap[] soapModels = new MigrationSummarySoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static MigrationSummarySoap[][] toSoapModels(
		MigrationSummary[][] models) {
		MigrationSummarySoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new MigrationSummarySoap[models.length][models[0].length];
		}
		else {
			soapModels = new MigrationSummarySoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static MigrationSummarySoap[] toSoapModels(
		List<MigrationSummary> models) {
		List<MigrationSummarySoap> soapModels = new ArrayList<MigrationSummarySoap>(models.size());

		for (MigrationSummary model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new MigrationSummarySoap[soapModels.size()]);
	}

	public MigrationSummarySoap() {
	}

	public long getPrimaryKey() {
		return _id;
	}

	public void setPrimaryKey(long pk) {
		setId(pk);
	}

	public long getId() {
		return _id;
	}

	public void setId(long id) {
		_id = id;
	}

	public Date getStartTime() {
		return _startTime;
	}

	public void setStartTime(Date startTime) {
		_startTime = startTime;
	}

	public Date getEndTime() {
		return _endTime;
	}

	public void setEndTime(Date endTime) {
		_endTime = endTime;
	}

	public String getTableName() {
		return _TableName;
	}

	public void setTableName(String TableName) {
		_TableName = TableName;
	}

	public long getAdded() {
		return _added;
	}

	public void setAdded(long added) {
		_added = added;
	}

	public long getSkipped() {
		return _skipped;
	}

	public void setSkipped(long skipped) {
		_skipped = skipped;
	}

	public long getTotalFound() {
		return _totalFound;
	}

	public void setTotalFound(long totalFound) {
		_totalFound = totalFound;
	}

	private long _id;
	private Date _startTime;
	private Date _endTime;
	private String _TableName;
	private long _added;
	private long _skipped;
	private long _totalFound;
}