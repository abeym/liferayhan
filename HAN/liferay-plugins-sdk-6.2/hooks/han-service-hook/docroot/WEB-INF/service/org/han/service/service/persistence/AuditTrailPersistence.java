/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.han.service.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import org.han.service.model.AuditTrail;

/**
 * The persistence interface for the audit trail service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Rohan
 * @see AuditTrailPersistenceImpl
 * @see AuditTrailUtil
 * @generated
 */
public interface AuditTrailPersistence extends BasePersistence<AuditTrail> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link AuditTrailUtil} to access the audit trail persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the audit trail in the entity cache if it is enabled.
	*
	* @param auditTrail the audit trail
	*/
	public void cacheResult(org.han.service.model.AuditTrail auditTrail);

	/**
	* Caches the audit trails in the entity cache if it is enabled.
	*
	* @param auditTrails the audit trails
	*/
	public void cacheResult(
		java.util.List<org.han.service.model.AuditTrail> auditTrails);

	/**
	* Creates a new audit trail with the primary key. Does not add the audit trail to the database.
	*
	* @param id the primary key for the new audit trail
	* @return the new audit trail
	*/
	public org.han.service.model.AuditTrail create(long id);

	/**
	* Removes the audit trail with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param id the primary key of the audit trail
	* @return the audit trail that was removed
	* @throws org.han.service.NoSuchAuditTrailException if a audit trail with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.han.service.model.AuditTrail remove(long id)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.han.service.NoSuchAuditTrailException;

	public org.han.service.model.AuditTrail updateImpl(
		org.han.service.model.AuditTrail auditTrail)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the audit trail with the primary key or throws a {@link org.han.service.NoSuchAuditTrailException} if it could not be found.
	*
	* @param id the primary key of the audit trail
	* @return the audit trail
	* @throws org.han.service.NoSuchAuditTrailException if a audit trail with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.han.service.model.AuditTrail findByPrimaryKey(long id)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.han.service.NoSuchAuditTrailException;

	/**
	* Returns the audit trail with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param id the primary key of the audit trail
	* @return the audit trail, or <code>null</code> if a audit trail with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.han.service.model.AuditTrail fetchByPrimaryKey(long id)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the audit trails.
	*
	* @return the audit trails
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.han.service.model.AuditTrail> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the audit trails.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.AuditTrailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of audit trails
	* @param end the upper bound of the range of audit trails (not inclusive)
	* @return the range of audit trails
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.han.service.model.AuditTrail> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the audit trails.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.AuditTrailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of audit trails
	* @param end the upper bound of the range of audit trails (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of audit trails
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.han.service.model.AuditTrail> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the audit trails from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of audit trails.
	*
	* @return the number of audit trails
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}