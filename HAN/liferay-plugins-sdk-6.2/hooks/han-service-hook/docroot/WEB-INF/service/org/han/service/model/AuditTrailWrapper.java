/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.han.service.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link AuditTrail}.
 * </p>
 *
 * @author Rohan
 * @see AuditTrail
 * @generated
 */
public class AuditTrailWrapper implements AuditTrail, ModelWrapper<AuditTrail> {
	public AuditTrailWrapper(AuditTrail auditTrail) {
		_auditTrail = auditTrail;
	}

	@Override
	public Class<?> getModelClass() {
		return AuditTrail.class;
	}

	@Override
	public String getModelClassName() {
		return AuditTrail.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("id", getId());
		attributes.put("eventType", getEventType());
		attributes.put("className", getClassName());
		attributes.put("classPK", getClassPK());
		attributes.put("realUserId", getRealUserId());
		attributes.put("realUserName", getRealUserName());
		attributes.put("realUserEmail", getRealUserEmail());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("userEmail", getUserEmail());
		attributes.put("createDate", getCreateDate());
		attributes.put("message", getMessage());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long id = (Long)attributes.get("id");

		if (id != null) {
			setId(id);
		}

		String eventType = (String)attributes.get("eventType");

		if (eventType != null) {
			setEventType(eventType);
		}

		String className = (String)attributes.get("className");

		if (className != null) {
			setClassName(className);
		}

		String classPK = (String)attributes.get("classPK");

		if (classPK != null) {
			setClassPK(classPK);
		}

		String realUserId = (String)attributes.get("realUserId");

		if (realUserId != null) {
			setRealUserId(realUserId);
		}

		String realUserName = (String)attributes.get("realUserName");

		if (realUserName != null) {
			setRealUserName(realUserName);
		}

		String realUserEmail = (String)attributes.get("realUserEmail");

		if (realUserEmail != null) {
			setRealUserEmail(realUserEmail);
		}

		String userId = (String)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		String userEmail = (String)attributes.get("userEmail");

		if (userEmail != null) {
			setUserEmail(userEmail);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		String message = (String)attributes.get("message");

		if (message != null) {
			setMessage(message);
		}
	}

	/**
	* Returns the primary key of this audit trail.
	*
	* @return the primary key of this audit trail
	*/
	@Override
	public long getPrimaryKey() {
		return _auditTrail.getPrimaryKey();
	}

	/**
	* Sets the primary key of this audit trail.
	*
	* @param primaryKey the primary key of this audit trail
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_auditTrail.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the ID of this audit trail.
	*
	* @return the ID of this audit trail
	*/
	@Override
	public long getId() {
		return _auditTrail.getId();
	}

	/**
	* Sets the ID of this audit trail.
	*
	* @param id the ID of this audit trail
	*/
	@Override
	public void setId(long id) {
		_auditTrail.setId(id);
	}

	/**
	* Returns the event type of this audit trail.
	*
	* @return the event type of this audit trail
	*/
	@Override
	public java.lang.String getEventType() {
		return _auditTrail.getEventType();
	}

	/**
	* Sets the event type of this audit trail.
	*
	* @param eventType the event type of this audit trail
	*/
	@Override
	public void setEventType(java.lang.String eventType) {
		_auditTrail.setEventType(eventType);
	}

	/**
	* Returns the class name of this audit trail.
	*
	* @return the class name of this audit trail
	*/
	@Override
	public java.lang.String getClassName() {
		return _auditTrail.getClassName();
	}

	/**
	* Sets the class name of this audit trail.
	*
	* @param className the class name of this audit trail
	*/
	@Override
	public void setClassName(java.lang.String className) {
		_auditTrail.setClassName(className);
	}

	/**
	* Returns the class p k of this audit trail.
	*
	* @return the class p k of this audit trail
	*/
	@Override
	public java.lang.String getClassPK() {
		return _auditTrail.getClassPK();
	}

	/**
	* Sets the class p k of this audit trail.
	*
	* @param classPK the class p k of this audit trail
	*/
	@Override
	public void setClassPK(java.lang.String classPK) {
		_auditTrail.setClassPK(classPK);
	}

	/**
	* Returns the real user ID of this audit trail.
	*
	* @return the real user ID of this audit trail
	*/
	@Override
	public java.lang.String getRealUserId() {
		return _auditTrail.getRealUserId();
	}

	/**
	* Sets the real user ID of this audit trail.
	*
	* @param realUserId the real user ID of this audit trail
	*/
	@Override
	public void setRealUserId(java.lang.String realUserId) {
		_auditTrail.setRealUserId(realUserId);
	}

	/**
	* Returns the real user name of this audit trail.
	*
	* @return the real user name of this audit trail
	*/
	@Override
	public java.lang.String getRealUserName() {
		return _auditTrail.getRealUserName();
	}

	/**
	* Sets the real user name of this audit trail.
	*
	* @param realUserName the real user name of this audit trail
	*/
	@Override
	public void setRealUserName(java.lang.String realUserName) {
		_auditTrail.setRealUserName(realUserName);
	}

	/**
	* Returns the real user email of this audit trail.
	*
	* @return the real user email of this audit trail
	*/
	@Override
	public java.lang.String getRealUserEmail() {
		return _auditTrail.getRealUserEmail();
	}

	/**
	* Sets the real user email of this audit trail.
	*
	* @param realUserEmail the real user email of this audit trail
	*/
	@Override
	public void setRealUserEmail(java.lang.String realUserEmail) {
		_auditTrail.setRealUserEmail(realUserEmail);
	}

	/**
	* Returns the user ID of this audit trail.
	*
	* @return the user ID of this audit trail
	*/
	@Override
	public java.lang.String getUserId() {
		return _auditTrail.getUserId();
	}

	/**
	* Sets the user ID of this audit trail.
	*
	* @param userId the user ID of this audit trail
	*/
	@Override
	public void setUserId(java.lang.String userId) {
		_auditTrail.setUserId(userId);
	}

	/**
	* Returns the user name of this audit trail.
	*
	* @return the user name of this audit trail
	*/
	@Override
	public java.lang.String getUserName() {
		return _auditTrail.getUserName();
	}

	/**
	* Sets the user name of this audit trail.
	*
	* @param userName the user name of this audit trail
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_auditTrail.setUserName(userName);
	}

	/**
	* Returns the user email of this audit trail.
	*
	* @return the user email of this audit trail
	*/
	@Override
	public java.lang.String getUserEmail() {
		return _auditTrail.getUserEmail();
	}

	/**
	* Sets the user email of this audit trail.
	*
	* @param userEmail the user email of this audit trail
	*/
	@Override
	public void setUserEmail(java.lang.String userEmail) {
		_auditTrail.setUserEmail(userEmail);
	}

	/**
	* Returns the create date of this audit trail.
	*
	* @return the create date of this audit trail
	*/
	@Override
	public java.util.Date getCreateDate() {
		return _auditTrail.getCreateDate();
	}

	/**
	* Sets the create date of this audit trail.
	*
	* @param createDate the create date of this audit trail
	*/
	@Override
	public void setCreateDate(java.util.Date createDate) {
		_auditTrail.setCreateDate(createDate);
	}

	/**
	* Returns the message of this audit trail.
	*
	* @return the message of this audit trail
	*/
	@Override
	public java.lang.String getMessage() {
		return _auditTrail.getMessage();
	}

	/**
	* Sets the message of this audit trail.
	*
	* @param message the message of this audit trail
	*/
	@Override
	public void setMessage(java.lang.String message) {
		_auditTrail.setMessage(message);
	}

	@Override
	public boolean isNew() {
		return _auditTrail.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_auditTrail.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _auditTrail.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_auditTrail.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _auditTrail.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _auditTrail.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_auditTrail.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _auditTrail.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_auditTrail.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_auditTrail.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_auditTrail.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new AuditTrailWrapper((AuditTrail)_auditTrail.clone());
	}

	@Override
	public int compareTo(org.han.service.model.AuditTrail auditTrail) {
		return _auditTrail.compareTo(auditTrail);
	}

	@Override
	public int hashCode() {
		return _auditTrail.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<org.han.service.model.AuditTrail> toCacheModel() {
		return _auditTrail.toCacheModel();
	}

	@Override
	public org.han.service.model.AuditTrail toEscapedModel() {
		return new AuditTrailWrapper(_auditTrail.toEscapedModel());
	}

	@Override
	public org.han.service.model.AuditTrail toUnescapedModel() {
		return new AuditTrailWrapper(_auditTrail.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _auditTrail.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _auditTrail.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_auditTrail.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof AuditTrailWrapper)) {
			return false;
		}

		AuditTrailWrapper auditTrailWrapper = (AuditTrailWrapper)obj;

		if (Validator.equals(_auditTrail, auditTrailWrapper._auditTrail)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public AuditTrail getWrappedAuditTrail() {
		return _auditTrail;
	}

	@Override
	public AuditTrail getWrappedModel() {
		return _auditTrail;
	}

	@Override
	public void resetOriginalValues() {
		_auditTrail.resetOriginalValues();
	}

	private AuditTrail _auditTrail;
}