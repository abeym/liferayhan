/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.han.service.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import org.han.service.service.ClpSerializer;
import org.han.service.service.OldNewMappingLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Rohan
 */
public class OldNewMappingClp extends BaseModelImpl<OldNewMapping>
	implements OldNewMapping {
	public OldNewMappingClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return OldNewMapping.class;
	}

	@Override
	public String getModelClassName() {
		return OldNewMapping.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _id;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _id;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("id", getId());
		attributes.put("oldPK", getOldPK());
		attributes.put("newPK", getNewPK());
		attributes.put("TableName", getTableName());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long id = (Long)attributes.get("id");

		if (id != null) {
			setId(id);
		}

		Long oldPK = (Long)attributes.get("oldPK");

		if (oldPK != null) {
			setOldPK(oldPK);
		}

		Long newPK = (Long)attributes.get("newPK");

		if (newPK != null) {
			setNewPK(newPK);
		}

		String TableName = (String)attributes.get("TableName");

		if (TableName != null) {
			setTableName(TableName);
		}
	}

	@Override
	public long getId() {
		return _id;
	}

	@Override
	public void setId(long id) {
		_id = id;

		if (_oldNewMappingRemoteModel != null) {
			try {
				Class<?> clazz = _oldNewMappingRemoteModel.getClass();

				Method method = clazz.getMethod("setId", long.class);

				method.invoke(_oldNewMappingRemoteModel, id);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getOldPK() {
		return _oldPK;
	}

	@Override
	public void setOldPK(long oldPK) {
		_oldPK = oldPK;

		if (_oldNewMappingRemoteModel != null) {
			try {
				Class<?> clazz = _oldNewMappingRemoteModel.getClass();

				Method method = clazz.getMethod("setOldPK", long.class);

				method.invoke(_oldNewMappingRemoteModel, oldPK);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getNewPK() {
		return _newPK;
	}

	@Override
	public void setNewPK(long newPK) {
		_newPK = newPK;

		if (_oldNewMappingRemoteModel != null) {
			try {
				Class<?> clazz = _oldNewMappingRemoteModel.getClass();

				Method method = clazz.getMethod("setNewPK", long.class);

				method.invoke(_oldNewMappingRemoteModel, newPK);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTableName() {
		return _TableName;
	}

	@Override
	public void setTableName(String TableName) {
		_TableName = TableName;

		if (_oldNewMappingRemoteModel != null) {
			try {
				Class<?> clazz = _oldNewMappingRemoteModel.getClass();

				Method method = clazz.getMethod("setTableName", String.class);

				method.invoke(_oldNewMappingRemoteModel, TableName);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getOldNewMappingRemoteModel() {
		return _oldNewMappingRemoteModel;
	}

	public void setOldNewMappingRemoteModel(
		BaseModel<?> oldNewMappingRemoteModel) {
		_oldNewMappingRemoteModel = oldNewMappingRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _oldNewMappingRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_oldNewMappingRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			OldNewMappingLocalServiceUtil.addOldNewMapping(this);
		}
		else {
			OldNewMappingLocalServiceUtil.updateOldNewMapping(this);
		}
	}

	@Override
	public OldNewMapping toEscapedModel() {
		return (OldNewMapping)ProxyUtil.newProxyInstance(OldNewMapping.class.getClassLoader(),
			new Class[] { OldNewMapping.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		OldNewMappingClp clone = new OldNewMappingClp();

		clone.setId(getId());
		clone.setOldPK(getOldPK());
		clone.setNewPK(getNewPK());
		clone.setTableName(getTableName());

		return clone;
	}

	@Override
	public int compareTo(OldNewMapping oldNewMapping) {
		int value = 0;

		if (getId() < oldNewMapping.getId()) {
			value = -1;
		}
		else if (getId() > oldNewMapping.getId()) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof OldNewMappingClp)) {
			return false;
		}

		OldNewMappingClp oldNewMapping = (OldNewMappingClp)obj;

		long primaryKey = oldNewMapping.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{id=");
		sb.append(getId());
		sb.append(", oldPK=");
		sb.append(getOldPK());
		sb.append(", newPK=");
		sb.append(getNewPK());
		sb.append(", TableName=");
		sb.append(getTableName());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(16);

		sb.append("<model><model-name>");
		sb.append("org.han.service.model.OldNewMapping");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>id</column-name><column-value><![CDATA[");
		sb.append(getId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>oldPK</column-name><column-value><![CDATA[");
		sb.append(getOldPK());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>newPK</column-name><column-value><![CDATA[");
		sb.append(getNewPK());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>TableName</column-name><column-value><![CDATA[");
		sb.append(getTableName());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _id;
	private long _oldPK;
	private long _newPK;
	private String _TableName;
	private BaseModel<?> _oldNewMappingRemoteModel;
	private Class<?> _clpSerializerClass = org.han.service.service.ClpSerializer.class;
}