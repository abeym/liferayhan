/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.han.service.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link SkippedEntries}.
 * </p>
 *
 * @author Rohan
 * @see SkippedEntries
 * @generated
 */
public class SkippedEntriesWrapper implements SkippedEntries,
	ModelWrapper<SkippedEntries> {
	public SkippedEntriesWrapper(SkippedEntries skippedEntries) {
		_skippedEntries = skippedEntries;
	}

	@Override
	public Class<?> getModelClass() {
		return SkippedEntries.class;
	}

	@Override
	public String getModelClassName() {
		return SkippedEntries.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("id", getId());
		attributes.put("oldPK", getOldPK());
		attributes.put("newPK", getNewPK());
		attributes.put("TableName", getTableName());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long id = (Long)attributes.get("id");

		if (id != null) {
			setId(id);
		}

		Long oldPK = (Long)attributes.get("oldPK");

		if (oldPK != null) {
			setOldPK(oldPK);
		}

		Long newPK = (Long)attributes.get("newPK");

		if (newPK != null) {
			setNewPK(newPK);
		}

		String TableName = (String)attributes.get("TableName");

		if (TableName != null) {
			setTableName(TableName);
		}
	}

	/**
	* Returns the primary key of this skipped entries.
	*
	* @return the primary key of this skipped entries
	*/
	@Override
	public long getPrimaryKey() {
		return _skippedEntries.getPrimaryKey();
	}

	/**
	* Sets the primary key of this skipped entries.
	*
	* @param primaryKey the primary key of this skipped entries
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_skippedEntries.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the ID of this skipped entries.
	*
	* @return the ID of this skipped entries
	*/
	@Override
	public long getId() {
		return _skippedEntries.getId();
	}

	/**
	* Sets the ID of this skipped entries.
	*
	* @param id the ID of this skipped entries
	*/
	@Override
	public void setId(long id) {
		_skippedEntries.setId(id);
	}

	/**
	* Returns the old p k of this skipped entries.
	*
	* @return the old p k of this skipped entries
	*/
	@Override
	public long getOldPK() {
		return _skippedEntries.getOldPK();
	}

	/**
	* Sets the old p k of this skipped entries.
	*
	* @param oldPK the old p k of this skipped entries
	*/
	@Override
	public void setOldPK(long oldPK) {
		_skippedEntries.setOldPK(oldPK);
	}

	/**
	* Returns the new p k of this skipped entries.
	*
	* @return the new p k of this skipped entries
	*/
	@Override
	public long getNewPK() {
		return _skippedEntries.getNewPK();
	}

	/**
	* Sets the new p k of this skipped entries.
	*
	* @param newPK the new p k of this skipped entries
	*/
	@Override
	public void setNewPK(long newPK) {
		_skippedEntries.setNewPK(newPK);
	}

	/**
	* Returns the table name of this skipped entries.
	*
	* @return the table name of this skipped entries
	*/
	@Override
	public java.lang.String getTableName() {
		return _skippedEntries.getTableName();
	}

	/**
	* Sets the table name of this skipped entries.
	*
	* @param TableName the table name of this skipped entries
	*/
	@Override
	public void setTableName(java.lang.String TableName) {
		_skippedEntries.setTableName(TableName);
	}

	@Override
	public boolean isNew() {
		return _skippedEntries.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_skippedEntries.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _skippedEntries.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_skippedEntries.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _skippedEntries.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _skippedEntries.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_skippedEntries.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _skippedEntries.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_skippedEntries.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_skippedEntries.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_skippedEntries.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new SkippedEntriesWrapper((SkippedEntries)_skippedEntries.clone());
	}

	@Override
	public int compareTo(org.han.service.model.SkippedEntries skippedEntries) {
		return _skippedEntries.compareTo(skippedEntries);
	}

	@Override
	public int hashCode() {
		return _skippedEntries.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<org.han.service.model.SkippedEntries> toCacheModel() {
		return _skippedEntries.toCacheModel();
	}

	@Override
	public org.han.service.model.SkippedEntries toEscapedModel() {
		return new SkippedEntriesWrapper(_skippedEntries.toEscapedModel());
	}

	@Override
	public org.han.service.model.SkippedEntries toUnescapedModel() {
		return new SkippedEntriesWrapper(_skippedEntries.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _skippedEntries.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _skippedEntries.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_skippedEntries.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof SkippedEntriesWrapper)) {
			return false;
		}

		SkippedEntriesWrapper skippedEntriesWrapper = (SkippedEntriesWrapper)obj;

		if (Validator.equals(_skippedEntries,
					skippedEntriesWrapper._skippedEntries)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public SkippedEntries getWrappedSkippedEntries() {
		return _skippedEntries;
	}

	@Override
	public SkippedEntries getWrappedModel() {
		return _skippedEntries;
	}

	@Override
	public void resetOriginalValues() {
		_skippedEntries.resetOriginalValues();
	}

	private SkippedEntries _skippedEntries;
}