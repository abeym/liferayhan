/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.han.service.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link OldNewMapping}.
 * </p>
 *
 * @author Rohan
 * @see OldNewMapping
 * @generated
 */
public class OldNewMappingWrapper implements OldNewMapping,
	ModelWrapper<OldNewMapping> {
	public OldNewMappingWrapper(OldNewMapping oldNewMapping) {
		_oldNewMapping = oldNewMapping;
	}

	@Override
	public Class<?> getModelClass() {
		return OldNewMapping.class;
	}

	@Override
	public String getModelClassName() {
		return OldNewMapping.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("id", getId());
		attributes.put("oldPK", getOldPK());
		attributes.put("newPK", getNewPK());
		attributes.put("TableName", getTableName());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long id = (Long)attributes.get("id");

		if (id != null) {
			setId(id);
		}

		Long oldPK = (Long)attributes.get("oldPK");

		if (oldPK != null) {
			setOldPK(oldPK);
		}

		Long newPK = (Long)attributes.get("newPK");

		if (newPK != null) {
			setNewPK(newPK);
		}

		String TableName = (String)attributes.get("TableName");

		if (TableName != null) {
			setTableName(TableName);
		}
	}

	/**
	* Returns the primary key of this old new mapping.
	*
	* @return the primary key of this old new mapping
	*/
	@Override
	public long getPrimaryKey() {
		return _oldNewMapping.getPrimaryKey();
	}

	/**
	* Sets the primary key of this old new mapping.
	*
	* @param primaryKey the primary key of this old new mapping
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_oldNewMapping.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the ID of this old new mapping.
	*
	* @return the ID of this old new mapping
	*/
	@Override
	public long getId() {
		return _oldNewMapping.getId();
	}

	/**
	* Sets the ID of this old new mapping.
	*
	* @param id the ID of this old new mapping
	*/
	@Override
	public void setId(long id) {
		_oldNewMapping.setId(id);
	}

	/**
	* Returns the old p k of this old new mapping.
	*
	* @return the old p k of this old new mapping
	*/
	@Override
	public long getOldPK() {
		return _oldNewMapping.getOldPK();
	}

	/**
	* Sets the old p k of this old new mapping.
	*
	* @param oldPK the old p k of this old new mapping
	*/
	@Override
	public void setOldPK(long oldPK) {
		_oldNewMapping.setOldPK(oldPK);
	}

	/**
	* Returns the new p k of this old new mapping.
	*
	* @return the new p k of this old new mapping
	*/
	@Override
	public long getNewPK() {
		return _oldNewMapping.getNewPK();
	}

	/**
	* Sets the new p k of this old new mapping.
	*
	* @param newPK the new p k of this old new mapping
	*/
	@Override
	public void setNewPK(long newPK) {
		_oldNewMapping.setNewPK(newPK);
	}

	/**
	* Returns the table name of this old new mapping.
	*
	* @return the table name of this old new mapping
	*/
	@Override
	public java.lang.String getTableName() {
		return _oldNewMapping.getTableName();
	}

	/**
	* Sets the table name of this old new mapping.
	*
	* @param TableName the table name of this old new mapping
	*/
	@Override
	public void setTableName(java.lang.String TableName) {
		_oldNewMapping.setTableName(TableName);
	}

	@Override
	public boolean isNew() {
		return _oldNewMapping.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_oldNewMapping.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _oldNewMapping.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_oldNewMapping.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _oldNewMapping.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _oldNewMapping.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_oldNewMapping.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _oldNewMapping.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_oldNewMapping.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_oldNewMapping.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_oldNewMapping.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new OldNewMappingWrapper((OldNewMapping)_oldNewMapping.clone());
	}

	@Override
	public int compareTo(org.han.service.model.OldNewMapping oldNewMapping) {
		return _oldNewMapping.compareTo(oldNewMapping);
	}

	@Override
	public int hashCode() {
		return _oldNewMapping.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<org.han.service.model.OldNewMapping> toCacheModel() {
		return _oldNewMapping.toCacheModel();
	}

	@Override
	public org.han.service.model.OldNewMapping toEscapedModel() {
		return new OldNewMappingWrapper(_oldNewMapping.toEscapedModel());
	}

	@Override
	public org.han.service.model.OldNewMapping toUnescapedModel() {
		return new OldNewMappingWrapper(_oldNewMapping.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _oldNewMapping.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _oldNewMapping.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_oldNewMapping.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof OldNewMappingWrapper)) {
			return false;
		}

		OldNewMappingWrapper oldNewMappingWrapper = (OldNewMappingWrapper)obj;

		if (Validator.equals(_oldNewMapping, oldNewMappingWrapper._oldNewMapping)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public OldNewMapping getWrappedOldNewMapping() {
		return _oldNewMapping;
	}

	@Override
	public OldNewMapping getWrappedModel() {
		return _oldNewMapping;
	}

	@Override
	public void resetOriginalValues() {
		_oldNewMapping.resetOriginalValues();
	}

	private OldNewMapping _oldNewMapping;
}