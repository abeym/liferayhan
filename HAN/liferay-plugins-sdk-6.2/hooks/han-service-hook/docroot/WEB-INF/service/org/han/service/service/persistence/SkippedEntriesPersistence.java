/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.han.service.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import org.han.service.model.SkippedEntries;

/**
 * The persistence interface for the skipped entries service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Rohan
 * @see SkippedEntriesPersistenceImpl
 * @see SkippedEntriesUtil
 * @generated
 */
public interface SkippedEntriesPersistence extends BasePersistence<SkippedEntries> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link SkippedEntriesUtil} to access the skipped entries persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the skipped entrieses where TableName = &#63; and oldPK = &#63;.
	*
	* @param TableName the table name
	* @param oldPK the old p k
	* @return the matching skipped entrieses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.han.service.model.SkippedEntries> findByentityNameAndOldPK(
		java.lang.String TableName, long oldPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the skipped entrieses where TableName = &#63; and oldPK = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.SkippedEntriesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param TableName the table name
	* @param oldPK the old p k
	* @param start the lower bound of the range of skipped entrieses
	* @param end the upper bound of the range of skipped entrieses (not inclusive)
	* @return the range of matching skipped entrieses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.han.service.model.SkippedEntries> findByentityNameAndOldPK(
		java.lang.String TableName, long oldPK, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the skipped entrieses where TableName = &#63; and oldPK = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.SkippedEntriesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param TableName the table name
	* @param oldPK the old p k
	* @param start the lower bound of the range of skipped entrieses
	* @param end the upper bound of the range of skipped entrieses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching skipped entrieses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.han.service.model.SkippedEntries> findByentityNameAndOldPK(
		java.lang.String TableName, long oldPK, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first skipped entries in the ordered set where TableName = &#63; and oldPK = &#63;.
	*
	* @param TableName the table name
	* @param oldPK the old p k
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching skipped entries
	* @throws org.han.service.NoSuchSkippedEntriesException if a matching skipped entries could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.han.service.model.SkippedEntries findByentityNameAndOldPK_First(
		java.lang.String TableName, long oldPK,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.han.service.NoSuchSkippedEntriesException;

	/**
	* Returns the first skipped entries in the ordered set where TableName = &#63; and oldPK = &#63;.
	*
	* @param TableName the table name
	* @param oldPK the old p k
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching skipped entries, or <code>null</code> if a matching skipped entries could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.han.service.model.SkippedEntries fetchByentityNameAndOldPK_First(
		java.lang.String TableName, long oldPK,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last skipped entries in the ordered set where TableName = &#63; and oldPK = &#63;.
	*
	* @param TableName the table name
	* @param oldPK the old p k
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching skipped entries
	* @throws org.han.service.NoSuchSkippedEntriesException if a matching skipped entries could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.han.service.model.SkippedEntries findByentityNameAndOldPK_Last(
		java.lang.String TableName, long oldPK,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.han.service.NoSuchSkippedEntriesException;

	/**
	* Returns the last skipped entries in the ordered set where TableName = &#63; and oldPK = &#63;.
	*
	* @param TableName the table name
	* @param oldPK the old p k
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching skipped entries, or <code>null</code> if a matching skipped entries could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.han.service.model.SkippedEntries fetchByentityNameAndOldPK_Last(
		java.lang.String TableName, long oldPK,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the skipped entrieses before and after the current skipped entries in the ordered set where TableName = &#63; and oldPK = &#63;.
	*
	* @param id the primary key of the current skipped entries
	* @param TableName the table name
	* @param oldPK the old p k
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next skipped entries
	* @throws org.han.service.NoSuchSkippedEntriesException if a skipped entries with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.han.service.model.SkippedEntries[] findByentityNameAndOldPK_PrevAndNext(
		long id, java.lang.String TableName, long oldPK,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.han.service.NoSuchSkippedEntriesException;

	/**
	* Removes all the skipped entrieses where TableName = &#63; and oldPK = &#63; from the database.
	*
	* @param TableName the table name
	* @param oldPK the old p k
	* @throws SystemException if a system exception occurred
	*/
	public void removeByentityNameAndOldPK(java.lang.String TableName,
		long oldPK) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of skipped entrieses where TableName = &#63; and oldPK = &#63;.
	*
	* @param TableName the table name
	* @param oldPK the old p k
	* @return the number of matching skipped entrieses
	* @throws SystemException if a system exception occurred
	*/
	public int countByentityNameAndOldPK(java.lang.String TableName, long oldPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the skipped entries in the entity cache if it is enabled.
	*
	* @param skippedEntries the skipped entries
	*/
	public void cacheResult(org.han.service.model.SkippedEntries skippedEntries);

	/**
	* Caches the skipped entrieses in the entity cache if it is enabled.
	*
	* @param skippedEntrieses the skipped entrieses
	*/
	public void cacheResult(
		java.util.List<org.han.service.model.SkippedEntries> skippedEntrieses);

	/**
	* Creates a new skipped entries with the primary key. Does not add the skipped entries to the database.
	*
	* @param id the primary key for the new skipped entries
	* @return the new skipped entries
	*/
	public org.han.service.model.SkippedEntries create(long id);

	/**
	* Removes the skipped entries with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param id the primary key of the skipped entries
	* @return the skipped entries that was removed
	* @throws org.han.service.NoSuchSkippedEntriesException if a skipped entries with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.han.service.model.SkippedEntries remove(long id)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.han.service.NoSuchSkippedEntriesException;

	public org.han.service.model.SkippedEntries updateImpl(
		org.han.service.model.SkippedEntries skippedEntries)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the skipped entries with the primary key or throws a {@link org.han.service.NoSuchSkippedEntriesException} if it could not be found.
	*
	* @param id the primary key of the skipped entries
	* @return the skipped entries
	* @throws org.han.service.NoSuchSkippedEntriesException if a skipped entries with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.han.service.model.SkippedEntries findByPrimaryKey(long id)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.han.service.NoSuchSkippedEntriesException;

	/**
	* Returns the skipped entries with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param id the primary key of the skipped entries
	* @return the skipped entries, or <code>null</code> if a skipped entries with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.han.service.model.SkippedEntries fetchByPrimaryKey(long id)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the skipped entrieses.
	*
	* @return the skipped entrieses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.han.service.model.SkippedEntries> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the skipped entrieses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.SkippedEntriesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of skipped entrieses
	* @param end the upper bound of the range of skipped entrieses (not inclusive)
	* @return the range of skipped entrieses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.han.service.model.SkippedEntries> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the skipped entrieses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.SkippedEntriesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of skipped entrieses
	* @param end the upper bound of the range of skipped entrieses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of skipped entrieses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.han.service.model.SkippedEntries> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the skipped entrieses from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of skipped entrieses.
	*
	* @return the number of skipped entrieses
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}