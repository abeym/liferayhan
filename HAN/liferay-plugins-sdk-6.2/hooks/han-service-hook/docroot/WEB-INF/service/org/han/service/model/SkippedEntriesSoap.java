/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.han.service.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Rohan
 * @generated
 */
public class SkippedEntriesSoap implements Serializable {
	public static SkippedEntriesSoap toSoapModel(SkippedEntries model) {
		SkippedEntriesSoap soapModel = new SkippedEntriesSoap();

		soapModel.setId(model.getId());
		soapModel.setOldPK(model.getOldPK());
		soapModel.setNewPK(model.getNewPK());
		soapModel.setTableName(model.getTableName());

		return soapModel;
	}

	public static SkippedEntriesSoap[] toSoapModels(SkippedEntries[] models) {
		SkippedEntriesSoap[] soapModels = new SkippedEntriesSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static SkippedEntriesSoap[][] toSoapModels(SkippedEntries[][] models) {
		SkippedEntriesSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new SkippedEntriesSoap[models.length][models[0].length];
		}
		else {
			soapModels = new SkippedEntriesSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static SkippedEntriesSoap[] toSoapModels(List<SkippedEntries> models) {
		List<SkippedEntriesSoap> soapModels = new ArrayList<SkippedEntriesSoap>(models.size());

		for (SkippedEntries model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new SkippedEntriesSoap[soapModels.size()]);
	}

	public SkippedEntriesSoap() {
	}

	public long getPrimaryKey() {
		return _id;
	}

	public void setPrimaryKey(long pk) {
		setId(pk);
	}

	public long getId() {
		return _id;
	}

	public void setId(long id) {
		_id = id;
	}

	public long getOldPK() {
		return _oldPK;
	}

	public void setOldPK(long oldPK) {
		_oldPK = oldPK;
	}

	public long getNewPK() {
		return _newPK;
	}

	public void setNewPK(long newPK) {
		_newPK = newPK;
	}

	public String getTableName() {
		return _TableName;
	}

	public void setTableName(String TableName) {
		_TableName = TableName;
	}

	private long _id;
	private long _oldPK;
	private long _newPK;
	private String _TableName;
}