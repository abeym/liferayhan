/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.han.service.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import org.han.service.model.SkippedEntries;

import java.util.List;

/**
 * The persistence utility for the skipped entries service. This utility wraps {@link SkippedEntriesPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Rohan
 * @see SkippedEntriesPersistence
 * @see SkippedEntriesPersistenceImpl
 * @generated
 */
public class SkippedEntriesUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(SkippedEntries skippedEntries) {
		getPersistence().clearCache(skippedEntries);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<SkippedEntries> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<SkippedEntries> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<SkippedEntries> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static SkippedEntries update(SkippedEntries skippedEntries)
		throws SystemException {
		return getPersistence().update(skippedEntries);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static SkippedEntries update(SkippedEntries skippedEntries,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(skippedEntries, serviceContext);
	}

	/**
	* Returns all the skipped entrieses where TableName = &#63; and oldPK = &#63;.
	*
	* @param TableName the table name
	* @param oldPK the old p k
	* @return the matching skipped entrieses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<org.han.service.model.SkippedEntries> findByentityNameAndOldPK(
		java.lang.String TableName, long oldPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByentityNameAndOldPK(TableName, oldPK);
	}

	/**
	* Returns a range of all the skipped entrieses where TableName = &#63; and oldPK = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.SkippedEntriesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param TableName the table name
	* @param oldPK the old p k
	* @param start the lower bound of the range of skipped entrieses
	* @param end the upper bound of the range of skipped entrieses (not inclusive)
	* @return the range of matching skipped entrieses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<org.han.service.model.SkippedEntries> findByentityNameAndOldPK(
		java.lang.String TableName, long oldPK, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByentityNameAndOldPK(TableName, oldPK, start, end);
	}

	/**
	* Returns an ordered range of all the skipped entrieses where TableName = &#63; and oldPK = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.SkippedEntriesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param TableName the table name
	* @param oldPK the old p k
	* @param start the lower bound of the range of skipped entrieses
	* @param end the upper bound of the range of skipped entrieses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching skipped entrieses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<org.han.service.model.SkippedEntries> findByentityNameAndOldPK(
		java.lang.String TableName, long oldPK, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByentityNameAndOldPK(TableName, oldPK, start, end,
			orderByComparator);
	}

	/**
	* Returns the first skipped entries in the ordered set where TableName = &#63; and oldPK = &#63;.
	*
	* @param TableName the table name
	* @param oldPK the old p k
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching skipped entries
	* @throws org.han.service.NoSuchSkippedEntriesException if a matching skipped entries could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static org.han.service.model.SkippedEntries findByentityNameAndOldPK_First(
		java.lang.String TableName, long oldPK,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.han.service.NoSuchSkippedEntriesException {
		return getPersistence()
				   .findByentityNameAndOldPK_First(TableName, oldPK,
			orderByComparator);
	}

	/**
	* Returns the first skipped entries in the ordered set where TableName = &#63; and oldPK = &#63;.
	*
	* @param TableName the table name
	* @param oldPK the old p k
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching skipped entries, or <code>null</code> if a matching skipped entries could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static org.han.service.model.SkippedEntries fetchByentityNameAndOldPK_First(
		java.lang.String TableName, long oldPK,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByentityNameAndOldPK_First(TableName, oldPK,
			orderByComparator);
	}

	/**
	* Returns the last skipped entries in the ordered set where TableName = &#63; and oldPK = &#63;.
	*
	* @param TableName the table name
	* @param oldPK the old p k
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching skipped entries
	* @throws org.han.service.NoSuchSkippedEntriesException if a matching skipped entries could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static org.han.service.model.SkippedEntries findByentityNameAndOldPK_Last(
		java.lang.String TableName, long oldPK,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.han.service.NoSuchSkippedEntriesException {
		return getPersistence()
				   .findByentityNameAndOldPK_Last(TableName, oldPK,
			orderByComparator);
	}

	/**
	* Returns the last skipped entries in the ordered set where TableName = &#63; and oldPK = &#63;.
	*
	* @param TableName the table name
	* @param oldPK the old p k
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching skipped entries, or <code>null</code> if a matching skipped entries could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static org.han.service.model.SkippedEntries fetchByentityNameAndOldPK_Last(
		java.lang.String TableName, long oldPK,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByentityNameAndOldPK_Last(TableName, oldPK,
			orderByComparator);
	}

	/**
	* Returns the skipped entrieses before and after the current skipped entries in the ordered set where TableName = &#63; and oldPK = &#63;.
	*
	* @param id the primary key of the current skipped entries
	* @param TableName the table name
	* @param oldPK the old p k
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next skipped entries
	* @throws org.han.service.NoSuchSkippedEntriesException if a skipped entries with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static org.han.service.model.SkippedEntries[] findByentityNameAndOldPK_PrevAndNext(
		long id, java.lang.String TableName, long oldPK,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.han.service.NoSuchSkippedEntriesException {
		return getPersistence()
				   .findByentityNameAndOldPK_PrevAndNext(id, TableName, oldPK,
			orderByComparator);
	}

	/**
	* Removes all the skipped entrieses where TableName = &#63; and oldPK = &#63; from the database.
	*
	* @param TableName the table name
	* @param oldPK the old p k
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByentityNameAndOldPK(java.lang.String TableName,
		long oldPK) throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByentityNameAndOldPK(TableName, oldPK);
	}

	/**
	* Returns the number of skipped entrieses where TableName = &#63; and oldPK = &#63;.
	*
	* @param TableName the table name
	* @param oldPK the old p k
	* @return the number of matching skipped entrieses
	* @throws SystemException if a system exception occurred
	*/
	public static int countByentityNameAndOldPK(java.lang.String TableName,
		long oldPK) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByentityNameAndOldPK(TableName, oldPK);
	}

	/**
	* Caches the skipped entries in the entity cache if it is enabled.
	*
	* @param skippedEntries the skipped entries
	*/
	public static void cacheResult(
		org.han.service.model.SkippedEntries skippedEntries) {
		getPersistence().cacheResult(skippedEntries);
	}

	/**
	* Caches the skipped entrieses in the entity cache if it is enabled.
	*
	* @param skippedEntrieses the skipped entrieses
	*/
	public static void cacheResult(
		java.util.List<org.han.service.model.SkippedEntries> skippedEntrieses) {
		getPersistence().cacheResult(skippedEntrieses);
	}

	/**
	* Creates a new skipped entries with the primary key. Does not add the skipped entries to the database.
	*
	* @param id the primary key for the new skipped entries
	* @return the new skipped entries
	*/
	public static org.han.service.model.SkippedEntries create(long id) {
		return getPersistence().create(id);
	}

	/**
	* Removes the skipped entries with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param id the primary key of the skipped entries
	* @return the skipped entries that was removed
	* @throws org.han.service.NoSuchSkippedEntriesException if a skipped entries with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static org.han.service.model.SkippedEntries remove(long id)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.han.service.NoSuchSkippedEntriesException {
		return getPersistence().remove(id);
	}

	public static org.han.service.model.SkippedEntries updateImpl(
		org.han.service.model.SkippedEntries skippedEntries)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(skippedEntries);
	}

	/**
	* Returns the skipped entries with the primary key or throws a {@link org.han.service.NoSuchSkippedEntriesException} if it could not be found.
	*
	* @param id the primary key of the skipped entries
	* @return the skipped entries
	* @throws org.han.service.NoSuchSkippedEntriesException if a skipped entries with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static org.han.service.model.SkippedEntries findByPrimaryKey(long id)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.han.service.NoSuchSkippedEntriesException {
		return getPersistence().findByPrimaryKey(id);
	}

	/**
	* Returns the skipped entries with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param id the primary key of the skipped entries
	* @return the skipped entries, or <code>null</code> if a skipped entries with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static org.han.service.model.SkippedEntries fetchByPrimaryKey(
		long id) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(id);
	}

	/**
	* Returns all the skipped entrieses.
	*
	* @return the skipped entrieses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<org.han.service.model.SkippedEntries> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the skipped entrieses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.SkippedEntriesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of skipped entrieses
	* @param end the upper bound of the range of skipped entrieses (not inclusive)
	* @return the range of skipped entrieses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<org.han.service.model.SkippedEntries> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the skipped entrieses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.SkippedEntriesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of skipped entrieses
	* @param end the upper bound of the range of skipped entrieses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of skipped entrieses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<org.han.service.model.SkippedEntries> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the skipped entrieses from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of skipped entrieses.
	*
	* @return the number of skipped entrieses
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static SkippedEntriesPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (SkippedEntriesPersistence)PortletBeanLocatorUtil.locate(org.han.service.service.ClpSerializer.getServletContextName(),
					SkippedEntriesPersistence.class.getName());

			ReferenceRegistry.registerReference(SkippedEntriesUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(SkippedEntriesPersistence persistence) {
	}

	private static SkippedEntriesPersistence _persistence;
}