/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.han.service.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Rohan
 * @generated
 */
public class AuditTrailSoap implements Serializable {
	public static AuditTrailSoap toSoapModel(AuditTrail model) {
		AuditTrailSoap soapModel = new AuditTrailSoap();

		soapModel.setId(model.getId());
		soapModel.setEventType(model.getEventType());
		soapModel.setClassName(model.getClassName());
		soapModel.setClassPK(model.getClassPK());
		soapModel.setRealUserId(model.getRealUserId());
		soapModel.setRealUserName(model.getRealUserName());
		soapModel.setRealUserEmail(model.getRealUserEmail());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setUserEmail(model.getUserEmail());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setMessage(model.getMessage());

		return soapModel;
	}

	public static AuditTrailSoap[] toSoapModels(AuditTrail[] models) {
		AuditTrailSoap[] soapModels = new AuditTrailSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static AuditTrailSoap[][] toSoapModels(AuditTrail[][] models) {
		AuditTrailSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new AuditTrailSoap[models.length][models[0].length];
		}
		else {
			soapModels = new AuditTrailSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static AuditTrailSoap[] toSoapModels(List<AuditTrail> models) {
		List<AuditTrailSoap> soapModels = new ArrayList<AuditTrailSoap>(models.size());

		for (AuditTrail model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new AuditTrailSoap[soapModels.size()]);
	}

	public AuditTrailSoap() {
	}

	public long getPrimaryKey() {
		return _id;
	}

	public void setPrimaryKey(long pk) {
		setId(pk);
	}

	public long getId() {
		return _id;
	}

	public void setId(long id) {
		_id = id;
	}

	public String getEventType() {
		return _eventType;
	}

	public void setEventType(String eventType) {
		_eventType = eventType;
	}

	public String getClassName() {
		return _className;
	}

	public void setClassName(String className) {
		_className = className;
	}

	public String getClassPK() {
		return _classPK;
	}

	public void setClassPK(String classPK) {
		_classPK = classPK;
	}

	public String getRealUserId() {
		return _realUserId;
	}

	public void setRealUserId(String realUserId) {
		_realUserId = realUserId;
	}

	public String getRealUserName() {
		return _realUserName;
	}

	public void setRealUserName(String realUserName) {
		_realUserName = realUserName;
	}

	public String getRealUserEmail() {
		return _realUserEmail;
	}

	public void setRealUserEmail(String realUserEmail) {
		_realUserEmail = realUserEmail;
	}

	public String getUserId() {
		return _userId;
	}

	public void setUserId(String userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public String getUserEmail() {
		return _userEmail;
	}

	public void setUserEmail(String userEmail) {
		_userEmail = userEmail;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public String getMessage() {
		return _message;
	}

	public void setMessage(String message) {
		_message = message;
	}

	private long _id;
	private String _eventType;
	private String _className;
	private String _classPK;
	private String _realUserId;
	private String _realUserName;
	private String _realUserEmail;
	private String _userId;
	private String _userName;
	private String _userEmail;
	private Date _createDate;
	private String _message;
}