/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.han.service.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import org.han.service.model.OldNewMapping;

/**
 * The persistence interface for the old new mapping service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Rohan
 * @see OldNewMappingPersistenceImpl
 * @see OldNewMappingUtil
 * @generated
 */
public interface OldNewMappingPersistence extends BasePersistence<OldNewMapping> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link OldNewMappingUtil} to access the old new mapping persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the old new mappings where TableName = &#63; and oldPK = &#63;.
	*
	* @param TableName the table name
	* @param oldPK the old p k
	* @return the matching old new mappings
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.han.service.model.OldNewMapping> findByentityNameAndOldPK(
		java.lang.String TableName, long oldPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the old new mappings where TableName = &#63; and oldPK = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.OldNewMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param TableName the table name
	* @param oldPK the old p k
	* @param start the lower bound of the range of old new mappings
	* @param end the upper bound of the range of old new mappings (not inclusive)
	* @return the range of matching old new mappings
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.han.service.model.OldNewMapping> findByentityNameAndOldPK(
		java.lang.String TableName, long oldPK, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the old new mappings where TableName = &#63; and oldPK = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.OldNewMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param TableName the table name
	* @param oldPK the old p k
	* @param start the lower bound of the range of old new mappings
	* @param end the upper bound of the range of old new mappings (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching old new mappings
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.han.service.model.OldNewMapping> findByentityNameAndOldPK(
		java.lang.String TableName, long oldPK, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first old new mapping in the ordered set where TableName = &#63; and oldPK = &#63;.
	*
	* @param TableName the table name
	* @param oldPK the old p k
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching old new mapping
	* @throws org.han.service.NoSuchOldNewMappingException if a matching old new mapping could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.han.service.model.OldNewMapping findByentityNameAndOldPK_First(
		java.lang.String TableName, long oldPK,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.han.service.NoSuchOldNewMappingException;

	/**
	* Returns the first old new mapping in the ordered set where TableName = &#63; and oldPK = &#63;.
	*
	* @param TableName the table name
	* @param oldPK the old p k
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching old new mapping, or <code>null</code> if a matching old new mapping could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.han.service.model.OldNewMapping fetchByentityNameAndOldPK_First(
		java.lang.String TableName, long oldPK,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last old new mapping in the ordered set where TableName = &#63; and oldPK = &#63;.
	*
	* @param TableName the table name
	* @param oldPK the old p k
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching old new mapping
	* @throws org.han.service.NoSuchOldNewMappingException if a matching old new mapping could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.han.service.model.OldNewMapping findByentityNameAndOldPK_Last(
		java.lang.String TableName, long oldPK,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.han.service.NoSuchOldNewMappingException;

	/**
	* Returns the last old new mapping in the ordered set where TableName = &#63; and oldPK = &#63;.
	*
	* @param TableName the table name
	* @param oldPK the old p k
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching old new mapping, or <code>null</code> if a matching old new mapping could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.han.service.model.OldNewMapping fetchByentityNameAndOldPK_Last(
		java.lang.String TableName, long oldPK,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the old new mappings before and after the current old new mapping in the ordered set where TableName = &#63; and oldPK = &#63;.
	*
	* @param id the primary key of the current old new mapping
	* @param TableName the table name
	* @param oldPK the old p k
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next old new mapping
	* @throws org.han.service.NoSuchOldNewMappingException if a old new mapping with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.han.service.model.OldNewMapping[] findByentityNameAndOldPK_PrevAndNext(
		long id, java.lang.String TableName, long oldPK,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.han.service.NoSuchOldNewMappingException;

	/**
	* Removes all the old new mappings where TableName = &#63; and oldPK = &#63; from the database.
	*
	* @param TableName the table name
	* @param oldPK the old p k
	* @throws SystemException if a system exception occurred
	*/
	public void removeByentityNameAndOldPK(java.lang.String TableName,
		long oldPK) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of old new mappings where TableName = &#63; and oldPK = &#63;.
	*
	* @param TableName the table name
	* @param oldPK the old p k
	* @return the number of matching old new mappings
	* @throws SystemException if a system exception occurred
	*/
	public int countByentityNameAndOldPK(java.lang.String TableName, long oldPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the old new mapping in the entity cache if it is enabled.
	*
	* @param oldNewMapping the old new mapping
	*/
	public void cacheResult(org.han.service.model.OldNewMapping oldNewMapping);

	/**
	* Caches the old new mappings in the entity cache if it is enabled.
	*
	* @param oldNewMappings the old new mappings
	*/
	public void cacheResult(
		java.util.List<org.han.service.model.OldNewMapping> oldNewMappings);

	/**
	* Creates a new old new mapping with the primary key. Does not add the old new mapping to the database.
	*
	* @param id the primary key for the new old new mapping
	* @return the new old new mapping
	*/
	public org.han.service.model.OldNewMapping create(long id);

	/**
	* Removes the old new mapping with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param id the primary key of the old new mapping
	* @return the old new mapping that was removed
	* @throws org.han.service.NoSuchOldNewMappingException if a old new mapping with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.han.service.model.OldNewMapping remove(long id)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.han.service.NoSuchOldNewMappingException;

	public org.han.service.model.OldNewMapping updateImpl(
		org.han.service.model.OldNewMapping oldNewMapping)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the old new mapping with the primary key or throws a {@link org.han.service.NoSuchOldNewMappingException} if it could not be found.
	*
	* @param id the primary key of the old new mapping
	* @return the old new mapping
	* @throws org.han.service.NoSuchOldNewMappingException if a old new mapping with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.han.service.model.OldNewMapping findByPrimaryKey(long id)
		throws com.liferay.portal.kernel.exception.SystemException,
			org.han.service.NoSuchOldNewMappingException;

	/**
	* Returns the old new mapping with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param id the primary key of the old new mapping
	* @return the old new mapping, or <code>null</code> if a old new mapping with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public org.han.service.model.OldNewMapping fetchByPrimaryKey(long id)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the old new mappings.
	*
	* @return the old new mappings
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.han.service.model.OldNewMapping> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the old new mappings.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.OldNewMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of old new mappings
	* @param end the upper bound of the range of old new mappings (not inclusive)
	* @return the range of old new mappings
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.han.service.model.OldNewMapping> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the old new mappings.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.OldNewMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of old new mappings
	* @param end the upper bound of the range of old new mappings (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of old new mappings
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<org.han.service.model.OldNewMapping> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the old new mappings from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of old new mappings.
	*
	* @return the number of old new mappings
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}