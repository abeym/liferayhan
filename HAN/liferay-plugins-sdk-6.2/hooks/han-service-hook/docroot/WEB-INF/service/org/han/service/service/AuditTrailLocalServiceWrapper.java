/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.han.service.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link AuditTrailLocalService}.
 *
 * @author Rohan
 * @see AuditTrailLocalService
 * @generated
 */
public class AuditTrailLocalServiceWrapper implements AuditTrailLocalService,
	ServiceWrapper<AuditTrailLocalService> {
	public AuditTrailLocalServiceWrapper(
		AuditTrailLocalService auditTrailLocalService) {
		_auditTrailLocalService = auditTrailLocalService;
	}

	/**
	* Adds the audit trail to the database. Also notifies the appropriate model listeners.
	*
	* @param auditTrail the audit trail
	* @return the audit trail that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public org.han.service.model.AuditTrail addAuditTrail(
		org.han.service.model.AuditTrail auditTrail)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _auditTrailLocalService.addAuditTrail(auditTrail);
	}

	/**
	* Creates a new audit trail with the primary key. Does not add the audit trail to the database.
	*
	* @param id the primary key for the new audit trail
	* @return the new audit trail
	*/
	@Override
	public org.han.service.model.AuditTrail createAuditTrail(long id) {
		return _auditTrailLocalService.createAuditTrail(id);
	}

	/**
	* Deletes the audit trail with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param id the primary key of the audit trail
	* @return the audit trail that was removed
	* @throws PortalException if a audit trail with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public org.han.service.model.AuditTrail deleteAuditTrail(long id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _auditTrailLocalService.deleteAuditTrail(id);
	}

	/**
	* Deletes the audit trail from the database. Also notifies the appropriate model listeners.
	*
	* @param auditTrail the audit trail
	* @return the audit trail that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public org.han.service.model.AuditTrail deleteAuditTrail(
		org.han.service.model.AuditTrail auditTrail)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _auditTrailLocalService.deleteAuditTrail(auditTrail);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _auditTrailLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _auditTrailLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.AuditTrailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _auditTrailLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.AuditTrailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _auditTrailLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _auditTrailLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _auditTrailLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public org.han.service.model.AuditTrail fetchAuditTrail(long id)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _auditTrailLocalService.fetchAuditTrail(id);
	}

	/**
	* Returns the audit trail with the primary key.
	*
	* @param id the primary key of the audit trail
	* @return the audit trail
	* @throws PortalException if a audit trail with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public org.han.service.model.AuditTrail getAuditTrail(long id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _auditTrailLocalService.getAuditTrail(id);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _auditTrailLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the audit trails.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.AuditTrailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of audit trails
	* @param end the upper bound of the range of audit trails (not inclusive)
	* @return the range of audit trails
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<org.han.service.model.AuditTrail> getAuditTrails(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _auditTrailLocalService.getAuditTrails(start, end);
	}

	/**
	* Returns the number of audit trails.
	*
	* @return the number of audit trails
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getAuditTrailsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _auditTrailLocalService.getAuditTrailsCount();
	}

	/**
	* Updates the audit trail in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param auditTrail the audit trail
	* @return the audit trail that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public org.han.service.model.AuditTrail updateAuditTrail(
		org.han.service.model.AuditTrail auditTrail)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _auditTrailLocalService.updateAuditTrail(auditTrail);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _auditTrailLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_auditTrailLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _auditTrailLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	@Override
	public org.han.service.model.AuditTrail auditLoginFailure(
		java.lang.String eventName, java.lang.String realUserId,
		java.lang.String realUserName, java.lang.String realUserEmail,
		java.lang.String userId, java.lang.String userName,
		java.lang.String userEmail, java.lang.String className,
		java.lang.String classPK,
		com.liferay.portal.kernel.json.JSONObject additionalInfo) {
		return _auditTrailLocalService.auditLoginFailure(eventName, realUserId,
			realUserName, realUserEmail, userId, userName, userEmail,
			className, classPK, additionalInfo);
	}

	@Override
	public org.han.service.model.AuditTrail auditLoginSuccess(
		java.lang.String eventName, java.lang.String realUserId,
		java.lang.String realUserName, java.lang.String realUserEmail,
		java.lang.String userId, java.lang.String userName,
		java.lang.String userEmail, java.lang.String className,
		java.lang.String classPK,
		com.liferay.portal.kernel.json.JSONObject additionalInfo) {
		return _auditTrailLocalService.auditLoginSuccess(eventName, realUserId,
			realUserName, realUserEmail, userId, userName, userEmail,
			className, classPK, additionalInfo);
	}

	@Override
	public org.han.service.model.AuditTrail auditUserLockout(
		java.lang.String eventName, java.lang.String realUserId,
		java.lang.String realUserName, java.lang.String realUserEmail,
		java.lang.String userId, java.lang.String userName,
		java.lang.String userEmail, java.lang.String className,
		java.lang.String classPK,
		com.liferay.portal.kernel.json.JSONObject additionalInfo) {
		return _auditTrailLocalService.auditUserLockout(eventName, realUserId,
			realUserName, realUserEmail, userId, userName, userEmail,
			className, classPK, additionalInfo);
	}

	@Override
	public java.util.List<org.han.service.model.AuditTrail> getAuditTrailEntries(
		com.liferay.portal.kernel.json.JSONObject jsonParameters, int start,
		int size) {
		return _auditTrailLocalService.getAuditTrailEntries(jsonParameters,
			start, size);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONArray getAuditTrailEntriesAsJson(
		com.liferay.portal.kernel.json.JSONObject jsonParameters, int start,
		int size) {
		return _auditTrailLocalService.getAuditTrailEntriesAsJson(jsonParameters,
			start, size);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject convertToJson(
		org.han.service.model.AuditTrail anAuditTrail) {
		return _auditTrailLocalService.convertToJson(anAuditTrail);
	}

	@Override
	public org.han.service.model.AuditTrail auditUserLogout(
		java.lang.String eventName, java.lang.String realUserId,
		java.lang.String realUserName, java.lang.String realUserEmail,
		java.lang.String userId, java.lang.String userName,
		java.lang.String userEmail, java.lang.String className,
		java.lang.String classPK,
		com.liferay.portal.kernel.json.JSONObject additionalInfo) {
		return _auditTrailLocalService.auditUserLogout(eventName, realUserId,
			realUserName, realUserEmail, userId, userName, userEmail,
			className, classPK, additionalInfo);
	}

	@Override
	public org.han.service.model.AuditTrail auditSessionDestroyed(
		java.lang.String eventName, java.lang.String realUserId,
		java.lang.String realUserName, java.lang.String realUserEmail,
		java.lang.String userId, java.lang.String userName,
		java.lang.String userEmail, java.lang.String className,
		java.lang.String classPK,
		com.liferay.portal.kernel.json.JSONObject additionalInfo) {
		return _auditTrailLocalService.auditSessionDestroyed(eventName,
			realUserId, realUserName, realUserEmail, userId, userName,
			userEmail, className, classPK, additionalInfo);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public AuditTrailLocalService getWrappedAuditTrailLocalService() {
		return _auditTrailLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedAuditTrailLocalService(
		AuditTrailLocalService auditTrailLocalService) {
		_auditTrailLocalService = auditTrailLocalService;
	}

	@Override
	public AuditTrailLocalService getWrappedService() {
		return _auditTrailLocalService;
	}

	@Override
	public void setWrappedService(AuditTrailLocalService auditTrailLocalService) {
		_auditTrailLocalService = auditTrailLocalService;
	}

	private AuditTrailLocalService _auditTrailLocalService;
}