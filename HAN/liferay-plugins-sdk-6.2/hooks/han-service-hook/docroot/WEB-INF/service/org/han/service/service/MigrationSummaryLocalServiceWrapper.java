/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package org.han.service.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link MigrationSummaryLocalService}.
 *
 * @author Rohan
 * @see MigrationSummaryLocalService
 * @generated
 */
public class MigrationSummaryLocalServiceWrapper
	implements MigrationSummaryLocalService,
		ServiceWrapper<MigrationSummaryLocalService> {
	public MigrationSummaryLocalServiceWrapper(
		MigrationSummaryLocalService migrationSummaryLocalService) {
		_migrationSummaryLocalService = migrationSummaryLocalService;
	}

	/**
	* Adds the migration summary to the database. Also notifies the appropriate model listeners.
	*
	* @param migrationSummary the migration summary
	* @return the migration summary that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public org.han.service.model.MigrationSummary addMigrationSummary(
		org.han.service.model.MigrationSummary migrationSummary)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _migrationSummaryLocalService.addMigrationSummary(migrationSummary);
	}

	/**
	* Creates a new migration summary with the primary key. Does not add the migration summary to the database.
	*
	* @param id the primary key for the new migration summary
	* @return the new migration summary
	*/
	@Override
	public org.han.service.model.MigrationSummary createMigrationSummary(
		long id) {
		return _migrationSummaryLocalService.createMigrationSummary(id);
	}

	/**
	* Deletes the migration summary with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param id the primary key of the migration summary
	* @return the migration summary that was removed
	* @throws PortalException if a migration summary with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public org.han.service.model.MigrationSummary deleteMigrationSummary(
		long id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _migrationSummaryLocalService.deleteMigrationSummary(id);
	}

	/**
	* Deletes the migration summary from the database. Also notifies the appropriate model listeners.
	*
	* @param migrationSummary the migration summary
	* @return the migration summary that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public org.han.service.model.MigrationSummary deleteMigrationSummary(
		org.han.service.model.MigrationSummary migrationSummary)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _migrationSummaryLocalService.deleteMigrationSummary(migrationSummary);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _migrationSummaryLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _migrationSummaryLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.MigrationSummaryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _migrationSummaryLocalService.dynamicQuery(dynamicQuery, start,
			end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.MigrationSummaryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _migrationSummaryLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _migrationSummaryLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _migrationSummaryLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public org.han.service.model.MigrationSummary fetchMigrationSummary(long id)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _migrationSummaryLocalService.fetchMigrationSummary(id);
	}

	/**
	* Returns the migration summary with the primary key.
	*
	* @param id the primary key of the migration summary
	* @return the migration summary
	* @throws PortalException if a migration summary with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public org.han.service.model.MigrationSummary getMigrationSummary(long id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _migrationSummaryLocalService.getMigrationSummary(id);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _migrationSummaryLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the migration summaries.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link org.han.service.model.impl.MigrationSummaryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of migration summaries
	* @param end the upper bound of the range of migration summaries (not inclusive)
	* @return the range of migration summaries
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<org.han.service.model.MigrationSummary> getMigrationSummaries(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _migrationSummaryLocalService.getMigrationSummaries(start, end);
	}

	/**
	* Returns the number of migration summaries.
	*
	* @return the number of migration summaries
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getMigrationSummariesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _migrationSummaryLocalService.getMigrationSummariesCount();
	}

	/**
	* Updates the migration summary in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param migrationSummary the migration summary
	* @return the migration summary that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public org.han.service.model.MigrationSummary updateMigrationSummary(
		org.han.service.model.MigrationSummary migrationSummary)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _migrationSummaryLocalService.updateMigrationSummary(migrationSummary);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _migrationSummaryLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_migrationSummaryLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _migrationSummaryLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	@Override
	public org.han.service.model.MigrationSummary FindByTableName(
		java.lang.String tableName) {
		return _migrationSummaryLocalService.FindByTableName(tableName);
	}

	@Override
	public org.han.service.model.MigrationSummary addEntry(
		java.lang.String tableName, java.util.Date startDateTime,
		java.util.Date endDateTime, java.lang.Integer skipped,
		java.lang.Integer added, java.lang.Integer totalFound) {
		return _migrationSummaryLocalService.addEntry(tableName, startDateTime,
			endDateTime, skipped, added, totalFound);
	}

	@Override
	public org.han.service.model.MigrationSummary updateEntry(
		org.han.service.model.MigrationSummary migrationSummary,
		java.util.Date endDateTime, java.lang.Integer skipped,
		java.lang.Integer added) {
		return _migrationSummaryLocalService.updateEntry(migrationSummary,
			endDateTime, skipped, added);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public MigrationSummaryLocalService getWrappedMigrationSummaryLocalService() {
		return _migrationSummaryLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedMigrationSummaryLocalService(
		MigrationSummaryLocalService migrationSummaryLocalService) {
		_migrationSummaryLocalService = migrationSummaryLocalService;
	}

	@Override
	public MigrationSummaryLocalService getWrappedService() {
		return _migrationSummaryLocalService;
	}

	@Override
	public void setWrappedService(
		MigrationSummaryLocalService migrationSummaryLocalService) {
		_migrationSummaryLocalService = migrationSummaryLocalService;
	}

	private MigrationSummaryLocalService _migrationSummaryLocalService;
}